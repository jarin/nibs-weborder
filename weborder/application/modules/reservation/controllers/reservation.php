<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Reservation Controller
 *
 * @category Controller
 * @package  CodeIgniter
 * @author   Jarin Anika <jarinanika@gmail.com>
 * @license  http://directory.fsf.org/wiki/License:ReciprocalPLv1.3 Reciprocal Public License v1.3
 * @link     http://nibssolutions.com
 */
class Reservation extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->data['module'] = 'reservation';
	}
	
	function index(){
		$user = array();
		$restuarntInfo = array();
		$reservationTime = array();
		$this->data['breadcrumb']['Reservation'] = 'reservation';
		$this->data['tab'] = 'reservation';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		if($this->session->userdata("userid") ==''){
			$this->form_validation->set_rules('userName', 'User Name', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
		}
		
		$uid = $this->session->userdata("userid");
		$userAr = $this->common_m->getByDql("SELECT t, to, s, c FROM entities\\ResCustomer t LEFT JOIN t.town to LEFT JOIN t.state s LEFT JOIN t.country c WHERE t.id = '".$uid."' ORDER BY t.customerKey");
		if(count($userAr) > 0){
			$user = $userAr[0];
		}
		$promo = $this->common_m->getByDql("SELECT t FROM entities\\ResPromo t");
		$b_id=$this->session->userdata('restaurantid');
		
		$restuarntInfoAr = $this->common_m->runSQL("SELECT * FROM restaurant WHERE id=$b_id AND is_deposit = 1 AND deposit_week_day = (Select IF(WEEKDAY(CURDATE())=6,  1, WEEKDAY(CURDATE())+2))");
		if(count($restuarntInfoAr) > 0){
			$restuarntInfo = $restuarntInfoAr[0];
		}
		
		$reservationTimeAr = $this->common_m->getBy("ResReservationTime", "", "", array("timeFrom" => "asc", "timeTo"=>"asc"));
		if(count($reservationTimeAr)){
			
			$reservationTime = $reservationTimeAr;
		}
		if ($this->form_validation->run() == FALSE)
		{
			$this->viewPage('reservation',array('user'=>$user, 'promo' => $promo, 'restuarntInfo'=>$restuarntInfo, 'reservationTime'=>$reservationTime));
		} else {
			$data = $this->input->post(NULL, TRUE);
			$ary = $data;
			
			if($data['town']==''){
				$data['town']=$data['town'];
			}else{
				$town = $this->common_m->getAValue("ResCity", "id", array("cityName"=>$data['town']));
				if($town===FALSE){
					$result = $this->common_m->insert("ResCity", array("cityName"=>$data['town']));
					$data['town'] = $result;
				}else{
					$data['town'] = $town;
				}
			}
			
			if($data['state']==''){
				$data['state']='';
			}else{
				$state = $this->common_m->getAValue("ResState", "id", array("stateName"=>$data['state']));
				if($state===FALSE){
					$result = $this->common_m->insert("ResState", array("stateName"=>$data['state']));
					$data['state'] = $result;
				}else{
					$data['state'] = $state;
				}
			}
			
			if($data['country']==''){
				$data['country']=1;
			}else{
				$country = $this->common_m->getAValue("ResCountry", "id", array("countryName"=>$data['country']));
				if($country===FALSE){
					$result = $this->common_m->insert("ResCountry", array("countryName"=>$data['country']));
					$data['country'] = $result;
				}else{
					$data['country'] = $country;
				}
			}
			
			$address1 = $this->common_m->getAValue("ResSettings", "value", array("settingsName"=>"address"));
			$address2 = "";
			if(isset($ary['house_no'])){
				$address2 .= $ary['house_no'].',';
			}
			if(isset($ary['house_name'])){
				$address2 .= $ary['house_name'].',';
			}
			if(isset($ary['street'])){
				$address2 .= $ary['street'].',';
			}
			if($ary['town']!=''){
				$address2 .= $ary['town'].',';
			}
			if($ary['state']!=''){
				$address2 .= $ary['state'].',';
			}
			if(isset($ary['post_code'])){
				$address2 .= $ary['post_code'].',';
			}
			if(isset($ary['country'])){
				$address2 .= $ary['country'].',';
			}
			if($address2!=''){
				$address2 = substr($address2, 0, strlen($address2)-1);
				$insertAry['dist'] = number_format(_calculateDrivingDistanceFromAddress($address1, $address2),1).'miles';
			}
			
			if($this->session->userdata("userid") ==''){
				$result = $this->common_m->isUnique("ResCustomer", array("userName" => $data['userName']));
				if ($result) {
					$insertAry = array(
							"customerName"=>$data['name'],
							"houseNo"=>$data['house_no'],
							"houseName"=>$data['house_name'],
							"street"=>$data['street'],
							"townId"=>$data['town'],
							"stateId"=>$data['state'],
							"postCode"=>$data['post_code'],
							"countryId"=>$data['country'],
							"email"=>$data['email'],
							"mobile"=>$data['phone'],
							"customerKey"=>generateCustomerCode(),
							
					);
					if($this->session->userdata("userid") ==''){
					$insertAry['userName'] = $data['userName'];
					$insertAry['password'] = makeEncryptedPassword($data['password']);
					}
						
				
						
					$result = $this->common_m->insert("ResCustomer", $insertAry);
					if(is_numeric($result)){
						$user_id = $result;
						$this->session->set_userdata("userid", $result);
					
						$this->session->set_userdata("customer_name", $data['userName']);
						
					}else{
						$this->session->set_flashdata("error", $result);
						redirect('restaurant/'. $this->data['restaurant'] ."/reservation");
					}
				} else {
				$this->session->set_flashdata("error", "This Customer Name is already Added.");
				if($this->session->userdata('restaurantid') !=''){
	            	redirect('restaurant/'. $this->data['restaurant'] ."/reservation");
	            } else {
	            	redirect('reservation');
	            }
			}
			}else {
				$updateAry = array(
						"customerName"=>$data['name'],
						"houseNo"=>$data['house_no'],
						"houseName"=>$data['house_name'],
						"street"=>$data['street'],
						"townId"=>$data['town'],
						"stateId"=>$data['state'],
						"postCode"=>$data['post_code'],
						"countryId"=>$data['country'],
						"email"=>$data['email'],
						"mobile"=>$data['phone'],
						"customerKey"=>generateCustomerCode(),
							
				);
				
				$user_id = $this->session->userdata("userid");
				$this->common_m->update("ResCustomer", $updateAry , array("id"=>$user_id));
			}
			
			$reservationTypeId = $this->common_m->getAValue("ResReservationType", "id", array("reservationType"=>'Web'));
			$promoTypeId = $this->common_m->getAValue("ResPromo", "id", array("promoType"=>$data['promotional_Code']));
			
			$reservationAry = array(
				'customerId' => $user_id,
				'reservationTypeId' => $reservationTypeId,
				'promoId' =>$promoTypeId,				
				'timeFrom' => $data['resevation_date']. ' ' .$data['reservation_time'],
				'timeTo' => $data['resevation_date']. ' ' .$data['reservation_time'],
				'noOfGuests' => $data['people'],
				'specialInstruction' => $data['request'],				
				'reservationDate' => $data['resevation_date'],				
				//'sendConfirmation' => $data['confirmation'],
				"isActive"=>1,
				'restaurantId' => $this->session->userdata('restaurantid')
			);
			
				if(isset($data['payment']) && isset($data['deposit_amount'])){
					if(count($restuarntInfo) > 0 && isset($restuarntInfo['deposit_amount'])){					
						$depositTypeId = $this->common_m->getAValue("ResDepositType", "id", array("depositType"=>'Web visit restaurant and pay'));
						$reservationAry['depositAmount']= $restuarntInfo['deposit_amount'];
						$reservationAry['depositTypeId']= $depositTypeId;
					}
				}
			
			$reservation_id = $this->common_m->insert("ResReservation", $reservationAry);
			if($reservation_id){				
				if(isset($data['payment']) && $data['payment'] == 'paypal'){
					$this->reservationPay($reservation_id);
				} else {
					$message ='Reservation has been placed successfully';
					$this->viewPage('notification', array('message'=>$message));
				}
				
			}
		}
	}
	
	function reservationPay($reservation_id){
		$restuarntInfo = array();
		if($this->session->userdata('restaurantid') !=''){
			$restuarntInfoAr = $this->common_m->runSQL("SELECT * FROM restaurant WHERE id=".$this->session->userdata('restaurantid')." AND is_deposit = 1 AND deposit_week_day = (Select IF(WEEKDAY(CURDATE())=6,  1, WEEKDAY(CURDATE())+2))");
			if(count($restuarntInfoAr) > 0){
				$restuarntInfo = $restuarntInfoAr[0];
			}
		}
		$settings =  $this->common_m->runSQL("SELECT field_name,value FROM payment_gateway WHERE gateway='paypal'");
		$gateway = array();
		foreach ($settings as $setting){
			$gateway[$setting['field_name']] = $setting['value'];
		}
		$reservInfo = $this->common_m->getByDql("SELECT t, p FROM entities\\ResReservation t LEFT JOIN t.promo p WHERE t.id = " .$reservation_id. "");
		
		$this->viewPage('reservation_payment', array('restuarntInfo'=>$restuarntInfo, 'gateway' => $gateway, 'reservInfo'=>$reservInfo[0]));
	}
	
}