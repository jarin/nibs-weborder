<?php



namespace entities;

/**
 * ResQuantityType
 *
 * @Table(name="res_quantity_type")
 * @Entity
 */
class ResQuantityType
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $quantityTypeName
   *
   * @Column(name="quantity_type_name", type="string", length=150, nullable=false)
   */
  private $quantityTypeName;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;


  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set quantityTypeName
   *
   * @param string $quantityTypeName
   */
  public function setQuantityTypeName($quantityTypeName)
  {
    $this->quantityTypeName = $quantityTypeName;
  }

  /**
   * Get quantityTypeName
   *
   * @return string 
   */
  public function getQuantityTypeName()
  {
    return $this->quantityTypeName;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
}