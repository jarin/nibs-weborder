<?php



namespace entities;

/**
 * ResCustomer
 *
 * @Table(name="res_customer")
 * @Entity
 */
class ResCustomer
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $customerKey
   *
   * @Column(name="customer_key", type="string", length=10, nullable=false)
   */
  private $customerKey;
  
  /**
   * @var decimal $availableCredit
   *
   * @Column(name="available_credit", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $availableCredit=0;

  /**
   * @var string $customerName
   *
   * @Column(name="customer_name", type="string", length=50, nullable=false)
   */
  private $customerName;

  /**
   * @var string $houseNo
   *
   * @Column(name="house_no", type="string", length=150, nullable=true)
   */
  private $houseNo;
  
  /**
   * @var string $houseName
   *
   * @Column(name="house_name", type="string", length=150, nullable=true)
   */
  private $houseName;
  
  /**
   * @var string $street
   *
   * @Column(name="street", type="string", length=150, nullable=true)
   */
  private $street;
  
  /**
   * @var ResCity
   *
   * @ManyToOne(targetEntity="ResCity")
   * @JoinColumns({
   *   @JoinColumn(name="town_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $town;
  
  /**
   * @var ResState
   *
   * @ManyToOne(targetEntity="ResState")
   * @JoinColumns({
   *   @JoinColumn(name="state_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $state;
  
  /**
   * @var string $postCode
   *
   * @Column(name="post_code", type="string", length=150, nullable=true)
   */
  private $postCode;
  
  /**
   * @var ResCountry
   *
   * @ManyToOne(targetEntity="ResCountry")
   * @JoinColumns({
   *   @JoinColumn(name="country_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $country;
  
  /**
   * @var string $email
   *
   * @Column(name="email", type="string", length=50, nullable=true)
   */
  private $email;
  
  /**
   * @var string $mobile
   *
   * @Column(name="mobile", type="string", length=50, nullable=true)
   */
  private $mobile;
  
  /**
   * @var string $landLine
   *
   * @Column(name="land_line", type="string", length=50, nullable=true)
   */
  private $landLine;
  
  /**
   * @var string $membershipNo
   *
   * @Column(name="membership_no", type="string", length=50, nullable=true)
   */
  private $membershipNo;
  
  /**
   * @var string $membershipPoints
   *
   * @Column(name="membership_points", type="integer", nullable=true)
   */
  private $membershipPoints;
  
  /**
   * @var string $dist
   *
   * @Column(name="dist", type="string", length=50, nullable=true)
   */
  private $dist;
  
  /**
   * @var date $dateOfBirth
   *
   * @Column(name="date_of_birth", type="date", nullable=true)
   */
  private $dateOfBirth;
  
  /**
   * @var date $anniversaryDate
   *
   * @Column(name="anniversary_date", type="date", nullable=true)
   */
  private $anniversaryDate;
  
  /**
   * @var string $userName
   *
   * @Column(name="user_name", type="string", length=150, nullable=true)
   */
  private $userName;
  
  /**
   * @var string $password
   *
   * @Column(name="password", type="string", length=40, nullable=true)
   */
  private $password;
  
  /**
   * @var string $ipAddress
   *
   * @Column(name="ip_address", type="string", length=15, nullable=true)
   */
  private $ipAddress;
  
  /**
   * @var datetime $lastActivity
   *
   * @Column(name="last_activity", type="datetime", nullable=true)
   */
  private $lastActivity;
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set customerKey
   *
   * @param string $customerKey
   */
  public function setCustomerKey($customerKey)
  {
    $this->customerKey = $customerKey;
  }

  /**
   * Get customerKey
   *
   * @return string 
   */
  public function getCustomerKey()
  {
    return $this->customerKey;
  }

  /**
   * Set customerName
   *
   * @param string $customerName
   */
  public function setCustomerName($customerName)
  {
    $this->customerName = $customerName;
  }

  /**
   * Get customerName
   *
   * @return string 
   */
  public function getCustomerName()
  {
    return $this->customerName;
  }

  /**
   * Set houseNo
   *
   * @param string $houseNo
   */
  public function setHouseNo($houseNo)
  {
    $this->houseNo = $houseNo;
  }

  /**
   * Get houseNo
   *
   * @return string 
   */
  public function getHouseNo()
  {
    return $this->houseNo;
  }
  
  /**
   * Set houseName
   *
   * @param string $houseName
   */
  public function setHouseName($houseName)
  {
  	$this->houseName = $houseName;
  }
  
  /**
   * Get houseName
   *
   * @return string
   */
  public function getHouseName()
  {
  	return $this->houseName;
  }
  
  /**
   * Set street
   *
   * @param string $street
   */
  public function setStreet($street)
  {
  	$this->street = $street;
  }
  
  /**
   * Get street
   *
   * @return string
   */
  public function getStreet()
  {
  	return $this->street;
  }
  
  /**
   * Set postCode
   *
   * @param string $postCode
   */
  public function setPostCode($postCode)
  {
  	$this->postCode = $postCode;
  }
  
  /**
   * Get postCode
   *
   * @return string
   */
  public function getPostCode()
  {
  	return $this->postCode;
  }
  
  /**
   * Set mobile
   *
   * @param string $mobile
   */
  public function setMobile($mobile)
  {
  	$this->mobile = $mobile;
  }
  
  /**
   * Get mobile
   *
   * @return string
   */
  public function getMobile()
  {
  	return $this->mobile;
  }
  
  /**
   * Set landLine
   *
   * @param string $landLine
   */
  public function setLandLine($landLine)
  {
  	$this->landLine = $landLine;
  }
  
  /**
   * Get landLine
   *
   * @return string
   */
  public function getLandLine()
  {
  	return $this->landLine;
  }
  
  /**
   * Set email
   *
   * @param string $email
   */
  public function setEmail($email)
  {
  	$this->email = $email;
  }
  
  /**
   * Get email
   *
   * @return string
   */
  public function getEmail()
  {
  	return $this->email;
  }

  /**
   * Set town
   *
   * @param ResCity $town
   */
  public function setTown(\ResCity $town)
  {
    $this->town = $town;
  }

  /**
   * Get town
   *
   * @return ResCity 
   */
  public function getTown()
  {
    return $this->town;
  }

  /**
   * Set country
   *
   * @param ResCountry $country
   */
  public function setCountry(\ResCountry $country)
  {
    $this->country = $country;
  }

  /**
   * Get country
   *
   * @return ResCountry 
   */
  public function getCountry()
  {
    return $this->country;
  }

  /**
   * Set state
   *
   * @param ResState $state
   */
  public function setState(\ResState $state)
  {
    $this->state = $state;
  }

  /**
   * Get state
   *
   * @return ResState 
   */
  public function getState()
  {
    return $this->state;
  }
  
  /**
   * Set availableCredit
   *
   * @param decimal $availableCredit
   */
  public function setAvailableCredit($availableCredit)
  {
  	$this->availableCredit = $availableCredit;
  }
  
  /**
   * Get availableCredit
   *
   * @return decimal
   */
  public function getAvailableCredit()
  {
  	return $this->availableCredit;
  }
  
  /**
   * Set membershipNo
   *
   * @param string $membershipNo
   */
  public function setMembershipNo($membershipNo)
  {
  	$this->membershipNo = $membershipNo;
  }
  
  /**
   * Get membershipNo
   *
   * @return string
   */
  public function getMembershipNo()
  {
  	return $this->membershipNo;
  }
  
  /**
   * Set membershipPoints
   *
   * @param string $membershipPoints
   */
  public function setMembershipPoints($membershipPoints)
  {
  	$this->membershipPoints = $membershipPoints;
  }
  
  /**
   * Get membershipPoints
   *
   * @return string
   */
  public function getMembershipPoints()
  {
  	return $this->membershipPoints;
  }
  
  /**
   * Set dist
   *
   * @param string $dist
   */
  public function setDist($dist)
  {
  	$this->dist = $dist;
  }
  
  /**
   * Get dist
   *
   * @return string
   */
  public function getDist()
  {
  	return $this->dist;
  }
  
  /**
   * Set dateOfBirth
   *
   * @param date $dateOfBirth
   */
  public function setDateOfBirth($dateOfBirth)
  {
      $this->dateOfBirth = $dateOfBirth;
  }
  
  /**
   * Get dateOfBirth
   *
   * @return date
   */
  public function getDateOfBirth()
  {
      return $this->dateOfBirth;
  }
  
  /**
   * Set anniversaryDate
   *
   * @param date $anniversaryDate
   */
  public function setAnniversaryDate($anniversaryDate)
  {
      $this->anniversaryDate = $anniversaryDate;
  }
  
  /**
   * Get anniversaryDate
   *
   * @return date
   */
  public function getAnniversaryDate()
  {
      return $this->anniversaryDate;
  }
  
  /**
   * Set userName
   *
   * @param string $userName
   */
  public function setUserName($userName)
  {
      $this->userName = $userName;
  }
  
  /**
   * Get userName
   *
   * @return string
   */
  public function getUserName()
  {
      return $this->userName;
  }
  
  /**
   * Set password
   *
   * @param string $password
   */
  public function setPassword($password)
  {
      $this->password = $password;
  }
  
  /**
   * Get password
   *
   * @return string
   */
  public function getPassword()
  {
      return $this->password;
  }
  
  /**
   * Set ipAddress
   *
   * @param string $ipAddress
   */
  public function setIpAddress($ipAddress)
  {
      $this->ipAddress = $ipAddress;
  }
  
  /**
   * Get ipAddress
   *
   * @return string
   */
  public function getIpAddress()
  {
      return $this->ipAddress;
  }
  
  /**
   * Set lastActivity
   *
   * @param datetime $lastActivity
   */
  public function setLastActivity($lastActivity)
  {
      $this->lastActivity = $lastActivity;
  }
  
  /**
   * Get lastActivity
   *
   * @return datetime
   */
  public function getLastActivity()
  {
      return $this->lastActivity;
  }
}