<?php



namespace entities;

/**
 * ResStatus
 *
 * @Table(name="res_status")
 * @Entity
 */
class ResStatus
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $status
   *
   * @Column(name="status", type="string", length=150, nullable=false)
   */
  private $status;
  
  /**
   * @var string $topColor
   *
   * @Column(name="top_color", type="string", length=9, nullable=false)
   */
  private $topColor;
  
  /**
   * @var string $bottomColor
   *
   * @Column(name="bottom_color", type="string", length=9, nullable=false)
   */
  private $bottomColor;
  
  /**
   * @var boolean $isFree
   *
   * @Column(name="is_free", type="boolean", nullable=false)
   */
  private $isFree=TRUE;


  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set status
   *
   * @param string $status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * Get status
   *
   * @return string 
   */
  public function getStatus()
  {
    return $this->status;
  }
  
  /**
   * Set isFree
   *
   * @param boolean $isFree
   */
  public function setIsFree($isFree)
  {
  	$this->isFree = $isFree;
  }
  
  /**
   * Get isFree
   *
   * @return boolean
   */
  public function getIsFree()
  {
  	return $this->isFree;
  }
  
  /**
   * Set topColor
   *
   * @param string $topColor
   */
  public function setTopColor($topColor)
  {
  	$this->topColor = $topColor;
  }
  
  /**
   * Get topColor
   *
   * @return string
   */
  public function getTopColor()
  {
  	return $this->topColor;
  }
  
  /**
   * Set bottomColor
   *
   * @param string $bottomColor
   */
  public function setBottomColor($bottomColor)
  {
  	$this->bottomColor = $bottomColor;
  }
  
  /**
   * Get bottomColor
   *
   * @return string
   */
  public function getBottomColor()
  {
  	return $this->bottomColor;
  }
}