<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/reservation.css" type="text/css" media="screen"  />
<link href="<?=$baseUrl?>assets/css/admin/daterangepicker-bs2.css" rel="stylesheet" media="screen">
<script src="<?=$baseUrl?>assets/js/moment.min.js"></script>
<script src="<?=$baseUrl?>assets/js/daterangepicker.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/reservation.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
	<h3><?=$pagename?></h3>
	<div class="initPage">
		<div>
		  <p>Click on the list of Reservation on the left to view details....</p>
		</div>
	</div>
</script> 
<div id="thirdBar" style="display:none"></div>
<div id="main-content" class=''>
  	<div class="container-fluid">
	    <div id="page" class='site-settings'>
	       	<div class="row-fluid" id="dContent"></div>
	  	</div>
	</div>
</div>


<div id="sidebar" class="nav-collapse collapse">
	
	<div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
	
		<form method='post'  action='<?=base_url()?>admin/reservation' loadingDiv='loader' autocomplete='off' enctype='multipart/form-data' style="margin:0px;padding:20px;">
		<table cellpadding='4' cellspacing='0' align='center'>
			<tr>
				<div class="control-group">
					<label class="control-label" for="daterange" style="display: inline-block;width: 100px;line-height: 30px;">Date Range</label>
        			        <div class="controls" style="display: inline-block;height: 30px;">
						<div class="input-prepend">
							<span class="add-on"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
							<input type="text" style="width: 200px;" name="dates" id="daterange" class="daterange" value="<?php echo $dates;?>" />
						</div>
					</div>
				</div>
			</tr>
		</table>
	</form>
	<br/>
  	
  
  	<ul class="sidebar-menu list">
   	 <?=$list?>
  	</ul>
  	<div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>
<script>
var body=$("body");
	$(document).ready(function(){
		
	

		body.find('#daterange').daterangepicker({
				ranges: {
			  	'Today': [moment(), moment()],
			    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			    'Last 7 Days': [moment().subtract('days', 6), moment()],
			    'Last 30 Days': [moment().subtract('days', 29), moment()],
			    'This Month': [moment().startOf('month'), moment().endOf('month')],
			    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
				},
			  startDate: moment().subtract('days', 29),
			  endDate: moment()
			},
			function(start, end) {
				body.find('#daterange').val(start.format('YYYY.MM.DD') + ' - ' + end.format('YYYY.MM.DD'));
				body.find("form").submit();
		});
		body.find('.order').on("click", function(e){
			e.preventDefault();
			var _this = $(this),
				sortby = _this.attr('rel'),
				row = body.find('#orderdiv');
			
				$.ajax({
		    		async: false, 
		    		type: "POST",
		    		url: "admin/home/orderlist",
		    		data: "sortby="+ sortby,
		    		success:function(data){
		    		  row.html(data);		    		
		    		}
		    	});
			}).trigger('click');

		});
</script>
