<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reservation_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->reservation="res_reservation";
		$this->promo="res_promo";
		$this->reservationType="res_reservation_type";
		$this->customer="res_customer";
		$this->city="res_city";
		$this->state="res_state";
		$this->country="res_country";
		$this->table = "res_table";
		$this->reservationStatus = "res_reservation_status";
		$this->depositType = "res_deposit_type";
		$this->tablename=$this->reservation;
	}
		
	function getReservationTypeId($status){
		 $reservation = $this->db->select("id")
		->where("reservation_type",$status)
		->get($this->reservationType)->row_array();
		
		 return $reservation['id'];
	}
	
	function getAllReservation($restaurantId, $typeId, $from_date, $to_date){
		return $this->db->select("*")
		->where("restaurant_id",$restaurantId)
		->where("reservation_type_id",$typeId)
		->where('reservation_date >=',str_replace('.', '/', $from_date).' 00:00:00')
		->where('reservation_date <=',str_replace('.', '/', $to_date).' 23:59:59')
		->order_by('time_from','asc')
		->get($this->tablename)->result_array();
	}
	
	function getReservation($id){
		return $this->db->select("*,$this->tablename.id as id")
		->where("$this->tablename.id",$id)
		->join($this->customer,"$this->customer.id=$this->tablename.customer_id",'left')
		->join($this->promo,"$this->promo.id=$this->tablename.promo_id",'left')
		->join("$this->city","$this->customer.town_id = $this->city.id",'left')
		->join("$this->state","$this->customer.state_id = $this->state.id",'left')
		->join("$this->country","$this->customer.country_id = $this->country.id",'left')
		->join("$this->table","$this->tablename.table_id = $this->table.id",'left')
		->join("$this->reservationStatus","$this->tablename.reservation_status_id = $this->reservationStatus.id",'left')
		->join("$this->depositType","$this->tablename.deposit_type_id = $this->depositType.id",'left')
		->get($this->tablename)->row_array();
	}
}
?>