	<header id="header">
        <div id="cartsucces" style=" background:#e00000;color: #fff;position:fixed; top:0px;font-size: 20px; height: 50px; padding-top: 10px; text-align: center;width: 100%;z-index: 5;display:none;"></div>
			<div class="header-top-bar">
				<div class="container">
					<div class="row">
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="header-login">
								<a href="#">Order online</a>
								<?php if($this->session->userdata('userid') !=''){?>
								<a href="auth/logout">Logout</a>
								<?php } else {?>
								<a href="auth/index?uri=<?php echo $module?>/<?php echo $method?>">Login</a>
								<?php }?>
							</div>
							<!-- end .header-login -->
							<!-- Header Social -->
							<ul class="header-social">
								<?php
								if(is_array($openClose_info)){
									$currentTime=date("h:i A");
								$now =strtotime($currentTime);
								$open= $openClose_info['opening'];	
								$close= $openClose_info['closing'];	
								//$ap1 = substr(trim($open), -2);  
								
								//$ap2 = substr(trim($close), -2);
								$start=strtotime($open); 
								$last=strtotime($close);  
								
									if($start>$last)
										$last=$last+$last*60*60*12;

								}
								
								
						
							    function makeExternalLink($link){
							    	return "http://".$link;

							    } 
							    ?>
							    <?php if(!empty($social_info['fb_url'])){ ?>
                                 <li><a href="<?php echo makeExternalLink($social_info['fb_url']); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>
								</li>
							   <?php } 
								if(!empty($social_info['twit_url'])){ ?>
                                 <li><a href="<?php echo makeExternalLink($social_info['twit_url']); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
								<?php }
							if(!empty($social_info['google_url'])){ ?>						
								<li><a href="<?php echo makeExternalLink($social_info['google_url']); ?>" target="_blank"><i class="fa fa-google-plus-square"></i></a>
								</li>
								<?php } 
								if(!empty($social_info['linkedin_url'])){ ?>	
								<li><a href="<?php echo makeExternalLink($social_info['linkedin_url']); ?>" target="_blank"><i class="fa fa-linkedin-square"></i></a>
								</li>
								<?php } 
								if(!empty($social_info['pinterest_url'])){ ?>	
								<li><a href="<?php echo makeExternalLink($social_info['pinterest_url']); ?>" target="_blank"><i class="fa fa-pinterest-square"></i></a>
								</li>
								<?php } ?>
							</ul>
						</div>
						<div class="col-md-7 col-sm-12 col-xs-12">
							<p class="call-us">

								<?php if(isset($businessPhone) || isset($businessPhone)){?>Call Us: <a class="font" href="#"><?php echo ((isset($businessPhone))? $businessPhone:'');if(isset($businessPhone) && isset($businessLandPhone)) echo ', &nbsp;'; echo ((isset($businessLandPhone))? $businessLandPhone:'')?></a><?php } ?>
									<?php 
									if(is_array($openClose_info)){
										if($now<=$last){?>

										<span class="open-now"><i class="fa fa-check-square"></i>We are open now(<?php echo $openClose_info['opening'].'-'.$openClose_info['closing'];?>)</span>
										<?php }
										else{
											?>
											<span class="close-now"><i class="fa fa-square"></i>We are close now(<?php echo $openClose_info['closing'].'-'.$openClose_info['opening'];?>)</span>
											<?php } 

										}?>
                                    
										</p>
								</div>
					</div>
					<!-- end .container -->
				</div>
			</div>
			<!-- end .header-top-bar -->

			<div class="header-nav-bar">
				<nav class="navbar navbar-default" role="navigation">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="">
								<img src="<?=str_replace('weborder/../','',base_url().$logo_path)?>" alt="logo" >
							</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<li <?=(($tab == 'menu')? 'class="active"':'')?>><a href="order">Menu</a></li>
								<?php if(count($cmspages) > 0){
									foreach ($cmspages as $page){
									?>
									<li <?=(($tab == $page['id'])? 'class="active"':'')?>><a href="page/content/<?=$page['id']?>"><?=$page['name']?></a></li>
								<?php } } ?>
								
								<?php if(isset($reservation) && $reservation !=0){
								?>
								<li <?=(($tab == 'reservation')? 'class="active"':'')?>><a href="reservation">Reservation</a></li>
								<?php 
								} ?>
								
							</ul>
						</div>
						<!-- /.navbar-collapse -->
					</div>
					<!-- /.container-fluid -->
				</nav>
			</div>
			<!-- end .header-nav-bar -->
            <?php if(in_array($module, array('order','page','', 'reservation'))){ ?>
			<div class="small-menu">
				<div class="container">
					<?php if(in_array($module, array('order',''))){ ?>
					<div class="choose-option">
					    <?php if(in_array($method, array('index',''))){
					        $active='class="active"';
					    }else{
					        $active= '';
					    }?>
						<ul class="list-unstyled list-inline">
							<li <?=$active?>><a href="order">1. Choose</a></li>
							<li <?php if($method =='confirm') echo 'class="active"'?>><a  href="order/confirm">2. Confirm</a></li>
							<li  <?php if($method =='checkout') echo 'class="active"'?>><a href="order/checkout">3. Checkout</a></li>
						</ul>
					</div>
					<?php } ?>
					<!-- end .choose-option-->
					<ul class="list-unstyled list-inline">
						
						<?php if(count($breadcrumb) > 0 && $breadcrumb !='') {
							$l = count($breadcrumb);
							$i = 1;
						    foreach($breadcrumb as $k=>$v){
							if($l==$i){?>
							<li><a href="<?=$v?>"><?=$k?></a></li>
						<?php } else {?>
							
							<li><a href="<?=$v?>"><?=$k?></a></li>
							<li><i class="fa fa-chevron-right"></i></li>
						<?php }
							$i += 1;
							} 
						}?>
					</ul>
				</div>
				<!-- end .container-->
			</div>
			<!--end .small-menu -->
			<?php } ?>
		</header>
		<!-- end #header -->