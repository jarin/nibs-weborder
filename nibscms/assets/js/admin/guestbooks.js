$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addPageBtn").overlay({width:"30%",inline:true,title:"Add Entry",href:"#addPageForm"});

  //Delete Buttons
	$("#main-content button.delete").live("click",function(){
		if(confirm("Are you sure?")){
			NIBS.util.ajax({id:$(this).attr("elId")},"admin/"+$(this).attr("rel"));
		}
		return false;
	});

  //checkboxes
  $("#sidebar ul.comp_multilineListView li input[type=checkbox]").live("click",function(){
    activate_Entry($(this));
    return false;
  });
  
  //radiobutton
  $("#sidebar ul.comp_multilineListView li span input[type=radio]").live("click",function(){
    activate_radio_Entry($(this));
    return false;
  });

  // See More details on click
  $("#sidebar ul.comp_multilineListView li").live("click",function(){
    var _li=$(this);
    _li.siblings().removeClass("active");
    _li.addClass("active");
    loadEntryDetails(_li.attr("id"));
    return false;
  });



});

function loadEntryDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
  var id=e[2];

  $("#dContent").load(base_url+"admin/guestbooks/viewEntry/"+id);

}

function deleteEntry(guestbook_id){
  var el=$("#guestbook_"+guestbook_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
  }
  el.addClass("deleted");
  setTimeout(function(){
    el.remove()
  },500);
  NIBS.notif.notify("success","Entry deleted successfully");
}

function activate_Entry(el){
  if(el){
    var e=el.attr("id").match(/(.+)[-=_](.+)/);
    var id=e[2];
  }
  NIBS.util.ajax({id:id},"admin/guestbooks/activateDeactivateEntry");
}

function activate_radio_Entry(el){
  if(el){
    var e=el.attr("id").match(/(.+)[-=_](.+)/);
    var id=e[2];
  }
  NIBS.util.ajax({id:id},"admin/guestbooks/activateDeactivateRadioEntry");
}

function do_activate_Entry(id,val){
  $("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
  $("#main-content").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

function do_activate_radio_Entry(id,val){
  $("#showAll_"+id).prop("checked",val=="1"?true:false);
  $("#main-content").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}




