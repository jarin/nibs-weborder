<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/settings.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/settings.js"></script>

<script type="text/tmpl" id="defaultContent" charset="utf-8">
  <h3 class="page-title"><?=$pagename?></h3>
  <div class="initPage">
    <div>
      <p>Click on the Setting Groups on the left</p>
      <p>to edit settings in that group...</p>
    </div>
  </div>
</script>



<div id="main-content" class=''>
  <div class="container-fluid">
    <div id="page" class='site-settings'>      
        <div class="row-fluid" id="dContent">
    </div>
    </div>
  </div>
</div>


<div id="sidebar" class="nav-collapse collapse">
  <div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
  <div id="sidebarHeading">
    <div class="headertoolbar" style='padding-left:15px'>
      <span><i class='icon-cog'></i>
        <em>Setting Groups</em>
      </span>
    </div>
  </div> 

  <ul class="sidebar-menu shortlist">
    <?=$list1?>
  </ul>
</div>

