$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addBusinessBtn").overlay({width:"30%",inline:true,title:"Add New Delivery charge",href:"#addForm"});
  $("#sidebar ul.list li input[type=checkbox]").live("click",function(){
  	activate_charge($(this));
  	return false;
  });

  // See More details on click
  $("#sidebar ul.list li").live("click",function(){
  	var _li=$(this);
  	_li.siblings().removeClass("active");
  	_li.addClass("active");
  	loadDetails(_li.attr("id"));
  	return false;
  });

	//checkbox states
	$("#thirdBar input[type=checkbox]:not(.main_cb)").live("click",function(){
		determineMainCBState(this);
	});
});

function replaceImage(id){
	$("#"+id).find("img").attr("src","");
}

function addDelivery(name,id){
	NIBS.notif.notify("success",name+" has been added as a new business");
	
	
	var html="<li id='delivery_"+id+"'>"+
	"<a href='#'>"+
	"<span class='name icon-box'><i class=''><img src='"+base_url+"assets/images/admin/application_list.png' border='0'/></i></span>"+
	"<span class='name nameBtn' title='"+name+"'>"+name+"</span>"+
	"<span class='list_icon_btn'>"+
	"<input title='Delete Page' type='image' class='action delete' rel='deliverycharge/delete' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
	"<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"+
	"</span></a></li>";

	$("#sidebar ul.list").append(html);
	$("#add_form").reset();
	setTimeout(function(){
		$("#delivery_"+id).removeClass("deleted");
	},100);
}

function loadDetails(id){
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#dContent").load(base_url+"admin/deliverycharge/loadDetails/"+id);

	$("#thirdBar").html("<div style='text-align:center;margin-top:40px'><button class='button edit secondary' onclick='loadPermissions(\"business_"+id+"\");return false;'>Manage Permissions</button></div>");
}

function deleteCharge(id){
	var el=$("#delivery_"+id);
	if(el.is(".active")){
		$("#dContent").html($("#defaultContent").html());
		
	}
	el.addClass("deleted");
	setTimeout(function(){
		el.remove()
	},500);
	NIBS.notif.notify("success",el.find("span").text()+" removed successfully");
}

function activate_charge(el){
	if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({id:id},"admin/deliverycharge/activateCharge");
}
function do_activate_page(id,val){
	$("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
	$("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

