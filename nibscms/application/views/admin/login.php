<?php $baseUrl=base_url();?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8">
</html>
<![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9">
</html>
<![endif]-->
<!--[if !IE]>
<!-->
<html lang="en">
  <!--
<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?=$baseUrl?>">
    <title>Control Panel</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?=$baseUrl?>assets/third_party/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>assets/third_party/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>assets/css/admin/style.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>assets/css/admin/style_responsive.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>assets/css/admin/style_default.css" rel="stylesheet" id="style_color" />
    <script src="<?=$baseUrl?>assets/js/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="<?=$baseUrl?>assets/third_party/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=$baseUrl?>assets/js/application.js" type="text/javascript"></script>
  </head>


  <body id="login-body">
    <div class="login-header">
      <div id="logo" class="center">
        <img src="<?=$baseUrl?>assets/images/admin/nibs_logo.png" alt="Nibs Solutions" class="center" /><div id="login_moniker">Control Panel</div>
      </div>
      
    </div>
    <div id="login">
    	<?php
    		if(isset($msg) && trim($msg)!=""){
    			echo "<span style='color:red;background-color:#f2dfe0;display:block;text-align:center;padding:5px;font-weight:bold'>$msg</span>";
    			echo "<script type='text/javascript'>$(function(){\$(\"#loginBox\").addClass(\"shake\")})</script>";
    		}
            ?>
      <form id="loginform" class="form-vertical no-padding no-margin" action="<?=$baseUrl?>admin/authentication/login" method="post" autocomplete="off" >
      <div class="lock">
        <i class="icon-lock">
        </i>
      </div>
      <div class="control-wrap">
        <h4>Login</h4>
        <div class="control-group" id='loginBox'>
          <div class="controls">
            <div class="input-prepend">
              <span class="add-on">
                <i class="icon-user">
                </i>
              </span>
              <input id="input-username" type="text" placeholder="Username" name="username" />
            </div>
          </div>
        </div>
        <div class="control-group">
          <div class="controls">
            <div class="input-prepend">
              <span class="add-on">
                <i class="icon-key">
                </i>
              </span>
              <input id="input-password" type="password" placeholder="Password" name="password" />
            </div>
            <div class="clearfix space5">
            </div>
          </div>
        </div>
      </div>
      <input type="submit" id="login-btn" class="btn btn-block login-btn" value="Login" />
      
      <input type="hidden" name="uri" value="">
    </form>
</div>
<div id="login-copyright">
  <?=date("Y")?> &copy; Nibs Solutions.
</div>


	  <br /><br />
  	<table cellpadding="3" cellspacing="2" align="center" id="browserNotGood" style="display:none">
    	<tr>
    		<td colspan="3" align="center"><b>Please Note:</b> This control panel only works reliably<br /> in one of the following A-Grade Browsers</td>
    	</tr>
    	<tr>
    		<td style="border:1px solid #e3e3e3" width="100" align="center"><a target="_blank" href="http://www.getfirefox.com"><img width="90" src="<?=$baseUrl?>assets/images/admin/firefox.png" border="0" /></a></td>
    		<td style="border:1px solid #e3e3e3" width="100" align="center"><a target="_blank" href="http://www.apple.com/safari/download"><img width="90" src="<?=$baseUrl?>assets/images/admin/safari.png" border="0" /></a></td>
    		<td style="border:1px solid #e3e3e3" width="100" align="center"><a target="_blank" href="http://www.google.com/chrome"><img width="90" src="<?=$baseUrl?>assets/images/admin/chrome.png" border="0" /></a></td>
    	</tr>
    	<tr>
    		<td style="border:1px solid #e3e3e3;font-weight:bold" width="100" align="center">Firefox</td>
    		<td style="border:1px solid #e3e3e3;font-weight:bold" width="100" align="center">Safari</td>
    		<td style="border:1px solid #e3e3e3;font-weight:bold" width="100" align="center">Chrome</td>
    	</tr>
    	<tr>
    		<td colspan="3" align="center">Click on the logos above to download any of them...</td>
    	</tr>
    </table>
	  
	</div>
</div>


<script type="text/javascript">

	$(document).ready(function(){if($.browser.msie){$("#browserNotGood").show();}});
  
</script>
        
    </body>
</html>    
        
        
