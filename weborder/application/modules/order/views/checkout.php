       <div id="page-content">
       
		 <?php $this->load->view('top_content');?>

			<!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-9 col-sm-12 col-md-push-0">
    				     
				                    <?php if($this->session->userdata('cart') !=''){
                                        ?>
                                        <div class="my-check-right" style="border:0px;">
                                            <h5 class="">My Check</h5>
                                            <div class=" ">
                                        <ul class="list-unstyled">
                                            <li>
                                                <p>
                                                <span>Product Name</span>
                                                <span class="checkoutQty">Quantity</span>
                                                <span class="checkoutPrice">Price</span>
                                                <span class="checkoutTotal">Sub-Total</span>
                                                </p>
                                            </li>
                                            <?php 
                                            $total = 0;
                                            foreach ($this->session->userdata('cart') as $key => $cartItem){
                                            $total = $total + $cartItem['total_price'];
                                             ?>
											<li>
												<p>
												<span><?=($key+1).'. '?></span>
												<?php echo $cartItem['product_name']?>
												<span class="checkoutQty"><?php echo $cartItem['quantity']?>x</span><span class="price checkoutPrice">&pound; <?php echo $cartItem['price']?></span>
												
												<span class="price checkoutTotal">&pound; <?php echo $cartItem['total_price']?></span>
												</p>
											</li>
											<?php } ?>
											<li>
												<!-- list for total price-->
												<p>Sub Total</p>
												<p class="price-total">&pound; <?php echo $total?></p>
											</li>
											<?php if($discount > 0) {?>
											<li>
												<!-- list for total price-->
												<p>Discount</p>
												<p class="price-total">(-)&pound; <?php echo $discount?></p>
											</li>
											<?php }?>
											<li id="deliveryCharge" style="display:none">
											</li>
											<li>
												<!-- list for total price-->
												<p>Total</p>
												<p class="price-total">&pound; <span id="cartTotal"><?php echo $total - $discount?></span></p>
											</li>
										</ul>
								    </div>
                             </div>
                          
                            <form id="confirmorder" method="POST" action="order/confirmorder">
                           		<input type="hidden" name="customer_id" value="<?=$this->session->userdata("userid")?>"/>
                              <div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Order Type</h4>
											<span class="radio-input">
												<input type="radio" id="collection" value="collection" name="type" checked>
												<label for="collection">Collection</label>
											</span>
											<br/>
											<span class="radio-input">
												<input type="radio" id="delivery" value="delivery" name="type" >
												<label for="delivery">Delivery</label>
											</span>
											<p id="minimumvalue" style="color:#E00000"></p>
									    </div>
									</div>
							</div>
							
							<div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Payment Type</h4>
                                         	<span class="radio-input">
												<input type="radio" id="paypal" value="paypal" name="payment" checked>
												<label for="paypal"><i class="fa fa-cc-paypal"></i> Paypal</label>
											</span>
											<br/>
											<span class="radio-input">
												<input type="radio" id="paycollection" value="payonCollection" name="payment" >
												<label for="paycollection"> Pay On Collection</label>
											</span>
											
											<span class="radio-input">
												<input type="radio" id="paydelivery" value="payonDelivery" name="payment" >
												<label for="paydelivery"> Pay On Delivery</label>
											</span>
											
											
									    </div>
									</div>
							</div>
														
                             <div class="page-content">
                                 <div class="contact-us">
                                     <div class="send-message" style="border:0px;">
                                        <h4>Customer Info</h4>
                                       
                                          <div class="row">
                                            <?php if(count($user) > 0){
                                                
                                                ?>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Name*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Name*" name="name" value="<?=$user['customerName']?>" data-rule-required="true"  data-msg-required="Please enter your name">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>House No*:</label>
                                            </div> 
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House No*" name="house_no" value="<?=$user['houseNo']?>" data-rule-required="true"  data-msg-required="Please enter your house no">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>House Name:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House Name" name="house_name" value="<?=$user['houseName']?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Street*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Street*" name="street" value="<?=$user['street']?>" data-rule-required="true"  data-msg-required="Please enter your street">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Town:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Town" name="town" value="<?=$user['town']['cityName']?>" >
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Post Code*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Post Code*" name="post_code" value="<?=$user['postCode']?>" data-rule-required="true"  data-msg-required="Please enter your post code">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country / State:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                             <input type="text" placeholder="Country / State" name="state" value="<?=$user['state']['stateName']?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                             <input type="text" placeholder="Country*" name="country_default" value="<?=$user['country']['countryName']?>" data-rule-required="true"  data-msg-required="Please enter your country">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Phone*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Phone*" name="phone" value="<?=$user['mobile']?>" data-rule-required="true"  data-msg-required="Please enter your phone number">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Email*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6" style="padding-right:0px;">
                                              <input type="email" placeholder="Email*" name="email" value="<?=$user['email']?>" data-rule-required="true" data-rule-email="true" data-msg-required="Please enter your email address" data-msg-email="Please enter a valid email address">
                                            </div>
                                            <?php } ?>
                                          
                                          <!-- end nasted .row -->
                                          <!--  <div class="col-md-6 col-sm-6">
                                                <label>Your message:</label>
                                            </div>
                                        <textarea placeholder="Your message" name="message"></textarea>-->
                                        </div>
                                          
                                        
                                      </div>
                                       <!-- end .send-message -->
                                  </div>
                              </div>
                              
                             <div class="page-content" id="deliveryaddress">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Alternative Delivery address</h4>
                                         	<div class="row">
                                         		
	                                        	<div class="col-md-6 col-sm-6">
	                                                <label>House No*:</label>
	                                            </div> 
	                                            <div class="col-md-6 col-sm-6">
	                                              <input type="text" placeholder="House No*" name="delivery_house_no" value="">
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>House Name:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                              <input type="text" placeholder="House Name" name="delivery_house_name" value="">
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Street*:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                              <input type="text" placeholder="Street*" name="delivery_street" value="">
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Town:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                              <input type="text" placeholder="Town" name="delivery_town" value="" >
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Post Code*:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                              <input type="text" placeholder="Post Code*" name="delivery_post_code" value="">
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Country / State:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                             <input type="text" placeholder="Country / State" name="delivery_state" value="">
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Country*:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                             <input type="text" placeholder="Country*" name="delivery_country_default" value="" >
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Phone:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                              <input type="text" placeholder="Phone" name="delivery_phone" value="">
	                                            </div>
	                                            
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Land Line:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                              <input type="text" placeholder="Land Line" name="delivery_landphone" value="">
	                                            </div>
	                                        	<div class="col-md-6 col-sm-6">
		                                        	<span class="checkbox-input">
														<input type="checkbox" id="alter_address" value="1" name="alter_address" >
														<label for="alter_address"> Use this as delivery address</label>
													</span>
												</div>
                                         	</div>
									  </div>
								</div>
							</div>
              
							<a class="btn btn-default-black " href="order/confirm" style="padding: 7px 15px;">Back</a>
							<?php
							
							 if(count($ordervalue)>0){
								echo '<input type="hidden" name="collectionminimum" value='.$ordervalue['order_value_collection'].' />
									 <input type="hidden" name="deliveryminimum" value='.$ordervalue['order_value_delivery'].' />';
								
				                }
				             ?>
				             
                    	    <button class="btn btn-default-red" type="submit"  style="float:right;margin-top:15px"><i class="fa fa-money"></i> Confirm</button>
							
						</form>
						
                        <script type="text/javascript">
                            
                        	
                        	 $(document).ready(function() {
                        		 $( '.checkbox-input' ).each(function(){
                             		$(this).uouCheckboxInput();
                             	});
                             	$( '.radio-input' ).each(function(){
                             		$(this).uouRadioInput();
                             	});
                        		 $("input[name='type']").change(function(){
                                  	var _this=$(this),
                                  		orderType = _this.val(),
                                  		mincollection = parseFloat($("input[name='collectionminimum']").val()),
                                  		mindelivery = parseFloat($("input[name='deliveryminimum']").val()),
                                  		total = parseFloat("<?php echo $total?>"),
                                  		row = $("#minimumvalue");
                                  		
                                  		$('#paypal').parent().addClass('active');
     	                             	if(orderType == 'collection'){
     	                             		$('#deliveryCharge').hide(0);
     	                             		$('#cartTotal').html("<?php echo $total - $discount?>");
     	                             		$('#deliveryaddress').hide(0); 
     	                             		$('#cartsucces').hide(0);
     	                             		$('.btn-default-red').removeClass('disabled');
     	                             		$("#paydelivery").parents().removeClass('active');
         	                             	$('#paycollection').parent().show(0);
         	                             	$('#paydelivery').parent().hide(0);
  	                             			if(mincollection != 'undefined'){
  	                             				if(mincollection > total){
  	     	                         				row.html("You need to spend &pound;"+mincollection+" or more to order for collection");
  	     	                         				$('.btn-default-red').addClass('disabled');
  	     	                                 	}else {
  	     	                                 		
  	     	                                 		row.html('');
  	     	                                 	} 
  	  	                             		}
     	                         			
     	                         		} else if(orderType == 'delivery'){
         	                         		$('#deliveryaddress').show(0); 
     	                         			$("#paycollection").parents().removeClass('active');
     	                         			$('#paydelivery').parent().show(0);
     	                         			$('#paycollection').parent().hide(0);
         	                         		if(mindelivery != 'undefined'){
         	                         			if(mindelivery > total){ 
         	                         				$('.btn-default-red').addClass('disabled');
         	                         				row.html("You need to spend &pound;"+mindelivery+" or more to order for delivery");
         	                                 	} else {
         	                                 		$('.btn-default-red').removeClass('disabled');
         	                                 		row.html('');
         	                                 	}
             	                         	}
         	                         		$.ajax({
         	                           		async: false, 
         	                           		type: "POST",
         	                           		dataType: "json",
         	                           		url: "order/checkDistance",         	                           		
         	                           		success:function(data){
             	                           		if(data.success == true){             	                           		
	                 	                           		$('#deliveryCharge').html("<p>Delivery Charge</p> <p class='price-total'>&pound;"+data.value+ "</p>");
	                 	                           		$('#cartTotal').html(parseFloat("<?php echo $total - $discount?>")+parseFloat(data.value));
	                 	                           		$('#deliveryCharge').show(0);
                 	                           		} else if(data.error == true) {
                     	                           		if(data.value == 00){
                     	                           			$('.btn-default-red').addClass('disabled');
                     	                           		 	$('#cartsucces').html('We do not deliver at this location');
                     	                           			$('#cartsucces').show(0);
                         	                           	}                    	                           		
                 	                           		}
             	                           		}
         	                         		});
     	                         		}
                                  	});
                        		  
                        		  $("input[name='type']:first").prop("checked", true).trigger("change");

                        		  $("input[name='alter_address']").change(function(){
                                    	var _this=$(this);
                                    	 if(_this.is(':checked')) {
											
											 $("input[name='delivery_house_no']").attr('data-rule-required',"true").attr('data-msg-required',"Please enter your house no");
											 $("input[name='delivery_street']").attr('data-rule-required',"true").attr('data-msg-required',"Please enter your street");
											 $("input[name='delivery_post_code']").attr('data-rule-required',"true").attr('data-msg-required',"Please enter your post code");
											 $("input[name='delivery_country_default']").attr('data-rule-required',"true").attr('data-msg-required',"Please enter your country");
                                        } else {
                                        	//$("input[name='delivery_house_no']").removeAttr('data-rule-required').removeAttr('data-msg-required').removeAttr('aria-required').removeAttr('aria-invalid');
                                        	//$("input[name='delivery_house_no']").removeClass('error').closest('div').find("label").removeClass('error').removeAttr('id').html('');
                                        	$("input[name='delivery_street']").attr("novalidate","novalidate");
											 //$("input[name='delivery_post_code']").attr('data-rule-required',"").attr('data-msg-required',"");
											// $("input[name='delivery_country_default']").attr('data-rule-required',"").attr('data-msg-required',"");
                                            }
                        		  });
                        		  
                        		});
                        </script>
							
							<?php } else { ?>
							<p style="text-align: center;">You do not have any Items</p>
							<?php } ?>
										

                           

				     
					</div>
			            <!-- end .col-md-9 -->

					<?php $this->load->view('left_sidebar')?>
				</div>
				<!-- end .row -->
			</div>
			<!--end .container -->