<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/<#=plural($base_name)#>.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/<#=plural($base_name)#>.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
  <div class="padded"><h1><?=$pagename?></h1>
  <div class="initPage">
    <div>
      <p>Click on the list of <#=plural(ucfirst(uncamelize($single_item_name)))#> on the left to edit....</p>
      <p>To delete a <#=uncamelize($single_item_name)#> use the <img src="<?=$baseUrl?>assets/images/admin/icon_trash.png" /> delete icon...</p>
      <p>To add a <#=uncamelize($single_item_name)#>, click on the <img src="<?=$baseUrl?>assets/images/admin/icon_add.png" /> Plus button...</p>
    </div>
  </div></div>
</script>
<div id="innerTabs" style="display:none">
  <ul>
    <li class="active"><#=ucfirst(uncamelize($single_item_name))#> Details</li>
    <li><#=ucfirst(uncamelize($single_item_name))#> Content</li>
  </ul>
</div>
<div id="thirdBar" style="display:none">
  <div class='headertoolbar'><span>Page Info</span></div>
  <div class='padded' id='infoPanel'></div>
</div>
<div id="mainContent" class="">
  <div id="mainContentInner" class="">
    <div class="hidden">
      <div id="addPageForm">
       <form id='add_page_form' method='post' rel='ajaxed' action='<?=$baseUrl?>admin/<#=plural($base_name)#>/add<#=ucfirst($single_item_name)#>' loadingDiv='loader_add_page' formType="adder">
			<table cellpadding='4' cellspacing='0' align='center'>
				<tr>
					<th>Headline</th>
					<td colspan="2"><input type='text' name='headline' /></td>
				</tr>
				<tr><td colspan='3'><hr class='hr' /></td></tr>
				<td align='right' colspan='3'><button class="button add" type='submit'>Add <#=ucfirst(uncamelize($single_item_name))#></button><div id='loader_add_page'  class='q_feedback'></div></td> </tr>
			</table>
		</form>
      </div>
    </div>
    <div id="dContent">

    </div>
  </div>
</div>
<div id="sideBar">
  <div class="headertoolbar"><#=plural(ucfirst(uncamelize($single_item_name)))#> <div id="searchCount">0 Filtered Results</div> <button class="addPageBtn" title="Add <#=ucfirst(uncamelize($single_item_name))#>"><img src="<?=$baseUrl?>assets/images/admin/icon_add.png" /></button></div>
  <ul class="list<#if($reorderable){#> reorderable<#}#>"<#if($reorderable){#> rel="<#=plural($base_name)#>/reOrder<#=ucfirst($base_name)#>Items"<#}#>>
    <?=$list1?>
  </ul>
  <div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>
<#if($tinymce){#><script type="text/javascript" src="<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce/tiny_mce_gzip.js"></script>
<script type="text/javascript">
tinyMCE_GZ.init({
	plugins : 'safari,style,table,save,advimage,advlink,contextmenu,paste,directionality,visualchars,nonbreaking,xhtmlxtras,inlinepopups,fullscreen,spellchecker',
	themes : 'advanced',
	languages : 'en',
	spellchecker_languages : "+English=en",
	disk_cache : true,
	debug : false,
	baseURL : "<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce"
});

</script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script><#}#>
<script type="text/javascript">
	var _KA=window.setInterval(function(){
		$.ajax({
			url:base_url+"admin/pages/XkeepAlive",
			dataType:"json",
			type:"post",
			data:{"keepAlive":"true"}
		})
	},180000);
<#if($tinymce && $autosave){#>
	var changed_count=0;
  function MCE_Changed(){
    var autosaveUrl="<?=(isset($autosaveUrl) ? $autosaveUrl : "")?>"||"",threshold=3;
    changed_count++;
    if(changed_count>=threshold && $.trim(autosaveUrl)!=""){
      changed_count=0;
      var EE=tinyMCE.get("EditableArea"),
      content=EE.getContent(),
      <#=$base_name#>_id=$("#EditableArea").parent().find("input[name=<#=$base_name#>_id]").val();

      if(<#=$base_name#>_id > 0){
        NIBS.util.ajax({<#=$base_name#>_id:<#=$base_name#>_id,content:content},autosaveUrl,null,null);
      }
    }
  }
<#}#>
</script>
