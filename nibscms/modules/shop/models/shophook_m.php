<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shophook_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->tablename="";
	}


	function save($primaryKey="id",$id,$arr){
		return $this->db->where($primaryKey,$id)->update($this->tablename,$arr);
	}

	function get($primaryKey="id",$id){
		if(trim($id)==""){
			return array();
		}
		return $this->db->where($primaryKey,$id)
		->get($this->tablename)->row_array();
	}

	function getAll(){
	  return $this->db
		->get($this->tablename)->result_array();
	}

	function getProductsByCategory($id){
	  $this->tablename="SS_products";
	  return $this->db->query("Select * from `$this->tablename` where `categoryID`='$id' order by `sort_order` ASC")->result_array();
	}

	function searchProducts($term,$fields=array(),$extraFilter=""){
		$this->tablename="SS_products";
		$results=array();
		if(count($fields)>0){
			$query="";
			foreach ($fields as $field) {
				$queryArr[]=" `a`.`$field` LIKE '%".mysql_real_escape_string($term)."%' ";
			}
			$query=implode(" or ",$queryArr);
			$results=$this->db->query("Select a.*,b.filename,b.thumbnail from `$this->tablename` a left join `SS_product_pictures` b on a.default_picture=b.photoID where ($query) $extraFilter")->result_array();
		}

		//reset tablename to empty string
		$this->tablename="";
		return $results;


	}

	function searchAttachments($term,$fields=array(),$extraFilter=""){
		$this->tablename="SS_product_attachments";
		$results=array();
		if(count($fields)>0){
			$query="";
			foreach ($fields as $field) {
				$queryArr[]=" $field LIKE '%".mysql_real_escape_string($term)."%' ";
			}
			$query=implode(" or ",$queryArr);
			$results=$this->db->query("Select a.name as filename,a.path as filepath,a.*,b.productID,b.brief_description,b.stub,b.product_code from `$this->tablename` a left join `SS_products` b on a.productID=b.productID where ($query) $extraFilter")->result_array();
		}

		//reset tablename to empty string
		$this->tablename="";
		return $results;
	}

	function getCustomerId($username){
		$this->tablename="SS_customers";
		$data=$this->db->where("Login",$username)->get($this->tablename)->row_array();
		//reset tablename to empty string
		$this->tablename="";
		if(isset($data["customerID"])){
			return $data["customerID"];
		}else{
			return NULL;
		}
	}

	function getAllActiveProducts(){
		$data=array();
		$this->tablename="SS_products";
		$data=$this->db->query("select `name`,`product_code`,`stub` from `$this->tablename` where `enabled`='1'")->result_array();
		//reset tablename to empty string
		$this->tablename="";
		return $data;
	}

	function verifyProductCode($code=""){
		$ret=false;
		$this->tablename="SS_products";
		$code=strtolower($code);
		if(trim($code)!=""){
			$dt=$this->db->query("select `productID`,`product_code`,`Price` from `$this->tablename` where `product_code`='$code'")->row_array();
			if(is_array($dt)){
				if(isset($dt["product_code"]) && strtolower($dt["product_code"])==$code ){
					if($dt["Price"]>0){
						$ret=$dt["productID"];
					}else{
						$ret="POA";
					}
				}
			}
		}

		return $ret;
	}

	//=======================================
	function getOrders($status_id="ALL"){
	  $this->tablename="SS_orders";
	  if($status_id=="ALL"){
	    return $this->getAll("order_time");
	  }else{
	    $res=$this->db
	    ->select("$this->tablename.orderID,
	    $this->tablename.customer_firstname,
	    $this->tablename.customer_lastname,
	    Unix_Timestamp($this->tablename.order_time) as order_time,
	    $this->tablename.order_amount,
	    $this->tablename.currency_code,
	    $this->tablename.statusID,
	    SS_order_status.status_name")
	    ->join("SS_order_status","SS_order_status.statusID = $this->tablename.statusID");
	    if(is_array($status_id)){
	      foreach ($status_id as $i) {
	       $res=$res->or_where("$this->tablename.statusID",$i);
	      }
	    }else{
	      $res=$res->where("$this->tablename.statusID",$status_id);
	    }
      return $res->order_by("$this->tablename.order_time","ASC")
      ->get($this->tablename)->result_array();
	  }
	}

	function searchOrders($q){
    $this->tablename="SS_orders";
    return $this->db->select("$this->tablename.orderID,
    $this->tablename.customer_firstname,
    $this->tablename.customer_lastname,
    Unix_Timestamp($this->tablename.order_time) as order_time,
    $this->tablename.order_amount,
    $this->tablename.currency_code,
    $this->tablename.statusID,
    SS_order_status.status_name")
    ->join("SS_order_status","SS_order_status.statusID = $this->tablename.statusID")
    ->where("$this->tablename.orderID",$q)
    ->get($this->tablename)->result_array();
	}

	function getOrder($id){
	  $this->tablename="SS_orders";
	  $res=$this->db
	  ->select("$this->tablename.*,Unix_Timestamp($this->tablename.order_time) as ordered_on,SS_order_status.status_name")
	  ->join("SS_order_status","SS_order_status.statusID = $this->tablename.statusID","left")
	  ->where("$this->tablename.orderID",$id)
	  ->get($this->tablename)->row_array();
	  if(is_array($res) && isset($res["orderID"])){
	    $this->tablename="SS_ordered_carts";
	    $totalTax=0;$clear_total_price=0;$discount_amount=0;
	    $orderContents=$this->db->where("orderID",$res["orderID"])->get($this->tablename)->result_array();
	    foreach ($orderContents as $item) {
	     $totalTax+=($item["tax"]/100)*($item["Price"]*$item["Quantity"]);
	     $clear_total_price += $item["Price"]*$item["Quantity"];
	    }
	    if($res["order_discount"]>0){
  			$totalTax=$totalTax-(($res["order_discount"]/100)*$totalTax);
  			$discount_amount=($clear_total_price/100)*$res["order_discount"];
  		}

  		$this->tablename="SS_order_status_changelog";
  		$orderHistory=$this->db
  		->select("orderID, status_name, status_comment, Unix_Timestamp($this->tablename.status_change_time) as status_change_time")
  		->where("orderID",$res["orderID"])->get($this->tablename)->result_array();

	    $res["orderHistory"]=$orderHistory;
	    $res["orderContent"]=$orderContents;
	    $res["totalTax"]=sprintf("%01.2f", $totalTax);
	    $res["clear_total_price"]=sprintf("%01.2f", $clear_total_price);
	    $res["discount_amount"]=sprintf("%01.2f", $discount_amount);
	    $res["order_amount"]=sprintf("%01.2f", $res["order_amount"]);
	    $res["shipping_cost"]=sprintf("%01.2f", $res["shipping_cost"]);
	  }
	  return $res;
	}

	function updateOrderStatus($orderID,$statusID,$status,$status_comment,$time){
	  $this->tablename="SS_orders";
	  $res=$this->db->where("orderID",$orderID)->update($this->tablename,array(
	   "statusID"=>$statusID
	  ));
	  if($res){
	    $this->tablename="SS_order_status_changelog";
  	  $res=$this->db->insert($this->tablename,array(
  	    "orderID"=>$orderID,
  	    "status_name"=>$status,
  	    "status_comment"=>$status_comment,
  	    "status_change_time"=>$time
  	  ));
  	  return $res;
	  }else{
	    return false;
	  }

	}

  //=======================================
  function getSettingGroups($id=0){
    $this->tablename="SS_settings_groups";
    if($id==0){
      return $this->getAll("sort_order");
    }
    return $this->db->where("settings_groupID",$id)->get($this->tablename)->row_array();
  }

  function getSettings($id){
    $this->tablename="SS_settings";
    return $this->db
    ->where("settings_groupID",$id)
    ->order_by("sort_order","ASC")
    ->get($this->tablename)->result_array();
  }

  function getCountries(){
    $this->tablename="SS_countries";
    return $this->db->get($this->tablename)->result_array();
  }

  function getCurrencies(){
    $this->tablename="SS_currency_types";
    return $this->db->get($this->tablename)->result_array();
  }

  function getTaxClasses(){
    $this->tablename="SS_tax_classes";
    return $this->db->get($this->tablename)->result_array();
  }

  function getOrderStatuses(){
    $this->tablename="SS_order_status";
    return $this->db->get($this->tablename)->result_array();
  }


	//=======================================

	function homepageCategories(){
		$this->tablename="SS_homepage_categories";
		$data=$this->db->query("select * from `$this->tablename` order by `sort_order`")->result_array();
		//reset tablename to empty string
		$this->tablename="";
		return $data;
	}

	function getAllCategories(){
		$this->tablename="SS_categories";
		$this->db->query('SET names latin1');
		$data=$this->_recursiveGetCategories(1,0);

		//reset tablename to empty string
		$this->tablename="";
		return $data;
	}

	function _recursiveGetCategories($parent,$level){
		$result=array();
		$cats=$this->db->query("SELECT categoryID, name, products_count, ".
				"products_count_admin, stub, parent FROM ".
				$this->tablename.
				" WHERE parent ='$parent' ORDER BY sort_order, name")->result_array();
		if(is_array($cats)){
			foreach ($cats as $row) {
				$row["level"]=$level;
				$count=$this->db->query("select count(categoryID) as count from ".$this->tablename.
						" where categoryID<>0 AND parent='".$row["categoryID"]."'")->row_array();
				$count=$count["count"];
				$row["ExistSubCategories"] = ( $count != 0 );
				$result[$row['categoryID']] = $row;

				//process subcategories
				$subcategories = $this->_recursiveGetCategories( $row["categoryID"], $level+1);
				foreach ($subcategories as $_sub){

					$result[$_sub['categoryID']] = $_sub;
				}
			}
		}
		return $result;
	}

	function convertCategoriesToTree($categories,$level=0,$parent=1){
		$return = array();
		if(is_array($categories)){
			foreach($categories as $category){
				if($category["level"]==$level && $category["parent"]==$parent){
					unset($category[0],$category[1],$category[2],$category[3],$category[4],$category[5],$category["ExpandedCategory"],$category["products_count"],$category["products_count_admin"],$category["products_count_category"]);
					if($category["ExistSubCategories"]){
						$category["children"]=$this->convertCategoriesToTree($categories,($level+1),$category["categoryID"]);
					}
					$return[]=$category;

				}
			}
		}

		return $return;
	}

}
?>
