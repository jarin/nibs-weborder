<?php
$baseUrl=base_url();
?>
<link href="<?=$baseUrl?>assets/css/admin/jquery-ui/jquery.ptTimeSelect.css" rel="stylesheet" media="screen">
<script src="<?php echo $baseUrl; ?>assets/js/admin/jquery-ui/jquery.ptTimeSelect.js"></script>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/business.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/business.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
	<h3><?=$pagename?></h3>
	<div class="initPage">
		<div>
		  <p>Click on the list of businesses on the left to edit....</p>
		  <p>To delete a business use the <img src="<?=$baseUrl?>assets/images/admin/icon_trash.png" /> delete icon...</p>
		  <p>To add a business, click on the <img src="<?=$baseUrl?>assets/images/admin/icon_add.png" /> Plus button...</p>
		</div>
	</div>
</script> 

<style>
#ptTimeSelectCntr{
  z-index: 99999 !important;
}
</style>

<div id="thirdBar" style="display:none"></div>


<div id="main-content" class=''>
  	<div class="container-fluid">
	    <div id="page" class='site-settings'>
	    	<div class="hidden">
		      <div id="addBusinessForm">
		       <form id='add_business_form' method='post' rel='ajaxed' action='<?=$baseUrl?>admin/business/addBusiness' loadingDiv='loader_add_user' formType="adder">
					<table cellpadding='4' cellspacing='0' align='center'>
						<tr>
							<th align='left' >Business Name</th>
							<td ><input type='text' name='businessname' id='add_businessname_input'
							 class='stubSenders'
							stubReceiver='add_pagesurl_input'
							stubStatus='urlReturnValue'
							stubUrl='admin/business/checkUrlDuplication'
							 /></td>
						</tr>
						<tr>
							<th align='left'>Unique Url</th>
							<td ><input type='text' name='uniqueUrl' id='add_pagesurl_input'  class='stubSenders'
							
							stubStatus='urlReturnValue'
							stubUrl='admin/business/checkUrlDuplication' /></td>
							<td width="20"><span id="urlReturnValue" style="font-weight:bold;"></span></td>
						</tr>
						<tr>
							<th align='left' >Username</th>
							<td ><input type='text' name='username' id='add_username_input' /></td>
						</tr>
						<tr>
							<th align='left' >Password</th>
							<td ><input type='password' name='password' /></td>
						</tr>
						<tr>
							<th align='left' >Retype Password</th>
							<td ><input type='password' name='re_password' id='add_re_password_input' /></td>
						</tr>
						<tr>
							<th align='left' >Email</th>
							<td ><input type='text' name='email' id='add_email_input' /></td>
						</tr>
						<tr>
							<th align='left' >Phone</th>
							<td ><input type='text' name='phone' id='add_phone_input' /></td>
						</tr>
						<tr>
							<th align='left' >Land Line Number</th>
							<td ><input type='text' name='landLine' id='add_land_line_input' /></td>
						</tr>
						<tr>
							<th align='left' >Address Line 1</th>
							<td ><textarea rows="" cols="" name="address1" id='add_address1_input' ></textarea></td>
						</tr>
						<tr>
							<th align='left' >Address Line 2</th>
							<td ><textarea rows="" cols="" name="address2" id='add_address2_input' ></textarea></td>
						</tr>
						<tr>
							<th align='left' >Discount</th>
							<td ><input type='text' name='discount' id='add_discount_input' /></td>
						</tr>
						<tr>
							<th align='left' >Discount Validity Time</th>
							<td ><input type='text' name='discount_validity' id='add_discount_validity_input' class="datepicker" /></td>
						</tr>
						<tr>
							<th align='left' >Service Charge</th>
							<td ><input type='text' name='serviceCharge' id='add_serviceCharge_input' /></td>
						</tr>
						<tr>
							<th align='left' >Transaction Charge</th>
							<td ><input type='text' name='transactionCharge' id='add_transactionCharge_input' /></td>
						</tr>
						<tr >
							<th align='left' >Opening Time </th>
							<td ><input type='text'  name='opening' id='edit_opening_input'  /></td>
						</tr>
						<tr >
							<th align='left' >Closing Time </th>
							<td ><input type='text'  name='closing' id='edit_closing_input'  /></td>
						</tr>
						<tr >
							<th align='left' >Reservation</th>
							<td ><input type='checkbox'  name='reservation' value="1" /></td>
						</tr>
						<tr >
							<th align='left'>Deposit</th>
							<td ><input type='checkbox'  name='deposit' value="1" /></td>
						</tr>
						<tr >
							<th align='left'>Deposit Week Day</th>
	                     	<td >
		                     	<select name="depositweekday">
		                     		<option value="">Select</option>
			                    	<option value="1">Sunday</option>
			                    	<option value="2">Monday</option>
			                    	<option value="3">Tuesday</option>
			                    	<option value="4">Wednesday</option>
			                    	<option value="5">Thursday</option>
			                    	<option value="6">Friday</option>
			                    	<option value="7">Saturday</option>
			                    </select>
	                    	</td>
						</tr>
						<tr >
							<th align='left'>Deposit Amount</th>
	                     	<td ><input type='text'  name='deposit_amount' value="" /></td>
						</tr>	
							<tr >
							<th align='left'>Minimum Order Value for Collection</th>
	                     	<td ><input type='text'  name='order_value_collection' value="" /></td>
						</tr>
						<tr >
							<th align='left'>Minimum Order Value for Delivery</th>
	                     	<td ><input type='text'  name='order_value_delivery' value="" /></td>
						</tr>						
						<tr><td colspan='2'><hr class='hr' /></td></tr>
						<td align='right' colspan='2'><button class="button add" type='submit'>Add business</button><div id='loader_add_user'  class='q_feedback'></div></td> </tr>
					</table>
				</form>
		      </div>
	    	</div>
	    	
	      	<div class="row-fluid" id="dContent">
		 	</div>
	  	</div>
	</div>
</div>


<div id="sidebar" class="nav-collapse collapse">
	<div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
	<div id="sidebarHeading">
		<div class="headertoolbar">
			<span>
				<em>Businesses</em>
			</span>

	  		<div id="searchCount">0 Filtered Results</div> 
			<button class="addBusinessBtn" title="Add Business" style=''>
				<img src="<?=$baseUrl?>assets/images/admin/icon_add.png" style='margin-top:-6px'/>
			</button>
		</div>
  	</div> 
  	<ul class="sidebar-menu list">
   	 <?=$list1?>
  	</ul>
  	<div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>
<script type="text/javascript">
$('.datepicker').datepicker({
	dateFormat: 'yy-mm-dd',
	minDate: 0
});
</script>