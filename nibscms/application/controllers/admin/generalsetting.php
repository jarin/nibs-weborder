<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generalsetting extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");

		//controller Specific
		$this->tab="generalSetting";
		$this->load->model("Business_m","business");
		$this->load->model("Generalsetting_m","generalsetting");
		$this->generalSetting = array(
				"nibssms_api_key",
				"nibssms_username",
				"nibssms_sender_name",
				"RestaurantCountry",
				"address"
				);
		
	}
	
	function index(){
		if(_checkAuthentication($this)){			
			$business=$this->business->getRowBy('user_id',$this->_userid);
			$settings=$this->generalsetting->getRows('*',array(),array("where"=>array('restaurant_id' => $business['id'])));
			
				$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
				$this->load->view("admin/general_setting",array(
						"pagename"=>"General Setting",
						"settingsAr" => $this->generalSetting,
						"settings" => $settings
				));
				$this->load->view("admin/admin_foot");
		}
	}
	
	function editSetting(){
		$data = $this->input->post(NULL, TRUE);
		$html = "<div class='form-group'>
					<label>"._uncamelizeextended(camelize($data['settingName'],'_')).":&nbsp;</label>";
		$business=$this->business->getRowBy('user_id',$this->_userid);
		$restaurantId = $business['id'];
		$value = $this->generalsetting->getAValue("res_settings", "value", array("settings_name"=>$data['settingName'],"restaurant_id"=>$restaurantId));
		
		if($data['settingName'] == 'RestaurantCountry'){ 
			$this->generalsetting->tablename = $this->generalsetting->country;
			$smsCountries = $this->generalsetting->getAll("country_name","asc");
			
			$html .=" 
    		    
    		    <select name='setting[".$data['settingName']."]' id='value'>
						<option value='0'>None</option>";
					
							foreach($smsCountries as $c){
								$html .= '<option value="'.$c['country_name'].'" '. ((isset($value) && $value==$c['country_name']) ? 'selected="selected"' : '') .'>'.$c['country_name'].'</option>';
							}
						
				$html .="</select>
    		   ";
		} elseif($data['settingName'] == 'address'){
			$html .="
    		    <textarea name='setting[".$data['settingName']."]'>".$value."</textarea>
    		   
    		";
		} else {
			$html .="
    		    <input type='text' value='".$value."' name='setting[".$data['settingName']."]' style='width:60%'/>
    		    
    		";
		}
		$html .="<input type='hidden' value='".$restaurantId."' name='restaurant' />
    			</div>";
		
		echo $html;
	}
	
	function saveSetting(){
		$data = $this->input->post(NULL, TRUE);
		foreach ($data['setting'] as $key => $val){
			$dataAr = array(
					'settings_name' => $key,
					'value'=>$val
			);
			
			$settingid = $this->generalsetting->getAValue("res_settings", "id", array("settings_name"=>$key,"restaurant_id"=>$data['restaurant']));
			
			if($settingid){
				
				if($this->generalsetting->save($settingid,$dataAr)){
				
					echo '{"success":"success",successFunction:function(){
						
						location.reload();
						
					}}';
					exit;
				
				}else{
					echo '{"success":"error",successFunction:function(){
						alert("Could not add setting due to a database error");
						}}';
					exit;
				}
			} else {
				$dataAr['restaurant_id'] = $data['restaurant'];
				
				if($this->generalsetting->create($dataAr)){
				
					echo '{"success":"success",successFunction:function(){
						
						location.reload();
						
					}}';
					exit;
				
				}else{
					echo '{"success":"error",successFunction:function(){
						alert("Could not add setting due to a database error");
						}}';
					exit;
				}
			}
		}
		
		
	}
	
}