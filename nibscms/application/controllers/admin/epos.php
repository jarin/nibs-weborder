<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Epos extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="epos";
		$this->load->model("activity_m","activity");
		$this->load->model("Business_m","business");
		$this->load->model("epos_m","epos");
		
	}
	function index(){		
		if(_checkAuthentication($this)){
			$this->tab = 'epos';
			$business=$this->business->getBusinesByUser($this->session->userdata('uid'));			
			if(!isset($business["id"]) || !$business["id"]>0){
				echo '<span style="color:red">Error: Invalid Business</span>';
				exit;
			}
			$epos_info = $this->epos->infobyBid($business["id"]);

			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
			$this->load->view("admin/business_epos",array("epos"=>$epos_info,"business_id"=>$business["id"]));
			$this->load->view("admin/admin_foot");
		}	
	}
	function saveinfo(){
		if(_checkAuthentication($this)){
			$id=$this->input->post("epos_id");	     
			$data=array(
				"db_name" => $this->input->post("db_name"),	
				"db_host" => $this->input->post("db_host"),	
				"db_user" => $this->input->post("db_user"),	
				"db_pass" => $this->input->post("db_pass"),	
				"restaurant_id" => $this->input->post("b_id")		
				);
			$message='';
			if(!empty($id)){
				if($this->epos->save($id,$data)){
					$message='Successfully Updated';

				}else{
					$message='There is a problem in database';
				}

			}
			else{
				if($this->epos->create($data)){
					$message='Successfully Saved';

				}else{
					$message='There is a problem in database';
				}
			}
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
			$this->load->view("admin/status_template",array(
			    "pagename"=>"Epos Database Settings",
				"message"=>$message
				));
			$this->load->view("admin/admin_foot");
			
		}
	}

}


?>
