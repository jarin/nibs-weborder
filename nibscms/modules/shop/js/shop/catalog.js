$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  //override sortable
  /*$("ul.list.reorderable").sortable("option","axis",false)
  .sortable("option","helper","clone")
  .sortable("option","zIndex","2700")
  .sortable( "option", "appendTo", '#mainContent' );*/

	//checkboxes
  $("#thirdBar ul.list li input[type=checkbox]").live("click",function(){
		activate_product($(this));
		return false;
	});

	// See More details on click
  $("#thirdBar ul.list li").live("click",function(){
		var _li=$(this);
		_li.siblings().removeClass("active");
		_li.addClass("active");
		loadProductDetails(_li.attr("id"));
		return false;
	});

  //resize MCE editor as soon as it is shown
	$("#innerTabs li").live("click",function(){
	  var _this=$(this);
	  if (_this.index()==1){
	    resizeEditor();
	  }
	});


	//--orphanage
	$("#orphanage_link").live("click",function(){
	  var _this=$(this);
		var state=_this.data("menuShowing") || false;
		if(state){
			hideOrphanage();
			_this.removeClass("active").data("menuShowing",false);
		}else{
			showOrphanage();
			_this.addClass("active").data("menuShowing",true);
		}
	});

	$("#orphanage li").livequery(function(){
		$(this).draggable({
			opacity: 0.9,
			revert:"invalid",
			zIndex:"2700",
			helper:"clone"
		});
	});

	$("#treeHolder li").livequery(function(){
		$(this).droppable({
			accept:".orphan,.product",
			hoverClass:"onDrop",
			tolerance:"intersect",
			greedy:true,
			drop:function(e,ui){
			  var dropped=ui.draggable;
				var e=dropped.attr("id").match(/(.+)[-=_](.+)/);
				var dropped_id=e[2];
				var e2=$(this).attr("id").match(/(.+)[-=_](.+)/);
				var recv_id=(e2 ? e2[2] : 0);
				var recv_type="page";
				NIBS.util.ajax({type:recv_type,orphan_id:dropped_id,page_id:recv_id},"admin/content/attachOrphanToNewParent");
			}
		});
	});

  //the TREEEE
  createTree();
});




function loadProducts(id){
  var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#thirdBar ul.list").load(base_url+"admin/shop_catalog/viewProducts/"+id,function(){

	});

}

function loadProductDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#innerTabs").show(0);
	$("#mainContent").addClass("hastabs");
	tinyMCE && tinyMCE.execCommand("mceRemoveControl",false,"EditableArea");
	$("#dContent").load(base_url+"admin/shop_catalog/viewProduct/"+id,function(){
	  $(window).bind("resize",NIBS.util.debounce(resizeEditor));
	});
}


function activate_product(el){
	if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var type=e[1];
		var id=e[2];
	}
	NIBS.util.ajax({"id":id},"admin/shop_catalog/activateDeactivateProducts");

}

function do_act_deactivateProd(id,val){
  $("#checkboxprod_"+id).prop("checked",val=="1"?true:false);
}

function resizeEditor(){
  var ifr=$("#EditableArea_ifr"),
  spn=$("#EditableArea_parent");
  ifr.css({height:spn.height()-62});
}

function loadPageInfo(id){
  $("#infoPanel").load(base_url+"admin/content/viewPageInfo/"+id);
}



//--orphanage
function showOrphanage(){
  var orphanage=$("#orphanage");
  orphanage.removeClass("showing").show(0);
  setTimeout(function(){
	  orphanage.addClass("showing");
	},50);
}

function hideOrphanage(){
  var orphanage=$("#orphanage");
  orphanage.removeClass("showing");
  setTimeout(function(){
	  orphanage.hide(0);
	},300);
}


/*--------------------*/
function createTree(){
	//--------TREE
	$("#treeHolder").tree(
		{

		    selected    : false,        // FALSE or STRING or ARRAY
		    opened      : [],           // ARRAY OF INITIALLY OPENED NODES
		    languages   : [],           // ARRAY of string values (which will be used as CSS classes - so they must be valid)
		    path        : base_url+"assets/js/",        // FALSE or STRING (if false - will be autodetected)
		    cookies     : {prefix:"SHP"},        // FALSE or OBJECT (prefix, open, selected, opts - from jqCookie - expires, path, domain, secure)
		    ui      : {
		        dots        : true,     // BOOL - dots or no dots
		        rtl         : false,    // BOOL - is the tree right-to-left
		        animation   : 100,        // INT - duration of open/close animations in miliseconds
		        hover_mode  : true,     // SHOULD get_* functions chage focus or change hovered item
		        scroll_spd  : 4,
		        theme_path  : base_url+"assets/images/admin/",    // Path to themes
		        theme_name  : "tree",// Name of theme
		        context     : [
		            {
		                id      : "create",
		                label   : "Create",
		                icon    : "create.png",
		                visible : function (NODE, TREE_OBJ) { if(NODE.length != 1) return false; return TREE_OBJ.check("creatable", NODE); },
		                action  : function (NODE, TREE_OBJ) { TREE_OBJ.create(false, TREE_OBJ.get_node(NODE)); }
		            },
		            "separator",

					      /*{
		                id      : "edit",
		                label   : "View Details",
		                icon    : "edit.png",
		                visible : function (NODE, TREE_OBJ) { return NODE.attr("rel")!="folderparent";},
		                action  : function (NODE, TREE_OBJ) {
			 				        $(NODE).find(" > a").click();
						        }
		            },*/
		            {
		                id      : "rename",
		                label   : "Rename",
		                icon    : "rename.png",
		                visible : function (NODE, TREE_OBJ) { if(NODE.length != 1) return false; return TREE_OBJ.check("renameable", NODE); },
		                action  : function (NODE, TREE_OBJ) { TREE_OBJ.rename(); }
		            },
		            {
		                id      : "delete",
		                label   : "Delete",
		                icon    : "remove.png",
		                visible : function (NODE, TREE_OBJ) { var ok = true; $.each(NODE, function () { if(TREE_OBJ.check("deletable", this) == false) ok = false; return false; }); return ok; },
		                action  : function (NODE, TREE_OBJ) { $.each(NODE, function () { TREE_OBJ.remove(this); }); }
		            }
		        ]
		    },
		    rules   : {
		        multiple    : false,
		        metadata    : false,
		        type_attr   : "rel",
		        multitree   : false,
		        createat    : "bottom",
		        use_inline  : false,
		        clickable   : "all",
		        renameable  : ["file"],
		        deletable   : ["file"],
		        creatable   : ["folderparent","file"],
		        draggable   : ["file"],
		        dragrules   : [
					"file inside file",
					"file before file",
					"file after file"

				],
		        drag_copy   : false,
		        droppable   : [],
		        drag_button : "left"
		    },
		    lang : {
		        new_node    : "New Page",
		        loading     : "Loading ..."
		    },
		    callback    : {
		        beforecreate: function(NODE,REF_NODE,TYPE,TREE_OBJ,PARENT) {
					var $PARENT=$(PARENT);
					var $NODE=$(NODE).attr("rel","file");;
					if($PARENT.attr("id")=="treebase"){
						var type="treebase";
						var id="";
					}else{
						var e=$PARENT.attr("id").match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id=e[2];
					}

					var ret=false;

					var url=base_url+"admin/shop_catalog/addCategory";

					$.ajax({
						url:url,
						dataType:"text",
						type:"post",
						async:false,
						data:{type:type,id:id},
						success:function(text){
						  var data = eval("(" + text + ")");
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();
							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
								$NODE.attr("id",data.newID).prepend("<input type='checkbox' id='"+data.newCheckBoxId+"' />");
								ret=true;
							}
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}

						}
					});

					return ret;
				},
		        beforerename: function(NODE,LANG,TREE_OBJ) {
					$NODE=$(NODE);
					$NODE.data("oldName",$NODE.children("a:eq(0)").text());
					return true;
				},
		        beforedelete: function(NODE,TREE_OBJ,a,b,c) {
					if(confirm("Are you sure you want to delete this page?\nSubpages, if any, will be orphaned !!")){
						var $NODE=$(NODE);
						var e=$NODE.attr("id").match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id=e[2];
						var ret=false;

						var url=base_url+"admin/shop_catalog/deleteCategory/";

						$.ajax({
							url:url,
							dataType:"text",
							type:"post",
							async:false,
							data:{id:id},
							success:function(text){
							  var data = eval("(" + text + ")");
								if($.isFunction(data.OVERRIDE)){
									data.OVERRIDE.apply();

								}
								if($.isFunction(data.successFunction)){
									data.successFunction.apply();
									ret=true;
								}
							},
							error:function(){
								if($.isFunction(ajaxFailure)){
									ajaxFailure.apply();
								}

							}
						});
					}

					return ret;
				},

				onrename    : function(NODE,LANG,TREE_OBJ,RB) {
					var $NODE=$(NODE);
					var oldName=$NODE.data("oldName");
					var newName=$NODE.children("a:eq(0)").text();
					var e=$NODE.attr("id").match(/(.+)[-=_](.+)/);
					var type=e[1];
					var id=e[2];

					var url=base_url+"admin/shop_catalog/renameCategory";

					$.ajax({
						url:url,
						dataType:"text",
						type:"post",
						data:{newName:newName,type:type,id:id},
						success:function(text){
						  var data = eval("(" + text + ")");
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();
								$NODE.children("a:eq(0)").text(oldName);
							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
							}
							$("a",NODE).click();
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}
							$NODE.children("a:eq(0)").text(oldName);
						}
					});
				},
		        onmove      : function(NODE,REF_NODE,TYPE,TREE_OBJ,RB) {
					var json=TREE_OBJ.getJSON();
					var url=base_url+"admin/shop_catalog/reOrderCategories";
					$.ajax({
						url:url,
						dataType:"text",
						type:"post",
						data:{data:JSON_stringify(json)},
						success:function(text){
						  var data = eval("(" + text + ")");
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();
							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
							}
							$("a",NODE).click();
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}
						}
					});
				},


		        ondblclk    : function(NODE, TREE_OBJ) { TREE_OBJ.toggle_branch.call(TREE_OBJ, NODE); TREE_OBJ.select_branch.call(TREE_OBJ, NODE); },
				onchange : function (NODE, TREE_OBJ) {
				  	var $NODE=$(NODE);
					var id=$NODE.attr("id");
					if(id!="treebase"){
						var e=id.match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id1=e[2];

						loadProducts(id);

					}else{
					  $("#dContent").html($("#defaultContent").html());
					}

					$("#innerTabs").hide(0);
        	$("#mainContent").removeClass("hastabs");
				}

		    }
		}
		);


	//--------END TREE

}
