$(document).ready(function() {

  //initial file view
  $("body").addClass(($.cookie("filesview")||"") + "View");

  // Add Button
  $("button.addFileBtn").overlay({width:"30%",inline:true,title:"Add New Image",href:"#addDownloadForm"});

	//--------DRAGGABLES AND DROPPABLES
	$("#downloadInner div.download:not(.noDrag)").live("click",function(e){
		var _this=$(this);
		if(e.metaKey){
		  _this.toggleClass("ui-selected");
		  MultipleFilesSelected();
		}else{
		  _this.addClass("ui-selected").siblings(".ui-selected")
  		.removeClass("ui-selected");
  		loadFileDetails(_this);
		}
	});

	$("#downloadInner").sortable({
	  zIndex:"2700",
	  items:"div.download:not(.noDrag)",
	  appendTo:"body",
    update:function(){
      NIBS.util.ajax($(this).sortable("serialize"),"admin/"+$(this).attr("rel"));
    },
    helper:function(e,el){
		  var _this=$(el);
		  if(_this.hasClass("ui-selected")){
		    var count=_this.siblings().andSelf().filter(".ui-selected").length;
		    if(count>1){
		      var container=$("<div id='multifileDragHelper' class='drghelper'><span class='count'>"+count+"</span></div>");
			    return container;
		    }
		  }
		  return _this.clone().addClass("drghelper");
		}
	});

	$("#downloads").selectable({
	  filter:"div.download:not(.noDrag)",
	  selected:function(){
	    MultipleFilesSelected();
	  }
	});

	$("#trashCan").droppable({
		accept:".download",
		hoverClass:"onDrop",
		tolerance:"pointer",
		drop:function(e,ui){
			var dropped=ui.draggable;
			if(dropped.hasClass("ui-selected")){
			  var selected=$("#downloadInner").find(".ui-selected:not(.drghelper,.ui-sortable-placeholder)");
			  var msg=selected.length>1 ? "Delete "+selected.length+" files?": "Are you sure?";
			  if(confirm(msg)){
			    var ids=[];
			    selected.each(function(){
			      var e=$(this).attr("id").match(/(.+)[-=_](.+)/),
			      id=e[2];
			      ids.push(id);
			    });
			    if(ids.length){
			      NIBS.util.ajax({ids:ids.join(",")},"admin/photos/deleteFile");
			    }
			  }
			}else{
			  if(confirm("Are you sure?")){
  				var e=dropped.attr("id").match(/(.+)[-=_](.+)/);
  				var id=e[2];
  				NIBS.util.ajax({id:id},"admin/photos/deleteFile");
  			}
			}

		}
	});
	//--------END DRAGGABLES AND DROPPABLES
	//--------CHECK BOXES
	$("#downloads div.download input[type=checkbox]").livequery("click",function(){
		act_deactivateFile($(this));
		return false;
	});
	//--------END CHECK BOXES
	//--------CAPTIONS
	$("#downloads div.download input[type=text]").live("change",function(){
		var _this=$(this),caption=_this.val();
  	var e=_this.parent().parent().attr("id").match(/(.+)[-=_](.+)/);
  	var id=e[2];

		NIBS.util.ajax({id:id,caption:caption},"admin/photos/changeCaption");
	});
	//--------END CAPTIONS

	//--------DND FILE UPLOAD
	var ddBox=$("#downloads"),ddBoxC=$("#downloadInner"),
	ddBoxP=$("#mainContentInner"),
	pbTemplate="<div class='download noDrag pn deleted'  style='z-index:2700;' id='pb_{guid}'><div><span class='filename'>{file}</span><span class='bar_holder'><span class='bar'></span></span></div></div>";

	ddBox.DNDU({url:base_url+"admin/photos/addFileViaDragAndDrop"})
	.bind("DNDU_drop",function(){
		ddBoxP.removeClass("onDrop");
	})
	.bind("DNDU_leave",function(){
		ddBoxP.removeClass("onDrop");
	})
	.bind("DNDU_over",function(){
		ddBoxP.addClass("onDrop");
	})
	.bind("DNDU_processStart",function(e,guid,filename){
	  ddBox[0].scrollTop=0;
		ddBoxC.prepend(pbTemplate.replace("{guid}",guid).replace("{file}",filename));
		_add($("#pb_"+guid+""));
	})
	.bind("DNDU_uploadProgress",function(e,guid,loaded,total){
		$("#pb_"+guid+" .bar").css(
			{width:((loaded/total)*100) + "%"}
			);
	})
	.bind("DNDU_uploadEnd",function(e,guid){
		$("#pb_"+guid+" .bar").css(
			{width:"100%"}
			);
	})
	.bind("DNDU_processEnd",function(e,guid,data){
		try{var objData= eval( "(" + data + ")" ) || null;}catch(e){};
		if(objData){
			if(objData.success=="error"){
				alert(objData.msg);
				_remove($("#pb_"+guid+""));
			}else if(objData.success=="success"){
				if($.isFunction(objData.successFunction)){
					addFileViaDnDHelper(guid,objData.successFunction);
				}
			}
		}else{
			alert("Error in response, File(s) may not have been saved");
			console.log(data);
			_remove($("#pb_"+guid+""));
		}
	});

	//--------END DND FILE UPLOAD

});





function addFile(id,path,caption){
  var html="<div class='download deleted' id='download_"+id+"'>\
  <div><img src='"+path+"'/></div>\
  <span class='caption'><input type='text' name='caption' placeholder='Image Caption' value='"+caption+"'/></span>\
  <span class='activator'><input type='checkbox' id='downloadCheckbox_"+id+"' checked /></span>\
  </div>"
	$("#downloads")[0].scrollTop=0
	$("#downloadInner").prepend(html);
	_add($("#download_"+id));
}

function addFileViaDnDHelper(guid,fn){
  setTimeout(function(){
    $("#pb_"+guid+"").replaceWith(fn.apply());
  },500);
}
function addFileViaDnD(id,path){
  return $("<div class='download' id='download_"+id+"'>\
  <div><img src='"+path+"'/></div>\
  <span class='caption'><input type='text' name='caption' placeholder='Image Caption' value=''/></span>\
  <span class='activator'><input type='checkbox' id='downloadCheckbox_"+id+"' checked /></span>\
  </div>");
}

function act_deactivateFile(el){
	if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({"id":id},"admin/photos/activateDeactivateFile");

}

function do_act_deactivateFile(id,val){
	if(val=="0"){
		$("#downloadCheckbox_"+id).prop("checked",false);
		$("#download_"+id).addClass("not_visible");
	}else{
		$("#downloadCheckbox_"+id).prop("checked",true);
		$("#download_"+id).removeClass("not_visible");
	}
}

function deleteMultiFileHelper(download_id,delay){
  setTimeout(function(){
    deleteFile(download_id)
  },delay);
}
function deleteFile(download_id){
  _remove($("#download_"+download_id));
}

function _remove(el){
  el.addClass("deleted");
	setTimeout(function(){
	  el.remove();
	},600);
}
function _add(el){
  setTimeout(function(){
	  el.removeClass("deleted");
	  setTimeout(function(){
	    el.css({zIndex:""});
	  },600);
	},100);
}

function loadFileDetails(id){
	var e=id.attr("id").match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#downloadEditor").load(base_url+"admin/photos/viewFileDetails/"+id);
}
function MultipleFilesSelected(){
  $("#downloadEditor").html("<div class='headertoolbar'>File Details</div><div class='padded' style='text-align:center'><b>Multiple files selected...</b></div>")
}

(function($){
	var support={"XHRsendAsBinary":false,"XHRupload":false,"fileReader":false,"formData":false};
	var s_xhr=new XMLHttpRequest();
	support.XHRsendAsBinary=("sendAsBinary" in s_xhr);
	support.XHRupload=("upload" in s_xhr);
	try{ // firefox,chrome
		support.fileReader = !!new FileReader();
		support.formData = !!new FormData();
	}catch(e){};

	$.fn.DNDU=function(settings){

		//options
		var opts=$.extend({},{
			url:""
		},settings);

		return this.each(function(){
			var _this=this,$this=$(_this);
			_this.addEventListener("drop", function(e){
				e.preventDefault();
				var data=e.dataTransfer;
				var files=data.files;
				$this.trigger("DNDU_drop");
				new DNDU(_this,files,opts);
				return false;
			}, true);
			_this.addEventListener("dragenter", function(e){
				e.stopPropagation();
				e.preventDefault();
				$this.trigger("DNDU_enter");
				return false;
			}, true);
			_this.addEventListener("dragover", function(e){
				e.stopPropagation();
				e.preventDefault();
				$this.trigger("DNDU_over");
				return false;
			}, true);
			_this.addEventListener("dragleave", function(e){
				$this.trigger("DNDU_leave");
				return false;
			}, true);
		});
	};

	function DNDU(el,files,opts){ //constructor
		this.files=files;
		this.opts=opts;
		this.supports=support;
		this.el=el;
		this.url=this.opts.url+"/";
		var temp=[];
		for(var prop in this.supports){
			temp.push(prop+":"+this.supports[prop]);
		}
		this.url+=temp.join("_");

		this.process(this.files);

	};
	DNDU.prototype.process=function(){
		var _this=this,files=_this.files;
		for(var i=0;i<files.length;i++){
		  if(files[i].fileSize>0 || files[i].size>0){
		    var guid=(new Date()).getTime()+"_"+Math.floor(Math.random()*200);
  			files[i].guid=guid;
  			files[i].isResizable=false;
  			files[i].isResized=false;
  			files[i].fileName=files[i].fileName||files[i].name;
  			files[i].fileSize=files[i].fileSize||files[i].size;

  			$(_this.el).trigger("DNDU_processStart",[files[i].guid,files[i].fileName]);
  			_this.upload(files[i]);
		  }
		}
	};

	DNDU.prototype.upload=function(file){
		var _this=this;
		var headers={};
		if(_this.supports.XHRupload){
			headers={
			"Cache-Control":"no-cache",
			"X-Requested-With":"XMLHttpRequest",
			"X-File-Name":file.fileName,
			"X-File-Size":file.fileSize,
			"Content-Type":"multipart/form-data"};
			_this.send(file,headers);
		}
	};
	DNDU.prototype.send=function(file,headers){
		var _this=this;
		var xhr = new XMLHttpRequest();
		var upload = xhr.upload;
		upload.fileObj = file;
		upload.downloadStartTime = new Date().getTime();
		upload.currentStart = upload.downloadStartTime;
		upload.currentProgress = 0;
		upload.startData = 0;

		//trigger start upload
		$(_this.el).trigger("DNDU_uploadStart",[file.guid,file.fileName]);
		// add listeners
		upload.onprogress=function(e){
			//console.log(e);
			if (e.lengthComputable) {
				$(_this.el).trigger("DNDU_uploadProgress",[file.guid,e.loaded,e.total]);
			}
		};

		upload.onload=function(e){
			//console.log(e);
			$(_this.el).trigger("DNDU_uploadEnd",[file.guid]);
			//file=null;
			//delete(file);
		};

		xhr.onload=function(e){
			if ( e.target.readyState == 4 ) {
			  //console.log(this.responseText);
				$(_this.el).trigger("DNDU_processEnd",[file.guid,this.responseText]);
			}
		};

		if(file.isResized){
			xhr.open("POST", _this.url+"isResized=true",true);
		}else{
			xhr.open("POST", _this.url,true);
		}

		for(var prop in headers){
			xhr.setRequestHeader(prop, headers[prop]);
		}

		xhr.send(file);

	};
})(jQuery);
