<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings_m extends My_Model {
	function __construct(){
		parent::__construct();
		$this->tablename="app_settings";
        $this->_repo="./assets/images";
	}
	function getSettingByName($name) {
      return $this->db->where("name",$name)->get($this->tablename)->row_array();
  	}
}

?>