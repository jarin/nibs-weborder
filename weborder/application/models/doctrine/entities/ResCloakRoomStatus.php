<?php



namespace entities;

/**
 * ResCloakRoomStatus
 *
 * @Table(name="res_cloak_room_status")
 * @Entity
 */
class ResCloakRoomStatus
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="integer", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $status
   *
   * @Column(name="status", type="string", length=45, nullable=false)
   */
  private $status;
  
  /**
   * @var string $statusColor
   *
   * @Column(name="status_color", type="string", length=9, nullable=false)
   */
  private $statusColor;


  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set status
   *
   * @param string $status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * Get status
   *
   * @return string 
   */
  public function getStatus()
  {
    return $this->status;
  }
  
  /**
   * Set statusColor
   *
   * @param string $statusColor
   */
  public function setStatusColor($statusColor)
  {
      $this->statusColor = $statusColor;
  }
  
  /**
   * Get statusColor
   *
   * @return string
   */
  public function getStatusColor()
  {
      return $this->statusColor;
  }
}