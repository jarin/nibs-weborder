<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="x-ua-compatible" content="IE=10">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="<?=$meta_description?>" />
	<meta name="keywords" content="<?=$meta_key?>">
	<meta name="author" content="" />
	   
	<base href="<?=base_url()?><?=(($restaurant !='')? 'restaurant/'.$restaurant.'/':'')?>">
	<title><?=$page_title?></title>
	<!-- Stylesheets -->
	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/responsive.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/thumb-slide.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.css">
	<link href="<?=base_url()?>assets/css/datepicker.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-datetimepicker.min.css" media="screen" rel="stylesheet" type="text/css" />
	
	
	<link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css">

	<!--[if IE 9]>
	<script src="assets/js/media.match.min.js"></script>
	<![endif]-->
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="<?=base_url()?>assets/js/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.autocomplete.js"></script>	
	
	
	<script type="text/javascript">
    	var BASE_URL = '<?=base_url()?>';
    	var body=$("body");
    </script>

</head>

<body>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '667555933361147',
      xfbml      : true,
      version    : 'v2.2'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
    <div id="main-wrapper">