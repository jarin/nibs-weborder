                                       <?php  if($this->session->userdata('cart') !=''){
                                        ?>
                                        <ul class="list-unstyled">
                                            <?php 
                                            $total = 0;
                                            foreach ($this->session->userdata('cart') as $cartItem){
                                            $total = $total + $cartItem['total_price'];
                                             ?>
											<li>
												<p style="line-height:20px;"><?php echo $cartItem['quantity']?>x <?php echo $cartItem['product_name']?>
													<span class="icon-link"><i class="fa fa-pencil editItem"  rel="<?php echo $cartItem['product_id']?>" data-item="<?php echo $cartItem['orderItemId']?>"></i><i class="fa fa-times delItems" rel="<?php echo $cartItem['product_id']?>" data-item="<?php echo $cartItem['orderItemId']?>"></i>
													</span>
												</p>
												
												<!--  <p class="price">&pound; <?php echo $cartItem['price']?></p>-->
												<p class="price">&pound; <?php echo $cartItem['total_price']?></p>
											</li>
											<?php } ?>
											

											<li>
												<!-- list for total price-->
												<p>Total</p>
												<p class="price-total">&pound; <?php echo $total?></p>
											</li>
										</ul>
										<div class="checkout">
											<a class="btn btn-default-red" href="order/confirm"><i class="fa fa-shopping-cart"></i>My Order</a>
										</div>
										
										<!-- Modal -->
                                        <div id="editItemModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:600px;left:30%;top:10%;">
                                          <div style="background: #fff;">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                            <h3 id="myModalLabel" style="font-weight: bold;">Edit Cart Items</h3>
                                          </div>
                                          <form class="default-form form-inline" role="form" id="updateItem">
                                              <div class="modal-body modal_content">
                                                
                                              </div>
                                              <div class="modal-footer">
                                                <button class="btn btn-default-red updateCart" name="submit" type="submit" style="padding:2px 10px">Confirm</button>
                                                <button class="btn btn-default-black" data-dismiss="modal" aria-hidden="true" style="margin-bottom:15px;">Close</button>
                                              </div>
                                          </form>
                                          </div>
                                        </div>
                                        <!-- END Modal -->
										
										<?php } else { ?>
										<p style="text-align: center;">You do not have any Items</p>
										<?php } ?>
										



