$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addBusinessBtn").overlay({width:"30%",inline:true,title:"Add New Business",href:"#addBusinessForm"});
  $("#sidebar ul.list li input[type=checkbox]").live("click",function(){
  	activate_business($(this));
  	return false;
  });

  // See More details on click
  $("#sidebar ul.list li").live("click",function(){
  	var _li=$(this);
  	_li.siblings().removeClass("active");
  	_li.addClass("active");
  	loadDetails(_li.attr("id"));
  	return false;
  });

	//checkbox states
	$("#thirdBar input[type=checkbox]:not(.main_cb)").live("click",function(){
		determineMainCBState(this);
	});
});

function replaceImage(id){
	$("#"+id).find("img").attr("src","");
}

function addBusiness(name,id){
	NIBS.notif.notify("success",name+" has been added as a new business");
	
	
	var html="<li id='business_"+id+"'>"+
	"<a href='#'>"+
	"<span class='name icon-box'><i class=''><img src='"+base_url+"assets/images/admin/application_list.png' border='0'/></i></span>"+
	"<span class='name nameBtn' title='"+name+"'>"+name+"</span>"+
	"<span class='list_icon_btn'>"+
	"<input title='Delete Page' type='image' class='action delete' rel='business/deleteBusiness' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
	"<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"+
	"</span></a></li>";

	$("#sidebar ul.list").append(html);
	$("#add_business_form").reset();
	setTimeout(function(){
		$("#business_"+id).removeClass("deleted");
	},100);
}

function loadDetails(id){
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#dContent").load(base_url+"admin/business/loadDetails/"+id);
	$("#thirdBar").show(0);
	$("#main-content").addClass("threepane");
	$("#thirdBar").html("<div style='text-align:center;margin-top:40px'><button class='button edit secondary' onclick='loadPermissions(\"business_"+id+"\");return false;'>Manage Permissions</button></div>");
}

function deleteBusiness(id){
	var el=$("#business_"+id);
	if(el.is(".active")){
		$("#dContent").html($("#defaultContent").html());
		$("#thirdBar").hide(0);
		$("#main-content").removeClass("threepane");
	}
	el.addClass("deleted");
	setTimeout(function(){
		el.remove()
	},500);
	NIBS.notif.notify("success",el.find("span").text()+" removed successfully");
}

function activate_business(el){
	if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({id:id},"admin/business/activateDeactivate");
}
function do_activate_page(id,val){
	$("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
	$("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

function loadPermissions(id){
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#thirdBar").load(base_url+"admin/business/loadPermissions/"+id,function(){
		$("#thirdBar input[type=checkbox]:not(.main_cb):first-child").each(function(){
			determineMainCBState(this);
		});
	});
}

function checkUncheck_all_perms(e,el){
	e.stopPropagation();
	var el=$(el),
	parent=el.parent().parent(),
	currentState=el.prop("checked");
	if(currentState==true || currentState==false){
		$("input[type=checkbox]",parent[0]).prop("checked",currentState);
	}
}

function determineMainCBState(el){
	var el=$(el),
	parent=el.parent().parent(),
	main_cb=parent.find("input.main_cb"),
	initial=false,truecount=falsecount=totalcount=0;
	parent.find("input[type=checkbox]:not(.main_cb)").each(function(){
		var _this=$(this);
		_this.prop("checked")==true ? truecount++ : falsecount++;
		totalcount++;
	});
	if(totalcount==truecount){
		main_cb.prop("indeterminate",false).prop("checked",true);

	}else if(totalcount==falsecount){
		main_cb.prop("indeterminate",false).prop("checked", false);
	}else{
		main_cb.prop("indeterminate",true);
	}
}

$(document).ready(function(){	
$('#edit_opening_input').css("z-index","9999");

	$('#edit_opening_input').ptTimeSelect({
		containerClass: "timeCntr",
		containerWidth: "350px",
		setButtonLabel: "Select",
		minutesLabel: "min",
		hoursLabel: "Hrs"
	});
	$('#edit_closing_input').ptTimeSelect({
		containerClass: "timeCntr",
		containerWidth: "350px",
		setButtonLabel: "Select",
		minutesLabel: "min",
		hoursLabel: "Hrs"
	});
});