       <?php if(count($categories) > 0){ ?>
        <!-- thumbnail slide section -->
			<div id="thumbnail-slide">
				<div class="container">
					<div id="thumb-slide">
						<div id="thumb-slide-section" class="owl-carousel">
						    <?php foreach ($categories as $category){?>
							<div class="item" style="display:table-cell;vertical-align:middle">
								<a href="order/index/<?php echo $category['id']?>">
									<?php echo $category['categoryName']?>
								</a>
							</div>
							<?php } ?>
							
						</div>
						<!-- end .thumb-slide-section -->
					</div>
					<!-- end #thumb-slide -->
				</div>
				<!-- end .container -->
			</div>
			<!-- end .thumbnail-slide -->
			<?php } ?>