<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Import_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->product="res_product";
		$this->category="res_category";
		$this->prep_location="res_prep_location";
		$this->product_ingredient="res_product_ingredient";
		$this->ingredient="res_ingredient";
		$this->product_addon="res_product_addon";
		$this->addon="res_addon";
		$this->epos="epos_setting";
		$this->tablename=$this->product;
		$this->load->model("Business_m","business");
		
	}

	function getEposDbSettings($res_id)
	{
		  return $addon_id= $this->db->select("*")
	          ->where($this->epos.".restaurant_id", $res_id)
		     ->get($this->epos)->row_array();
	}
	
	function getProducts($restarant_id){

		
		
		$databaseConfig = $this->getEposDbSettings($restarant_id);
		
		
		if(!empty($databaseConfig)){
			$dsn = 'mysqli://'.$databaseConfig['db_user'].':'.$databaseConfig['db_pass'].'@'.$databaseConfig['db_host'].'/'.$databaseConfig['db_name'].'';
			
			 $this->import_db = $this->load->database($dsn,TRUE);
			
			 if ( $this->import_db->conn_id === FALSE) {
			 	$this->session->set_flashdata('error', 'Database connection error, please check your epos setting');
				redirect(base_url().'admin/productImport');
			 }
			
			$products=$this->import_db->select("*,$this->tablename.id as id,$this->tablename.description as description, $this->tablename.is_active as is_active,
					 $this->category.is_active as cat_active, $this->category.last_update as cat_last_update, 
					$this->category.is_bar as cat_is_bar, $this->category.description as cat_description, 
					$this->category.sequence_number as cat_sequence_number,$this->category.category_name as category_name, parentCat.category_name as parent_category_name,
					$this->category.parent_id as parent_id,$this->category.top_color as top_color,$this->category.bottom_color as bottom_color")
			->join($this->category,"$this->category.id=$this->tablename.category_id",'left')
			->join("$this->category as parentCat","parentCat.id=$this->category.parent_id",'left')
			->get($this->tablename)->result_array();
			
			if(count($products) > 0){
				$addon = array();
				$ingredient = array();
				foreach ($products as $product){
					$addon[$product['id']] = $this->import_db->select("$this->addon.*")
					->where("$this->tablename.id",$product['id'])
					->join($this->product_addon,"$this->tablename.id = $this->product_addon.product_id","left")
					->join($this->addon,"$this->addon.id = $this->product_addon.addon_id","left")
					->get($this->tablename)->result_array();
						
					$ingredient[$product['id']] = $this->import_db->select("$this->ingredient.*,$this->product_ingredient.is_with")
					->where("$this->tablename.id",$product['id'])
					->join($this->product_ingredient,"$this->tablename.id = $this->product_ingredient.product_id","left")
					->join($this->ingredient,"$this->ingredient.id = $this->product_ingredient.ingredient_id","left")
					->get($this->tablename)->result_array();
				}
				$this->import_db->close();
				
				$this->_updateDb($restarant_id,$products,$ingredient,$addon);
			} else {
				$this->import_db->close();
				return false;
			}
		} else {
			$this->session->set_flashdata('error', 'To import product, please provide your epos settings first');
			redirect(base_url().'admin/productImport');
		}
		
		
	}
	
	function _updateDb($restarant_id, $products, $ingredient, $addon){
		$ret = false;
		$this->load->helper('date');
		foreach ($products as $key => $product){
		
			$product_id = $product['id'];
			$product_id_old = $product['id'];
			$categoryExist = $this->db->get_where($this->category, array("category_name" => $product['category_name'],'restaurant_id'=>$restarant_id))->result_array();
			
			if(count($categoryExist) > 0){
				if(!empty($product['parent_id'])){
					$categoryparent = $this->db->get_where($this->category, array("category_name" => $product['parent_category_name'],'restaurant_id'=>$restarant_id))->row_array();
					
					if(count($categoryparent) > 0){
						
							$this->db->where(array('id'=>$categoryExist[0]['id'],'restaurant_id'=>$restarant_id))->update($this->category,array(
									'parent_id' => $categoryparent['id']
							));
						
					} else {
							
						$this->db->insert($this->category,array(
					
								"sequence_number" => $product['cat_sequence_number'],
								"category_name" => $product['parent_category_name'],
								"description" => $product['cat_description'],
								"top_color" => $product['top_color'],
								"bottom_color" => $product['bottom_color'],
								"is_active" => $product['cat_active'],
								"last_update" => $product['cat_last_update'],
								"is_bar" => $product['cat_is_bar'],
								'restaurant_id'=>$restarant_id
						));
					$parent_cat_id =$this->db->insert_id();
						$this->db->insert($this->category,array(
									"parent_id" => $parent_cat_id,
									"sequence_number" => $product['cat_sequence_number'],
									"category_name" => $product['category_name'],
									"description" => $product['cat_description'],
									"top_color" => $product['top_color'],
									"bottom_color" => $product['bottom_color'],
									"is_active" => $product['cat_active'],
									"last_update" => $product['cat_last_update'],
									"is_bar" => $product['cat_is_bar'],
									'restaurant_id'=>$restarant_id
							));
							$cat_id = $this->db->insert_id();
						$this->db->where(array('id'=>$categoryExist[0]['id'],'restaurant_id'=>$restarant_id))->update($this->category,array(
								'parent_id' => $cat_id
						));
							
					}
				} else {
					$this->db->where(array('id'=>$categoryExist[0]['id'],'restaurant_id'=>$restarant_id))->update($this->category,array(
							'parent_id' => NULL,
					));
				}
				
				
				
				
				$productExist = $this->db->get_where($this->product, array("product_name" => $product['product_name'], "category_id" => $categoryExist[0]['id'],'restaurant_id'=>$restarant_id))->result_array();
				
				if(count($productExist) > 0){
					$this->db->where(array('id'=>$productExist[0]['id'],'restaurant_id'=>$restarant_id))->update($this->product,array(
							'description' => $product['description'],
							'price' => $product['price'],
							'takeaway_price' => $product['takeaway_price'],
							'web_order_price' => $product['web_order_price'],
							'delivery_price' => $product['delivery_price'],
							'is_favourite' => $product['is_favourite'],
							'is_active' => $product['is_active'],
							'is_tax_inclusive' => $product['is_tax_inclusive'],
							'sequence_number' => $product['sequence_number'],
							'is_taxable' => $product['is_taxable'],
							'is_bar_favourite' => $product['is_bar_favourite'],
							'waiting_price' => $product['waiting_price'],
							'is_stock_controlled' => $product['is_stock_controlled'],
							'quantity' => $product['quantity'],
							'stock' => $product['stock'],
					));
					$product_id = $productExist[0]['id'];
					$ret = true;
				} else {
					
					$this->db->insert($this->product,array(
							"product_name" => $product['product_name'],
							"category_id" => $categoryExist[0]['id'],
							"prep_location_id" => $product['prep_location_id'],
							
							'description' => $product['description'],
							'price' => $product['price'],
							'takeaway_price' => $product['takeaway_price'],
							'web_order_price' => $product['web_order_price'],
							'delivery_price' => $product['delivery_price'],
							'is_favourite' => $product['is_favourite'],
							'is_active' => (($product['is_active'] !='')?$product['is_active']:0),
							'last_update' => (($product['last_update'] !='')?$product['last_update']:mdate("%Y-%m-%d %H:%i:%s")),
							'is_tax_inclusive' => $product['is_tax_inclusive'],
							'sequence_number' => (($product['sequence_number'] !='')?$product['sequence_number']:0),
							'is_taxable' => $product['is_taxable'],
							'is_bar_favourite' => $product['is_bar_favourite'],
							'waiting_price' => $product['waiting_price'],
							'long_name' => $product['long_name'],
							'quantity_type_id' => $product['quantity_type_id'],
							'stock_quantity_type_id' => $product['stock_quantity_type_id'],
							'is_stock_controlled' => $product['is_stock_controlled'],
							'quantity' => $product['quantity'],
							'stock' => $product['stock'],
							'restaurant_id'=>$restarant_id
					));
					$product_id = $this->db->insert_id();
					$ret = true;
				}
			} else {
				$cat_id =NULL;
				if(!empty($product['parent_id'])){
					
					$categoryparent = $this->db->get_where($this->category, array("category_name" => $product['parent_category_name'],'restaurant_id'=>$restarant_id))->row_array();
					if(count($categoryparent) == 0){
						
							
						$this->db->insert($this->category,array(
					
								"sequence_number" => $product['cat_sequence_number'],
								"category_name" => $product['parent_category_name'],
								"description" => $product['cat_description'],
								"top_color" => $product['top_color'],
								"bottom_color" => $product['bottom_color'],
								"is_active" => $product['cat_active'],
								"last_update" => $product['cat_last_update'],
								"is_bar" => $product['cat_is_bar'],
								'restaurant_id'=>$restarant_id
						));
						$parent_cat_id = $this->db->insert_id();
					
						$this->db->insert($this->category,array(
								"parent_id" => $parent_cat_id,
								"sequence_number" => $product['cat_sequence_number'],
								"category_name" => $product['category_name'],
								"description" => $product['cat_description'],
								"top_color" => $product['top_color'],
								"bottom_color" => $product['bottom_color'],
								"is_active" => $product['cat_active'],
								"last_update" => $product['cat_last_update'],
								"is_bar" => $product['cat_is_bar'],
								'restaurant_id'=>$restarant_id
						));
						$cat_id = $this->db->insert_id();
							
					} else {
						$this->db->insert($this->category,array(
									
								"sequence_number" => $product['cat_sequence_number'],
								"category_name" => $product['category_name'],
								"description" => $product['cat_description'],
								"top_color" => $product['top_color'],
								"bottom_color" => $product['bottom_color'],
								"is_active" => $product['cat_active'],
								"last_update" => $product['cat_last_update'],
								"is_bar" => $product['cat_is_bar'],
								'restaurant_id'=>$restarant_id
						));
						$cat_id = $this->db->insert_id();
					}
				} else {
					
					$this->db->insert($this->category,array(
							
							"sequence_number" => $product['cat_sequence_number'],
							"category_name" => $product['category_name'],
							"description" => $product['cat_description'],
							"top_color" => $product['top_color'],
							"bottom_color" => $product['bottom_color'],
							"is_active" => $product['cat_active'],
							"last_update" => $product['cat_last_update'],
							"is_bar" => $product['cat_is_bar'],
							'restaurant_id'=>$restarant_id
					));
					$cat_id = $this->db->insert_id();
				}
				
				
				
				
				$this->db->insert($this->product,array(
						"product_name" => $product['product_name'],
						"category_id" => $cat_id,
						"prep_location_id" => $product['prep_location_id'],
						
						'description' => $product['description'],
						'price' => $product['price'],
						'takeaway_price' => $product['takeaway_price'],
						'web_order_price' => $product['web_order_price'],
						'delivery_price' => $product['delivery_price'],
						'is_favourite' => $product['is_favourite'],
						'is_active' => (($product['is_active'] !='')?$product['is_active']:0),
						'last_update' => (($product['last_update'] !='')?$product['last_update']:mdate("%Y-%m-%d %H:%i:%s")),
						'is_tax_inclusive' => $product['is_tax_inclusive'],
						'sequence_number' => (($product['sequence_number'] !='')?$product['sequence_number']:0),
						'is_taxable' => $product['is_taxable'],
						'is_bar_favourite' => $product['is_bar_favourite'],
						'waiting_price' => $product['waiting_price'],
						'long_name' => $product['long_name'],
						'quantity_type_id' => $product['quantity_type_id'],
						'stock_quantity_type_id' => $product['stock_quantity_type_id'],
						'is_stock_controlled' => $product['is_stock_controlled'],
						'quantity' => $product['quantity'],
						'stock' => $product['stock'],
						'restaurant_id'=>$restarant_id
				));
				$product_id = $this->db->insert_id();
				$ret = true;
			}
			
			if(count($addon) > 0){
				$this->db->delete($this->product_addon,array('product_id'=> $product_id,'restaurant_id'=>$restarant_id));
				if(isset($addon[$product_id_old])){
					foreach ($addon[$product_id_old] as $key => $ad){
						if(!empty($ad['id'])){
							$addonExist = $this->db->get_where($this->addon, array("addon_name" => $ad['addon_name'],'restaurant_id'=>$restarant_id))->row_array();
								
							if(count($addonExist) > 0){
								$this->db->where(array('id' => $addonExist['id'],'restaurant_id'=>$restarant_id))->update($this->addon,array(
										'addon_name' => $ad['addon_name'],
										'addon_image' => $ad['addon_image'],
										'is_active' => $ad['is_active'],
										'price' => $ad['price'],
										'takeaway_price' => $ad['takeaway_price'],
										'web_order_price' => $ad['web_order_price'],
										'delivery_price' => $ad['delivery_price'],
										'waiting_price' => $ad['waiting_price']
								));
					
									
					
								$this->db->insert($this->product_addon,array(
										"product_id" => $product_id,
										"addon_id" => $addonExist['id'],
										'restaurant_id'=>$restarant_id
								));
									
								$ret = true;
							} else {
								$this->db->insert($this->addon,array(
										'addon_name' => $ad['addon_name'],
										'addon_image' => $ad['addon_image'],
										'is_active' => $ad['is_active'],
										'price' => $ad['price'],
										'takeaway_price' => $ad['takeaway_price'],
										'web_order_price' => $ad['web_order_price'],
										'delivery_price' => $ad['delivery_price'],
										'waiting_price' => $ad['waiting_price'],
										'restaurant_id'=>$restarant_id
								));
								$this->db->insert($this->product_addon,array(
										"product_id" => $product_id,
										"addon_id" => $this->db->insert_id(),
										'restaurant_id'=>$restarant_id
								));
					
								$ret = true;
							}
						}
					}
				}
				
			}
			
			if(count($ingredient) > 0){
				$this->db->where(array('product_id'=>$product_id,'restaurant_id'=>$restarant_id))->delete($this->product_ingredient);
				
				if(isset($ingredient[$product_id_old])){
					foreach ($ingredient[$product_id_old] as $key => $ingred){
							
						if(!empty($ingred['id'])){
							$ingredExist = $this->db->get_where($this->ingredient, array("ingredient_name" => $ingred['ingredient_name'],'restaurant_id'=>$restarant_id))->row_array();
							if(count($ingredExist) > 0){
								$this->db->where(array('id'=>$ingredExist['id'],'restaurant_id'=>$restarant_id))->update($this->ingredient,array(
										'ingredient_name' => $ingred['ingredient_name'],
										'price' => $ingred['price'],
										'is_active' => $ingred['is_active'],
										'price_without' => $ingred['price_without']
								));
									
									
								$this->db->insert($this->product_ingredient,array(
										"is_with" => $ingred['is_with'],
										"product_id" => $product_id,
										"ingredient_id" => $ingredExist['id'],
										'restaurant_id'=>$restarant_id
								));
									
								$ret = true;
							} else {
								$this->db->insert($this->ingredient,array(
										'ingredient_name' => $ingred['ingredient_name'],
										'price' => $ingred['price'],
										'is_active' => $ingred['is_active'],
										'price_without' => $ingred['price_without'],
										'restaurant_id'=>$restarant_id
								));
					
								$this->db->insert($this->product_ingredient,array(
										"is_with" => $ingred['is_with'],
										"product_id" => $product_id,
										"ingredient_id" => $this->db->insert_id(),
										'restaurant_id'=>$restarant_id
								));
								$ret = true;
							}
								
						}
							
					}
				}
				
			}
			
		}
		
	
		
		return $ret;
	}
	

}
?>