<?php



namespace entities;

/**
 * ResReservation
 *
 * @Table(name="res_reservation")
 * @Entity
 */
class ResReservation
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;
  
  /**
   * @var boolean $hasCustomerSatDown
   *
   * @Column(name="has_customer_sat_down", type="boolean", nullable=false)
   */
  private $hasCustomerSatDown=FALSE;

  /**
   * @var datetime $timeFrom
   *
   * @Column(name="time_from", type="datetime", nullable=false)
   */
  private $timeFrom;
  
  /**
   * @var datetime $timeTo
   *
   * @Column(name="time_to", type="datetime", nullable=false)
   */
  private $timeTo;

  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $customer;
  
  /**
   * @var smallint $noOfGuests
   *
   * @Column(name="no_of_guests", type="smallint", nullable=false)
   */
  private $noOfGuests;
  
  /**
   * @var ResTable
   *
   * @ManyToOne(targetEntity="ResTable")
   * @JoinColumns({
   *   @JoinColumn(name="table_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $table;
  
  /**
   * @var string $specialInstruction
   *
   * @Column(name="special_instruction", type="string", length=500, nullable=false)
   */
  private $specialInstruction;

  /**
   * @var ResReservationType
   *
   * @ManyToOne(targetEntity="ResReservationType")
   * @JoinColumns({
   *   @JoinColumn(name="reservation_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $reservationType;
  
  /**
   * @var decimal $depositAmount
   *
   * @Column(name="deposit_amount", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $depositAmount=0;

  /**
   * @var ResDepositType
   *
   * @ManyToOne(targetEntity="ResDepositType")
   * @JoinColumns({
   *   @JoinColumn(name="deposit_type_id", referencedColumnName="id", onDelete="RESTRICT", onUpdate="CASCADE")
   * })
   */
  private $depositType;
  
  /**
   * @var datetime $reservationDate
   *
   * @Column(name="reservation_date", type="datetime", nullable=false)
   */
  private $reservationDate;

  /**
   * @var ResPromo
   *
   * @ManyToOne(targetEntity="ResPromo")
   * @JoinColumns({
   *   @JoinColumn(name="promo_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $promo;
  
  /**
   * @var bigint $restaurantId
   *
   * @Column(name="restaurant_id", type="bigint", nullable=true)
   */
  private $restaurantId;
  
  /**
   * @var ResReservationStatus
   *
   * @ManyToOne(targetEntity="ResReservationStatus")
   * @JoinColumns({
   *   @JoinColumn(name="reservation_status_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $status;
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set hasCustomerSatDown
   *
   * @param boolean $hasCustomerSatDown
   */
  public function setHasCustomerSatDown($hasCustomerSatDown)
  {
    $this->hasCustomerSatDown = $hasCustomerSatDown;
  }

  /**
   * Get hasCustomerSatDown
   *
   * @return boolean 
   */
  public function getHasCustomerSatDown()
  {
    return $this->hasCustomerSatDown;
  }
  
  /**
   * Set timeFrom
   *
   * @param datetime $timeFrom
   */
  public function setTimeFrom($timeFrom)
  {
    $this->timeFrom = $timeFrom;
  }

  /**
   * Get timeFrom
   *
   * @return datetime 
   */
  public function getTimeFrom()
  {
    return $this->timeFrom;
  }

  /**
   * Set timeTo
   *
   * @param datetime $timeTo
   */
  public function setTimeTo($timeTo)
  {
  	$this->timeTo = $timeTo;
  }
  
  /**
   * Get timeTo
   *
   * @return datetime
   */
  public function getTimeTo()
  {
  	return $this->timeTo;
  }
  
  /**
   * Set reservationDate
   *
   * @param datetime $reservationDate
   */
  public function setReservationDate($reservationDate)
  {
  	$this->reservationDate = $reservationDate;
  }
  
  /**
   * Get reservationDate
   *
   * @return datetime
   */
  public function getReservationDate()
  {
  	return $this->reservationDate;
  }
  
  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
  	$this->customer = $customer;
  }
  
  /**
   * Get customer
   *
   * @return ResCustomer
   */
  public function getCustomer()
  {
  	return $this->customer;
  }
  
  /**
   * Set table
   *
   * @param ResTable $table
   */
  public function setTable(\ResTable $table)
  {
  	$this->table = $table;
  }
  
  /**
   * Get table
   *
   * @return ResTable
   */
  public function getTable()
  {
  	return $this->table;
  }
  
  /**
   * Set noOfGuests
   *
   * @param smallint $noOfGuests
   */
  public function setNoOfGuests($noOfGuests)
  {
  	$this->noOfGuests = $noOfGuests;
  }
  
  /**
   * Get noOfGuests
   *
   * @return smallint
   */
  public function getNoOfGuests()
  {
  	return $this->noOfGuests;
  }
  
  /**
   * Set specialInstruction
   *
   * @param string $specialInstruction
   */
  public function setSpecialInstruction($specialInstruction)
  {
  	$this->specialInstruction = $specialInstruction;
  }
  
  /**
   * Get specialInstruction
   *
   * @return string
   */
  public function getSpecialInstruction()
  {
  	return $this->specialInstruction;
  }
  
  /**
   * Set reservationType
   *
   * @param ResUser $reservationType
   */
  public function setReservationType(\ResReservationType $reservationType)
  {
  	$this->reservationType = $reservationType;
  }
  
  /**
   * Get reservationType
   *
   * @return ResReservationType
   */
  public function getReservationType()
  {
  	return $this->reservationType;
  }
  
  /**
   * Set depositAmount
   *
   * @param decimal $depositAmount
   */
  public function setDepositAmount($depositAmount)
  {
  	$this->depositAmount = $depositAmount;
  }
  
  /**
   * Get depositAmount
   *
   * @return decimal
   */
  public function getDepositAmount()
  {
  	return $this->depositAmount;
  }
  
  /**
   * Set depositType
   *
   * @param ResDepositType $depositType
   */
  public function setDepositType(\ResDepositType $depositType)
  {
  	$this->depositType = $depositType;
  }
  
  /**
   * Get depositType
   *
   * @return ResDepositType
   */
  public function getDepositType()
  {
  	return $this->depositType;
  }
  
  /**
   * Set promo
   *
   * @param ResPromo $promo
   */
  public function setPromo(\ResPromo $promo)
  {
  	$this->promo = $promo;
  }
  
  /**
   * Get promo
   *
   * @return ResPromo
   */
  public function getPromo()
  {
  	return $this->promo;
  }
  

  /**
   * Set restaurantId
   *
   * @param bigint $restaurantId
   */
  public function setRestaurantId($restaurantId)
  {
  	$this->restaurantId = $restaurantId;
  }
  
  /**
   * Get restaurantId
   *
   * @return bigint
   */
  public function getRestaurantId()
  {
  	return $this->restaurantId;
  }
  
  /**
   * Set status
   *
   * @param ResReservationStatus $status
   */
  public function setStatus(\ResReservationStatus $status)
  {
  	$this->status = $status;
  }
  
  /**
   * Get status
   *
   * @return ResReservationStatus
   */
  public function getStatus()
  {
  	return $this->status;
  }
}