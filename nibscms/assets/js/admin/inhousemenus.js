$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addPageBtn").overlay({width:"30%",inline:true,title:"Add Category",href:"#addPageForm"});
  // Secondary Add Button
  $("button.addDishBtn").livequery(function(){
    $(this).overlay({width:"30%",inline:true,title:"Add New Dish",href:"#addDishForm"})
  });

  //checkboxes
  $("#sidebar ul.list li input[type=checkbox]").live("click",function(){
    activate_Category($(this));
    return false;
  });

  //Secondary checkboxes
  $("#main-content ul.list li input[type=checkbox]").live("click",function(){
    activate_Dish($(this));
    return false;
  });

  //Secondary Sortables
  $("#main-content ul.list.reorderable").livequery(function(){
    $(this).sortable({
      axis:"y",
      update:function(){
        NIBS.util.ajax($(this).sortable("serialize"),"admin/"+$(this).attr("rel"));
      }
    });
  })

  //See More details on click
  $("#sidebar ul.list li").live("click",function(){
    var _li=$(this);
    _li.siblings().removeClass("active");
    _li.addClass("active");
    loadCategoryDetails(_li.attr("id"));
    return false;
  });

  //Secondary See More details on click
  $("#main-content ul.list li").live("click",function(){
    var _li=$(this);
    _li.siblings().removeClass("active");
    _li.addClass("active");
    loadDishDetails(_li.attr("id"));
    return false;
  });



});

function addCategory(name,id){
  NIBS.notif.notify("success","category added");
  var html="<li id='inhousemenu_"+id+"' class='deleted'>"+
  "<a href='#'>"+
				"<span class='name icon-box'><i class=''><img src='"+base_url+"assets/images/admin/drag_handle_active.png'/></i></span>"+
        "<span class='name nameBtn' title='"+name+"'>"+name+"</span>"+
        "<span class='list_icon_btn'>"+
          "<input title='Delete Item' type='image' class='action delete' rel='inhousemenu/deleteCategory' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
          "<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"+
          "</span>"+
		"</a>"+
  "</li>";

  $("#sidebar ul.list").prepend(html);
  $("#add_page_form").reset();
  setTimeout(function(){
    $("#inhousemenu_"+id).removeClass("deleted");
  },100);
}

function addDish(name,id){
  NIBS.notif.notify("success","Dish added");
  var html="<li id='dish_"+id+"' class='deleted'>"+
  "<span class='name nameBtn' title='"+name+"'>"+name+"</span>"+
  "<span style='float:right'>"+
  "<input title='Delete Item' type='image' class='action delete' rel='inhousemenu/deleteDish' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' style='margin-top:6px; margin-right:5px'/>"+
  "<input class='main' id='checkboxsub_"+id+"' type='checkbox' />"+
  "</span>"+
  "</li>";

  $("#main-content ul.list").prepend(html);
  $("#add_dish_form").reset();
  setTimeout(function(){
    $("#dish_"+id).removeClass("deleted");
  },100);
}

function loadCategoryDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
  var id=e[2];

  $("#dContent").load(base_url+"admin/inhousemenu/viewCategory/"+id);
  $("#thirdBar").show(0);
  $("#infoPanel").empty();
  $("#category_id").val(id);
  $("#main-content").addClass("threepane");
}

function loadDishDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
  var id=e[2];
  $("#infoPanel").load(base_url+"admin/inhousemenu/viewDish/"+id);
}


function deleteCategory(inhousemenu_id){
  var el=$("#inhousemenu_"+inhousemenu_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
    $("#thirdBar").hide(0);
    $("#main-content").removeClass("threepane");
  }
  el.addClass("deleted");
  setTimeout(function(){
    el.remove()
  },500);
  NIBS.notif.notify("success","Category deleted successfully");
}

function deleteDish(dish_id){
  var el=$("#dish_"+dish_id);
  if(el.is(".active")){
    $("#infoPanel").empty();
  }
  el.addClass("deleted");
  setTimeout(function(){
    el.remove()
  },500);
  NIBS.notif.notify("success","Dish deleted successfully");
}

function activate_Category(el){
  if(el){
    var e=el.attr("id").match(/(.+)[-=_](.+)/);
    var id=e[2];
  }
  NIBS.util.ajax({id:id},"admin/inhousemenu/activateDeactivateCategory");
}

function do_activate_Category(id,val){
  $("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
}

function activate_Dish(el){
  if(el){
    var e=el.attr("id").match(/(.+)[-=_](.+)/);
    var id=e[2];
  }
  NIBS.util.ajax({id:id},"admin/inhousemenu/activateDeactivateDish");
}

function do_activate_Dish(id,val){
  $("#checkboxsub_"+id).prop("checked",val=="1"?true:false);
}


