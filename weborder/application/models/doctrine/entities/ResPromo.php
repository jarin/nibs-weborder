<?php



namespace entities;

/**
 * ResPromo
 *
 * @Table(name="res_promo")
 * @Entity
 */
class ResPromo
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $promoType
   *
   * @Column(name="promo_type", type="string", length=150, nullable=false)
   */
  private $promoType;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set promoType
   *
   * @param string $promoType
   */
  public function setPromoType($promoType)
  {
    $this->promoType = $promoType;
  }

  /**
   * Get promoType
   *
   * @return string 
   */
  public function getPromoType()
  {
    return $this->promoType;
  }
}