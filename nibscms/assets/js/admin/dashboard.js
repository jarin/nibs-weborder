$(function(){
  $("div.dashbox:not(.loaded)").livequery(function(){
    var _this=$(this),content=_this.find(".content"),
    widgetID=_this.attr("id"),
    dataurl=_this.attr("rel"),
    min=_this.find("a.minimise"),close=_this.find("a.close"),opts=_this.find("a.options"),
    optslist=_this.find("ul.options");
    if($.trim(dataurl)!=""){
      content.append("<iframe border='0' contentborder='0' src='"+base_url+dataurl+"'></iframe>");
    }
    min.bind("click",function(){
      var cls=_this.toggleClass("minimised").hasClass("minimised");
      saveWidgetPref(widgetID,"minimised",(cls==true ? 1 : 0));
      return false;
    });
    close.bind("click",function(){
      if(confirm("Are you sure you would like to hide this widget?\n\nYou can re-instate it back by using the add widget button on the top right corner")){
        saveWidgetPref(widgetID,"hidden",1);
        var par=$(this).parent(),
        id=widgetID.replace("widget_","");
        $("#availableWidgets ul").append("<li id=\"hwidget_"+id+"\" class=\"clearfix widget\" rel=\""+id+"\"><span class=\"name\">"+par.text()+"</span></li>");
        par.parent().parent().remove();
      }
      return false;
    })
    opts.bind("click",function(){
      optslist.toggleClass("showing");
      return false;
    });
    optslist.find("li").bind("click",function(){
      var newcls=$(this).attr("rel");
      optslist.removeClass("showing");
      _this.removeClass("half third two_thirds").addClass(newcls);
      saveWidgetPref(widgetID,"size",newcls);
    });
    _this.addClass("loaded");
  });

  $(document).click(function(){
    $("ul.options").removeClass("showing");
  });

  $("#widgetsMain,#widgetsRight").sortable({
    items:"div.dashbox",
    dropOnEmpty:true,
    connectWith:".widgetpane",
    placeholder:"placeholder",
    forcePlaceholderSize: true,
  }).bind("sortupdate",function(){
    NIBS.util.ajax($(this).sortable("serialize"),"admin/"+$(this).attr("rel"));
  });

  $("#widgetAdderButton").live("click",function(){
    var _this=$(this);
    var state=_this.data("menuShowing") || false;
    if(state){
      hideWidgetRepo();
      _this.removeClass("important").data("menuShowing",false);
    }else{
      showWidgetRepo();
      _this.addClass("important").data("menuShowing",true);
    }
  });

  $("#availableWidgets li").livequery(function(){
    $(this).draggable({
      opacity: 0.9,
      revert:"invalid",
      zIndex:"2700",
      helper:"clone"
    });
  });

  $("#widgetsMain,#widgetsRight").droppable({
    accept:".widget",
    activeClass: 'onDrop',
    drop:function(e,ui){
      var dropped=ui.draggable;
      var id=dropped.attr("rel");
      NIBS.util.ajax({widget_id:id,dropped_on:$(this).attr("id")},"admin/home/addWidget");
    }
  });
});
function saveWidgetPref(widget,k,v){
  var widget=(widget.split("_"))[1];
  NIBS.util.ajax({widget:widget,key:k,value:v},"admin/home/saveWidgetPreferences");
}

//--widget repo
function showWidgetRepo(){
  var widRep=$("#availableWidgets");
  widRep.removeClass("showing").show(0);
  setTimeout(function(){
    widRep.addClass("showing");
  },50);
}

function hideWidgetRepo(){
  var widRep=$("#availableWidgets");
  widRep.removeClass("showing");
  setTimeout(function(){
    widRep.hide(0);
  },300);
}