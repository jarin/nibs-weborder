<div id='main-content' class='onepane'>
        <div class='container-fluid' style=''>
          <div class='row-fluid'>
            <div class='span12'>	
			<div class=''>
				<h3 class='page-title'>Business Logo</h3><br />
				<div id='form_wizard_1' class='widget box'>
		            
		        <div class='widget-body form'>
				<div style='margin:0 auto;width:60%;text-align:center'>
				<form method='post' rel='ajaxedUpload' action='<?=base_url()?>admin/business/saveBusinessLogo' loadingDiv='loader<?=$business['id']?>' autocomplete='off' enctype='multipart/form-data'>
					<table cellpadding='4' cellspacing='0' align='center'>
						
						<tr>
							<th>Uploaded image</th>
							<td align='left' colspan='2' class='img' id='logoIcon_<?=$business['id']?>'>
  							  	<img  src="<?=base_url()?>assets/logo/<?=$business['logo']?>" style='width:100px;height:100px'  />
  							 </td>
						</tr>
						<tr><td></td></tr>
							<tr>
  							  <th align='left' valign='top'><label>Replace Image</label></th>
  							  <td align='left' colspan='2'><input type='file' name='logo' id='uploadField' style='width:100%' /></td>
  						  	</tr>
  						  	<tr><td></td></tr>
  						  	<tr><td></td></tr>
						<tr><td colspan='4'><hr></td></tr>
						<tr>
							<td align='right' colspan='4'>
							<button type='submit' class='button save'>Save Changes</button>
							<div id='loader<?=$business['id']?>' class='q_feedback'></div>
							
							<input type='hidden' name='business_id' value='<?=$business['id']?>' />
							</td>
						</tr>
					</table>
				</form></div>
			</div>
			</div></div>
			</div>
			</div>
			</div>
			</div>