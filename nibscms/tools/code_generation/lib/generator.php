<?php
class generator{

  function __construct($path,$values){
    if(trim($path)==""){
      throw new Exception("Empty Template Path");
    }
    $this->path="templates/".((substr($path,-1)!="/")?($path."/"):($path));
    if(is_array($values)){
      foreach ($values as $k => $v) {
        if($v=="false"){
          $v=false;
        }
        if($v=="true"){
          $v=true;
        }
        $this->values[$k]=$v;
      }
    }
    $this->base_name=isset($values["base_name"])?$values["base_name"]:"anonymous";
    $this->prep();
    $this->process();
  }

  function getBuiltDirectory(){
    return $this->target;
  }

  function prep(){
    if(!file_exists("build")){
      mkdir("build");
      chmod("build",0777);
    }
    $this->target="build/built_".$this->base_name."-".date("d_m_y_h_i_s")."/";
    mkdir($this->target);
    chmod($this->target,0777);
  }

  function process(){
    if(trim($this->target)==""){
      throw new Exception("Empty Target Path");
    }
    if(file_exists($this->path)){
      //process controllers
      if(file_exists($this->path."controllers")&&is_dir($this->path."controllers")){
        $path=$this->path."controllers/";

        $target_path=$this->target."controllers/";
        mkdir($target_path);
        chmod($target_path,0777);

        $dh=opendir($path);
        while(false!==($file=readdir($dh))){
          if(!is_dir($path.$file)&&substr($file,0,1)!="."){
            $t=$this->read($path.$file);
            $this->write(
              $target_path.(($file=="controller.php")?(plural($this->base_name).".php"):($file)),
              $this->parse($t)
            );
            unset($t);
          }
        }
      }

      //process models
      if(file_exists($this->path."models")&&is_dir($this->path."models")){
        $path=$this->path."models/";

        $target_path=$this->target."models/";
        mkdir($target_path);
        chmod($target_path,0777);


        $dh=opendir($path);
        while(false!==($file=readdir($dh))){
          if(!is_dir($path.$file)&&substr($file,0,1)!="."){
            $t=$this->read($path.$file);
            $this->write(
              $target_path.(($file=="model.php")?(plural($this->base_name)."_m.php"):($file)),
              $this->parse($t)
            );
            unset($t);
          }
        }
      }

      //process views
      if(file_exists($this->path."views")&&is_dir($this->path."views")){
        $path=$this->path."views/";

        $target_path=$this->target."views/";
        mkdir($target_path);
        chmod($target_path,0777);


        $dh=opendir($path);
        while(false!==($file=readdir($dh))){
          if(!is_dir($path.$file)&&substr($file,0,1)!="."){
            $t=$this->read($path.$file);
            $this->write(
              $target_path.(($file=="view.php")?(plural($this->base_name)."_template.php"):($file)),
              $this->parse($t)
            );
            unset($t);
          }
        }
      }

      //process assets/js
      if(file_exists($this->path."assets/js")&&is_dir($this->path."assets/js")){
        $path=$this->path."assets/js/";

        $target_path=$this->target."js/";
        mkdir($target_path);
        chmod($target_path,0777);


        $dh=opendir($path);
        while(false!==($file=readdir($dh))){
          if(!is_dir($path.$file)&&substr($file,0,1)!="."){
            $t=$this->read($path.$file);
            $this->write(
              $target_path.(($file=="script.js")?(plural($this->base_name).".js"):($file)),
              $this->parse($t)
            );
            unset($t);
          }
        }
      }

      //process assets/scss
      if(file_exists($this->path."assets/scss")&&is_dir($this->path."assets/scss")){
        $path=$this->path."assets/scss/";

        $target_path=$this->target."css/";
        mkdir($target_path);
        chmod($target_path,0777);


        $dh=opendir($path);
        while(false!==($file=readdir($dh))){
          if(!is_dir($path.$file)&&substr($file,0,1)!="."){
            $t=$this->read($path.$file);
            $this->write(
              $target_path.(($file=="style.scss")?(plural($this->base_name).".scss"):($file)),
              $this->parse($t)
            );
            unset($t);
          }
        }
      }
    }else{
      throw new Exception("Template Path does not Exist");
    }
  }
  private function read($template){
    return file_get_contents($template);
  }
  private function write($file,$content){
    file_put_contents($file,$content);
    chmod($file,0777);
  }
  private function parse(&$template){
    $res=preg_replace("/[\t]/","  ",$template);
    $res=preg_replace("/'(?=[^#]*#>)/","\t",$res);
    $res=implode("~_~",explode("\\'",$res));
    $res=implode("\\'",explode("'",$res));
    $res=implode("'",explode("\t",$res));
    $res=preg_replace("/<#=(.+?)#>/","',$1,'",$res);
    $res=implode("');",explode("<#",$res));
    $res=implode("array_push(\$p,'",explode("#>",$res));

    $res="extract(\$values);\$p=array();array_push(\$p,'".$res."');";

    $res.="return str_replace('~_~',\"\\'\",implode(\"\",\$p));";

    $func=create_function('$values',$res);
    return $func($this->values);
  }
}
?>