<?php

namespace entities;

/**
 * ResOrderProductAddon
 *
 * @Table(name="res_order_product_addon")
 * @Entity
 */
class ResOrderProductAddon
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResOrderChild
   *
   * @ManyToOne(targetEntity="ResOrderChild")
   * @JoinColumns({
   *   @JoinColumn(name="order_child_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $orderChild;
  
  /**
   * @var ResProduct
   *
   * @ManyToOne(targetEntity="ResProduct")
   * @JoinColumns({
   *   @JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $product;
  
  /**
   * @var ResAddon
   *
   * @ManyToOne(targetEntity="ResAddon")
   * @JoinColumns({
   *   @JoinColumn(name="addon_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $addon;
  
  /**
   * @var smallint $quantity
   *
   * @Column(name="quantity", type="smallint", nullable=false)
   */
  private $quantity;

 
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set orderChild
   *
   * @param ResOrderChild $orderChild
   */
  public function setOrderChild(\ResOrderChild $orderChild)
  {
    $this->orderChild = $orderChild;
  }

  /**
   * Get orderChild
   *
   * @return ResOrderChild 
   */
  public function getOrderChild()
  {
    return $this->orderChild;
  }
  
  /**
   * Set product
   *
   * @param ResProduct $product
   */
  public function setProduct(\ResProduct $product)
  {
      $this->product = $product;
  }
  
  /**
   * Get product
   *
   * @return ResProduct
   */
  public function getProduct()
  {
      return $this->product;
  }
    
  /**
   * Set addon
   *
   * @param ResAddon $addon
   */
  public function setAddon(\ResAddon $addon)
  {
  	$this->addon = $addon;
  }
  
  /**
   * Get addon
   *
   * @return ResAddon
   */
  public function getAddon()
  {
  	return $this->addon;
  }
  
  /**
   * Set quantity
   *
   * @param smallint $quantity
   */
  public function setQuantity($quantity)
  {
      $this->quantity = $quantity;
  }
  
  /**
   * Get quantity
   *
   * @return smallint
   */
  public function getQuantity()
  {
      return $this->quantity;
  }
}