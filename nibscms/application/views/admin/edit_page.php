<script type="text/javascript">
	tinyMCE.init({
	relative_urls : false,
	accessibility_warnings : false,
	remove_script_host : false,
	document_base_url : "<?=base_url()?>",
	mode : "none",
	extended_valid_elements:"a[name|href|target|title|onclick|rel|class]",
	theme : "advanced",
	theme_advanced_blockformats : "h2,h3,p",
	//theme_advanced_resizing : true, theme_advanced_resize_horizontal : false, theme_advanced_resizing_use_cookie : true,
	plugins : "safari,style,table,save,advimage,advlink,contextmenu,paste,directionality,visualchars,nonbreaking,xhtmlxtras,inlinepopups,fullscreen,spellchecker,media",
	theme_advanced_buttons1 : "bold,italic,underline,forecolor ,|,sup,sub,hr,|,justifyleft,justifycenter,justifyright,bullist,numlist,|,outdent,indent,link,unlink,|,anchor,charmap,image,media,|,spellchecker,|,undo,redo",
	theme_advanced_buttons2 : "tablecontrols,|,formatselect,styleselect,|,styleprops,|,pastetext,pasteword,|,|,fullscreen,code",
	theme_advanced_buttons3 : "",
	theme_advanced_styles : "Underlined Heading 2=u;Red Link=redlink",

	external_link_list_url : "<?=base_url()?>sitemap/js",

	theme_advanced_toolbar_location : "external",
	theme_advanced_toolbar_align : "center",
	theme_advanced_statusbar_location : "none",
	file_browser_callback : 'tinyBrowser',
	onchange_callback : "MCE_Changed"
	});
	tinyMCE.execCommand("mceAddControl",false,"EditableArea");
</script>

<?=$html?>
