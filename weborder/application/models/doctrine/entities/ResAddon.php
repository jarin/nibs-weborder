<?php



namespace entities;

/**
 * ResAddon
 *
 * @Table(name="res_addon")
 * @Entity
 */
class ResAddon
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="integer", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $addonName
   *
   * @Column(name="addon_name", type="string", length=150, nullable=false)
   */
  private $addonName;
  
  
  /**
   * @var string $addonImage
   *
   * @Column(name="addon_image", type="string", length=255, nullable=false)
   */
  private $addonImage;
  
  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;
  
  /**
   * @var decimal $price
   *
   * @Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $price=0;
  
  /**
   * @var decimal $takeawayPrice
   *
   * @Column(name="takeaway_price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $takeawayPrice=0;
  
  /**
   * @var decimal $webOrderPrice
   *
   * @Column(name="web_order_price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $webOrderPrice=0;
  
  /**
   * @var decimal $deliveryPrice
   *
   * @Column(name="delivery_price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $deliveryPrice=0;
  
  /**
   * @var decimal $waitingPrice
   *
   * @Column(name="waiting_price", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $waitingPrice=0;
  
  /**
   * @var bigint $restaurantId
   *
   * @Column(name="restaurant_id", type="bigint", nullable=true)
   */
  private $restaurantId;
  
  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set addonName
   *
   * @param string $addonName
   */
  public function setAddonName($addonName)
  {
    $this->addonName = $addonName;
  }

  /**
   * Get addonName
   *
   * @return string 
   */
  public function getAddonName()
  {
    return $this->addonName;
  }
  
  /**
   * Set addonImage
   *
   * @param string $addonImage
   */
  public function setAddonImage($addonImage)
  {
  	$this->addonImage = $addonImage;
  }
  
  /**
   * Get addonImage
   *
   * @return string
   */
  public function getAddonImage()
  {
  	return $this->addonImage;
  }
  
  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
      $this->isActive = $isActive;
  }
  
  /**
   * Get isActive
   *
   * @return boolean
   */
  public function getIsActive()
  {
      return $this->isActive;
  }
  
  /**
   * Set price
   *
   * @param decimal $price
   */
  public function setPrice($price)
  {
      $this->price = $price;
  }
  
  /**
   * Get price
   *
   * @return decimal
   */
  public function getPrice()
  {
      return $this->price;
  }
  
  /**
   * Set takeawayPrice
   *
   * @param decimal $takeawayPrice
   */
  public function setTakeawayPrice($takeawayPrice)
  {
      $this->takeawayPrice = $takeawayPrice;
  }
  
  /**
   * Get takeawayPrice
   *
   * @return decimal
   */
  public function getTakeawayPrice()
  {
      return $this->takeawayPrice;
  }
  
  /**
   * Set webOrderPrice
   *
   * @param decimal $webOrderPrice
   */
  public function setWebOrderPrice($webOrderPrice)
  {
      $this->webOrderPrice = $webOrderPrice;
  }
  
  /**
   * Get webOrderPrice
   *
   * @return decimal
   */
  public function getWebOrderPrice()
  {
      return $this->webOrderPrice;
  }
  
  /**
   * Set deliveryPrice
   *
   * @param decimal $deliveryPrice
   */
  public function setDeliveryPrice($deliveryPrice)
  {
      $this->deliveryPrice = $deliveryPrice;
  }
  
  /**
   * Get deliveryPrice
   *
   * @return decimal
   */
  public function getDeliveryPrice()
  {
      return $this->deliveryPrice;
  }
  
  /**
   * Set waitingPrice
   *
   * @param decimal $waitingPrice
   */
  public function setWaitingPrice($waitingPrice)
  {
      $this->waitingPrice = $waitingPrice;
  }
  
  /**
   * Get waitingPrice
   *
   * @return decimal
   */
  public function getWaitingPrice()
  {
      return $this->waitingPrice;
  }
  
  /**
   * Set restaurantId
   *
   * @param bigint $restaurantId
   */
  public function setRestaurantId($restaurantId)
  {
  	$this->restaurantId = $restaurantId;
  }
  
  /**
   * Get restaurantId
   *
   * @return bigint
   */
  public function getRestaurantId()
  {
  	return $this->restaurantId;
  }
}