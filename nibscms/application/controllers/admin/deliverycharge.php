<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deliverycharge extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		

		//controller Specific
		$this->tab="deliverysetting";		
		$this->load->model("Order_m","order");
		$this->load->model("Delivery_m","delivery");
		
	}

	function index(){
		if(_checkAuthentication($this)){
			$html="";
			$restaurant=$this->order->getweb_business($this->_userid);
			$deliveryinfos=$this->delivery->getAllDeliverySettings($restaurant[0]['id']);
			if(is_array($deliveryinfos)){
				foreach($deliveryinfos as $deliveryinfo){
					if($deliveryinfo["is_active"]==0){
						$class="off";
						$checked="";
					}else{
						$class="";
						$checked="checked";
					}
					
					
					$html.="<li id='delivery_$deliveryinfo[id]'>
					<a href='#'>
					<span class='name icon-box'><i class=''><img src='".base_url()."assets/images/admin/application_list.png'/></i></span>
					<span class='name nameBtn' title='$deliveryinfo[distance] Miles' >$deliveryinfo[distance] Miles($deliveryinfo[amount])</span>
					<span class='list_icon_btn'>
					<input title='Delete Page' type='image' class='action delete' rel='deliverycharge/delete' elId='$deliveryinfo[id]' src='".base_url()."assets/images/admin/icon_trash.png' border='0' />
					<input class='main' id='checkboxmain_$deliveryinfo[id]' type='checkbox' $checked />
					</span>
					</a></li>";
				}
			}
			$temp="";
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
			$this->load->view("admin/delivery_template",array(
				"pagename"=>"Delivery Charge Setting",
				"list"=>$html,
				"restaurant"=>$restaurant[0]['id']
			));
			$this->load->view("admin/admin_foot");
		}
	}

	function loadDetails($id=""){
		if(_checkAuthentication($this)){
			if(trim($id)==""){
				echo '<span style="color:red">Error: </span>Business ID Not defined... Please reload page';
					exit;
			}
			$deliveryInfo=$this->delivery->getdelivery($id);
			
			if(!isset($deliveryInfo["id"]) || !$deliveryInfo["id"]>0){
				echo '<span style="color:red">Error: Invalid Delivery charge </span>';
				exit;
			}			

			$html="<div class=''>
				<h3 class='page-title'>Delivery charge Details</h3><br />
				<div id='form_wizard_1' class='widget box'>
		            <div class='widget-title'>
		                <h4><i class='icon-reorder'></i>Delivery charge Form</h4>
		            </div>
		        <div class='widget-body form'>
				<div style='margin:0 auto;width:100%;text-align:center'><form method='post' rel='ajaxedUpload' action='".base_url()."admin/deliverycharge/save' loadingDiv='loader$deliveryInfo[id]' autocomplete='off' enctype='multipart/form-data'>
					<table cellpadding='4' cellspacing='0' align='center' style='width:90%'>
						
						<tr>
							<th align='left' >Distance (Miles)</th>
							<td ><input type='text' name='distance' id='edit_distance_input' value=\"$deliveryInfo[distance]\" /></td>
						</tr>
						<tr>
							<th align='left' >Delivery Charge</th>
							<td ><input type='text' name='amount' id='edit_amount_input' value=\"$deliveryInfo[amount]\" /></td>
						</tr>
						
						<tr >
							<th align='left' >Is Active</th>
							<td><input type='checkbox'  name='is_active' value='1' ". (($deliveryInfo['is_active'] ==1)? 'checked':'') ." /></td>
						</tr>
							<tr >
							<th align='left'>Do not take Order greater then this distance</th>
							<td ><input type='checkbox'  name='take_order' value='1' ". (($deliveryInfo['is_not_take_order'] ==1)? 'checked':'') ."/></td>
						</tr>
					

						<tr>
							<td align='right' colspan='4'>
							<button type='submit' class='button save'>Save Changes</button>
							<div id='loader$deliveryInfo[id]' class='q_feedback'></div>
							<input type='hidden' name='restaurant' value='$deliveryInfo[restaurant_id]' />
							<input type='hidden' name='deliveryId' value='$deliveryInfo[id]' />
							</td>
						</tr>
					</table>
				</form></div>
			</div>
			</div></div>

			";

			echo $html;
		}
	}


	function save(){
		if(_checkAuthentication($this)){
			
			$distance = $this->input->post("distance");
			$amount = $this->input->post("amount");
			$is_active = $this->input->post("is_active");
			$takeOrder = $this->input->post("take_order");
			$restaurant = $this->input->post("restaurant");
			$id=$this->input->post("deliveryId");
			
			
			if(empty($distance)){
				echo '{success:"error",msg:"Empty distance is not allowed..."}';				
				exit;
			}
			if(empty($amount)){
				echo '{success:"error",msg:"Empty Delivery Charge is not allowed..."}';
				exit;
			}
				
		
			
			$data=array(
				"distance" => $distance,
				"amount" => $amount,
				"is_active" => (($is_active !='')?$is_active:0),
				"is_not_take_order" => (($takeOrder !='')?$takeOrder:0),
				"restaurant_id" => $restaurant
			);

			$disEx = $this->delivery->countBy(array('distance'=>$distance, 'restaurant_id'=>$restaurant,'id !='=>$id));
			
			if($disEx > 0){
				echo '{success:"error",msg:"You have added this distance before. Try another one"}';
				exit;
			} else { 
				if($takeOrder !=''){
					$this->delivery->updateDeliveryNotTake($restaurant);
				}
				if($this->delivery->save($id, $data)){
					
						echo '{"success":"success","successFunction":function(){
							$("#delivery_'.$id.'").click();
							$("#delivery_'.$id.' span.nameBtn").html("'.$distance.' Miles('.$amount.')");
							NIBS.notif.notify("success","Delivery Charge details saved");
									
						}}';
						exit;
				}else{
					echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update Delivery Charge information due to a database error...")
					}}';
					exit;
				}
			}
			
		}
	}

	function add(){
		if(_checkAuthentication($this)){
			
			$distance = $this->input->post("distance");
			$amount = $this->input->post("amount");
			$is_active = $this->input->post("is_active");
			$takeOrder = $this->input->post("take_order");
			$restaurant = $this->input->post("restaurant");
			
			
			
			if(empty($distance)){
				echo '{"success":"error","successFunction":function(){
					alert("Empty distance is not allowed...");
				}}';
				exit;
			}
			if(empty($amount)){
				echo '{"success":"error","successFunction":function(){
					alert("Empty delivery charge is not allowed...");
				}}';
				exit;
			}
			
			$disEx = $this->delivery->countBy(array('distance'=>$distance, 'restaurant_id'=>$restaurant));
			if($disEx > 0){
				echo '{"success":"error","successFunction":function(){
					alert("You have added this distance before. Try another one");	
				}}';
				exit;
			} else {
				$ins=$this->delivery->create(
						array(
								"distance" => $distance,
								"amount" => $amount,
								"is_active" => (($is_active !='')?$is_active:0),
								"is_not_take_order" => (($takeOrder !='')?$takeOrder:0),
								"restaurant_id" => $restaurant
						));
				if($ins){
				
					echo '{"success":"success",successFunction:function(){
					addDelivery("'.$distance.' Miles ('.$amount.')'.'","'.$ins.'");
				}}';
					exit;
				
				}else{
					echo '{"success":"error",successFunction:function(){
					alert("Could not add due to a database error");
					}}';
					exit;
				}
			}
		}
	}

	function delete(){
		if(_checkAuthentication($this)){
			$id=$this->input->post('id');
			if(trim($id)==""){
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove delivery charge due to a database error,\\nPlease reload page");
					}}';
					exit;
			}
			
			if($this->delivery->delete($id)){


				echo '{"success":"success",successFunction:function(){
					deleteCharge("'.$id.'");
					}}';
					exit;
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove business due to a database error");
					}}';
					exit;
			}
		}
	}

	function activateCharge(){
		if(_checkAuthentication($this)){
			$id=$this->input->post('id');
			$res=$this->delivery->get($id);
			if($res["is_active"]=="0"){
				$new_value="1";
			}else{
				$new_value="0";
			}
			if($this->delivery->save($id,array("is_active"=>$new_value))){
				echo '{"success":"success",successFunction:function(){
					do_activate_page("'.$id.'","'.$new_value.'");
					}}';
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not toggle visiblity due to server error...<br />Please try again");
					}}';
			}
		}
	}
	
	

}


?>
