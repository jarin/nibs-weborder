<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autocomplete extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->data['page_title'] = "Autocomplete";
		$this->load->helper('date');
	}
	
  
  public function States(){
		$data = $this->input->post(NULL, TRUE);
		$retval = array();
		$state_info = $this->common_m->getByDql("SELECT t FROM entities\\ResState t WHERE t.stateName LIKE '".$data['query']."%' ORDER BY t.stateName");
		foreach($state_info as $c){
			$p = new stdClass();
			$p->value = $c['stateName'];
			$p->data = $c;
			$retval[] = $p;
		}
		echo json_encode(array("suggestions"=>$retval));
	}
  
  public function Countries(){
		$data = $this->input->post(NULL, TRUE);
		$retval = array();
		$country_info = $this->common_m->getByDql("SELECT t FROM entities\\ResCountry t WHERE t.countryName LIKE '".$data['query']."%' ORDER BY t.countryName");
		foreach($country_info as $c){
			$p = new stdClass();
			$p->value = $c['countryName'];
			$p->data = $c;
			$retval[] = $p;
		}
		echo json_encode(array("suggestions"=>$retval));
	}
  
  public function Towns(){
		$data = $this->input->post(NULL, TRUE);
		$retval = array();
		$city_info = $this->common_m->getByDql("SELECT t FROM entities\\ResCity t WHERE t.cityName LIKE '".$data['query']."%' ORDER BY t.cityName");
		foreach($city_info as $c){
			$p = new stdClass();
			$p->value = $c['cityName'];
			$p->data = $c;
			$retval[] = $p;
		}
		echo json_encode(array("suggestions"=>$retval));
	}
	

	

}