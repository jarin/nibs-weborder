$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // See More details on click
  $("#thirdBar ul.comp_multilineListView li").live("click",function(){
    var _li=$(this);
    _li.siblings().removeClass("active");
    _li.addClass("active");
    loadEntryDetails(_li.attr("id"));
    return false;
  });

  //filters
  $("#findByStatus").click(function(){
    var par=$(this).parent().parent().prev("li").find("ul"),
    ids=[];
    par.find("input:checked").each(function(){
      ids.push($(this).val());
    });
    if(ids.length > 0){
      $("#thirdBar ul.comp_multilineListView").load(base_url+"admin/shop_orders/filterOrders/",{ids:ids.join(",")});
    }else{
      alert("Please tick one or more statuses")
    }
  });
  $("#findByID").click(function(){
    var inp=$(this).parent().parent().prev("li").find("input"),
    val=inp.val();
    if($.trim(val)!=""){
      $("#thirdBar ul.comp_multilineListView").load(base_url+"admin/shop_orders/searchOrders/",{q:val});
    }else{
      alert("Please type something to search for");
    }
  });

});


function loadEntryDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
  var id=e[2];
  $("#innerTabs").show(0);
	var idx=$("#innerTabs li.active").index();
	$("#mainContent").addClass("hastabs");
  $("#dContent").load(base_url+"admin/shop_orders/viewOrder/"+id,function(){
    //sticky tabs
    $("#innerTabs li").eq(idx).click();
  });

}