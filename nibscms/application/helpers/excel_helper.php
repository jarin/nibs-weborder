<?php
function downloadSample(&$CI,$type) {
	switch ($type) {
		case "Products":
			$headers = array(
			"Product Name" => "M",
			"Category" => "M",
			"Description" => "M",
			"Prep Location" => "M",
			"Dine In Price" => "M",
			"Delivery Price" => "M",
			"TakeAway Price"=>"M",
			"WebOrder Price"=>"M",
			"Waiting Price"=>"M",
			"Is Taxable" => "O",
			"Is Tax Inclusive" => "O",
			"Is Favourite" => "O",
			"Is Bar Favourite" => "O",
			"Is Stockable" => "O",
			"Stock" => "O",
			"Is Active" => "O"
					);
			$title = "Products";
			break;


		default:
			$headers = array("Template Not Found" => "M");
			$title = "Error";
			break;
	}

	$CI->excel_lib->setActiveSheetIndex(0);
	$sheet = $CI->excel_lib->getActiveSheet();
	$sheet->setTitle($title);
	$i = "A";
	$width = array();
	foreach ($headers as $key => $val) {
		$sheet->getStyle($i . '1')->getFont()->setBold(true);
		if ($val == "M") {
			$sheet->getStyle($i . '1')->getFont()->getColor()->applyFromArray(array("rgb" => "FF0000"));
		}
		$sheet->setCellValue($i . '1', $key);
		if ((isset($width[$i]) && $width[$i] < strlen($key)) || !isset($width[$i])) {
			$width[$i] = strlen($key);
		}
		$sheet->getStyle($i . '1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$i++;
	}
	$endCell = $i;

	for ($f = "A"; $f !== $endCell; $f++) {
		/*
		 if($width[$f]<10){
		$sheet->getColumnDimension($f)->setWidth(10);
		}else{
		$sheet->getColumnDimension($f)->setWidth((int) $width[$f] * 1.09);
		}
		*/
		$sheet->getColumnDimension($f)->setAutoSize(true);
	}

	/*
	 if($type=="Staff"){
	$sheet->getStyle('E')->getNumberFormat()->setFormatCode('yyyy-mm-dd');
	$sheet->getStyle('F')->getNumberFormat()->setFormatCode('yyyy-mm-dd');
	}
	*/

	$sheet->setSelectedCell('A2');

	$CI->excel_lib->getProperties()->setCreator("Nibs Solutions Ltd.");
	$CI->excel_lib->getProperties()->setLastModifiedBy("Nibs Solutions Ltd.");
	$CI->excel_lib->getProperties()->setTitle($title);
	$CI->excel_lib->getProperties()->setSubject("List");
	$CI->excel_lib->getProperties()->setDescription("This is a list downloaded from Nibs Solutions Ltd.");

	$filename = $title . (($title != 'Error') ? 'Template' : '') . ".xls";
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="' . $filename . '"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($CI->excel_lib, 'Excel5');
	$objWriter->save('php://output');
}


function bulkProducts(&$CI,$filepath,$restarant_id) {
	//Read Excel workbook
	
	try {
		$inputFileType = PHPExcel_IOFactory::identify($filepath);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($filepath);
	} catch (Exception $e) {
		
		return false;
	}
	
	//Get worksheet dimensions
	$sheet = $objPHPExcel->getSheet(0);
	$highestRow = $sheet->getHighestRow();
	$highestColumn = $sheet->getHighestColumn();

	$header = $sheet->rangeToArray('A1:' . $highestColumn . '1', NULL, TRUE, FALSE);
	$responseColumn = 15; // 15 is Number of Columns
	$header[0][$responseColumn] = "Response";
	$highestColumn++;

	//Open the Server Response File
	$CI->excel_lib->setActiveSheetIndex(0);

	$CI->excel_lib->getProperties()->setCreator("Nibs Solutions Ltd.");
	$CI->excel_lib->getProperties()->setLastModifiedBy("Nibs Solutions Ltd.");
	$CI->excel_lib->getProperties()->setTitle("Products");
	$CI->excel_lib->getProperties()->setSubject("List");
	$CI->excel_lib->getProperties()->setDescription("This is a list downloaded from Nibs Solutions Ltd.");

	$sheet_new = $CI->excel_lib->getActiveSheet();
	$sheet_new->setTitle($sheet->getTitle());
	$sheet_new->fromArray($header, null, 'A1');
	$sheet_new->getStyle('A1:' . $highestColumn . '1')->getFont()->setBold(true);
	$sheet_new->getStyle('A1:' . $highestColumn . '1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	//Loop through each row of the worksheet in turn
	for ($row = 2; $row <= $highestRow; $row++) {
		//Read a row of data into an array
		$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
		$errorFound = false;

		if (is_null($rowData[0][0])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Product Name Can not be left Blank.";
		} else if (strlen($rowData[0][0]) < 3) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Product Name should have at least 3 Characters.";
		} else if (is_null($rowData[0][1])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Category Can not be left Blank.";
		} else if (is_null($rowData[0][2])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Description Can not be left Blank.";
		} else if (strlen($rowData[0][2]) < 10) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Description should have at least 10 Characters.";
		} else if (is_null($rowData[0][3])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Prep Location Can not be left Blank.";
		} else if (is_null($rowData[0][4])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Dine In Price Can not be left Blank.";
		} else if (!is_numeric($rowData[0][4])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Dine In Price contains only Numeric Value.";
		} else if (is_null($rowData[0][5])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Delivery Price Can not be left Blank.";
		} else if (!is_numeric($rowData[0][5])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Delivery Price contains only Numeric Value.";
		} else if (is_null($rowData[0][6])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] TakeAway Price Can not be left Blank.";
		} else if (!is_numeric($rowData[0][6])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] TakeAway Price contains only Numeric Value.";
		} else if (is_null($rowData[0][7])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] WebOrder Price Can not be left Blank.";
		} else if (!is_numeric($rowData[0][7])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] WebOrder Price contains only Numeric Value.";
		} else if (is_null($rowData[0][8])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Waiting Price Can not be left Blank.";
		} else if (!is_numeric($rowData[0][8])) {
			$errorFound = true;
			$rowData[0][$responseColumn] = "[ERROR] Waiting Price contains only Numeric Value.";
		}

		if ($errorFound) {
			$sheet_new->fromArray($rowData, null, 'A' . $row);
			$sheet_new->getStyle($highestColumn . $row)->getFont()->getColor()->applyFromArray(array("rgb" => "FF0000"));
		} else {
			if (is_null($rowData[0][9]) || $rowData[0][9] == '' || strtolower($rowData[0][9]) == 'no') {
				$rowData[0][9] = "0";
			} else {
				$rowData[0][9] = "1";
			}

			if (is_null($rowData[0][10]) || $rowData[0][10] == '' || strtolower($rowData[0][10]) == 'no') {
				$rowData[0][10] = "0";
			} else {
				$rowData[0][10] = "1";
			}

			if (is_null($rowData[0][11]) || $rowData[0][11] == '' || strtolower($rowData[0][11]) == 'no') {
				$rowData[0][11] = "0";
			} else {
				$rowData[0][11] = "1";
			}

			if (is_null($rowData[0][12]) || $rowData[0][12] == '' || strtolower($rowData[0][12]) == 'no') {
				$rowData[0][12] = "0";
			} else {
				$rowData[0][12] = "1";
			}

			if (is_null($rowData[0][13]) || $rowData[0][13] == '' || strtolower($rowData[0][13]) == 'no') {
				$rowData[0][13] = "0";
			} else {
				$rowData[0][13] = "1";
			}
			$CI->load->helper('date');
			$errorFound = FALSE;
			$catAry = explode("=>", $rowData[0][1]);
			if(count($catAry) > 1){
				//Find Parent Cat ID
				$CI->import->tablename=$CI->import->category;
				$parent_cat =  $CI->db->get_where($CI->import->category, array("category_name" => trim($catAry[0]),'restaurant_id'=>$restarant_id))->row_array();
				
				if(count($parent_cat) > 0){
					$cat = $CI->db->get_where($CI->import->category, array("parent_id" =>$parent_cat['id'], "category_name" => trim($catAry[1]),'restaurant_id'=>$restarant_id))->result_array();
					
					if(count($cat) > 0){
						$cat = $cat[0]['id'];
					}else{
						$CI->import->tablename=$CI->import->category;
						$cat = $CI->import->create(array(
							"category_name" => trim($catAry[1]),
							"parent_id" =>$parent_cat['id'],
							"description" => '',
							"top_color" => '#000000',
							"bottom_color" => '#000000',
							"is_active" => 1,
							"last_update" => mdate("%Y-%m-%d %H:%i:%s"),
							"is_bar" => 0,
							'restaurant_id'=>$restarant_id
							));
					}
				}else{
					$CI->import->tablename=$CI->import->category;
					$cat = $CI->import->create(array(
						"category_name" => trim($catAry[0]),
						"description" => '',
						"top_color" => '#000000',
						"bottom_color" => '#000000',
						"is_active" => 1,
						"last_update" => mdate("%Y-%m-%d %H:%i:%s"),
						"is_bar" => 0,
						'restaurant_id'=>$restarant_id
						));
				}
			}else{
				
				$cat =$CI->db->get_where($CI->import->category, array("category_name" => trim($catAry[0]), 'restaurant_id'=>$restarant_id))->row_array();
				
				if(count($cat) > 0){
					$cat = $cat['id'];
				}else{
					$CI->import->tablename=$CI->import->category;
					$cat = $CI->import->create(array(
						"category_name" => trim($catAry[0]),
						"description" => '',
						"top_color" => '#000000',
						"bottom_color" => '#000000',
						"is_active" => 1,
						"last_update" => mdate("%Y-%m-%d %H:%i:%s"),
						"is_bar" => 0,
						'restaurant_id'=>$restarant_id
						));
				}
			}
			
			$CI->import->tablename=$CI->import->prep_location;
			$pl = $CI->import->getRowBy("prep_location_name",$rowData[0][3]);
			
			

			if(!$errorFound){
				$result = $CI->db->get_where($CI->import->product, array("product_name" => $rowData[0][0], "category_id" => $cat))->result_array();
				if(strtolower($rowData[0][15]) == 'yes'){
					$active = 1;
				} else {
					$active = 0;
				}
				if (count($result) > 0) {
						$CI->import->tablename=$CI->import->product;
						$CI->import->save($result[0]['id'],array(
								"description" => $rowData[0][2],
								"price" => $rowData[0][4],
								"delivery_price" => $rowData[0][5],
								"takeaway_price" => $rowData[0][6],
								"web_order_price" => $rowData[0][7],
								"waiting_price" => $rowData[0][8],
								"prep_location_id" => $pl['id'],
								"is_taxable" => $rowData[0][9],
								"is_tax_inclusive" => $rowData[0][10],
								"is_favourite" => $rowData[0][11],
								"is_bar_favourite" => $rowData[0][12],
								"is_active" => $active,
								"long_name" => $rowData[0][0],
								"last_update" => mdate("%Y-%m-%d %H:%i:%s"),
								'restaurant_id'=>$restarant_id
								));
					
						$rowData[0][$responseColumn] = "This Product is already Added under this Category. Update OK.";
						$sheet_new->fromArray($rowData, null, 'A' . $row);
						$sheet_new->getStyle($highestColumn . $row)->getFont()->getColor()->applyFromArray(array("rgb" => "00FF00"));
					
					
					
				} else {
					$so=$CI->db->query("select max(sequence_number) as sequence_number from res_product where restaurant_id=$restarant_id")->row_array();
					
					$CI->import->tablename=$CI->import->product;
					$result = $CI->import->create(array(
							"product_name" => $rowData[0][0],
							"description" => $rowData[0][2],
							"price" => $rowData[0][4],
							"delivery_price" => $rowData[0][5],
							"takeaway_price" => $rowData[0][6],
							"web_order_price" => $rowData[0][7],
							"waiting_price" => $rowData[0][8],
							"category_id" => $cat,
							"prep_location_id" => $pl['id'],
							"is_taxable" => $rowData[0][9],
							"is_tax_inclusive" => $rowData[0][10],
							"is_favourite" => $rowData[0][11],
							"is_bar_favourite" => $rowData[0][12],
							"is_active" => $active,
							"sequence_number"=>$so['sequence_number']+1,
							"long_name" => $rowData[0][0],
							"last_update" => mdate("%Y-%m-%d %H:%i:%s"),
							'restaurant_id'=>$restarant_id));
					if (is_numeric($result)) {
						$rowData[0][$responseColumn] = "Insertion OK.";
						$sheet_new->fromArray($rowData, null, 'A' . $row);
						$sheet_new->getStyle($highestColumn . $row)->getFont()->getColor()->applyFromArray(array("rgb" => "00FF00"));
					} else {
						$rowData[0][$responseColumn] = "[ERROR] Database Error.";
						$sheet_new->fromArray($rowData, null, 'A' . $row);
						$sheet_new->getStyle($highestColumn . $row)->getFont()->getColor()->applyFromArray(array("rgb" => "FF0000"));
					}
				}
			}
		}
	}

	$highestColumn++;
	for ($f = "A"; $f !== $highestColumn; $f++) {
		$sheet_new->getColumnDimension($f)->setAutoSize(true);
	}
	$sheet_new->setSelectedCell('A' . $row);

	$ary = explode("/", $filepath);

	$objWriter = PHPExcel_IOFactory::createWriter($CI->excel_lib, 'Excel5');
	$objWriter->save('downloads/xls/' . $ary[1]);
	@unlink($filepath);

	return 'downloads/xls/' . $ary[1];
}