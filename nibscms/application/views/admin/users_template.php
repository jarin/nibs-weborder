<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/users.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/users.js"></script>


<script type="text/tmpl" id="defaultContent" charset="utf-8">
	<h3><?=$pagename?></h3>
	<div class="initPage">
		<div>
		  <p>Click on the list of users on the left to edit....</p>
		  <p>To delete a user use the <img src="<?=$baseUrl?>assets/images/admin/icon_trash.png" /> delete icon...</p>
		  <p>To add an user, click on the <img src="<?=$baseUrl?>assets/images/admin/icon_add.png" /> Plus button...</p>
		</div>
	</div>
</script> 

<div id="thirdBar" style="display:none"></div>


<div id="main-content" class=''>
  	<div class="container-fluid">
	    <div id="page" class='site-settings'>
	    	<div class="hidden">
		      <div id="addUserForm">
		       <form id='add_user_form' method='post' rel='ajaxed' action='<?=$baseUrl?>admin/users/addUser' loadingDiv='loader_add_user' formType="adder">
					<table cellpadding='4' cellspacing='0' align='center'>
						<tr>
							<th align='left' >Username</th>
							<td ><input type='text' name='username' id='add_username_input' /></td>
						</tr>
						<tr>
							<th align='left' >User Level</th>
							<td ><select name='userlevel'>
								<option value='managed'>Managed User</option>
								<option value='admin'>Administrator</option>
							</select></td>
						</tr>
						<tr>
							<th align='left' >Password</th>
							<td ><input type='password' name='password' /></td>
						</tr>
						<tr>
							<th align='left' >Retype Password</th>
							<td ><input type='password' name='re_password' id='add_re_password_input' /></td>
						</tr>
						<tr><td colspan='2'><hr class='hr' /></td></tr>
						<td align='right' colspan='2'><button class="button add" type='submit'>Add user</button><div id='loader_add_user'  class='q_feedback'></div></td> </tr>
					</table>
				</form>
		      </div>
	    	</div>
	    	
	      	<div class="row-fluid" id="dContent">
		 	</div>
	  	</div>
	</div>
</div>


<div id="sidebar" class="nav-collapse collapse">
	<div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
	<div id="sidebarHeading">
		<div class="headertoolbar">
			<span><i class='icon-user'></i>
				<em>Users</em>
			</span>

	  		<div id="searchCount">0 Filtered Results</div> 
			<button class="addUserBtn" title="Add User" style=''>
				<img src="<?=$baseUrl?>assets/images/admin/icon_add.png" style='margin-top:-6px'/>
			</button>
		</div>
  	</div> 
  	<ul class="sidebar-menu list">
   	 <?=$list1?>
  	</ul>
  	<div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>