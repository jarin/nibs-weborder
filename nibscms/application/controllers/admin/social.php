<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="social";
		$this->load->model("activity_m","activity");
		$this->load->model("Business_m","business");
		$this->load->model("social_m","social");
		
	}
	function index(){		
		if(_checkAuthentication($this)){
			$this->tab = 'social';
			$business=$this->business->getBusinesByUser($this->session->userdata('uid'));			
			if(!isset($business["id"]) || !$business["id"]>0){
				echo '<span style="color:red">Error: Invalid Business</span>';
				exit;
			}
			$social_info = $this->social->infobyBid($business["id"]);
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
			$this->load->view("admin/business_social",array("social"=>$social_info,"business_id"=>$business["id"]));
			$this->load->view("admin/admin_foot");
		}	
	}

	function saveinfo(){
		if(_checkAuthentication($this)){
			$id=$this->input->post("social_id");	
			


			$data=array(
				"fb_url" => $this->input->post("fb_url"),	
				"twit_url" => $this->input->post("twitter_url"),	
				"google_url" => $this->input->post("google_plus"),	
				"linkedin_url" => $this->input->post("linkedin"),	
				"pinterest_url" => $this->input->post("pinterest"),	
				"restaurant_id" => $this->input->post("b_id")		
				);
			$message='';
			if(!empty($id)){
				if($this->social->save($id,$data)){
					$message='Successfully Updated';

				}else{
					$message='There is a problem in database';
				}

			}
			else{
				if($this->social->create($data)){
					$message='Successfully Saved';

				}else{
					$message='There is a problem in database';
				}
			}
			   $this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
				$this->load->view("admin/status_template",array(
				"pagename"=>"Social Information",
				"message"=>$message
				));
				$this->load->view("admin/admin_foot");
		}
	}

}


?>
