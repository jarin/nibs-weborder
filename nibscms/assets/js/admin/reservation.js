$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);



  // See More details on click
  $("#sidebar ul.list li").live("click",function(){
  	var _li=$(this);
  	_li.siblings().removeClass("active");
  	_li.addClass("active");
  	loadDetails(_li.attr("id"));
  	return false;
  });

	//checkbox states
	$("#thirdBar input[type=checkbox]:not(.main_cb)").live("click",function(){
		determineMainCBState(this);
	});
});

function replaceImage(id){
	$("#"+id).find("img").attr("src","");
}

function loadDetails(id){
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#dContent").load(base_url+"admin/reservation/loadDetails/"+id);
	
	$("#thirdBar").show(0);
	$("#main-content").addClass("threepane");
	$("#thirdBar").html("<div style='text-align:center;margin-top:40px'>" +
			"<button class='button edit secondary' onclick='loadSendSms(\"reservation_"+id+"\", \"confirm\");return false;'>Send SMS</button><div style='height:20px;'></div>" +
			"<button class='button edit secondary' onclick='loadPermissions(\"reservation_"+id+"\");return false;'>Send Email</button><div style='height:20px;'></div>" +
			"<button class='button edit secondary' onclick='loadPermissions(\"reservation_"+id+"\");return false;'>Send Both</button><div style='height:20px;'></div>"+
			"<button class='button edit secondary' onclick='loadSendSms(\"reservation_"+id+"\", \"cancel\");return false;'>Cancel</button><div style='height:20px;'></div>"+
			"<button class='button edit secondary' onclick='loadDeleteReservation(\"reservation_"+id+"\");return false;'>Delete</button><div style='height:20px;'></div>"+
					"</div>");
	
}

function loadSendSms(id,type){
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	
	NIBS.util.ajax({id:id,type:type},"admin/sms/sendReservationConfirmation");	
}

function loadDeleteReservation(id){
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	
	NIBS.util.ajax({id:id},"admin/reservation/deleteReservation");	
}

function deleteReservation(id){
	var el=$("#reservation_"+id);
	if(el.is(".active")){
		$("#dContent").html($("#defaultContent").html());
		$("#thirdBar").hide(0);
		$("#main-content").removeClass("threepane");
	}
	el.addClass("deleted");
	setTimeout(function(){
		el.remove()
	},500);
	NIBS.notif.notify("success",el.find("span").text()+" removed successfully");

}

