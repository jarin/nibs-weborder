<?php



namespace entities;

/**
 * ResTable
 *
 * @Table(name="res_table")
 * @Entity
 */
class ResTable
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $tableNumber
   *
   * @Column(name="table_number", type="string", length=45, nullable=false)
   */
  private $tableNumber;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;
  
  /**
   * @var boolean $holdTable
   *
   * @Column(name="hold_table", type="boolean", nullable=false)
   */
  private $holdTable=FALSE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResFloor
   *
   * @ManyToOne(targetEntity="ResFloor")
   * @JoinColumns({
   *   @JoinColumn(name="floor_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $floor;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $user;

  /**
   * @var ResStatus
   *
   * @ManyToOne(targetEntity="ResStatus")
   * @JoinColumns({
   *   @JoinColumn(name="status_id", referencedColumnName="id", onDelete="RESTRICT", onUpdate="CASCADE")
   * })
   */
  private $status;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var ResPrepLocation
   *
   * @ManyToOne(targetEntity="ResPrepLocation")
   * @JoinColumns({
   *   @JoinColumn(name="prep_location_id", referencedColumnName="id", onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $prepLocation;
  
  /**
   * @var smallint $sequenceNumber
   *
   * @Column(name="sequence_no", type="smallint", nullable=false)
   */
  private $sequenceNumber;
  

  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set tableNumber
   *
   * @param string $tableNumber
   */
  public function setTableNumber($tableNumber)
  {
    $this->tableNumber = $tableNumber;
  }

  /**
   * Get tableNumber
   *
   * @return string 
   */
  public function getTableNumber()
  {
    return $this->tableNumber;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }
  
  /**
   * Set holdTable
   *
   * @param boolean $holdTable
   */
  public function setHoldTable($holdTable)
  {
      $this->holdTable = $holdTable;
  }
  
  /**
   * Get holdTable
   *
   * @return boolean
   */
  public function getHoldTable()
  {
      return $this->holdTable;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set floor
   *
   * @param ResFloor $floor
   */
  public function setFloor(\ResFloor $floor)
  {
    $this->floor = $floor;
  }

  /**
   * Get floor
   *
   * @return ResFloor
   */
  public function getFloor()
  {
    return $this->floor;
  }

  /**
   * Set user
   *
   * @param ResUser $user
   */
  public function setUser(\ResUser $user)
  {
    $this->user = $user;
  }

  /**
   * Get user
   *
   * @return ResUser 
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Set status
   *
   * @param ResStatus $status
   */
  public function setStatus(\ResStatus $status)
  {
    $this->status = $status;
  }

  /**
   * Get status
   *
   * @return ResStatus 
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
  
  /**
   * Set prepLocation
   *
   * @param ResPrepLocation $prepLocation
   */
  public function setPrepLocation(\ResPrepLocation $prepLocation)
  {
  	$this->prepLocation = $prepLocation;
  }
  
  /**
   * Get prepLocation
   *
   * @return ResPrepLocation
   */
  public function getPrepLocation()
  {
  	return $this->prepLocation;
  }
  
  /**
   * Set sequenceNumber
   *
   * @param smallint $sequenceNumber
   */
  public function setSequenceNumber($sequenceNumber)
  {
  	$this->sequenceNumber = $sequenceNumber;
  }
  
  /**
   * Get sequenceNumber
   *
   * @return smallint
   */
  public function getSequenceNumber()
  {
  	return $this->sequenceNumber;
  }
}