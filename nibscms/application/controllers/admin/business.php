<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Business extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");

		//controller Specific
		$this->tab="business";
		$this->load->model("activity_m","activity");
		$this->load->model("Business_m","business");
		
		$this->replace = 'nibscms';
		$this->replace_word = 'weborder';
		
	}

	function index(){
		if(_checkAuthentication($this)){
			$Businessshtml="";
			$Business=$this->business->getAllbusines();
			if(is_array($Business)){
				foreach($Business as $business){
					if($business["is_active"]==0){
						$class="off";
						$checked="";
					}else{
						$class="";
						$checked="checked";
					}
					
					
					$Businessshtml.="<li id='business_$business[id]'>
					<a href='#'>
					<span class='name icon-box'><i class=''><img src='".base_url()."assets/images/admin/application_list.png'/></i></span>
					<span class='name nameBtn' title='$business[name]'>$business[name]</span>
					<span class='list_icon_btn'>
					<input title='Delete Page' type='image' class='action delete' rel='business/deleteBusiness' elId='$business[id]' src='".base_url()."assets/images/admin/icon_trash.png' border='0' />
					<input class='main' id='checkboxmain_$business[id]' type='checkbox' $checked />
					</span>
					</a></li>";
				}
			}
			$temp="";
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
			$this->load->view("admin/business_template",array(
				"pagename"=>"Business Manager",
				"list1"=>$Businessshtml
			));
			$this->load->view("admin/admin_foot");
		}
	}

	function loadDetails($id=""){
		if(_checkAuthentication($this)){
			if(trim($id)==""){
				echo '<span style="color:red">Error: </span>Business ID Not defined... Please reload page';
					exit;
			}
			$business=$this->business->getBusines($id);
			$baseUrl=base_url();
			if(!isset($business["id"]) || !$business["id"]>0){
				echo '<span style="color:red">Error: Invalid Business</span>';
					exit;
			}			
		//	$html='<link href="'.$baseUrl.'assets/css/admin/jquery-ui/jquery.ptTimeSelect.css" rel="stylesheet" media="screen">';
		//	$html.'<script src="'.$baseUrl.'assets/js/admin/jquery-ui/jquery.ptTimeSelect.js"></script>';
		//	$html.='<script type="text/javascript" src="'.$baseUrl.'assets/js/admin/profileEdit.js"></script>';
			$html="<div class=''>
				<h3 class='page-title'>Business Details</h3><br />
				<div id='form_wizard_1' class='widget box'>
		            <div class='widget-title'>
		                <h4><i class='icon-reorder'></i>Business Form</h4>
		            </div>
		        <div class='widget-body form'>
				<div style='margin:0 auto;width:100%;text-align:center'><form method='post' rel='ajaxedUpload' action='".base_url()."admin/business/saveBusiness' loadingDiv='loader$business[id]' autocomplete='off' enctype='multipart/form-data'>
					<table cellpadding='4' cellspacing='0' align='center' style='width:90%'>
						<tr>
							<th align='left' >Business Name</th>
							<td ><input type='text' name='businessname' value=\"$business[name]\" id='edit_businessname_input' 
							 class='stubSenders'
							stubReceiver='edit_pagesurl_input'
							stubStatus='edit_urlReturnValue'
							stubUrl='admin/business/checkUrlDuplication' /></td>
						</tr>
						<tr>
							<th align='left'>Unique Url</th>
							<td ><input type='text' name='uniqueUrl' id='edit_pagesurl_input' rel='$business[id]' value=\"$business[unique_url]\" 
							 class='stubSenders'
							 
							 stubStatus='edit_urlReturnValue'
							stubUrl='admin/business/checkUrlDuplication' /></td>
							<td><span id='edit_urlReturnValue' style='font-weight:bold;'></span></td>
						</tr>
						<tr>
							<th align='left' >Username</th>
							<td ><input type='text' name='username' value=\"$business[username]\" id='edit_username_input' /></td>
						</tr>
						<tr>
							<th align='left' >Password</th>
							<td ><input type='password' name='password' /></td>
						</tr>
						<tr>
							<th align='left' >Retype Password</th>
							<td ><input type='password' name='re_password' id='edit_re_password_input' /></td>
						</tr>
						<tr>
							<th align='left' >Email</th>
							<td ><input type='text' name='email' id='edit_email_input' value=\"$business[email]\" /></td>
						</tr>
						<tr>
							<th align='left' >Phone</th>
							<td ><input type='text' name='phone' id='edit_phone_input' value=\"$business[phone]\" /></td>
						</tr>
						<tr>
							<th align='left' >Land Line Number</th>
							<td ><input type='text' name='landLine' id='edit_phone_input' value=\"$business[land_line]\" /></td>
						</tr>
						<tr>
							<th align='left' >Address Line 1</th>
							<td ><textarea  name='address1' id='edit_address1_input' >" . $business['address_line_1'] . "</textarea></td>
						</tr>
						<tr>
							<th align='left' >Address Line 2</th>
							<td ><textarea  name='address2' id='edit_address2_input' >" . $business['address_line_2'] . "</textarea></td>
						</tr>
						<tr>
							<th align='left' >Discount</th>
							<td ><input type='text' name='discount' id='edit_discount_input' value=\"$business[discount]\" /></td>
						</tr>
						<tr>
							<th align='left' >Discount Validity Time</th>
							<td ><input type='text' name='discount_validity' value=\"$business[discount_validity_time]\" class='datepicker' /></td>
						</tr>
						<tr>
							<th align='left' >Service Charge</th>
							<td ><input type='text' name='serviceCharge' id='edit_serviceCharge_input' value=\"$business[service_charge]\" /></td>
						</tr>
						<tr>
							<th align='left' >Transaction Charge</th>
							<td ><input type='text' name='transactionCharge' id='edit_transactionCharge_input' value=\"$business[transaction_charge]\" /></td>
						</tr>
				
						<tr >
							<th align='left' >Opening Time </th>
							<td ><input type='text'  name='opening1' id='edit_opening_input1' value=\"$business[opening]\" /></td>
						</tr>
						<tr >
							<th align='left' >Closing Time </th>
							<td ><input type='text'  name='closing1' id='edit_closing_input1' value=\"$business[closing]\"  /></td>
						</tr>
						<tr >
							<th align='left' >Reservation</th>
							<td><input type='checkbox'  name='reservation' value='1' ". (($business['is_reservation'] ==1)? 'checked':'') ." /></td>
						</tr>
							<tr >
							<th align='left'>Deposit</th>
							<td ><input type='checkbox'  name='deposit' value='1' ". (($business['is_deposit'] ==1)? 'checked':'') ."/></td>
						</tr>
						<tr >
							<th align='left'>Deposit Week Day</th>
	                     	<td >
		                     	<select name='depositweekday'>
		                     		<option value='' >Select</option>
			                    	<option value='1' ". (($business['deposit_week_day'] ==1)? 'selected':'') .">Sunday</option>
			                    	<option value='2' ". (($business['deposit_week_day'] ==2)? 'selected':'') .">Monday</option>
			                    	<option value='3' ". (($business['deposit_week_day'] ==3)? 'selected':'') .">Tuesday</option>
			                    	<option value='4' ". (($business['deposit_week_day'] ==4)? 'selected':'') .">Wednesday</option>
			                    	<option value='5' ". (($business['deposit_week_day'] ==5)? 'selected':'') .">Thursday</option>
			                    	<option value='6' ". (($business['deposit_week_day'] ==6)? 'selected':'') .">Friday</option>
			                    	<option value='7' ". (($business['deposit_week_day'] ==7)? 'selected':'') .">Saturday</option>
			                    </select>
	                    	</td>
						</tr>
						<tr >
							<th align='left'>Deposit Amount</th>
	                     	<td ><input type='text'  name='deposit_amount' value=\"$business[deposit_amount]\" /></td>
						</tr>
							<tr >
							<th align='left'>Minimum Order Value for Collection</th>
	                     	<td ><input type='text'  name='order_value_collection' value=\"$business[order_value_collection]\" /></td>
						</tr>
						<tr >
							<th align='left'>Minimum Order Value for Delivery</th>
	                     	<td ><input type='text'  name='order_value_delivery' value=\"$business[order_value_delivery]\" /></td>
						</tr>
						<tr>
							<th align='left' >Url</th>
							<td >" . str_replace($this->replace, $this->replace_word, base_url()).'restaurant/'.$business['unique_url'] . "</td>
						</tr>

						</tr>
							<th align='left'>Logo </th>
							<td align='left' class='img'>
  							  	<img  src='". base_url()."assets/logo/$business[logo]' style='width:100px;height:100px'  />
  							 </td>
						</tr>

						<tr>
							<td align='right' colspan='4'>
							<button type='submit' class='button save'>Save Changes</button>
							<div id='loader$business[id]' class='q_feedback'></div>
							<input type='hidden' name='u_id' value='$business[uid]' />
							<input type='hidden' name='business_id' value='$business[id]' />
							</td>
						</tr>
					</table>
				</form></div>
			</div>
			</div></div>
			<script type=\"text/javascript\">
$('.datepicker').datepicker({
	dateFormat: 'yy-mm-dd',
	minDate: 0
});

	$('#edit_opening_input1').ptTimeSelect({
			containerClass: 'timeCntr',
			containerWidth: '350px',
			setButtonLabel: 'Select',
			minutesLabel: 'min',
			hoursLabel: 'Hrs'
		});
		$('#edit_closing_input1').ptTimeSelect({
			containerClass: 'timeCntr',
			containerWidth: '350px',
			setButtonLabel: 'Select',
			minutesLabel: 'min',
			hoursLabel: 'Hrs'
		});

</script>
			";

			echo $html;
		}
	}


	function saveBusiness(){
		if(_checkAuthentication($this)){
			
			$businessname=$this->input->post("businessname");
			$username=$this->input->post("username");
			$password=$this->input->post("password");
			$re_password=$this->input->post("re_password");
			$email=$this->input->post("email");
			$phone=$this->input->post("phone");
			$landLine=$this->input->post("landLine");
			$address1=$this->input->post("address1");
			$address2=$this->input->post("address2");
			$discount=$this->input->post("discount");
			$discount_validity=$this->input->post("discount_validity");
			$serviceCharge=$this->input->post("serviceCharge");
			$transactionCharge=$this->input->post("transactionCharge");
			$opening=$this->input->post("opening1");
			$closing=$this->input->post("closing1");
			$id=$this->input->post("business_id");
			$uid=$this->input->post("u_id");
			$url=$this->input->post("uniqueUrl");
			$reservation =$this->input->post("reservation");
			$deposit =$this->input->post("deposit");
			$depositweekday =$this->input->post("depositweekday");
			$deposit_amount =$this->input->post("deposit_amount");
			$ordervaluecollection =$this->input->post("order_value_collection");
			$ordervaluedelivery =$this->input->post("order_value_delivery");
			
			if(trim($password)!="" && $password!=$re_password){
				echo '{success:"error",msg:"Passwords do not match..."}';				
				exit;
			}
			
			if(empty($businessname)){
				echo '{success:"error",msg:"Empty businessname is not allowed..."}';				
				exit;
			}
			if(empty($username)){
				echo '{success:"error",msg:"Empty username is not allowed..."}';
				exit;
			}
				
			
			if(empty($address1)){
				echo '{success:"error",msg:"Empty address line 1 is not allowed..."}';
				exit;
			}
			
			$ex=$this->business->usernameExists($username);
			$continue=false;
			if($ex==$uid || $ex===false){
				//same user
				$continue=true;
			}else{
				echo '{success:"error",msg:"This username already exists...\nPlease try another one."}';			
				exit;
			}

			if(!empty($email)){
				
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					
					echo '{success:"error",msg:"Please enter a valid email address"}';
					exit;
				}
			}
			
			if(!empty($discount)){
				if($discount !=0){
					if (!filter_var($discount, FILTER_VALIDATE_FLOAT))
					{
						echo '{success:"error",msg:"Please enter a valid number for Discount"}';
						exit;
					}
				}
			}
				
			if(!empty($serviceCharge)){
				if($serviceCharge !=0){
					if (!filter_var($serviceCharge, FILTER_VALIDATE_FLOAT))
					{
						echo '{success:"error",msg:"Please enter a valid number for Service Charge"}';
						exit;
					}
				}
				
			}
				
			if(!empty($transactionCharge)){
				if($transactionCharge !=0){
					if (!filter_var($transactionCharge, FILTER_VALIDATE_FLOAT))
					{
						echo '{success:"error",msg:"Please enter a valid number for Transaction Charge"}';
						exit;
					}
				}
			}
			
			if(!empty($url)){
				$urlAr=$this->business->getBy("unique_url",trim($url));
				if(is_array($urlAr)){
					foreach ($urlAr as $v) {
						
						if($id != $v['id']){
							echo '{success:"error",msg:"Please enter a valid Unique Url..."}';
							
							exit;
						}
						
					}
				}
			} else {
				echo '{success:"error",msg:"Empty Unique Url is not allowed..."}';
				
				exit;
			}
			
			if(!empty($deposit) &&  $deposit_amount ==0){
				echo '{"success":"error",msg:"Please enter a deposit amount..."}';
				exit;
			}
			
			if(empty($ordervaluecollection) || !filter_var($ordervaluecollection, FILTER_VALIDATE_FLOAT)){
				echo '{success:"error",msg:"Empty Minimum Order Value for Collection is not allowed..."}';
				exit;
			}
			if(empty($ordervaluecollection) || !filter_var($ordervaluedelivery, FILTER_VALIDATE_FLOAT)){
				echo '{success:"error",msg:"Empty Minimum Order Value for Delivery is not allowed..."}';
				exit;
			}
			
			$data=array(
				"name" => $businessname,
				"email" => $email,
				"phone" => $phone,
				"land_line" => $landLine,
				"address_line_1" => $address1,
				"address_line_2" => $address2,
				"unique_url" => $url,
				"discount" => $discount,
				"discount_validity_time" => $discount_validity,
				"service_charge" => $serviceCharge,
				"transaction_charge" => $transactionCharge,
				"opening" => $opening,
				"closing" => $closing,
				"is_reservation"=>(($reservation =='')?0:$reservation),
				"is_deposit"=>(($deposit =='')?0:$deposit),
				"deposit_week_day"=>$depositweekday,
				"deposit_amount"=>$deposit_amount,
				"order_value_collection"=>$ordervaluecollection,
				"order_value_delivery"=>$ordervaluedelivery
			);

			$data1=array(
				"name" => $businessname,
				"email" => $email,
				"phone" => $phone,
				"land_line" => $landLine,
				"address_line_1" => $address1,
				"address_line_2" => $address2,
				"unique_url" => $url,
				"discount" => $discount,
				"discount_validity_time" => $discount_validity,
				"service_charge" => $serviceCharge,
				"transaction_charge" => $transactionCharge,
				"opening" => $opening,
				"closing" => $closing,
				"is_reservation"=>(($reservation =='')?0:$reservation),
				"is_deposit"=>(($deposit =='')?0:$deposit),
				"deposit_week_day"=>$depositweekday,
				"deposit_amount"=>$deposit_amount,
				"order_value_collection"=>$ordervaluecollection,
				"order_value_delivery"=>$ordervaluedelivery
			);



			if($continue && !empty($password)){
				if($this->business->save($id,$data1)){
					$this->business->tablename=$this->business->user;
					$this->business->save($uid,array("username" => $username,"password" => md5($password)));
					activitylog($this,"Business <i>".$username."</i> details modified","m");

						echo '{"success":"success","successFunction":function(){
							$("#business_'.$id.'").click();
							$("#business_'.$id.' span.nameBtn").html("'.$username.'");
							NIBS.notif.notify("success","Business details saved");
						}}';
						exit;
				}else{
					echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update business information due to a database error...")
					}}';
					exit;
				}
			}elseif($continue && empty($password)){
				if($this->business->save($id, $data)){
					$this->business->tablename=$this->business->user;
					$this->business->save($uid,array("username" => $username));
					  activitylog($this,"Business <i>".$username."</i> details modified","m");

						echo '{"success":"success","successFunction":function(){
							$("#business_'.$id.'").click();
							$("#business_'.$id.' span.nameBtn").html("'.$username.'");
							NIBS.notif.notify("success","Business details saved");
									
						}}';
						exit;
				}else{
					echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update business information due to a database error...")
					}}';
					exit;
				}
			}
		}
	}

	function addBusiness(){
		if(_checkAuthentication($this)){
			$businessname=$this->input->post("businessname");
			$url=$this->input->post("uniqueUrl");
			$username=$this->input->post("username");
			$password=$this->input->post("password");
			$re_password=$this->input->post("re_password");
			$email=$this->input->post("email");
			$phone=$this->input->post("phone");
			$landLine=$this->input->post("landLine");
			$address1=$this->input->post("address1");
			$address2=$this->input->post("address2");
			$discount=$this->input->post("discount");
			$discount_validity=$this->input->post("discount_validity");
			$serviceCharge=$this->input->post("serviceCharge");
			$transactionCharge=$this->input->post("transactionCharge");
			$opening=$this->input->post("opening");
			$closing=$this->input->post("closing");
			$reservation =$this->input->post("reservation");
			$deposit =$this->input->post("deposit");
			$depositweekday =$this->input->post("depositweekday");
			$deposit_amount =$this->input->post("deposit_amount");
			$ordervaluecollection =$this->input->post("order_value_collection");
			$ordervaluedelivery =$this->input->post("order_value_delivery");
			
			
			
			if(empty($businessname)){
				echo '{"success":"error","successFunction":function(){
					alert("Empty businessname is not allowed...");
				}}';
				exit;
			}
			if(empty($username)){
				echo '{"success":"error","successFunction":function(){
					alert("Empty username is not allowed...");
				}}';
				exit;
			}
			
			if(empty($password)){
				echo '{"success":"error","successFunction":function(){
					alert("Empty password is not allowed...");
				}}';
				exit;
			}
			if(empty($address1)){
				echo '{"success":"error","successFunction":function(){
					alert("Empty address line 1 is not allowed...");
				}}';
				exit;
			}
			
			if($password!=$re_password){
				echo '{"success":"error","successFunction":function(){
					alert("Passwords do not match...");
					$("#add_re_password_input").focus();
				}}';
				exit;
			}
			if($this->business->usernameExists($username)){
				echo '{"success":"error","successFunction":function(){
					alert("Username Exists,\nPlease try a different one...");
					$("#add_username_input").focus();
				}}';
				exit;
			}
			if(!empty($email)){
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					echo '{"success":"error","successFunction":function(){
						alert("Please enter a valid email address");
						$("#add_email_input").focus();
					}}';
					exit;
				}
			}
			if(!empty($discount)){
				if (!filter_var($discount, FILTER_VALIDATE_FLOAT))
				{
					echo '{"success":"error","successFunction":function(){
						alert("Please enter a valid number");
						$("#add_discount_input").focus();
					}}';
					exit;
				}
			}
			
			if(!empty($serviceCharge)){
				if (!filter_var($serviceCharge, FILTER_VALIDATE_FLOAT))
				{
					echo '{"success":"error","successFunction":function(){
						alert("Please enter a valid number");
						$("#add_serviceCharge_input").focus();
					}}';
					exit;
				}
			}
			
			if(!empty($transactionCharge)){
				if (!filter_var($transactionCharge, FILTER_VALIDATE_FLOAT))
				{
					echo '{"success":"error","successFunction":function(){
						alert("Please enter a valid number");
						$("#add_transactionCharge_input").focus();
					}}';
					exit;
				}
			}
			
			if(!empty($url)){
				$urls=$this->business->getBy("unique_url",trim($url));
				if(is_array($urls)){
					foreach ($urls as $v) {
						echo '{"success":"error","successFunction":function(){
							alert("Please enter a valid Unique Url");
							$("#add_pagesurl_input").focus();
						}}';
						exit;
					}
				}
			} 
			else {
				echo '{"success":"error","successFunction":function(){
					alert("Empty Unique Url is not allowed...");
					$("#add_pagesurl_input").focus();
				}}';
				exit;
			}

			if(!empty($deposit) && empty($deposit_amount)){
				echo '{"success":"error","successFunction":function(){
					alert("Please enter a deposit amount...");
				}}';
				exit;
			}
			if(empty($ordervaluecollection) || !filter_var($ordervaluecollection, FILTER_VALIDATE_FLOAT)){
				echo '{success:"error","successFunction":function(){
					alert("Empty Minimum Order Value for Collection is not allowed..."); }}';
				exit;
			}
			if(empty($ordervaluecollection) || !filter_var($ordervaluedelivery, FILTER_VALIDATE_FLOAT)){
				echo '{success:"error","successFunction":function(){
					alert("Empty Minimum Order Value for Delivery is not allowed..."); }}';
				exit;
			}

		
			$this->business->tablename=$this->business->user;
			$b_id = $this->business->create(array(
				"username" => $username,
				"password" => md5($password),
				'userlevel' => 'business'
				));
			if($b_id){
				$this->business->tablename=$this->business->business;
				$ins=$this->business->create(
						array(
								"name" => $businessname,
								"user_id" => $b_id,
								"email" => $email,
								"phone" => $phone,
								"land_line" => $landLine,
								"address_line_1" => $address1,
								"address_line_2" => $address2,
								"unique_url" => $url,
								"discount" => $discount,
								"discount_validity_time" => $discount_validity,
								"service_charge" => $serviceCharge,
								"transaction_charge" => $transactionCharge,
								"opening" => $opening,
								"closing" => $closing,
								"is_active" => 1,
								"is_reservation"=>(($reservation =='')?0:$reservation),
								"is_deposit"=>(($deposit =='')?0:$deposit),
								"deposit_week_day"=>$depositweekday,
								"deposit_amount"=>$deposit_amount,
								"order_value_collection"=>$ordervaluecollection,
								"order_value_delivery"=>$ordervaluedelivery
								));
				if($ins){
	
				  activitylog($this,"Business <i>".$username."</i> added","c");
	
					echo '{"success":"success",successFunction:function(){
						addBusiness("'.$username.'","'.$ins.'");
					}}';
					exit;
	
				}else{
					echo '{"success":"error",successFunction:function(){
						alert("Could not add Business due to a database error");
						}}';
						exit;
				}
			}
		}
	}

	function deleteBusiness(){
		if(_checkAuthentication($this)){
			$id=$this->input->post('id');
			if(trim($id)==""){
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove business due to a database error,\\nPlease reload page");
					}}';
					exit;
			}
			$U=$this->business->getBusines($id);
			if($this->business->deleteBusiness($id)){

			  activitylog($this,"Business <i>".$U["username"]."</i> deleted","d");

				echo '{"success":"success",successFunction:function(){
					deleteBusiness("'.$id.'");
					}}';
					exit;
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove business due to a database error");
					}}';
					exit;
			}
		}
	}

	function activateDeactivate(){
		if(_checkAuthentication($this)){
			$id=$this->input->post('id');
			$res=$this->business->get($id);
			if($res["is_active"]=="0"){
				$new_value="1";
			}else{
				$new_value="0";
			}
			if($this->business->save($id,array("is_active"=>$new_value))){
				echo '{"success":"success",successFunction:function(){
					do_activate_page("'.$id.'","'.$new_value.'");
					}}';
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not toggle visiblity due to server error...<br />Please try again");
					}}';
			}
		}
	}
	
	function loadPermissions($id=""){
		//By Default, business are allowed every where
		//have to manually uncheck items you would want them to be blocked on
		if(_checkAuthentication($this)){
			if(trim($id=="")){
				echo '<span style="color:red">Error: No user defined</span>';
					exit;
			}

			$business=$this->business->getBusines($id);
			if(!isset($business["id"]) || !$business["id"]>0){
				echo '<span style="color:red">Error: Invalid business</span>';
					exit;
			}
			$html="<form action='".base_url()."admin/business/savePermissions' method='post' rel='ajaxed' loadingDiv='permsLoader$business[id]'>";
			
					$allPerms=$this->_permittedActions();
					$userPerms=unserialize($business["pMatrix"]);
					if(is_array($allPerms)){
						ksort($allPerms);
						foreach($allPerms as $section=>$subsections){
							$html.="<div class='permissionSection clearfix'><span class='add_shadow section headertoolbar collapsible open'>$section <input class='main_cb' type='checkbox' onclick='checkUncheck_all_perms(event,this);' /></span>";
							if(is_array($subsections)){
								foreach($subsections as $subsection=>$name){
									if(isset($userPerms[$section][$subsection])){
										$html.="<label class='subsection'><input type='checkbox' name='".$section."_".$subsection."' /> $name</label>";
									}else{
										$html.="<label class='subsection'><input type='checkbox' name='".$section."_".$subsection."' checked /> $name</label>";
									}

								}
							}
							$html.="</div>";
						}
					}
					echo $html."<div class='clearit'></div><hr /><input type='hidden' name='business_id' value='".$business["id"]."' /><input type='hidden' name='u_id' value='".$business["uid"]."' />

					<div style='text-align:center'><button class='button save' type='submit'>Save Permissions</button><div class='q_feedback' id='permsLoader$business[id]'></div></div>
          <hr />
					<div class='clearit'></div>
					</form>";
			



		}
	}

	function savePermissions(){
		if(_checkAuthentication($this)){
			$business_id=$this->input->post("business_id");
			$uid=$this->input->post("u_id");
			$business=$this->business->getBusines($business_id);
			if(!isset($business["id"]) || !$business["id"]>0){
				echo '{"success":"error",successFunction:function(){
					alert("Error: Business not defined");
					}}';
					exit;
			}
			
				//get available permissions list
				$allPerms=$this->_permittedActions();
				//parse posted permissions
				$Posted=$_POST;
				$Posted_Arr=array();
				if(is_array($Posted)){
					foreach($Posted as $name=>$value){
						if($name!="business_id" && $name!="ajaxed11185"){
							list($class,$function)=explode("_",$name);
							$Posted_Arr[$class][$function]="";
						}

					}
				}

				//compare permissions and flag up that are not included
				$flagged_array=array();
				if(is_array($allPerms)){
					foreach($allPerms as $section=>$subsections){
						if(is_array($subsections)){
							foreach($subsections as $subsection=>$name){
								if(!isset($Posted_Arr[$section][$subsection])){
									//this permission is unchecked, ao add to flagged array
									$flagged_array[$section][$subsection]="";
								}
							}
						}
					}
				}

				$to_db=serialize($flagged_array);
				$this->business->tablename=$this->business->user;
				$success=$this->business->save($uid,array("pMatrix"=>$to_db));
				if($success){

				  activitylog($this,"Business <i>".$business["username"]."</i> permissions modified","m");

					echo '{"success":"success",successFunction:function(){
						  NIBS.notif.notify("success","Business permissions saved successfully");
						}}';
						exit;
				}else{
					echo '{"success":"error",successFunction:function(){
						alert("Could not save permissions due to a database error");
						}}';
						exit;
				}
			

		}
	}

	function _permittedActions(){
		//list application/controllers/admin
		$actionsArray=array();
		$path=APPPATH."controllers/admin/";
		$dh=opendir($path);
		while(false != ($file=readdir($dh))){
			//include all php files not beginning with a "."
			if(!is_dir($path.$file) && substr($file,0,1)!="."  && substr($file,-4,4)==".php"  && $file!="authentication.php" && $file!="notifydaemon.php" && $file!="business.php"){
				include($path.$file);
				//uppercase the filename to get class name
				$className=ucfirst(array_shift(explode(".",$file)));
				$readableClassName=ucwords(str_replace(array("_","-")," ",$className));
				$actionsArray[$readableClassName]=array();
				//foreach class name, find all methods
				$meths=get_class_methods($className);
				if(is_array($meths)){
					foreach ($meths as $method) {
						if($method!="Controller" && $method!="CI_Base" && $method!="get_instance" && $method!=$className && $method!="Controller" && substr($method,0,1)!="_" && substr($method,0,1)!="X"){
							if($method=="index"){
								$method2="showList";
							}else{
								$method2=$method;
							}
							$actionsArray[$readableClassName][$method]=ucfirst(_uncamelize($method2," "));
						}
					}
				}
			}
		}
		// now do this file

		$actionsArray["Business"]=array();
		//for this class name, find all methods
		$meths=get_class_methods($this);
		if(is_array($meths)){
			foreach ($meths as $method) {
				if($method!="Controller" && $method!="CI_Base" && $method!="get_instance" && $method!="Business" && $method!="Controller" && substr($method,0,1)!="_"){
					if($method=="index"){
						$method2="showList";
					}else{
						$method2=$method;
					}
					$actionsArray["Business"][$method]=ucfirst(_uncamelize($method2," "));
				}
			}
		}

		closedir($dh);
		return $actionsArray;

	}

	function profileEdit()
	{
		if(_checkAuthentication($this)){
		$this->tab = 'profile';
		$business=$this->business->getBusinesByUser($this->session->userdata('uid'));
			
		if(!isset($business["id"]) || !$business["id"]>0){
			echo '<span style="color:red">Error: Invalid Business</span>';
			exit;
		}
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
			$this->load->view("admin/business_profile",array("business"=>$business));
			$this->load->view("admin/admin_foot");
		}
	}
	
	function saveBusinessProfile(){
		if(_checkAuthentication($this)){
				
			$businessname=$this->input->post("businessname");
			$username=$this->input->post("username");
			$password=$this->input->post("password");
			$re_password=$this->input->post("re_password");
			$email=$this->input->post("email");
			$phone=$this->input->post("phone");
			$landline=$this->input->post("landline");
			$address1=$this->input->post("address1");
			$address2=$this->input->post("address2");
			$opeing=$this->input->post("opening");
			$closing=$this->input->post("closing");
			$id=$this->input->post("business_id");
			$uid=$this->input->post("u_id");
			$reservation =$this->input->post("reservation");
			$deposit =$this->input->post("deposit");
			$depositweekday =$this->input->post("depositweekday");
			$deposit_amount =$this->input->post("deposit_amount");
			$ordervaluecollection =$this->input->post("order_value_collection");
			$ordervaluedelivery =$this->input->post("order_value_delivery");
				
			if(trim($password)!="" && $password!=$re_password){
				echo '{success:"error",msg:"Passwords do not match..."}';
				exit;
			}
				
			if(empty($businessname)){
				echo '{success:"error",msg:"Empty businessname is not allowed..."}';
				exit;
			}
			if(empty($username)){
				echo '{success:"error",msg:"Empty username is not allowed..."}';
				exit;
			}
	
				
			if(empty($address1)){
				echo '{success:"error",msg:"Empty address line 1 is not allowed..."}';
				exit;
			}
			if(empty($ordervaluecollection) || !filter_var($ordervaluecollection, FILTER_VALIDATE_FLOAT)){
				echo '{success:"error",msg:"Empty Minimum Order Value for Collection is not allowed..."}';
				exit;
			}
			if(empty($ordervaluecollection) || !filter_var($ordervaluedelivery, FILTER_VALIDATE_FLOAT)){
				echo '{success:"error",msg:"Empty Minimum Order Value for Delivery is not allowed..."}';
				exit;
			}
				
			$ex=$this->business->usernameExists($username);
			$continue=false;
			if($ex==$uid || $ex===false){
				//same user
				$continue=true;
			}else{
				echo '{success:"error",msg:"This username already exists...\nPlease try another one."}';
				exit;
			}
	
			if(!empty($email)){
	
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
						
					echo '{success:"error",msg:"Please enter a valid email address"}';
					exit;
				}
			}
			
			if(!empty($deposit) &&  $deposit_amount ==0){
				echo '{"success":"error",msg:"Please enter a deposit amount..."}';
				exit;
			}
			
	
			$data=array(
					"name" => $businessname,
					"email" => $email,
					"phone" => $phone,
					"land_line" =>$landline,
					"address_line_1" => $address1,
					"address_line_2" => $address2,
					"opening" => $opeing,
					"closing" => $closing,
					"is_reservation"=>(($reservation =='')?0:$reservation),
					"is_deposit"=>(($deposit =='')?0:$deposit),
					"deposit_week_day"=>$depositweekday,
					"deposit_amount"=>$deposit_amount,
					"order_value_collection"=>$ordervaluecollection,
					"order_value_delivery"=>$ordervaluedelivery
					
			);
	
			$data1=array(
					"name" => $businessname,
					"email" => $email,
					"phone" => $phone,
					"land_line" =>$landline,
					"address_line_1" => $address1,
					"address_line_2" => $address2,
					"opening" => $opeing,
					"closing" => $closing,
					"is_reservation"=>(($reservation =='')?0:$reservation),
					"is_deposit"=>(($deposit =='')?0:$deposit),
					"deposit_week_day"=>$depositweekday,
					"deposit_amount"=>$deposit_amount,
					"order_value_collection"=>$ordervaluecollection,
					"order_value_delivery"=>$ordervaluedelivery
			);
	
	
	
			if($continue && !empty($password)){
				if($this->business->save($id,$data1)){
					$this->business->tablename=$this->business->user;
					$this->business->save($uid,array("username" => $username,"password" => md5($password)));
					activitylog($this,"Business <i>".$username."</i> details modified","m");
	
					echo '{"success":"success","successFunction":function(){
							location.reload();
							
							NIBS.notif.notify("success","Business details saved");
						}}';
					exit;
				}else{
					echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update business information due to a database error...")
					}}';
					exit;
				}
			}elseif($continue && empty($password)){
				if($this->business->save($id, $data)){
					$this->business->tablename=$this->business->user;
					$this->business->save($uid,array("username" => $username));
					activitylog($this,"Business <i>".$username."</i> details modified","m");
	
					echo '{"success":"success","successFunction":function(){
							setInterval(function(){location.reload()},2000);
							
							NIBS.notif.notify("success","Business details saved");
					
						}}';
					exit;
				}else{
					echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update business information due to a database error...")
					}}';
					exit;
				}
			}
		}
	}
	
	public function updateLogo()
	{
		if(_checkAuthentication($this)){
			$this->tab = 'businessLogo';
			$business=$this->business->getBusinesByUser($this->session->userdata('uid'));
				
			if(!isset($business["id"]) || !$business["id"]>0){
				echo '<span style="color:red">Error: Invalid Business</span>';
				exit;
			}
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
			$this->load->view("admin/business_logo",array("business"=>$business));
			$this->load->view("admin/admin_foot");
		}
	}
	
	function saveBusinessLogo(){
		if(_checkAuthentication($this)){
			$data = array();
			$business_id=$this->input->post("business_id");
			$business=$this->business->getBusinesByUser($this->session->userdata('uid'));
			
			$path=$this->business->_repo;
			$this->load->library("upload",array(
					"upload_path"=>$path,
					"allowed_types"=>"gif|jpeg|jpg|png"
			));
			
			$files=array();
			foreach($_FILES as $key=>$value){
				if($value['size']!=0){
					$img_file=$value['name'];
					$this->upload->encrypt_name = true;
					$this->upload->file_ext = $this->upload->get_extension($img_file);
					$file=$this->upload->set_filename($path,$img_file);
			
					$files[$key]=$file;
			
					move_uploaded_file($value['tmp_name'], $path.$file);
			
					$data[$key]=$file;
					
				}
			}
			if(count($data) > 0){
				if($this->business->save($business_id,$data)){
					
				
					activitylog($this,"Business <i>".$business['username']."</i> logo modified","m");
				
					echo '{"success":"success","successFunction":function(){
							setInterval(function(){location.reload()},2000);
							
							NIBS.notif.notify("success","Business Logo saved");
						}}';
					exit;
				}else{
					echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update business logo due to a database error...")
					}}';
					exit;
				}
			}
		}
	}
	
	
	function checkUrlDuplication(){
		$business =$this->input->post('stub');
		$rel=$this->input->post('rel');
		if(trim($business!="")){
			$url=$this->business->getBy("unique_url",$business);
			if(is_array($url)){
				foreach ($url as $v) {
					if($v["id"]!=$rel){
						echo "<span title='Stub Exists'><img src='".base_url()."assets/images/admin/icon_error.png'  border='0' /></span>";
						exit;
					}
				}
			}
			echo "<span title='Stub is Unique'><img src='".base_url()."assets/images/admin/icon_success.png'  border='0' /></span>";
		}
	}
	

}


?>
