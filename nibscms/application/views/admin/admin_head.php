<?php $baseUrl=base_url(); ?>

<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8">
</html>
<![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9">
</html>
<![endif]-->
<!--[if !IE]>
<!-->
<html lang="en">
  <!--
<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <base href="<?=$baseUrl?>">
    <title>Dashboard</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    

    <link href="<?=$baseUrl?>assets/third_party/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- <link href="<?=$baseUrl?>assets/third_party/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /> -->
    <link href="<?=$baseUrl?>assets/third_party/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>assets/css/admin/style.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>assets/css/admin/style_responsive.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>assets/css/admin/style_default.css" rel="stylesheet" id="style_color" />
    <link href="<?=$baseUrl?>assets/third_party/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?=$baseUrl?>assets/third_party/uniform/css/uniform.default.css" />
    <link href="<?=$baseUrl?>assets/third_party/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="<?=$baseUrl?>assets/third_party/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
    
   


    
    <!--<script src="<?=$baseUrl?>assets/third_party/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=$baseUrl?>assets/js/application.js" type="text/javascript"></script>
    -->

    <link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/common.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/animations.css" type="text/css" media="screen" />
   
  <script type="text/javascript" src="<?=$baseUrl?>assets/js/jquery-1.7.2.min.js"></script> 
   <script src="<?=$baseUrl?>assets/third_party/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=$baseUrl?>assets/js/application.js" type="text/javascript"></script>
  
  <link rel="stylesheet" href="<?=site_url()?>assets/css/admin/jquery-ui/jquery-ui-1.8.23.custom.css" type="text/css" />
  <script type="text/javascript" src="<?=site_url()?>assets/js/jquery-ui-1.8.23.custom.min.js"></script>
  <script src="<?=$baseUrl?>assets/js/css.js" type="text/javascript"></script>
  <script src="<?=$baseUrl?>assets/js/tree_component.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/common.js"></script>
 <script type="text/javascript">
      var base_url="<?=$baseUrl?>";
      var BASE_URL = '<?=$baseUrl?>';
    </script>

  </head>
  <body class="fixed-top">


    <div id="header" class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="brand" href="#" style='padding:0;padding-left:2px; padding-right:2px'>
            <img src="<?=$baseUrl?>assets/images/admin/nibs_logo.png" alt="Nibs Solutions"/><div id="moniker" style='font-size:25px; font-weight:bold'>Control Panel</div>
          </a>
          <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="arrow"></span>
          </a>

          <div id="notifications">
            <div id="notifier" class="error"></div>
          </div>

          <div id="headerBar">
            <div id="preloader"><img src="<?=$baseUrl?>assets/images/admin/loader.gif" /></div>
          </div>

          <div id="top_menu" class="nav top-menu main-submenu">
            <ul class="nav top-menu">
              <li title="Dashboard"><a href="<?=$baseUrl?>admin/" class='<?=(($tab=="dashboard")?("active"):(""))?>'>Dashboard</a></li>      
              <?php if($this->session->userdata('utype') == 'admin') { ?>
               <li title="Business" <?=(($tab=="business")?("class='active'"):(""))?>><a href="<?=$baseUrl?>admin/business/">Business</a></li>
              
              
              	<li title="Gateway" <?=(($tab=="order")?("class='active'"):(""))?>><a href="<?=$baseUrl?>admin/gateway/">Gateway Settings</a></li>
               <?php } ?>
              <?php if($this->session->userdata('utype') == 'business') { ?>              
               <li title="Product" <?=(($tab=="product")?("class='active'"):(""))?>><a href="<?=$baseUrl?>admin/product/">Product</a></li>
               <li title="Order" <?=(($tab=="order")?("class='active'"):(""))?>><a href="<?=$baseUrl?>admin/order/">Order</a></li>
              
			   <li title="Profile Edit" <?=(($tab=="profile")?("class='active'"):(""))?>><a href="<?=$baseUrl?>admin/business/profileEdit">Profile</a></li>
			   <li title="Business Logo" <?=(($tab=="businessLogo")?("class='active'"):(""))?>><a href="<?=$baseUrl?>admin/business/updateLogo">Logo</a></li>
               <li title="Pages"><a class='<?=(($tab=="pages")?("active"):(""))?>' href="<?=$baseUrl?>admin/pages/">Pages</a></li>
               <li title="Social"><a class='<?=(($tab=="social")?("active"):(""))?>' href="<?=$baseUrl?>admin/social/">Social</a></li>
               <li title="Delivery Charge Setting"><a class='<?=(($tab=="deliverysetting")?("active"):(""))?>' href="<?=$baseUrl?>admin/deliverycharge/">Delivery Charge Setting</a></li>
               
               <li title="Reservation"><a class='<?=(($tab=="reservation")?("active"):(""))?>' href="<?=$baseUrl?>admin/reservation">Reservation</a></li>
               
               <li title="General Setting"><a class='<?=(($tab=="generalSetting")?("active"):(""))?>' href="<?=$baseUrl?>admin/generalsetting">General Setting</a></li>
               <?php } ?>
                <li title="Import Product" <?=(($tab=="import")?("class='active'"):(""))?>><a href="<?=$baseUrl?>admin/productImport/">Import Product</a></li>
               
<!--                  
                <li class="dropdown" title="Products">
                <a data-toggle="dropdown" class="dropdown-toggle <?=(($tab=="fresh_produce" || $tab=="halal_meat" || $tab=="offers")?("active"):(""))?>" href="<?=$baseUrl?>admin/products/">Products<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                      <li title="Fresh Produce"><a class='<?=(($tab=="fresh_produce")?("active"):(""))?>' href="<?=$baseUrl?>admin/products/">Fresh Produce</a></li>
                      <li title="Halal Meat"><a class='<?=(($tab=="halal_meat")?("active"):(""))?>' href="<?=$baseUrl?>admin/halal_meats/">Halal Meat</a></li>
                      <li title="Offers"><a class='<?=(($tab=="offers")?("active"):(""))?>' href="<?=$baseUrl?>admin/offers/">Offers</a></li>
                  </ul>                           
              </li>
              -->
               
                
                <!--<li title="SMS"><a href="<?=$baseUrl?>admin/sms/" class='<?=(($tab=="sms")?("active"):(""))?>'>SMS</a></li>-->
              </ul>
          </div>
          <div class="top-nav ">
            <ul class="nav pull-right top-menu">
<!--               <li class="dropdown mtop5">
                <a class="dropdown-toggle element <?=(($tab=="settings")?("active"):(""))?>" data-placement="bottom" data-toggle="tooltip" href="<?=$baseUrl?>admin/settings/" data-original-title="Settings" >
                  <i class="icon-cog"></i>
                </a>
              </li> -->
              
              <li class="dropdown">
                <a href="#" class="dropdown-toggle <?=(($tab=="settings" || $tab=="users")?("active"):(""))?>" data-toggle="dropdown">
                  <img src="<?=$baseUrl?>assets/<?=((isset($userImg) && $userImg !='')?('users_img/'.$userImg):('images/avatar-mini.png'))?>" alt="" />
                  <span class="username" id='userName'>
                    <?php
          
                        if(isset($username)){
                          echo ucwords($username);
                        }else{
                          echo "Not Logged in &nbsp;&nbsp;";
                        }
                      
                    ?>
                  </span>
                  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?=$baseUrl?>admin/settings/" class="<?=(($tab=="settings")?("active"):(""))?>"><i class="icon-cog"></i> Settings</a></li>
                  <li><a href="<?=$baseUrl?>admin/users/" class="<?=(($tab=="users")?("active"):(""))?>"><i class="icon-user"></i> Users</a></li>
                  <li class="divider"></li>
                  <li><a href='<?=$baseUrl?>admin/authentication/logout'><i class="icon-key"></i> Log Out</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div id="container" class="row-fluid">
    
      <div id='containerInner'>
