<?php
@set_time_limit(0);
date_default_timezone_set("UTC");
ini_set("display_errors", 1);
error_reporting(E_ALL);
ob_implicit_flush(true);
ob_start();
?>
<!DOCTYPE html>
<html>
  <head><title>Setting Up Development Environment</title></head>
  <body>
    <pre id="mainOutput" style='width:60%;margin:0 auto;overflow:auto;height:400px'></pre>
  </body>
</html>
<?php
ob_flush();
function outPut($msg=""){
  usleep(10000);
  echo "<script type='text/javascript'>
  var output=document.getElementById('mainOutput');
  var ih=output.innerHTML;
  output.innerHTML=ih + '".strip_tags($msg)."' + '\\n';
  output.scrollTop = output.scrollHeight;
  </script>";
  ob_flush();
}

$PATHS=array(
  "controllers"=>"../application/controllers/admin/",
  "views"=>"../application/views/admin/",
  "models"=>"../application/models/",
  "js"=>"../assets/js/admin/",
  "scss"=>"../assets/css/source/",
  "css"=>"../assets/css/admin/",

  "modules"=>"../modules/"
);

// Check recursive linker script permission
$linkable=false;
$perm=substr(sprintf('%o', fileperms('./recursive_ln')), -4);
if($perm!=="0755"){
  if(FALSE!==chmod("./recursive_ln",0755)){
    $linkable=true;
  }
}else{
  $linkable=true;
}

if($linkable){
  $path=$PATHS["modules"];
  $dh=opendir($path);
  while(false!==($file=readdir($dh))){
    if(is_dir($path.$file) && substr($file,0,1)!="."){
      outPut("Opened ".$path.$file);
      $dh2=opendir($path.$file);
      while(false!==($file2=readdir($dh2))){
        if(is_dir($path.$file."/".$file2) && substr($file2,0,1)!="."){
          if(
            $file2=="controllers" ||
            $file2=="models" ||
            $file2=="views" ||
            $file2=="css" ||
            $file2=="js" ||
            $file2=="scss"
          ){
            outPut(">>Opened ".$path.$file."/".$file2);
            $pth=$PATHS[$file2];
            $dh3=opendir($path.$file."/".$file2);
            while(false!==($file3=readdir($dh3))){
              if(substr($file3,0,1)!="."){
                $cmd="./recursive_ln ".$path.$file."/".$file2."/".$file3." ".$pth.$file3;
                outPut($cmd);
                exec($cmd, $res);
                if(count($res)>0){
                  outPut(implode(", ",$res));
                }
              }
            }
          }

        }
      }

    }
  }
}else{
  outPut("The linker file `recursive_ln` is not executable, attempts to make it executable were unsuccessful");
  outPut("Please make it chmod 0755");
}




outPut("----------------DONE!!!----------------");
?>
