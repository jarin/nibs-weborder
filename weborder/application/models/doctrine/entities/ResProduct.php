<?php

namespace entities;

/**
 * ResProduct
 *
 * @Table(name="res_product")
 * @Entity
 */
class ResProduct
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $productName
   *
   * @Column(name="product_name", type="string", length=150, nullable=false)
   */
  private $productName;

  /**
   * @var string $longName
   *
   * @Column(name="long_name", type="string", length=250, nullable=true)
   */
  private $longName;
  
  /**
   * @var string $description
   *
   * @Column(name="description", type="string", length=250, nullable=true)
   */
  private $description;

  /**
   * @var decimal $price
   *
   * @Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $price=0;
  
  /**
   * @var decimal $takeawayPrice
   *
   * @Column(name="takeaway_price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $takeawayPrice=0;
  
  /**
   * @var decimal $webOrderPrice
   *
   * @Column(name="web_order_price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $webOrderPrice=0;
  
  /**
   * @var decimal $deliveryPrice
   *
   * @Column(name="delivery_price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $deliveryPrice=0;
  
  /**
   * @var decimal $waitingPrice
   *
   * @Column(name="waiting_price", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $waitingPrice=0;

  /**
   * @var boolean $isFavourite
   *
   * @Column(name="is_favourite", type="boolean", nullable=false)
   */
  private $isFavourite=FALSE;
  
  /**
   * @var boolean $isBarFavourite
   *
   * @Column(name="is_bar_favourite", type="boolean", nullable=false)
   */
  private $isBarFavourite=FALSE;
  
  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;
  
  /**
   * @var ResCategory
   *
   * @ManyToOne(targetEntity="ResCategory")
   * @JoinColumns({
   *   @JoinColumn(name="category_id", referencedColumnName="id", onDelete="RESTRICT", onUpdate="CASCADE")
   * })
   */
  private $category;
  
  /**
   * @var ResPrepLocation
   *
   * @ManyToOne(targetEntity="ResPrepLocation")
   * @JoinColumns({
   *   @JoinColumn(name="prep_location_id", referencedColumnName="id", onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $prepLocation;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var boolean $isTaxInclusive
   *
   * @Column(name="is_tax_inclusive", type="boolean", nullable=false)
   */
  private $isTaxInclusive=FALSE;
  
  /**
   * @var boolean $isTaxable
   *
   * @Column(name="is_taxable", type="boolean", nullable=false)
   */
  private $isTaxable=FALSE;
  
  /**
   * @var smallint $sequenceNumber
   *
   * @Column(name="sequence_number", type="smallint", nullable=false)
   */
  private $sequenceNumber;
  
  /**
   * @var boolean $isStockControlled
   *
   * @Column(name="is_stock_controlled", type="boolean", nullable=false)
   */
  private $isStockControlled=FALSE;
  
  /**
   * @var decimal $quantity
   *
   * @Column(name="quantity", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $quantity=0;
  
  /**
   * @var ResQuantityType
   *
   * @ManyToOne(targetEntity="ResQuantityType")
   * @JoinColumns({
   *   @JoinColumn(name="quantity_type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $quantityType;
  
  /**
   * @var decimal $stock
   *
   * @Column(name="stock", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $stock=0;
  
  /**
   * @var ResStockQuantityType
   *
   * @ManyToOne(targetEntity="ResQuantityType")
   * @JoinColumns({
   *   @JoinColumn(name="stock_quantity_type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $stockQuantityType;
  
  /**
   * @var bigint $restaurantId
   *
   * @Column(name="restaurant_id", type="bigint", nullable=true)
   */
  private $restaurantId;
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set productName
   *
   * @param string $productName
   */
  public function setProductName($productName)
  {
    $this->productName = $productName;
  }

  /**
   * Get productName
   *
   * @return string 
   */
  public function getProductName()
  {
    return $this->productName;
  }

  /**
   * Set longName
   *
   * @param string $longName
   */
  public function setLongName($longName)
  {
    $this->longName = $longName;
  }

  /**
   * Get longName
   *
   * @return string 
   */
  public function getLongName()
  {
    return $this->longName;
  }
  
  /**
   * Set description
   *
   * @param string $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * Get description
   *
   * @return string 
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Set price
   *
   * @param decimal $price
   */
  public function setPrice($price)
  {
    $this->price = $price;
  }

  /**
   * Get price
   *
   * @return decimal 
   */
  public function getPrice()
  {
    return $this->price;
  }
  
  /**
   * Set takeawayPrice
   *
   * @param decimal $takeawayPrice
   */
  public function setTakeawayPrice($takeawayPrice)
  {
  	$this->takeawayPrice = $takeawayPrice;
  }
  
  /**
   * Get takeawayPrice
   *
   * @return decimal
   */
  public function getTakeawayPrice()
  {
  	return $this->takeawayPrice;
  }
  
  /**
   * Set webOrderPrice
   *
   * @param decimal $webOrderPrice
   */
  public function setWebOrderPrice($webOrderPrice)
  {
  	$this->webOrderPrice = $webOrderPrice;
  }
  
  /**
   * Get webOrderPrice
   *
   * @return decimal
   */
  public function getWebOrderPrice()
  {
  	return $this->webOrderPrice;
  }
  
  /**
   * Set deliveryPrice
   *
   * @param decimal $deliveryPrice
   */
  public function setDeliveryPrice($deliveryPrice)
  {
  	$this->deliveryPrice = $deliveryPrice;
  }
  
  /**
   * Get deliveryPrice
   *
   * @return decimal
   */
  public function getDeliveryPrice()
  {
  	return $this->deliveryPrice;
  }
  
  /**
   * Set waitingPrice
   *
   * @param decimal $waitingPrice
   */
  public function setWaitingPrice($waitingPrice)
  {
  	$this->waitingPrice = $waitingPrice;
  }
  
  /**
   * Get waitingPrice
   *
   * @return decimal
   */
  public function getWaitingPrice()
  {
  	return $this->waitingPrice;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }
  
  /**
   * Set isFavourite
   *
   * @param boolean $isFavourite
   */
  public function setIsFavourite($isFavourite)
  {
  	$this->isFavourite = $isFavourite;
  }
  
  /**
   * Get isFavourite
   *
   * @return boolean
   */
  public function getIsFavourite()
  {
  	return $this->isFavourite;
  }
  
  /**
   * Set isBarFavourite
   *
   * @param boolean $isBarFavourite
   */
  public function setIsBarFavourite($isBarFavourite)
  {
  	$this->isBarFavourite = $isBarFavourite;
  }
  
  /**
   * Get isBarFavourite
   *
   * @return boolean
   */
  public function getIsBarFavourite()
  {
  	return $this->isBarFavourite;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set category
   *
   * @param ResCategory $category
   */
  public function setCategory(\ResCategory $category)
  {
    $this->category = $category;
  }

  /**
   * Get category
   *
   * @return ResCategory 
   */
  public function getCategory()
  {
    return $this->category;
  }
  
  /**
   * Set prepLocation
   *
   * @param ResPrepLocation $prepLocation
   */
  public function setPrepLocation(\ResPrepLocation $prepLocation)
  {
  	$this->prepLocation = $prepLocation;
  }
  
  /**
   * Get prepLocation
   *
   * @return ResPrepLocation
   */
  public function getPrepLocation()
  {
  	return $this->prepLocation;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
  
  /**
   * Set isTaxInclusive
   *
   * @param boolean $isTaxInclusive
   */
  public function setIsTaxInclusive($isTaxInclusive)
  {
  	$this->isTaxInclusive = $isTaxInclusive;
  }
  
  /**
   * Get isTaxInclusive
   *
   * @return boolean
   */
  public function getIsTaxInclusive()
  {
  	return $this->isTaxInclusive;
  }
  
  /**
   * Set sequenceNumber
   *
   * @param smallint $sequenceNumber
   */
  public function setSequenceNumber($sequenceNumber)
  {
  	$this->sequenceNumber = $sequenceNumber;
  }
  
  /**
   * Get sequenceNumber
   *
   * @return smallint
   */
  public function getSequenceNumber()
  {
  	return $this->sequenceNumber;
  }
  
  /**
   * Set isTaxable
   *
   * @param boolean $isTaxable
   */
  public function setIsTaxable($isTaxable)
  {
  	$this->isTaxable = $isTaxable;
  }
  
  /**
   * Get isTaxable
   *
   * @return boolean
   */
  public function getIsTaxable()
  {
  	return $this->isTaxable;
  }
  
  /**
   * Set isStockControlled
   *
   * @param boolean $isStockControlled
   */
  public function setIsStockControlled($isStockControlled)
  {
      $this->isStockControlled = $isStockControlled;
  }
  
  /**
   * Get isStockControlled
   *
   * @return boolean
   */
  public function getIsStockControlled()
  {
      return $this->isStockControlled;
  }
  
  /**
   * Set quantity
   *
   * @param decimal $quantity
   */
  public function setQuantity($quantity)
  {
      $this->quantity = $quantity;
  }
  
  /**
   * Get quantity
   *
   * @return decimal
   */
  public function getQuantity()
  {
      return $this->quantity;
  }
  
  /**
   * Set quantityType
   *
   * @param ResQuantityType $quantityType
   */
  public function setQuantityType(\ResQuantityType $quantityType)
  {
      $this->quantityType = $quantityType;
  }
  
  /**
   * Get quantityType
   *
   * @return ResQuantityType
   */
  public function getQuantityType()
  {
      return $this->quantityType;
  }
  
  /**
   * Set stock
   *
   * @param decimal $stock
   */
  public function setStock($stock)
  {
      $this->stock = $stock;
  }
  
  /**
   * Get stock
   *
   * @return decimal
   */
  public function getStock()
  {
      return $this->stock;
  }
  
  /**
   * Set stockQuantityType
   *
   * @param ResQuantityType $stockQuantityType
   */
  public function setStockQuantityType(\ResQuantityType $stockQuantityType)
  {
      $this->stockQuantityType = $stockQuantityType;
  }
  
  /**
   * Get stockQuantityType
   *
   * @return ResQuantityType
   */
  public function getStockQuantityType()
  {
      return $this->stockQuantityType;
  }
  
  /**
   * Set restaurantId
   *
   * @param bigint $restaurantId
   */
  public function setRestaurantId($restaurantId)
  {
  	$this->restaurantId = $restaurantId;
  }
  
  /**
   * Get restaurantId
   *
   * @return bigint
   */
  public function getRestaurantId()
  {
  	return $this->restaurantId;
  }
}