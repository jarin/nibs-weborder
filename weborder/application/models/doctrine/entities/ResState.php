<?php



namespace entities;

/**
 * ResState
 *
 * @Table(name="res_state")
 * @Entity
 */
class ResState
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $stateName
   *
   * @Column(name="state_name", type="string", length=150, nullable=false)
   */
  private $stateName;


  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set stateName
   *
   * @param string $stateName
   */
  public function setStateName($stateName)
  {
    $this->stateName = $stateName;
  }

  /**
   * Get stateName
   *
   * @return string 
   */
  public function getStateName()
  {
    return $this->stateName;
  }
}