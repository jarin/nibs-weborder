<?php



namespace entities;

/**
 * ResOrderActionType
 *
 * @Table(name="res_order_action_type")
 * @Entity
 */
class ResOrderActionType
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $actionType
   *
   * @Column(name="action_type", type="string", length=150, nullable=false)
   */
  private $actionType;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set actionType
   *
   * @param string $actionType
   */
  public function setActionType($actionType)
  {
    $this->actionType = $actionType;
  }

  /**
   * Get actionType
   *
   * @return string 
   */
  public function getActionType()
  {
    return $this->actionType;
  }
}