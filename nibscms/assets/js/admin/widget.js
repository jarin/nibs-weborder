$(function(){

});
function renderCharts(){
  var chartable=$("table.chartable");
  if(chartable.length){
    chartable.each(function(){
      var _this=$(this),thead=_this.find("thead"),tbody=_this.find("tbody");
      var data=new google.visualization.DataTable(),
      map=[];
      //get the data
      thead.find("tr").eq(0).find("th,td").each(function(i,el){
        var _this=$(this);
        if(_this.is("th")){
          data.addColumn('string', _this.text());
          map.push("string");
        }else{
          data.addColumn('number', _this.text());
          map.push("number")
        }
      });
      tbody.find("tr").each(function(){
        var _this=$(this),temp=[];
        _this.find("td,th").each(function(i,el){
          var _t=$(this);
          var val=map[i]=="string"?_t.text():parseInt(_t.text(),10);
          temp.push(val);
        });
        data.addRow(temp);
      });
      //end data getting
      //render the chart
      var w=_this.width(),h=280;
      var chrt=_this.after("<div class='chart'></div>").next();
      _this.remove();
      var chart = new google.visualization.AreaChart(chrt[0]);
      chart.draw(data, {"width": w, "height": h, "backgroundColor": "#dde2eb"});
    });
  }

}