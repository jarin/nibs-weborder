<?php



namespace entities;

/**
 * ResReservationStatus
 *
 * @Table(name="res_reservation_status")
 * @Entity
 */
class ResReservationStatus
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $status
   *
   * @Column(name="status", type="string", length=150, nullable=false)
   */
  private $status;
  

  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set status
   *
   * @param string $status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * Get status
   *
   * @return string 
   */
  public function getStatus()
  {
    return $this->status;
  }
  
}