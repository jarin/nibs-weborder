       <div id="page-content">
       
		 <?php $this->load->view('top_content');?>

			<!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-9 col-sm-12 col-md-push-0">
    				     
				                    <?php if($this->session->userdata('cart') !=''){
                                        ?>
                                        <div class="my-check-right" style="border:0px;">
                                            <h5 class="">My Check</h5>
                                            <div class=" ">
                                        <ul class="list-unstyled">
                                            <li>
                                                <p>
                                                <span>Product Name</span>
                                                <span class="checkoutQty">Quantity</span>
                                                <span class="checkoutPrice">Price</span>
                                                <span class="checkoutTotal">Sub-Total</span>
                                                </p>
                                            </li>
                                            <?php 
                                            $total = 0;
                                            foreach ($this->session->userdata('cart') as $key => $cartItem){
                                            $total = $total + $cartItem['total_price'];
                                             ?>
											<li>
												<p>
												<span><?=($key+1).'. '?></span>
												<?php echo $cartItem['product_name']?>
													<span class="checkoutQty"><?php echo $cartItem['quantity']?>x</span><span class="price checkoutPrice">&pound; <?php echo $cartItem['price']?></span>
												
												<span class="price checkoutTotal">&pound; <?php echo $cartItem['total_price']?></span>
												</p>
											</li>
											<?php } ?>
											<li>
												<!-- list for total price-->
												<p>Sub Total</p>
												<p class="price-total">&pound; <?php echo $total?></p>
											</li>
											<?php if($discount > 0) {?>
											<li>
												<!-- list for total price-->
												<p>Discount</p>
												<p class="price-total">(-)&pound; <?php echo $discount?></p>
											</li>
											<?php }?>

											<li>
												<!-- list for total price-->
												<p>Total</p>
												<p class="price-total">&pound; <?php echo $total - $discount?></p>
											</li>
										</ul>
								    </div>
                             </div>
                             <?php 
                             if($gateway['mode'] == 'test'){
                             	$paypalUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
                             } else {
                             	$paypalUrl = 'https://www.paypal.com/cgi-bin/webscr';
                             }
                             ?>
                            <form id="confirmorder" method="POST" action="<?=$paypalUrl?>">
                                <input type="hidden" name="customer_id" value="<?=$this->session->userdata("userid")?>"/>
                                <input type="hidden" name="cmd" value="_cart"/>
                                <input type="hidden" name="business" value="<?=$gateway['merchant_id']?>"/>
                                <input type="hidden" name="upload" value="1"/>
                                <input type="hidden" name="currency_code" value="GBP"/>
                                <input type="hidden" name="rm" value="2"/>
                                <input type="hidden" name="discount_amount_cart" value="<?=$discount?>"/>
                                <input type="hidden" name="return" value="<?=base_url().'payment/paypal/success'?>"/>
                                <input type="hidden" name="cancel_return" value="<?=base_url().'payment/paypal/cancel'?>"/>
                                <input type="hidden" name="notify_url" value="<?=base_url().'payment/paypal/notify'?>"/>
                                <?php 	$i=1;
			    	           foreach ($this->session->userdata('cart') as $key => $cart){
			    	           
			    	           	echo ' <input type="hidden" name="item_name_'.$i.'" value='.$cart['product_name'].' />';
			    	           	echo ' <input type="hidden" name="amount_'.$i.'" value='.$cart['price'].' />';
			    	           	echo ' <input type="hidden" name="quantity_'.$i.'" value='.$cart['quantity'].' />';
							            $i++;
						        }
						        
				             ?>
                              <div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Order Type</h4>
											<span class="radio-input">
												<input type="radio" id="collection" value="collection" name="type" checked>
												<label for="collection">Collection</label>
											</span>
											
											<span class="radio-input">
												<input type="radio" id="delivery" value="delivery" name="type">
												<label for="delivery">Delivery</label>
											</span>
									    </div>
									</div>
							</div>
							
							<div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Payment Type</h4>
                                         	<span class="radio-input">
												<input type="radio" id="paypal" value="paypal" name="payment" checked>
												<label for="paypal"><i class="fa fa-cc-paypal"></i> Paypal</label>
											</span>
											<span class="radio-input">
												<input type="radio" id="paycollection" value="payonCollection" name="payment" >
												<label for="paycollection"> Pay On Collection</label>
											</span>
											
											<span class="radio-input">
												<input type="radio" id="paydelivery" value="payonDelivery" name="payment" >
												<label for="paydelivery"> Pay On Delivery</label>
											</span>
											
											
									    </div>
									</div>
							</div>
														
                             <div class="page-content">
                                 <div class="contact-us">
                                     <div class="send-message" style="border:0px;">
                                        <h4>Customer Info</h4>
                                       
                                          <div class="row">
                                            <?php if(count($user) > 0){
                                                
                                                ?>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Name*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Name*" name="name" value="<?=$user['customerName']?>" data-rule-required="true"  data-msg-required="Please enter your name">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>House No*:</label>
                                            </div> 
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House No*" name="house_no" value="<?=$user['houseNo']?>" data-rule-required="true"  data-msg-required="Please enter your house no">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>House Name:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House Name" name="house_name" value="<?=$user['houseName']?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Street*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Street*" name="street" value="<?=$user['street']?>" data-rule-required="true"  data-msg-required="Please enter your street">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Town*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Town*" name="town" value="<?=$user['town']['cityName']?>" data-rule-required="true"  data-msg-required="Please enter your town">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Post Code*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Post Code*" name="post_code" value="<?=$user['postCode']?>" data-rule-required="true"  data-msg-required="Please enter your post code">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country / State*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                             <input type="text" placeholder="Country / State*" name="state" value="<?=$user['state']['stateName']?>" data-rule-required="true"  data-msg-required="Please enter your state">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                             <input type="text" placeholder="Country" name="country_default" value="<?=$user['country']['countryName']?>" data-rule-required="true"  data-msg-required="Please enter your country">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Phone*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Phone*" name="phone" value="<?=$user['mobile']?>" data-rule-required="true"  data-msg-required="Please enter your phone number">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Email*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6" style="padding-right:0px;">
                                              <input type="email" placeholder="Email*" name="email" value="<?=$user['email']?>" data-rule-required="true" data-rule-email="true" data-msg-required="Please enter your email address" data-msg-email="Please enter a valid email address">
                                            </div>
                                            <?php } ?>
                                          
                                          <!-- end nasted .row -->
                                          <!--  <div class="col-md-6 col-sm-6">
                                                <label>Your message:</label>
                                            </div>
                                        <textarea placeholder="Your message" name="message"></textarea>-->
                                        </div>
                                          
                                        
                                      </div>
                                       <!-- end .send-message -->
                                  </div>
                              </div>
              
							<a class="btn btn-default-black " href="order/confirm" style="padding: 7px 15px;">Back</a>
                    	    <button class="btn btn-default-red" type="submit"  style="float:right;margin-top:15px"><i class="fa fa-money"></i> Confirm & Payment</button>
						</form>
						
                        <script type="text/javascript">
                                             
                        //$("body").on("click", "input[name='payment']", function(e){
                        	 $("input[name='payment']").change(function(){
                        	var _this=$(this),
                        		payment = _this.val(),
                        		payString = 'payon';//alert(payment);
                        		if(payment.indexOf(payString) != -1){
                        			var ar = payment.split('payon');
                        			if(ar[1]){
                            			
                        				$("input[name='type']").parents().removeClass('active');
                        				
                        				$('#'+ar[1].toLowerCase()).parent().addClass('active');
                        				$('#'+ar[1].toLowerCase()).prop('checked', true);
                        				$("input[name='type']").not($('#'+ar[1].toLowerCase()))
                        	            .prop('checked', false);
                        				$('form#confirmorder').attr('action','order/confirmorder');
                        			}
                        		} else if(payment == 'paypal'){
                        			$('form#confirmorder').attr('action','<?php echo $paypalUrl;?>');
                        		}
                        });
                        </script>
							
							<?php } else { ?>
							<p style="text-align: center;">You do not have any Items</p>
							<?php } ?>
										

                           

				     
					</div>
			            <!-- end .col-md-9 -->

					<?php $this->load->view('left_sidebar')?>
				</div>
				<!-- end .row -->
			</div>
			<!--end .container -->