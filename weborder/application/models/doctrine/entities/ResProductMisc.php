<?php



namespace entities;

/**
 * ResProductMisc
 *
 * @Table(name="res_product_misc")
 * @Entity
 */
class ResProductMisc
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResOrderChild
   *
   * @ManyToOne(targetEntity="ResOrderChild")
   * @JoinColumns({
   *   @JoinColumn(name="order_child_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $orderChild;
  
  /**
   * @var ResPrepLocation
   *
   * @ManyToOne(targetEntity="ResPrepLocation")
   * @JoinColumns({
   *   @JoinColumn(name="prep_location_id", referencedColumnName="id", onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $prepLocation;

  /**
   * @var string $product
   *
   * @Column(name="product", type="string", length=500, nullable=true)
   */
  private $product;
  
  /**
   * @var decimal $price
   *
   * @Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $price;
  
  /**
   * @var smallint $quantity
   *
   * @Column(name="quantity", type="smallint", nullable=false)
   */
  private $quantity;
  
  /**
   * @var ResQuantityType
   *
   * @ManyToOne(targetEntity="ResQuantityType")
   * @JoinColumns({
   *   @JoinColumn(name="quantity_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $quantityTypeId;


  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set orderChild
   *
   * @param ResOrderChild $orderChild
   */
  public function setOrderChild(\ResOrderChild $orderChild)
  {
    $this->orderChild = $orderChild;
  }

  /**
   * Get orderChild
   *
   * @return ResOrderChild 
   */
  public function getOrderChild()
  {
    return $this->orderChild;
  }
  
  /**
   * Set prepLocation
   *
   * @param ResPrepLocation $prepLocation
   */
  public function setPrepLocation(\ResPrepLocation $prepLocation)
  {
  	$this->prepLocation = $prepLocation;
  }
  
  /**
   * Get prepLocation
   *
   * @return ResPrepLocation
   */
  public function getPrepLocation()
  {
  	return $this->prepLocation;
  }

  /**
   * Set product
   *
   * @param string $product
   */
  public function setProduct($product)
  {
  	$this->product = $product;
  }
  
  /**
   * Get product
   *
   * @return string
   */
  public function getProduct()
  {
  	return $this->product;
  }
  
  /**
   * Set price
   *
   * @param decimal $price
   */
  public function setPrice($price)
  {
  	$this->price = $price;
  }
  
  /**
   * Get price
   *
   * @return decimal
   */
  public function getPrice()
  {
  	return $this->price;
  }
  
  /**
   * Set quantity
   *
   * @param smallint $quantity
   */
  public function setQuantity($quantity)
  {
  	$this->quantity = $quantity;
  }
  
  /**
   * Get quantity
   *
   * @return smallint
   */
  public function getQuantity()
  {
  	return $this->quantity;
  }
  
  /**
   * Set quantityTypeId
   *
   * @param boolean $quantityTypeId
   */
  public function setQuantityTypeId($quantityTypeId)
  {
  	$this->quantityTypeId = $quantityTypeId;
  }
  
  /**
   * Get quantityTypeId
   *
   * @return boolean
   */
  public function getQuantityTypeId()
  {
  	return $this->quantityTypeId;
  }
}