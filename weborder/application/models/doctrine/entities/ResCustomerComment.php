<?php



namespace entities;

/**
 * ResCustomerComment
 *
 * @Table(name="res_customer_comment")
 * @Entity
 */
class ResCustomerComment
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var string $comments
   *
   * @Column(name="comments", type="string", length=500, nullable=false)
   */
  private $comments;

  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $customer;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;
  
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set comments
   *
   * @param string $comments
   */
  public function setComments($comments)
  {
      $this->comments = $comments;
  }
  
  /**
   * Get comments
   *
   * @return string
   */
  public function getComments()
  {
      return $this->comments;
  }

  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
      $this->customer = $customer;
  }
  
  /**
   * Get customer
   *
   * @return ResCustomer
   */
  public function getCustomer()
  {
      return $this->customer;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
      $this->lastUpdatedBy = $lastUpdatedBy;
  }
  
  /**
   * Get lastUpdatedBy
   *
   * @return ResUser
   */
  public function getLastUpdatedBy()
  {
      return $this->lastUpdatedBy;
  }
  
  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
      $this->lastUpdate = $lastUpdate;
  }
  
  /**
   * Get lastUpdate
   *
   * @return datetime
   */
  public function getLastUpdate()
  {
      return $this->lastUpdate;
  }

}