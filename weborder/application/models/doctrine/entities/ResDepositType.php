<?php



namespace entities;

/**
 * ResDepositType
 *
 * @Table(name="res_deposit_type")
 * @Entity
 */
class ResDepositType
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $depositType
   *
   * @Column(name="deposit_type", type="string", length=150, nullable=false)
   */
  private $depositType;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set depositType
   *
   * @param string $depositType
   */
  public function setDepositType($depositType)
  {
    $this->depositType = $depositType;
  }

  /**
   * Get depositType
   *
   * @return string 
   */
  public function getDepositType()
  {
    return $this->depositType;
  }
}