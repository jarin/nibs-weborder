<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model{
  function __construct(){
    parent::__construct();
    $this->tablename="";
    $this->id_field="id";
    //$this->db->query('SET names utf8');
  }

  function count(){
    return $this->db->from($this->tablename)->count_all_results();
  }

  function nextSortOrderValue($column=null,$value=null){
    if($column==null){
      $res=$this->db->query("select max(sort_order) as sort_order from $this->tablename")->result_array();
    }else{
      $res=$this->db->query("select max(sort_order) as sort_order from $this->tablename where $column='$value'")->result_array();
    }
    if(isset($res[0]["sort_order"])){
      return $res[0]["sort_order"]+1;
    }else{
      return 0;
    }
  }

  function getAll($order_by="",$direction="asc"){
    if(trim($order_by)==""){
      return $this->db->get($this->tablename)->result_array();
    }else{
      return $this->db->order_by($order_by,$direction)->get($this->tablename)->result_array();
    }

  }

  function getVisible($order_by="",$direction="asc"){
    if(trim($order_by)==""){
      return $this->db->where("active",1)
      ->get($this->tablename)->result_array();
    }else{
      return $this->db->where("active",1)->order_by($order_by,$direction)
      ->get($this->tablename)->result_array();
    }
  }

  function get($id){
    if(trim($id)==""){
      return array();
    }
    return $this->db->where($this->id_field,$id)
    ->get($this->tablename)->row_array();
  }

  function getRowBy($field,$value){
    if(trim($field)==""){
      return array();
    }
    return $this->db->where($field,$value)
    ->get($this->tablename)->row_array();
  }

  function getBy($field,$value){
    if(trim($field)==""){
      return array();
    }
    return $this->db->where($field,$value)
    ->get($this->tablename)->result_array();
  }

  function create($arr){
    $res=$this->db->insert($this->tablename,$arr);       
    return $this->db->insert_id();
  }

  function save($id,$arr){
    return $this->db->where($this->id_field,$id)->update($this->tablename,$arr);
  }

  function delete($id){
    return $this->db->where($this->id_field,$id)->delete($this->tablename);
  }

  function reorder($arr,$reverse=false){
    return $this->_multiSort($this->tablename,$arr,$reverse);
  }

  function search($term,$fields=array(),$extraFilter=""){
    $results=array();
    $term=mysql_real_escape_string($term);
    $matchStrings=array();
    $whereStrings=array();
    foreach ($fields as $field => $rating) {
      $matchStrings[]="(MATCH ($field) AGAINST ('$term' IN BOOLEAN MODE)*".$rating.")";
      $whereStrings[]=$field;
    }
    $matchString=implode(" + ",$matchStrings). " AS rating";
    $whereString="MATCH (".implode(",",$whereStrings).") AGAINST ('$term' IN BOOLEAN MODE)";
    $sql= "SELECT *,$matchString FROM $this->tablename WHERE $whereString $extraFilter ORDER BY rating DESC";
    $results=$this->db->query($sql)->result_array();
    return $results;
  }

  //-----------------------------------------------
  function _multiSort($tablename,$array,$reverse=false){
    if(is_array($array)){
      if($reverse){
        $start=count($array) - 1;
      }
      foreach($array as $order=>$id){
        if($reverse){
          $order=$start-$order;
        }
        @$this->db->update($tablename,array("sort_order"=>$order),array($this->id_field=>$id));
      }
      return true;
    }
    return false;
  }

  function countBy($where=array()){
  
  	if(count($where) > 0){
  		foreach ($where as $f => $v){
  			$this->db->where($f,$v);
  		}
  	}
  	
  	return $this->db->from($this->tablename)->count_all_results();
  	// echo $this->db->last_query();
  	
  }
  

  function getAValue($tablename, $field, $whereAry=array(),$join=array()){
  	
  	$this->db->select($field);
  	
  	if (count($join)>0) {
            foreach ($join as $k=>$v) {
                $this->db->join($k, $v, 'left');
            }
    }
  
 	if(count($whereAry) > 0){
  		foreach ($whereAry as $f => $v){
  			$this->db->where($f,$v);
  		}
  	}
  
  	$result = $this->db->get($tablename)->row_array();
  	
  	if(count($result)>0){
  		return $result[$field];
  	}else{
  		return false;
  	}
  }
  
  /**
   * Get multiple rows from table(s)
   *
   * @param string $select   comma separated field list
   * @param array  $join     array of joined tables if any
   * @param array  $criteria array of criteria
   * @param string $order_by comma separated order by string
   *
   * @return array
   */
  function getRows($select="*", $join=array(), $criteria=array(), $order_by='')
  {
  	$this->_commonInGetRows($select, $join, $criteria);
  
  	
  	if ($order_by!='') {
  		$this->db->order_by($order_by);
  	}
  	
  	$result=$this->db->get()->result_array();
  	//echo $this->db->last_query();exit;
  	
  
  	return $result;
  }
  
  /**
   * private function to construct where clause
   *
   * @param string $select   comma separated field list
   * @param array  $join     array of tables which may be in join
   * @param array  $criteria array of criteria to construct where clause
   *
   * @return void
   */
  private function _commonInGetRows($select="*", $join=array(), $criteria=array())
  {
  	
  	$this->db->_protect_identifiers=false;
  
  	if ($select=="distinct") {
  		$this->db->distinct();
  	} else {
  		if ($select!="*") {
  			$this->db->select($select);
  		}
  	}
  	
  	if (is_array($this->tablename)) {
  		foreach ($this->tablename as $tablename) {
  			$this->db->from($tablename);
  		}
  	} else {
  		$this->db->from($this->tablename);
  	}
  
  	if (count($join)>0) {
  		foreach ($join as $k=>$v) {
  			$this->db->join($k, $v, 'left');
  		}
  	}
  
  	if (count($criteria)>0) {
  		foreach ($criteria as $k=>$v) {
  			switch ($k) {
  				case "between":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							list($from, $to)=explode(" AND ", $value);
  							$bet=" '".$from."' AND '".$to."'";
  							$this->db->where($field, $bet, false);
  						}
  					}
  					break;
  				case "where":
  					if (is_array($v)) {
  						if (count($v)>0) {
  							foreach ($v as $field=>$value) {
  								$this->db->where($field, $value);
  							}
  						}
  					} else {
  						$this->db->where($v, null, false);
  					}
  					break;
  				case "or_where":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->or_where($field, $value);
  						}
  					}
  					break;
  				case "where_in":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->where_in($field, $value);
  						}
  					}
  					break;
  				case "or_where_in":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->or_where_in($field, $value);
  						}
  					}
  					break;
  				case "where_not_in":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->where_not_in($field, $value);
  						}
  					}
  					break;
  				case "or_where_not_in":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->or_where_not_in($field, $value);
  						}
  					}
  					break;
  				case "like":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->like($field, $value);
  						}
  					}
  					break;
  				case "or_like":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->or_like($field, $value);
  						}
  					}
  					break;
  				case "not_like":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->not_like($field, $value);
  						}
  					}
  					break;
  				case "or_not_like":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->or_not_like($field, $value);
  						}
  					}
  					break;
  				case "group_by":
  					if (count($v)>0) {
  						foreach ($v as $field=>$value) {
  							$this->db->group_by($value);
  						}
  					}
  					break;
  				case "having":
  					if (is_array($v)) {
  						if (count($v)>0) {
  							foreach ($v as $field=>$value) {
  								$this->db->having($field, $value);
  							}
  						}
  					} else {
  						$this->db->having($v);
  					}
  					break;
  			}
  		}
  	}
  }
  

}
