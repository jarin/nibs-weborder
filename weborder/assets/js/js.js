$('.datepicker').datepicker({});

$("input[name=state],input[name=delivery_state]").autocomplete({
	serviceUrl: 'autocomplete/States',
	type: 'POST',
	noCache: true,
	autoSelectFirst: true,
	minChars:1,
	maxHeight:400,
	width:'auto',
	zIndex: 9999,
	dataType: 'json'
});

$("input[name=country], input[name=country_default],input[name=delivery_country_default]").autocomplete({
	serviceUrl: 'autocomplete/Countries',
	type: 'POST',
	noCache: true,
	autoSelectFirst: true,
	minChars:1,
	maxHeight:400,
	width:'auto',
	zIndex: 9999,
	dataType: 'json'
});


$("input[name=town],input[name=delivery_town]").autocomplete({
	serviceUrl: 'autocomplete/Towns',
	type: 'POST',
	noCache: true,
	autoSelectFirst: true,
	minChars:1,
	maxHeight:400,
	width:'auto',
	zIndex: 9999,
	dataType: 'json'
});

$(function() {
    /*$('#datetimepicker').datetimepicker({
      language: 'en',
      startDate: '-1d'
     
    }).on('show', function(e){
	  $('.dropdown-menu').css('opacity','1');
	  $('.dropdown-menu').css('visibility','visible');
  });*/
	
	$('.datepickerF').datepicker({
	      startDate: '1d'
	     
	    }).on('show', function(e){
		  $('.dropdown-menu').css('opacity','1');
		  $('.dropdown-menu').css('visibility','visible');
	  });
    
    $("#confirmReg").validate();
    $("#confirmorder").validate();
    
    // fixed sidebar
    
    if (document.documentElement.clientWidth > 1200 ) { 
		var topbar = $('#thumbnail-slide').outerHeight(),
	    header = $('#header').outerHeight(),
	    height = topbar + header,
	    
	    sideBarHeight = $('.left-side-panel').outerHeight();
		
		
	    if(document.documentElement.clientHeight > sideBarHeight){
	    	
        	 
	    	$(window).scroll(function() {
    	    	
	    		if(!$('.left-side-panel').hasClass('noSticky')){
	            if($(this).scrollTop() > height)
	            {
	                $('.left-side-panel').addClass('sticky');
	                
	            }
	            else if($(this).scrollTop() <= height)
	            {
	                $('.left-side-panel').removeClass('sticky');
	                
	            }
	    		}
	    	});
    	    
	    }
	}
    
    
    $("body").on("click",".addCart",function(e){
    	e.preventDefault();
    	var _this=$(this),
    	rel = parseInt(_this.attr("rel")),
    	qty = body.find("#qty_"+rel).val(),
    	price = body.find("#price_"+rel).val(),
    	actualprice = body.find("#actualprice_"+rel).val(),
    	name = body.find("#name_"+rel).val(),
    	row = body.find('.mycart'),
    	form = $('#formIngred_'+rel),
    	formdata= form.serialize();//alert(formdata);
    	
    	if(parseInt(qty) < 1 || isNaN(qty)) {
    		alert("please enter valid values");
    		body.find("#qty_"+rel).val("");
    		body.find("#qty_"+rel).focus();
    	} else {
    		
    	$.ajax({
    		async: false, 
    		type: "POST",
    		url: "order/addCart",
    		data: "pid="+ rel +"&qty="+ qty +"&price="+ price +"&name="+ name +"&actualprice="+ actualprice +"&ingredient="+ formdata,
    		success:function(data){
    		  row.html(data);
    		  form.find('input[type=checkbox]:checked').removeAttr('checked');
    			form.find('.checkbox-input').removeClass('active');
    			form.find('textarea').val('');
    			form.find('.removeIngred').removeClass('ajaxCall'); 
    			body.find("#qty_"+rel).val(1);
    		  body.find('#cartsucces').html('Item is added to your cart');
    		  body.find('#cartsucces').show(0);
    		  setTimeout(function(){ $('#cartsucces').hide(0); } , 1000);
    		  var sideBarHeight = body.find('.left-side-panel').outerHeight();
    		  if(document.documentElement.clientHeight > sideBarHeight){
    			  body.find('.left-side-panel').removeClass('noSticky');
    		  } else {
    			  body.find('.left-side-panel').addClass('noSticky');
    			  body.find('.left-side-panel').removeClass('sticky');
    		  }
    		}
    	});
    	}	
    	});
    $("body").on("click", ".editItem", function(e){
    	e.preventDefault();

    	var _this=$(this),
    	rel = parseInt(_this.attr("rel")),
    	itemId = _this.attr('data-item'),
    	row = $('.modal_content');
    	  
    	  $.ajax({
    			async: false, 
    			type: "POST",
    			url: "order/editCartItem",
    			data: "pid="+rel+"&itemId="+itemId,
    			success:function(data){
    				row.html(data);
    				body.find(".sticky").css({"z-index":'auto','position':'relative'});
    				$('#editItemModal').modal('show');
    			}
    		});
    	})
    	
    	$("body").on("click", ".updateCart", function(e){
    	e.preventDefault();
    	
    	var _this=$(this),
    	rel = parseInt(_this.attr("rel")),
    	row = $('#cartsucces'),
    	div = $('.mycart'),
    	form = $('#updateItem'),
    	qty =  $("input[name=productQty]").val(),
    	confirm = $('#confirm').val();
    	if(parseInt(qty) < 1 || isNaN(qty)) {
    		alert("please enter valid values");
    		body.find("input[name=productQty]").focus();
    	} else {
    	
    	$.ajax({
    		async: false, 
    		type: "POST",
    		url: "order/updateCartItem",
    		data: form.serialize(),
    		success:function(data){
    			$('#editItemModal').modal('hide');
    			$(".sticky").css({"z-index":1,'position':'fixed'});
    			$("body").find(".modal-backdrop").remove();
    			if(confirm == 1){
    				location.reload();
    			} else {
    				div.html(data)
    			}
    			
    	   
    		 row.html('Cart Item updated successfully');
    		 row.show(0);
    		 setTimeout(function(){ row.hide(0); } , 1000);
    		
    		}
    	});
    	}
    	});




    $("body").on("click", ".delItems", function(e){
    	e.preventDefault();
    	var _this=$(this),
    	rel = parseInt(_this.attr("rel")),
    	itemId = _this.attr('data-item'),
    	row = $('.mycart');
    	var confirm = $('#confirm').val();
    	$.ajax({
    		
    		type: "POST",
    		url: "order/removeCart",
    		data: "pid="+rel+"&itemId="+itemId,
    		success:function(data){
    			if(confirm == 1){
    				location.reload();
    			} else {
    			    row.html(data);
    			}
    			var sideBarHeight = body.find('.left-side-panel').outerHeight();
    			  if(document.documentElement.clientHeight > sideBarHeight){
    				  body.find('.left-side-panel').removeClass('noSticky');
    			  } else {
    				  body.find('.left-side-panel').addClass('noSticky');
    				  body.find('.left-side-panel').removeClass('sticky');
    			  } 
    		  $('#cartsucces').html('Item is deleted from your cart');
    		  $('#cartsucces').show(0);
    		  setTimeout(function(){ $('#cartsucces').hide(0); } , 1000);
    		}
    	});
    		
    	});


    $(document).on('hidden.bs.modal', '#editItemModal', function (event) {
    	$(".sticky").css({"z-index":1,'position':'fixed'});
    	
    	});
  });