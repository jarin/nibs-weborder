<?php



namespace entities;

/**
 * ResInvoice
 *
 * @Table(name="res_invoice")
 * @Entity
 */
class ResInvoice
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var datetime $invoiceDate
   *
   * @Column(name="invoice_date", type="datetime", nullable=false)
   */
  private $invoiceDate;

  /**
   * @var decimal $billAmount
   *
   * @Column(name="bill_amount", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $billAmount=0;

  /**
   * @var decimal $deliveryCharge
   *
   * @Column(name="delivery_charge", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $deliveryCharge=0;

  /**
   * @var decimal $tax
   *
   * @Column(name="tax", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $tax=0;

  /**
   * @var decimal $totalAmount
   *
   * @Column(name="total_amount", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $totalAmount=0;

  /**
   * @var string $notes
   *
   * @Column(name="notes", type="string", length=250, nullable=true)
   */
  private $notes;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResOrder
   *
   * @ManyToOne(targetEntity="ResOrder")
   * @JoinColumns({
   *   @JoinColumn(name="order_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $order;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;

  /**
   * @var ResPaymentMethod
   *
   * @ManyToOne(targetEntity="ResPaymentMethod")
   * @JoinColumns({
   *   @JoinColumn(name="payment_method_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $paymentMethod;


  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set invoiceDate
   *
   * @param datetime $invoiceDate
   */
  public function setInvoiceDate($invoiceDate)
  {
    $this->invoiceDate = $invoiceDate;
  }

  /**
   * Get invoiceDate
   *
   * @return datetime 
   */
  public function getInvoiceDate()
  {
    return $this->invoiceDate;
  }

  /**
   * Set billAmount
   *
   * @param decimal $billAmount
   */
  public function setBillAmount($billAmount)
  {
    $this->billAmount = $billAmount;
  }

  /**
   * Get billAmount
   *
   * @return decimal 
   */
  public function getBillAmount()
  {
    return $this->billAmount;
  }

  /**
   * Set deliveryCharge
   *
   * @param decimal $deliveryCharge
   */
  public function setDeliveryCharge($deliveryCharge)
  {
    $this->deliveryCharge = $deliveryCharge;
  }

  /**
   * Get deliveryCharge
   *
   * @return decimal 
   */
  public function getDeliveryCharge()
  {
    return $this->deliveryCharge;
  }

  /**
   * Set tax
   *
   * @param decimal $tax
   */
  public function setTax($tax)
  {
    $this->tax = $tax;
  }

  /**
   * Get tax
   *
   * @return decimal 
   */
  public function getTax()
  {
    return $this->tax;
  }

  /**
   * Set totalAmount
   *
   * @param decimal $totalAmount
   */
  public function setTotalAmount($totalAmount)
  {
    $this->totalAmount = $totalAmount;
  }

  /**
   * Get totalAmount
   *
   * @return decimal 
   */
  public function getTotalAmount()
  {
    return $this->totalAmount;
  }

  /**
   * Set notes
   *
   * @param string $notes
   */
  public function setNotes($notes)
  {
    $this->notes = $notes;
  }

  /**
   * Get notes
   *
   * @return string 
   */
  public function getNotes()
  {
    return $this->notes;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set order
   *
   * @param ResOrder $order
   */
  public function setOrder(\ResOrder $order)
  {
    $this->order = $order;
  }

  /**
   * Get order
   *
   * @return ResOrder 
   */
  public function getOrder()
  {
    return $this->order;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }

  /**
   * Set paymentMethod
   *
   * @param ResPaymentMethod $paymentMethod
   */
  public function setPaymentMethod(\ResPaymentMethod $paymentMethod)
  {
    $this->paymentMethod = $paymentMethod;
  }

  /**
   * Get paymentMethod
   *
   * @return ResPaymentMethod 
   */
  public function getPaymentMethod()
  {
    return $this->paymentMethod;
  }
}