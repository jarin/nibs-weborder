<?php

namespace entities;

/**
 * ResProductPurchaseHistory
 *
 * @Table(name="res_product_purchase_history")
 * @Entity
 */
class ResProductPurchaseHistory
{
    /**
     * @var bigint $id
     *
     * @Column(name="id", type="bigint", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var ResProduct
     *
     * @ManyToOne(targetEntity="ResProduct")
     * @JoinColumns({
     *   @JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
     * })
     */
    private $product;
    
    /**
     * @var datetime $purchaseDate
     *
     * @Column(name="purchase_date", type="datetime", nullable=false)
     */
    private $purchaseDate;
    
    /**
     * @var decimal $quantity
     *
     * @Column(name="quantity", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $quantity=0;
    
    /**
     * @var ResQuantityType
     *
     * @ManyToOne(targetEntity="ResQuantityType")
     * @JoinColumns({
     *   @JoinColumn(name="quantity_type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
     * })
     */
    private $quantityType;
    
    /**
     * @var ResUser
     *
     * @ManyToOne(targetEntity="ResUser")
     * @JoinColumns({
     *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
     * })
     */
    private $lastUpdatedBy;
    
    
    /**
     * Get id
     *
     * @return bigint
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set product
     *
     * @param ResProduct $product
     */
    public function setProduct(\ResProduct $product)
    {
        $this->product = $product;
    }
    
    /**
     * Get product
     *
     * @return ResProduct
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /**
     * Set purchaseDate
     *
     * @param datetime $purchaseDate
     */
    public function setPurchaseDate($purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;
    }
    
    /**
     * Get purchaseDate
     *
     * @return datetime
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }
    
    /**
     * Set quantity
     *
     * @param decimal $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
    
    /**
     * Get quantity
     *
     * @return decimal
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    
    /**
     * Set quantityType
     *
     * @param ResQuantityType $quantityType
     */
    public function setQuantityType(\ResQuantityType $quantityType)
    {
        $this->quantityType = $quantityType;
    }
    
    /**
     * Get quantityType
     *
     * @return ResQuantityType
     */
    public function getQuantityType()
    {
        return $this->quantityType;
    }
    
    /**
     * Set lastUpdatedBy
     *
     * @param ResUser $lastUpdatedBy
     */
    public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
    {
        $this->lastUpdatedBy = $lastUpdatedBy;
    }
    
    /**
     * Get lastUpdatedBy
     *
     * @return ResUser
     */
    public function getLastUpdatedBy()
    {
        return $this->lastUpdatedBy;
    }
}
