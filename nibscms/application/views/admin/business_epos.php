
<div  class='onepane'>
	<div class='container-fluid' style=''>
		<div class='row-fluid'>
			<div class='span12'>	
				<div class=''>
					<h3 class='page-title'>Epos Database Settings</h3><br />
					<div id='form_wizard_1' class='widget box'>

						<div class='widget-body form'>
							<div style='margin:0 auto;width:60%;text-align:center'>
								<form method='post'  action='<?=base_url()?>admin/epos/saveinfo'>
									<table cellpadding='4' cellspacing='0' align='center'>
										<tr>
											<th align='left' >Database Name</th>
											<td ><input type='text' name='db_name' value="<?php if(!empty($epos[0]['db_name'])) echo $epos[0]['db_name']; ?>" id='edit_db_name' /></td>
										</tr>
										<tr>
											<th align='left' >Host name</th>
											<td ><input type='text' name='db_host' value='<?php if(!empty($epos[0]['db_host'])) echo $epos[0]["db_host"]; ?>' id='edit_db_host' /></td>
										</tr>
										<tr>
											<th align='left' >User name</th>
											<td ><input type='text' name='db_user' value='<?php if(!empty($epos[0]['db_user'])) echo $epos[0]["db_user"]; ?>' id='google-plus_url' /></td>
										</tr>
									
										<tr>
											<th align='left' >password</th>
											<td ><input type='text' name='db_pass' value='<?php if(!empty($epos[0]['db_pass'])) echo $epos[0]["db_pass"]; ?>' id='db_pass' /></td>
										</tr>
										
										<tr>										
											<td align='right' colspan='4'>
												<button type='submit' class='button save'>Save Changes</button>
												<div id='loader<?=$business['id']?>' class='q_feedback'></div>
												<input type='hidden' name='b_id' value='<? if(!empty($business_id)) echo $business_id; ?>' />
												<input type='hidden' name='epos_id' value='<? if(!empty($epos[0]["id"])) echo $epos[0]["id"]; ?>' />
											</td>
										</tr>
									</table>
								</form></div>
							</div>
						</div></div>
					</div>
				</div>
			</div>
		</div>