<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//Codeigniter wrapper for HTML Diff

class Diff{
	
	var $HtmlDiff;
	
	function Diff(){
		require_once("htmldiff/HTMLDiff.php");
		$this->HtmlDiff=new HTMLDiffer();
	}
	
	function showDiff($v1,$v2){
		return $this->HtmlDiff->htmlDiff($v1,$v2);
	}
	
	
}




?>