<!DOCTYPE html>
<html lang="en-EN">
<head>
  <?php
  $baseUrl=base_url();
  ?>
	<meta charset="UTF-8">
  <title>Control Panel</title>
  <link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/common.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/animations.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/widget.css" type="text/css" media="screen" />
  <script type="text/javascript">
    var base_url="<?=$baseUrl?>";
  </script>
  <script type="text/javascript" src="<?=$baseUrl?>assets/js/jquery.js"></script>
  <script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/widget.js"></script>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript" charset="utf-8">
    google.load('visualization', '1.0', {'packages':['corechart']});
    google.setOnLoadCallback(renderCharts);
  </script>
</head>
<body>
  <div id="widgetMain">