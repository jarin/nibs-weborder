<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gateway extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="gateway";
		$this->load->model("activity_m","activity");
		$this->load->model("gateway_m","gateway");
		
	}
	
	public function index()
	{
		if(_checkAuthentication($this)){
		
			$gateway = $this->gateway->getAll('field_name');
			$config['paypal'] = $this->_paypalConfig();
			
			if(count($gateway) == 0){
				echo '<span style="color:red">Error: Invalid Gateway</span>';
				exit;
			}
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
			$this->load->view("admin/gateway_template",array("gateway"=>$gateway, 'config'=>$config));
			$this->load->view("admin/admin_foot");
		}
	}
	
	function _paypalConfig()
	{
		$config = array(
				
				"merchant_id" => array("FriendlyName" => "API Login ID", "Type" => "text", "Size" => "20", ),
				"merchant_secret" => array(
						"FriendlyName" => "Transaction Key",
						"Type" => "password",
						"Size" => "20",
				),
				"mode" => array(
						"FriendlyName" => "Sandbox",
						"Type" => "dropdown",
						"Options" => array('test','live'),
				),
		);
	
		return $config;
	}
	
	function save(){
		$gatewayAr=$this->input->post("gateway");
		//print_r($gatewayAr);
		foreach ($gatewayAr as $key => $val){
			if(empty($val)){
				echo '{success:"error",msg:"Empty Field is not allowed..."}';
				exit;
			}
			$updateAr["$key"]=$val;
		}
		
		if(count($updateAr) > 0){
			
			if($this->gateway->saveData($updateAr)){
				
					
				echo '{"success":"success","successFunction":function(){
							
							NIBS.notif.notify("success","Gateway details saved");
							setInterval(function(){location.reload()}, 2000);
						}}';
				exit;
			} else{
				echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update Gateway information due to a database error...")
					}}';
				exit;
			}
			
		}
		
	}
	

}