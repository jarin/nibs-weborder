$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addCustomerBtn").overlay({width:"70%",inline:true,title:"Add Customer",href:"#addCustomerForm"});

  //add line item
  $("img.adder").live("click",function(){
    var _this=$(this),tr=_this.parent().parent();
    var cloned=tr.clone();
    cloned.find("img.adder").replaceWith($("<img class='deleter' src='"+base_url+"assets/images/admin/icon_trash.png' />")).end().find("input").val("").end().find(".isOther").css({display:"none"});
    tr.before(cloned);
  });
  $("img.deleter").live("click",function(){
    var _this=$(this),tr=_this.parent().parent();
    tr.remove();
  });

  //isOther/hasOther
  $(".hasOther").live("change",function(){
    var _this=$(this);
    if(_this.val()!="other"){
      _this.next(".isOther").hide(0).val("");
    }else{
      _this.next(".isOther").show(0);
    }
  })

  //Delete Buttons
	$("#mainContent button.delete,#cancelBtn").live("click",function(){
		if(confirm("Are you sure?")){
			NIBS.util.ajax({id:$(this).attr("elId")},"admin/"+$(this).attr("rel"));
		}
		return false;
	});

  // See More details on click
  $("#thirdBar ul.comp_multilineListView li").live("click",function(){
    var _li=$(this);
    _li.siblings().removeClass("active");
    _li.addClass("active");
    loadEntryDetails(_li.attr("id"));
    return false;
  });

  //filters
  $("#sideBar li.clickable").live("click",function(){
    var _li=$(this);
    _li.parent().parent().parent().find("li").not(_li).removeClass("active");
    _li.addClass("active");
    loadFiltered(_li.attr("rel"));    
    return false;
  });
  $("#searchcustomers").click(function(){
    var q=$("#bQuery").val();
    if($.trim(q)!=""){
      $("#sideBar li.active").removeClass("active");
      $("#thirdBar ul.comp_multilineListView").load(base_url+"admin/customers/searchCustomers/",{type:"text",query:q});
    }
  });
  $("#searchdates").click(function(){
    var q=$("#bDate").val();
    if($.trim(q)!=""){
      $("#sideBar li.active").removeClass("active");
      $("#thirdBar ul.comp_multilineListView").load(base_url+"admin/customers/searchCustomers/",{type:"date",query:q});
    }
  });

  //date pickers
  $("input.date").livequery(function(){
    $(this).Zebra_DatePicker({
      format:"d/m"
    });
  })

});

function loadFiltered(filter){
  $("#thirdBar ul.comp_multilineListView").load(base_url+"admin/customers/filterCustomers/"+filter);
}

function loadEntryDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
  var id=e[2];

  $("#dContent").load(base_url+"admin/customers/viewCustomer/"+id);

}

function deleteCustomer(id){
  var el=$("#customer_"+id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
  }
  el.addClass("deleted");
  setTimeout(function(){
    el.remove()
  },500);
  NIBS.notif.notify("success","Customer deleted successfully");
}

function addCustomer(id,name,email,phone){
  if($("#sideBar ul.shortlist li[rel=all]").hasClass("active")){
    var html="<li class='deleted' id='customer_"+id+"'>\
        <span>"+name+"</span>\
        <span>"+phone+"</span>\
        <span>"+email+"</span>\
        </li>";
    $("#thirdBar ul").append(html);
    $("#customer_"+id).removeClass("deleted");
  }



}





