<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifydaemon extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->_userid=$this->session->userdata("uid");
    $this->_username=$this->session->userdata("uname");
    $this->_utype=$this->session->userdata("utype");
    $this->_userImg=$this->session->userdata("uimg");
    $this->SETTINGS=array();
    //make sure this class is only available via ajax
    if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
      $this->load->library("json");
      $this->load->helper("generic");
      $this->load->model("news_m","news");
    }else{
      exit;
    }

  }

  function index($func="",$callingPage="",$data=""){
    switch ($func){
      case "newsDelta":
        $this->_checkNewsDelta($callingPage,$data);
      break;
      default:

      break;
    }
  }

  function _checkNewsDelta($callingPage="",$data=""){
    $delta=1209600;//2 weeks
    $timediff=time()-$delta;
    $res=$this->news->getRowBy("modifieddate >",$timediff);
    if(!isset($res["id"])){
      //no news items newer than delta
      if($callingPage=="news"){
        _notify("error","All news items are older than 2 weeks");
      }elseif($callingPage!="undefined"){
        _notify("error",'All news items are older than 2 weeks. <a href=\"'.base_url().'admin/news\">Click here</a> to fix this.');
      }
    }

    //
  }


}


?>