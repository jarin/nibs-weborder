<?php



namespace entities;

/**
 * ResFlaggedOccassionType
 *
 * @Table(name="res_flagged_occassion_type")
 * @Entity
 */
class ResFlaggedOccassionType
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $flaggedOccassionType
   *
   * @Column(name="flagged_occassion_type", type="string", length=100, nullable=true)
   */
  private $flaggedOccassionType;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set flaggedOccassionType
   *
   * @param string $flaggedOccassionType
   */
  public function setFlaggedOccassionType($flaggedOccassionType)
  {
    $this->flaggedOccassionType = $flaggedOccassionType;
  }

  /**
   * Get flaggedOccassionType
   *
   * @return string 
   */
  public function getFlaggedOccassionType()
  {
    return $this->flaggedOccassionType;
  }
}