<?php

namespace entities;

/**
 * ResNumberIdentifier
 *
 * @Table(name="res_number_identifier")
 * @Entity
 */
class ResNumberIdentifier
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $startingDigit
   *
   * @Column(name="starting_digit", type="string", length=45, nullable=true)
   */
  private $startingDigit;

  /**
   * @var boolean $isMobileNumber
   *
   * @Column(name="is_mobile_number", type="boolean", nullable=true)
   */
  private $isMobileNumber;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;
  
  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;
  
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set startingDigit
   *
   * @param string $startingDigit
   */
  public function setStartingDigit($startingDigit)
  {
    $this->startingDigit = $startingDigit;
  }

  /**
   * Get startingDigit
   *
   * @return string 
   */
  public function getStartingDigit()
  {
    return $this->startingDigit;
  }
  
  /**
   * Set isMobileNumber
   *
   * @param boolean $isMobileNumber
   */
  public function setIsMobileNumber($isMobileNumber)
  {
  	$this->isMobileNumber = $isMobileNumber;
  }
  
  /**
   * Get isMobileNumber
   *
   * @return boolean
   */
  public function getIsMobileNumber()
  {
  	return $this->isMobileNumber;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
  	$this->lastUpdatedBy = $lastUpdatedBy;
  }
  
  /**
   * Get lastUpdatedBy
   *
   * @return ResUser
   */
  public function getLastUpdatedBy()
  {
  	return $this->lastUpdatedBy;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
  	$this->lastUpdate = $lastUpdate;
  }
  
  /**
   * Get lastUpdate
   *
   * @return datetime
   */
  public function getLastUpdate()
  {
  	return $this->lastUpdate;
  }
  
  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
  	$this->isActive = $isActive;
  }
  
  /**
   * Get isActive
   *
   * @return boolean
   */
  public function getIsActive()
  {
  	return $this->isActive;
  }
}