<?php



namespace entities;

/**
 * ResQuantityTypeConversion
 *
 * @Table(name="res_quantity_type_conversion")
 * @Entity
 */
class ResQuantityTypeConversion
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResQuantityType
   *
   * @ManyToOne(targetEntity="ResQuantityType")
   * @JoinColumns({
   *   @JoinColumn(name="from_type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $fromType;

  /**
   * @var ResQuantityType
   *
   * @ManyToOne(targetEntity="ResQuantityType")
   * @JoinColumns({
   *   @JoinColumn(name="to_type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $toType;
  
  /**
   * @var decimal $conversionRate
   *
   * @Column(name="conversion_rate", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $conversionRate=0;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set fromType
   *
   * @param ResQuantityType $fromType
   */
  public function setFromType(\ResQuantityType $fromType)
  {
    $this->fromType = $fromType;
  }

  /**
   * Get fromType
   *
   * @return ResQuantityType 
   */
  public function getFromType()
  {
    return $this->fromType;
  }
  
  /**
   * Set toType
   *
   * @param ResQuantityType $toType
   */
  public function setToType(\ResQuantityType $toType)
  {
      $this->toType = $toType;
  }
  
  /**
   * Get toType
   *
   * @return ResQuantityType
   */
  public function getToType()
  {
      return $this->toType;
  }
  
  /**
   * Set conversionRate
   *
   * @param decimal $conversionRate
   */
  public function setConversionRate($conversionRate)
  {
      $this->conversionRate = $conversionRate;
  }
  
  /**
   * Get conversionRate
   *
   * @return decimal
   */
  public function getConversionRate()
  {
      return $this->conversionRate;
  }
}