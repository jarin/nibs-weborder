<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservation extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="reservation";
		$this->load->model("activity_m","activity");
		$this->load->model("reservation_m","reservation");	
		$this->load->model("order_m","order");
		
		
	}
	
	function index(){
		
		if(_checkAuthentication($this)){
			$html="";
			$data = $this->input->post(NULL, TRUE);
			if(isset($data['dates'])){
				$d = explode("-", $data['dates']);
			
				$from_date = trim($d[0]);
				$to_date = trim($d[1]);
			}else{
				$from_date = date('Y.m.d');
				$to_date = date('Y.m.d');
			}
			$dates = $from_date." - ".$to_date;
			
			
			$reservation_type_id = $this->reservation->getReservationTypeId('Web');
			$restaurant=$this->order->getweb_business($this->_userid);
			$Reservations = $this->reservation->getAllReservation($restaurant[0]['id'],$reservation_type_id, $from_date, $to_date);
			if(is_array($Reservations)){
				foreach($Reservations as $reservation){
					
					$html.="<li id='reservation_$reservation[id]'>
					<a href='#'>
					<span class='name icon-box'><i class=''><img src='".base_url()."assets/images/admin/application_list.png'/></i></span>
					<span class='name nameBtn' title='$reservation[id]'>$reservation[id] $reservation[reservation_date]</span>					
					</a></li>";
				}
			}
			$temp="";
			$this->load->view("admin/admin_head",array(
					"tab"=>$this->tab,
					"username"=>$this->_username,
					"userImg"=>$this->_userImg)
					);
			$this->load->view("admin/reservation_template",array(
					"pagename"=>"Reservation Manager",
					"list"=>$html,
					"dates" => $dates
			));
			$this->load->view("admin/admin_foot");
		}
	}
	
	function loadDetails($id=""){
		if(_checkAuthentication($this)){
			if(trim($id)==""){
				echo '<span style="color:red">Error: </span>Reservation ID Not defined... Please reload page';
				exit;
			}
			$reservation=$this->reservation->getReservation($id);
			
			
			if(!isset($reservation["id"]) || !$reservation["id"]>0){
				echo '<span style="color:red">Error: Invalid Reservation</span>';
				exit;
			}
			//	$html='<link href="'.$baseUrl.'assets/css/admin/jquery-ui/jquery.ptTimeSelect.css" rel="stylesheet" media="screen">';
			//	$html.'<script src="'.$baseUrl.'assets/js/admin/jquery-ui/jquery.ptTimeSelect.js"></script>';
			//	$html.='<script type="text/javascript" src="'.$baseUrl.'assets/js/admin/profileEdit.js"></script>';
			$html="<div class=''>
					<h3 class='page-title'>Reservation Details</h3><br />
					<div id='form_wizard_1' class='widget box'>
			            <div class='widget-title'>
			                <h4><i class='icon-reorder'></i>Reservation Information</h4>
			            </div>
				        <div class='widget-body form'>
							<div style='margin:0 auto;width:100%;text-align:center'>
								<table cellpadding='4' cellspacing='0' align='center' style='width:90%'>
									<tr>
										<th align='left' >Reservation Time</th>
										<td>$reservation[time_from]</td>
									</tr>
								
									<tr>
										<th align='left' >Number of Guest</th>
										<td>$reservation[no_of_guests]</td>
									</tr>
									
									<tr>
										<th align='left' >Deposit Amount</th>
										<td>$reservation[deposit_amount]</td>
									</tr>
									
									<tr>
										<th align='left' >Deposit Type</th>
										<td>$reservation[deposit_type]</td>
									</tr>
									
									<tr>
										<th align='left' >Special Instruction</th>
										<td>$reservation[special_instruction]</td>
									</tr>
									
									
									<tr>
										<th align='left'>Promo Type</th>
										<td>$reservation[promo_type]</td>
									</tr>
									
									<tr>
										<th align='left'>Table Number</th>
										<td>$reservation[table_number]</td>
									</tr>
									
									<tr>
										<th align='left' >Status</th>
										<td>$reservation[status]</td>
									</tr>
									<tr>
										<td align='right' colspan='4'>
										
										<div id='loader$reservation[id]' class='q_feedback'></div>
										
										<input type='hidden' name='business_id' value='$reservation[id]' />
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				
				<div class=''>
					
					<div id='form_wizard_1' class='widget box'>
			            <div class='widget-title'>
			                <h4><i class='icon-reorder'></i>Customer Information</h4>
			            </div>
				        <div class='widget-body form'>
							<div style='margin:0 auto;width:100%;text-align:center'>
								<table cellpadding='4' cellspacing='0' align='center' style='width:90%'>
									<tr>
										<th align='left' >Name</th>
										<td>$reservation[customer_name]</td>
									</tr>
								
									<tr>
										<th align='left' >Address</th>
										<td>".(($reservation['house_no'] !='')? $reservation['house_no'].', ':'').(($reservation['house_name'] !='')? $reservation['house_name'].', ':'').(($reservation['street'] !='')? $reservation['street'].', ':'').(($reservation['city_name'] !='')? $reservation['city_name'].', ':'').(($reservation['state_name'] !='')? $reservation['state_name']:'').(($reservation['post_code'] !='')?  ' - '.$reservation['post_code'].',':'').(($reservation['country_name'] !='')? ' '.$reservation['country_name']:'')."</td>
									</tr>
									
									<tr>
										<th align='left' >Email</th>
										<td>$reservation[email]</td>
									</tr>
									
									<tr>
										<th align='left' >Mobile</th>
										<td>$reservation[mobile]</td>
									</tr>
									<tr>
										<th align='left' >Phone</th>
										<td>$reservation[land_line]</td>
									</tr>
									
								</table>
							</div>
						</div>
					</div>
				</div>
				";
	
				echo $html;
			}
		}
		
	function deleteReservation(){
		if(_checkAuthentication($this)){
			$id=$this->input->post('id');
			if(trim($id)==""){
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove reservatio due to a database error,\\nPlease reload page");
					}}';
				exit;
			}
			
			if($this->reservation->delete($id)){
		
				
		
				echo '{"success":"success",successFunction:function(){
					deleteReservation("'.$id.'");
					}}';
				exit;
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove business due to a database error");
					}}';
				exit;
			}
		}
	}
	
	
}