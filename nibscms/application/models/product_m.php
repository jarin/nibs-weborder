<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_m extends My_Model {

	function __construct(){
		parent::__construct();
		
		$this->product="res_product";
		$this->category="res_category";
		$this->product_ingredient="res_product_ingredient";
		$this->ingredient="res_ingredient";
		$this->product_addon="res_product_addon";
		$this->addon="res_addon";
		$this->tablename=$this->product;
	}
	
	function getProducts($restaurant_id,$order_by="sequence_number",$direction="asc"){
		return $this->db->select("*")
		->where('restaurant_id',$restaurant_id)
		->order_by($order_by,$direction)->get($this->tablename)->result_array();
	}
	
	function getProduct($id){
		$rlt = $this->db->select("*,$this->tablename.id as id, $this->tablename.is_active as is_active, $this->ingredient.price as price_with")
		->where("$this->tablename.id",$id)
		
		->join($this->category,"$this->tablename.category_id = $this->category.id","left")
		->join($this->product_ingredient,"$this->tablename.id = $this->product_ingredient.product_id","left")
		->join($this->ingredient,"$this->ingredient.id = $this->product_ingredient.ingredient_id","left")
		
		->get($this->tablename)->result_array();
		
		return $rlt;
	}
	
	function getProductAddon($id){
		$rlt = $this->db->select("$this->addon.*")
		->where("$this->tablename.id",$id)
		->where("$this->addon.is_active",1)
		->join($this->product_addon,"$this->tablename.id = $this->product_addon.product_id","left")
		->join($this->addon,"$this->addon.id = $this->product_addon.addon_id","left")
		->get($this->tablename)->result_array();
		
		return $rlt;
	}
	

}
?>