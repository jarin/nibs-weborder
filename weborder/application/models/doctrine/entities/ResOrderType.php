<?php



namespace entities;

/**
 * ResOrderType
 *
 * @Table(name="res_order_type")
 * @Entity
 */
class ResOrderType
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $orderType
   *
   * @Column(name="order_type", type="string", length=150, nullable=false)
   */
  private $orderType;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set orderType
   *
   * @param string $orderType
   */
  public function setOrderType($orderType)
  {
    $this->orderType = $orderType;
  }

  /**
   * Get orderType
   *
   * @return string 
   */
  public function getOrderType()
  {
    return $this->orderType;
  }
}