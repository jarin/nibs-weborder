function mailingListUploaded(data){
  var _this=$("textarea[name=numbersList]");
  _this.val(data);
  if(_this.val()!=''){
    $("#finalBtn").prop('disabled',false);
  }else{
   $("#finalBtn").prop('disabled',true);
  }
}

$(function(){

  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // See More details on click
  $("#sidebar ul.list li").each(function(){
    var _this=$(this),
    source=_this.attr("rel"),
    view=_this.attr("template");

    _this.click(function(){
      if(typeof source!="undefined" && source!=""){
        $.getJSON(base_url+"admin/sms/"+source,function(data){
          $("#dContent").html($("#"+view).parseTemplate({data:data}));
          var itemTotal = $("#dContent").find(".grand-total").attr("itemTotal");
          characterCount();
          bulkSmsForm();
          datePickerScript();
          loadPagination(itemTotal);
          $('.descriptionRow').tipsy({fade: true, gravity: 'e'});
          if(typeof _this.attr('fnName')!='undefined'){
            var fn=window[_this.attr('fnName')];
            if($.isFunction(fn)){
              fn();
            }
          }
        });
      }else{
        $("#main-content").removeClass("hastabs");
        $("#innerTabs").hide(0);
        $("#dContent").html($("#"+view).parseTemplate({data:null}));
        $('.descriptionRow').tipsy({fade: true, gravity: 'e'});
      }
      _this.addClass("active").siblings().removeClass("active").find("li").removeClass("active");

      return false;
    });
  });
  
  /**
   * Multi-purpose HTML template parser
   */
   var _tmplCache = {};
  $.fn.parseTemplate = function(data){
      var str = $(this).html();
      var err = "";
      try{
        var func = _tmplCache[str];
        if (!func){
          var strFunc =
          "var p=[],print=function(){p.push.apply(p,arguments);};" +
                      "with(obj){p.push('" +
          str.replace(/[\r\t\n]/g, " ")
             .replace(/'(?=[^#]*#>)/g, "\t")
             .split("'").join("\\'")
             .split("\t").join("'")
             .replace(/<#=(.+?)#>/g, "',$1,'")
             .split("<#").join("');")
             .split("#>").join("p.push('")
             + "');}return p.join('');";

          func = new Function("obj", strFunc);
          _tmplCache[str] = func;
        }
        return func(data);
      } catch (e) {err = e.message;}
      return "< # ERROR: " + err.toString() + " # >";
  };
  
  // word counter 
  $.fn.charCount = function(options){
    // default configuration properties
    var defaults = {  
            allowed: 150,   
            warning: 25,
            css: 'counter',
            counterElement: 'span',
            cssWarning: 'warning',
            cssExceeded: 'exceeded',
            counterText: ''
    }; 

    var options = $.extend(defaults, options); 

    function calculate(obj){
            var count = $(obj).val().length;
            var available = options.allowed - count;
            if(available <= options.warning && available >= 0){
                    $(obj).next().addClass(options.cssWarning);
            } else {
                    $(obj).next().removeClass(options.cssWarning);
            }
            if(available <= 0){
                    $(obj).next().addClass(options.cssExceeded);
            } else {
                    $(obj).next().removeClass(options.cssExceeded);
            }
            $(obj).next().html(options.counterText + available);
    };

    this.each(function() {        
            $(this).after('<'+ options.counterElement +' class="' + options.css + '">'+ options.counterText +'</'+ options.counterElement +'>');
            calculate(this);
            $(this).keyup(function(){calculate(this)});
            $(this).change(function(){calculate(this)});
    });

  };
  // bulk sms form
  function bulkSmsForm() {
    $("select[name=number_options]").live("change",function(){
        var ops = $(this).val(),form=$("form");
        if (ops=='enter_numbers') {
          form.find('.enter_number_pattern').hide();
          form.find('.category').hide();
          form.find('.enter_numbers').show();
          form.find("input[name=type]").val('numbers');
        }
        if (ops=='category') {
          form.find('.enter_number_pattern').hide();
          form.find('.enter_numbers').hide();
          form.find('.category').show();
          form.find("input[name=type]").val('list');
        }
        if (ops=='enter_number_pattern') {
          form.find('.enter_numbers').hide();
          form.find('.category').hide();
          form.find('.enter_number_pattern').show();
          form.find("input[name=type]").val('pattern');
        }
        form.find("input[name=phone_code]").val('');
        form.find("input[name=pattern]").val('');
        form.find("textarea[name=numbers]").val('');
        form.find("select[name=catname]").val('0').trigger("change");;
        form.find("input[name=totalcost]").val('');
        form.find("select[name=countrycode]").val('0').trigger("change");
    }).trigger("change");
  
   }
   
   // datepicker
   function datePickerScript() {
      $.datepicker.setDefaults({
        dateFormat: 'dd.mm.yy' 
      });

      $('.date-pick').datepicker();    
   }
   
  // chartacter count 
  function characterCount() {
    $(".message").charCount({
      allowed: 150,   
      warning: 20,
      css: 'charactercounter',
      counterElement: 'span',
      cssWarning: 'charactercounter-warning',
      cssExceeded: 'charactercounter-exceeded',
      counterText: 'Characters left: '
    }); 
  }
  
  // pagination
  function loadPagination(itemTotal) {
      $('.list-box').pajinate({
      abort_on_small_lists: false,
      items_total: itemTotal,
      item_container_id : '.items',
      nav_panel_id : '.paginate',
      items_per_page : 10,      
      num_page_links_to_display : 10
    });
  }
  
  
  $("select[name=countrycode]").live("change",function(){
   var _this = $(this),code=_this.find("option:selected").val(),form=$("form");
   if(code=='0'){
    form.find("input[name=phone_code]").val('');
        form.find("input[name=localcost]").val('');
        form.find("#phone_no").prop("disabled",true);
   }else{
     $.ajax({
          type: "POST",
          url: "admin/sms/Xapi/getCountryInfoFromCountryCode/echo",
          data: "countrycode="+code,
          success: function(rdata){
            rdata=$.parseJSON(rdata);
            if(rdata.success){
              form.find("input[name=phone_code]").val(rdata.result.phone_code);
              //form.find("input[name=localcost]").val(rdata.result.prefix+rdata.result.rate+' '+rdata.result.suffix);
              form.find("#numbers").prop("disabled",false).focus();
              form.find("#phone_number_pattern").attr("readonly",false).focus();
            }else{
              NIBS.notif.notify("error",rdata.msg);
            }
          }
      });
   }
  });
  
  $("input[name=numbers]").live("change",function(){
   var _this = $(this),form=$("form"),country=form.find("select[name=countrycode]").find("option:selected").val();
   form.find("input[name=totalcost]").val('');
   $.ajax({
        type: "POST",
        url: "admin/sms/Xapi/getSMSRate/echo",
        data: "countrycode="+country+"&phone_no="+_this.val(),
        success: function(rdata){
          rdata=$.parseJSON(rdata);
          if(rdata.success){
            form.find("input[name=totalcost]").val(rdata.result.prefix+' '+Number(rdata.result.rate).toFixed(5)+' '+rdata.result.suffix);
            form.find("textarea[name=message]").focus();
            form.find("#phone_number_pattern").attr("readonly",false).focus();
          }else{
            NIBS.notif.notify("error",rdata.msg);
          }
        }
    });
  });
  
  $("textarea[name=numbers]").live("change",function(){
   var _this = $(this),form=$("form"),country=form.find("select[name=countrycode]").find("option:selected").val(),tnum=_this.val();
   form.find("input[name=totalcost]").val('');
   $.ajax({
        type: "POST",
        url: "admin/sms/Xapi/getSMSRate/echo",
        data: "countrycode="+country+"&phone_nos="+_this.val(),
        success: function(rdata){
          rdata=$.parseJSON(rdata);
          if(rdata.success){
            form.find("input[name=totalcost]").val(rdata.result.prefix+' '+Number(rdata.result.rate).toFixed(5)+' '+rdata.result.suffix);
            form.find("textarea[name=message]").focus();
            form.find("#phone_number_pattern").attr("readonly",false).focus();
            form.find("textarea[name=numbers]").val(tnum);
          }else{
            NIBS.notif.notify("error",rdata.msg);
          }
        }
    });
  });

  $("input[name=pattern]").live("keyup",function(e){
    var _this=$(this);
    _this.val(_this.val().replace(/[^0-9-]/, function(str){return '';}));
  }).live("change",function(){
    var _this=$(this),form=$("#sms"),phone_nos='';
    $.ajax({
          type: "POST",
          url: "admin/sms/Xapi/getPhoneNumbersFromPattern/echo",
          data: "countrycode="+form.find("select[name=countrycode]").find("option:selected").val()+"&pattern="+_this.val(),
          success: function(rdata){
            var data=$.parseJSON(rdata),ta=$("textarea[name=numbers]");
            if(data.success){
              for(var i=0;i<data.result.length;i++){
                ta.val(ta.val()+data.result[i]+';');
                phone_nos=phone_nos+data.phone_no[i]+'-';
              }
              ta.val(ta.val().substr(0,ta.val().length-1));
              phone_nos=phone_nos.substr(0,phone_nos.length-1);
              $("textarea[name=message]").focus();
              _this.attr("readonly",true);
              $.ajax({
                type: "POST",
                url: "admin/sms/Xapi/getSMSRate/echo",
                data: "countrycode="+form.find("select[name=countrycode]").find("option:selected").val()+"&phone_nos="+phone_nos,
                success: function(cdata){
                  cdata=$.parseJSON(cdata);
                  if(cdata.success){
                    form.find("input[name=totalcost]").val(cdata.result.prefix+' '+Number(cdata.result.rate).toFixed(5)+' '+cdata.result.suffix);
                  }else{
                    NIBS.notif.notify("error",cdata.msg);
                  }
                }
              });
            }else{
              NIBS.notif.notify("error",data.msg);
            }
          }
      });
  });
  
  $("select[name=catname]").live("change",function(){
    var _this = $(this),catname=_this.find("option:selected").val(),form=$("form");
    if(catname==''){
        form.find("input[name=totalcost]").val('');
        form.find("#numbers").prop("disabled",true);
    }else{
     $.ajax({
          type: "POST",
          url: "admin/sms/Xapi/getPhoneNumbersFromCategoryName/echo",
          data: "catname="+catname,
          success: function(rdata){
            rdata=$.parseJSON(rdata);
            if(rdata.success){
              form.find("input[name=totalcost]").val(rdata.result.prefix+' '+Number(rdata.result.cost).toFixed(5)+' '+rdata.result.suffix);
              form.find("textarea[name=message]").focus();
            }else{
              NIBS.notif.notify("error",rdata.msg);
              form.find("input[name=totalcost]").val('');
              form.find("#numbers").prop("disabled",true);
            }
          }
      });
    }
  });
  
  $("textarea[name=numbers]").live("keyup",function(e){
  var _this=$(this), ta=$("textarea[name=phone_no]"),c=0,form=$("#sms");
  _this.val(_this.val().replace(/[^0-9-]/, function(str){return '';}));
  var arry=_this.val().split("-");
  ta.val('');
  for(var i=0;i<arry.length;i++){
    if(arry[i]!=''){
      ta.val(ta.val()+form.find("input[name=phone_code]").val()+arry[i]+";");
      c++;
    }
  }
  ta.val(ta.val().substr(0,ta.val().length-1));
  });
  
  $(".datess").live("change",function(){
   var _this=$(this),cont=$(".clienbill"),from_date=cont.find("input[name=from_date]").val(),to_date=cont.find("input[name=to_date]").val();
   $.ajax({
        type: "POST",
        url: "admin/sms/XgetBill/"+from_date+"/"+to_date,
        success: function(rdata){
          rdata=$.parseJSON(rdata);
          if(rdata.success){
            cont.find(".table-style").show(0);
            cont.find(".grand-total").find("span:first-child").html('Total Bill from '+rdata.result.from_date+' to '+rdata.result.to_date);
            var ta=cont.find(".items"),totalsms=0,totalbill=0,prefix='',suffix='',item='';
            for(var i=0;i<rdata.result.bills.length;i++){
              item = item + '<div class="item"><span style="width: 59%;">';
              item = item + rdata.result.bills[i].date;
              item = item + '</span><span style="text-align: center;">';
              item = item + rdata.result.bills[i].cnt;
              item = item + '</span><span style="text-align: right;">';
              item = item + rdata.result.bills[i].prefix+rdata.result.bills[i].bill+rdata.result.bills[i].suffix;
              item = item + '</span></div>';
              totalsms = totalsms+parseInt(rdata.result.bills[i].cnt);
          totalbill = totalbill+parseFloat(rdata.result.bills[i].bill);
          prefix=rdata.result.bills[i].prefix;
          suffix=rdata.result.bills[i].suffix;
              }
            ta.html(item);
            
            cont.find(".grand-total").find("span:nth-child(2)").html(totalsms);
            cont.find(".grand-total").find("span:nth-child(3)").html(prefix+totalbill.toFixed(5)+suffix);
          }else{
            cont.find(".table-style").hide(0);
            NIBS.notif.notify("error",rdata.msg);
          }
        }
    });
  });
  
  $(".dateshs").live("change",function(){
   var _this=$(this),cont=$(".clienthistory"),from_date=cont.find("input[name=from_date]").val(),to_date=cont.find("input[name=to_date]").val(),status=cont.find("select[name=status]").find("option:selected").val();
   $.ajax({
        type: "POST",
        url: "admin/sms/XgetSMSHistory/"+from_date+"/"+to_date+"/"+status+"/0",
        success: function(rdata){
          rdata=$.parseJSON(rdata);
          if(rdata.success){
            $(".list-box .table-style").html($("#sms_history_rows").parseTemplate({data:rdata})).show(0);
            loadPagination(rdata.result.history.total);
              $(".paginate").show(0);
              $('.descriptionRow').tipsy({fade: true, gravity: 'e'});
          }else{
            cont.find(".table-style").hide(0);
            $(".paginate").hide(0);
            NIBS.notif.notify("error",rdata.msg);
          }
        }
    });
  });
  
  $(".clienthistory").find("select[name=status]").live("change",function(){
   var _this=$(this),cont=$(".clienthistory"),from_date=cont.find("input[name=from_date]").val(),to_date=cont.find("input[name=to_date]").val(),status=cont.find("select[name=status]").find("option:selected").val();
   $.ajax({
        type: "POST",
        url: "admin/sms/XgetSMSHistory/"+from_date+"/"+to_date+"/"+status+"/0",
        success: function(rdata){
          rdata=$.parseJSON(rdata);
          if(rdata.success){
            $(".list-box .table-style").html($("#sms_history_rows").parseTemplate({data:rdata})).show(0);
            loadPagination(rdata.result.history.total);
              $(".paginate").show(0);
              $('.descriptionRow').tipsy({fade: true, gravity: 'e'});
          }else{
            cont.find(".table-style").hide(0);
            $(".paginate").hide(0);
            NIBS.notif.notify("error",rdata.msg);
          }
        }
    });
  });
  
  $("select[name=countrycodes]").live("change",function(){
   var _this = $(this),code=_this.find("option:selected").val(),form=$(".sms-rate");
   $.ajax({
        type: "POST",
        url: "admin/sms/XgetRate/"+code,
        success: function(rdata){
          rdata=$.parseJSON(rdata);
          if(rdata.success){
            form.find(".table-style").show(0);
            var ta=form.find(".items"),item='';
            for(var i=0;i<rdata.result.rate.rates.country.length;i++){
                item = item + '<div class="item"><span style="width: 180px !important;">';
                item = item + rdata.result.rate.rates.country[i];
                item = item + '</span><span style="width: 100px !important;text-align: left;">';
                item = item + rdata.result.rate.rates.operator[i];
                item = item + '</span>';
                for(var j in rdata.result.rate.rates.currency){
                  item = item + '<span style="width: 50px !important;text-align: right;">';
                  item = item + Number(rdata.result.rate.rates.sell[j][i]).toFixed(5);
                  item = item + '</span>';
                }
                item = item + '</div>';
              }
              ta.html(item);
          }else{
            form.find(".table-style").hide(0);
            NIBS.notif.notify("error",rdata.msg);
          }
        }
    });
  });
  
  $("select[name=list_options]").live("change",function(){
   var _this=$(this),form=$("form");
   form.find(".numb_opt").hide(0);
   form.find("."+_this.val()).show(0);
   form.find("select[name=countrycodelist]").val('0').trigger('change');
   form.find("textarea[name=numbersList]").val('');
   if(_this.val()=='csv'){
     form.find("#numbersListCSV").prop("disabled",false);
     form.find("#numbersList").prop("disabled",true);
   }else{
     form.find("#numbersListCSV").prop("disabled",true);
     form.find("#numbersList").prop("disabled",false);
   }
  });
  
  $("select[name=countrycodelist]").live("change",function(){
   var _this = $(this),code=_this.find("option:selected").val(),form=$("form"), rel=_this.attr("rel");
   if(rel=='list'){
     var url = "admin/sms/Xapi/getMailingList/echo";
     $.ajax({
       dataType: "json",
             type: "POST",
             url: url,
             data: "country="+code+"&category=0&page=0",
             success:function(data){
               if(data.success==false){
                 $(".items").html("");
                 NIBS.notif.notify("error",data.msg);
                 $('.paginate').hide(0);
                 $('.heading').hide(0);
               }else{
                 $(".mailinglist .table-style").html($("#mailinglist_rows").parseTemplate({data:data}));
                 if(data.result.total>0){
                   loadPagination(data.result.total);
                   $('.paginate').show(0);
                   $('.heading').show(0);
                 }else{
                   $('.paginate').hide(0);
                   $('.heading').hide(0);
                 }
               }
             }
      });    
   }
   if(code=='0'){
    form.find("input[name=phone_code]").val('');
    if(rel=='list'){
      form.find("select[name=categorylist]").html('<option value="0">All Category</option>').trigger('change');
    }else{
      form.find("select[name=category]").html('<option value="0">Please select a country to see category list</option>').trigger('change');
    }
    form.find("#btnNewCategory").prop('disabled',true);
    form.find("#finalBtn").prop('disabled',true);
   }else{
     $.ajax({
          type: "POST",
          url: "admin/sms/Xapi/getCategoryListByCountryCode/echo",
          data: "countrycode="+code,
          success: function(rdata){
            rdata=$.parseJSON(rdata);
            if(rdata.success){
              var options=[];
                for(var i=0;i<rdata.result.length;i++){
                  options.push("<option value='"+rdata.result[i].category+"' >"+rdata.result[i].category+"</option>");
                }
                if(rel=='list'){
                  options="<option value='0' >All Category</option>" + options.join("");
                  form.find("select[name=categorylist]").html("").append(options);
                }else{
                  options="<option value='0' >Please Select...</option>" + options.join("");
                  form.find("select[name=category]").html("").append(options).trigger('change');
                }
                form.find("input[name=phone_code]").val(_this.find('option:selected').attr('phoneprefix'));
                form.find("#btnNewCategory").prop('disabled',false);
            }else{
              if(rel=='list'){
                form.find("select[name=categorylist]").html('<option value="0">All Category</option>');
              }else{
                form.find("select[name=category]").html('<option value="0">Please select a country to see category list</option>').trigger('change');
              }
              form.find("input[name=phone_code]").val('');
              form.find("#btnNewCategory").prop('disabled',false);
              NIBS.notif.notify("error",rdata.msg);
            }
          }
      });
   }
  });
  
  $("#createNewMailingList").live("click",function(e){
   e.preventDefault();
   var url = "admin/sms/XgetDataForList";
    $.ajax({
           dataType: "json",
                 type: "POST",
                 url: url,
                 success:function(data){
                   $("#dContent").html($("#database").parseTemplate({data:data}));
                   $("#fileToUpload").uploadFile({allowed: ['csv']});
                 }
          }); 
   
  });
  
  $("#deleteCategory").live("click",function(e){
    e.preventDefault();
    var _this = $(this),form=$("form"),cat=form.find("select[name=categorylist]").find("option:selected").val(), code=form.find("select[name=countrycodelist]").find("option:selected").val();
    var url = "admin/sms/Xapi/deleteCategory/echo";
    $.ajax({
       dataType: "json",
            type: "POST",
            url: url,
            data: "country="+code+"&category="+cat,
            success:function(data){
             if(data.success==false){
              NIBS.notif.notify("error",data.msg);
             }else{
              NIBS.notif.notify("success",data.result);
             }
             $("#page_mailinglist").trigger("click");
            }
     });  
  });
  
  $(".delNumber").live("click",function(e){
    e.preventDefault();
    var _this = $(this),form=$("form"),id=_this.attr("rid");
    var url = "admin/sms/Xapi/deleteNumber/echo";
    $.ajax({
       dataType: "json",
            type: "POST",
            url: url,
            data: "id="+id,
            success:function(data){
             if(data.success==false){
              NIBS.notif.notify("error",data.msg);
             }else{
              NIBS.notif.notify("success",data.result);
             }
             $("#page_mailinglist").trigger("click");
            }
     });  
  });

  $("#backToList").live("click",function(e){
    e.preventDefault();
    $("#page_mailinglist").trigger("click");
  });
  
  $("select[name=categorylist]").live("change",function(){
  var _this = $(this),cat=_this.find("option:selected").val(),form=$("form"), code=form.find("select[name=countrycodelist]").find("option:selected").val(), rel=_this.attr("rel");
  var url = "admin/sms/Xapi/getMailingList/echo";
  $.ajax({
         dataType: "json",
               type: "POST",
               url: url,
               data: "country="+code+"&category="+cat+"&page=0",
               success:function(data){
                 cnosole.log(data);
                 if(data.success==false){
                   $(".items").html("");
                   NIBS.notif.notify("error",data.msg);
                   $('.paginate').hide(0);
                   $('.heading').hide(0);
                 }else{
                   $(".mailinglist .table-style").html($("#mailinglist_rows").parseTemplate({data:data}));
                   if(data.result.total>0){
                     loadPagination(data.result.total);
                     $('.paginate').show(0);
                     $('.heading').show(0);
                   }else{
                     $('.paginate').hide(0);
                     $('.heading').hide(0);
                   }
                 }
               }
        });    
  });
  
  $("#btnNewCategory").live("click",function(e){
   e.preventDefault();
   var form=$('form');
   form.find('select[name=category]').hide(0);
   form.find("#btnNewCategory").hide(0);
   form.find("input[name=newCategory]").show(0).focus();
   form.find("#btnSaveCategory").show(0);
  });
  
  $("#btnSaveCategory").live("click",function(e){
   e.preventDefault();
   var form=$('form'),code=form.find("select[name=countrycodelist]").find("option:selected").val();
   $.ajax({
        type: "POST",
        url: "admin/sms/Xapi/addEditCategory/echo",
        data: "countrycode="+code+"&cat_name="+form.find("input[name=newCategory]").val(),
        success: function(rdata){
          rdata=$.parseJSON(rdata);
          if(rdata.success){
          form.find("select[name=countrycodelist]").val('0').trigger("change");
          form.find('select[name=category]').show(0);
          form.find("#btnNewCategory").show(0);
          form.find("input[name=newCategory]").hide(0);
          form.find("#btnSaveCategory").hide(0);
          }else{
          NIBS.notif.notify("error",rdata.msg);
          }
        }
    });
  });
  
  $("input[name=newCategory]").live("keyup",function(e){
   var _this=$(this),form=$('form');
   if(_this.val()==''){
     form.find("#btnSaveCategory").prop('disabled',true);
   }else{
     form.find("#btnSaveCategory").prop('disabled',false);
   }
  });
  
  $("select[name=category]").live("change",function(){
   var _this=$(this),form=$("form"); 
   if(_this.find("option:selected").val()=='0'){
     form.find("#numbersList").prop("disabled",true);
     form.find("#fileToUpload").prop("disabled",true);
   }else{
     form.find("#numbersList").prop("disabled",false).focus();
     form.find("#fileToUpload").prop("disabled",false);
   }
  });
  
  $("textarea[name=numbersList]").live("keyup",function(e){
  var _this=$(this),form=$("form");
  _this.val(_this.val().replace(/[^0-9-]/, function(str){return '';}));
  if(_this.val()!=''){
     form.find("#finalBtn").prop('disabled',false);
   }else{
     form.find("#finalBtn").prop('disabled',true);
   }
  });

  // pagination
  var totalPage = "10";
  var spinner=$('<img src="'+base_url+'assets/images/admin/loader.gif" border="0" alt="Loader..." />');
    
   $(".clienthistory").find('.page_link').live("click",function(e){
         e.preventDefault;
         $(".items").html(spinner);
         var _this=$(this);
     var url = "admin/sms/XgetSMSHistory/"+$("input[name=from_date]").val()+"/"+$("input[name=to_date]").val()+"/"+$("select[name=status]").find("option:selected").val()+"/"+_this.attr('longdesc');
         $.ajax({
                  dataType: "json",
                  type: "POST",
                  url: url,
                  success:function(data){
                    if(data.success==false){
                      $(".items").html("");
                      NIBS.notif.notify("error",data.msg);
                    }else{
                      $(".clienthistory .table-style").html($("#sms_history_rows").parseTemplate({data:data}));
                      $('.descriptionRow').tipsy({fade: true, gravity: 'e'});
                    }
                  }
           });    
   });
   $(".clienthistory").find('.last_link, .clienthistory .first_link, .clienthistory .previous_link, .clienthistory .next_link').live("click",function(e){
         e.preventDefault;
         $(".items").html(spinner);
         var _this=$(".active_page");
         var url = "admin/sms/XgetSMSHistory/"+$("input[name=from_date]").val()+"/"+$("input[name=to_date]").val()+"/"+$("select[name=status]").find("option:selected").val()+"/"+_this.attr('longdesc');
         $.ajax({
                  dataType: "json",
                  type: "POST",
                  url: url,
                  success:function(data){

                    if(data.success==false){
                      $(".items").html("");
                      NIBS.notif.notify("error",data.msg);
                    }else{
                      $(".clienthistory .table-style").html($("#sms_history_rows").parseTemplate({data:data}));
                      $('.descriptionRow').tipsy({fade: true, gravity: 'e'});
                    }
                  }
           });    
   }); 
   
   $(".mailinglist").find('.page_link').live("click",function(e){
       e.preventDefault;
       $(".items").html(spinner);
       var _this=$(this),form=$("form"),cou=form.find("select[name=countrycodelist]").find("option:selected").val(),cat=form.find("select[name=categorylist]").find("option:selected").val();
       var url = "admin/sms/Xapi/getMailingList/echo";
       $.ajax({
                dataType: "json",
                type: "POST",
                url: url,
                data: "country="+cou+"&category="+cat+"&page="+_this.attr('longdesc'),
                success:function(data){
                  if(data.success==false){
                    $(".items").html("");
                    NIBS.notif.notify("error",data.msg);
                  }else{
                    $(".mailinglist .table-style").html($("#mailinglist_rows").parseTemplate({data:data}));
                  }
                }
         });    
 });
 $(".mailinglist").find('.last_link, .mailinglist .first_link, .mailinglist .previous_link, .mailinglist .next_link').live("click",function(e){
       e.preventDefault;
       $(".items").html(spinner);
       var _this=$(".active_page"),form=$("form"),cou=form.find("select[name=countrycodelist]").find("option:selected").val(),cat=form.find("select[name=categorylist]").find("option:selected").val();
       var url = "admin/sms/Xapi/getMailingList/echo";
       $.ajax({
                dataType: "json",
                type: "POST",
                url: url,
                data: "country="+cou+"&category="+cat+"&page="+_this.attr('longdesc'),
                success:function(data){
                  if(data.success==false){
                    $(".items").html("");
                    NIBS.notif.notify("error",data.msg);
                  }else{
                    $(".mailinglist .table-style").html($("#mailinglist_rows").parseTemplate({data:data}));
                  }
                }
         });    
 }); 
   
});


//tipsy, facebook style tooltips for jquery
//version 1.0.0a
//(c) 2008-2010 jason frame [jason@onehackoranother.com]
//released under the MIT license

(function($) {

function maybeCall(thing, ctx) {
   return (typeof thing == 'function') ? (thing.call(ctx)) : thing;
};

function isElementInDOM(ele) {
 while (ele = ele.parentNode) {
   if (ele == document) return true;
 }
 return false;
};

function Tipsy(element, options) {
   this.$element = $(element);
   this.options = options;
   this.enabled = true;
   this.fixTitle();
};

Tipsy.prototype = {
   show: function() {
       var title = this.getTitle();
       if (title && this.enabled) {
           var $tip = this.tip();
           
           $tip.find('.tipsy-inner')[this.options.html ? 'html' : 'text'](title);
           $tip[0].className = 'tipsy'; // reset classname in case of dynamic gravity
           $tip.remove().css({top: 0, left: 0, visibility: 'hidden', display: 'block'}).prependTo(document.body);
           
           var pos = $.extend({}, this.$element.offset(), {
               width: this.$element[0].offsetWidth,
               height: this.$element[0].offsetHeight
           });
           
           var actualWidth = $tip[0].offsetWidth,
               actualHeight = $tip[0].offsetHeight,
               gravity = maybeCall(this.options.gravity, this.$element[0]);
           
           var tp;
           switch (gravity.charAt(0)) {
               case 'n':
                   tp = {top: pos.top + pos.height + this.options.offset, left: pos.left + pos.width / 2 - actualWidth / 2};
                   break;
               case 's':
                   tp = {top: pos.top - actualHeight - this.options.offset, left: pos.left + pos.width / 2 - actualWidth / 2};
                   break;
               case 'e':
                   tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth - this.options.offset};
                   break;
               case 'w':
                   tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width + this.options.offset};
                   break;
           }
           
           if (gravity.length == 2) {
               if (gravity.charAt(1) == 'w') {
                   tp.left = pos.left + pos.width / 2 - 15;
               } else {
                   tp.left = pos.left + pos.width / 2 - actualWidth + 15;
               }
           }
           
           $tip.css(tp).addClass('tipsy-' + gravity);
           $tip.find('.tipsy-arrow')[0].className = 'tipsy-arrow tipsy-arrow-' + gravity.charAt(0);
           if (this.options.className) {
               $tip.addClass(maybeCall(this.options.className, this.$element[0]));
           }
           
           if (this.options.fade) {
               $tip.stop().css({opacity: 0, display: 'block', visibility: 'visible'}).animate({opacity: this.options.opacity});
           } else {
               $tip.css({visibility: 'visible', opacity: this.options.opacity});
           }
       }
   },
   
   hide: function() {
       if (this.options.fade) {
           this.tip().stop().fadeOut(function() { $(this).remove(); });
       } else {
           this.tip().remove();
       }
   },
   
   fixTitle: function() {
       var $e = this.$element;
       if ($e.attr('title') || typeof($e.attr('original-title')) != 'string') {
           $e.attr('original-title', $e.attr('title') || '').removeAttr('title');
       }
   },
   
   getTitle: function() {
       var title, $e = this.$element, o = this.options;
       this.fixTitle();
       var title, o = this.options;
       if (typeof o.title == 'string') {
           title = $e.attr(o.title == 'title' ? 'original-title' : o.title);
       } else if (typeof o.title == 'function') {
           title = o.title.call($e[0]);
       }
       title = ('' + title).replace(/(^\s*|\s*$)/, "");
       return title || o.fallback;
   },
   
   tip: function() {
       if (!this.$tip) {
           this.$tip = $('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>');
           this.$tip.data('tipsy-pointee', this.$element[0]);
       }
       return this.$tip;
   },
   
   validate: function() {
       if (!this.$element[0].parentNode) {
           this.hide();
           this.$element = null;
           this.options = null;
       }
   },
   
   enable: function() { this.enabled = true; },
   disable: function() { this.enabled = false; },
   toggleEnabled: function() { this.enabled = !this.enabled; }
};

$.fn.tipsy = function(options) {
   
   if (options === true) {
       return this.data('tipsy');
   } else if (typeof options == 'string') {
       var tipsy = this.data('tipsy');
       if (tipsy) tipsy[options]();
       return this;
   }
   
   options = $.extend({}, $.fn.tipsy.defaults, options);
   
   function get(ele) {
       var tipsy = $.data(ele, 'tipsy');
       if (!tipsy) {
           tipsy = new Tipsy(ele, $.fn.tipsy.elementOptions(ele, options));
           $.data(ele, 'tipsy', tipsy);
       }
       return tipsy;
   }
   
   function enter() {
       var tipsy = get(this);
       tipsy.hoverState = 'in';
       if (options.delayIn == 0) {
           tipsy.show();
       } else {
           tipsy.fixTitle();
           setTimeout(function() { if (tipsy.hoverState == 'in') tipsy.show(); }, options.delayIn);
       }
   };
   
   function leave() {
       var tipsy = get(this);
       tipsy.hoverState = 'out';
       if (options.delayOut == 0) {
           tipsy.hide();
       } else {
           setTimeout(function() { if (tipsy.hoverState == 'out') tipsy.hide(); }, options.delayOut);
       }
   };
   
   if (!options.live) this.each(function() { get(this); });
   
   if (options.trigger != 'manual') {
       var binder   = options.live ? 'live' : 'bind',
           eventIn  = options.trigger == 'hover' ? 'mouseenter' : 'focus',
           eventOut = options.trigger == 'hover' ? 'mouseleave' : 'blur';
       this[binder](eventIn, enter)[binder](eventOut, leave);
   }
   
   return this;
   
};

$.fn.tipsy.defaults = {
   className: null,
   delayIn: 0,
   delayOut: 0,
   fade: false,
   fallback: '',
   gravity: 'n',
   html: false,
   live: false,
   offset: 0,
   opacity: 0.8,
   title: 'title',
   trigger: 'hover'
};

$.fn.tipsy.revalidate = function() {
 $('.tipsy').each(function() {
   var pointee = $.data(this, 'tipsy-pointee');
   if (!pointee || !isElementInDOM(pointee)) {
     $(this).remove();
   }
 });
};

// Overwrite this method to provide options on a per-element basis.
// For example, you could store the gravity in a 'tipsy-gravity' attribute:
// return $.extend({}, options, {gravity: $(ele).attr('tipsy-gravity') || 'n' });
// (remember - do not modify 'options' in place!)
$.fn.tipsy.elementOptions = function(ele, options) {
   return $.metadata ? $.extend({}, options, $(ele).metadata()) : options;
};

$.fn.tipsy.autoNS = function() {
   return $(this).offset().top > ($(document).scrollTop() + $(window).height() / 2) ? 's' : 'n';
};

$.fn.tipsy.autoWE = function() {
   return $(this).offset().left > ($(document).scrollLeft() + $(window).width() / 2) ? 'e' : 'w';
};

/**
* yields a closure of the supplied parameters, producing a function that takes
* no arguments and is suitable for use as an autogravity function like so:
*
* @param margin (int) - distance from the viewable region edge that an
*        element should be before setting its tooltip's gravity to be away
*        from that edge.
* @param prefer (string, e.g. 'n', 'sw', 'w') - the direction to prefer
*        if there are no viewable region edges effecting the tooltip's
*        gravity. It will try to vary from this minimally, for example,
*        if 'sw' is preferred and an element is near the right viewable 
*        region edge, but not the top edge, it will set the gravity for
*        that element's tooltip to be 'se', preserving the southern
*        component.
*/
$.fn.tipsy.autoBounds = function(margin, prefer) {
    return function() {
      var dir = {ns: prefer[0], ew: (prefer.length > 1 ? prefer[1] : false)},
          boundTop = $(document).scrollTop() + margin,
          boundLeft = $(document).scrollLeft() + margin,
          $this = $(this);

      if ($this.offset().top < boundTop) dir.ns = 'n';
      if ($this.offset().left < boundLeft) dir.ew = 'w';
      if ($(window).width() + $(document).scrollLeft() - $this.offset().left < margin) dir.ew = 'e';
      if ($(window).height() + $(document).scrollTop() - $this.offset().top < margin) dir.ns = 's';

      return dir.ns + (dir.ew ? dir.ew : '');
    }
  };
  
  /**
  * File Uploader
  */
  $.fn.uploadFile=function(opts){
    var opts=$.extend({
    allowed:['csv']
    },opts);
    return this.each(function(){
      var pb='<div class="progress" style="width:0"><span>0%</span></div>',
      _this=$(this),
      progressbar=$(_this.attr("data-progressbar")),endpoint=_this.attr("data-endpoint"),viewer=$(_this.attr("data-viewer"));
      _this.change(function(){
        progressbar.show(0);
          var filename=_this.val().toLowerCase().split(/[\\\/]/).pop(),is_allowed=false,
          ext=filename.split(".").pop();
          $.each(opts.allowed,function(i,el){
            if(ext==el){
              is_allowed=true;
            }
          });
          if(!is_allowed){
            alert("This file type is not supported. Please select another file.");
            return;
          }else{
            var files=_this[0].files,
            file=_this[0].files[0], headers={
              "Cache-Control":"no-cache",
              "X-Requested-With":"XMLHttpRequest",
              "X-File-Name":file.fileName||file.name,
              "X-File-Size":file.fileSize||file.size,
              "Content-Type":"multipart/x-www-form-urlencoded"
            };
            _this.attr("disabled",true);
            progressbar.append(pb);
            var bar=progressbar.find(".progress"),percent=bar.find("span");

            var xhr = new XMLHttpRequest();
            var upload = xhr.upload;
            upload.fileObj = file;
            upload.downloadStartTime = new Date().getTime();
            upload.currentStart = upload.downloadStartTime;
            upload.currentProgress = 0;
            upload.startData = 0;
            upload.onprogress=function(e){
            viewer.hide(0);
              if(e.lengthComputable){
                var _p=Math.floor((e.loaded/e.total)*100);
                bar.css("width",_p+"%");
                percent.html(_p+"%");
                if(_p==100){
                  percent.html("Processing...");
                }
              }
            };
            xhr.onload=function(e){
              if(e.target.readyState == 4){
                var data=eval("("+this.responseText+")");
                if(data && data.successFunction){
                  data.successFunction.apply(_this);
                }

                progressbar.find(".progress").remove();
                progressbar.hide(0);
              }
            };
            xhr.open("POST",endpoint,true);
            for(var prop in headers){
              xhr.setRequestHeader(prop, headers[prop]);
            }
            xhr.send(file);
          }
      });
    });
  }

})(jQuery);

;(function($){
/*******************************************************************************************/    
// jquery.pajinate.js - version 0.4
// A jQuery plugin for paginating through any number of DOM elements
// 
// Copyright (c) 2010, Wes Nolte (http://wesnolte.com)
// Licensed under the MIT License (MIT-LICENSE.txt)
// http://www.opensource.org/licenses/mit-license.php
// Created: 2010-04-16 | Updated: 2010-04-26
//
/*******************************************************************************************/

    $.fn.pajinate = function(options){
        // Set some state information
        var current_page = 'current_page';
    var items_per_page = 'items_per_page';
    
    var meta;
  
    // Setup default option values
    var defaults = {
      item_container_id : '.content',
      items_total: 0,
      items_per_page : 10,      
      nav_panel_id : '.page_navigation',
      nav_info_id : '.info_text',
      num_page_links_to_display : 20,     
      start_page : 0,
      wrap_around : false,
      nav_label_first : 'First',
      nav_label_prev : 'Prev',
      nav_label_next : 'Next',
      nav_label_last : 'Last',
      nav_order : ["first", "prev", "num", "next", "last"],
      nav_label_info : 'Showing {0}-{1} of {2} results',
            show_first_last: true,
            abort_on_small_lists: false,
            jquery_ui: false,
            jquery_ui_active: "ui-state-highlight",
            jquery_ui_default: "ui-state-default",
            jquery_ui_disabled: "ui-state-disabled"
    };
    var options = $.extend(defaults,options);
    var $item_container;
    var $page_container;
    var $items;
    var $nav_panels;
        var total_page_no_links;
        var jquery_ui_default_class = options.jquery_ui ? options.jquery_ui_default : '';
        var jquery_ui_active_class = options.jquery_ui ? options.jquery_ui_active : '';
        var jquery_ui_disabled_class = options.jquery_ui ? options.jquery_ui_disabled : '';
  
    return this.each(function(){
      $page_container = $(this);
      $item_container = $(this).find(options.item_container_id);
      $items = $page_container.find(options.item_container_id).children();
      
      if (options.abort_on_small_lists && options.items_per_page >= $items.size())
                return $page_container;
                
      meta = $page_container;
      
      // Initialize meta data
      meta.data(current_page,0);
      meta.data(items_per_page, options.items_per_page);
          
      // Get the total number of items
      if(options.items_total==0){
        var total_items = $item_container.children().size();
      }else{
        var total_items = options.items_total;
      }
      
      // Calculate the number of pages needed
      var number_of_pages = Math.ceil(total_items/options.items_per_page);
      
      // Construct the nav bar
      var more = '<span class="ellipse more">...</span>';
      var less = '<span class="ellipse less">...</span>';
            var first = !options.show_first_last ? '' : '<a class="first_link '+ jquery_ui_default_class +'" href="">'+ options.nav_label_first +'</a>';
            var last = !options.show_first_last ? '' : '<a class="last_link '+ jquery_ui_default_class +'" href="">'+ options.nav_label_last +'</a>';
      
      var navigation_html = "";
      
      for(var i = 0; i < options.nav_order.length; i++) {
        switch (options.nav_order[i]) {
        case "first":
          navigation_html += first;
          break;
        case "last":
          navigation_html += last;
          break;
        case "next":
          navigation_html += '<a class="next_link '+ jquery_ui_default_class +'" href="">'+ options.nav_label_next +'</a>';
          break;
        case "prev":
          navigation_html += '<a class="previous_link '+ jquery_ui_default_class +'" href="">'+ options.nav_label_prev +'</a>';
          break;
        case "num":
          navigation_html += less;
          var current_link = 0;
          while(number_of_pages > current_link){
            navigation_html += '<a class="page_link '+ jquery_ui_default_class +'" href="" longdesc="' + current_link +'">'+ (current_link + 1) +'</a>';
            current_link++;
          }
          navigation_html += more;
          break;
        default:
          break;
        }
          
      }
      
      // And add it to the appropriate area of the DOM  
      $nav_panels = $page_container.find(options.nav_panel_id);     
      $nav_panels.html(navigation_html).each(function(){
      
        $(this).find('.page_link:first').addClass('first');
        $(this).find('.page_link:last').addClass('last');
        
      });
      
      // Hide the more/less indicators
      $nav_panels.children('.ellipse').hide();
      
      // Set the active page link styling
      $nav_panels.find('.previous_link').next().next().addClass('active_page '+ jquery_ui_active_class);
      
      /* Setup Page Display */
      // And hide all pages
      $items.hide();
      // Show the first page      
      $items.slice(0, meta.data(items_per_page)).show();

      /* Setup Nav Menu Display */
      // Page number slices
      
      total_page_no_links = $page_container.children(options.nav_panel_id+':first').children('.page_link').size();
      options.num_page_links_to_display = Math.min(options.num_page_links_to_display,total_page_no_links);

      $nav_panels.children('.page_link').hide(); // Hide all the page links
      
      // And only show the number we should be seeing
      $nav_panels.each(function(){
        $(this).children('.page_link').slice(0, options.num_page_links_to_display).show();      
      });
      
      /* Bind the actions to their respective links */
       
      // Event handler for 'First' link
      $page_container.find('.first_link').click(function(e){
        e.preventDefault();
        
        movePageNumbersRight($(this),0);
        gotopage(0);        
      });     
      
      // Event handler for 'Last' link
      $page_container.find('.last_link').click(function(e){
        e.preventDefault();
        var lastPage = total_page_no_links - 1;
        movePageNumbersLeft($(this),lastPage);
        gotopage(lastPage);       
      });     
      
      // Event handler for 'Prev' link
      $page_container.find('.previous_link').click(function(e){
        e.preventDefault();
        showPrevPage($(this));
      });
      
      
      // Event handler for 'Next' link
      $page_container.find('.next_link').click(function(e){
        e.preventDefault();       
        showNextPage($(this));
      });
      
      // Event handler for each 'Page' link
      $page_container.find('.page_link').click(function(e){
        e.preventDefault();
        gotopage($(this).attr('longdesc'));
        
        if($page_container.find('.next_link').siblings('.active_page').next('.page_link').length==true){
          new_page = parseInt(meta.data(current_page)) + 1;
          movePageNumbersLeft($page_container.find('.next_link'),new_page);
        } else if (options.wrap_around) {
          gotopage(0);
        }
        
        if($page_container.find('.next_link').siblings('.active_page').prev('.page_link').length==true){
          new_page = parseInt(meta.data(current_page)) - 1;
          movePageNumbersRight($page_container.find('.next_link'),new_page);
        }else if(options.wrap_around){
                  gotopage(total_page_no_links-1);   
        }
      });     
      
      // Goto the required page
      gotopage(parseInt(options.start_page));
      toggleMoreLess();
            if(!options.wrap_around)
          tagNextPrev();
    });
    
    function showPrevPage(e){
      new_page = parseInt(meta.data(current_page)) - 1;           
      
      // Check that we aren't on a boundary link
      if($(e).siblings('.active_page').prev('.page_link').length==true){
        movePageNumbersRight(e,new_page);
        gotopage(new_page);
      }else if(options.wrap_around){
                gotopage(total_page_no_links-1);   
      }
        
    };
      
    function showNextPage(e){
      new_page = parseInt(meta.data(current_page)) + 1;
      
      // Check that we aren't on a boundary link
      if($(e).siblings('.active_page').next('.page_link').length==true){    
        movePageNumbersLeft(e,new_page);
        gotopage(new_page);
      } else if (options.wrap_around) {
        gotopage(0);
      }
        
    };
      
    function gotopage(page_num){
      
      var ipp = parseInt(meta.data(items_per_page));
      
      var isLastPage = false;
      
      // Find the start of the next slice
      start_from = page_num * ipp;
      
      // Find the end of the next slice
      end_on = start_from + ipp;
      // Hide the current page  
      var items = $items.hide().slice(start_from, end_on);
      
      items.show();
      
      // Reassign the active class
      $page_container.find(options.nav_panel_id).children('.page_link[longdesc=' + page_num +']').addClass('active_page '+ jquery_ui_active_class)
                           .siblings('.active_page')
                           .removeClass('active_page ' + jquery_ui_active_class);                    
      
      // Set the current page meta data             
      meta.data(current_page,page_num);
      
      $page_container.find(options.nav_info_id).html(options.nav_label_info.replace("{0}",start_from+1).
          replace("{1}",start_from + items.length).replace("{2}",$items.length));
      
      // Hide the more and/or less indicators
      toggleMoreLess();
      
      // Add a class to the next or prev links if there are no more pages next or previous to the active page
      tagNextPrev();
    };  
    
    // Methods to shift the diplayed index of page numbers to the left or right
    function movePageNumbersLeft(e, new_p){
      var new_page = new_p;
      
      var $current_active_link = $(e).siblings('.active_page');
    
      if($current_active_link.siblings('.page_link[longdesc=' + new_page +']').css('display') == 'none'){
        
        $nav_panels.each(function(){
              $(this).children('.page_link')
                .hide() // Hide all the page links
                .slice(parseInt(new_page - options.num_page_links_to_display + 1) , new_page + 1)
                .show();    
              });
      }
      
    } 
    
    function movePageNumbersRight(e, new_p){
      var new_page = new_p;
      
      var $current_active_link = $(e).siblings('.active_page');
      
      if($current_active_link.siblings('.page_link[longdesc=' + new_page +']').css('display') == 'none'){
                        
        $nav_panels.each(function(){
              $(this).children('.page_link')
                .hide() // Hide all the page links
                .slice( new_page , new_page + parseInt(options.num_page_links_to_display))
                .show();
              });
      }
    }
    
    // Show or remove the ellipses that indicate that more page numbers exist in the page index than are currently shown
    function toggleMoreLess(){
                           
      if(!$nav_panels.children('.page_link:visible').hasClass('last')){         
        $nav_panels.children('.more').show();
      }else {
        $nav_panels.children('.more').hide();
      }
      
      if(!$nav_panels.children('.page_link:visible').hasClass('first')){
        $nav_panels.children('.less').show();
      }else {
        $nav_panels.children('.less').hide();
      }     
    }
    
        /* Add the style class ".no_more" to the first/prev and last/next links to allow custom styling */
      function tagNextPrev() {
      if($nav_panels.children('.last').hasClass('active_page')){
        $nav_panels.children('.next_link').add('.last_link').addClass('no_more ' + jquery_ui_disabled_class);
      } else {
        $nav_panels.children('.next_link').add('.last_link').removeClass('no_more ' + jquery_ui_disabled_class);
      }
      
      if($nav_panels.children('.first').hasClass('active_page')){
        $nav_panels.children('.previous_link').add('.first_link').addClass('no_more ' + jquery_ui_disabled_class);
      } else {
        $nav_panels.children('.previous_link').add('.first_link').removeClass('no_more ' + jquery_ui_disabled_class);
      }
    }
    
  };
  
})(jQuery);