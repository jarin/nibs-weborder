<?php $baseUrl=base_url() ?>
<!DOCTYPE html>
<html>
  <head>
    <title><?=((trim($page["pagetitle"])=="")?("Bombay Spice"):(ucfirst($page["pagetitle"])))?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=$baseUrl?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$baseUrl?>assets/css/bombay.css" rel="stylesheet">
    <script src="<?=$baseUrl?>assets/js/jquery.js"></script>
    <script src="<?=$baseUrl?>assets/js/bombay_spice.js"></script>
 
    <script type="text/javascript">
     base_url="<?=$baseUrl?>";
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
      
      
      
      
    <div id="headerArea">
    	<div class="top">
        	<div class="container inner">
            	<span class="left">
                    <span>For bookings</span>
                    <span>call us on</span>
                    <span class="phone">01283 550 097</span>
                    <!-- <span>or visit <a href="#">reservations</a></span> -->
                </span><!-- .left -->
                
                <span class="fbLikeArea">
                    <?php if(!isset($noFoodRating) || $noFoodRating!=true) {?>
                        <div class="fb-like" data-href="http://bombayspice.net/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                        <?php }else{ ?>
                            <?=_showFoodRating($this);?>
                       <?php }?>
                    
                </span><!-- .fbLikeArea -->
            	<span class="logo">
            		<a href="#"><img class="img-responsive" src="<?=$baseUrl?>assets/images/common/top_logo.png" alt="Bombay Spice"></a>
                </span><!-- .logo -->
            </div><!-- .container inner -->
        </div><!-- .top -->

        <div id="navArea">
    		<div class="masthead container">
    
        <ul class="nav nav-justified">
          <?=$headerMenu;?>
          <?php if(!isset($fbLikeInMenu) || $fbLikeInMenu!=true) {?>
          <li  style='padding-left:5px'><div class="fb-like" data-href="http://bombayspice.net/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
          <?php } ?>
        </ul>
      </div>
        </div><!-- #navArea -->
    </div><!-- #headerArea -->


    <div id="mainContent">
                
                
