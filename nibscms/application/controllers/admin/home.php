<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
        $this->_userImg=$this->session->userdata("uimg");

		//controller Specific
		$this->tab="dashboard";
		$this->load->model("settings_m","settings");
		$this->load->model("activity_m","activity");
		$this->load->model("dashboard_m","dashboard");
		$this->load->model("users_m","users");
		$this->load->model("order_m","order");
		$this->load->model("reservation_m","reservation");
		//Settings
		$this->SETTINGS=array();
		_loadSettings($this);
	}

	function index($msg=""){
	  if(_checkAuthentication($this)){
	    $user_id=$this->_userid;	   
	    if($this->_utype =='business'){
	    	$data = $this->input->post(NULL, TRUE);
	    	if(isset($data['dates'])){
	    		$d = explode("-", $data['dates']);
	    	
	    		$from_date = trim($d[0]);
	    		$to_date = trim($d[1]);
	    	}else{
	    		$from_date = date('Y.m.d');
	    		$to_date = date('Y.m.d');
	    	}
	    	$dates = $from_date." - ".$to_date;
	    	
	    	$this->order->tablename=$this->order->restaurant;
	    	$restaurant = $this->order->getRowBy('user_id',$user_id);
	    	$this->order->tablename=$this->order->orderType;
	    	$orderStatus = $this->order->getRowBy('order_type','Web Order');
	    	
	    	$this->order->tablename=$this->order->order;
	    	
	    	$total_order = $this->order->countBy(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id']));
	    	$total_sales = $this->order->totalSales(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id']));
	    	$total_delivery = $this->order->countBy(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id'], 'web_order_type'=>'delivery'));
	    	$total_collection = $this->order->countBy(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id'], 'web_order_type'=>'collection'));
	    	$total_sales_net = $this->order->totalNetSales(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id']));
	    	$total_reservation = $this->reservation->countBy(array('reservation_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'reservation_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'restaurant_id'=>$restaurant['id']));
	    	
	    	$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
	    	$this->load->view("admin/business_dashboard",array(
	    			'totalOrder' => $total_order,
	    			'totalSales'=>$total_sales['sub_total'],
	    			'totalDelivery'=>$total_delivery, 
	    			'totalCollection'=>$total_collection,
	    			'totalNetSate' =>$total_sales_net['total'],
	    			'total_reservation' => $total_reservation,
	    			"dates"=>$dates));
	    	$this->load->view("admin/admin_foot");
	    } else {
	    	//normalise user dashboard widget entries
	    	$widgets=$this->dashboard->getAll();
	    	$userWidgets=$this->dashboard->getUserWidgets($user_id);
	    	if(count($widgets)!=count($userWidgets)){
	    		foreach($widgets as $w){
	    			$found=false;
	    			if(is_array($userWidgets)){
	    				foreach ($userWidgets as $uw) {
	    					if($uw["widget_id"]==$w["id"]){
	    						$found=true;
	    						break;
	    					}
	    				}
	    			}
	    			if(!$found){
	    				@$this->db->insert($this->dashboard->user_tablename,array(
	    						"user_id"=>$user_id,
	    						"widget_id"=>$w["id"],
	    						"minimised"=>0,
	    						"hidden"=>1
	    				));
	    			}
	    		}
	    	}
	    	
	    	//retreive normalised widgets
	    	$widgets=$this->dashboard->getUserWidgetsJoined($user_id);
	    	
	    	$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
	    	$this->load->view("admin/dashboard",array("widgets"=>$widgets));
	    	$this->load->view("admin/admin_foot");
	    }
	    
    }
	}

	function reoderWidgets($type="left"){
	  if(_checkAuthentication($this)){
  	  switch($type){
  	    case "left":
  	      $array=$this->input->post("widget");
  	      foreach($array as $order=>$id){
    				@$this->db->update($this->dashboard->user_tablename,array("sort_order"=>$order,"column"=>"left"),array("id"=>$id));
    			}
  	      echo '{success:"success"}';
  	    break;
  	    case "right":
  	      $array=$this->input->post("widget");
  	      foreach($array as $order=>$id){
    				@$this->db->update($this->dashboard->user_tablename,array("sort_order"=>$order,"column"=>"right"),array("id"=>$id));
    			}
          echo '{success:"success"}';
  	    break;
  	  }
    }
	}

	function saveWidgetPreferences(){
	  if(_checkAuthentication($this)){
	    $widget=$this->input->post("widget");
	    $key=$this->input->post("key");
	    $value=$this->input->post("value");
	    @$this->db->update($this->dashboard->user_tablename,array($key=>$value),array("id"=>$widget));
	    echo '{success:"success"}';
	  }
	}

	function addWidget(){
	  if(_checkAuthentication($this)){
	    $id=$this->input->post("widget_id");
	    $dropped_on=$this->input->post("dropped_on");
	    $widget=$this->dashboard->get($id);
	    @$this->db->update($this->dashboard->user_tablename,array(
	      "hidden"=>0,
	      "minimised"=>0,
	      "size"=>"",
	      "sort_order"=>0,
	      "column"=>(($dropped_on=="widgetsRight")?("right"):("left"))),
	      array("id"=>$id));
	    echo '{success:"success",successFunction:function(){
	      $("#hwidget_'.$id.'").remove();
	      $("#'.$dropped_on.'").prepend("<div id=\"widget_'.$id.'\" class=\"dashbox\" rel=\"'.$widget["dataurl"].'\"><div class=\"headertoolbar\"><span>'.$widget["title"].' <a class=\"options\" title=\"Options\" href=\"#\"></a><a class=\"minimise\" title=\"Minimise\" href=\"#\"></a><a class=\"close\" title=\"Close\" href=\"#\"></a></span><ul class=\"options\"><li rel=\"\">Full Width</li><li rel=\"half\">Half Width</li><li rel=\"two_thirds\">2/3<sup>rd</sup> Width</li><li rel=\"third\">1/3<sup>rd</sup> Width</li></ul></div><div class=\"content\"></div></div>");
	      $("#'.$dropped_on.'").trigger("sortupdate");
	    }}';
    }
	}



//===========================Widgets Start

	function viewActivityLog(){
	  if(_checkAuthentication($this)){
	    $a=$this->activity->getAll("time","desc");
	    $html="";
	    if(is_array($a)){
	      $i=0;$cutoff=0;
	      foreach ($a as $act) {
	        $html.="<tr><td class='".$act["type"]."'>".$act["log"]." <abbr title='".date("d/m/Y H:i",$act["time"])."'>"._prettyDate($act["time"])."</abbr></td></tr>";
	        $i++;
	        if($i==20){
	          $cutoff=$act["time"];
	        }
	      }
	    }

	    if($cutoff > 0){
	      $this->db->where("`time` < $cutoff")->delete($this->activity->tablename);
	    }


	    $data="<table cellpadding='4' cellspacing='0' width='100%' class='log'>";
      $data.="<tbody>".$html."</tbody>";
      $data.="</table>";

      $this->load->view("admin/widget_head");
      $this->load->view("admin/widget",array("data"=>$data));
      $this->load->view("admin/widget_foot");

	  }
	}

	function viewAnalytics($type="visits"){
	  if(_checkAuthentication($this)){
	    if($this->SETTINGS["useGoogleAnalytics"]=="Yes" &&
  	  $this->SETTINGS["analyticsEmail"] && $this->SETTINGS["anaylticsPassword"] && $this->SETTINGS["anaylticsProfileID"] ){
  	    $this->load->library("analytics",array(
  				'username' => $this->SETTINGS["analyticsEmail"],
  				'password' => $this->SETTINGS["anaylticsPassword"]
  			));
        $this->analytics->setProfileById('ga:'.$this->SETTINGS["anaylticsProfileID"]);

        $end_date = date('Y-m-d');
  			$start_date = date('Y-m-d', strtotime('-1 month'));

  			$this->analytics->setDateRange($start_date, $end_date);

  			switch($type){
  			  case "visits":
  			    $data=$this->analytics->getVisitors();
  			  break;
  			  case "pageviews":
  			    $data=$this->analytics->getPageviews();
  			  break;
  			  case "referrers":
  			    $data=$this->analytics->getReferrers();
  			  break;
  			  case "keywords":
  			    $data=$this->analytics->getSearchWords();
  			  break;
  			}

        $html="";
        if(is_array($data)){
          $i=0;
          foreach ($data as $k=>$v) {
            if($type=="visits" || $type=="pageviews"){
              $k=preg_replace("/(\d{4})(\d{2})(\d{2})/","$3/$2/$1",$k);
            }
            $html.="<tr><th>$k</th><td>$v</td></tr>";
            $i++;
            if($type!="visits" && $type!="pageviews" && $i==10){
              break;
            }
          }
        }

        $data="<table cellpadding='4' cellspacing='0' width='100%' class='".(($type=="visits" || $type=="pageviews")?("chartable"):("data"))."'>";
        if($type=="visits" || $type=="pageviews"){
          $data.="<thead><tr><th>Date</th><td>".ucfirst($type)."</td></tr></thead>";
        }
        $data.="<tbody>".$html."</tbody>";
        $data.="</table>";

        $this->load->view("admin/widget_head");
        $this->load->view("admin/widget",array("data"=>$data));
        $this->load->view("admin/widget_foot");

  	  }else{
  	    $this->load->view("admin/widget_head");
  	    $this->load->view("admin/widget",array("data"=>"<p>Error: Please make sure all Google Analytics settings are filled on the <a href='".base_url()."admin/settings'>settings panel</a>.</p>"));
  	    $this->load->view("admin/widget_foot");
  	  }
	  }
	}
	
	/**
	 * Show order list in dashboard
	 * 
	 * @param 
	 * 
	 * @return void
	 */
	function orderlist(){
		$orders = array();
		$discount = false;
		$data = $this->input->post(NULL, TRUE);
		
		if($data != FALSE) {
			
				if($data['sortby'] == 'paid') {
					$orderStatus = "Web Order Paid";
				} elseif($data['sortby'] == 'cancel') {
					$orderStatus = "Web Order Cancelled";
					
				} elseif($data['sortby'] == 'transit') {
					$orderStatus = "Web Order Transit";
					
				} elseif($data['sortby'] == 'discount') {
					$discount = true;
					$statusId = 0;
					
				} else {
					$orderStatus = "Web Order Unpaid";
				}
				if(isset($orderStatus)){
					$this->order->tablename=$this->order->orderStatus;
					$statusAr = $this->order->getRowBy('status',$orderStatus);
					if(count($statusAr) > 0){
						$statusId = $statusAr['id'];
					}
				}
				
				$restaurant=$this->order->getweb_business($this->_userid);
				$this->order->tablename=$this->order->orderType;
				$orderType =  $this->order->getRowBy('order_type','Web Order');
				$orderTypeId ='';
				if(count($orderType) > 0){
					$orderTypeId = $orderType['id'];
				}
				
				if(isset($statusId)){
					$orders = $this->order->getOrderByStatus($restaurant[0]['id'], $statusId, $orderTypeId, $discount);
				}
			$this->load->view("admin/order_list",array('orders'=>$orders));
			
		}
	}

}