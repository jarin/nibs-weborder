-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: zadmin_respos
-- ------------------------------------------------------
-- Server version	5.1.69-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `res_button_permission_settings`
--

DROP TABLE IF EXISTS `res_button_permission_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_button_permission_settings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `permission_type_id` smallint(6) DEFAULT NULL,
  `user_id` smallint(6) DEFAULT NULL,
  `value` longtext NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  KEY `IDX_1DB11892F25D6DC4` (`permission_type_id`),
  KEY `IDX_1DB11892A76ED395` (`user_id`),
  CONSTRAINT `FK_1DB11892A76ED395` FOREIGN KEY (`user_id`) REFERENCES `res_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_1DB11892F25D6DC4` FOREIGN KEY (`permission_type_id`) REFERENCES `res_button_permission_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_button_permission_settings`
--

LOCK TABLES `res_button_permission_settings` WRITE;
/*!40000 ALTER TABLE `res_button_permission_settings` DISABLE KEYS */;
INSERT INTO `res_button_permission_settings` VALUES (13,1,NULL,'a:2:{s:9:\"dashboard\";a:2:{s:9:\"left_menu\";a:7:{i:0;s:7:\"kitchen\";i:1;s:9:\"take_away\";i:2;s:9:\"table_map\";i:3;s:7:\"options\";i:4;s:3:\"bar\";i:5;s:10:\"pre_orders\";i:6;s:10:\"web_orders\";}s:10:\"right_menu\";a:8:{i:0;s:8:\"settings\";i:1;s:12:\"reservations\";i:2;s:5:\"stock\";i:3;s:7:\"manager\";i:4;s:8:\"supplies\";i:5;s:9:\"cash_draw\";i:6;s:9:\"customers\";i:7;s:13:\"notifications\";}}s:12:\"order_screen\";a:2:{s:9:\"left_menu\";a:7:{i:0;s:13:\"customer_info\";i:1;s:13:\"convert_order\";i:2;s:11:\"change_user\";i:3;s:10:\"split_bill\";i:4;s:4:\"void\";i:5;s:11:\"print_label\";i:6;s:6:\"option\";}s:11:\"bottom_menu\";a:9:{i:0;s:4:\"home\";i:1;s:10:\"print_bill\";i:2;s:4:\"misc\";i:3;s:8:\"gratuity\";i:4;s:7:\"arrange\";i:5;s:7:\"add_qty\";i:6;s:6:\"modify\";i:7;s:10:\"send_order\";i:8;s:8:\"pay_bill\";}}}'),(14,2,2,'a:2:{s:9:\"dashboard\";a:2:{s:9:\"left_menu\";a:7:{i:0;s:7:\"kitchen\";i:1;s:9:\"take_away\";i:2;s:9:\"table_map\";i:3;s:7:\"options\";i:4;s:3:\"bar\";i:5;s:10:\"pre_orders\";i:6;s:10:\"web_orders\";}s:10:\"right_menu\";a:8:{i:0;s:8:\"settings\";i:1;s:12:\"reservations\";i:2;s:5:\"stock\";i:3;s:7:\"manager\";i:4;s:8:\"supplies\";i:5;s:9:\"cash_draw\";i:6;s:9:\"customers\";i:7;s:13:\"notifications\";}}s:12:\"order_screen\";a:2:{s:9:\"left_menu\";a:7:{i:0;s:13:\"customer_info\";i:1;s:13:\"convert_order\";i:2;s:11:\"change_user\";i:3;s:10:\"split_bill\";i:4;s:4:\"void\";i:5;s:11:\"print_label\";i:6;s:6:\"option\";}s:11:\"bottom_menu\";a:9:{i:0;s:4:\"home\";i:1;s:10:\"print_bill\";i:2;s:4:\"misc\";i:3;s:8:\"gratuity\";i:4;s:7:\"arrange\";i:5;s:7:\"add_qty\";i:6;s:6:\"modify\";i:7;s:10:\"send_order\";i:8;s:8:\"pay_bill\";}}}');
/*!40000 ALTER TABLE `res_button_permission_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_button_permission_type`
--

DROP TABLE IF EXISTS `res_button_permission_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_button_permission_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `permission_type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_button_permission_type`
--

LOCK TABLES `res_button_permission_type` WRITE;
/*!40000 ALTER TABLE `res_button_permission_type` DISABLE KEYS */;
INSERT INTO `res_button_permission_type` VALUES (1,'Global'),(2,'User');
/*!40000 ALTER TABLE `res_button_permission_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_call_log`
--

DROP TABLE IF EXISTS `res_call_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_call_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` smallint(6) DEFAULT NULL,
  `call_date` datetime DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_91F34805A76ED395` (`user_id`),
  CONSTRAINT `FK_91F34805A76ED395` FOREIGN KEY (`user_id`) REFERENCES `res_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_call_log`
--

LOCK TABLES `res_call_log` WRITE;
/*!40000 ALTER TABLE `res_call_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_call_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_category`
--

DROP TABLE IF EXISTS `res_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_category` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `parent_id` smallint(6) DEFAULT NULL,
  `sequence_number` smallint(6) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `top_color` varchar(9) NOT NULL,
  `bottom_color` varchar(9) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_41DC95EAFF8A180B` (`last_updated_by`),
  KEY `IDX_41DC95EA727ACA70` (`parent_id`),
  CONSTRAINT `FK_41DC95EA727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `res_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_41DC95EAFF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_category`
--

LOCK TABLES `res_category` WRITE;
/*!40000 ALTER TABLE `res_category` DISABLE KEYS */;
INSERT INTO `res_category` VALUES (39,4,NULL,1,'Starters','Appetizers','#ff5c5c','#ff6666',1,'2014-01-08 08:28:00'),(44,4,39,1,'Seafood Starter','Seafood Starter','#ff6161','#ff0000',1,'2014-01-08 08:31:30'),(47,7,39,1,'Meat Starters','Meat Appetizers','#ff5c5c','#ff6666',1,'2014-02-26 07:18:23'),(51,4,39,1,'Vegtarian Starter','Vegtarian Starter','#ff5c5c','#ff6666',1,'2014-01-08 08:53:22'),(52,4,39,1,'Sharing Platters','Sharing Platters','#ff6161','#ff0000',1,'2014-01-08 08:53:22'),(53,4,39,1,'Papadoms & Relish','Papadoms & Relish','#ff5c5c','#ff6666',1,'2014-01-08 08:53:22'),(59,7,39,1,'Seafood Appetizers','Seafood Appetizers','#ff6161','#ff0000',0,'2014-02-26 07:15:54'),(69,4,NULL,2,'Tandoori Dishes','Tandoori Dishes','#ff6161','#ff0000',1,'2014-01-08 09:04:08'),(70,4,NULL,3,'Biryani','Biryani Dishes','#ff5c5c','#ff6666',1,'2014-01-08 09:04:08'),(71,4,NULL,4,'Traditional','Traditional','#ff5c5c','#ff6666',1,'2014-01-08 09:04:08'),(72,4,71,4,'Balti','Balti Dishes','#ff6161','#ff0000',1,'2014-01-08 09:04:08'),(73,4,71,4,'Kurma','Kurma Dishes','#ff5c5c','#ff6666',1,'2014-01-08 09:04:08'),(74,4,71,4,'Bhuna','Bhuna Dishes','#ff6161','#ff0000',1,'2014-01-08 09:04:08'),(75,4,71,4,'Rogan Josh','Rogan Josh','#ff5c5c','#ff6666',1,'2014-01-08 09:04:08'),(76,4,71,4,'Pathia','Pathia Dishes','#ff6161','#ff0000',1,'2014-01-08 09:04:09'),(77,4,71,4,'Sagwala','Sagwala Dishes','#ff5c5c','#ff6666',1,'2014-01-08 09:04:09'),(78,4,71,4,'Dhansak','Dhansak Dishes','#ff6161','#ff0000',1,'2014-01-08 09:04:09'),(79,4,71,4,'Jalfrezi','Jalfrezi Dishes','#ff5c5c','#ff6666',1,'2014-01-23 09:25:45'),(80,4,71,4,'Curry','Curry Dishes','#ff6161','#ff0000',1,'2014-01-08 09:04:09'),(81,4,71,4,'Madras','Madras Dishes','#ff5c5c','#ff6666',1,'2014-01-23 09:25:01'),(82,4,71,4,'Vindaloo','Vindaloo Dishes','#ff6161','#ff0000',1,'2014-01-08 09:04:09'),(83,4,71,4,'Phall','Phall Dishes','#ff6162','#ff0001',1,'2014-01-08 09:04:09'),(86,4,NULL,5,'India Red Specials','India Red Specials','#ff5c5c','#ff6000',1,'2014-01-23 09:41:23'),(87,4,NULL,6,'Chefs Specials','Chefs Specials','#ff6161','#ff0000',1,'2014-01-24 08:06:09'),(88,4,NULL,7,'English Dishes','English Dishes','#ff6161','#ff0000',1,'2014-01-24 08:09:01'),(89,4,NULL,12,'Extra items','Extra items','#ff6161','#ff0000',1,'2014-01-24 08:10:33'),(90,4,NULL,11,'Side Dishes','Side Dishes','#ff6161','#ff0000',1,'2014-01-24 08:11:14'),(91,4,NULL,9,'Rice','Rice Dishes','#ff6161','#ff0000',1,'2014-01-27 08:52:42'),(97,4,NULL,10,'Nans & Breads','Nan Breads','#ff6161','#ff0000',1,'2014-01-25 12:48:26'),(105,4,NULL,16,'Desserts','Various Desserts','#ff6161','#ff0000',1,'2014-01-27 08:54:18'),(106,4,NULL,15,'Hot & Cold Drinks','Hot & Cold Drinks','#ff6161','#ff0000',1,'2014-01-25 12:21:07'),(107,4,39,1,'Healthy Starters','Healthy Starters','#ff6161','#ff0000',1,'2014-01-26 02:57:01'),(108,4,NULL,13,'Healthy Main Dishes','Healthy Main Dishes','#ff6161','#ff0000',1,'2014-01-26 03:20:36'),(111,4,NULL,14,'Healthy Sundries','Healthy Sundries','#ff6161','#ff0000',1,'2014-01-26 03:53:06'),(112,4,NULL,8,'Early Bird Special','Early Bird Special','#800080','#800080',1,'2014-02-09 04:31:13'),(113,4,112,8,'Meat Starters (Early Bird)','Meat Starters (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:17:21'),(114,4,112,8,'Fish Starters (Early Bird)','Fish Starters (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:21:10'),(115,4,114,3,'Veg Starters (Early Bird)','Veg Starters (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:19:41'),(116,4,112,8,'Veg Starter (Early Bird)','Veg Starter (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:22:12'),(117,4,112,8,'English Dishes (Early Bird)','English Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:24:17'),(118,4,112,8,'Tandoori Dishes (Early Bird)','Tandoori Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:25:56'),(119,4,112,8,'Biryani Dishes (Early Bird)','Biryani Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:27:00'),(120,4,112,8,'Balti Dishes (Early Bird)','Balti Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:43:06'),(121,4,112,8,'Bhuna Dishes (Early Bird)','Bhuna Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:44:36'),(122,4,112,8,'Jalfrezi Dishes (Early Bird)','Jalfrezi Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:45:43'),(123,4,112,8,'Patia Dishes (Early Bird)','Patia Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:47:09'),(124,4,112,8,'Korma Dishes (Early Bird)','Korma Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:48:53'),(125,4,112,8,'Curry Dishes (Early Bird)','Curry Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:50:39'),(126,4,112,8,'Madras Dishes (Early Bird)','Madras Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:52:55'),(127,4,112,8,'Sagwala Dishes (Early Bird)','Sagwala Dishes (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:54:46'),(128,4,112,8,'Rogan Josh (Early Bird)','Rogan Josh (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 04:56:05'),(129,4,112,8,'India Red Specials (Early Bird)','India Red Specials (Early Bird)','#ffff00','#ffff00',1,'2014-02-09 05:00:19'),(130,4,112,8,'Sundries (early Bird)','Sundries (early Bird)','#ffff00','#ffff00',1,'2014-02-09 05:01:43'),(131,4,112,8,'Early Bird @ Â£8.95','Early Bird @ Â£8.95','#ffff00','#ffff00',1,'2014-02-09 05:04:51'),(132,7,112,8,'Vindaloo (Early Bird)','Vindaloo (Early Bird)','#ffff00','#ffff00',1,'2014-02-13 10:06:31'),(133,7,112,8,'Dhansak (Early Bird)','Dhansak (Early Bird)','#ffff00','#ffff00',1,'2014-02-13 10:11:54'),(134,7,112,8,'Chefs Specials (Early Bird)','Chefs Specials','#ff5c5c','#ff6666',1,'2014-02-26 08:04:12'),(135,4,71,4,'fdd','This is fddddddddddd','#ffadad','#ff9a57',1,'2014-03-11 12:04:25');
/*!40000 ALTER TABLE `res_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_city`
--

DROP TABLE IF EXISTS `res_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_city`
--

LOCK TABLES `res_city` WRITE;
/*!40000 ALTER TABLE `res_city` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_converted_to_take_away`
--

DROP TABLE IF EXISTS `res_converted_to_take_away`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_converted_to_take_away` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` smallint(6) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `quantity_type_id` smallint(6) DEFAULT NULL,
  `quantity` smallint(6) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_870F8FBB9395C3F3` (`customer_id`),
  KEY `IDX_870F8FBB8D9F6D38` (`order_id`),
  KEY `IDX_870F8FBB4584665A` (`product_id`),
  KEY `IDX_870F8FBBFF8A180B` (`last_updated_by`),
  KEY `IDX_870F8FBB36F84596` (`quantity_type_id`),
  CONSTRAINT `FK_870F8FBB36F84596` FOREIGN KEY (`quantity_type_id`) REFERENCES `res_quantity_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_870F8FBB4584665A` FOREIGN KEY (`product_id`) REFERENCES `res_product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_870F8FBB8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `res_order` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_870F8FBB9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `res_customer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_870F8FBBFF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_converted_to_take_away`
--

LOCK TABLES `res_converted_to_take_away` WRITE;
/*!40000 ALTER TABLE `res_converted_to_take_away` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_converted_to_take_away` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_country`
--

DROP TABLE IF EXISTS `res_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_country` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_country`
--

LOCK TABLES `res_country` WRITE;
/*!40000 ALTER TABLE `res_country` DISABLE KEYS */;
INSERT INTO `res_country` VALUES (1,'United Kingdom');
/*!40000 ALTER TABLE `res_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_customer`
--

DROP TABLE IF EXISTS `res_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `town_id` int(11) DEFAULT NULL,
  `state_id` smallint(6) DEFAULT NULL,
  `country_id` smallint(6) DEFAULT NULL,
  `customer_key` varchar(10) NOT NULL,
  `available_credit` decimal(10,2) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `house_no` varchar(150) DEFAULT NULL,
  `house_name` varchar(150) DEFAULT NULL,
  `street` varchar(150) DEFAULT NULL,
  `post_code` varchar(150) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `land_line` varchar(50) DEFAULT NULL,
  `membership_no` varchar(50) DEFAULT NULL,
  `membership_points` int(11) DEFAULT NULL,
  `dist` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C6A9022275E23604` (`town_id`),
  KEY `IDX_C6A902225D83CC1` (`state_id`),
  KEY `IDX_C6A90222F92F3E70` (`country_id`),
  CONSTRAINT `FK_C6A902225D83CC1` FOREIGN KEY (`state_id`) REFERENCES `res_state` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_C6A9022275E23604` FOREIGN KEY (`town_id`) REFERENCES `res_city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_C6A90222F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `res_country` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_customer`
--

LOCK TABLES `res_customer` WRITE;
/*!40000 ALTER TABLE `res_customer` DISABLE KEYS */;
INSERT INTO `res_customer` VALUES (1,NULL,4,1,'Naresh','0.00','Naresh','123','KAL','abc12345','66543','naresh@outlook.com','345345345345','9211100420','',0,'Unavailable'),(2,NULL,12,NULL,'Raj','0.00','Raj','31','','Balagiri','WS1 4BA','raj@outlook.com','13133423455','7868767766','',0,'Unavailable'),(3,NULL,4,1,'Jamal ahm','0.00','Jamal ahmed','31 tame close',NULL,'eeee','654435','jamal@gmail.com','234234234','0802261722','',0,'Unavailable'),(4,NULL,NULL,1,'TINU','0.00','TINU','456','df','rtt','4444','rtf@df.com','9911223344','456','',0,'Unavailable'),(5,NULL,5,1,'raj','50.00','Raj',NULL,NULL,NULL,NULL,'raj@w.com',NULL,NULL,'125',12,NULL),(6,NULL,NULL,NULL,'na','0.00','na',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'187555',489,NULL),(7,NULL,NULL,NULL,'na','0.00','na',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'14278',54,NULL),(8,NULL,NULL,NULL,'JAMAL','0.00','JAMAL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'584',5896,NULL),(9,NULL,NULL,NULL,'bony','0.00','bony','30',NULL,'leichester square',NULL,NULL,'454512316',NULL,'',150,NULL),(10,NULL,NULL,NULL,'NareshO','0.00','NareshO',NULL,NULL,NULL,NULL,NULL,'00919873250848','9211100420','',0,NULL),(11,NULL,NULL,NULL,'faid','0.00','faid','3',NULL,'traffalgar square',NULL,NULL,'4654631316',NULL,'',0,'Unavailable'),(12,NULL,NULL,NULL,'','0.00','Tariqul Islam','148 west bromwich road',NULL,'Walsall','ws1 3hn','tareq@webkutir.net','01911291717',NULL,NULL,0,NULL),(13,NULL,1,1,'Jamal1234','0.00','Jamal1234','1234','','1234','62323',NULL,'234234234',NULL,'',0,'186 km'),(15,NULL,10,NULL,'','2960.00','Jamal Ahmed','31 Tame Close',NULL,NULL,'WS1 4BA','jamal@nibssolutions.com','07944472217',NULL,NULL,600,'13.5miles'),(16,NULL,1,NULL,'Aalam','0.00','Aalam','767',NULL,'678',NULL,NULL,'9898980',NULL,'',0,'Unavailable'),(17,NULL,1,NULL,'gaurav','0.00','gaurav','54786',NULL,NULL,NULL,'','908908908',NULL,'',0,'Unavailable'),(18,NULL,NULL,NULL,'hhhhhh','0.00','hhhhhh',NULL,NULL,NULL,NULL,NULL,'897897',NULL,'',0,'Unavailable'),(19,NULL,NULL,NULL,'jjjjjj','0.00','jjjjjj',NULL,NULL,NULL,NULL,NULL,'99999',NULL,'',0,'Unavailable'),(20,NULL,NULL,NULL,'kkkkkkkk','0.00','kkkkkkkk',NULL,NULL,NULL,NULL,NULL,'8888888888',NULL,'',0,'Unavailable'),(21,NULL,NULL,NULL,'jkhkjh','0.00','jkhkjh',NULL,NULL,NULL,NULL,NULL,'kjkkjl',NULL,'',0,'Unavailable'),(22,NULL,NULL,NULL,'rabindra','0.00','rabindra',NULL,NULL,NULL,NULL,NULL,'3434',NULL,'',0,'Unavailable'),(23,NULL,NULL,NULL,'ManojKumar','0.00','ManojKumar','2333',NULL,'Galleria','33333',NULL,'34234234',NULL,'',0,'Unavailable'),(24,NULL,NULL,NULL,'kumarrrr','0.00','kumarrrr','767','8768','gggg',NULL,NULL,'3534534545',NULL,'',0,'Unavailable'),(25,NULL,11,1,'','0.00','kabir ahmed','92','parsons house','hall place','w2 1nf',NULL,NULL,NULL,NULL,NULL,'126.8miles'),(26,NULL,1,NULL,'Alok','0.00','Alok','123',NULL,NULL,NULL,NULL,'345345345','9211100420','',0,NULL),(27,NULL,NULL,NULL,'Anukj','0.00','Anukj',NULL,NULL,NULL,NULL,NULL,'09790809',NULL,'',0,'Unavailable'),(28,NULL,NULL,NULL,'Raka','0.00','Raka',NULL,NULL,NULL,NULL,NULL,'745874544754',NULL,'',0,'Unavailable'),(29,NULL,12,1,'','0.00','bony amin','12',NULL,'uttara','1230','bony1186@gmail.com','01717252520',NULL,NULL,0,'6.8miles'),(31,NULL,7,1,'Anuj','0.00','Anuj','1234','','Galleria','62323','anu@gmail.com','234234234','123456789','',0,'Unavailable'),(32,NULL,NULL,NULL,'Ritesh','0.00','Ritesh',NULL,NULL,NULL,NULL,'kumarrajesh@email.com','7425878875',NULL,'',0,'Unavailable'),(35,NULL,NULL,NULL,'khaled','0.00','khaled','',NULL,NULL,NULL,NULL,'07913978465',NULL,'',0,NULL),(36,NULL,NULL,NULL,'Alan','0.00','Alan','46',NULL,NULL,'ws12 3tg',NULL,'07971955617',NULL,'',0,NULL),(37,NULL,13,NULL,'','0.00','dddddddd','','','ddddd','','ddddd@yahoo.com','034832342','','',0,'0.0miles'),(39,NULL,NULL,NULL,'','0.00','','','','','','','','','',0,NULL),(40,NULL,NULL,1,'naresh ph','0.00','naresh phuloria','76','','sushant lok','122009','','','','',0,NULL),(41,NULL,NULL,NULL,'Rajgaurav','0.00','Rajgaurav','31','',NULL,'WS1 4BA',NULL,NULL,'7868767766','',0,'13Â 485 km'),(42,NULL,4,1,'absrt','0.00','absrt','7676','uyuy','6767','345345','','345345','345345345','',0,'Unavailable'),(43,NULL,NULL,NULL,'kumar','0.00','kumar','123','123','123',NULL,NULL,'121212','121212','',0,'Unavailable'),(44,NULL,NULL,NULL,'axxa','0.00','axxa','axxa','','axxa','','','2323423','234234234','',0,'Unavailable'),(45,NULL,NULL,NULL,'rrrrr','0.00','rrrrr','3333','345ertert','ertert','','','2343424','234234','',0,''),(46,NULL,NULL,NULL,'naresh p','0.00','naresh p','76','','','','','','','',0,''),(47,NULL,NULL,NULL,'tom sahay','0.00','tom sahay','90',NULL,NULL,'ws41ba',NULL,NULL,NULL,'',0,NULL);
/*!40000 ALTER TABLE `res_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_customer_message`
--

DROP TABLE IF EXISTS `res_customer_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_customer_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sender` smallint(6) DEFAULT NULL,
  `receiver` bigint(20) DEFAULT NULL,
  `send_date` datetime NOT NULL,
  `subject` varchar(150) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `message_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B67FF85D5F004ACF` (`sender`),
  KEY `IDX_B67FF85D3DB88C96` (`receiver`),
  CONSTRAINT `FK_B67FF85D3DB88C96` FOREIGN KEY (`receiver`) REFERENCES `res_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_B67FF85D5F004ACF` FOREIGN KEY (`sender`) REFERENCES `res_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_customer_message`
--

LOCK TABLES `res_customer_message` WRITE;
/*!40000 ALTER TABLE `res_customer_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_customer_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_dashboard_settings`
--

DROP TABLE IF EXISTS `res_dashboard_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_dashboard_settings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` smallint(6) DEFAULT NULL,
  `value` longtext NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  KEY `IDX_39DBEC18A76ED395` (`user_id`),
  CONSTRAINT `FK_39DBEC18A76ED395` FOREIGN KEY (`user_id`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_dashboard_settings`
--

LOCK TABLES `res_dashboard_settings` WRITE;
/*!40000 ALTER TABLE `res_dashboard_settings` DISABLE KEYS */;
INSERT INTO `res_dashboard_settings` VALUES (10,2,'a:2:{s:5:\"floor\";a:1:{i:0;s:1:\"1\";i:1;s:1:\"2\"}s:6:\"others\";a:2:{i:0;s:8:\"delivery\";i:1;s:10:\"collection\";}}'),(11,3,'a:2:{s:5:\"floor\";a:1:{i:0;s:1:\"2\";}s:6:\"others\";a:2:{i:0;s:8:\"delivery\";i:1;s:10:\"collection\";}}'),(12,6,'a:2:{s:5:\"floor\";a:1:{i:0;s:1:\"4\";}s:6:\"others\";a:2:{i:0;s:8:\"delivery\";i:1;s:10:\"collection\";}}'),(13,17,'a:2:{s:5:\"floor\";a:3:{i:0;s:1:\"2\";i:1;s:1:\"4\";i:2;s:1:\"5\";}s:6:\"others\";a:2:{i:0;s:8:\"delivery\";i:1;s:10:\"collection\";}}'),(14,7,'a:2:{s:5:\"floor\";a:1:{i:0;s:1:\"6\";}s:6:\"others\";a:2:{i:0;s:8:\"delivery\";i:1;s:10:\"collection\";}}'),(15,16,'a:2:{s:5:\"floor\";a:1:{i:0;s:1:\"5\";}s:6:\"others\";a:2:{i:0;s:8:\"delivery\";i:1;s:10:\"collection\";}}');
/*!40000 ALTER TABLE `res_dashboard_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_dbbackup_settings`
--

DROP TABLE IF EXISTS `res_dbbackup_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_dbbackup_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_hour` varchar(2) NOT NULL,
  `schedule_ap` varchar(2) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remote_directory` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_dbbackup_settings`
--

LOCK TABLES `res_dbbackup_settings` WRITE;
/*!40000 ALTER TABLE `res_dbbackup_settings` DISABLE KEYS */;
INSERT INTO `res_dbbackup_settings` VALUES (5,'12','am','md.faridkhandaker@yahoo.com','','','','');
/*!40000 ALTER TABLE `res_dbbackup_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_deposit_type`
--

DROP TABLE IF EXISTS `res_deposit_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_deposit_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `deposit_type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_deposit_type`
--

LOCK TABLES `res_deposit_type` WRITE;
/*!40000 ALTER TABLE `res_deposit_type` DISABLE KEYS */;
INSERT INTO `res_deposit_type` VALUES (1,'Card'),(2,'Cash'),(3,'Web');
/*!40000 ALTER TABLE `res_deposit_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_floor`
--

DROP TABLE IF EXISTS `res_floor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_floor` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `floor_name` varchar(100) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F8680085FF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_F8680085FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_floor`
--

LOCK TABLES `res_floor` WRITE;
/*!40000 ALTER TABLE `res_floor` DISABLE KEYS */;
INSERT INTO `res_floor` VALUES (1,4,'1st Floor','2014-03-11 09:48:56'),(2,NULL,'2nd Floor','2013-09-27 16:29:09'),(4,4,'3rd Floor','2013-09-30 12:09:56'),(5,4,'4th Floor','2013-09-30 12:10:08'),(6,4,'5th Floor','2013-09-30 12:10:29');
/*!40000 ALTER TABLE `res_floor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_ingredient`
--

DROP TABLE IF EXISTS `res_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_ingredient` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `ingredient_name` varchar(150) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  `price_without` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F9D10D82FF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_F9D10D82FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_ingredient`
--

LOCK TABLES `res_ingredient` WRITE;
/*!40000 ALTER TABLE `res_ingredient` DISABLE KEYS */;
INSERT INTO `res_ingredient` VALUES (206,4,'Curry Sauce','0.00',1,'2014-02-10 06:08:37','0.00'),(207,4,'Dhaniya','0.00',1,'2014-02-10 06:08:38','0.00'),(208,4,'Green Chillies','0.00',1,'2014-02-10 06:09:42','0.00'),(209,4,'King Prawn','1.00',1,'2014-02-10 06:08:38','1.00'),(211,4,'Malaya Style','1.00',1,'2014-02-10 06:08:38','0.00'),(212,4,'Mild','0.00',1,'2014-02-10 06:08:38','0.00'),(213,4,'Mint Sauce','0.00',1,'2014-02-10 06:08:38','0.00'),(214,4,'Mushrooms','0.50',1,'2014-02-10 06:08:38','0.00'),(216,4,'Oil','0.00',1,'2014-02-21 09:12:24','0.00'),(218,4,'Onion Salad','0.00',1,'2014-02-10 06:08:39','0.00'),(219,4,'Persian Style','1.00',1,'2014-02-10 06:08:39','0.00'),(220,4,'Salad','1.00',1,'2014-02-10 06:08:39','1.00'),(221,4,'Slightly Hot','0.00',1,'2014-02-21 09:12:53','0.00'),(222,4,'Special Instruction','1.00',1,'2014-02-21 09:15:16','0.00'),(223,4,'sultana','0.00',1,'2014-02-21 09:16:14','0.00'),(224,4,'Madras','0.00',1,'2014-02-10 06:08:40','0.00'),(225,4,'Massala Souce','0.00',1,'2014-02-10 06:08:40','0.00'),(226,4,'Vindaloo','0.00',1,'2014-02-10 06:08:40','0.00'),(227,4,'Chi Tikka','0.50',1,'2014-02-21 09:08:58','0.00'),(228,4,'Chicken','0.50',1,'2014-02-21 09:09:18','0.00'),(229,4,'Garlic','0.00',1,'2014-02-10 06:08:40','0.00'),(231,4,'Lamb','1.00',1,'2014-02-10 06:08:41','0.00'),(232,4,'Lamb Tikka','1.00',1,'2014-02-10 06:08:41','0.00'),(234,4,'Prawns','1.00',1,'2014-02-21 09:13:47','0.00'),(235,4,'Saucy','0.00',1,'2014-02-10 06:08:41','0.00'),(236,4,'Sk Kabab','1.00',1,'2014-02-10 06:08:41','0.00'),(237,4,'Tomatoes','0.00',1,'2014-02-10 06:08:52','0.00'),(238,4,'Onion','0.00',1,'2014-02-10 06:08:52','0.00'),(239,4,'Nuts','0.00',1,'2014-02-10 06:08:52','0.00'),(240,4,'Ghee','0.00',1,'2014-02-10 06:08:52','0.00'),(241,4,'Saag','0.50',1,'2014-02-21 09:14:12','0.00'),(242,4,'Keema','0.50',1,'2014-02-21 09:10:21','0.00'),(243,4,'Cheese','0.00',1,'2014-02-11 09:55:19','0.00'),(244,4,'Bombay Aloo','0.50',1,'2014-02-21 09:18:50','0.00'),(245,4,'Tan Kg Prawns','1.50',1,'2014-02-21 09:19:51','0.00'),(246,4,'Paneer','0.50',1,'2014-02-21 10:34:55','0.00'),(247,4,'Capsicum','0.00',1,'2014-03-02 04:37:05','0.00'),(248,4,'Keema Nan','0.50',1,'2014-03-02 04:37:41','0.50'),(249,4,'Cheese Nan','0.50',1,'2014-03-02 04:38:11','0.50'),(250,4,'K & G Naan','0.50',1,'2014-03-02 04:39:00','0.50'),(251,4,'Cheese & G Naan','0.50',1,'2014-03-02 04:39:48','0.50'),(252,4,'CCC Naan','0.50',1,'2014-03-02 04:40:18','0.50'),(253,4,'fgfgfgfgfggggg','1.50',1,'2014-03-12 06:32:10','1.00');
/*!40000 ALTER TABLE `res_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_invoice`
--

DROP TABLE IF EXISTS `res_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_invoice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `payment_method_id` smallint(6) DEFAULT NULL,
  `invoice_date` datetime NOT NULL,
  `bill_amount` decimal(10,2) NOT NULL,
  `delivery_charge` decimal(10,2) DEFAULT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B22643AA8D9F6D38` (`order_id`),
  KEY `IDX_B22643AAFF8A180B` (`last_updated_by`),
  KEY `IDX_B22643AA5AA1164F` (`payment_method_id`),
  CONSTRAINT `FK_B22643AA5AA1164F` FOREIGN KEY (`payment_method_id`) REFERENCES `res_payment_method` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_B22643AA8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `res_order` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_B22643AAFF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_invoice`
--

LOCK TABLES `res_invoice` WRITE;
/*!40000 ALTER TABLE `res_invoice` DISABLE KEYS */;
INSERT INTO `res_invoice` VALUES (1,1,17,NULL,'2014-03-28 13:43:41','50.00',NULL,NULL,'49.60',NULL,1,'2014-03-28 13:43:41');
/*!40000 ALTER TABLE `res_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_log`
--

DROP TABLE IF EXISTS `res_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `log_type_id` smallint(6) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E3C0B4A9FD81B404` (`log_type_id`),
  KEY `IDX_E3C0B4A9FF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_E3C0B4A9FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_E3C0B4A9FD81B404` FOREIGN KEY (`log_type_id`) REFERENCES `res_log_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_log`
--

LOCK TABLES `res_log` WRITE;
/*!40000 ALTER TABLE `res_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_log_type`
--

DROP TABLE IF EXISTS `res_log_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_log_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `log_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_log_type`
--

LOCK TABLES `res_log_type` WRITE;
/*!40000 ALTER TABLE `res_log_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_log_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_message`
--

DROP TABLE IF EXISTS `res_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `sender` smallint(6) DEFAULT NULL,
  `receiver` smallint(6) DEFAULT NULL,
  `send_date` datetime NOT NULL,
  `message` varchar(1000) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_94FE6491727ACA70` (`parent_id`),
  KEY `IDX_94FE64915F004ACF` (`sender`),
  KEY `IDX_94FE64913DB88C96` (`receiver`),
  CONSTRAINT `FK_94FE64913DB88C96` FOREIGN KEY (`receiver`) REFERENCES `res_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_94FE64915F004ACF` FOREIGN KEY (`sender`) REFERENCES `res_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_94FE6491727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `res_message` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_message`
--

LOCK TABLES `res_message` WRITE;
/*!40000 ALTER TABLE `res_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_message_status`
--

DROP TABLE IF EXISTS `res_message_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_message_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message_id` bigint(20) DEFAULT NULL,
  `reader` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9DCF2A94537A1329` (`message_id`),
  KEY `IDX_9DCF2A94CC3F893C` (`reader`),
  CONSTRAINT `FK_9DCF2A94537A1329` FOREIGN KEY (`message_id`) REFERENCES `res_message` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_9DCF2A94CC3F893C` FOREIGN KEY (`reader`) REFERENCES `res_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_message_status`
--

LOCK TABLES `res_message_status` WRITE;
/*!40000 ALTER TABLE `res_message_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_message_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_modem`
--

DROP TABLE IF EXISTS `res_modem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_modem` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `modem_name` varchar(45) DEFAULT NULL,
  `modem_device_id` varchar(200) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  `query_timer` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E6BF94E4FF8A180B` (`last_updated_by`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_modem`
--

LOCK TABLES `res_modem` WRITE;
/*!40000 ALTER TABLE `res_modem` DISABLE KEYS */;
INSERT INTO `res_modem` VALUES (1,2,'HUAWEI Mobile Connect - Modem','Modem0','127.0.0.1',1,'2014-03-04 22:46:25',NULL),(2,2,'USB Data Fax Voice Modem','USB\\VID_0572&PID_1329\\24680246','127.0.0.1',1,'2014-03-04 18:46:16',NULL);
/*!40000 ALTER TABLE `res_modem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_number_identifier`
--

DROP TABLE IF EXISTS `res_number_identifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_number_identifier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `starting_digit` varchar(45) DEFAULT NULL,
  `is_mobile_number` tinyint(1) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `last_update` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D078389AFF8A180B` (`last_updated_by`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_number_identifier`
--

LOCK TABLES `res_number_identifier` WRITE;
/*!40000 ALTER TABLE `res_number_identifier` DISABLE KEYS */;
INSERT INTO `res_number_identifier` VALUES (1,'102020200200',1,2,'2014-03-28 16:51:18',1),(2,'07',1,2,'2014-03-29 01:48:43',1);
/*!40000 ALTER TABLE `res_number_identifier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_order`
--

DROP TABLE IF EXISTS `res_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `table_id` smallint(6) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `order_type_id` smallint(6) DEFAULT NULL,
  `order_date` datetime NOT NULL,
  `is_converted_to_take_away` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_part_payment` tinyint(1) NOT NULL,
  `is_write_off` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `service_charge` decimal(10,2) NOT NULL,
  `gratuity` decimal(10,2) NOT NULL,
  `gratuity_settings` decimal(10,2) NOT NULL,
  `gratuity_is_flat` tinyint(1) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `is_pre_order` tinyint(1) NOT NULL,
  `is_print_que` tinyint(1) NOT NULL,
  `que_sequence_number` smallint(6) NOT NULL,
  `order_child_ids` varchar(5000) DEFAULT NULL,
  `status_id` smallint(6) DEFAULT NULL,
  `is_coming_from_web` tinyint(1) NOT NULL,
  `is_coming_from_local_web` tinyint(1) NOT NULL,
  `local_web_action_id` smallint(6) DEFAULT NULL,
  `duplicate_order_child_ids` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B3044533ECFF285C` (`table_id`),
  KEY `IDX_B30445339395C3F3` (`customer_id`),
  KEY `IDX_B3044533FF8A180B` (`last_updated_by`),
  KEY `IDX_B3044533333625D8` (`order_type_id`),
  KEY `IDX_B30445336BF700BD` (`status_id`),
  KEY `IDX_B3044533CD11DB9F` (`local_web_action_id`),
  CONSTRAINT `FK_B3044533333625D8` FOREIGN KEY (`order_type_id`) REFERENCES `res_order_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_B30445336BF700BD` FOREIGN KEY (`status_id`) REFERENCES `res_order_status` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_B30445339395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `res_customer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_B3044533CD11DB9F` FOREIGN KEY (`local_web_action_id`) REFERENCES `res_order_action_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_B3044533ECFF285C` FOREIGN KEY (`table_id`) REFERENCES `res_table` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_B3044533FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_order`
--

LOCK TABLES `res_order` WRITE;
/*!40000 ALTER TABLE `res_order` DISABLE KEYS */;
INSERT INTO `res_order` VALUES (1,56,15,NULL,1,'2014-03-28 07:59:56',0,0,0,0,'2014-03-28 13:41:11','8.27','0.00','0.00','0.00',0,'49.60','0.00','49.60',0,0,0,NULL,NULL,0,1,NULL,NULL),(2,NULL,47,2,3,'2014-03-28 22:16:44',0,0,0,0,'2014-03-28 22:16:44','0.00','0.00','0.00','0.00',0,'0.00','0.00','0.00',0,0,0,NULL,NULL,0,0,NULL,NULL),(3,28,NULL,17,1,'2014-03-29 03:40:55',0,1,0,0,'2014-03-29 03:40:55','0.00','0.00','0.00','0.00',1,'0.00','0.00','0.00',0,0,0,NULL,NULL,0,1,NULL,NULL);
/*!40000 ALTER TABLE `res_order` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`nibsusers`@`localhost`*/ /*!50003 TRIGGER before_update_order BEFORE UPDATE ON res_order
FOR EACH ROW
BEGIN
   IF (NEW.gratuity_is_flat=1) THEN
      SET NEW.gratuity = NEW.gratuity_settings;
   ELSE
      SET NEW.gratuity = (NEW.sub_total*NEW.gratuity_settings)/100;
   END IF;

   SET NEW.total = NEW.sub_total + NEW.gratuity;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `res_order_action_type`
--

DROP TABLE IF EXISTS `res_order_action_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_order_action_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `action_type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_order_action_type`
--

LOCK TABLES `res_order_action_type` WRITE;
/*!40000 ALTER TABLE `res_order_action_type` DISABLE KEYS */;
INSERT INTO `res_order_action_type` VALUES (1,'SendOrder'),(2,'SendDuplicateOrder'),(3,'SendOrderUpdate'),(4,'SendDuplicateOrderUpdate'),(5,'PrintReceipt');
/*!40000 ALTER TABLE `res_order_action_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_order_child`
--

DROP TABLE IF EXISTS `res_order_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_order_child` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantity_type_id` smallint(6) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` smallint(6) DEFAULT NULL,
  `status_id` smallint(6) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `quantity` smallint(6) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  `is_print_que` tinyint(1) NOT NULL,
  `que_sequence_number` smallint(6) NOT NULL,
  `has_send_to_kitchen` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F20CCD9136F84596` (`quantity_type_id`),
  KEY `IDX_F20CCD918D9F6D38` (`order_id`),
  KEY `IDX_F20CCD914584665A` (`product_id`),
  KEY `IDX_F20CCD916BF700BD` (`status_id`),
  KEY `IDX_F20CCD91FF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_F20CCD9136F84596` FOREIGN KEY (`quantity_type_id`) REFERENCES `res_quantity_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_F20CCD914584665A` FOREIGN KEY (`product_id`) REFERENCES `res_product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_F20CCD916BF700BD` FOREIGN KEY (`status_id`) REFERENCES `res_order_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_F20CCD918D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `res_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_F20CCD91FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_order_child`
--

LOCK TABLES `res_order_child` WRITE;
/*!40000 ALTER TABLE `res_order_child` DISABLE KEYS */;
INSERT INTO `res_order_child` VALUES (1,NULL,1,17,6,17,1,'4.25',1,'2014-03-28 13:39:00',0,0,1),(2,NULL,1,18,6,17,1,'4.25',1,'2014-03-28 13:39:00',0,0,1),(3,NULL,1,574,7,17,2,'11.90',1,'2014-03-28 13:39:00',0,0,1),(4,NULL,1,32,7,17,2,'6.90',1,'2014-03-28 13:39:00',0,0,1),(5,NULL,1,278,7,17,2,'21.90',1,'2014-03-28 13:41:11',0,0,1),(6,NULL,1,399,7,17,2,'0.40',1,'2014-03-28 13:41:11',0,0,1),(7,NULL,NULL,402,7,NULL,4,'2.00',1,'0001-01-01 00:00:00',0,0,1);
/*!40000 ALTER TABLE `res_order_child` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`nibsusers`@`localhost`*/ /*!50003 TRIGGER after_insert_child_order AFTER INSERT ON res_order_child
FOR EACH ROW
BEGIN
   DECLARE tax float(10,2);
   DECLARE inclusive_tax float(10,2);
   DECLARE exclusive_tax float(10,2);
   DECLARE tax_per tinyint(1);
   DECLARE amount_total float(10,2);
   DECLARE inclusive_amount_total float(10,2);
   DECLARE exclusive_amount_total float(10,2);
   DECLARE sc float(10,2);
   DECLARE sc_per tinyint(1);

   SET tax=(SELECT value FROM res_settings WHERE settings_name='tax');
   SET inclusive_tax = 0;
   SET exclusive_tax = 0;
   SET tax_per=(SELECT is_flat FROM res_settings WHERE settings_name='tax');

   SET sc=(SELECT value FROM res_settings WHERE settings_name='service_charge');
   SET sc_per=(SELECT is_flat FROM res_settings WHERE settings_name='service_charge');

   SET amount_total=(SELECT SUM(amount) FROM res_order_child WHERE is_active=1 AND order_id=NEW.order_id);

   SET inclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=NEW.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=NEW.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=NEW.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);

   SET exclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=NEW.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=NEW.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=NEW.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(pm.price),0) FROM res_product_misc pm LEFT JOIN res_order_child oc ON oc.id=pm.order_child_id WHERE oc.is_active=1 AND oc.order_id=NEW.order_id);

   IF (tax_per=0) THEN
      SET inclusive_tax = inclusive_amount_total - ((inclusive_amount_total*100)/(100+tax));
      SET exclusive_tax = (tax*exclusive_amount_total)/100;
      SET tax = inclusive_tax + exclusive_tax;
   END IF;

   SET amount_total=amount_total+exclusive_tax;

   IF (sc_per=0) THEN
      SET sc = (sc*amount_total)/100;
   END IF;

   SET amount_total = amount_total + sc;

   UPDATE res_order SET tax = tax, service_charge = sc, sub_total = amount_total WHERE id=NEW.order_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`nibsusers`@`localhost`*/ /*!50003 TRIGGER after_update_child_order AFTER UPDATE ON res_order_child
FOR EACH ROW
BEGIN
   DECLARE tax float(10,2);
   DECLARE inclusive_tax float(10,2);
   DECLARE exclusive_tax float(10,2);
   DECLARE tax_per tinyint(1);
   DECLARE amount_total float(10,2);
   DECLARE inclusive_amount_total float(10,2);
   DECLARE exclusive_amount_total float(10,2);
   DECLARE sc float(10,2);
   DECLARE sc_per tinyint(1);

   SET tax=(SELECT value FROM res_settings WHERE settings_name='tax');
   SET inclusive_tax = 0;
   SET exclusive_tax = 0;
   SET tax_per=(SELECT is_flat FROM res_settings WHERE settings_name='tax');

   SET sc=(SELECT value FROM res_settings WHERE settings_name='service_charge');
   SET sc_per=(SELECT is_flat FROM res_settings WHERE settings_name='service_charge');

   SET amount_total=(SELECT SUM(amount) FROM res_order_child WHERE is_active=1 AND order_id=OLD.order_id);

   SET inclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);

   SET exclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(pm.price),0) FROM res_product_misc pm LEFT JOIN res_order_child oc ON oc.id=pm.order_child_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id);

   IF (tax_per=0) THEN
      SET inclusive_tax = inclusive_amount_total - ((inclusive_amount_total*100)/(100+tax));
      SET exclusive_tax = (tax*exclusive_amount_total)/100;
      SET tax = inclusive_tax + exclusive_tax;
   END IF;

   SET amount_total=amount_total+exclusive_tax;

   IF (sc_per=0) THEN
      SET sc = (sc*amount_total)/100;
   END IF;

   SET amount_total = amount_total + sc;

   UPDATE res_order SET tax = tax, service_charge = sc, sub_total = amount_total WHERE id=OLD.order_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`nibsusers`@`localhost`*/ /*!50003 TRIGGER after_delete_child_order AFTER DELETE ON res_order_child
FOR EACH ROW
BEGIN
   DECLARE tax float(10,2);
   DECLARE inclusive_tax float(10,2);
   DECLARE exclusive_tax float(10,2);
   DECLARE tax_per tinyint(1);
   DECLARE amount_total float(10,2);
   DECLARE inclusive_amount_total float(10,2);
   DECLARE exclusive_amount_total float(10,2);
   DECLARE sc float(10,2);
   DECLARE sc_per tinyint(1);

   SET tax=(SELECT value FROM res_settings WHERE settings_name='tax');
   SET inclusive_tax = 0;
   SET exclusive_tax = 0;
   SET tax_per=(SELECT is_flat FROM res_settings WHERE settings_name='tax');

   SET sc=(SELECT value FROM res_settings WHERE settings_name='service_charge');
   SET sc_per=(SELECT is_flat FROM res_settings WHERE settings_name='service_charge');

   SET amount_total=(SELECT SUM(amount) FROM res_order_child WHERE is_active=1 AND order_id=OLD.order_id);

   SET inclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=1 AND p.is_taxable=1);

   SET exclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(pm.price),0) FROM res_product_misc pm LEFT JOIN res_order_child oc ON oc.id=pm.order_child_id WHERE oc.is_active=1 AND oc.order_id=OLD.order_id);

   IF (tax_per=0) THEN
      SET inclusive_tax = inclusive_amount_total - ((inclusive_amount_total*100)/(100+tax));
      SET exclusive_tax = (tax*exclusive_amount_total)/100;
      SET tax = inclusive_tax + exclusive_tax;
   END IF;

   SET amount_total=amount_total+exclusive_tax;

   IF (sc_per=0) THEN
      SET sc = (sc*amount_total)/100;
   END IF;

   SET amount_total = amount_total + sc;

   UPDATE res_order SET tax = tax, service_charge = sc, sub_total = amount_total WHERE id=OLD.order_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `res_order_status`
--

DROP TABLE IF EXISTS `res_order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_order_status` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `status` varchar(150) NOT NULL,
  `top_color` varchar(9) NOT NULL,
  `bottom_color` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_order_status`
--

LOCK TABLES `res_order_status` WRITE;
/*!40000 ALTER TABLE `res_order_status` DISABLE KEYS */;
INSERT INTO `res_order_status` VALUES (6,'Taking Order','#a1a1a1','#818181'),(7,'Order Taken','#ffb239','#f69600'),(8,'Order Served','#d44949','#c11313'),(9,'Order Paid','#49d470','#13c144'),(10,'Order Completed','#000000','#000000'),(11,'Delivery on the way','#000000','#000000');
/*!40000 ALTER TABLE `res_order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_order_type`
--

DROP TABLE IF EXISTS `res_order_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_order_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `order_type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_order_type`
--

LOCK TABLES `res_order_type` WRITE;
/*!40000 ALTER TABLE `res_order_type` DISABLE KEYS */;
INSERT INTO `res_order_type` VALUES (1,'Dine In'),(2,'Collection'),(3,'Delivery'),(4,'Bar'),(5,'Waiting'),(6,'Web Order');
/*!40000 ALTER TABLE `res_order_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_payment`
--

DROP TABLE IF EXISTS `res_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `payment_method_id` smallint(6) DEFAULT NULL,
  `voucher_id` bigint(20) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `cheque_number` varchar(15) DEFAULT NULL,
  `cc_number` varchar(16) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4F6BD0E38D9F6D38` (`order_id`),
  KEY `IDX_4F6BD0E35AA1164F` (`payment_method_id`),
  KEY `IDX_4F6BD0E328AA1B6F` (`voucher_id`),
  KEY `IDX_4F6BD0E3FF8A180B` (`last_updated_by`),
  KEY `FK_4F6BD0E3FF8A180C_idx` (`invoice_id`),
  CONSTRAINT `FK_4F6BD0E32989F1FD` FOREIGN KEY (`invoice_id`) REFERENCES `res_invoice` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_4F6BD0E328AA1B6F` FOREIGN KEY (`voucher_id`) REFERENCES `res_voucher` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_4F6BD0E35AA1164F` FOREIGN KEY (`payment_method_id`) REFERENCES `res_payment_method` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_4F6BD0E38D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `res_order` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_4F6BD0E3FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_payment`
--

LOCK TABLES `res_payment` WRITE;
/*!40000 ALTER TABLE `res_payment` DISABLE KEYS */;
INSERT INTO `res_payment` VALUES (1,1,1,1,NULL,NULL,'50.00',NULL,NULL,1,'2014-03-28 13:43:42');
/*!40000 ALTER TABLE `res_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_payment_method`
--

DROP TABLE IF EXISTS `res_payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_payment_method` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `payment_method_name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_payment_method`
--

LOCK TABLES `res_payment_method` WRITE;
/*!40000 ALTER TABLE `res_payment_method` DISABLE KEYS */;
INSERT INTO `res_payment_method` VALUES (1,'Cash'),(2,'Credit Card'),(3,'Cheque'),(4,'Account'),(5,'Voucher'),(6,'Loyalty Point'),(7,'Complimentry'),(8,'Refund');
/*!40000 ALTER TABLE `res_payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_predefined_message`
--

DROP TABLE IF EXISTS `res_predefined_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_predefined_message` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `message_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_predefined_message`
--

LOCK TABLES `res_predefined_message` WRITE;
/*!40000 ALTER TABLE `res_predefined_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_predefined_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_prep_location`
--

DROP TABLE IF EXISTS `res_prep_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_prep_location` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `user_id` smallint(6) DEFAULT NULL,
  `printer_id` smallint(6) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `prep_location_name` varchar(150) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6FD9342EA76ED395` (`user_id`),
  KEY `IDX_6FD9342E46EC494A` (`printer_id`),
  KEY `IDX_6FD9342EFF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_6FD9342E46EC494A` FOREIGN KEY (`printer_id`) REFERENCES `res_printer` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_6FD9342EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_6FD9342EFF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_prep_location`
--

LOCK TABLES `res_prep_location` WRITE;
/*!40000 ALTER TABLE `res_prep_location` DISABLE KEYS */;
INSERT INTO `res_prep_location` VALUES (1,3,3,4,'Kitchen Chef',1,'2014-01-08 08:05:52'),(2,4,4,4,'Desert',1,'2014-01-08 08:06:04'),(3,3,3,4,'Bar',1,'2014-01-08 08:06:13'),(5,NULL,4,4,'Bar4',0,'2014-03-11 10:59:46');
/*!40000 ALTER TABLE `res_prep_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_print_block`
--

DROP TABLE IF EXISTS `res_print_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_print_block` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `block_name` varchar(150) NOT NULL,
  `sequence_number` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_print_block`
--

LOCK TABLES `res_print_block` WRITE;
/*!40000 ALTER TABLE `res_print_block` DISABLE KEYS */;
INSERT INTO `res_print_block` VALUES (1,'Block A',1),(2,'Block C',3),(3,'Block B',2),(4,'Block D',4);
/*!40000 ALTER TABLE `res_print_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_print_block_category`
--

DROP TABLE IF EXISTS `res_print_block_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_print_block_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `print_block_id` smallint(6) DEFAULT NULL,
  `category_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D2AA620688A31ECA` (`print_block_id`),
  KEY `IDX_D2AA620612469DE2` (`category_id`),
  CONSTRAINT `FK_D2AA620612469DE2` FOREIGN KEY (`category_id`) REFERENCES `res_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_D2AA620688A31ECA` FOREIGN KEY (`print_block_id`) REFERENCES `res_print_block` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_print_block_category`
--

LOCK TABLES `res_print_block_category` WRITE;
/*!40000 ALTER TABLE `res_print_block_category` DISABLE KEYS */;
INSERT INTO `res_print_block_category` VALUES (2,1,39),(3,3,69),(4,3,70),(5,3,71),(6,2,91),(7,4,105),(8,3,86),(9,3,87),(10,3,88),(11,3,112),(12,2,97),(13,2,90),(14,2,111),(15,3,108),(16,4,106);
/*!40000 ALTER TABLE `res_print_block_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_printer`
--

DROP TABLE IF EXISTS `res_printer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_printer` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `printer_name` varchar(100) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AF0F2D03FF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_AF0F2D03FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_printer`
--

LOCK TABLES `res_printer` WRITE;
/*!40000 ALTER TABLE `res_printer` DISABLE KEYS */;
INSERT INTO `res_printer` VALUES (1,4,'POS80','10.0.0.0',1,'2014-01-12 07:26:47'),(2,1,'Microsoft XPS Document Writer','127.0.0.1',1,'2014-03-07 14:06:21'),(3,2,'POS80 Kitchen','127.0.0.1',1,'2014-03-10 20:46:16'),(4,2,'Pos80_preplocation','127.0.0.1',1,'2014-03-10 20:46:21'),(5,1,'Fax','127.0.0.1',1,'2014-03-20 13:04:07');
/*!40000 ALTER TABLE `res_printer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_product`
--

DROP TABLE IF EXISTS `res_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_product` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `category_id` smallint(6) DEFAULT NULL,
  `prep_location_id` smallint(6) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `product_name` varchar(150) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `takeaway_price` decimal(10,2) DEFAULT NULL,
  `web_order_price` decimal(10,2) DEFAULT NULL,
  `delivery_price` decimal(10,2) DEFAULT NULL,
  `is_favourite` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  `is_tax_inclusive` tinyint(1) NOT NULL,
  `sequence_number` smallint(6) NOT NULL,
  `is_taxable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F109504312469DE2` (`category_id`),
  KEY `IDX_F1095043CA85BAE` (`prep_location_id`),
  KEY `IDX_F1095043FF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_F109504312469DE2` FOREIGN KEY (`category_id`) REFERENCES `res_category` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_F1095043CA85BAE` FOREIGN KEY (`prep_location_id`) REFERENCES `res_prep_location` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_F1095043FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=609 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_product`
--

LOCK TABLES `res_product` WRITE;
/*!40000 ALTER TABLE `res_product` DISABLE KEYS */;
INSERT INTO `res_product` VALUES (1,47,3,4,'Amritsa Chicken','Chicken marinated in a garam masala and panch puran mix, tandoori cooked, spicy to taste','4.45','4.75','5.00','4.50',1,1,'2014-03-14 06:16:02',1,0,1),(2,47,1,4,'Tava Kebab','Fillet of chicken breast lightly coated in Tandoori','4.45','4.45','4.45','4.45',1,1,'2014-01-08 08:43:52',1,0,1),(3,59,1,4,'Pepper Fried Tuna','Pepper Fried Tuna','4.95','4.95','4.95','4.95',1,1,'2014-01-08 09:53:03',1,0,1),(4,47,1,4,'Nawabi Kebab','Home style meat patty, lightly spiced, onions and loaded with fresh herbs, served with a fried egg','4.50','3.50','3.40','3.55',1,1,'2014-03-12 04:23:22',1,0,1),(5,47,1,4,'Spicy Chicken','Strips of Tandoori chicken breast flavoured with chilli pickle, bell peppers, garlic, onions & fresh herbs','4.60','4.20','4.15','4.50',1,1,'2014-03-12 05:07:51',1,0,1),(6,47,1,4,'Chilli Fried Kolja','Sweet and spicy pan fried chicken liver, marinated in fresh herbs and chilli based sauce','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:24:43',1,0,1),(7,47,1,4,'Malai Kufta','Spicy meat balls in a sweet and spicy cheese sauce','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:24:43',1,0,1),(8,47,1,4,'Fara Sira Chicken','Shredded chicken stir-fried and wrapped in laacha paratha','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:24:43',1,0,1),(9,44,1,4,'Pepper Fried Tuna','Tuna flakes, pan fried with garlic, ginger, fresh corriander and chilli seeds','4.95','4.95','4.95','4.95',1,1,'2014-01-14 10:24:44',1,0,1),(10,44,1,4,'Butterfly Sardines','Masala coated grilled sardines','4.95','4.95','4.95','4.95',1,1,'2014-01-14 10:24:44',1,0,1),(11,44,1,4,'Baked Spice Monk Fish','Masala coated grilled sardines pepper and onion sauce','4.95','4.95','4.95','4.95',1,1,'2014-01-14 10:24:44',1,0,1),(12,44,1,4,'Salmon Birran','Salmon steak, lightly marinated in a selection of herbs and sauces, pan fried','4.95','4.95','4.95','4.95',1,1,'2014-01-14 10:24:44',1,0,1),(13,44,1,4,'Garlic King Prawn','Jumbo king prawns, marinated in garlic and herbs and flash fried, served with caramelised onions','4.95','4.95','4.95','4.95',1,1,'2014-01-14 10:24:45',1,0,1),(14,47,1,4,'Chatt Puree (Chicken)','Chicken marinated in a garam masala and panch puran mix, tandoori cooked, spicy to taste','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:33:11',1,0,1),(15,47,1,4,'Tikka Pakora (Chicken)','Chicken cooked in batter and deep fried','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:33:11',1,0,1),(16,47,1,4,'Meat Somosa','mince meat in pastry and deep fried','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:33:11',1,0,1),(17,47,1,4,'Tandoori Chicken (Starter)','Chicken marinated in tandoori spices and cooked in tandoori oven','4.25','4.25','4.25','4.25',1,1,'2014-01-30 12:28:40',1,0,1),(18,47,1,4,'Chicken Tikka (starter)','Chicken marinated in tikka spiced and cooked in tandoori oven','4.25','4.25','4.25','4.25',1,1,'2014-01-30 12:25:51',1,0,1),(19,47,1,4,'Sheek Kebab','mince meet kebab cooked in tandoori oven','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:33:12',1,0,1),(20,47,1,4,'Shami Kebab','spicy mince patte shallow fried','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:33:12',1,0,1),(21,47,1,4,'Nargis Kebab','boilled egg covered in mince meat and cooked in tandoori oven','4.25','4.25','4.25','4.25',1,1,'2014-01-14 10:33:12',1,0,1),(22,44,1,4,'Bhuna Prawns on Puree','prawns cooked in spicy bhoona sauce','4.95','4.95','4.95','4.95',1,1,'2014-01-14 10:33:21',1,0,1),(23,44,1,4,'Prawn Cocktail','prawns in cocktail sauce','4.95','4.95','4.95','4.95',1,1,'2014-01-14 10:33:22',1,0,1),(24,44,1,4,'Tandoori King Prawns (Starter)','Jumbo King Prawns marinated and cooked in tandoori oven','4.95','4.95','4.95','4.95',1,1,'2014-01-30 12:31:03',1,0,1),(25,51,1,4,'Paneer Relish','Thick strips of paneer stir-fried with onions, peppers in a spicy sauce','4.25','4.25','4.25','4.25',1,1,'2014-01-14 11:03:39',1,0,1),(26,51,1,4,'Vegetable Somosa','mix vegetables in thin pastry deep fried','2.95','2.95','2.95','2.95',1,1,'2014-01-14 11:03:40',1,0,1),(27,51,1,4,'Onion Bhajee','deep fried onion in batter','2.95','2.95','2.95','2.95',1,1,'2014-01-14 11:03:40',1,0,1),(28,51,1,4,'Garlic Mushrooms','Garlic mushrooms','3.45','3.45','3.45','3.45',1,1,'2014-01-14 11:03:40',1,0,1),(32,51,3,4,'Aloo Chomka','crispy potato with fillings','3.45','3.45','3.45','3.45',1,1,'2014-01-14 11:10:57',1,0,1),(33,52,1,4,'Meat Platter','All Platters are designed for sharing. Platters for 2 persons','8.95','8.95','8.95','8.95',1,1,'2014-01-14 11:16:24',1,0,1),(34,52,1,4,'Vegetable platter','All Platters are designed for sharing. Platters for 2 persons','8.95','8.95','8.95','8.95',1,1,'2014-01-14 11:16:25',1,0,1),(35,52,1,4,'Sea food platter','All Platters are designed for sharing. Platters for 2 persons','10.45','10.45','10.45','10.45',1,1,'2014-01-14 11:16:25',1,0,1),(36,69,1,4,'Tandoori Chicken (Main)','Chicken on the bone, marinated in tandoori spices and cooked on tandoori oven','7.10','7.10','7.10','7.10',1,1,'2014-01-30 12:29:30',1,0,1),(37,69,1,4,'Chicken Tikka (Main)','Chicken off the bone, marinated in Tikka spices and cooked in tandoori oven','7.10','7.10','7.10','7.10',1,1,'2014-01-30 12:26:34',1,0,1),(38,69,1,4,'Tandoori Lamb Chops','Lamb Chops, marinated in tandoori spices and cooked on tandoori oven','9.95','9.95','9.95','9.95',1,1,'2014-01-16 11:50:27',1,0,1),(39,69,1,4,'Tandoori King Prawns (Main)','Jumbo King Prawns marinated in tandoori spices and cooked on tandoori oven','10.95','10.95','10.95','10.95',1,1,'2014-01-30 12:31:27',1,0,1),(40,69,1,4,'Tandoori Mix','Tandoori Chicken, chicken Tikka, Lamb Tikka and Tandoori Lamb Chops & Sheek Kebab, served with plain nan','9.95','9.95','9.95','9.95',1,1,'2014-01-16 11:50:28',1,0,1),(41,69,1,4,'Vegetarian Shasliq (v)','Cubes of paneer and button mushrooms marinated in herbs and yoghurt, then slowly char grilled in the Tandoor with onions, peppers and tomato','7.95','7.95','7.95','7.95',1,1,'2014-01-16 11:50:28',1,0,1),(42,69,1,4,'Chicken Shashliq (Main)','Chicken marinated in herbs and yoghurt, then slowly char grilled in the Tandoor with onions, peppers and tomato','8.45','8.45','8.45','8.45',1,1,'2014-01-30 12:27:22',1,0,1),(43,69,1,4,'Lamb Shashliq','Lamb marinated in herbs and yoghurt, then slowly char grilled in the Tandoor with onions, peppers and tomato','8.95','8.95','8.95','8.95',1,1,'2014-01-16 11:50:28',1,0,1),(44,69,1,4,'Shasliq Special','Chicken Tikka, lamb Tikka, button mushrooms and paneer slowly baked in the Tandoori oven','9.25','9.25','9.25','9.25',1,1,'2014-01-16 11:50:29',1,0,1),(45,70,1,4,'Lamb Biryani','Lamb stir-fried in basmati rice, onions, peppers, dozen freshly pounded exotic spices in pure ghee and served with a medium strength vegetable curry','9.95','9.95','9.95','9.95',1,1,'2014-01-16 11:50:29',1,0,1),(46,70,1,4,'Chicken Tikka Biryani','Chicken Tikka stir-fried in basmati rice, onions, peppers, dozen freshly pounded exotic spices in pure ghee and served with a medium vegetable curry','7.95','7.95','7.95','7.95',1,1,'2014-01-16 11:50:29',1,0,1),(47,70,1,4,'Chicken Biryani','Chicken  stir-fried in basmati rice, onions, peppers, dozen freshly pounded exotic spices in pure ghee and served with a medium strength vegetable curry','7.95','7.95','7.95','7.95',1,1,'2014-01-16 11:50:29',1,0,1),(48,70,1,4,'Tandoori Chicken Biryani','Tandoori Chicken  stir-fried in basmati rice, onions, peppers, dozen freshly pounded exotic spices in pure ghee and served with a  vegetable curry','7.95','7.95','7.95','7.95',1,1,'2014-01-16 11:50:29',1,0,1),(139,73,1,4,'Chicken Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:45',1,0,1),(140,73,1,4,'Prawn Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:47',1,0,1),(141,73,1,4,'Mix Veg Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:47',1,0,1),(142,73,1,4,'Lamb Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:47',1,0,1),(143,73,1,4,'King Prawn Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:48',1,0,1),(144,73,1,4,'Mix Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:48',1,0,1),(145,73,1,4,'Chicken Tikka Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:48',1,0,1),(146,73,1,4,'Lamb Tikka Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','7.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:48',1,0,1),(147,73,1,4,'Sag Aloo Paneer Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:48',1,0,1),(148,73,1,4,'Chicken & Mushroom Kurma','Very mild and sweet dish, consisting of cream, coconut and sultanas.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(149,73,1,4,'Chicken Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(150,74,1,4,'Prawn Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(151,74,1,4,'Mix Veg Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(152,74,1,4,'Lamb Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(153,74,1,4,'King Prawn Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(154,74,1,4,'Mix Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(155,74,1,4,'Chicken Tikka Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(156,74,1,4,'Lamb Tikka Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','7.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(157,74,1,4,'Sag Aloo Paneer Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:49',1,0,1),(158,74,1,4,'Chicken & Mushroom Bhuna','With onions and various tomatoes, plenty of green herbs and seven freshly grounded spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:50',1,0,1),(159,75,1,4,'Chicken Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:50',1,0,1),(160,75,1,4,'Prawn Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:50',1,0,1),(161,75,1,4,'Mix Veg Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:50',1,0,1),(162,75,1,4,'Lamb Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:50',1,0,1),(163,75,1,4,'King Prawn Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:50',1,0,1),(164,75,1,4,'Mix Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:50',1,0,1),(165,75,1,4,'Chicken Tikka Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:50',1,0,1),(166,75,1,4,'Lamb Tikka Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','7.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(167,75,1,4,'Sag Aloo Paneer Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(168,75,1,4,'Chicken & Mushroom Rogan Josh','Cooked in a rich gravy, concentrated broth of tomatoes and select spices','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(169,76,1,4,'Chicken  Pathia','Tomato based, fairly spicy hot, sweet and sour','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(170,76,1,4,'Prawn Pathia','Tomato based, fairly spicy hot, sweet and sour','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(171,76,1,4,'Mix Veg Pathia','Tomato based, fairly spicy hot, sweet and sour','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(172,76,1,4,'Lamb Pathia','Tomato based, fairly spicy hot, sweet and sour','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(173,76,1,4,'King Prawn Pathia','Tomato based, fairly spicy hot, sweet and sour','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(174,76,1,4,'Mix Pathia','Tomato based, fairly spicy hot, sweet and sour','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(175,76,1,4,'Chicken Tikka Pathia','Tomato based, fairly spicy hot, sweet and sour','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(176,76,1,4,'Lamb Tikka Pathia','Tomato based, fairly spicy hot, sweet and sour','7.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(177,76,1,4,'Sag Aloo Paneer Pathia','Tomato based, fairly spicy hot, sweet and sour','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(178,76,1,4,'Chicken & Mushroom Pathia','Tomato based, fairly spicy hot, sweet and sour','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(179,72,1,4,'Chicken  Balti','herbs and spices in medium bell pepper and onion sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(180,72,1,4,'PrawnBalti','herbs and spices in medium bell pepper and onion sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:51',1,0,1),(181,72,1,4,'Mix Veg Balti','herbs and spices in medium bell pepper and onion sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(182,72,1,4,'Lamb Balti','herbs and spices in medium bell pepper and onion sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(183,72,1,4,'King Prawn Balti','herbs and spices in medium bell pepper and onion sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(184,72,1,4,'Mix Balti','herbs and spices in medium bell pepper and onion sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(185,72,1,4,'Chicken Tikka Balti','herbs and spices in medium bell pepper and onion sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(186,72,1,4,'Lamb Tikka Balti','herbs and spices in medium bell pepper and onion sauce.','7.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(187,72,1,4,'Sag Aloo Paneer Balti','herbs and spices in medium bell pepper and onion sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(188,72,1,4,'Chicken & Mushroom Balti','herbs and spices in medium bell pepper and onion sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(189,77,1,4,'Chicken  Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(190,77,1,4,'Prawn Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(191,77,1,4,'Mix Veg Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(192,77,1,4,'Lamb Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:52',1,0,1),(193,77,1,4,'King Prawn Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(194,77,1,4,'Mix Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(195,77,1,4,'Chicken Tikka Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(196,77,1,4,'Lamb Tikka Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','7.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(197,77,1,4,'Sag Aloo Paneer Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(198,77,1,4,'Chicken & Mushroom Sagwala','Briskly fried preparation of freshly chopped spinach, garlic, onions and spices.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(199,78,1,4,'Chicken  Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(200,78,1,4,'Prawn Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(201,78,1,4,'Mix Veg Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(202,78,1,4,'Lamb Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(203,78,1,4,'King Prawn Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(204,78,1,4,'Mix Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(205,78,1,4,'Chicken Tikka Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(206,78,1,4,'Lamb Tikka Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','7.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:53',1,0,1),(207,78,1,4,'Sag Aloo Paneer Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(208,78,1,4,'Chicken & Mushroom Dhansak','Pan fried with pineapple and green herbs and garlic then cooked in a spicy, sweet and sour lentil sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(209,80,1,4,'Chicken  Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(210,80,1,4,'Prawn Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(211,80,1,4,'Mix Veg Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(212,80,1,4,'Lamb Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(213,80,1,4,'King Prawn Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(214,80,1,4,'Mix Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(215,80,1,4,'Chicken Tikka Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(216,80,1,4,'Lamb Tikka Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.95','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(217,80,1,4,'Sag Aloo Paneer Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(218,80,1,4,'Chicken & Mushroom Curry','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-17 12:15:54',1,0,1),(219,79,1,4,'Chicken  Jalfrezi','herbs and spices with green chillies and coriander.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:11',1,0,1),(220,79,1,4,'Prawn Jalfrezi','herbs and spices with green chillies and coriander.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:16',1,0,1),(221,79,1,4,'Mix Veg Jalfrezi','herbs and spices with green chillies and coriander.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:19',1,0,1),(222,79,1,4,'Lamb Jalfrezi','herbs and spices with green chillies and coriander.','7.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:19',1,0,1),(223,79,1,4,'King Prawn Jalfrezi','herbs and spices with green chillies and coriander.','9.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:20',1,0,1),(224,79,1,4,'Mix Jalfrezi','herbs and spices with green chillies and coriander.','9.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:20',1,0,1),(225,79,1,4,'Chicken Tikka Jalfrezi','herbs and spices with green chillies and coriander.','7.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:21',1,0,1),(226,79,1,4,'Lamb Tikka Jalfrezi','herbs and spices with green chillies and coriander.','7.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:21',1,0,1),(227,79,1,4,'Sag Aloo Paneer Jalfrezi','herbs and spices with green chillies and coriander.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:22',1,0,1),(228,79,1,4,'Chicken & Mushroom Jalfrezi','herbs and spices with green chillies and coriander.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:23',1,0,1),(229,80,1,4,'Chicken  Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:24',1,0,1),(230,80,1,4,'Prawn Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:24',1,0,1),(231,80,1,4,'Mix Veg Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:25',1,0,1),(232,80,1,4,'Lamb Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:25',1,0,1),(233,80,1,4,'King Prawn Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:26',1,0,1),(234,80,1,4,'Mix Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:27',1,0,1),(235,80,1,4,'Chicken Tikka Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:27',1,0,1),(236,80,1,4,'Lamb Tikka Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:28',1,0,1),(237,80,1,4,'Sag Aloo Paneer Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:29',1,0,1),(238,80,1,4,'Chicken & Mushroom Madras','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:29',1,0,1),(239,80,1,4,'Chicken  Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:30',1,0,1),(240,80,1,4,'Prawn Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:30',1,0,1),(241,80,1,4,'Mix Veg Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:31',1,0,1),(242,80,1,4,'Lamb Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:32',1,0,1),(243,80,1,4,'King Prawn Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:32',1,0,1),(244,80,1,4,'Mix Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','9.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:33',1,0,1),(245,80,1,4,'Chicken Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:33',1,0,1),(246,80,1,4,'Lamb Tikka Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','7.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:34',1,0,1),(247,80,1,4,'Sag Aloo Paneer Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:35',1,0,1),(248,80,1,4,'Chicken & Mushroom Vindaloo','Most popular dishes, made with a wide range ofAsian spices to give a variety of heat in the sauce.','6.75','7.95','7.95','7.95',1,1,'2014-01-25 12:51:36',1,0,1),(249,86,1,4,'Naga Chicken','prepared with the famous Bangladeshi Scotch Bonnet pepper','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:36',1,0,1),(250,86,1,4,'Naga Lamb','prepared with the famous Bangladeshi Scotch Bonnet ','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:37',1,0,1),(251,86,1,4,'Bazia Beef','Beef chunks with grilled peppers and onions, shatkora (citrus fruit), semi dry madras','8.95','6.50','6.50','6.50',1,1,'2014-01-25 12:51:38',1,0,1),(252,86,1,4,'South Indian Chicken','Cooked in hot chilli garlic sauce and garnished with onion chilli salad, hot and spicy to taste','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:38',1,0,1),(253,86,1,4,'Relish Ghust','Tender chunks of lamb flambÃ©ed in hot mustard','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:38',1,0,1),(254,86,1,4,'Bahari Mach','Pangash (tropical white fish), marinated with Bangladeshi pickles then cooked with spinach','9.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:39',1,0,1),(255,86,1,4,'Sharegama','Chefâ€™s own recipe, off the bone Tandoori chicken mushrooms, sliced garlic and green chillies','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:39',1,0,1),(256,86,1,4,'Bengal Curry','Pan fried garlic mushrooms cooked with chicken breast strips with sautÃ©ed onions, semi dry sauce','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:40',1,0,1),(257,86,1,4,'Methi Tikka Curry','Tikka spiced chicken wrapped in methi leaves and garlic cooked in a dupiaza sauce','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:40',1,0,1),(258,86,1,4,'Mostani Murgh','Spring chicken cooked in the Tandoori oven cooked in garam masala, fresh coriander and lemon sauce garnished with pan fried fresh garlic topped with thin fried potatoes','7.95','6.50','6.50','6.50',1,1,'2014-01-25 12:51:41',1,0,1),(259,86,1,4,'Rezalah Chicken','A classic dish from the North of Bengal,','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:41',1,0,1),(260,86,1,4,'Rezalah Lamb','A classic dish from the North of Bengal,','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:42',1,0,1),(261,86,1,4,'Tandoori Murghi Mosala','Boneless strips of Tandoori chicken, cooked in a medium mince meat sauce with spicy boiled egg','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:43',1,0,1),(262,86,1,4,'Sizzling Steak Fry','Juliennes of sirloin steak pan fried with onion and bell pepper rings, mushrooms and spicy keema with hint of mustard, served with chips','10.95','9.95','9.95','9.95',1,1,'2014-01-25 12:51:43',1,0,1),(263,86,1,4,'Shasliq Bhuna Kadai','Succulent pieces of chicken Tikka, lamb Tikka, tomato, onion and peppers char-grilled and sizzled up into a balti','8.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:43',1,0,1),(264,86,1,4,'Jinga Jool','King prawns fried with fennel seeds','12.95','10.95','10.95','10.95',1,1,'2014-01-25 12:51:44',1,0,1),(265,86,1,4,'Tarka Chicken','Thinly sliced chicken Tikka stir fried with peppers spring onions, baby corn, baby carrots and layered with a garlic Tikka sauce','8.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:44',1,0,1),(266,86,1,4,'Peshwari Chicken','cooked with cinnamon and cardamom in a Alphonso mango sauce, a mild flavoursome dish from the North Punjab','7.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:45',1,0,1),(267,86,1,4,'Peshwari Lamb','cooked with cinnamon and cardamom in a Alphonso mango sauce, a mild flavoursome dish from the North Punjab','7.95','7.95','7.95','7.95',1,1,'2014-01-25 12:51:46',1,0,1),(268,86,1,4,'Shahi Lamb','Tender lamb pieces cooked in a mild sauce','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:46',1,0,1),(269,86,1,4,'Chicken Makhani (n)','Pieces of chicken Tikka prepared in cream sauce,','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:50',1,0,1),(270,86,1,4,'Chicken Tikka Pasanda (n)','Chicken Tikka marinated in spiced yoghurt Mild and Creamy','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:57',1,0,1),(271,86,1,4,'Chicken Tikka Mosala (n)','Mild and Creamy','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:51:59',1,0,1),(272,86,1,4,'Chicken Cheese Kofta Cy (n)','Mild and Creamy','8.95','7.95','7.95','7.95',1,1,'2014-01-25 12:52:00',1,0,1),(273,87,1,4,'Chefs SP of the Day','This is an exclusive dish conjured up by our Head Chef.','10.95','10.95','10.95','10.95',1,1,'2014-01-25 12:52:00',1,0,1),(274,87,1,4,'Lamb Nihari','Steamed lamb shank, coated in freshly ground panch puran cooked with barbequed onions','10.95','10.95','10.95','10.95',1,1,'2014-01-25 12:52:01',1,0,1),(275,87,1,4,'Shabji Bazia','Medley of fresh vegetables; (potatoes, carrots, mushrooms, butternut squash, baby sweetcorn)','8.95','8.95','8.95','8.95',1,1,'2014-01-25 12:52:01',1,0,1),(276,87,1,4,'Salmon Surprise','Salmon fillet dusted with chilli and turmeric powder sautÃ©ed in a garlic butter sauce','10.95','10.95','10.95','10.95',1,1,'2014-01-25 12:52:02',1,0,1),(277,87,1,4,'Kerala Duck Curry','Breast of duck, oven roasted and prepared in a mushroom, green chilli','10.95','10.95','10.95','10.95',1,1,'2014-03-12 06:37:17',1,0,1),(278,87,1,4,'Bahari Baked Chicken','Seasoned chicken breast oven baked with spicy minced-meat stuffing, served on three layers of','10.95','10.95','10.95','10.95',1,1,'2014-01-25 12:52:05',1,0,1),(279,87,1,4,'Bass Salon','Sea Bass delicately cooked in a tomato and mustard infused sauce with roast','10.95','10.95','10.95','10.95',1,1,'2014-01-25 12:52:06',1,0,1),(280,87,3,4,'Aromatic Beef Curry','Chunky cut beef, steamed and cooked in a red pepper and coriander sauce,','10.95','10.95','10.95','10.95',1,1,'2014-01-25 12:52:07',1,0,1),(281,87,1,4,'Lamb Roasti','Braised lamb pieces on vegetables, pan fired cabbage and red peppers,','10.95','10.95','10.95','10.95',1,1,'2014-01-25 12:52:07',1,0,1),(282,87,1,4,'Curried Chooza','Spiced chicken pieces cooked with a variety of peppers, inspired by oriental','10.95','10.95','10.95','10.95',1,1,'2014-01-25 12:52:08',1,0,1),(283,88,1,4,'Beef Steak','Served with fresh green salad and chips','9.95','8.95','8.95','8.95',1,1,'2014-01-25 12:52:08',1,0,1),(284,88,1,4,'Scampi','Served with fresh green salad and chips','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:52:09',1,0,1),(285,88,1,4,'Chicken Nuggets','Served with fresh green salad and chips','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:52:09',1,0,1),(286,88,1,4,'Plain Omlette','Served with fresh green salad and chips','7.75','6.25','6.25','6.25',1,1,'2014-01-25 12:52:09',1,0,1),(287,88,1,4,'Fried Chicken','Served with fresh green salad and chips','7.95','6.25','6.25','6.25',1,1,'2014-01-25 12:52:10',1,0,1),(288,90,1,4,'Mach Bhajee','Pangash, spring onion, garlic, onions and turmeric','3.95','3.25','3.25','3.25',1,1,'2014-01-25 12:52:10',1,0,1),(289,90,1,4,'Shobuj Kobi (S/D)','Brocolli and savoy cabbage, onions, spring onions,','3.95','3.25','3.25','3.25',1,1,'2014-01-25 12:52:10',1,0,1),(290,90,1,4,'Bindi Bhajee(S/D)','Okra prepared in a semi dry sauce with tomatoes','3.95','3.25','3.25','3.25',1,1,'2014-01-25 12:52:11',1,0,1),(291,90,1,4,'Sag Aloo (S/D)','Fresh spinach & aloo tossed in butter','3.95','3.25','3.25','3.25',1,1,'2014-01-25 12:52:11',1,0,1),(292,90,1,4,'Tarka Dall (S/D)','Red lentils fried with garlic and herbs','3.95','3.25','3.25','3.25',1,1,'2014-01-25 12:52:12',1,0,1),(293,90,1,4,'Bombay Aloo (S/D)','Spicy potato cooked with plum tomato sauce','3.95','3.25','3.25','3.25',1,1,'2014-01-25 12:52:12',1,0,1),(294,90,1,4,'Sag Paneer (S/D)','Fresh spinach, Indian cheese with onions','3.95','3.25','3.25','3.25',1,1,'2014-01-25 12:52:13',1,0,1),(295,91,1,4,'Boiled Rice','Boiled Rice','2.25','1.95','1.95','1.95',1,1,'2014-01-25 12:52:13',1,0,1),(296,91,1,4,'Pilau Rice','Pilau Rice','2.45','2.10','2.10','2.10',1,1,'2014-01-25 12:52:13',1,0,1),(297,91,1,4,'Fried Rice','Fried Rice','2.25','2.10','2.10','2.10',1,1,'2014-01-25 12:52:14',1,0,1),(298,91,1,4,'Navratan Rice','Mixed Fruits','2.95','2.60','2.60','2.60',1,1,'2014-01-25 12:52:14',1,0,1),(299,91,1,4,'Egg Fried Rice','Egg Fried Rice','2.95','2.60','2.60','2.60',1,1,'2014-01-25 12:52:15',1,0,1),(300,91,1,4,'Mushroom Fried Rice','Mushroom Fried Rice','2.95','2.60','2.60','2.60',1,1,'2014-01-25 12:52:15',1,0,1),(301,91,1,4,'Spicy Keema Pilau Rice','Spicy Keema Pilau Rice','3.45','3.25','3.25','3.25',1,1,'2014-01-25 12:52:16',1,0,1),(302,91,1,4,'Sizzling Rice','(Garlic, Lemon & Green chilli)','2.95','2.60','2.60','2.60',1,1,'2014-01-25 12:52:17',1,0,1),(303,91,1,4,'Chicken Fried Rice','Chicken Fried','3.95','3.45','3.45','3.45',1,1,'2014-01-25 12:52:17',1,0,1),(304,97,1,4,'Peshwari Nan (n)','(mixed nuts and honey)','2.45','2.20','2.20','2.20',1,1,'2014-01-25 12:52:18',1,0,1),(305,97,1,4,'Garlic Nan','(fresh slices of garlic)','2.45','2.20','2.20','2.20',1,1,'2014-01-25 12:52:18',1,0,1),(306,97,1,4,'Cheese Nan','(soft cheese)','2.45','2.20','2.20','2.20',1,1,'2014-01-25 12:52:18',1,0,1),(307,97,1,4,'Keema Nan','(spicy mince lamb)','2.45','2.20','2.20','2.20',1,1,'2014-01-25 12:52:19',1,0,1),(308,97,1,4,'Kulcha Nan','(mixed vegetables)','2.45','2.20','2.20','2.20',1,1,'2014-01-25 12:52:19',1,0,1),(309,97,1,4,'Chilli Cheese Corriander Nan','Chilli Cheese Corriander Nan','2.95','2.75','2.75','2.75',1,1,'2014-01-25 12:52:20',1,0,1),(310,97,1,4,'Keema & Cheese Nan','Keema & Cheese Nan','2.95','2.75','2.75','2.75',1,1,'2014-01-25 12:52:20',1,0,1),(311,97,1,4,'Tandoori Roti','(baked wholemeal bread)','2.25','1.95','1.95','1.95',1,1,'2014-01-25 12:52:21',1,0,1),(312,97,1,4,'Paratha','(Layered bread)','2.95','2.50','2.50','2.50',1,1,'2014-01-25 12:52:21',1,0,1),(313,97,1,4,'Stuffed Paratha','(Mix vegetables)','3.45','2.75','2.75','2.75',1,1,'2014-01-25 12:52:22',1,0,1),(314,89,1,4,'Spicy Papadom','Spicy Papadom','0.70','0.25','0.25','0.25',1,1,'2014-01-25 12:52:22',1,0,1),(315,89,1,4,'Raitha (Plain or Mix)','Raitha (Plain or Mix)','1.25','1.25','1.25','1.25',1,1,'2014-01-25 12:52:23',1,0,1),(316,89,1,4,'Fresh Salad','Fresh Salad','1.95','1.95','1.95','1.95',1,1,'2014-01-25 12:52:23',1,0,1),(317,89,1,4,'Salad/Sauce Tray','Salad/Sauce Tray','1.50','1.50','1.50','1.50',1,1,'2014-01-25 12:52:24',1,0,1),(318,89,1,4,'Mango Chutney','Mango Chutney','0.50','0.50','0.50','0.50',1,1,'2014-01-25 12:52:24',1,0,1),(319,89,1,4,'Chilli Pickle','Chilli Pickle','0.50','0.50','0.50','0.50',1,1,'2014-01-25 12:52:24',1,0,1),(320,89,1,4,'Mint Sauce','Mint Sauce','0.50','0.50','0.50','0.50',1,1,'2014-01-25 12:52:25',1,0,1),(321,89,1,4,'Chilli Tamarind Sauce','Chilli Tamarind Sauce','0.50','0.50','0.50','0.50',1,1,'2014-01-25 12:52:25',1,0,1),(322,89,1,4,'Lime Picke','Lime Picke','0.50','0.50','0.50','0.50',1,1,'2014-01-25 12:52:25',1,0,1),(323,107,1,4,'Spice Papadom (Healthy)','(baked not fried, made from fibre-rich lentils)','0.90','0.90','0.90','0.90',0,1,'2014-01-26 03:15:49',1,0,1),(324,107,1,4,'Plain Papadom (Healthy)','(baked not fried, made from fibre-rich lentils)','0.90','0.90','0.90','0.90',0,1,'2014-01-26 03:15:05',1,0,1),(325,107,1,4,'Tandoori Salmon Biran (Healthy)','Salmon cooked in the tandoor served with salad and mint yoghurt sauce','5.25','5.25','5.25','5.25',0,1,'2014-01-26 03:45:45',1,0,1),(326,107,1,4,'Garlic King Prawn (Healthy)','Fresh garlic and herb infused king prawn flash fried in a pan with coconut oil, served with salad and mint yoghurt sauce','5.95','5.95','5.95','5.95',0,1,'2014-01-26 03:29:27',1,0,1),(327,107,1,4,'Amritsa Chicken (Healthy)','Spicer alternative to chicken tikka, pieces of boneless skinless chicken marinated in a varying array of flavors, served on a bed of Greek yoghurt and shredded lettuce','4.45','4.45','4.45','4.45',1,1,'2014-01-26 03:17:04',0,0,1),(328,107,1,4,'Cholay Bhajia (Healthy)','Chickpeas stir-fried with onions, spring onions, peppers, garlic, fresh coriander and ground mix spice served with salad and tamarind sauce','4.45','4.45','4.45','4.45',0,1,'2014-01-26 03:17:52',1,0,1),(329,107,1,4,'Mulligatawny Soup (Healthy)','Spicy lentil and curry soup, with hint of lemon and fresh coriander','4.20','4.20','4.20','4.20',0,1,'2014-01-26 03:14:36',1,0,1),(330,108,1,4,'Chicken Tikka Masala (Healthy)','Britainâ€™s favourite Tikka Masala marinated in Greek yogurt and spice simmered on low heat until the sauce thickens reducing the calories for a healthier alternative to a favourite dish','7.95','7.95','7.95','7.95',0,1,'2014-01-26 03:25:22',1,0,1),(331,108,1,4,'Spiced Tava Kebab (m) (Healthy)','Marinated spring chicken breast, sautÃ©ed in a tamarind based sauce with green peppers and onions with coconut oil','7.95','7.95','7.95','7.95',0,1,'2014-01-26 03:24:03',1,0,1),(332,108,1,4,'King Prawn Shashliq (Healthy)','Barbecued on the charcoal with green peppers, tomatoes, onions and garlic','10.95','10.95','10.95','10.95',0,1,'2014-01-26 03:26:20',1,0,1),(333,108,1,4,'Shabji Patia (M) (Healthy)','Fresh spinach, chickpeas and aubergine in a tangy, hot and sweet tomato based sauce','7.45','7.45','7.45','7.45',0,1,'2014-01-26 03:27:59',1,0,1),(334,108,1,4,'Shahi Lamb (Healthy)','Lean and tender lamb pieces cooked in a mild sauce made from Greek yogurt, ground spices with a hint of fresh garden mint','7.95','7.95','7.95','7.95',0,1,'2014-01-26 03:32:48',1,0,1),(335,108,1,4,'Kala Mirch Chicken (Healthy)','Chicken cooked in coconut oil with sundried red chilli flakes, tandoor roasted garlic, coconut milk and lean minced meat with scotch bonnet pepper and coriander','8.95','8.95','8.95','8.95',0,1,'2014-01-26 03:34:38',1,0,1),(336,108,1,4,'Fish Salon (Healthy)','Tropical white fish dusted with mustard powder pan fried with coconut oil, cooked in light spicy broth with fresh spinach and tandoor baked pepper, onion and tomato','10.95','10.95','10.95','10.95',0,1,'2014-01-26 03:35:37',1,0,1),(337,107,1,4,'Chawal Fitta (Healthy)','Bangladeshi version of chapatti, made of Basmati rice flour with a light texture','1.00','1.00','1.00','1.00',0,1,'2014-01-26 03:47:08',1,0,1),(338,111,1,4,'Chawal Fitta (Healthy)','Bangladeshi version of chapatti, made of Basmati rice flour with a light texture','1.00','1.00','1.00','1.00',0,1,'2014-01-26 03:58:37',1,0,1),(339,111,1,4,'Chapati (Healthy)','Wholegrain flour tortilla style bread','1.25','1.25','1.25','1.25',0,1,'2014-01-26 03:59:59',1,0,1),(340,111,3,4,'Tandoori Roti (Healthy)',' Tandoor baked nan style bread','2.25','2.25','2.25','2.25',0,1,'2014-01-26 04:01:04',1,0,1),(341,111,1,4,'Boiled Rice (Healthy)','Boiled Basmati Rice','2.45','2.45','2.45','2.45',0,1,'2014-01-26 04:02:37',1,0,1),(342,111,1,4,'Tandoor Pepper Rice','Baked pepper pan fried with Basmati rice with coconut oil','2.95','2.95','2.95','2.95',0,1,'2014-01-26 04:03:43',1,0,1),(343,111,3,4,'Pineapple Chilli Rice','Pineapple pieces and fresh chopped green chillies pan fried with Basmati rice with coconut oil','2.95','2.95','2.95','2.95',0,1,'2014-01-26 04:04:22',1,0,1),(344,111,1,4,'Sizzling Rice (Healthy)','Spring onions, fresh chopped chillies, fresh sliced garlic and lemon pieces pan fried with Basmati rice with coconut','2.95','2.95','2.95','2.95',0,1,'2014-01-26 04:05:45',1,0,1),(345,111,1,4,'Coconut Rice (Healthy)','Desiccated coconut pan fried with Basmati rice with coconut oil','2.95','2.95','2.95','2.95',1,0,'2014-01-26 04:06:54',1,0,1),(346,106,3,4,'Tea','Tea........','1.75','1.75','1.75','1.75',0,1,'2014-01-26 04:10:49',1,0,1),(347,106,3,4,'Coffee','Coffee....','1.95','1.95','1.95','1.95',0,1,'2014-01-26 04:11:25',1,0,1),(348,106,3,4,'Cream Coffee','Cream Coffee','2.50','2.50','2.50','2.50',0,1,'2014-01-26 04:12:30',1,0,1),(349,106,3,4,'Diet Coke','Diet Coke....','1.75','1.75','1.75','1.75',0,1,'2014-01-26 04:13:28',1,0,1),(350,106,3,4,'Coke','Coke......','1.75','1.75','1.75','1.75',0,1,'2014-01-26 04:14:26',1,0,1),(351,106,3,4,'Lemonade','Lemonade...','1.75','1.75','1.75','1.75',0,1,'2014-01-26 04:15:24',1,0,1),(352,106,3,4,'Orange Juice','Orange Juice','1.75','1.75','1.75','1.75',0,1,'2014-01-26 04:16:07',1,0,1),(353,106,3,4,'J20 Apple & Mango','J20 Apple & Mango','2.50','2.50','2.50','2.50',0,1,'2014-01-26 04:16:59',1,0,1),(354,106,3,4,'J20 Orange & Passion','J20 Orange & Passion','2.50','2.50','2.50','2.50',0,1,'2014-01-26 04:18:56',1,0,1),(355,106,3,4,'Mineral Water','Mineral Water','1.50','1.50','1.50','1.50',0,1,'2014-01-26 04:19:44',1,0,1),(356,105,2,4,'Coconut Sponge','Coconut Sponge','4.25','4.25','4.25','4.25',0,1,'2014-01-26 04:21:06',1,0,1),(357,105,2,4,'Ferrero Roche Cheesecake','Ferrero Roche Cheesecake','4.75','4.75','4.75','4.75',0,1,'2014-01-26 04:21:58',1,0,1),(358,105,2,4,'Chocolate Orange Sponge','Chocolate Orange Sponge','4.25','4.25','4.25','4.25',0,1,'2014-01-26 04:23:39',1,0,1),(359,105,2,4,'Mango Cheesecake','Mango Cheesecake','4.75','4.75','4.75','4.75',0,1,'2014-01-26 04:24:13',1,0,1),(360,105,2,4,'Waffles','Waffles.....','4.25','4.25','4.25','4.25',0,1,'2014-01-26 04:25:17',1,0,1),(361,105,2,4,'Fantastica','Fantastica','3.95','3.95','3.95','3.95',0,1,'2014-01-26 04:26:00',1,0,1),(363,105,2,4,'Mango Kulfi','Mango Kulfi','3.50','3.50','3.50','3.50',0,1,'2014-01-26 04:27:57',1,0,1),(364,105,2,4,'Pistachio Kulfi','Pistachio Kulfi','3.45','3.45','3.45','3.45',0,1,'2014-01-26 04:28:43',1,0,1),(365,105,2,4,'Gulabjamun','Gulabjamun','3.95','3.95','3.95','3.95',0,1,'2014-01-26 04:29:58',1,0,1),(366,105,3,4,'Banana Sponge Fritters with fruity custard','Banana Sponge Fritters with fruity custard','4.25','4.25','4.25','4.25',0,1,'2014-01-26 04:30:49',1,0,1),(367,105,2,4,'Ice Cream','Ice Cream....','2.50','2.50','2.50','2.50',0,1,'2014-01-26 04:32:07',1,0,1),(368,89,1,4,'Chips','Chips.....','1.80','1.70','1.70','1.70',1,1,'2014-01-30 12:13:39',1,0,1),(369,53,2,4,'Papadom','Papadom.....','0.70','0.25','0.25','0.25',1,1,'2014-01-30 12:40:01',1,0,1),(370,53,2,4,'Spice Papadom','Spice Papadom','0.80','0.60','0.60','0.60',1,1,'2014-01-30 12:39:22',1,0,1),(371,53,2,4,'salad/sauce tray','salad/sauce tray','1.50','1.50','1.50','1.50',1,1,'2014-01-30 12:38:13',1,0,1),(372,53,3,4,'Mango Chutney','Mango Chutney','0.50','0.50','0.50','0.50',1,1,'2014-03-11 07:00:25',1,0,1),(373,53,2,4,'Lime Pickle','Lime Pickle','0.50','0.50','0.50','0.50',1,1,'2014-01-30 12:43:20',1,0,1),(374,72,1,4,'E-B Chicken Balti','E-B Chicken Balti','0.01','0.01','0.01','0.01',0,1,'2014-02-06 08:13:56',1,0,1),(375,112,1,4,'Early Bird','Early Bird','8.95','8.95','8.95','8.95',1,1,'2014-02-09 04:13:17',1,0,1),(376,131,1,4,'Early Bird','Early Bird','8.95','8.95','8.95','8.95',1,1,'2014-02-09 05:14:16',1,0,1),(377,113,5,4,'Amritsa Chicken ','Chicken marinated in a garam masala and panch\npuran mix, tandoori cooked, spicy to taste','0.50','0.50','0.50','0.50',1,1,'2014-02-26 10:26:55',1,1,1),(379,113,1,4,'Tava Kebab','Fillet of chicken breast, lightly coated in Tandoori\nherbs and spices then slowly cooked on a griddle','0.50','0.00','0.00','0.00',1,1,'2014-02-26 10:30:50',1,2,1),(380,113,1,4,'Nawabi Kebab ','Home style meat patty, lightly spiced, onions and\nloaded with fresh herbs, served with a fried egg','0.50','0.00','0.00','0.00',1,1,'2014-02-26 10:32:35',1,3,1),(382,113,1,4,'Spicy Chicken Fry','Strips of Tandoori chicken breast flavoured with\nchilli pickle, bell peppers, garlic, onions and\nvarious fresh herbs','0.50','0.50','0.50','0.50',1,1,'2014-02-26 10:36:51',1,4,1),(383,113,1,4,'Chilli Fried Kolja','Sweet and spicy pan fried chicken liver, marinated\nin fresh herbs and chilli based sauce','0.50','0.50','0.50','0.50',1,1,'2014-02-26 10:37:57',1,5,1),(384,113,1,4,'Malai Kufta','Spicy meat balls in a sweet and spicy\ncheese sauce','0.50','0.50','0.50','0.50',1,1,'2014-02-26 10:39:10',1,6,1),(385,113,1,4,'Fara Sira Chicken','Shredded chicken stir-fried and wrapped\nin laacha paratha','0.50','0.50','0.50','0.50',1,1,'2014-02-26 10:40:14',1,7,1),(386,113,1,4,'Chatt Puree (Chicken)','Chatt Puree (Chicken)','0.20','0.20','0.20','0.20',0,1,'2014-02-26 10:41:54',1,8,1),(387,113,1,4,'Chicken Pakora','Chicken Pakora','0.50','0.50','0.50','0.50',1,1,'2014-02-26 10:43:10',1,9,1),(388,113,1,4,'Meat Somosa','Meat Somosa','0.00','0.00','0.00','0.00',0,1,'2014-02-26 10:48:43',1,10,1),(389,113,1,4,'Tandoori Chicken','Tandoori Chicken','0.00','0.00','0.00','0.00',1,1,'2014-02-26 10:49:30',1,11,1),(390,113,1,4,'Chicken Tikka','Chicken Tikka','0.00','0.00','0.00','0.00',1,1,'2014-02-26 10:50:27',1,12,1),(391,113,1,4,'Sheek Kebab','Sheek Kebab','0.00','0.00','0.00','0.00',1,1,'2014-02-26 10:51:11',1,13,1),(392,113,1,4,'Shami Kebab','Shami Kebab','0.00','0.00','0.00','0.00',1,1,'2014-02-26 10:51:59',1,14,1),(393,113,1,4,'Nargis Kebab','Nargis Kebab','0.50','0.50','0.50','0.50',1,1,'2014-02-26 10:52:59',1,15,1),(394,114,1,4,'Pepper Fried Tuna','Tuna flakes, pan fried with garlic, ginger,\nfresh coriander and chilli seeds','1.20','1.20','1.20','1.20',1,1,'2014-02-26 10:55:09',1,16,1),(395,114,1,4,'Butterfly Sardines','Masala coated grilled sardines','0.50','0.50','0.50','0.50',1,1,'2014-02-26 10:56:37',1,17,1),(396,114,1,4,'Baked Spice Monk Fish','Pan fried spiced Monk fish, with grilled\npepper and onion sauce','3.95','3.95','3.95','3.95',1,1,'2014-02-26 10:57:48',1,18,1),(397,114,1,4,'Salmon Birran','Salmon steak, lightly marinated in a selection\nof herbs and sauces, pan fried','1.20','1.20','1.20','1.20',1,1,'2014-02-26 10:59:30',1,19,1),(398,114,1,4,'Garlic King Prawn','Jumbo king prawns, marinated in garlic and herbs\nand flash fried, served with caramelised onions','3.95','3.95','3.95','3.95',1,1,'2014-02-26 11:00:31',1,20,1),(399,114,1,4,'Bhuna Prawns on Puree','Bhuna Prawns on Puree','0.20','0.20','0.20','0.20',1,1,'2014-02-26 11:01:35',1,21,1),(400,114,1,4,'Prawn Cocktail','Prawn Cocktail','0.20','0.20','0.20','0.20',1,1,'2014-02-26 11:03:29',1,22,1),(401,116,1,4,'Paneer Relish','Thick strips of paneer stir-fried with inions\npeppers in a spicy sauce','0.50','0.50','0.50','0.50',1,1,'2014-02-26 11:05:22',1,23,1),(402,116,5,4,'Aloo Chomka','Aloo Chomka','0.50','0.50','0.50','0.50',1,1,'2014-02-26 11:06:41',1,24,1),(403,116,1,4,'Vegetable Somosa','Vegetable Somosa','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:07:17',1,25,1),(404,116,1,4,'Onion Bhajee','Onion Bhajee','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:07:59',1,26,1),(405,116,1,4,'Garlic Mushrooms','Garlic Mushrooms','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:10:14',1,27,1),(406,118,1,4,'Tandoori Chicken (M)','Tandoori Chicken','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:13:11',1,28,1),(407,118,1,4,'Chicken Tikka (M)','Chicken Tikka','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:14:40',1,29,1),(408,118,1,4,'Tandoori Lamb Chops (M)','Tandoori Lamb Chops','2.00','2.00','2.00','2.00',1,1,'2014-02-26 11:16:22',1,30,1),(409,118,1,4,'Tandoori Mix','Tandoori Mix','4.95','4.95','4.95','4.95',1,1,'2014-02-26 11:17:49',1,31,1),(410,118,1,4,'Vegetarian Shasliq','Vegetarian Shasliq','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:19:09',1,32,1),(411,118,1,4,'Chicken Shashliq','Chicken Shashliq','0.50','0.50','0.50','0.50',1,1,'2014-02-26 11:20:32',1,33,1),(412,118,1,4,'Shasliq Special','Shasliq Special','3.95','3.95','3.95','3.95',1,1,'2014-02-26 11:21:53',1,34,1),(413,119,1,7,'Madrasi Biryani','Madrasi Biryani','1.00','1.00','1.00','1.00',1,1,'2014-02-26 11:28:30',1,35,1),(414,119,1,7,'Mix Biryani','Mix Biryani','2.00','2.00','2.00','2.00',1,1,'2014-02-26 11:29:23',1,36,1),(415,119,1,7,'Lamb Biryani','Lamb Biryani','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:30:39',1,37,1),(416,119,1,7,'Chicken Biryani','Chicken Biryani','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:32:02',1,38,1),(417,119,1,7,'Chicken & Mush Biryani','Chicken & Mush Biryani','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:34:46',1,39,1),(418,119,1,7,'King Prawn Biryani','King Prawn Biryani','2.95','2.95','2.95','2.95',1,1,'2014-02-26 11:35:39',1,40,1),(419,119,1,7,'Tan Kg Pr Biryani','Tan Kg Pr Biryani','4.95','4.95','4.95','4.95',1,1,'2014-02-26 11:36:32',1,41,1),(420,124,1,7,'Chicken Kurma','Very mild and sweet dish, consisting of cream,\ncoconut and sultanas.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:40:01',1,42,1),(421,124,1,7,'Prawn Kurma','Very mild and sweet dish, consisting of cream,\ncoconut and sultanas.','0.00','0.00','0.00','0.00',0,1,'2014-02-26 11:40:58',1,43,1),(422,124,1,7,'Veg Kurma','Very mild and sweet dish, consisting of cream,\ncoconut and sultanas.','0.00','0.00','0.00','0.00',0,1,'2014-02-26 11:41:53',1,44,1),(423,124,1,7,'Lamb Kurma','Very mild and sweet dish, consisting of cream,\ncoconut and sultanas.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:42:43',1,45,1),(424,124,1,7,'Mix Kurma','Very mild and sweet dish, consisting of cream,\ncoconut and sultanas.','3.95','3.95','3.95','3.95',1,1,'2014-02-26 11:43:33',1,46,1),(425,124,1,7,'Chicken Tikka Kurma','Chicken Tikka Kurma','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:44:41',1,47,1),(426,124,1,7,'Lamb Tikka Kurma','Lamb Tikka Kurma','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:45:29',1,48,1),(427,124,1,7,'Sag Aloo Paneer Kurma','Sag Aloo Paneer Kurma','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:46:21',1,49,1),(428,124,1,7,'Chicken & Mushroom Kurma','Chicken & Mushroom Kurma','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:47:19',1,50,1),(429,124,1,7,'Chicken & Kg Prawn Kurma','Chicken & Kg Prawn Kurma','2.00','1.50','1.50','1.50',1,1,'2014-02-26 12:30:00',1,51,1),(430,121,1,7,'Chicken Bhuna','Chicken Bhuna','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:57:54',1,52,1),(431,121,1,7,'Prawn Bhuna','Prawn Bhuna','0.00','0.00','0.00','0.00',1,1,'2014-02-26 11:59:26',1,53,1),(432,121,1,7,'Veg Bhuna','Vegtable Bhuna','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:00:33',1,54,1),(433,121,1,7,'Lamb Bhuna','Lamb Bhuna','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:01:20',1,55,1),(434,121,1,7,'King Prawn Bhuna','King Prawn Bhuna','2.95','2.00','2.00','2.00',1,1,'2014-02-26 12:08:31',1,56,1),(435,124,1,7,'King Prawn Korma','King Prawn Korma','2.95','2.95','2.95','2.95',1,1,'2014-02-26 12:05:40',1,57,1),(436,124,1,7,'King Prawn Kurma','King Prawn Kurma','2.95','2.95','2.95','2.95',1,1,'2014-02-26 12:07:44',1,58,1),(437,121,1,7,'Mix Bhuna','Mix Bhuna....','3.95','3.95','3.95','3.95',1,1,'2014-02-26 12:09:47',1,59,1),(438,121,1,7,'Chicken Tikka Bhuna','Chicken Tikka Bhuna','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:10:51',1,60,1),(439,121,1,7,'Lamb Tikka Bhuna','Lamb Tikka Bhuna','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:11:37',1,61,1),(440,121,1,7,'Sag Aloo Paneer Bhuna','Sag Aloo Paneer Bhuna','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:13:32',1,62,1),(441,121,1,7,'Chicken & Mush Bhuna','Chicken & Mush Bhuna','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:14:31',1,63,1),(442,74,1,7,'Chicken & Kg Pr Bhuna','Chicken & Kg Pr Bhuna','2.00','2.00','2.00','2.00',1,1,'2014-02-26 12:15:22',1,64,1),(443,120,1,7,'Chicken Balti','Chicken Balti','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:16:08',1,65,1),(444,120,1,7,'Prawn Balti','Prawn Balti','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:16:51',1,66,1),(445,120,1,7,'Veg Balti ','Mix Veg Balti','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:17:47',1,67,1),(446,120,1,7,'Lamb Balti','Lamb Balti','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:19:18',1,68,1),(447,120,1,7,'King Prawn Balti','King Prawn Balti','2.00','2.00','2.00','2.00',1,1,'2014-02-26 12:21:18',1,69,1),(448,120,1,7,'Mix Balti','Mix Balti.....','3.95','3.95','3.95','3.95',1,1,'2014-02-26 12:22:25',1,70,1),(449,120,1,7,'Chicken Tikka Balti ','Chicken Tikka Balti ','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:23:29',1,71,1),(451,120,1,7,'Lamb Tikka Balti','Lamb Tikka Balti','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:25:38',1,72,1),(452,120,1,7,'Sag Aloo Paneer Balti','An authentic dish created with a dozen herbs and\nspices in medium bell pepper and onion sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:26:51',1,73,1),(454,120,1,7,'Chicken & Mushroom','Chicken & Mushroom','0.00','0.00','0.00','0.00',1,1,'2014-02-26 12:28:51',1,74,1),(455,120,1,7,'Chicken & Kg Pr Balti','Chicken & Kg Pr Balti','2.00','2.00','2.00','2.00',1,1,'2014-02-26 12:30:54',1,75,1),(456,128,1,4,'Chicken Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:24:05',1,76,1),(457,128,1,4,'Prawn Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:24:43',1,77,1),(458,128,1,4,'Veg Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:25:23',1,78,1),(459,128,1,4,'Lamb Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:26:05',1,79,1),(460,128,1,4,'King Prawn Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','2.95','2.95','2.95','2.95',1,1,'2014-02-26 04:27:28',1,80,1),(461,128,1,4,'Mix Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','3.95','3.95','3.95','3.95',1,0,'2014-02-26 04:28:25',1,81,1),(462,128,1,4,'Chicken Tikka Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:29:16',1,82,1),(463,128,1,4,'Lamb Tikka Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:30:58',1,83,1),(464,128,1,4,'Sag Aloo Paneer Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:31:44',1,84,1),(465,128,1,4,'Chicken & Mush Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:32:26',1,85,1),(466,128,1,4,'Chicken & Kg Pr Rogan','Cooked in a rich gravy, concentrated broth\nof tomatoes and select spices that provide\nsubtle flavours.','2.00','2.00','2.00','2.00',1,1,'2014-02-26 04:33:48',1,86,1),(467,123,1,4,'Chicken Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:40:26',1,87,1),(468,123,1,4,'Prawn Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:38:35',1,88,1),(469,123,1,4,'Veg Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:46:18',1,89,1),(470,123,1,4,'Lamb Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 04:47:50',1,90,1),(471,123,1,4,'King Prawn Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','2.95','2.95','2.95','2.95',1,1,'2014-02-26 04:48:57',1,91,1),(472,123,3,4,'Chicken Tikka Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:06:57',1,92,1),(473,123,1,4,'Lamb Tikka Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:08:23',1,93,1),(474,123,1,4,'Sag Aloo Paneer Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:09:35',1,94,1),(475,123,1,4,'Chicken & Mush Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:10:26',1,95,1),(476,123,1,4,'Chicken & Kg Pr Patia','Tomato based, fairly spicy hot, sweet and sour\ndish with hints of aromatic herbs.','2.00','2.00','2.00','2.00',1,1,'2014-02-26 05:11:17',1,96,1),(477,127,1,4,'Chicken Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:12:23',1,97,1),(478,127,1,4,'Prawn Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:13:10',1,98,1),(479,127,1,4,'Veg Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:13:54',1,99,1),(480,127,1,4,'Lamb Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:14:38',1,100,1),(481,127,1,4,'King Prawn Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','2.95','2.95','2.95','2.95',1,1,'2014-02-26 05:15:45',1,101,1),(482,127,1,4,'Mix Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','3.95','3.95','3.95','3.95',1,1,'2014-02-26 05:16:33',1,102,1),(483,127,1,4,'Chicken Tikka Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:17:18',1,103,1),(484,127,1,4,'Lamb Tikka Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:18:13',1,104,1),(485,127,1,4,'Sag Aloo Paneer Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:19:33',1,105,1),(486,127,1,4,'Chi & Mush Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:22:27',1,106,1),(487,127,1,4,'Chi & Kg Pr Sagwala','Briskly fried preparation of freshly chopped\nspinach, garlic, onions and spices','2.00','2.00','2.00','2.00',1,1,'2014-02-26 05:23:13',1,107,1),(488,133,1,4,'Chicken Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:25:44',1,108,1),(489,133,1,4,'Prawn Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:26:38',1,109,1),(490,133,1,4,'Veg Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:28:02',1,110,1),(491,133,1,4,'Lamb Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:28:40',1,111,1),(492,133,1,4,'King Prawn Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','2.95','0.00','0.00','0.00',1,1,'2014-02-26 05:31:30',1,112,1),(493,133,1,4,'Mix Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','3.95','3.95','3.95','3.95',1,1,'2014-02-26 05:32:23',1,113,1),(494,133,1,4,'Chicken Tikka Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:33:43',1,114,1),(495,133,1,4,'Lamb Tikka Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:34:40',1,115,1),(496,133,1,4,'Sag Aloo Paneer Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:36:11',1,116,1),(497,133,1,4,'Chi & Mush Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:37:24',1,117,1),(498,133,1,4,'Chi & Kg Pr Dhansak','Pan fried with pineapple and green herbs and garlic\nthen cooked in a spicy, sweet and sour lentil sauce.','2.00','2.00','2.00','2.00',1,1,'2014-02-26 05:38:10',1,118,1),(499,122,1,4,'Chi Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:40:46',1,119,1),(500,122,1,4,'Prawn Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:41:37',1,120,1),(501,79,1,4,'Veg Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:42:21',1,121,1),(502,122,1,4,'Veg Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:43:27',1,122,1),(503,122,1,4,'Lamb Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:44:06',1,123,1),(504,122,1,4,'King Prawn Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','2.00','2.00','2.00','2.00',1,1,'2014-02-26 05:45:46',1,124,1),(505,122,1,4,'Mix Jalfrexi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','3.95','3.95','3.95','3.95',1,1,'2014-02-26 05:46:50',1,125,1),(506,122,3,4,'Chicken Tikka Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:47:40',1,126,1),(507,122,3,4,'Lamb Tikka Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:49:03',1,127,1),(508,122,1,4,'Sag Aloo Paneer Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:49:45',1,128,1),(509,122,1,4,'Chi & Mush Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:50:44',1,129,1),(510,122,1,4,'Chi & Kg Pr Jalfrezi','Cooked with onions, peppers and tomato in Chefâ€™s\nspecial selection of herbs and spices with green\nchillies and coriander','2.00','2.00','2.00','2.00',1,1,'2014-02-26 05:51:27',1,130,1),(511,125,1,4,'Chicken Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:52:23',1,131,1),(512,125,1,4,'Prawn Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:53:00',1,132,1),(513,125,1,4,'Veg Curry (Main)','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:53:45',1,133,1),(514,125,3,4,'Lamb Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:54:58',1,134,1),(515,125,1,4,'King Prawn Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','2.95','2.95','2.95','2.95',1,1,'2014-02-26 05:55:55',1,135,1),(516,125,1,4,'MIx Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','3.95','3.95','3.95','3.95',1,1,'2014-02-26 05:56:37',1,136,1),(517,125,1,4,'Chicken Tikka Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 05:58:34',1,137,1),(518,125,1,4,'Lamb TIkka Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:00:02',1,138,1),(519,125,1,4,'Chicken & Mush Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:01:08',1,139,1),(520,125,1,4,'Chi & Kg Pr Curry','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','2.00','2.00','2.00','2.00',1,1,'2014-02-26 06:02:08',1,140,1),(521,126,1,4,'Chicken Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:02:49',1,141,1),(522,126,1,4,'Prawn Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:06:01',1,142,1),(523,126,1,4,'Veg Madras (Main)','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:06:41',1,143,1),(524,126,1,4,'Lamb Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:07:58',1,144,1),(525,126,1,4,'King Prawn Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','2.95','2.95','2.95','2.95',1,1,'2014-02-26 06:10:15',1,145,1),(526,126,1,4,'Mix Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','3.95','3.95','3.95','3.95',1,1,'2014-02-26 06:11:00',1,146,1),(527,126,1,4,'Chicken Tikka Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:11:43',1,147,1),(528,126,1,4,'Lamb Tikka Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:12:55',1,148,1),(529,126,1,4,'Chi & Mush Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:15:48',1,149,1),(530,126,1,4,'Chi & Kg Prawn Madras','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','2.00','2.00','2.00','2.00',1,1,'2014-02-26 06:17:01',1,150,1),(531,132,1,4,'Chicken Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:18:01',1,151,1),(532,132,1,4,'Prawn Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:18:44',1,152,1),(533,132,1,4,'Veg Vindaloo (Main)','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:20:09',1,153,1),(534,132,1,4,'Lamb Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:21:04',1,154,1),(535,132,1,4,'King Prawn Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:22:21',1,155,1),(536,132,1,4,'Mix Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','3.95','3.95','3.95','3.95',1,1,'2014-02-26 06:23:18',1,156,1),(537,132,1,4,'Chicken Tikka Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:24:17',1,157,1),(538,132,1,4,'Lamb Tikka Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:25:28',1,158,1),(539,132,1,4,'Chi & Mush Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 06:26:32',1,159,1),(540,132,1,4,'Chi & Kg Pr Vindaloo','Most popular dishes, made with a wide range of\nAsian spices to give a variety of heat in the sauce','2.00','2.00','2.00','2.00',1,1,'2014-02-26 06:28:03',1,160,1),(541,129,1,7,'Naga Chicken','Truly one for the adventurous, prepared with the\nfamous Bangladeshi Scotch Bonnet pepper','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:22:20',1,161,1),(542,129,1,7,'Lamb Naga','Truly one for the adventurous, prepared with the\nfamous Bangladeshi Scotch Bonnet pepper','1.00','1.00','1.00','1.00',1,1,'2014-02-26 07:23:13',1,162,1),(543,129,1,7,'Bazia Beef','Beef chunks with grilled peppers and onions,\nshatkora (citrus fruit), semi dry madras plus\nstrength dish served sizzling in pickling spices','1.00','1.00','1.00','1.00',1,1,'2014-02-26 07:24:29',1,163,1),(544,129,1,7,'South Indian Chicken','Cooked in hot chilli garlic sauce and garnished\nwith onion chilli salad, hot and spicy to taste','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:25:22',1,164,1),(545,129,1,7,'Relish Ghust','Tender chunks of lamb flambÃ©ed in hot mustard\noil then cooked in a tomato, garlic, coriander\nspicy sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:26:27',1,165,1),(546,129,1,7,'Bahari Mach','Pangash (tropical white fish), marinated with\nBangladeshi pickles then cooked with spinach,\ndressed with a layer of cream cheese & coriander\nherb sauce','4.95','4.95','4.95','4.95',1,1,'2014-02-26 07:27:25',1,166,1),(547,129,1,7,'Sharegama','Chefâ€™s own recipe, off the bone Tandoori chicken,\nmushrooms, sliced garlic and green chillies, loaded\nwith spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:28:24',1,167,1),(548,129,1,7,'Bengal Curry','Pan fried garlic mushrooms cooked with chicken\nbreast strips with sautÃ©ed onions, semi dry sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:29:14',1,168,1),(549,129,1,7,'Methi Tikka Curry','Tikka spiced chicken wrapped in methi leaves\nand garlic cooked in a dupiaza sauce','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:30:29',1,169,1),(550,129,1,7,'Mostani Murgh','Spring chicken cooked in the Tandoori oven,\ncooked in garam masala, fresh coriander and\nlemon sauce garnished with pan fried fresh garlic\nand topped with thin fried potatoes','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:31:22',1,170,1),(551,129,1,7,'Rezalah Chicken','A classic dish from the North of Bengal, tastefully\nspiced with a variety of herbs and spices with\nsautÃ©ed tomatoes, green pepper and fresh\ncoriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:32:22',1,171,1),(552,129,1,7,'Rezalah  Lamb','A classic dish from the North of Bengal, tastefully\nspiced with a variety of herbs and spices with\nsautÃ©ed tomatoes, green pepper and fresh\ncoriander','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:33:43',1,172,1),(553,129,1,7,'Tandoori Murghi Mosala','Boneless strips of Tandoori chicken, cooked in a\nmedium mince meat sauce with spicy boiled egg','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:35:21',1,173,1),(554,129,1,7,'Sizzling Steak Fry','Juliennes of sirloin steak pan fried with onion and\nbell pepper rings, mushrooms and spicy keema\nwith hint of mustard, served with chips','5.95','5.95','5.95','5.95',1,1,'2014-02-26 07:36:35',1,174,1),(555,129,1,7,'Shasliq Bhuna Kadai','Succulent pieces of chicken Tikka, lamb Tikka,\ntomato, onion and peppers char-grilled and\nsizzled up into a balti','1.00','1.00','1.00','1.00',1,1,'2014-02-26 07:37:39',1,175,1),(556,129,1,7,'Jinga Jool','King prawns fried with fennel seeds and\ncloves, fresh ginger and garlic in a creamy\ngarlic curry sauce','7.95','7.95','7.95','7.95',1,1,'2014-02-26 07:38:49',1,176,1),(557,129,1,7,'Tarka Chicken','Thinly sliced chicken Tikka stir fried with peppers,\nspring onions, baby corn, baby carrots and layered\nwith a garlic Tikka sauce','1.00','1.00','1.00','1.00',1,1,'2014-02-26 07:40:08',1,177,1),(558,129,1,7,'Peshwari Lamb','Chicken or lamb cooked with cinnamon and\ncardamom in a Alphonso mango sauce, a mild\nflavoursome dish from the North Punjab','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:42:27',1,178,1),(559,129,1,7,'Peshwari Chicken','Peshwari Lamb/Chicken','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:46:07',1,111,1),(560,129,1,7,'Shahi Lamb','Tender lamb pieces cooked in a mild sauce\nwith hints of fresh garden mint','1.00','1.00','1.00','1.00',1,1,'2014-02-26 07:46:57',1,113,1),(561,129,1,7,'Chicken Makhani','Pieces of chicken Tikka prepared in cream sauce,\nbutter, plum tomatoes and ground nuts garnished\nwith fresh coriander','1.00','1.00','1.00','1.00',1,1,'2014-02-26 07:47:48',1,114,1),(562,129,1,7,'Chicken Tikka Pasanda','Chicken Tikka marinated in spiced yoghurt, and\nthen char grilled and cooked in mild creamy\nsauce, with hints of cinnamon','1.00','1.00','1.00','1.00',1,1,'2014-02-26 07:49:21',1,115,1),(563,129,1,7,'Chicken Tikka Mosala','Tandoor baked chicken, then cooked in rich\ncreamy almond sauce with delicate blend\nof aromatic herbs and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 09:16:55',1,112,1),(564,129,1,7,'Lamb Tikka Mosala','Tandoor Lamb Tikka, then cooked in rich\ncreamy almond sauce with delicate blend\nof aromatic herbs and spices','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:52:12',1,116,1),(565,129,1,7,'Tandoori Kg Pr Mosala','Tandoor King Prawn, then cooked in rich\ncreamy almond sauce with delicate blend\nof aromatic herbs and spices','6.95','6.95','6.95','6.95',1,1,'2014-02-26 07:54:08',1,117,1),(566,129,1,7,'Garlic Chilli Chi Mosala','Garlic Chilli Chi Mosala','0.00','0.00','0.00','0.00',1,1,'2014-02-26 07:55:24',1,118,1),(567,129,1,7,'Chi Cheese Kofta Curry','Chi Cheese Kofta Curry','1.00','1.00','1.00','1.00',1,1,'2014-02-26 07:56:29',1,119,1),(568,134,1,7,'Chefs Special','This is an exclusive dish conjured up by our Head Chef. He has used his great experience and\ncooking flair to create you a unique dish that is true representation of Anglo-Indian culinary arts.\nWe aim to change the Chef Special of the day frequently;','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:06:08',1,120,1),(569,134,1,7,'Lamb Nihari','Steamed lamb shank, coated in freshly ground panch puran cooked with barbequed onions\nin a light sauce with hints of chilli, garlic and a dash of freshly squeezed orange juice','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:07:08',1,121,1),(570,134,1,7,'Shabji Bazia','Medley of fresh vegetables; (potatoes, carrots, mushrooms, butternut squash, baby sweetcorn)\nfirstly blanched then cooked with green chillies, green and red peppers, spring onions in a\ncombination of garam mosala and paunch puran spice mix','3.95','3.95','3.95','3.95',1,1,'2014-02-26 08:08:01',1,122,1),(571,134,1,7,'Salmon Surprise','Salmon fillet dusted with chilli and turmeric powder sautÃ©ed in a garlic butter sauce, combined\nwith virgin olive oil roasted vine tomatoes, coriander, spring onion and lightly spiced chicken\npieces added creating a unique and super tasty dish. Serv','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:08:52',1,123,1),(572,134,1,7,'Kerala Duck Curry','Breast of duck, oven roasted and prepared in a mushroom, green chilli and\nhoney medley, spicy with delicate flavour','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:09:42',1,124,1),(573,134,1,4,'Bass Salon','Sea Bass delicately cooked in a tomato and mustard infused sauce with roast\npotatoes, served with Sizzling Rice','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:14:49',1,125,1),(574,134,5,4,'Aromatic Beef Curry','Chunky cut beef, steamed and cooked in a red pepper and coriander sauce,\nserved with boiled rice and pommes frites','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:15:38',1,126,1),(575,134,1,4,'Lamb Roasti','Braised lamb pieces on vegetables, pan fired cabbage and red peppers,\ntopped with crispy fried onions and served with Spicy Keema Rice','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:25:20',1,127,1),(576,134,1,4,'Curried Chooza','Spiced chicken pieces cooked with a variety of peppers, inspired by oriental\nmethods, a dry dish served with Mushroom Rice','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:26:43',1,128,1),(577,130,1,4,'Boiled Rice','Boiled Rice','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:28:27',1,129,1),(578,130,1,4,'Pilau Rice','Pilau Rice','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:29:21',1,130,1),(579,130,1,4,'Fried Rice','Fried Rice','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:30:21',1,131,1),(580,130,1,4,'Navratan Rice','Navratan Rice','0.50','0.50','0.50','0.50',1,1,'2014-02-26 08:31:29',1,132,1),(581,130,1,4,'Egg Fried Rice','Egg Fried Rice','0.50','0.50','0.50','0.50',1,1,'2014-02-26 08:32:17',1,133,1),(582,130,1,4,'Mushroom Fried Rice','Mushroom Fried Rice','0.50','0.50','0.50','0.50',1,1,'2014-02-26 08:32:56',1,134,1),(583,130,1,4,'Keema Pilau Rice','Keema Pilau Rice','0.95','0.95','0.95','0.95',1,1,'2014-02-26 08:34:21',1,135,1),(584,130,1,4,'Spicy Keema Pilau Rice','Spicy Keema Pilau Rice','0.95','0.95','0.95','0.95',1,1,'2014-02-26 08:35:16',1,136,1),(585,130,1,4,'Sizzling Rice','Sizzling Rice','0.50','0.50','0.50','0.50',1,1,'2014-02-26 08:36:21',1,137,1),(586,130,1,4,'Chips','Chips.......','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:37:20',1,138,1),(587,130,1,4,'Naan','Naan.......','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:38:14',1,139,1),(588,130,1,4,'Peshwari Nan','Peshwari Nan','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:39:35',1,140,1),(589,130,1,4,'Garlic Nan','Garlic Nan','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:44:00',1,141,1),(590,130,1,4,'Cheese Nan','Cheese Nan','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:44:37',1,142,1),(591,130,1,4,'Keema Nan','Keema Nan...','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:45:38',1,143,1),(592,130,1,4,'Kulcha Nan','Kulcha Nan','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:46:25',1,144,1),(593,130,1,4,'Chilli Cheese Coriander Nan','Chilli Cheese Coriander Nan','0.50','0.50','0.50','0.50',1,1,'2014-02-26 08:47:14',1,145,1),(594,130,1,4,'Chappati','Chappati...','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:47:59',1,146,1),(595,130,1,4,'Tandoori Roti','Tandoori Roti','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:48:41',1,147,1),(596,130,1,4,'Paratha','Paratha...','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:49:14',1,148,1),(597,130,1,4,'Stuffed Paratha','Stuffed Paratha','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:50:09',1,149,1),(598,117,1,7,'Beef Steak','Beef Steak','5.95','5.95','5.95','5.95',1,1,'2014-02-26 08:52:41',1,150,1),(599,117,1,7,'Scampi & Chips','Scampi & Chips','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:53:41',1,151,1),(600,117,1,7,'Chicken Nuggets & Chips','Chicken Nuggets','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:54:26',1,152,1),(601,117,1,7,'Omelette & Chips','Omelette,,','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:55:37',1,153,1),(602,117,1,7,'Fried Chicken & Chips','Fried Chicken','0.00','0.00','0.00','0.00',1,1,'2014-02-26 08:56:30',1,154,1),(603,106,2,4,'Mangolam jous','That is good for health','5.00','5.10','5.05','5.20',1,0,'2014-03-12 05:20:44',1,179,1),(604,88,1,4,'Egg Tost','That is very good for health','8.05','8.20','8.07','8.10',1,1,'2014-03-14 05:22:29',1,180,1),(606,108,3,4,'sweet sponce','Sweet sponce is a very healthy food.','10.00','10.40','10.50','10.20',1,1,'2014-03-14 05:57:56',1,181,1),(607,83,3,4,'palllllll','ertetertetetertrrtrtrtrtrttrt','13.10','13.25','13.05','13.20',1,1,'2014-03-14 12:48:20',1,1,1),(608,83,2,4,'phalllllllllllllll2','This is a phall2 product.','9.00','9.20','9.05','9.10',1,1,'2014-03-14 12:44:35',1,2,1);
/*!40000 ALTER TABLE `res_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_product_ingredient`
--

DROP TABLE IF EXISTS `res_product_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_product_ingredient` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` smallint(6) DEFAULT NULL,
  `ingredient_id` smallint(6) DEFAULT NULL,
  `is_with` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6138D3214584665A` (`product_id`),
  KEY `IDX_6138D321933FE08C` (`ingredient_id`),
  CONSTRAINT `FK_6138D3214584665A` FOREIGN KEY (`product_id`) REFERENCES `res_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_6138D321933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `res_ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2538 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_product_ingredient`
--

LOCK TABLES `res_product_ingredient` WRITE;
/*!40000 ALTER TABLE `res_product_ingredient` DISABLE KEYS */;
INSERT INTO `res_product_ingredient` VALUES (866,4,207,1),(867,4,220,1),(868,4,222,1),(869,4,207,0),(870,4,220,0),(871,4,222,0),(872,20,207,1),(873,20,220,1),(874,20,222,1),(875,20,207,0),(876,20,220,0),(877,20,222,0),(878,20,208,1),(879,20,221,1),(880,20,208,0),(881,20,221,0),(882,20,229,0),(883,20,229,1),(884,20,238,0),(885,20,238,1),(886,19,238,1),(887,19,238,0),(888,19,229,1),(889,19,229,0),(890,19,208,1),(891,19,208,0),(892,19,222,1),(893,19,220,1),(894,19,207,1),(895,19,207,0),(896,19,220,0),(897,19,222,0),(898,5,207,1),(899,5,220,1),(900,5,222,1),(901,5,207,0),(902,5,220,0),(903,5,222,0),(904,5,208,1),(905,5,208,0),(906,5,238,1),(907,5,238,0),(908,17,238,1),(909,17,238,0),(910,17,229,1),(911,17,229,0),(912,17,208,1),(913,17,208,0),(914,17,207,1),(915,17,222,1),(916,17,220,1),(917,17,207,0),(918,17,220,0),(919,17,222,0),(920,2,207,1),(921,2,220,1),(922,2,222,1),(923,2,207,0),(924,2,220,0),(925,2,222,0),(926,2,208,0),(927,2,208,1),(928,2,229,1),(929,2,229,0),(930,2,238,1),(931,2,238,0),(932,15,238,1),(933,15,238,0),(934,15,229,1),(935,15,229,0),(936,15,208,1),(937,15,208,0),(938,15,222,1),(939,15,220,1),(940,15,207,1),(941,15,207,0),(942,15,220,0),(943,15,222,0),(944,3,207,1),(945,3,220,1),(946,3,222,1),(947,3,207,0),(948,3,220,0),(949,3,222,0),(950,3,208,1),(951,3,208,0),(952,3,229,1),(953,3,229,0),(954,3,238,0),(955,22,238,1),(956,22,238,0),(957,22,229,1),(958,22,229,0),(961,22,222,1),(962,22,220,1),(963,22,207,1),(964,22,222,0),(965,22,220,0),(966,22,207,0),(967,10,207,1),(968,10,220,1),(969,10,222,1),(970,10,207,0),(971,10,220,0),(972,10,222,0),(973,10,208,1),(974,10,208,0),(975,10,229,1),(976,10,229,0),(977,10,238,1),(978,10,238,0),(979,13,238,1),(980,13,238,0),(981,13,208,1),(982,13,214,1),(983,13,229,0),(985,13,208,0),(986,13,222,0),(987,13,220,0),(988,13,207,0),(989,13,222,1),(990,13,220,1),(991,13,207,1),(992,9,207,1),(993,9,220,1),(994,9,222,1),(995,9,207,0),(996,9,220,0),(997,9,222,0),(998,9,208,1),(999,9,208,0),(1000,9,229,1),(1001,9,229,0),(1002,9,238,1),(1003,9,238,0),(1005,23,220,0),(1006,23,222,1),(1007,23,222,0),(1008,12,207,1),(1009,12,220,1),(1010,12,222,1),(1011,12,207,0),(1012,12,220,0),(1013,12,222,0),(1015,12,208,0),(1017,12,208,1),(1018,12,229,1),(1019,12,229,0),(1020,12,238,1),(1021,12,238,0),(1022,24,238,1),(1023,24,238,0),(1024,24,229,1),(1025,24,229,0),(1026,24,208,1),(1027,24,208,0),(1028,24,222,1),(1029,24,220,1),(1031,24,207,1),(1032,24,207,0),(1033,24,220,0),(1034,24,222,0),(1035,33,207,1),(1036,33,220,1),(1037,33,222,1),(1038,33,207,0),(1039,33,220,0),(1040,33,222,0),(1041,33,208,1),(1042,33,208,0),(1043,33,238,1),(1044,33,238,0),(1045,35,238,1),(1046,35,238,0),(1047,35,229,1),(1048,35,229,0),(1049,35,208,1),(1050,35,208,0),(1051,35,222,1),(1052,35,220,1),(1053,35,207,1),(1054,35,222,0),(1055,35,220,0),(1056,35,207,0),(1057,34,207,1),(1058,34,220,1),(1059,34,222,1),(1060,34,207,0),(1061,34,220,0),(1062,34,222,0),(1063,34,208,1),(1064,34,208,0),(1065,34,229,1),(1066,34,229,0),(1067,34,238,1),(1068,34,238,0),(1069,32,238,1),(1070,32,238,0),(1071,32,229,1),(1072,32,229,0),(1075,32,208,1),(1076,32,208,0),(1077,32,222,1),(1078,32,220,1),(1079,32,207,1),(1080,32,222,0),(1081,32,220,0),(1082,32,207,0),(1083,28,207,1),(1084,28,220,1),(1085,28,222,1),(1086,28,220,0),(1087,28,207,0),(1088,28,222,0),(1089,28,208,1),(1090,28,208,0),(1091,28,214,0),(1092,28,214,1),(1093,28,229,1),(1094,28,229,0),(1095,28,238,1),(1096,28,238,0),(1098,27,222,1),(1099,27,220,1),(1100,27,207,1),(1101,27,222,0),(1102,27,220,0),(1103,27,207,0),(1104,25,207,1),(1105,25,220,1),(1106,25,222,1),(1107,25,207,0),(1108,25,220,0),(1109,25,222,0),(1110,25,208,1),(1111,25,208,0),(1112,25,214,1),(1113,25,214,0),(1114,25,229,1),(1115,25,229,0),(1116,25,238,1),(1117,25,238,0),(1119,303,238,0),(1120,303,208,1),(1121,303,208,0),(1122,303,222,1),(1123,303,222,0),(1124,303,207,1),(1125,303,207,0),(1126,299,207,1),(1127,299,222,1),(1128,299,207,0),(1129,299,222,0),(1131,299,238,0),(1132,297,238,0),(1133,297,207,1),(1134,297,222,1),(1135,297,207,0),(1136,297,222,0),(1137,300,207,1),(1138,300,222,1),(1139,300,207,0),(1140,300,222,0),(1141,300,238,0),(1142,298,222,1),(1143,298,207,1),(1144,298,238,0),(1145,298,238,1),(1146,298,207,0),(1147,298,222,0),(1148,302,207,1),(1150,302,222,1),(1151,302,207,0),(1152,302,222,0),(1153,302,238,0),(1154,301,238,0),(1155,301,208,0),(1156,301,229,0),(1157,301,222,0),(1158,301,207,0),(1159,301,229,1),(1160,301,208,1),(1161,301,222,1),(1162,301,207,1),(1163,47,207,1),(1164,47,222,1),(1166,47,207,0),(1167,47,220,0),(1168,47,222,0),(1169,47,206,1),(1170,47,224,1),(1171,47,211,1),(1172,47,225,1),(1173,47,219,1),(1174,47,234,1),(1175,47,208,1),(1176,47,209,1),(1177,47,221,1),(1178,47,223,1),(1179,47,238,0),(1180,47,229,1),(1181,47,229,0),(1183,46,207,1),(1185,47,235,1),(1186,46,222,1),(1187,46,207,0),(1188,46,220,0),(1189,46,222,0),(1190,46,206,1),(1191,46,224,1),(1192,46,211,1),(1193,46,225,1),(1195,46,219,1),(1196,46,234,1),(1204,46,208,1),(1205,46,229,0),(1206,46,229,1),(1207,46,242,1),(1209,46,238,0),(1210,46,239,0),(1211,46,226,1),(1212,46,231,1),(1213,46,212,1),(1214,46,223,0),(1216,46,221,1),(1217,46,214,1),(1218,46,209,1),(1219,45,207,1),(1220,45,220,1),(1221,45,222,1),(1222,45,207,0),(1223,45,220,0),(1224,45,222,0),(1225,45,228,1),(1226,45,206,1),(1227,45,224,1),(1228,45,211,1),(1229,45,225,1),(1230,45,219,1),(1231,45,234,1),(1232,45,208,1),(1233,45,209,1),(1234,45,214,1),(1235,45,221,1),(1236,45,223,0),(1237,45,226,1),(1238,45,231,1),(1239,45,212,1),(1240,45,229,1),(1241,45,242,1),(1243,45,229,0),(1244,45,242,0),(1245,45,239,0),(1246,45,238,0),(1247,48,207,1),(1248,48,220,1),(1249,48,222,1),(1250,48,207,0),(1251,48,220,0),(1252,48,222,0),(1253,48,228,1),(1254,48,206,1),(1255,48,224,1),(1256,48,211,1),(1257,48,225,1),(1258,48,219,1),(1259,48,234,1),(1260,48,208,1),(1261,48,209,1),(1262,48,214,1),(1263,48,221,1),(1265,48,223,0),(1266,48,229,0),(1267,48,226,1),(1268,48,231,1),(1269,48,212,1),(1270,48,229,1),(1271,48,242,1),(1273,48,238,1),(1274,48,239,0),(1381,1,207,1),(1382,1,220,1),(1383,1,222,1),(1384,1,207,0),(1385,1,220,0),(1386,1,222,0),(1387,1,208,1),(1388,1,208,0),(1389,1,238,1),(1390,1,238,0),(1392,14,238,0),(1393,14,207,1),(1394,14,220,1),(1395,14,222,1),(1396,14,207,0),(1397,14,216,0),(1398,14,220,0),(1399,14,222,0),(1400,14,208,0),(1401,14,208,1),(1402,14,214,1),(1416,328,221,1),(1417,328,221,0),(1418,328,208,1),(1443,329,207,0),(1444,329,222,0),(1448,18,222,1),(1449,18,207,0),(1450,18,216,0),(1451,18,220,0),(1452,18,222,0),(1453,18,238,0),(1454,6,222,1),(1455,6,238,0),(1456,6,237,0),(1457,6,207,0),(1458,6,220,0),(1459,6,222,0),(1460,6,216,0),(1461,6,214,1),(1462,6,221,1),(1463,6,208,1),(1464,18,208,1),(1465,8,222,1),(1466,8,207,0),(1467,8,216,0),(1468,8,220,0),(1469,8,222,0),(1470,8,208,1),(1472,8,221,1),(1473,8,229,0),(1474,8,238,0),(1475,7,238,0),(1477,7,229,1),(1478,7,222,0),(1479,7,220,0),(1480,7,216,0),(1481,7,207,0),(1482,7,222,1),(1483,7,208,1),(1486,325,221,1),(1487,325,208,1),(1490,1,221,1),(1491,1,227,1),(1492,14,228,1),(1493,6,207,1),(1494,8,207,1),(1495,8,212,1),(1496,7,207,1),(1497,7,238,1),(1498,16,207,1),(1499,16,222,1),(1500,16,207,0),(1501,16,220,0),(1502,16,222,0),(1503,16,237,0),(1504,21,207,1),(1505,21,220,1),(1506,21,222,1),(1509,21,207,0),(1510,21,220,0),(1511,21,222,0),(1512,21,237,0),(1513,4,208,1),(1514,4,238,1),(1515,4,237,0),(1516,20,237,1),(1517,20,237,0),(1518,19,237,0),(1519,5,237,0),(1520,5,221,1),(1521,5,212,1),(1522,17,237,0),(1524,11,207,1),(1525,11,220,1),(1526,11,222,1),(1527,11,208,1),(1528,11,221,1),(1529,11,212,1),(1530,11,237,1),(1531,11,207,0),(1532,11,216,0),(1533,11,220,0),(1534,11,222,0),(1535,11,238,0),(1536,11,237,0),(1537,22,221,1),(1538,22,208,1),(1539,23,237,0),(1540,47,223,0),(1541,46,232,1),(1668,329,207,1),(1669,329,229,1),(1670,328,207,1),(1671,328,207,0),(1672,327,207,1),(1673,327,228,1),(1674,327,235,1),(1675,327,221,1),(1676,327,207,0),(1679,327,229,0),(1680,327,208,1),(1681,327,229,1),(1687,325,229,1),(1688,325,207,1),(1689,325,207,0),(1690,14,228,1),(1691,14,221,1),(1692,14,216,0),(1693,8,228,1),(1694,8,229,1),(1695,6,212,1),(1696,5,228,1),(1697,5,229,1),(1698,4,229,1),(1699,4,229,0),(1700,4,216,0),(1701,4,238,0),(1702,1,244,1),(1703,1,228,1),(1704,1,229,1),(1705,18,227,1),(1706,18,207,1),(1707,18,238,1),(1708,18,237,0),(1709,13,229,1),(1710,13,221,1),(1711,12,237,0),(1712,9,237,0),(1713,24,245,1),(1714,24,237,0),(1715,27,237,0),(1716,26,207,1),(1717,26,207,0),(1718,26,238,0),(1719,26,237,0),(1720,25,221,1),(1721,25,237,0),(1722,48,227,1),(1723,48,232,1),(1724,48,245,1),(1725,47,227,1),(1726,47,228,1),(1727,47,214,1),(1728,46,228,1),(1729,46,227,1),(1730,46,245,1),(1731,45,245,1),(2353,329,208,1),(2354,329,207,1),(2355,329,229,1),(2357,329,222,1),(2358,329,229,0),(2359,329,207,0),(2360,329,222,0),(2361,337,222,1),(2362,337,222,0),(2365,326,240,1),(2368,326,222,1),(2371,326,212,0),(2372,326,221,0),(2374,326,216,0),(2382,326,222,1),(2393,326,222,1),(2415,44,207,1),(2416,44,227,1),(2417,44,247,1),(2418,44,208,1),(2419,44,214,1),(2420,44,246,1),(2421,44,222,1),(2422,44,247,0),(2423,44,207,0),(2424,44,208,0),(2425,44,238,0),(2426,44,246,0),(2427,44,220,0),(2428,44,222,0),(2429,43,247,1),(2430,43,232,1),(2431,43,207,1),(2432,43,238,1),(2433,43,222,1),(2434,43,247,0),(2435,43,207,0),(2436,43,238,0),(2437,43,220,0),(2438,43,222,0),(2439,42,227,1),(2440,42,247,1),(2441,42,207,1),(2442,42,208,1),(2444,42,222,1),(2445,42,247,0),(2446,42,207,0),(2447,42,238,0),(2448,42,222,0),(2449,42,237,1),(2450,41,207,1),(2451,41,238,1),(2452,41,214,1),(2453,41,246,1),(2454,41,222,1),(2455,41,247,0),(2456,41,207,0),(2457,41,214,0),(2458,41,238,0),(2459,41,246,0),(2460,41,222,0),(2461,40,227,1),(2462,40,207,1),(2463,40,252,1),(2464,40,243,1),(2465,40,251,1),(2466,40,249,1),(2467,40,206,1),(2468,40,225,1),(2469,40,226,1),(2470,40,224,1),(2471,40,247,0),(2472,40,229,0),(2473,40,248,1),(2474,40,222,0),(2475,40,238,0),(2476,39,247,1),(2477,39,207,1),(2478,39,238,1),(2479,39,222,1),(2480,39,247,0),(2481,39,207,0),(2482,39,238,0),(2483,39,220,0),(2484,39,222,0),(2485,38,247,1),(2486,38,207,1),(2487,38,238,1),(2488,38,222,1),(2490,38,207,0),(2491,38,238,0),(2492,38,220,0),(2493,38,222,0),(2494,37,207,1),(2495,37,227,1),(2496,37,238,1),(2497,37,222,1),(2498,37,207,0),(2499,37,238,0),(2500,37,222,0),(2501,36,207,1),(2502,36,222,1),(2503,36,207,0),(2504,36,238,0),(2505,36,222,0),(2506,372,207,0),(2507,372,229,0),(2508,372,240,0),(2509,372,208,0),(2510,372,250,0),(2511,372,242,0),(2512,372,248,0),(2513,372,209,0),(2514,372,231,0),(2515,372,232,0),(2516,372,224,0),(2517,372,211,0),(2518,372,225,0),(2519,372,212,0),(2520,372,213,0),(2521,372,214,0),(2522,372,239,0),(2523,372,216,0),(2524,372,238,0),(2525,372,218,0),(2526,372,223,0),(2527,372,245,0),(2528,372,237,0),(2529,372,226,0),(2530,372,219,1),(2531,372,246,1),(2532,372,218,1),(2533,21,238,1),(2534,604,243,1),(2535,604,238,1),(2536,604,236,0),(2537,604,216,0);
/*!40000 ALTER TABLE `res_product_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_product_misc`
--

DROP TABLE IF EXISTS `res_product_misc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_product_misc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_child_id` bigint(20) DEFAULT NULL,
  `quantity_type_id` smallint(6) DEFAULT NULL,
  `product` varchar(500) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `quantity` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F4AAACE93C9A20AA` (`order_child_id`),
  KEY `IDX_F4AAACE936F84596` (`quantity_type_id`),
  CONSTRAINT `FK_F4AAACE936F84596` FOREIGN KEY (`quantity_type_id`) REFERENCES `res_quantity_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_F4AAACE93C9A20AA` FOREIGN KEY (`order_child_id`) REFERENCES `res_order_child` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_product_misc`
--

LOCK TABLES `res_product_misc` WRITE;
/*!40000 ALTER TABLE `res_product_misc` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_product_misc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_product_with_without`
--

DROP TABLE IF EXISTS `res_product_with_without`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_product_with_without` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_child_id` bigint(20) DEFAULT NULL,
  `product_id` smallint(6) DEFAULT NULL,
  `ingredient_id` smallint(6) DEFAULT NULL,
  `quantity_type_id` smallint(6) DEFAULT NULL,
  `is_with` tinyint(1) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `quantity` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C47A60203C9A20AA` (`order_child_id`),
  KEY `IDX_C47A60204584665A` (`product_id`),
  KEY `IDX_C47A6020933FE08C` (`ingredient_id`),
  KEY `IDX_C47A602036F84596` (`quantity_type_id`),
  CONSTRAINT `FK_C47A602036F84596` FOREIGN KEY (`quantity_type_id`) REFERENCES `res_quantity_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_C47A60203C9A20AA` FOREIGN KEY (`order_child_id`) REFERENCES `res_order_child` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_C47A60204584665A` FOREIGN KEY (`product_id`) REFERENCES `res_product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_C47A6020933FE08C` FOREIGN KEY (`ingredient_id`) REFERENCES `res_ingredient` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_product_with_without`
--

LOCK TABLES `res_product_with_without` WRITE;
/*!40000 ALTER TABLE `res_product_with_without` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_product_with_without` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_promo`
--

DROP TABLE IF EXISTS `res_promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_promo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `promo_type` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_promo`
--

LOCK TABLES `res_promo` WRITE;
/*!40000 ALTER TABLE `res_promo` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_quantity_type`
--

DROP TABLE IF EXISTS `res_quantity_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_quantity_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `quantity_type_name` varchar(150) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_98064FAFFF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_98064FAFFF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_quantity_type`
--

LOCK TABLES `res_quantity_type` WRITE;
/*!40000 ALTER TABLE `res_quantity_type` DISABLE KEYS */;
INSERT INTO `res_quantity_type` VALUES (1,4,'Kg',1,'2013-09-30 12:55:15'),(2,4,'Litre',1,'2013-09-30 12:56:12'),(3,4,'ML',1,'2013-09-30 01:33:10'),(4,4,'Gm',1,'2013-09-30 02:48:31');
/*!40000 ALTER TABLE `res_quantity_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_reservation`
--

DROP TABLE IF EXISTS `res_reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_reservation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) DEFAULT NULL,
  `table_id` smallint(6) DEFAULT NULL,
  `reservation_type_id` smallint(6) DEFAULT NULL,
  `deposit_type_id` smallint(6) DEFAULT NULL,
  `promo_id` bigint(20) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `time_from` datetime NOT NULL,
  `time_to` datetime NOT NULL,
  `no_of_guests` smallint(6) NOT NULL,
  `special_instruction` varchar(500) NOT NULL,
  `deposit_amount` decimal(10,2) NOT NULL,
  `reservation_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_11E9A4109395C3F3` (`customer_id`),
  KEY `IDX_11E9A410ECFF285C` (`table_id`),
  KEY `IDX_11E9A410A2D93716` (`reservation_type_id`),
  KEY `IDX_11E9A410C48676C8` (`deposit_type_id`),
  KEY `IDX_11E9A410D0C07AFF` (`promo_id`),
  CONSTRAINT `FK_11E9A4109395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `res_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_11E9A410A2D93716` FOREIGN KEY (`reservation_type_id`) REFERENCES `res_reservation_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_11E9A410C48676C8` FOREIGN KEY (`deposit_type_id`) REFERENCES `res_deposit_type` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_11E9A410D0C07AFF` FOREIGN KEY (`promo_id`) REFERENCES `res_promo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_11E9A410ECFF285C` FOREIGN KEY (`table_id`) REFERENCES `res_table` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_reservation`
--

LOCK TABLES `res_reservation` WRITE;
/*!40000 ALTER TABLE `res_reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_reservation_time`
--

DROP TABLE IF EXISTS `res_reservation_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_reservation_time` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `time_from` datetime NOT NULL,
  `time_to` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_reservation_time`
--

LOCK TABLES `res_reservation_time` WRITE;
/*!40000 ALTER TABLE `res_reservation_time` DISABLE KEYS */;
INSERT INTO `res_reservation_time` VALUES (1,'2013-04-04 11:00:00','2013-04-04 11:30:00'),(2,'2013-04-04 11:30:00','2013-04-04 12:00:00'),(3,'2013-04-04 12:00:00','2013-04-04 12:30:00'),(4,'2013-04-04 12:30:00','2013-04-04 13:00:00'),(5,'2013-04-04 13:00:00','2013-04-04 13:30:00'),(6,'2013-04-04 13:30:00','2013-04-04 14:00:00'),(7,'2013-04-04 14:00:00','2013-04-04 14:30:00'),(8,'2013-04-04 14:30:00','2013-04-04 15:00:00'),(9,'2013-04-04 15:00:00','2013-04-04 15:30:00'),(10,'2013-04-04 15:30:00','2013-04-04 16:00:00'),(11,'2013-04-04 16:00:00','2013-04-04 16:30:00'),(12,'2013-04-04 16:30:00','2013-04-04 17:00:00'),(13,'2013-04-04 17:00:00','2013-04-04 17:30:00'),(14,'2013-04-04 17:30:00','2013-04-04 18:00:00'),(15,'2013-04-04 18:00:00','2013-04-04 18:30:00'),(16,'2013-04-04 18:30:00','2013-04-04 19:00:00'),(17,'2013-04-04 19:00:00','2013-04-04 19:30:00'),(18,'2013-04-04 19:30:00','2013-04-04 20:00:00'),(19,'2013-04-04 20:00:00','2013-04-04 20:30:00'),(20,'2013-04-04 20:30:00','2013-04-04 21:00:00');
/*!40000 ALTER TABLE `res_reservation_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_reservation_type`
--

DROP TABLE IF EXISTS `res_reservation_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_reservation_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `reservation_type` varchar(150) NOT NULL,
  `color` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_reservation_type`
--

LOCK TABLES `res_reservation_type` WRITE;
/*!40000 ALTER TABLE `res_reservation_type` DISABLE KEYS */;
INSERT INTO `res_reservation_type` VALUES (1,'Phone','#FF5E7E4D'),(2,'Web','#FF9D723B'),(3,'Walkin','#FF4E743F');
/*!40000 ALTER TABLE `res_reservation_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_settings`
--

DROP TABLE IF EXISTS `res_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_settings` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `settings_name` varchar(150) NOT NULL,
  `value` varchar(150) NOT NULL,
  `is_flat` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_settings`
--

LOCK TABLES `res_settings` WRITE;
/*!40000 ALTER TABLE `res_settings` DISABLE KEYS */;
INSERT INTO `res_settings` VALUES (1,'tax','20',0),(2,'service_charge','0.00',0),(3,'gratuity','0.00',1),(4,'how_much_loyalty_points_will_construct_1_pound','1',0),(5,'address','44 Market Street, Hednesford, Staffordshire, WS12 1AG, UK',0),(6,'nibssms_api_key','h3UI3thi5YpGB0bfGz4nkEBXBwhtnQYvJI4D1bMX',0),(7,'email_sender_account','nibssms@gmail.com',0),(9,'nibssms_username','bony_036@yahoo.com',0),(10,'email_sender_account_password','Amin1186',0),(11,'nibssms_sender_name','NIBS Restaurant',0),(12,'logo','assets/system_img/yoke_logo.png',0),(13,'RestaurantTown','Hednesford',0),(14,'RestaurantCity','Staffordshire',0),(15,'RestaurantPincode','WS12 1AG',0),(16,'RestaurantCountry','UK',0),(17,'RestaurantCompany','India Red',0),(18,'RestaurantTelephone','01922 454545',0),(19,'RestaurantWebsite','www.nibssolution.com',0),(20,'RestaurantEmail','admin@nibshost.com',0),(21,'RestaurantStreet','44 Market Street',0),(22,'VatNumber','123',0),(23,'footer_a','Footer Line A',1),(24,'footer_b','Footer Line B',1),(25,'footer_dinein','Dine In',0),(26,'footer_takeaway','Take Away Collection',0),(27,'footer_delivery','Delivery',0),(28,'footer_waiting','Waiting',0);
/*!40000 ALTER TABLE `res_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_special_instructions`
--

DROP TABLE IF EXISTS `res_special_instructions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_special_instructions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_child_id` bigint(20) DEFAULT NULL,
  `instruction` varchar(500) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2EBB11AB3C9A20AA` (`order_child_id`),
  CONSTRAINT `FK_2EBB11AB3C9A20AA` FOREIGN KEY (`order_child_id`) REFERENCES `res_order_child` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_special_instructions`
--

LOCK TABLES `res_special_instructions` WRITE;
/*!40000 ALTER TABLE `res_special_instructions` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_special_instructions` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`nibsusers`@`localhost`*/ /*!50003 TRIGGER after_insert_special_instruction AFTER INSERT ON res_special_instructions
FOR EACH ROW
BEGIN
   DECLARE tax float(10,2);
   DECLARE inclusive_tax float(10,2);
   DECLARE exclusive_tax float(10,2);
   DECLARE tax_per tinyint(1);
   DECLARE amount_total float(10,2);
   DECLARE inclusive_amount_total float(10,2);
   DECLARE exclusive_amount_total float(10,2);
   DECLARE sc float(10,2);
   DECLARE sc_per tinyint(1);
   DECLARE orderid bigint(20);

   SET tax=(SELECT value FROM res_settings WHERE settings_name='tax');
   SET inclusive_tax = 0;
   SET exclusive_tax = 0;
   SET tax_per=(SELECT is_flat FROM res_settings WHERE settings_name='tax');

   SET sc=(SELECT value FROM res_settings WHERE settings_name='service_charge');
   SET sc_per=(SELECT is_flat FROM res_settings WHERE settings_name='service_charge');

   SET orderid=(SELECT order_id FROM res_order_child WHERE id=NEW.order_child_id);
   SET amount_total=(SELECT SUM(amount) FROM res_order_child WHERE is_active=1 AND order_id=orderid);

   SET inclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);

   SET exclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(pm.price),0) FROM res_product_misc pm LEFT JOIN res_order_child oc ON oc.id=pm.order_child_id WHERE oc.is_active=1 AND oc.order_id=orderid);

   IF (tax_per=0) THEN
      SET inclusive_tax = inclusive_amount_total - ((inclusive_amount_total*100)/(100+tax));
      SET exclusive_tax = (tax*exclusive_amount_total)/100;
      SET tax = inclusive_tax + exclusive_tax;
   END IF;

   SET amount_total=amount_total+exclusive_tax;

   IF (sc_per=0) THEN
      SET sc = (sc*amount_total)/100;
   END IF;

   SET amount_total = amount_total + sc;

   UPDATE res_order SET tax = tax, service_charge = sc, sub_total = amount_total WHERE id=orderid;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`nibsusers`@`localhost`*/ /*!50003 TRIGGER after_update_special_instruction AFTER UPDATE ON res_special_instructions
FOR EACH ROW
BEGIN
   DECLARE tax float(10,2);
   DECLARE inclusive_tax float(10,2);
   DECLARE exclusive_tax float(10,2);
   DECLARE tax_per tinyint(1);
   DECLARE amount_total float(10,2);
   DECLARE inclusive_amount_total float(10,2);
   DECLARE exclusive_amount_total float(10,2);
   DECLARE sc float(10,2);
   DECLARE sc_per tinyint(1);
   DECLARE orderid bigint(20);

   SET tax=(SELECT value FROM res_settings WHERE settings_name='tax');
   SET inclusive_tax = 0;
   SET exclusive_tax = 0;
   SET tax_per=(SELECT is_flat FROM res_settings WHERE settings_name='tax');

   SET sc=(SELECT value FROM res_settings WHERE settings_name='service_charge');
   SET sc_per=(SELECT is_flat FROM res_settings WHERE settings_name='service_charge');

   SET orderid=(SELECT order_id FROM res_order_child WHERE id=OLD.order_child_id);
   SET amount_total=(SELECT SUM(amount) FROM res_order_child WHERE is_active=1 AND order_id=orderid);

   SET inclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);

   SET exclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(pm.price),0) FROM res_product_misc pm LEFT JOIN res_order_child oc ON oc.id=pm.order_child_id WHERE oc.is_active=1 AND oc.order_id=orderid);

   IF (tax_per=0) THEN
      SET inclusive_tax = inclusive_amount_total - ((inclusive_amount_total*100)/(100+tax));
      SET exclusive_tax = (tax*exclusive_amount_total)/100;
      SET tax = inclusive_tax + exclusive_tax;
   END IF;

   SET amount_total=amount_total+exclusive_tax;

   IF (sc_per=0) THEN
      SET sc = (sc*amount_total)/100;
   END IF;

   SET amount_total = amount_total + sc;

   UPDATE res_order SET tax = tax, service_charge = sc, sub_total = amount_total WHERE id=orderid;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`nibsusers`@`localhost`*/ /*!50003 TRIGGER after_delete_special_instruction AFTER DELETE ON res_special_instructions
FOR EACH ROW
BEGIN
   DECLARE tax float(10,2);
   DECLARE inclusive_tax float(10,2);
   DECLARE exclusive_tax float(10,2);
   DECLARE tax_per tinyint(1);
   DECLARE amount_total float(10,2);
   DECLARE inclusive_amount_total float(10,2);
   DECLARE exclusive_amount_total float(10,2);
   DECLARE sc float(10,2);
   DECLARE sc_per tinyint(1);
   DECLARE orderid bigint(20);

   SET tax=(SELECT value FROM res_settings WHERE settings_name='tax');
   SET inclusive_tax = 0;
   SET exclusive_tax = 0;
   SET tax_per=(SELECT is_flat FROM res_settings WHERE settings_name='tax');

   SET sc=(SELECT value FROM res_settings WHERE settings_name='service_charge');
   SET sc_per=(SELECT is_flat FROM res_settings WHERE settings_name='service_charge');

   SET orderid=(SELECT order_id FROM res_order_child WHERE id=OLD.order_child_id);
   SET amount_total=(SELECT SUM(amount) FROM res_order_child WHERE is_active=1 AND order_id=orderid);

   SET inclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);
   SET inclusive_amount_total=inclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=1 AND p.is_taxable=1);

   SET exclusive_amount_total=(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(oc.amount),0) FROM res_order_child oc RIGHT JOIN res_product_with_without pw ON pw.order_child_id=oc.id LEFT JOIN res_product p ON p.id=pw.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(si.price),0) FROM res_special_instructions si LEFT JOIN res_order_child oc ON si.order_child_id=oc.id LEFT JOIN res_product p ON p.id=oc.product_id WHERE oc.is_active=1 AND oc.order_id=orderid AND p.is_tax_inclusive=0 AND p.is_taxable=1);
   SET exclusive_amount_total=exclusive_amount_total+(SELECT IFNULL(SUM(pm.price),0) FROM res_product_misc pm LEFT JOIN res_order_child oc ON oc.id=pm.order_child_id WHERE oc.is_active=1 AND oc.order_id=orderid);

   IF (tax_per=0) THEN
      SET inclusive_tax = inclusive_amount_total - ((inclusive_amount_total*100)/(100+tax));
      SET exclusive_tax = (tax*exclusive_amount_total)/100;
      SET tax = inclusive_tax + exclusive_tax;
   END IF;

   SET amount_total=amount_total+exclusive_tax;

   IF (sc_per=0) THEN
      SET sc = (sc*amount_total)/100;
   END IF;

   SET amount_total = amount_total + sc;

   UPDATE res_order SET tax = tax, service_charge = sc, sub_total = amount_total WHERE id=orderid;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `res_state`
--

DROP TABLE IF EXISTS `res_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_state` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_state`
--

LOCK TABLES `res_state` WRITE;
/*!40000 ALTER TABLE `res_state` DISABLE KEYS */;
INSERT INTO `res_state` VALUES (1,'Kent'),(2,'Texas'),(3,'Medway'),(4,'Essex'),(5,'Norfolk'),(7,'Rxeed'),(9,'Reeds'),(10,'Walsall'),(11,'london'),(12,'bd'),(13,'ddd'),(14,'fjfjfjfjfff'),(16,'loklok'),(18,'dhaka');
/*!40000 ALTER TABLE `res_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_status`
--

DROP TABLE IF EXISTS `res_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_status` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `status` varchar(150) NOT NULL,
  `top_color` varchar(9) NOT NULL,
  `bottom_color` varchar(9) NOT NULL,
  `is_free` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_status`
--

LOCK TABLES `res_status` WRITE;
/*!40000 ALTER TABLE `res_status` DISABLE KEYS */;
INSERT INTO `res_status` VALUES (1,'Free','#49d470','#13c144',1),(2,'Occupied','#d44949','#c11313',0),(3,'Taking Order','#a1a1a1','#818181',0),(4,'Order Taken','#ffb239','#f69600',0);
/*!40000 ALTER TABLE `res_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_table`
--

DROP TABLE IF EXISTS `res_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_table` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `floor_id` smallint(6) DEFAULT NULL,
  `user_id` smallint(6) DEFAULT NULL,
  `status_id` smallint(6) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `table_number` varchar(45) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  `prep_location_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B00459ED854679E2` (`floor_id`),
  KEY `IDX_B00459EDA76ED395` (`user_id`),
  KEY `IDX_B00459ED6BF700BD` (`status_id`),
  KEY `IDX_B00459EDFF8A180B` (`last_updated_by`),
  KEY `IDX_B00459EDCA85BAE` (`prep_location_id`),
  CONSTRAINT `FK_B00459ED6BF700BD` FOREIGN KEY (`status_id`) REFERENCES `res_status` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_B00459ED854679E2` FOREIGN KEY (`floor_id`) REFERENCES `res_floor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_B00459EDA76ED395` FOREIGN KEY (`user_id`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_B00459EDCA85BAE` FOREIGN KEY (`prep_location_id`) REFERENCES `res_prep_location` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_B00459EDFF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_table`
--

LOCK TABLES `res_table` WRITE;
/*!40000 ALTER TABLE `res_table` DISABLE KEYS */;
INSERT INTO `res_table` VALUES (22,1,2,1,2,'1',1,'2014-03-28 00:56:08',NULL),(24,1,1,1,2,'2',1,'2014-03-27 23:30:56',NULL),(27,2,1,2,2,'8',1,'2014-03-28 16:03:11',NULL),(28,2,1,3,17,'9',1,'2014-03-29 03:40:53',NULL),(30,2,1,1,17,'10',1,'2014-03-28 06:58:06',NULL),(32,2,1,1,17,'11',1,'2014-03-28 07:04:54',NULL),(34,2,4,1,17,'12',1,'2014-03-28 07:07:06',NULL),(35,2,4,1,7,'13',1,'2014-03-11 10:52:04',NULL),(37,2,1,1,4,'14',1,'2014-03-11 10:10:16',NULL),(39,2,1,1,4,'15',1,'2014-03-11 10:03:58',NULL),(43,2,1,1,4,'16',1,'2014-03-11 10:03:59',NULL),(44,2,4,1,17,'17',1,'2014-03-28 06:58:02',NULL),(45,1,2,1,2,'3',1,'2014-03-21 13:15:27',NULL),(47,1,3,1,1,'4',1,'2014-03-20 16:39:29',NULL),(48,1,2,1,2,'5',1,'2014-03-27 23:28:38',NULL),(54,1,1,1,2,'6',1,'2014-03-27 23:27:48',NULL),(55,1,1,1,2,'7',1,'2014-03-11 12:36:46',NULL),(56,4,17,1,17,'301',1,'2014-03-28 13:43:45',NULL),(57,4,17,1,17,'302',1,'2014-03-19 01:45:59',3),(58,4,17,1,17,'303',1,'2014-03-22 05:04:50',3),(59,4,17,1,17,'304',1,'2014-03-24 08:33:30',3),(60,4,17,1,17,'305',1,'2014-03-28 06:58:26',3),(61,5,17,1,17,'401',1,'2014-03-15 11:10:04',3),(62,5,17,1,17,'402',1,'2014-03-15 11:10:07',3),(63,5,17,1,17,'403',1,'2014-03-15 11:10:10',3),(64,5,17,1,17,'404',1,'2014-03-15 11:10:12',3),(65,5,17,1,17,'405',1,'2014-03-15 04:41:04',3),(66,5,17,1,4,'406',1,'2014-03-13 11:39:41',3);
/*!40000 ALTER TABLE `res_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_table_transactions`
--

DROP TABLE IF EXISTS `res_table_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_table_transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `table_id` smallint(6) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `no_of_guests` smallint(6) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `last_update` datetime NOT NULL,
  `user_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C6BB7FF4ECFF285C` (`table_id`),
  KEY `IDX_C6BB7FF49395C3F3` (`customer_id`),
  KEY `IDX_C6BB7FF4FF8A180B` (`last_updated_by`),
  KEY `IDX_C6BB7FF4A76ED395` (`user_id`),
  CONSTRAINT `FK_C6BB7FF49395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `res_customer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_C6BB7FF4A76ED395` FOREIGN KEY (`user_id`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_C6BB7FF4ECFF285C` FOREIGN KEY (`table_id`) REFERENCES `res_table` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_C6BB7FF4FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_table_transactions`
--

LOCK TABLES `res_table_transactions` WRITE;
/*!40000 ALTER TABLE `res_table_transactions` DISABLE KEYS */;
INSERT INTO `res_table_transactions` VALUES (1,56,15,17,2,'2014-03-28 07:59:56','2014-03-28 13:43:47','2014-03-28 13:43:47',17),(2,27,3,2,1,'2014-03-28 16:03:09',NULL,'2014-03-28 16:03:09',NULL),(3,28,NULL,17,2,'2014-03-29 03:40:55',NULL,'2014-03-29 03:40:55',17);
/*!40000 ALTER TABLE `res_table_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_user`
--

DROP TABLE IF EXISTS `res_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_user` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `user_type_id` smallint(6) DEFAULT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `user_name` varchar(150) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C9FB04E69D419299` (`user_type_id`),
  KEY `IDX_C9FB04E6FF8A180B` (`last_updated_by`),
  CONSTRAINT `FK_C9FB04E69D419299` FOREIGN KEY (`user_type_id`) REFERENCES `res_user_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_C9FB04E6FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_user`
--

LOCK TABLES `res_user` WRITE;
/*!40000 ALTER TABLE `res_user` DISABLE KEYS */;
INSERT INTO `res_user` VALUES (1,1,NULL,'ryrtyy','Raj','e26823db27ec1920eeeaaffb5de5ddb0','87786876','raj@gmail.com','122.162.115.155','2014-03-21 15:07:19',1,NULL),(2,2,NULL,NULL,'Naresh Phuloria','5b9020c8478adab95b2251fcfc960e1c',NULL,NULL,'182.68.101.235','2014-03-29 00:51:16',1,NULL),(3,2,NULL,'Bony','Bony Amin','42c4f883800d499f72f8470bff89e083','4654566','bony@bony.com','182.160.103.107','2014-03-15 23:13:24',1,NULL),(4,1,NULL,'abc','Tareq','dd213d69e3ccba0e787ca4ba5d726521','01947967963','a@a.com','182.160.103.107','2014-03-21 10:13:03',1,NULL),(6,2,NULL,'Md1.Rasel Ahmed','Md.Rasel Ahmed',NULL,'019203894','rasel@yahoo.com',NULL,'2014-01-21 08:55:11',1,NULL),(7,2,NULL,NULL,'Waiter','dd213d69e3ccba0e787ca4ba5d726521',NULL,'w@w.com','182.160.103.107','2014-03-27 07:10:18',1,NULL),(8,3,NULL,NULL,'jamal','1f0423be29bd63ea7733c9ae7ee97f7d','3490067575','test@test.com',NULL,'2014-01-04 17:17:48',1,NULL),(9,3,NULL,NULL,'Test1','test1','3490067576','test1@test.com',NULL,NULL,1,NULL),(10,3,NULL,NULL,'Test2','test2','3490067577','test2@test.com',NULL,NULL,1,NULL),(11,3,NULL,NULL,'Test3','test3','3490067578','test3@test.com',NULL,NULL,1,NULL),(12,2,4,NULL,'Test Waiter',NULL,'01911291717','wak@w.com',NULL,NULL,0,'2014-01-24 06:37:54'),(13,4,NULL,NULL,'Special Admin','dd213d69e3ccba0e787ca4ba5d726521',NULL,NULL,'182.160.103.107','2014-03-15 12:06:08',1,NULL),(15,3,4,NULL,'zakir2',NULL,'01934090044','zakir2@yahoo.com',NULL,NULL,0,'2014-03-11 05:37:09'),(16,2,4,NULL,'zakir3',NULL,'01934078900','zakir3@yahoo.com',NULL,NULL,1,'2014-03-11 05:37:38'),(17,2,4,NULL,'Tareq Waiter','dd213d69e3ccba0e787ca4ba5d726521','','tareq_waiter@webkutir.net','182.160.103.107','2014-03-29 03:40:44',1,'2014-03-11 10:54:49');
/*!40000 ALTER TABLE `res_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_user_type`
--

DROP TABLE IF EXISTS `res_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_user_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(150) NOT NULL,
  `top_color` varchar(9) NOT NULL,
  `bottom_color` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_user_type`
--

LOCK TABLES `res_user_type` WRITE;
/*!40000 ALTER TABLE `res_user_type` DISABLE KEYS */;
INSERT INTO `res_user_type` VALUES (1,'Admin','#d60000','#d60000'),(2,'Waiter','#39a4ac','#39a4ac'),(3,'Super Admin','#e09146','#e09146'),(4,'Special','#d60000','#d60000');
/*!40000 ALTER TABLE `res_user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_voucher`
--

DROP TABLE IF EXISTS `res_voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_voucher` (
  `id` bigint(20) NOT NULL,
  `last_updated_by` smallint(6) DEFAULT NULL,
  `voucher_number` varchar(50) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `last_update` datetime NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_til` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_31D1F136FF8A180B` (`last_updated_by`),
  KEY `FK_31D1F136FF8A181B_idx` (`customer_id`),
  CONSTRAINT `FK_31D1F136FF8A180B` FOREIGN KEY (`last_updated_by`) REFERENCES `res_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_31D1F136FF8A181B` FOREIGN KEY (`customer_id`) REFERENCES `res_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_voucher`
--

LOCK TABLES `res_voucher` WRITE;
/*!40000 ALTER TABLE `res_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_voucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_voucher_exclusions`
--

DROP TABLE IF EXISTS `res_voucher_exclusions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_voucher_exclusions` (
  `id` bigint(20) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `exclusive_day_id` varchar(1) DEFAULT NULL,
  `exclusive_order_type_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_voucher_exclusions`
--

LOCK TABLES `res_voucher_exclusions` WRITE;
/*!40000 ALTER TABLE `res_voucher_exclusions` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_voucher_exclusions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-29  3:24:01
