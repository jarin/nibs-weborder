<?php
session_start();

require_once("lib/utilities.php");
require_once("lib/generator.php");

$state=isset($_POST["state"])?$_POST["state"]:"start";
$states=array(
  "start","declarations","processing","final"
);
if($state=="start"){
  if(isset($_GET["state"])){
    $state=$_GET["state"];
  }
}

switch($state){
  case "start":
    $_tmp=array();
    $thisdir="templates/";
    $dh=opendir($thisdir);
    while(false!==($file=readdir($dh))){
      if(is_dir($thisdir.$file)&&substr($file,0,1)!="."){
        $_tmp[]=$file;
      }
    }
?>
<div style="width:960px;margin:0 auto">
<h1>Select Template</h1>
  <form method="post">
    <table>
      <tr>
        <th>Template</th>
        <td>
          <select name="template">
            <option value="">--Please Select--</option>
            <?php
            foreach ($_tmp as $tpl) {
              echo "<option value='$tpl'>$tpl</option>";
            }
            ?>
          </select>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="right"><hr /><input type="submit" value="Use Template"></td>
      </tr>
    </table>
    <input type="hidden" name="state" value="declarations">
  </form>
</div>
<?php
  break;
  case "declarations":
?>
<div style="width:960px;margin:0 auto">
<h1>Template Declarations</h1>
  <form method="post">
    <table>
    <tr>
        <th>Template</th>
        <td><?=$_POST["template"]?></td>
      </tr>
<?php
    if(file_exists("templates/".$_POST["template"]."/"."config.php")){
      require_once("templates/".$_POST["template"]."/"."config.php");
    }
    if(isset($CONFIG) && is_array($CONFIG)){
      foreach ($CONFIG as $k => $v){
        $inp='<input type="text" name="'.$v.'">';
        $k_parts=explode(":",$k);
        if(isset($k_parts[1])){
          $k=$k_parts[0];
          switch($k_parts[1]){
            case "boolean":
              $inp='<label><input type="radio" name="'.$v.'" value="true"> Yes</label>&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio" name="'.$v.'" value="false"> No</label>';
            break;
          }
        }
?>
      <tr>
        <th><?=$k?></th>
        <td><?=$inp?></td>
      </tr>
<?php
    }
  }
?>
     <tr>
        <td colspan="2" align="right"><hr /><input type="submit" value="Build"></td>
      </tr>
    </table>
  <input type="hidden" name="state" value="processing">
  <input type="hidden" name="template" value="<?=$_POST["template"]?>">
  </form>
</div>
<?php
  break;
  case "processing":
    $_temp=array();
    foreach ($_POST as $key => $value){
      if($key!="state" && $key!="template"){
        $_temp[$key]=$value;
      }
    }
    try{
      $gen=new generator($_POST["template"],$_temp);
      $_SESSION["msg"]="Built files for $_POST[base_name] created in the build directory under ".$gen->getBuiltDirectory();
      header("Location: ".$_SERVER['PHP_SELF']."?state=final");
    }catch(Exception $e){
      echo $e->getMessage();
    }
  break;
  case "final":
  ?>
  <div style="width:960px;margin:0 auto">
    <h1>DONE</h1>
    <p><?=$_SESSION["msg"]?></p>
    <p><a href="<?=$_SERVER['PHP_SELF']?>">Start Again</a></p>
  </div>
  <?php
    session_unset();
    session_destroy();
  break;
}
?>