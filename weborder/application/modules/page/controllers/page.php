<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Page Controller
 *
 * @category Controller
 * @package  CodeIgniter
 * @author   Jarin Anika <jarinanika@gmail.com>
 * @license  http://directory.fsf.org/wiki/License:ReciprocalPLv1.3 Reciprocal Public License v1.3
 * @link     http://nibssolutions.com
 */
class Page extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->data['module'] = 'page';
    }
    
	public function index()
	{
		$this->content('1');
	}
	
	public function content($page)
	{
		$this->data['method'] = 'content/'.$page;
		$results = array();
		
		$results = $this->common_m->runSQL("SELECT * FROM app_pages WHERE active=1 AND id=$page ORDER BY sort_order");

		
		if(count($results) > 0){
			$results = $results[0];
			$this->data['page_title'] = $results['name'];
			$this->data['breadcrumb'][ucfirst($results['name'])] = 'page/content/'.$results['id'];
			$this->data['tab'] = $results['id'];
			$this->data['meta_description'] = $results['meta_description'];
			$this->data['meta_key'] = $results['meta_keyword'];
		}
		
		$this->viewPage('cmscontent',array("contents"=>$results));
		
	
	}
	
	
}