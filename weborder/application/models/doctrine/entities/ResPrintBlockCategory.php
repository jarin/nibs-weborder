<?php



namespace entities;

/**
 * ResPrintBlockCategory
 *
 * @Table(name="res_print_block_category")
 * @Entity
 */
class ResPrintBlockCategory
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResPrintBlock
   *
   * @ManyToOne(targetEntity="ResPrintBlock")
   * @JoinColumns({
   *   @JoinColumn(name="print_block_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $printBlock;

  /**
   * @var ResCategory
   *
   * @ManyToOne(targetEntity="ResCategory")
   * @JoinColumns({
   *   @JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $category;

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set printBlock
   *
   * @param ResPrintBlock $printBlock
   */
  public function setPrintBlock(\ResPrintBlock $printBlock)
  {
    $this->printBlock = $printBlock;
  }

  /**
   * Get printBlock
   *
   * @return ResPrintBlock 
   */
  public function getPrintBlock()
  {
    return $this->printBlock;
  }

  /**
   * Set category
   *
   * @param ResCategory $category
   */
  public function setCategory(\ResCategory $category)
  {
    $this->category = $category;
  }

  /**
   * Get category
   *
   * @return ResCategory 
   */
  public function getCategory()
  {
    return $this->category;
  }
}