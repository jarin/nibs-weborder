<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->tablename="app_content";
		$this->versions_tablename="app_content_versions";
		$this->autosave_tablename="app_content_autosave";
	}

	function nextSortOrderValue($parent_id){
		$res=$this->db->query("select max(sort_order) as sort_order from $this->tablename where `parent_id`='$parent_id'")->result_array();
		if(isset($res[0]["sort_order"])){
			return $res[0]["sort_order"]+1;
		}else{
			return 0;
		}
	}

	function getPageStubsById($pageId,$parent_id,$stub){
		$res=$this->db->query("select count(*) as count from $this->tablename where `parent_id`='$parent_id' and `stub`='$stub' and `id`<>'$pageId'")->row_array();
		if(isset($res["count"])){
			return ($res["count"]>0);
		}else{
			return 1;
		}
	}

	function orphanTheChildren($parent_id){
		return @$this->db->update($this->tablename,array("parent_id"=>NULL),array("parent_id"=>$parent_id));

	}

	function createSnapshot($page_id,$newContent){
		if(trim($newContent)!=""){
			$res=$this->get($page_id);
			if(is_array($res) && isset($res["id"])){
				if($res["content"]!=$newContent && trim($res["content"])!=""){
					$arr=array(
						"modifieddate"=>$res["modifieddate"],
						"modifiedby"=>$res["modifiedby"],
						"content"=>$res["content"],
						"page_id"=>$res["id"]
						);
					$res2=$this->db->insert($this->versions_tablename,$arr);
					return array(
						"version_id"=>$this->db->insert_id(),
						"modifieddate"=>$res["modifieddate"],
						"modifiedby"=>$res["modifiedby"]
						);
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	function autosave($page_id,$user_id,$content){
	  $res=$this->getAutoSaved($page_id,$user_id);
	  if(is_array($res) && isset($res["id"])){
	    $arr=array(
  			"modifieddate"=>time(),
  			"content"=>$content
  			);
  		return $this->db->update($this->autosave_tablename,$arr,array("user_id"=>$user_id,"page_id"=>$page_id));
	  }else{
	    $arr=array(
  			"modifieddate"=>time(),
  			"user_id"=>$user_id,
  			"content"=>$content,
  			"page_id"=>$page_id
  			);
  		return $this->db->insert($this->autosave_tablename,$arr);
	  }

	}

	function getAutoSaved($page_id,$user_id){
	  return $this->db->where("page_id",$page_id)
	  ->where("user_id",$user_id)
		->get($this->autosave_tablename)->row_array();
	}

	function deleteAutoSave($page_id,$user_id){
	  return $this->db->where("page_id",$page_id)
	  ->where("user_id",$user_id)
	  ->delete($this->autosave_tablename);
	}

	function deleteAutoSaves($page_id){
	  return $this->db->where("page_id",$page_id)->delete($this->autosave_tablename);
	}

	function getSnapshot($id){
		return $this->db->where("id",$id)
		->get($this->versions_tablename)->result_array();
	}

	function getSnapshots($page_id){
		return $this->db->where("page_id",$page_id)
		->order_by("modifieddate","asc")
		->get($this->versions_tablename)->result_array();
	}

	function deleteSnapshot($id){
		return $this->db->where("id",$id)->delete($this->versions_tablename);
	}

	function deleteSnapshots($page_id){
		return $this->db->where("page_id",$page_id)->delete($this->versions_tablename);
	}

	//-----------------------------------------------
	function _multiSort($tablename,$array){
		if(is_array($array)){
			foreach($array as $order=>$id){
				@$this->db->update($tablename,array("sort_order"=>$order),array("id"=>$id));
			}
			return true;
		}
		return false;
	}


}
?>