<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller {
	
	protected $data=array();
	
	function __construct(){
		parent::__construct();
		
		$this->data['breadcrumb'] =  '';
		$this->data['tab'] = '';
		$this->data['meta_description'] = '';
		$this->data['meta_key'] = '';
		$this->data['page_title'] = 'Order';
		$this->data['module'] = '';
		$this->data['method'] = '';
		
		
		$this->data['restaurant'] = '';
		
			$this->getRestaurant($this->uri->segment(1),$this->uri->segment(2));	
		
		$this->data['logo_path'] = getLogo();
		$this->data['cmspages'] = $this->cmsPages();
		$this->getBusinessInfo();
		$this->social_info();
    	$this->open_close();
  }
  
  function open_close(){
    $b_id=$this->session->userdata('restaurantid');
    $data = $this->common_m->runSQL("SELECT * FROM restaurant WHERE id=$b_id");

    if(count($data) > 0){
      $openClose_info =$data[0];
    } else {
      $openClose_info ='';
    }

    $this->data['openClose_info']= $openClose_info;
  }
  
  function social_info(){
   $b_id=$this->session->userdata('restaurantid');
   $data = $this->common_m->runSQL("SELECT * FROM res_social WHERE restaurant_id=$b_id");
   if(count($data) > 0){
   		$social_info =$data[0];
   } else {
   		$social_info ='';
   }
   
   $this->data['social_info']= $social_info;
  }
  function viewPage($page='content', $data=array()){
  	
  	$this->load->view('header', $this->data);
  	$this->load->view('top_bar', $this->data);
  	$this->load->view($page, $data);
  	$this->load->view('footer');
  }
  
  function cmsPages()
  {
  	$user='';
  	$results = array();
  	
  	if($this->session->userdata('restaurantid') !=''){
  		$users = $this->common_m->runSQL("SELECT user_id FROM restaurant WHERE id='" .$this->session->userdata('restaurantid'). "'");
  		if(count($users) > 0){
  			$user = $users[0]['user_id'];
  		}
  	} else {
  		//load admins pages
  		$users = $this->common_m->runSQL("SELECT id FROM app_users WHERE userlevel='admin'");
  		if(count($users) > 0){
  			foreach ($users as $u){
  				$user[] = $u['id'];
  			}
  		}
  	}
  	if(is_array($user)){
  		$results = $this->common_m->runSQL("SELECT id, name, stub FROM app_pages WHERE active=1 AND created_by IN (" .implode(',', $user). ") ORDER BY sort_order");
  	} else {
  		if(intval($user)){
  			$results = $this->common_m->runSQL("SELECT id, name, stub FROM app_pages WHERE active=1 AND created_by='" .$user. "' ORDER BY sort_order");
  		}
  	}
  	return $results;
  }
  
  /**
   * getBusinessInfo
   *
   * This function will get additional information of restaurant
   *
   * $param none
   * $return array
   */
  function getBusinessInfo(){
  	
  
  	if($this->session->userdata('restaurantid') !=''){
  		$businessAr = $this->common_m->runSQL("SELECT * FROM restaurant WHERE id='" .$this->session->userdata('restaurantid'). "'");
  
  		if(count($businessAr) > 0){
  			if($businessAr[0]['phone'] !=''){
  				$this->data['businessPhone'] = $businessAr[0]['phone'];
  
  			}
  			
  			if($businessAr[0]['land_line'] !=''){
  				$this->data['businessLandPhone'] = $businessAr[0]['land_line'];
  			
  			}
  			
  			if($businessAr[0]['is_reservation'] !=''){
  				$this->data['reservation'] = $businessAr[0]['is_reservation'];
  					
  			}
  			
  			if($businessAr[0]['name'] !=''){
  				$this->session->set_userdata('restaurantname',$businessAr[0]['name']);
  					
  			}
  
  		}
  	}
  	
  }
  
  /**
   * getRestaurant
   *
   * This function will get restaurant information
   *
   * @param string $restaurantName
   * @param string $module
   * @return void
   */
  function getRestaurant($module,$restaurantName){
  	
  	if($module == 'restaurant'){
  		$dataAr = $this->common_m->runSQL("SELECT id FROM restaurant WHERE unique_url='" .$restaurantName. "'");
  		
  		if(count($dataAr) > 0){
  		
  			if($this->session->userdata('restaurantid') !=''){
  				if($this->session->userdata('restaurantid') != $dataAr[0]['id']){
  					$this->session->unset_userdata('cart');
  					$this->session->set_userdata('restaurantid',$dataAr[0]['id']);
  				}
  			} else {
  				$this->session->unset_userdata('cart');
  				$this->session->set_userdata('restaurantid',$dataAr[0]['id']);
  			}
  			$this->data['restaurant'] = $restaurantName;
  		} else {
  			$this->session->unset_userdata('cart');
  			$this->session->unset_userdata('restaurantid');
  		
  			$this->session->set_flashdata('restaurantError','We don\'t have this restaurant');
  			redirect(base_url());
  		
  		}
  	} else {
  		$this->session->unset_userdata('cart');
  		$this->session->unset_userdata('restaurantid');
  	}
  	
  }
  
  
}