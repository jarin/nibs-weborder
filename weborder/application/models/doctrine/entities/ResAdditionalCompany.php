<?php



namespace entities;

/**
 * ResAdditionalCompany
 *
 * @Table(name="res_additional_company")
 * @Entity
 */
class ResAdditionalCompany
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="integer", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $companyName
   *
   * @Column(name="company_name", type="string", length=150, nullable=false)
   */
  private $companyName;
  
  /**
   * @var ResPrinter
   *
   * @ManyToOne(targetEntity="ResPrinter")
   * @JoinColumns({
   *   @JoinColumn(name="printer_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $printer;
  
  /**
   * @var string $companyStreet
   *
   * @Column(name="company_street", type="string", length=255, nullable=false)
   */
  private $companyStreet;
  
  /**
   * @var string $companyCity
   *
   * @Column(name="company_city", type="string", length=255, nullable=false)
   */
  private $companyCity;
  
  /**
   * @var string $companyTown
   *
   * @Column(name="company_town", type="string", length=255, nullable=false)
   */
  private $companyTown;
  
  /**
   * @var string $companyPinCode
   *
   * @Column(name="company_pin_code", type="string", length=255, nullable=false)
   */
  private $companyPinCode;
  
  /**
   * @var string $companyCountry
   *
   * @Column(name="company_country", type="string", length=255, nullable=false)
   */
  private $companyCountry;
  
  /**
   * @var string $companyEmail
   *
   * @Column(name="company_email", type="string", length=255, nullable=false)
   */
  private $companyEmail;
  
  /**
   * @var string $companyPhone
   *
   * @Column(name="company_phone", type="string", length=255, nullable=false)
   */
  private $companyPhone;
  
  /**
   * @var string $companyWebsite
   *
   * @Column(name="company_website", type="string", length=255, nullable=false)
   */
  private $companyWebsite;
  
  /**
   * @var string $logo
   *
   * @Column(name="logo", type="string", length=255, nullable=false)
   */
  private $logo;
  
  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set companyName
   *
   * @param string $companyName
   */
  public function setCompanyName($companyName)
  {
    $this->companyName = $companyName;
  }

  /**
   * Get companyName
   *
   * @return string 
   */
  public function getCompanyName()
  {
    return $this->companyName;
  }
  
  /**
   * Set companyStreet
   *
   * @param string $companyStreet
   */
  public function setCompanyStreet($companyStreet)
  {
  	$this->companyStreet = $companyStreet;
  }
  
  /**
   * Get companyStreet
   *
   * @return string
   */
  public function getCompanyStreet()
  {
  	return $this->companyStreet;
  }
  
  /**
   * Set companyCity
   *
   * @param string $companyCity
   */
  public function setCompanyCity($companyCity)
  {
      $this->companyCity = $companyCity;
  }
  
  /**
   * Get companyCity
   *
   * @return string
   */
  public function getCompanyCity()
  {
      return $this->companyCity;
  }
  
  /**
   * Set companyTown
   *
   * @param string $companyTown
   */
  public function setCompanyTown($companyTown)
  {
      $this->companyTown = $companyTown;
  }
  
  /**
   * Get companyTown
   *
   * @return string
   */
  public function getCompanyTown()
  {
      return $this->companyTown;
  }
  
  /**
   * Set companyPinCode
   *
   * @param string $companyPinCode
   */
  public function setCompanyPinCode($companyPinCode)
  {
      $this->companyPinCode = $companyPinCode;
  }
  
  /**
   * Get companyPinCode
   *
   * @return string
   */
  public function getCompanyPinCode()
  {
      return $this->companyPinCode;
  }
  
  /**
   * Set companyCountry
   *
   * @param string $companyCountry
   */
  public function setCompanyCountry($companyCountry)
  {
      $this->companyCountry = $companyCountry;
  }
  
  /**
   * Get companyCountry
   *
   * @return string
   */
  public function getCompanyCountry()
  {
      return $this->companyCountry;
  }
  
  /**
   * Set companyEmail
   *
   * @param string $companyEmail
   */
  public function setCompanyEmail($companyEmail)
  {
      $this->companyEmail = $companyEmail;
  }
  
  /**
   * Get companyEmail
   *
   * @return string
   */
  public function getCompanyEmail()
  {
      return $this->companyEmail;
  }
  
  /**
   * Set companyPhone
   *
   * @param string $companyPhone
   */
  public function setCompanyPhone($companyPhone)
  {
      $this->companyPhone = $companyPhone;
  }
  
  /**
   * Get companyPhone
   *
   * @return string
   */
  public function getCompanyPhone()
  {
      return $this->companyPhone;
  }
  
  /**
   * Set companyWebsite
   *
   * @param string $companyWebsite
   */
  public function setCompanyWebsite($companyWebsite)
  {
      $this->companyWebsite = $companyWebsite;
  }
  
  /**
   * Get companyWebsite
   *
   * @return string
   */
  public function getCompanyWebsite()
  {
      return $this->companyWebsite;
  }
  
  /**
   * Set logo
   *
   * @param string $logo
   */
  public function setLogo($logo)
  {
  	$this->logo = $logo;
  }
  
  /**
   * Get logo
   *
   * @return string
   */
  public function getLogo()
  {
  	return $this->logo;
  }
  
  /**
   * Set printer
   *
   * @param ResPrinter $printer
   */
  public function setPrinter(\ResPrinter $printer)
  {
      $this->printer = $printer;
  }
  
  /**
   * Get printer
   *
   * @return ResPrinter
   */
  public function getPrinter()
  {
      return $this->printer;
  }
}