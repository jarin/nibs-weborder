<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->data['page_title'] = "Login";
		
	}
	
	public function index(){
		$isLoggedin=$this->session->userdata('userid');
		$uri = $this->input->get('uri');
		if($uri===FALSE){
			$uri = '';
		}
		if($isLoggedin){
			if($data['uri']==''){
				//$data['uri']='dashboard';
			}
			redirect("/".$data['uri'],"location",301);
		}else{
			
		    $this->viewPage('login',array("uri"=>$uri));
		}
	}
	
	public function doLogin(){
	    $data = $this->input->post(NULL, TRUE);
	   
	    if($data['username'] == '' || $data['password'] ==''){
	        if($data['username'] ==''){
	            $this->session->set_flashdata("error", "Please, Enter your Username");
	            if(isset($data['uri']) && $data['uri'] !=''){
	                redirect('/auth/index?uri='.$data['uri']);
	            } else {
	                redirect("/auth");
	            } 
	        }
	        
	        if($data['password'] ==''){
	            $this->session->set_flashdata("error", "Please, Enter your Password");
	            if(isset($data['uri']) && $data['uri'] !=''){
	                redirect('/auth/index?uri='.$data['uri']);
	            } else {
	                redirect("/auth");
	            }
	        }
	    } else {
	        
    	    $user = $this->common_m->getAValue("ResCustomer", "id", array("userName"=>$data['username']));
    	    if($user!==FALSE){
    	        $user = $this->common_m->getBy("ResCustomer", array("password"=>makeEncryptedPassword($data['password']), 'id'=>$user));
    	        if($user!==FALSE){
    	            $user = $user[0];
    	
    	            //Set Session Here
    	            $this->session->set_userdata("userid", $user['id']);
    	            
    	            $this->session->set_userdata("customer_name", $user['customerName']);
    	
    	            //Set IP Address and Last Activity in GMT
    	            $this->load->helper('date');
    	            $result=$this->common_m->update("ResCustomer",array("ipAddress"=>$this->input->ip_address(), "lastActivity"=>mdate("%Y-%m-%d %H:%i:%s",time())),array("id"=>$user['id']));
    	            if(isset($data['uri']) && $data['uri'] !=''){
    	            	if($this->session->userdata('restaurantid') !=''){
    	            		redirect('restaurant/'. $this->data['restaurant']. '/' .$data['uri']);
    	            	} else{
    	            		redirect($data['uri']);
    	            	}
    	               
    	            } else {
    	            	if($this->session->userdata('restaurantid') !=''){
    	            		redirect('restaurant/'. $this->data['restaurant'] .'/order');
    	            	} else{
    	            		redirect("/order");
    	            	}
    	                
    	            }
    	            
    	        }else{
    	            $this->session->set_flashdata("error", "UserName and/or Password is wrong.");
    	            if(isset($data['uri']) && $data['uri'] !=''){
    	            	if($this->session->userdata('restaurantid') !=''){
    	            		redirect('restaurant/'. $this->data['restaurant'] .'/auth/index?uri='.$data['uri']);
    	            	} else{
    	            		redirect('/auth/index?uri='.$data['uri']);
    	            	}
    	                
    	            } else {
    	            	if($this->session->userdata('restaurantid') !=''){
    	            		redirect('restaurant/'. $this->data['restaurant'] ."/auth");
    	            	} else {
    	            		redirect("/auth");
    	            	}
    	                
    	            }
    	        }
    	    }else{
    	        $this->session->set_flashdata("error", "You are not a Valid Customer of this system.");
    	        if(isset($data['uri']) && $data['uri'] !=''){
    	        	if($this->session->userdata('restaurantid') !=''){
    	        		redirect('restaurant/'. $this->data['restaurant'] .'/auth/index?uri='.$data['uri']);
    	        	} else {
    	        		redirect('/auth/index?uri='.$data['uri']);
    	        	}
    	                
    	          } else {
    	          	if($this->session->userdata('restaurantid') !=''){
    	          		redirect('restaurant/'. $this->data['restaurant'] ."/auth");
    	          	} else{
    	          		redirect("/auth");
    	          	} 
    	          }
    	    }
	    }
	}
	
	public function logout()
	{
	    $this->session->unset_userdata("userid");
	     
	    $this->session->unset_userdata("customer_name");
	    if($this->session->userdata('restaurantid') !=''){
	    	redirect('restaurant/'. $this->data['restaurant'] ."/order");
	    } else {
	    	redirect("order");
	    }
	    
	}
	
}