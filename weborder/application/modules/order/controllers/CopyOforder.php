<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Order Controller
 *
 * @category Controller
 * @package  CodeIgniter
 * @author   Jarin Anika <jarinanika@gmail.com>
 * @license  http://directory.fsf.org/wiki/License:ReciprocalPLv1.3 Reciprocal Public License v1.3
 * @link     http://nibssolutions.com
 */
class Order extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->data['page_title'] = "Order";
        $this->data['limit'] = 20;
       
    }
    
    /*public function index()
    {
        $this->orderer();
    }*/
    
	public function index($categoryId='', $subCategoryId='')
	{
	    $data = $this->input->post(NULL, TRUE);
	   
	 if(isset($subCategoryId) && $subCategoryId !=''){
	     
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$subCategoryId." OR j.parent=".$subCategoryId.")  ORDER BY j.parentId, j.sequenceNumber, t.sequenceNumber" , "", array('offset'=>0,'limit'=>$this->data['limit']));
	      $count = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$subCategoryId." OR j.parent=".$subCategoryId.") ");
	      $count = count($count);
	 }elseif(isset($categoryId) && $categoryId !=''){
	     
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$categoryId." OR j.parent=".$categoryId.")  ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>0,'limit'=>$this->data['limit']));
	      $count = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$categoryId." OR j.parent=".$categoryId.") ");
	      $count = count($count);
	  }elseif($data){
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.categoryName LIKE '".$data['search']."' OR t.productName LIKE '%".$data['search']."%') ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>0,'limit'=>$this->data['limit']));
	      $count = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.categoryName LIKE '".$data['search']."' OR t.productName LIKE '%".$data['search']."%') ");
	      $count = count($count);
	  } else {
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES."  ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>0,'limit'=>$this->data['limit']));
	      $count = $this->common_m->countRows('ResProduct', array("isActive"=>ANSWER_YES));
	  }
	   $categories = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.parentId IS NULL AND t.isActive=".ANSWER_YES." ORDER BY t.parentId, t.sequenceNumber");
	    foreach ($categories as $k => $v) {
	        $categories[$k]['childCats'] = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.isActive=" . ACTIVE_TABLE . " AND t.parent=" . $categories[$k]['id'] . " ORDER BY t.parentId, t.sequenceNumber");
	    }
	       //echo "<pre>";print_r($items);echo '</pre>';
	       //echo $count;
	        $this->viewPage('order',array("items"=>$items, 'count'=>$count,'categories'=>$categories));
	    
	    
	}
	
	public function getNextItem()
	{
	    $data = $this->input->post(NULL, TRUE);
	    //print_r($data);
	    if($data['page'] > 0){
	        $offset = $data['data'] - $this->data['limit'];
	        
	    } else {
	         $offset = 0;
	    }
	    
	     $limit = $data['data'];
	   //echo $limit.'<br/>'.$offset;
	     if(isset($data['subCategoryId']) && $data['subCategoryId'] !=0){
	         $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$data['subCategoryId']." OR j.parent=".$data['subCategoryId'].")  ORDER BY j.parentId, j.sequenceNumber, t.sequenceNumber" , "", array('offset'=>$offset,'limit'=>$limit));
	     }elseif(isset($data['categoryId']) && $data['categoryId'] !=0){
	         $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$data['categoryId']." OR j.parent=".$data['categoryId'].")  ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>$offset,'limit'=>$limit));
	     }elseif($data['search'] && $data['search'] !=''){
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.categoryName LIKE '%".$data['search']."%' OR t.productName LIKE '%".$data['search']."%') ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>$offset,'limit'=>$limit));
	      
	      } else {
	         $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES."  ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>$offset,'limit'=>$limit));
	     }
	     
	    $this->load->view('nextItems',array("items"=>$items,"limit"=>$limit, "offset"=>$offset));
	}
	
	public function getIngredients(){
	    $data = $this->input->post(NULL, TRUE);
	    $with = array();
	    $without = array();
	
	    $items = $this->common_m->getByDql("SELECT t, j FROM entities\\ResProductIngredient t LEFT JOIN t.ingredient j WHERE t.product=".$data['pid']." AND j.isActive=".ACTIVE_TABLE." ORDER BY t.isWith DESC, j.ingredientName");
	    if(count($items)>0){
	        foreach($items as $k=>$v){
	            if($v['isWith']  ){
	                $with[] = $v['ingredient'];
	            }else{
	                $without[] = $v['ingredient'];  
	            }
	        }
	    }
	   //echo '<pre>';print_r($without);echo '</pre>';
	    $this->load->view('ingredients',array("ingredient"=>$items, "with"=>$with, "without"=>$without, "productid"=>$data['pid']));
	}
	
	public function addCart()
	{
	    $data = $this->input->post(NULL, TRUE);
	   
	   
	    if($data){
	        $total = $data['price'] * $data['qty'];
	        $cartNew[] = array(
	            'product_id'=>$data['pid'],
	            'product_name'=>$data['name'],
	            'quantity'=>$data['qty'],
	            'price'=>$data['price'],
	            'total_price'=>$total,
	            'ingredients' =>array()
	        );
	        if($this->session->userdata('cart') !=''){
	            $found = false;
	            foreach ($this->session->userdata('cart') as $cartitem){
	                if($cartitem['ingredients']){
	                    $ingred = $cartitem['ingredients'];
	                } else {
	                    $ingred = array();
	                }
	               if($cartitem['product_id'] == $data['pid']){
	                   $cartAr[] = array(
	                       'product_id'=>$cartitem['product_id'],
	                       'product_name'=>$cartitem['product_name'],
            	           'quantity'=>($cartitem['quantity']+$data['qty']),
            	           'price'=>$cartitem['price'],
            	           'total_price'=>($cartitem['total_price']+$total),
	                       'ingredients' =>$ingred
	                       );
	                   $found = true;
	                   
	               } else {
	                   
	                   $cartAr[] = array(
	                     'product_id'=>$cartitem['product_id'],
	                     'product_name'=>$cartitem['product_name'],
        	             'quantity'=>$cartitem['quantity'],
        	             'price'=>$cartitem['price'],
        	             'total_price'=>$cartitem['total_price'],
	                       'ingredients' =>$ingred
                       ); 
	               }
	            }
	            
	            if($found == false){
	                $this->session->set_userdata('cart',array_merge($cartNew, $cartAr));
	            } else {
	                $this->session->set_userdata('cart',$cartAr);
	            }
	        } else {
	            $this->session->set_userdata('cart',$cartNew);
	        }
	        
	        $this->load->view('cart');
	    }
	    
	}
    
	public function removeCart()
	{
	    $data = $this->input->post(NULL, TRUE);
	    
	    if($data){
	        if($this->session->userdata('cart') !=''){
	            $cartAr = '';
	            foreach ($this->session->userdata('cart') as $cartitem){
	                if($cartitem['ingredients']){
	                    $ingred = $cartitem['ingredients'];
	                } else {
	                    $ingred = array();
	                }
	                if($cartitem['product_id'] != $data['pid']){
	                    $cartAr[] = array(
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>$ingred
	                    );
	                   
	                
	                }
	            }
	            $this->session->set_userdata('cart',$cartAr);
	            
	        }
	    }
	    $this->load->view('cart');
	}
	
	public function addIngredient()
	{
	    $data = $this->input->post(NULL, TRUE);
	    if($data){
	        $product = $this->common_m->getBy('ResProduct', array('id'=>$data['productid']));
	        $with = array();
	        $without = array();
	        $inst = array();
	       if(isset($data['withoutingredient'])){
	           $without = $data['withoutingredient'];
	       }
	       if(isset($data['withingredient'])){
	           $with = $data['withingredient'];
	       }
	       if(isset($data['additionalNote'])){
	           $inst = $data['additionalNote'];
	       }
	        $cartNew[] = array(
	            'product_id'=>$data['productid'],
	            'product_name'=>$product[0]['productName'],
	            'quantity'=>"1",
	            'price'=>$product[0]['webOrderPrice'],
	            'total_price'=>$product[0]['webOrderPrice'],
	            'ingredients' =>array('without'=>$without,'with'=>$with,'specialInstruction'=>$inst)
	        );
	        
	        if($this->session->userdata('cart') !=''){
	            $found = false;
	            foreach ($this->session->userdata('cart') as $cartitem){
	                 
	                if($cartitem['product_id'] == $data['productid']){
	                    $found = true;
	                    $cartAr[] = array(
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>array('without'=>$without,'with'=>$with,'specialInstruction'=>$inst)
	                    );
	                } else {
	                    if($cartitem['ingredients']){
	                        $ingred = $cartitem['ingredients'];
	                    } else {
	                        $ingred = array();
	                    }
	                    $cartAr[] = array(
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>$ingred
	                    );
	                }
	                
	                
	            }
	             
	            if($found == false){
	                $this->session->set_userdata('cart',array_merge($cartNew, $cartAr));
	                
	            } else {
	                $this->session->set_userdata('cart',$cartAr);
	            }
	        } else {
	            $this->session->set_userdata('cart',$cartNew);
	        }
	        $this->load->view('cart');
	    }
	}
	
	public function removeIngredient()
	{
	    $data = $this->input->post(NULL, TRUE);
	    if($data){
	        if($this->session->userdata('cart') !=''){
	           
	            foreach ($this->session->userdata('cart') as $cartitem){
	        
	                if($cartitem['product_id'] == $data['pid']){
	                   
	                    $cartAr[] = array(
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>array()
	                    );
	                } else {
	                    if($cartitem['ingredients']){
	                        $ingred = $cartitem['ingredients'];
	                    } else {
	                        $ingred = array();
	                    }
	                    $cartAr[] = array(
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>$ingred
	                    );
	                }
	                 
	                 
	            }
	        
	            $this->session->set_userdata('cart',$cartAr);
	            
	        }
	       
	    }
	}
	
	public function editCartItem()
	{
	    $data = $this->input->post(NULL, TRUE);
	    if($data){
	        $with = array();
	        $without = array();
	        
	        $items = $this->common_m->getByDql("SELECT t, j FROM entities\\ResProductIngredient t LEFT JOIN t.ingredient j WHERE t.product=".$data['pid']." AND j.isActive=".ACTIVE_TABLE." ORDER BY t.isWith DESC, j.ingredientName");
	        if(count($items)>0){
	            foreach($items as $k=>$v){
	                if($v['isWith']  ){
	                    $with[] = $v['ingredient'];
	                }else{
	                    $without[] = $v['ingredient'];
	                }
	            }
	        }
	        $this->load->view('editItems',array("ingredient"=>$items, "with"=>$with, "without"=>$without, "productid"=>$data['pid']));
	    }
	}
	
	public function updateCartItem()
	{
	    $data = $this->input->post(NULL, TRUE);
	    if($data){
	        
	        $with = array();
	        $without = array();
	        $inst = array();
	       if(isset($data['withoutingredient'])){
	           $without = $data['withoutingredient'];
	       }
	       if(isset($data['withingredient'])){
	           $with = $data['withingredient'];
	       }
	       if(isset($data['additionalNote'])){
	           $inst = $data['additionalNote'];
	       }
	        $cartNew[] = array(
	            'product_id'=>$data['productid'],
	            'product_name'=>$data['productname'],
	            'quantity'=>$data['productQty'],
	            'price'=>$data['productprice'],
	            'total_price'=>($data['productprice']*$data['productQty']),
	            'ingredients' =>array('without'=>$without,'with'=>$with,'specialInstruction'=>$inst)
	        );
	        
	        if($this->session->userdata('cart') !=''){
	            $found = false;
	            foreach ($this->session->userdata('cart') as $cartitem){
	                 
	                if($cartitem['product_id'] == $data['productid']){
	                    $found = true;
	                    $cartAr[] = array(
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$data['productQty'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>($cartitem['price']*$data['productQty']),
	                        'ingredients' =>array('without'=>$without,'with'=>$with,'specialInstruction'=>$inst)
	                    );
	                } else {
	                    if($cartitem['ingredients']){
	                        $ingred = $cartitem['ingredients'];
	                    } else {
	                        $ingred = array();
	                    }
	                    $cartAr[] = array(
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>$ingred
	                    );
	                }
	                
	                
	            }
	             
	            if($found == false){
	                $this->session->set_userdata('cart',array_merge($cartNew, $cartAr));
	                
	            } else {
	                $this->session->set_userdata('cart',$cartAr);
	            }
	        } else {
	            $this->session->set_userdata('cart',$cartNew);
	        }
	        
	    }
	   
	    $this->load->view('cart');
	}
	
	public function confirm()
	{
	    $categories = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.parentId IS NULL AND t.isActive=".ANSWER_YES." ORDER BY t.parentId, t.sequenceNumber");
	    foreach ($categories as $k => $v) {
	        $categories[$k]['childCats'] = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.isActive=" . ACTIVE_TABLE . " AND t.parent=" . $categories[$k]['id'] . " ORDER BY t.parentId, t.sequenceNumber");
	    }
	    $cart = array();
	   
	    if($this->session->userdata('cart') !=''){
	        
	        foreach ($this->session->userdata('cart') as $cartItem){
	            $without= array();
	            $with =array();
	            $instruction = '';
	            $des = $this->common_m->getAValue('ResProduct','description',array('id'=>$cartItem['product_id']));
	            if(count($cartItem['ingredients']) > 0){
	                if(isset($cartItem['ingredients']['without']) && count($cartItem['ingredients']['without']) > 0){
	                    $without = $this->common_m->getByDql("SELECT t.ingredientName FROM entities\\ResIngredient t WHERE t.id IN(".implode(',',$cartItem['ingredients']['without']).") ");
	                     
	                }
	                if(isset($cartItem['ingredients']['with']) && count($cartItem['ingredients']['with']) >0){
	                    $with = $this->common_m->getByDql("SELECT t.ingredientName FROM entities\\ResIngredient t WHERE t.id IN(".implode(',',$cartItem['ingredients']['with']).") ");
	                
	                }
	                if(isset($cartItem['ingredients']['specialInstruction'])){
	                    $instruction = $cartItem['ingredients']['specialInstruction'];
	                }
	            }
	            $cart[] = array(
	                'product_id'=>$cartItem['product_id'],
	                'product_name'=>$cartItem['product_name'],
	                'quantity'=>$cartItem['quantity'],
	                'price'=>$cartItem['price'],
	                'total_price'=>$cartItem['total_price'],
	                'description' =>$des,
	                'without'=>$without,
	                'with'=>$with,
	                'instruction' =>$instruction
	            );
	        }
	    }
	    //print_r($this->session->userdata('cart'));
	    //print_r($cart);
	    $this->viewPage('confirmorder',array('categories'=>$categories,'cartItems'=>$cart));
	}
	
	public function checkout()
	{
	    $uid = $this->session->userdata("userid");
	    if(!isset($uid) || !is_numeric($uid)){
	        
	        redirect("/auth/index?uri=order/checkout");
	    } else {
	        $categories = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.parentId IS NULL AND t.isActive=".ANSWER_YES." ORDER BY t.parentId, t.sequenceNumber");
	        foreach ($categories as $k => $v) {
	            $categories[$k]['childCats'] = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.isActive=" . ACTIVE_TABLE . " AND t.parent=" . $categories[$k]['id'] . " ORDER BY t.parentId, t.sequenceNumber");
	        }
	        $user = $this->common_m->getByDql("SELECT t, to, s, c FROM entities\\ResCustomer t LEFT JOIN t.town to LEFT JOIN t.state s LEFT JOIN t.country c WHERE t.id = '".$uid."' ORDER BY t.customerKey");
	       //print_r($user);
	       
	        $this->viewPage('checkout',array('categories'=>$categories,'user'=>$user[0]));
	    }
	    
	}
	
	public function confirmorder()
	{
	    
	    $data = $this->input->post(NULL, TRUE);
	    print_r($data);exit;
	    if($data){
	        $order_id = $this->_makeOrder($data);
	        $this->editCustomer($data);
	        // add order item, ingredient, add-on
	    }
	}
	
	private function _makeOrder($data)
	{
	    if(!isset($data['date'])){
	        $time = time();
	        $data['date'] = mdate("%Y-%m-%d", $time);
	        $data['time'] = mdate("%H:%i:%s", $time);
	    }
	    $orderTypeId = $this->common_m->getAValue("ResOrderType", "id", array("orderType"=>$data['type']));
	    $statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Taking Order'));
	    if (isset($data['customer_id'])) {
	        $result = $this->common_m->insert("ResOrder", array("lastUpdate" => mdate("%Y-%m-%d %H:%i:%s"), "orderDate" => $data['date'].' '.$data['time'], "isConvertedToTakeAway"=>($data['type']!='Dine In' ? ANSWER_YES : ANSWER_NO), "isActive"=>ANSWER_YES, "tax"=>0, "serviceCharge"=>0, "gratuity"=>0, "subTotal"=>0, "total"=>0, "isPartPayment"=>ANSWER_NO, "isWriteOff"=>ANSWER_NO, "customerId"=>$data['customer_id'], "orderTypeId"=>$orderTypeId, "isPreOrder"=>ANSWER_YES, "statusId"=>$statusId, "isComingFromLocalWeb"=>ANSWER_NO));
	    } else {
	        $result = $this->common_m->insert("ResOrder", array("lastUpdate" => mdate("%Y-%m-%d %H:%i:%s"), "orderDate" => $data['date'].' '.$data['time'], "isConvertedToTakeAway"=>($data['type']!='Dine In' ? ANSWER_YES : ANSWER_NO), "isActive"=>ANSWER_YES, "tax"=>0, "serviceCharge"=>0, "gratuity"=>0, "subTotal"=>0, "total"=>0, "isPartPayment"=>ANSWER_NO, "isWriteOff"=>ANSWER_NO, "orderTypeId"=>$orderTypeId, "isPreOrder"=>ANSWER_YES, "statusId"=>$statusId, "isComingFromLocalWeb"=>ANSWER_NO));
	    }
	    $order_id = $result;
	    return $order_id;
	}
	
	public function editCustomer($data){
	    
	    $ary = $data;
	
	    if($data['town']==''){
	        $data['town']='NULL';
	    }else{
	        $town = $this->common_m->getAValue("ResCity", "id", array("cityName"=>$data['town']));
	        if($town===FALSE){
	            $result = $this->common_m->insert("ResCity", array("cityName"=>$data['town']));
	            $data['town'] = $result;
	        }else{
	            $data['town'] = $town;
	        }
	    }
	
	    if($data['state']==''){
	        $data['country']='NULL';
	    }else{
	        $state = $this->common_m->getAValue("ResState", "id", array("stateName"=>$data['state']));
	        if($state===FALSE){
	            $result = $this->common_m->insert("ResState", array("stateName"=>$data['state']));
	            $data['country'] = $result;
	        }else{
	            $data['country'] = $state;
	        }
	    }
	
	    if($data['country_default']==''){
	        $data['country_default']=1;
	    }else{
	        $country = $this->common_m->getAValue("ResCountry", "id", array("countryName"=>$data['country_default']));
	        if($country===FALSE){
	            $result = $this->common_m->insert("ResCountry", array("countryName"=>$data['country_default']));
	            $data['country_default'] = $result;
	        }else{
	            $data['country_default'] = $country;
	        }
	    }
	
	    foreach($data as $k=>$v){
	        if(!in_array($k, array("town", "country", "country_default"))){
	            if($v==''){
	                $data[$k] = 'NULL';
	            }else{
	                $data[$k]="'".$v."'";
	            }
	        }
	    }
	    if(isset($data['name'])){
	        $data['name']=$data['name'];
	    }
	    if($data['name']!='NULL'){
	        $address1 = $this->common_m->getAValue("ResSettings", "value", array("settingsName"=>"address"));
	        $address2 = "";
	        if($data['house_no']!='NULL'){
	            $address2 .= $ary['house_no'].',';
	        }
	        if($data['house_name']!='NULL'){
	            $address2 .= $ary['house_name'].',';
	        }
	        if($data['street']!='NULL'){
	            $address2 .= $ary['street'].',';
	        }
	        if($ary['town']!=''){
	            $address2 .= $ary['town'].',';
	        }
	        if($ary['country']!=''){
	            $address2 .= $ary['country'].',';
	        }
	        if($data['post_code']!='NULL'){
	            $address2 .= $ary['post_code'].',';
	        }
	        if($ary['country_default']!=''){
	            $address2 .= $ary['country_default'].',';
	        }
	
	        if($address2!=''){
	            $address2 = substr($address2, 0, strlen($address2)-1);
	            $data['dist'] = number_format(_calculateDrivingDistanceFromAddress($address1, $address2),1).'miles';
	
	            $dql = "UPDATE entities\\ResCustomer t SET t.town=".$data['town'].", t.country=".$data['country_default'].", t.state=".$data['country'].", t.postCode=".$data['postCode'].", t.email=".$data['email'].", t.customerName=".$data['name'].", t.houseNo=".$data['house_no'].", t.houseName=".$data['house_name'].", t.street=".$data['street'].", t.mobile=".$data['phone'].", t.dist='".$data['dist']."' WHERE t.id=".$data['customer_id'];
	            $result = $this->common_m->getByDql($dql);
	            return true;
	        }else{
	            return false;
	        }
	    }else{
	        return false;
	    }
	}
	
}
