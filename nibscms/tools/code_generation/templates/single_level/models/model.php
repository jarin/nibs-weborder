<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class <#=ucfirst(plural($base_name))#>_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->tablename="<#=$db_table#>";
		<#if($tinymce && $autosave){#>$this->autosave_tablename="<#=$db_table#>_autosave";<#}#>
	}
<#if($tinymce && $autosave){#>
	function autosave($<#=$base_name#>_id,$user_id,$content){
	  $res=$this->getAutoSaved($<#=$base_name#>_id,$user_id);
	  if(is_array($res) && isset($res["id"])){
	    $arr=array(
  			"modifieddate"=>time(),
  			"content"=>$content
  			);
  		return $this->db->update($this->autosave_tablename,$arr,array("user_id"=>$user_id,"<#=$base_name#>_id"=>$<#=$base_name#>_id));
	  }else{
	    $arr=array(
  			"modifieddate"=>time(),
  			"user_id"=>$user_id,
  			"content"=>$content,
  			"<#=$base_name#>_id"=>$<#=$base_name#>_id
  			);
  		return $this->db->insert($this->autosave_tablename,$arr);
	  }

	}

	function getAutoSaved($<#=$base_name#>_id,$user_id){
	  return $this->db->where("<#=$base_name#>_id",$<#=$base_name#>_id)
	  ->where("user_id",$user_id)
		->get($this->autosave_tablename)->row_array();
	}

	function deleteAutoSave($<#=$base_name#>_id,$user_id){
	  return $this->db->where("<#=$base_name#>_id",$<#=$base_name#>_id)
	  ->where("user_id",$user_id)
	  ->delete($this->autosave_tablename);
	}

	function deleteAutoSaves($<#=$base_name#>_id){
	  return $this->db->where("<#=$base_name#>_id",$<#=$base_name#>_id)->delete($this->autosave_tablename);
	}
<#}#>

}
?>