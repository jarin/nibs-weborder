<?php



namespace entities;

/**
 * ResButtonPermissionSettings
 *
 * @Table(name="res_button_permission_settings")
 * @Entity
 */
class ResButtonPermissionSettings
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $value
   *
   * @Column(name="value", type="array", nullable=false)
   */
  private $value;

  /**
   * @var ResButtonPermissionType
   *
   * @ManyToOne(targetEntity="ResButtonPermissionType")
   * @JoinColumns({
   *   @JoinColumn(name="permission_type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $permissionType;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $user;


  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set value
   *
   * @param string $value
   */
  public function setValue($value)
  {
    $this->value = $value;
  }

  /**
   * Get value
   *
   * @return string 
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set permissionType
   *
   * @param ResUser $permissionType
   */
  public function setPermissionType(\ResButtonPermissionType $permissionType)
  {
    $this->permissionType = $permissionType;
  }

  /**
   * Get permissionType
   *
   * @return ResButtonPermissionType 
   */
  public function getPermissionType()
  {
    return $this->permissionType;
  }
  
  /**
   * Set user
   *
   * @param ResUser $user
   */
  public function setUser(\ResUser $user)
  {
  	$this->user = $user;
  }
  
  /**
   * Get user
   *
   * @return ResUser
   */
  public function getUser()
  {
  	return $this->user;
  }
}