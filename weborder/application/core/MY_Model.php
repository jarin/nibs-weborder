<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {
	
	public $entity = 'entities\\';
	
	//Doctrine EntityManager
	public $em;
	
	//Doctrine Returns
	/*
	 * getResult();
	* getSingleResult();
	* getArrayResult();
	* getScalarResult();
	* getSingleScalarResult();
	*
	*/
	
	function __construct(){
		parent::__construct();
		$CI =& get_instance();
		
		//Instantiate a Doctrine Entity Manager
		$this->em = $CI->doctrine->em;
  }
  
  function countRows($entity, $whereAndAry='', $whereOrAry=''){
  	$entity = $this->entity.$entity;
  	$dql = "SELECT COUNT(t.id) AS cnt FROM $entity t";
  	if($whereAndAry!='' || $whereOrAry!=''){
  		$dql .= __parseWhere($whereAndAry, $whereOrAry);
  	}
  	$query = $this->em->createQuery($dql);
  	$ary=$query->getArrayResult();
  	return $ary[0]['cnt'];
  }
  
  function nextSortOrderValue($entity,$field,$whereAndAry='',$whereOrAry=''){
  	$entity = $this->entity.$entity;
  	$dql = "SELECT MAX(t.$field) AS sortOrder FROM $entity t";
  	if($whereAndAry!='' || $whereOrAry!=''){
  		$dql .= __parseWhere($whereAndAry, $whereOrAry);
  	}
  	$query = $this->em->createQuery($dql);
  	$result = $query->getArrayResult();
  	return $result[0]['sortOrder']+1;
  }
  
  function getAll($entity,$orderAry=""){
  	$entity = $this->entity.$entity;
  	$dql = "SELECT t FROM $entity t";
  	if(is_array($orderAry)){
  		$dql .= __parseOrderBy($orderAry);
  	}
  	$query = $this->em->createQuery($dql);
  	return $query->getArrayResult();
  }
  
  function getActive($entity,$orderAry="",$limit=""){
  	$entity = $this->entity.$entity;
  	$dql = "SELECT t FROM $entity t WHERE t.isActive=1";
  	if(is_array($orderAry)){
  		$dql .= __parseOrderBy($orderAry);
  	}
  	$query = $this->em->createQuery($dql);
  	if(is_array($limit)){
  		$query->setFirstResult($limit['offset']);
  		$query->setMaxResults($limit['limit']);
  	}
  	return $query->getArrayResult();
  }
  
	function getByDql($dql,$orderAry='',$limit=''){
	        if(is_array($orderAry)){
	                $dql .= __parseOrderBy($orderAry);
	        }
	        
	        $query = $this->em->createQuery($dql);
	        if(is_array($limit)){
	                $query->setFirstResult($limit['offset']);
	                $query->setMaxResults($limit['limit']);
	        }
	        return $query->getArrayResult();
	}

  function getBy($entity,$whereAndAry='',$whereOrAry='',$orderAry="",$limit="",$join=""){
  	$entity = $this->entity.$entity;
  	if(!is_array($whereAndAry) && !is_array($whereOrAry)){
  		//return array();
  	}
  	if($join!=''){
  		if(is_array($join)){
  			$ary=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","u","v","w","x","y","z");
  			
  			$select = "SELECT t,";
  			$body = " FROM $entity t";
  			foreach($join as $jElement){
  				$tName = array_shift(array_values($ary));
  				$select .= $tName.",";
  				$cAry = explode(".",$jElement);
  				if(count($cAry)>1){
  					$body .= " LEFT JOIN " . $jElement . " ".$tName;
  				}else{
  					$body .= " LEFT JOIN t." . $jElement . " ".$tName;
  				}
  				
  				array_shift($ary);
  			}
  			$select = substr($select, 0, -1);
  			$dql = $select.$body;
  		}else{
  			$dql = "SELECT t,j FROM $entity t LEFT JOIN t.$join j";
  		}
  	}else{
  		$dql = "SELECT t FROM $entity t";
  	}

  	if($whereAndAry!='' || $whereOrAry!=''){
  		$dql .= __parseWhere($whereAndAry, $whereOrAry);
  	}
  	if(is_array($orderAry)){
  		$dql .= __parseOrderBy($orderAry);
  	}
//echo $dql;exit;
  	$query = $this->em->createQuery($dql);
	
  	if(is_array($limit)){
  		$query->setFirstResult((($limit['page']-1) * $limit['limit']));
  		$query->setMaxResults($limit['limit']);
  	}
  	$result = $query->getArrayResult();
  	if(count($result)>0){
  		return $result;
  	}else{
  		return false;
  	}
  }
  
  function getAValue($entity, $field, $whereAndAry='',$whereOrAry='',$join=''){
  	$entity = $this->entity.$entity;
  	if($join!=''){
  		$dql = "SELECT t, j.$field FROM $entity t LEFT JOIN t.$join j";
  	}else{
  		$dql = "SELECT t.$field FROM $entity t";
  	}
  
  	if($whereAndAry!='' || $whereOrAry!=''){
  		$dql .= __parseWhere($whereAndAry, $whereOrAry);
  	}
  	$query = $this->em->createQuery($dql);
  	$result = $query->getArrayResult();
  	if(count($result)>0){
  		return $result[0][$field];
  	}else{
  		return false;
  	}
  }
  
  function isUnique($entity, $whereAndAry='',$whereOrAry=''){
  	$entity = $this->entity.$entity;
  	$dql = "SELECT COUNT(t.id) AS cnt FROM $entity t";
  	if($whereAndAry!='' || $whereOrAry!=''){
  		$dql .= __parseWhere($whereAndAry, $whereOrAry);
  	}
  	$query = $this->em->createQuery($dql);
  	$result = $query->getArrayResult();
  	if(count($result)>0){
  		if($result[0]['cnt']==0){
  			return true;
  		}else{
  			return false;
  		}
  	}else{
  		return true;
  	}
  }
  
  function insert($tableName, $insertAry=''){
  	if(!is_array($insertAry)){
  		return false;
  	}
  	$tableName=__uncamelize($tableName);
  	$sql = "INSERT INTO $tableName";
  	$sql .= __parseInsert($insertAry);
  	try{
  		$q = $this->em->getConnection();
  		$query = $q->prepare($sql);
  		$query->execute();
  		return $q->lastInsertId('id');
  	}catch(Exception $ex){
  		return $ex->getMessage();
  	}
  
  	//In Action Query you should implement Logging System.
  }
  
  function update($tableName,$valueAry='',$whereAndAry='',$whereOrAry=''){
  	$tableName=__uncamelize($tableName);
  	$sql = "UPDATE $tableName t SET ";
  	$sql .= __parseValue($valueAry);
  	if($whereAndAry!='' || $whereOrAry!=''){
  		$sql .= __parseWhere($whereAndAry, $whereOrAry, true);
  	}
  	try{
  		$q = $this->em->getConnection();
  		$query = $q->prepare($sql);
  		$query->execute();
  		return true;
  	}catch(Exception $ex){
  		return $ex->getMessage();
  	}
  }
  
  function setActive($tableName,$state,$whereAndAry='',$whereOrAry=''){
  	$tableName=__uncamelize($tableName);
  	$sql = "UPDATE $tableName t SET t.isActive=$state";
  	if($whereAndAry!='' || $whereOrAry!=''){
  		$sql .= __parseWhere($whereAndAry, $whereOrAry, true);
  	}
  	try{
  		$q = $this->em->getConnection();
  		$query = $q->prepare($sql);
  		$query->execute();
  		return true;
  	}catch(Exception $ex){
  		return $ex->getMessage();
  	}
  }
  
  function delete($entity,$whereAndAry='',$whereOrAry=''){
  	$entity = $this->entity.$entity;
  	$dql = "DELETE FROM $entity t";
  	if($whereAndAry!='' || $whereOrAry!=''){
  		$dql .= __parseWhere($whereAndAry, $whereOrAry);
  	}
  	try{
  		$query = $this->em->createQuery($dql);
  		$query->execute();
  		$this->em->flush();
  		return true;
  	}catch(Exception $ex){
  		return $ex->getMessage();
  	}
  }
  
  function insertOrUpdate($tableName,$insertAry='',$updateAry=''){
  	$tableName=__uncamelize($tableName);
  	$sql = "INSERT INTO $tableName SET ";
  	$sql .= __parseValue($insertAry, true);
  	$sql .= " ON DUPLICATE KEY UPDATE ";
  	$sql .= __parseValue($updateAry, true);
  	try{
  		$q = $this->em->getConnection();
  		$query = $q->prepare($sql);
  		$query->execute();
  		return $q->lastInsertId('id');
  	}catch(Exception $ex){
  		return $ex->getMessage();
  	}
  }
  
  function runSQL($sql){
  	
  	try{
  		$q = $this->em->getConnection();//echo $sql;
  		$query = $q->prepare($sql);
  		$query->execute();
  		$result = $query->fetchAll();
  		
  		return $result;
  	}catch(Exception $ex){
  		return $ex->getMessage();
  	}
  }
  
  function reorder($tableName,$arr,$field,$offset=0,$reverse=false){
  	return $this->_multiSort($tableName,$arr,$offset,$field,$reverse);
  }
  
  private function _multiSort($tableName,$array,$offset,$field,$reverse=false){
  	$tableName=__uncamelize($tableName);
  	if(is_array($array)){
  		if($reverse){
  			$start=(count($array))+$offset;
  		}
  		$sql = "UPDATE $tableName SET $field = CASE ";
  		foreach($array as $order=>$id){
  			if($reverse){
  				$order=$start-$order;
  			}
  			$sql .= "WHEN id=$id THEN ".($order+$offset+1)." ";
  		}
  		$sql .= "ELSE 0 END WHERE id IN(".implode(",",$array).")";
  		try{
  			$q = $this->em->getConnection();
  			$query = $q->prepare($sql);
  			$query->execute();
  	   
  			return true;
  		}catch(Exception $ex){
  			return $ex->getMessage();
  		}
  	}
  	return "Invalid Input";
  }
  
  //-----------------------------------------------
  function checkFieldType($tableName){
  	$tableName=__uncamelize($tableName);
  	$sql = "SHOW FIELDS FROM $tableName";
  	$q = $this->em->getConnection();
  	$query = $q->prepare($sql);
  	$query->execute();
  	return $query->fetchAll();
  }
  
  function batchInsert($tableName, $arr){
      $this->load->database();
      $res=$this->db->insert_batch($tableName, $arr);

      return true;
  }
  
  
 
}