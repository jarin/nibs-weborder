<div>
<!-- login -->
    <!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-8 col-sm-12 col-md-push-2">
							<div class="page-content">
                                 <div class="contact-us">
                                     <div class="send-message" style="border:0px;">
                                        <h4 style="color:#e00000;">Register</h4>
                                        <?php echo validation_errors();?>
                                        
                                        <?php if($this->session->flashdata("error") !='') echo '<p class="text-danger">'.$this->session->flashdata("error").'</p>';?>
                                       <form method="post" action="customer/addCustomer" id="confirmReg">
                                          <div class="row">
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>User Name*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="User Name*" name="userName" value="<?php echo set_value('userName'); ?>" data-rule-required="true"  data-msg-required="Please enter your user name">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Password*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="password" placeholder="Password*" name="password" value="" data-rule-required="true"  data-msg-required="Please enter your password">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Customer Name*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Customer Name*" name="customerName" value="<?php echo set_value('customerName'); ?>" data-rule-required="true"  data-msg-required="Please enter your full name">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Member Ship No:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Member Ship No" name="membershipNo" value="<?php echo set_value('membershipNo'); ?>">
                                            </div>
                                                                                       
                                            <div class="col-md-6 col-sm-6">
                                                <label>Date of Birth:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                            	<input type="text" name="dateOfBirth" value="" placeholder="Date of Birth" class="datepicker" data-date-format="yyyy-mm-dd">
						    					  
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Anniversary Date:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Anniversary Date" name="anniversaryDate" value="<?php echo set_value('anniversaryDate'); ?>" class="datepicker" data-date-format="yyyy-mm-dd">
          
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>House No:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House No" name="houseNo" value="<?php echo set_value('houseNo'); ?>">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>House Name:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House Name" name="houseName" value="<?php echo set_value('houseName'); ?>">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Street:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Street" name="street" value="<?php echo set_value('street'); ?>">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Town*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Town*" name="town" value="<?php echo set_value('town'); ?>" data-rule-required="true"  data-msg-required="Please enter your town">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>State*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="State*" name="state" value="<?php echo set_value('state'); ?>" data-rule-required="true"  data-msg-required="Please enter your state">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Post Code:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Post Code" name="postCode" value="<?php echo set_value('postCode'); ?>">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Country*" name="country" value="<?php echo set_value('country'); ?>" data-rule-required="true"  data-msg-required="Please enter your country">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Email:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="email" placeholder="Email" name="email" value="<?php echo set_value('email'); ?>">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Mobile:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Mobile" name="mobile" value="<?php echo set_value('mobile'); ?>">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Land Phone:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6" style="padding-right:0px;">
                                              <input type="text" placeholder="Land Phone" name="landLine" value="<?php echo set_value('landLine'); ?>">
                                            </div>
                                            <input type="hidden" name="uri" value="<?=$uri?>"/>
                                          </div>
                                          <!-- end nasted .row -->
                                        
                                          <button type="submit"><i class="fa fa-key"></i> Save</button>
                                         
                                          
                                        </form>
                                        
                                        
                                           
                                      </div>
                                       <!-- end .send-message -->
                                  </div>
                              </div>
                                  <!-- end .page-content -->
                          </div>
                      </div>
                 </div>
                 <!-- end .container -->

<!-- END login -->


