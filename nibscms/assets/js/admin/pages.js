$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addPageBtn").overlay({width:"30%",inline:true,title:"Add New Page",href:"#addPageForm"});

  //checkboxes
  $("#sidebar ul.list li input[type=checkbox]").live("click",function(){
		activate_page($(this));
		return false;
	});

  // See More details on click
  $("#sidebar ul.list li").live("click",function(){
		var _li=$(this);
		_li.siblings().removeClass("active");
		_li.addClass("active");
		loadPageDetails(_li.attr("id"));
		return false;
	});

	//resize MCE editor as soon as it is shown
	$("#innerTabs li").live("click",function(){
	  var _this=$(this);
	  if (_this.index()==1){
	    resizeEditor();
	  }

	});


  //show/hide menu details   
  $('#footerMenu').live('click', function(){
    var _this=$(this);
    var is_footerMenu=_this.prop('checked');
    var temp=$("#hasFooter").html(
      "<tr>"+
        "<th align='left'><label>Show in</label></th>"+
        "<th style='text-align:left' colspan='2' class='page_radio'>"+
          "<input type='radio' name='footerMenu' value='1' />About us &nbsp;"+
          "<input type='radio' name='footerMenu' value='2' />Find us"+
        "</th>"+
      "</tr>"
  );
    if(is_footerMenu==true){
      temp.show();
    }else{
      temp.hide(); 
    }
  });

  

	//Autosaves
  $("#loadAutoSave").live("click",function(){
    var page_id=$("#EditableArea").parent().find("input[name=page_id]").val(),
    _this=$(this);
    _this.html("Loading... Please wait...").prop("disabled",true).next().prop("disabled",true);
    $.get(base_url+"admin/pages/XgetAutoSave/"+page_id,function(text){
      var data = eval("(" + text + ")");
      if(data && data.data){
        tinyMCE.execInstanceCommand('EditableArea','mceSetContent',false,data.data);
      }
      var _par=_this.parent().parent()
      _par.addClass("hide");
      setTimeout(function(){
    	  _par.remove();
    	},300);
    })

  });
  $("#discardAutoSave").live("click",function(){
    var _par=$(this).parent().parent();
    _par.addClass("hide");
    setTimeout(function(){
  	  _par.remove();
  	},300);
  });
});

function replaceImage(id){
  $("#"+id).find("img").attr("src","");
}
function addPage(name,id){
	NIBS.notif.notify("success",name+" added");
	var html="<li id='page_"+id+"' class='deleted'>"+
  "<a href='#'>"+
  "<span class='name icon-box'><i class=''><img src='"+base_url+"assets/images/admin/drag_handle_active.png'/></i></span>"+
	"<span class='name nameBtn' title='"+name+"'>"+name+"</span>"+
  "<span class='list_icon_btn'>"+
	   "<input title='Delete Page' type='image' class='action delete' rel='pages/deletePage' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
     "<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"+
  "</span>"+
  "</a>"+  
	"</li>";

	$("#sidebar ul.list").prepend(html);
	$("#add_page_form").reset();
	setTimeout(function(){
	  $("#page_"+id).removeClass("deleted");
	},100);
}

function loadPageDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	tinyMCE && tinyMCE.execCommand("mceRemoveControl",false,"EditableArea");
	$("#dContent").load(base_url+"admin/pages/viewPage/"+id,function(){
	  $(window).bind("resize",NIBS.util.debounce(resizeEditor));
	});
	loadPageInfo(id);
	$("#innerTabs,#thirdBar").show(0);
	$("#innerTabs li").removeClass("active").eq(0).addClass("active");
	$("#main-content").addClass("hastabs threepane");
}

function loadPageInfo(id){
  $("#infoPanel").load(base_url+"admin/pages/viewPageInfo/"+id);
}

function deletePage(page_id){
  var el=$("#page_"+page_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
    $("#innerTabs,#thirdBar").hide(0);
    $("#main-content").removeClass("threepane hastabs");
    $(window).unbind("resize");
  }
	el.addClass("deleted");
	setTimeout(function(){
	  el.remove()
	},500);
	NIBS.notif.notify("success",el.find("span").text()+" deleted successfully");
}

function activate_page(el){
  if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({id:id},"admin/pages/activateDeactivatePage");
}

function do_activate_page(id,val){
  $("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
  $("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

function resizeEditor(){
  var ifr=$("#EditableArea_ifr"),
  spn=$("#EditableArea_parent");
  ifr.css({height:spn.height()-62});
}
