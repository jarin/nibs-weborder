<?php

namespace entities;

/**
 * ResProductWasteHistory
 *
 * @Table(name="res_product_waste_history")
 * @Entity
 */
class ResProductWasteHistory
{
    /**
     * @var bigint $id
     *
     * @Column(name="id", type="bigint", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var ResProduct
     *
     * @ManyToOne(targetEntity="ResProduct")
     * @JoinColumns({
     *   @JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
     * })
     */
    private $product;
    
    /**
     * @var datetime $wasteDate
     *
     * @Column(name="waste_date", type="datetime", nullable=false)
     */
    private $wasteDate;
    
    /**
     * @var decimal $quantity
     *
     * @Column(name="quantity", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $quantity=0;
    
    /**
     * @var ResUser
     *
     * @ManyToOne(targetEntity="ResUser")
     * @JoinColumns({
     *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
     * })
     */
    private $lastUpdatedBy;
    
    
    /**
     * Get id
     *
     * @return bigint
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set product
     *
     * @param ResProduct $product
     */
    public function setProduct(\ResProduct $product)
    {
        $this->product = $product;
    }
    
    /**
     * Get product
     *
     * @return ResProduct
     */
    public function getProduct()
    {
        return $this->product;
    }
    
    /**
     * Set wasteDate
     *
     * @param datetime $wasteDate
     */
    public function setWasteDate($wasteDate)
    {
        $this->wasteDate = $wasteDate;
    }
    
    /**
     * Get wasteDate
     *
     * @return datetime
     */
    public function getWasteDate()
    {
        return $this->wasteDate;
    }
    
    /**
     * Set quantity
     *
     * @param decimal $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
    
    /**
     * Get quantity
     *
     * @return decimal
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    
    /**
     * Set lastUpdatedBy
     *
     * @param ResUser $lastUpdatedBy
     */
    public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
    {
        $this->lastUpdatedBy = $lastUpdatedBy;
    }
    
    /**
     * Get lastUpdatedBy
     *
     * @return ResUser
     */
    public function getLastUpdatedBy()
    {
        return $this->lastUpdatedBy;
    }
}
