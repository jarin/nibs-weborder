<?php



namespace entities;

/**
 * ResVoucher
 *
 * @Table(name="res_voucher")
 * @Entity
 */
class ResVoucher
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $voucherNumber
   *
   * @Column(name="voucher_number", type="string", length=50, nullable=false)
   */
  private $voucherNumber;
  
  /**
   * @var decimal $value
   *
   * @Column(name="value", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $value=0;
  
  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;
  
  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $customer;
  
  /**
   * @var datetime $validFrom
   *
   * @Column(name="valid_from", type="datetime", nullable=false)
   */
  private $validFrom;
  
  /**
   * @var datetime $validTill
   *
   * @Column(name="valid_till", type="datetime", nullable=false)
   */
  private $validTill;


  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set validFrom
   *
   * @param datetime $validFrom
   */
  public function setValidFrom($validFrom)
  {
  	$this->validFrom = $validFrom;
  }
  
  /**
   * Get validFrom
   *
   * @return datetime
   */
  public function getValidFrom()
  {
  	return $this->validFrom;
  }
  
  /**
   * Set validTill
   *
   * @param datetime $validTill
   */
  public function setValidTill($validTill)
  {
  	$this->validTill = $validTill;
  }
  
  /**
   * Get validTill
   *
   * @return datetime
   */
  public function getValidTill()
  {
  	return $this->validTill;
  }
  
  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
  	$this->customer = $customer;
  }
  
  /**
   * Get customer
   *
   * @return ResCustomer
   */
  public function getCustomer()
  {
  	return $this->customer;
  }

  /**
   * Set voucherNumber
   *
   * @param string $voucherNumber
   */
  public function setVoucherNumber($voucherNumber)
  {
    $this->voucherNumber = $voucherNumber;
  }

  /**
   * Get voucherNumber
   *
   * @return string 
   */
  public function getVoucherNumber()
  {
    return $this->voucherNumber;
  }
  
  /**
   * Set value
   *
   * @param decimal $value
   */
  public function setValue($value)
  {
  	$this->value = $value;
  }
  
  /**
   * Get value
   *
   * @return decimal
   */
  public function getValue()
  {
  	return $this->value;
  }
  
  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
  	$this->isActive = $isActive;
  }
  
  /**
   * Get isActive
   *
   * @return boolean
   */
  public function getIsActive()
  {
  	return $this->isActive;
  }
  
  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
  	$this->lastUpdate = $lastUpdate;
  }
  
  /**
   * Get lastUpdate
   *
   * @return datetime
   */
  public function getLastUpdate()
  {
  	return $this->lastUpdate;
  }
  
  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
  	$this->lastUpdatedBy = $lastUpdatedBy;
  }
  
  /**
   * Get lastUpdatedBy
   *
   * @return ResUser
   */
  public function getLastUpdatedBy()
  {
  	return $this->lastUpdatedBy;
  }
}