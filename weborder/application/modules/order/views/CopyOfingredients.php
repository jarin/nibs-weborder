                                        <div class="dropdown-details">
                                            <?php if(count($ingredient)> 0){?>
											<form class="default-form" id="formIngred_<?php echo $productid;?>">
											    <input type="hidden" name="productid" value="<?php echo $productid;?>"/>
												<h5>Please Select Your Option</h5>
												<?php if(count($without)> 0){ ?>
												<h6>Without</h6>
												<?php foreach ($without as $k=>$val) {
												    if($val['ingredientName'] !='Special Instruction'){ ?>
												<span class="checkbox-input">
												<?php 
												$cheched = '';
												$cancelIns = false;
												if($this->session->userdata('cart') !=''){
												    foreach ($this->session->userdata('cart') as $cartItem){
                                                        if($cartItem['product_id'] == $productid){
                                                            $cancelIns = true;
                                                            if(isset($cartItem['ingredients']) && count($cartItem['ingredients'])> 0){
                                                                if(isset($cartItem['ingredients']['without']) && count($cartItem['ingredients']['without']) > 0){
                                                                    foreach ($cartItem['ingredients']['without'] as $v){
                                                                        if($val['id'] == $v){
                                                                            $cheched = 'checked';
                                                                        }
                                                                    }
                                                                   
                                                                }
                                                            }
                                                        }
                                                    }
												}
												?>
													<input type="checkbox" id="without_<?php echo $productid;?>_<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="withoutingredient[]" <?php echo $cheched;?>>
													<label for="without_<?php echo $productid;?>_<?php echo $val['id']?>"><?php echo $val['ingredientName']?><i class="fa fa-plus price">&pound;<?php echo $val['priceWithout']?></i>
													</label>
												</span>
												<?php 
												        }
                                                     }
                                                  } 
                                                  ?>
												<?php if(count($with)> 0){ ?>
												<h6>Extras</h6>
												<?php foreach ($with as $k=>$val) {
                                                    if($val['ingredientName'] !='Special Instruction'){
												    ?>
												   
												<span class="checkbox-input">
												 <?php 
												$cheched = '';
												if($this->session->userdata('cart') !=''){
												    foreach ($this->session->userdata('cart') as $cartItem){
                                                        if($cartItem['product_id'] == $productid){
                                                            $cancelIns = true;
                                                            if(isset($cartItem['ingredients']) && count($cartItem['ingredients'])> 0){
                                                                if(isset($cartItem['ingredients']['with']) && count($cartItem['ingredients']['with']) > 0){
                                                                    foreach ($cartItem['ingredients']['with'] as $v){
                                                                        if($val['id'] == $v){
                                                                            $cheched = 'checked';
                                                                        }
                                                                    }
                                                                   
                                                                }
                                                            }
                                                        }
                                                    }
												}
												?>
													<input type="checkbox" id="with_<?php echo $productid;?>_<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="withingredient[]" <?php echo $cheched;?>>
													<label for="with_<?php echo $productid;?>_<?php echo $val['id']?>"><?php echo $val['ingredientName']?><i class="fa fa-plus price">&pound;<?php echo $val['price']?></i>
													</label>
												</span>
										
												<?php
                                                        }
												    }
												}
												?>
												<?php 
												$additionalIns = false;
												    if(count($without)> count($with)){
                                                        $count= count($without);
                                                    } else if(count($with)>= count($without)){
                                                        $count= count($with);
                                                    }
												    for($i=0; $i<$count; $i++){
                                                    if(isset($without[$i]) || isset($with[$i]))
                                                    {
                                                        if((isset($without[$i]) && $without[$i]['ingredientName'] == 'Special Instruction') || (isset($with[$i]) && $with[$i]['ingredientName'] == 'Special Instruction'))
                                                        {
                                                           $additionalIns = true;         
                                                        }
                                                    }
                                                    }
												?>
												<?php if($additionalIns){?>
												<h6>Additional Notes</h6>
												 <?php 
												 $instruction ='';
												if($this->session->userdata('cart') !=''){
												    foreach ($this->session->userdata('cart') as $cartItem){
                                                        if($cartItem['product_id'] == $productid){
                                                            if(isset($cartItem['ingredients']) && count($cartItem['ingredients'])> 0){
                                                                if(isset($cartItem['ingredients']['specialInstruction'])){
                                                                    $instruction = $cartItem['ingredients']['specialInstruction'];
                                                                }
                                                            }
                                                        }
                                                    }
												}
												?>
												<textarea placeholder="Write here" name="additionalNote"><?php echo $instruction;?></textarea>
												<?php } ?>
												<a class="btn btn-default-red addIngred" rel="<?php echo $productid;?>" name="submit" type="submit">Confirm</a>
												<a class="btn btn-default-black removeIngred <?php if($cancelIns){ echo 'ajaxCall';}?>" rel="<?php echo $productid;?>" name="cancel" type="submit">Cancle</a>
											</form>
											<?php } else { ?>
											    <h5>No Options Availabe for this item</h5>
											<?php } ?>
										</div>
										<!--end .dropdown-details-->
<script type="text/javascript">
$( '.checkbox-input' ).each(function(){
	$(this).uouCheckboxInput();
});




</script>

