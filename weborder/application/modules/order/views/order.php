       <div id="page-content">
       
		 <?php $this->load->view('top_content');?>

			<!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
					<?php $this->load->view('main_content')?>

					<?php $this->load->view('left_sidebar')?>
				</div>
				<!-- end .row -->
			</div>
			<!--end .container -->