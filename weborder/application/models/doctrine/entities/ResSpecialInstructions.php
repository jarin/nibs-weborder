<?php



namespace entities;

/**
 * ResSpecialInstructions
 *
 * @Table(name="res_special_instructions")
 * @Entity
 */
class ResSpecialInstructions
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $instruction
   *
   * @Column(name="instruction", type="string", length=500, nullable=false)
   */
  private $instruction;

  /**
   * @var ResOrderChild
   *
   * @ManyToOne(targetEntity="ResOrderChild")
   * @JoinColumns({
   *   @JoinColumn(name="order_child_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $orderChild;

  /**
   * @var decimal $price
   *
   * @Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $price=0;


  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set instruction
   *
   * @param string $instruction
   */
  public function setInstruction($instruction)
  {
    $this->instruction = $instruction;
  }

  /**
   * Get instruction
   *
   * @return string 
   */
  public function getInstruction()
  {
    return $this->instruction;
  }

  /**
   * Set orderChild
   *
   * @param ResOrderChild $orderChild
   */
  public function setOrderChild(\ResOrderChild $orderChild)
  {
    $this->orderChild = $orderChild;
  }

  /**
   * Get orderChild
   *
   * @return ResOrderChild 
   */
  public function getOrderChild()
  {
    return $this->orderChild;
  }

  /**
   * Set price
   *
   * @param decimal $price
   */
  public function setPrice($price)
  {
  	$this->price = $price;
  }
  
  /**
   * Get price
   *
   * @return decimal
   */
  public function getPrice()
  {
  	return $this->price;
  }
}