<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function time_elapsed($date) {
	//$now = new DateTime();
    //$CI =& get_instance();
    //$CI->load->helper('date');
	//$now = now();
    $now = new DateTime(gmdate('Y-m-d H:i:s'));
	$interval = $date->diff($now);
	return ($interval->h.":".$interval->i.":".$interval->s);
}

/**
 * makeEncryptedPassword
 * 
 * This function will encrypt password
 * according to our encryption logic
 * 
 * @param string $rawPassword
 * @return string $encryptedPassword
 */
function makeEncryptedPassword($rawPassword)
{
    $salt = "AfI32f297a57a5a743894a0e4a801fc";
    return md5($salt.$rawPassword.$salt);
}

/**
 * generateCustomerCode
 * 
 * This function will generate our customer's code
 * according to our code generation logic
 * 
 * $param none
 * $return string $newCode
 */
function generateCustomerCode(){
    $CI =& get_instance();
    
    $length = $CI->common_m->getAValue("ResSettings", "value", array("settingsName"=>"customer_code_length"));
    $code = __rand_string($length, true);
    while (!$CI->common_m->isUnique("ResCustomer", array("customerKey"=>$code))) {
        $code = __rand_string($length, true);
    }
    return $code;
}

/**
 * URL safe base64_encode
 *
*/
function base64url_encode($plainText){
	$base64=base64_encode($plainText);
	$base64url= strtr($base64, '+/=', '-_~');
	return $base64url;
}

/**
 * URL safe base64_decode
 *
*/
function base64url_decode($encoded){
	$base64=strtr($encoded,'-_~','+/=');
	$plainText=base64_decode($base64);
	return $plainText;
}

/**
 * Address TypeAheads which will use many places
 * 
 */
function __getAddressTypeAhead(){
	$CI =& get_instance();
	$CI->load->model("common_m");
	
	$CI->common_m->setTableName("street");
	$data['street']=$CI->common_m->getValuesForTypeahead("street");
	
	$CI->common_m->setTableName("locality");
	$data['locality']=$CI->common_m->getValuesForTypeahead("locality");
	
	$CI->common_m->setTableName("town_city");
	$data['town']=$CI->common_m->getValuesForTypeahead("town_city");
	
	$CI->common_m->setTableName("country");
	$data['country']=$CI->common_m->getValuesForTypeahead("name");
	
	$CI->common_m->setTableName("postcode");
	$data['postcode']=$CI->common_m->getValuesForTypeahead("postcode");

	return $data;
}

/**
 * Insert An Address
 * 
 */
function __addAddress($data){
	$CI =& get_instance();
	$CI->load->model("common_m");
	
	//Address will be unique for each customer
	//As may be two customers can stay in a same address
	//After that, one changes his/her address
	//Then if we use common address for both the customers
	//then both customers will be effected.
	if($data['street_id']==0){
		$CI->common_m->setTableName("street");
		$data['street_id']=$CI->common_m->insert(array("street"=>$data['street']));
	}
		
	if($data['locality_id']==0){
		$CI->common_m->setTableName("locality");
		$data['locality_id']=$CI->common_m->insert(array("locality"=>$data['locality']));
	}
	
	if($data['town_city_id']==0){
		$CI->common_m->setTableName("town_city");
		$data['town_city_id']=$CI->common_m->insert(array("town_city"=>$data['town']));
	}
		
	if($data['country_id']==0){
		$CI->common_m->setTableName("country");
		$data['country_id']=$CI->common_m->insert(array("name"=>$data['country']));
	}
		
	if($data['postcode_id']==0){
		$CI->common_m->setTableName("postcode");
		$data['postcode_id']=$CI->common_m->insert(array("postcode"=>strtoupper($data['postcode'])));
	}
		
	$CI->common_m->setTableName("address");
	$address_id=$CI->common_m->insert(
			array(
					"building_name"=>$data['buildingName'],
					"house_no"=>$data['houseNo'],
					"street_id"=>$data['street_id'],
					"locality_id"=>$data['locality_id'],
					"town_city_id"=>$data['town_city_id'],
					"postcode_id"=>$data['postcode_id'],
					"country_id"=>$data['country_id']
			)
	);
	
	return $address_id;
}

/**
 * Insert Bulk Address
 *
 */
function __addAddresses($data){
	$CI =& get_instance();
	$CI->load->model("common_m");

	//Address will be unique for each customer
	//As may be two customers can stay in a same address
	//After that, one changes his/her address
	//Then if we use common address for both the customers
	//then both customers will be effected.
	
	$CI->common_m->setTableName("street");
	$street = $CI->common_m->getRowBy(array("street"=>$data['street']));
	if(count($street)>0){
		$data['street_id'] = $street['id'];
	}else{
		$data['street_id']=$CI->common_m->insert(array("street"=>$data['street']));
	}
	
	if(is_null($data['locality'])){
		$data['locality'] = '';
	}
	$CI->common_m->setTableName("locality");
	$locality = $CI->common_m->getRowBy(array("locality"=>$data['locality']));
	if(count($locality)>0){
		$data['locality_id'] = $locality['id'];
	}else{
		$data['locality_id']=$CI->common_m->insert(array("locality"=>$data['locality']));
	}
	
	$CI->common_m->setTableName("town_city");
	$town_city = $CI->common_m->getRowBy(array("town_city"=>$data['town_city']));
	if(count($town_city)>0){
		$data['town_city_id'] = $town_city['id'];
	}else{
		$data['town_city_id']=$CI->common_m->insert(array("town_city"=>$data['town_city']));
	}
	
	if(is_null($data['country'])){
		$data['country'] = '';
	}
	$CI->common_m->setTableName("country");
	$country = $CI->common_m->getRowBy(array("name"=>$data['country']));
	if(count($country)>0){
		$data['country_id'] = $country['id'];
	}else{
		$data['country_id']=$CI->common_m->insert(array("name"=>$data['country']));
	}
	
	$CI->common_m->setTableName("postcode");
	$postcode = $CI->common_m->getRowBy(array("postcode"=>$data['postcode']));
	if(count($postcode)>0){
		$data['postcode_id'] = $postcode['id'];
	}else{
		$data['postcode_id']=$CI->common_m->insert(array("postcode"=>$data['postcode']));
	}

	$CI->common_m->setTableName("address");
	$address_id=$CI->common_m->insert(
			array(
					"building_name"=>$data['buildingName'],
					"house_no"=>$data['houseNo'],
					"street_id"=>$data['street_id'],
					"locality_id"=>$data['locality_id'],
					"town_city_id"=>$data['town_city_id'],
					"postcode_id"=>$data['postcode_id'],
					"country_id"=>$data['country_id']
			)
	);

	return $address_id;
}


/**
 * Updated An Address
 * 
 */
function __updateAddress($data, $id){
	$CI =& get_instance();
	$CI->load->model("common_m");
	
	if($data['street_id']==0){
		$CI->common_m->setTableName("street");
		$data['street_id']=$CI->common_m->insert(array("street"=>$data['street']));
	}
	
	if($data['locality_id']==0){
		$CI->common_m->setTableName("locality");
		$data['locality_id']=$CI->common_m->insert(array("locality"=>$data['locality']));
	}
	
	if($data['town_city_id']==0){
		$CI->common_m->setTableName("town_city");
		$data['town_city_id']=$CI->common_m->insert(array("town_city"=>$data['town']));
	}
	
	if($data['country_id']==0){
		$CI->common_m->setTableName("country");
		$data['country_id']=$CI->common_m->insert(array("name"=>$data['country']));
	}
	
	if($data['postcode_id']==0){
		$CI->common_m->setTableName("postcode");
		$data['postcode_id']=$CI->common_m->insert(array("postcode"=>strtoupper($data['postcode'])));
	}

	$CI->common_m->setTableName("address");
	$CI->common_m->update($id,
			array(
					"building_name"=>$data['buildingName'],
					"house_no"=>$data['houseNo'],
					"street_id"=>$data['street_id'],
					"locality_id"=>$data['locality_id'],
					"town_city_id"=>$data['town_city_id'],
					"postcode_id"=>$data['postcode_id'],
					"country_id"=>$data['country_id']
			)
	);
}


/**
 * Generate Random String to use with password etc.
 * 
 */
function __rand_string( $length, $numeric = false ) {
    if ($numeric) {
        $chars = "0123456789";
    } else {
        $chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789";
    }
    $size = strlen( $chars );
	$str = '';
	for( $i = 0; $i < $length; $i++ ) {
		$str .= $chars[ rand( 0, $size - 1 ) ];
	}
	return $str;
}

/**
 * Helper function for File Upload
 * 
 */
if (!function_exists('getallheaders')){
	function getallheaders(){
		foreach ($_SERVER as $name => $value){
			if (substr($name, 0, 5) == 'HTTP_'){
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
	}
}

/**
 * File extensions
 *
 * breaks a file name into it's components
 * example input: document.doc
 * output: array( "filename" => "document", "extension" => ".doc", "extension_undotted" => "doc")
 *
 * @access  public
 * @param string
 * @return  array
 */
function _file_extension($filename){
	$x = explode('.', $filename);
	$ext=end($x);
	$filenameSansExt=str_replace('.'.$ext,"",$filename);
	return array(
			"filename"=>$filenameSansExt,
			"extension"=>'.'.$ext,
			"extension_undotted"=>$ext
	);
}

/**
 * Function to send Branded EMail on various Events
 */
function sendBrandedEmail($data, $event, $type=""){
	$saveData=$data;
	
	$CI =& get_instance();
	
	$CI->common_m->setTableName("email_templates");
	$email_template=$CI->common_m->getRowBy(array("name"=>$event));
	
	if(!is_array($email_template)){
		return FALSE;
	}
	
	$plain_msg=__parseMessage($email_template['plain_msg'],$data);
	
	if(strtolower($type)=="password"){
		//$source='assets/images/password_background.jpg';
		$passFile='temp/images/'.md5($data['password']).'.gif';
		
		$CI->load->library('image_lib');
		
		$config['image_library'] = 'gd2';
		$config['wm_type'] = 'text';
		
		$config['source_image']	= $passFile;
		$config['wm_vrt_alignment'] = 'middle';
		$config['wm_hor_alignment'] = 'center';
		$config['wm_text'] = $data['password'];
		$config['wm_font_path'] = 'assets/fonts/sansation_bold.ttf';
		$config['wm_font_size']	= '16';
		$config['wm_font_color'] = 'ffffff';
		
		$CI->image_lib->initialize($config);
		$CI->image_lib->watermark();
		
		$data['password']='<img src="cid:'.md5($data['password']).'.gif" border="0" width="200" height="99" alt="Your new password" />';
	}
	
	$etdata['html']=__parseMessage(html_entity_decode($email_template['message']),$data);
	$saveETData['html']=__parseMessage(html_entity_decode($email_template['message']),$saveData);
	
	$CI->common_m->setTableName("general_settings");
	$logo_path=$CI->common_m->getAValue("value",array("name"=>"siteLogo"));
	$logo_ary=explode("/",$logo_path);
	$etdata['logo']='cid:'.$logo_ary[count($logo_ary)-1];
	$saveETData['logo']=base_url().$logo_path;
	
	$html=$CI->load->view('email_template',$etdata,true);
	$saveHTML=$CI->load->view('email_template',$saveETData,true);
	
	$config['mailtype']='html';
	$CI->load->library('email');
	$CI->email->initialize($config);
	$CI->email->from($email_template['from_email'], $email_template['from_name']);
	$CI->email->to($data['email']);
	$CI->email->subject($email_template['subject']);
	$CI->email->message($html);
	$CI->email->set_alt_message($plain_msg);
	
	if(strtolower($type)=="password"){
		$CI->email->attach($passFile,'inline');
	}
	$CI->email->attach($logo_path,'inline');
	
	if($CI->email->send()){
		@unlink($passFile);
		$CI->common_m->setTableName("customer");
		$id=$CI->common_m->getAValue("id",array("email"=>$data['email']));
		if(!$id){
			$CI->common_m->setTableName("staff");
			$id=$CI->common_m->getAValue("id",array("email"=>$data['email']));
			$insArray['staff_id']=$id;
		}else{
			$insArray['customer_id']=$id;
		}
		$CI->common_m->setTableName("email_history");
		$insArray=array_merge($insArray,array(
				"to_email"		=> $data['email'],
				"subject"			=> $email_template['subject'],
				"message"			=> $saveHTML
		));
		$CI->common_m->insert($insArray);
		return TRUE;
	}else{
		return FALSE;
	}
}

/**
 * Universal Message Perser
 */
function __parseMessage($html,$data){
	//variables to replace with value
	$html=preg_replace('/{\$name}/', (isset($data['Name']) ? $data['Name'] : ''), $html);
	$html=preg_replace('/{\$firstname}/', (isset($data['firstName']) ? $data['firstName'] : ''), $html);
	$html=preg_replace('/{\$lastname}/', (isset($data['lastName']) ? $data['lastName'] : ''), $html);
	$html=preg_replace('/{\$siteurl}/', base_url(), $html);
	$html=preg_replace('/{\$password}/', (isset($data['password']) ? $data['password'] : ''), $html);
	$html=preg_replace('/{\$customer_id}/', (isset($data['customer_id']) ? $data['customer_id'] : ''), $html);
	
	//Invoice Related Variables
	$html=preg_replace('/{\$invoice_date_created}/', (isset($data['invoice_date_created']) ? $data['invoice_date_created'] : ''), $html);
	$html=preg_replace('/{\$invoice_payment_method}/', (isset($data['invoice_payment_method']) ? $data['invoice_payment_method'] : ''), $html);
	$html=preg_replace('/{\$invoice_num}/', (isset($data['invoice_num']) ? $data['invoice_num'] : ''), $html);
	$html=preg_replace('/{\$invoice_total}/', (isset($data['invoice_total']) ? $data['invoice_total'] : ''), $html);
	$html=preg_replace('/{\$invoice_date_due}/', (isset($data['invoice_date_due']) ? $data['invoice_date_due'] : ''), $html);
	$html=preg_replace('/{\$invoice_html_contents}/', (isset($data['invoice_html_contents']) ? $data['invoice_html_contents'] : ''), $html);
	$html=preg_replace('/{\$invoice_link}/', (isset($data['invoice_link']) ? $data['invoice_link'] : ''), $html);
	$html=preg_replace('/{\$invoice_balance}/', (isset($data['invoice_balance']) ? $data['invoice_balance'] : ''), $html);
	$html=preg_replace('/{\$invoice_status}/', (isset($data['invoice_status']) ? $data['invoice_status'] : ''), $html);
	$html=preg_replace('/{\$invoice_last_payment_amount}/', (isset($data['invoice_last_payment_amount']) ? $data['invoice_last_payment_amount'] : ''), $html);
	$html=preg_replace('/{\$invoice_last_payment_transid}/', (isset($data['invoice_last_payment_transid']) ? $data['invoice_last_payment_transid'] : ''), $html);
	$html=preg_replace('/{\$invoice_amount_paid}/', (isset($data['invoice_amount_paid']) ? $data['invoice_amount_paid'] : ''), $html);

	return $html;
}

/**
 * Generate File Name for New Password
 */
function _generateFileName($password){
	if(!file_exists('temp/images')){
		@mkdir('temp/images');
		@chmod('temp/images',0777);
	}
	if(file_exists('temp/images/'.md5($password).'.jpg')){
		$password=$this->__rand_string(10);
		$password=$this->_generateFileName($password);
	}else{
		$source='assets/images/password_background.jpg';
		$passFile='temp/images/'.md5($password).'.gif';

		@copy($source, $passFile);
	}
	return $password;
}

function __parseWhere($whereAndAry, $whereOrAry, $uncamelize=false){
	$where = " WHERE ";
	if(is_array($whereAndAry)){
		foreach($whereAndAry as $field=>$value){
			if(preg_match('/(IS|IS NOT).*/',$field)){ // IS, IS NOT
				if($uncamelize){
					if(preg_match('/(IS NOT).*/',$field)){
						$field=preg_replace('/IS NOT/','',$field);
						if(preg_match("/\./", $field)){
							$where .= "CONCAT(".__uncamelize($field).",'')!='' AND ";
						}else{
							$where .= "CONCAT(t.".__uncamelize($field).",'')!='' AND ";
						}
					}else{
						$field=preg_replace('/IS/','',$field);
						if(preg_match("/\./", $field)){
							$where .= "CONCAT(".__uncamelize($field).",'')='' AND ";
						}else{
							$where .= "CONCAT(t.".__uncamelize($field).",'')='' AND ";
						}
					}
				}else{
					if(preg_match('/(IS NOT).*/',$field)){
						$field=preg_replace('/IS NOT/','',$field);
						if(preg_match("/\./", $field)){
							$where .= "CONCAT(".$field.",'')!='' AND ";
						}else{
							$where .= "CONCAT(t.".$field.",'')!='' AND ";
						}
					}else{
						$field=preg_replace('/IS/','',$field);
						if(preg_match("/\./", $field)){
							$where .= "CONCAT(".$field.",'')='' AND ";
						}else{
							$where .= "CONCAT(t.".$field.",'')='' AND ";
						}
					}
				}
			}else if(preg_match('/(IN|NOT IN).*/',$field)){
				if($uncamelize){
					if(preg_match("/\./", $field)){
						$where .= __uncamelize($field)." (".$value.") AND ";
					}else{
						$where .= "t.".__uncamelize($field)." (".$value.") AND ";
					}
				}else{
					if(preg_match("/\./", $field)){
						$where .= $field." (".$value.") AND ";
					}else{
						$where .= 't.'.$field." (".$value.") AND ";
					}
				}
			}else if(preg_match('/(>|<|[>=]|[<=]|[!=]|LIKE|NOT LIKE).*/',$field)===0){ // > < >= <= != LIKE
				if($uncamelize){
					if(preg_match("/\./", $field)){
						$where .= __uncamelize($field)."='".$value."' AND ";
					}else{
						$where .= "t.".__uncamelize($field)."='".$value."' AND ";
					}
				}else{
					if(preg_match("/\./", $field)){
						$where .= $field."='".$value."' AND ";
					}else{
						$where .= 't.'.$field."='".$value."' AND ";
					}
				}
			}else{
				if($uncamelize){
					if(preg_match("/\./", $field)){
						$where .= __uncamelize($field)." '".$value."' AND ";
					}else{
						$where .= "t.".__uncamelize($field)." '".$value."' AND ";
					}
				}else{
					if(preg_match("/\./", $field)){
						$where .= $field." '".$value."' AND ";
					}else{
						$where .= 't.'.$field." '".$value."' AND ";
					}
				}
			}
		}
		$where = substr($where,0,strlen($where)-5);
	}

	if(is_array($whereOrAry)){
		if($where!=' WHERE '){
			$where .= " OR ";
		}
		foreach($whereOrAry as $field=>$value){
			if(preg_match('/(IS|IS NOT).*/',$field)){ // IS, IS NOT
				if($uncamelize){
					if(preg_match('/(IS NOT).*/',$field)){
						$field=preg_replace('/IS NOT/','',$field);
						if(preg_match("/\./", $field)){
							$where .= "CONCAT(".__uncamelize($field).",'')!='' OR ";
						}else{
							$where .= "CONCAT(t.".__uncamelize($field).",'')!='' OR ";
						}
					}else{
						$field=preg_replace('/IS/','',$field);
						if(preg_match("/\./", $field)){
							$where .= "CONCAT(".__uncamelize($field).",'')='' OR ";
						}else{
							$where .= "CONCAT(t.".__uncamelize($field).",'')='' OR ";
						}
					}
				}else{
					if(preg_match('/(IS NOT).*/',$field)){
						$field=preg_replace('/IS NOT/','',$field);
						if(preg_match("/\./", $field)){
							$where .= "CONCAT(".$field.",'')!='' OR ";
						}else{
							$where .= "CONCAT(t.".$field.",'')!='' OR ";
						}
					}else{
						$field=preg_replace('/IS/','',$field);
						if(preg_match("/\./", $field)){
							$where .= "CONCAT(".$field.",'')='' OR ";
						}else{
							$where .= "CONCAT(t.".$field.",'')='' OR ";
						}
					}
				}
			}else if(preg_match('/(IN|NOT IN).*/',$field)){
				if($uncamelize){
					if(preg_match("/\./", $field)){
						$where .= __uncamelize($field)." (".$value.") OR ";
					}else{
						$where .= "t.".__uncamelize($field)." (".$value.") OR ";
					}
				}else{
					if(preg_match("/\./", $field)){
						$where .= $field." (".$value.") OR ";
					}else{
						$where .= 't.'.$field." (".$value.") OR ";
					}
				}
			}else if(preg_match('/(>|<|[>=]|[<=]|[!=]|LIKE|NOT LIKE).*/',$field)===0){ // > < >= <= != LIKE
				if($uncamelize){
					if(preg_match("/\./", $field)){
						$where .= __uncamelize($field)."='".$value."' OR ";							
					}else{
						$where .= "t.".__uncamelize($field)."='".$value."' OR ";
					}
				}else{
					if(preg_match("/\./", $field)){
						$where .= $field."='".$value."' OR ";
					}else{
						$where .= 't.'.$field."='".$value."' OR ";
					}
				}
			}else{
				if($uncamelize){
					if(preg_match("/\./", $field)){
						$where .= __uncamelize($field)." '".$value."' OR ";
					}else{
						$where .= "t.".__uncamelize($field)." '".$value."' OR ";
					}
				}else{
					if(preg_match("/\./", $field)){
						$where .= $field." '".$value."' OR ";
					}else{
						$where .= 't.'.$field." '".$value."' OR ";
					}
				}
			}
		}
		$where = substr($where,0,strlen($where)-4);
	}

	if($where==' WHERE '){
		return '';
	}else{
		return $where;
	}
}

function __parseOrderBy($orderAry){
	$orderBy = ' ORDER BY ';
	if(is_array($orderAry)){
		foreach($orderAry as $field=>$direction){
			if(preg_match("/\./", $field)){
				$orderBy .= "$field $direction, ";
			}else{
				$orderBy .= "t.$field $direction, ";
			}
		}
		$orderBy = substr($orderBy,0,strlen($orderBy)-2);
		return $orderBy;
	}else{
		return '';
	}
}

function __parseValue($valueAry, $withoutPrefix=false){
	if(is_array($valueAry)){
		$setValue = '';
		foreach($valueAry as $field=>$value){
			if(strtolower($value)=="null"){
				if($withoutPrefix){
					$setValue .= __uncamelize($field)."=$value, ";
				}else{
					$setValue .= "t.".__uncamelize($field)."=$value, ";
				}
			}else{
				if($withoutPrefix){
					$setValue .= __uncamelize($field)."='".trim($value)."', ";
				}else{
					$setValue .= "t.".__uncamelize($field)."='".trim($value)."', ";
				}
			}
		}
		$setValue = substr($setValue,0,strlen($setValue)-2);
		return $setValue;
	}else{
		return '';
	}
}

function __parseInsert($insertAry){
	if(is_array($insertAry)){
		$setValue = '('.__uncamelize(implode(", ",array_keys($insertAry))).") VALUES(";
		foreach($insertAry as $field=>$value){
			if(strtolower($value)=='null'){
				$setValue .= "$value, ";
			}else{
				$setValue .= "'$value', ";
			}
		}
		$setValue = substr($setValue,0,strlen($setValue)-2).")";
		return $setValue;
	}else{
		return '';
	}
}

/*
 * It will make myFieldName to my_field_name
*/
function __uncamelize($camel,$splitter="_") {
	$camel=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter.'$0', $camel));
	return strtolower($camel);
}

/**
 * Uncamelize
 *
 * returns uncamelized versions of camel case strings
 * example input: theQuickBrownFox
 * output: The Quick Brown Fox
 *
 * @access	public
 * @param string
 * @param string
 * @return	string
 */
function _uncamelizeextended($camel,$case="camel",$splitter=" ") {
	$camel=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter.'$0', $camel));
	if($case=='camel'){
		return ucfirst($camel);
	}else{
		return strtolower($camel);
	}
}

/**
 * calculateGeographicalDistance
 *
 * calculates the distance between to pairs of latitude longitude coordinates. 
 * Returns the distance in miles or kilometers
 * 
 * @access	public
 * @param string
 * @param string
 * @param string
 * @param string
 * @param boolean
  * @return	float
 */
function _calculateGeographicalDistance($lat1, $lng1, $lat2, $lng2, $miles = true){
	$pi80 = M_PI / 180;
	$lat1 *= $pi80;
	$lng1 *= $pi80;
	$lat2 *= $pi80;
	$lng2 *= $pi80;
 
	$r = 6372.797; // mean radius of Earth in km
	$dlat = $lat2 - $lat1;
	$dlng = $lng2 - $lng1;
	$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
	$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
	$km = $r * $c;
 
	return ($miles ? ($km * 0.621371192) : (float)$km);
}

/**
 * calculateGeographicalDistanceFromAddress
 *
 * calculates the distance between to pairs of latitude longitude coordinates.
 * Returns the distance in miles or kilometers
 *
 * @access	public
 * @param string
 * @param string
 * @param boolean
 * @return	float
 */
function _calculateGeographicalDistanceFromAddress($address1, $address2, $miles = true){
	$url = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($address1)."&sensor=false&region=England";
	$result = @file_get_contents($url);
	//$data = explode(",", $result);
	$data = json_decode(utf8_encode($result), true);
	$lat1 = $data['results'][0]['geometry']['location']['lat'];
	$lng1 = $data['results'][0]['geometry']['location']['lng'];

	$url = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($address2)."&sensor=false&region=England";
	$result = @file_get_contents($url);
	$data = json_decode(utf8_encode($result), true);
	$lat2 = $data['results'][0]['geometry']['location']['lat'];
	$lng2 = $data['results'][0]['geometry']['location']['lng'];
	
	return _calculateGeographicalDistance($lat1, $lng1, $lat2, $lng2, $miles);
}

/**
 * calculateDrivingDistance
 *
 * calculates the distance between to pairs of latitude longitude coordinates.
 * Returns the distance in miles or kilometers
 *
 * @access	public
 * @param string
 * @param string
 * @param string
 * @param string
 * @param boolean
 * @return	float
 */
function _calculateDrivingDistance($lat1, $lng1, $lat2, $lng2, $miles = true){
	$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.urlencode("$lat1,$lng1").'&sensor=false&region=England&language=en-En';
	$result = @file_get_contents($url);
	$data = json_decode(utf8_encode($result), true);
	$address1 = $data['results'][0]['formatted_address'];
	
	$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.urlencode("$lat2,$lng2").'&sensor=false&region=England&language=en-En';
	$result = @file_get_contents($url);
	$data = json_decode(utf8_encode($result), true);
	$address2 = $data['results'][0]['formatted_address'];

	return _calculateDrivingDistanceFromAddress($address1, $address2, $miles);
}

/**
 * calculateDrivingDistanceFromAddress
 *
 * calculates the distance between to pairs of latitude longitude coordinates.
 * Returns the distance in miles or kilometers
 *
 * @access	public
 * @param string
 * @param string
 * @param boolean
 * @return	float
 */
function _calculateDrivingDistanceFromAddress($address1, $address2, $miles = true){
	$url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=".urlencode($address1)."&destinations=".urlencode($address2)."&mode=car&language=en-EN&sensor=false";
	$result = @file_get_contents($url);
	$data = json_decode(utf8_encode($result), true);
//var_dump($address1, $address2, $data);exit;


	if(isset($data['rows'][0]['elements'][0]['distance']['text'])){
		$kmText = $data['rows'][0]['elements'][0]['distance']['text'];
		$kmText = explode(" ",$kmText);
		$km = $kmText[0];
		
		return ($miles ? ($km * 0.621371192) : (float)$km);
	}else{
		return 0;
	}
}

/**
 * getLogo
 *
 * This function will get logo of restaurant
 * 
 * @param none
 * @return string
 */
function getLogo(){
	$CI =& get_instance();

	if($CI->session->userdata('restaurantid') !=''){
		$logoAr = $CI->common_m->runSQL("SELECT logo FROM restaurant WHERE id='" .$CI->session->userdata('restaurantid'). "'");
		
		if(count($logoAr) > 0){
			if($logoAr[0]['logo'] !=''){
				$logos = $logoAr[0]['logo'];
				$logo = "../nibscms/assets/logo/".$logos;
			} else {
				$logo = "assets/img/header-logo.png";
			}
			
		} else {
			$logo = "assets/img/header-logo.png";
		}
	} else {
		$logo = "assets/img/header-logo.png";
	}
	return $logo;
}




