<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shop_catalog extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="shop_catalog";
		$this->load->model("settings_m","settings");
		$this->load->model("shophook_m","shop");
		//Settings
		$this->SETTINGS=array();
		_loadSettings($this);
	}

	function index($msg=""){
	  if(_checkAuthentication($this)){

	    $cats=$this->_catTree($this->shop->getAllCategories());
	    $cats='<li id="treebase" rel="folderparent" class="open"><a href="#"><b>Categories Root</b></a>'.$cats.'</li>';

  	  $this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
  		$this->load->view("admin/shop/catalog_template",array(
  		  "pagename"=>"Categories &amp; Products",
  		  "orphanList"=>"",
				"orphanCount"=>0,
				"list1"=>$cats
  		));
  		$this->load->view("admin/admin_foot");
    }
	}

	function viewProducts($id=0){
	  if(_checkAuthentication($this)){
	    $products=$this->shop->getProductsByCategory($id);
	    $html="";$baseUrl=base_url();
	    if(is_array($products)){
	      foreach ($products as $prod) {
	        if($prod["enabled"]==0){
						$class="off";
						$checkedmain="";
					}else{
						$class="";
						$checkedmain="checked";
					}
	        $html.="<li id='product_$prod[productID]' class='product'>
					<span class='name' title='$prod[name]'>".(($prod["name"]!="")?($prod["name"]):($prod["product_code"]))."</span>
					<input title='Delete Product' type='image' class='action delete' rel='shop_catalog/deleteProduct' elId='$prod[productID]' src='".$baseUrl."assets/images/admin/icon_trash.png' border='0' />
					<input class='main' id='checkboxprod_$prod[productID]' type='checkbox' $checkedmain />
					<span class='extraData'>".sprintf("%01.2f",$prod["Price"])."</span>
					</li>";
	      }
	    }

	    echo $html;
    }
	}

	function reOrderCategories(){
		if(_checkAuthentication($this)){
			$this->shop->tablename="SS_categories";
			$arr=json_decode($this->input->post("data"),true);

			if(is_array($arr)){
				$this->_recursiveReOrder($arr["children"]);
				echo '{"successFunction":function(){
				}}';
			}else{
				echo '{"successFunction":function(){
					alert(\'There was a problem with re-ordering the categories,\\nreload the page to get a true category order...\');
				}}';
				exit;
			}

		}
	}

	function reorderProducts(){
    if(_checkAuthentication($this)){
			$this->shop->tablename="SS_products";
			$arr=$this->input->post("product");
			if(is_array($arr)){
				foreach($arr as $order=>$id){
					@$this->shop->save("productID",$id,array("sort_order"=>$order));
				}
			}
			echo '{"success":"success","successFunction":function(){

			}}';
			exit;
		}
	}

	function activateDeactivateProducts(){
		if(_checkAuthentication($this)){
			$this->shop->tablename="SS_products";
			$id=$this->input->post("id");
			$res=$this->shop->get("productID",$id);
			if($res["enabled"]=="0"){
				$new_value="1";
			}else{
				$new_value="0";
			}
			if($this->shop->save("productID",$id,array("enabled"=>$new_value))){
				echo '{"success":"success",successFunction:function(){
						do_act_deactivateProd("'.$id.'","'.$new_value.'");
					}}';
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not toggle activation due to server error...\\nPlease try again");
					}}';
			}

			exit;
		}
	}

	function viewProduct($id=0){
	  if(_checkAuthentication($this)){
	    $this->shop->tablename="SS_products";
			$res=$this->shop->get("productID",$id);
			$this->shop->tablename="SS_tax_classes";
			$taxes=$this->shop->getAll();
			$taxOpts="<option value='null'>No Tax</option>";
			if(is_array($taxes)){
			  foreach ($taxes as $tax) {
			   $taxOpts.="<option value='$tax[classID]' ".(($tax["classID"]==$res["classID"])?("selected"):("")).">$tax[name]</option>";
			  }
			}
	    if(is_array($res) && count($res)>0){
	      $html='
	      <div class="tab_content padded" style="display:block">
	        <h1>Basic Options</h1><br />
          '."<form method='post' rel='ajaxed' action='".base_url()."admin/shop_catalog/saveProductDetails' loadingDiv='loader_save_page'>

          <table cellpadding='4' cellspacing='0'>
					  <tr>
						  <th align='left'><label>Name</label></th>
						  <td align='left' colspan='2'><input type='text' id='edit_pagename_input' name='name' value=\"".str_replace('"',"&#34;",$res["name"])."\"
						  style='width:300px'
						  class='stubSenders'
						  stubReceiver='edit_pagestub_input'
						  stubPageTitle='edit_pagetitle_input'
						   /></td>
					  </tr>
					  <tr>
						  <th align='left'><label>Page Title</label></th>
						  <td align='left' colspan='2'><input type='text' style='width:300px' id='edit_pagetitle_input' name='pagetitle' value=\"".str_replace('"',"&#34;",$res["pagetitle"])."\"  /></td>
					  </tr>
					  <tr>
						  <th align='left'><label>Stub</label></th>
						  <td align='left'><input type='text' name='stub' id='edit_pagestub_input' value=\"".$res["stub"]."\"
						  style='width:300px'
						  class='stubReceivers'
						   /></td>
						  <td width='20'><span id='edit_stubReturnValue' style='font-weight:bold;'></span></td>
					  </tr>
					  <tr>
						  <th align='left'><label>Product Code</label></th>
						  <td align='left'><input type='text' name='product_code' value=\"".str_replace('"',"&#34;",$res["product_code"])."\"
						  style='width:300px'
						   /></td>
					  </tr>
					  <tr>
						  <th align='left'><label>Price</label></th>
						  <td align='left'><input type='text' style='width:300px' name='Price' value=\"".str_replace('"',"&#34;",$res["Price"])."\"  /></td>
					  </tr>
						  <tr>
							  <th align='left'><label>Tax Class</label></th>
							  <td align='left'><select name='classID'>$taxOpts</select></td>
						  </tr>
              <tr><td colspan='3' align='right'>
              <hr />
						  <button type='submit' class='button save'>Save Changes</button>
						  <div id='loader_save_page' class='q_feedback'></div>
						  <input type='hidden' name='productID' value='$res[productID]' /></td></tr>
            </table>
					  </form>".'
	      </div>
	      <div class="tab_content" style="position:absolute;left:0;top:0;width:100%;height:100%;overflow:hidden;">
	        <div id="editArea">
            <form method="post" rel="ajaxed" action="'.base_url().'admin/shop_catalog/saveProductDescription" loadingDiv="loader_save_c">
				      <textarea name="content" style="position:absolute;width:100%;height:100%" id="EditableArea">'.htmlspecialchars($res["description"]).'</textarea>
				      <div class="footertoolbar" style="left:0;right:0;bottom:-31px;position:absolute;z-index:2">
						    <div style="float:right">
  							  <div class="q_feedback" id="loader_save_c" style="float:right"></div>
  							  <button type="submit" class="button save">Save Changes</button>
  							</div>
						  </div>
						  <input type="hidden" name="page_id" value="$page[id]" />
				    </form>
          </div>
	      </div>
	      <div class="tab_content padded">
	        <h1>Product Pictures</h1>

	      </div>
	      <div class="tab_content padded">
	        <h1>Additional Categories</h1>

	      </div>
	      <div class="tab_content padded">
	        <h1>Product Configuration</h1>

	      </div>
	      ';

	      $this->load->view("admin/edit_page",array(
  				"html"=>$html
  			));
	    }
    }
	}

	function saveProductDetails(){
	  if(_checkAuthentication($this)){
      $name=$this->input->post("name");
      $pagetitle=$this->input->post("pagetitle");
      $stub=$this->input->post("stub");
      $product_code=$this->input->post("product_code");
      $Price=$this->input->post("Price");
      $classID=$this->input->post("classID");
      $productID=$this->input->post("productID");

      $aggregate=$product_code." ".$name;
      if(!isset($stub) || $stub==""){
        $stub=preg_replace('/[-]{2,}/','-',preg_replace('/-$/','',preg_replace('/^-/','',preg_replace('/[&| \/\\*%\)\(\]\[\}\{\"\'=+–><?.`,:;\#]/',"-",trim(strtolower($aggregate))))));
      }

      $this->shop->tablename="SS_products";
      $res=$this->shop->save("productID",$productID,array(
        "name"=>$name,
        "pagetitle"=>$pagetitle,
        "stub"=>$stub,
        "product_code"=>$product_code,
        "Price"=>$Price,
        "classID"=>$classID
      ));
      if($res){
        echo '{"success":"success","successFunction":function(){
          $("#product_'.$productID.'").find("span.name").text("'.(($name!="")?($name):($product_code)).'")
          .end().find("span.extraData").text("'.sprintf("%01.2f",$Price).'")
        }}';
      }else{
        echo '{"success":"error","successFunction":function(){alert("There was a problem with saving changes, please reload page.")}}';
      }
    }
	}

  // ================================

  function _recursiveReOrder($arr,$parent_id=1,$currPageId=0){
		if(!is_array($arr)){
			return false;
		}
		$i=0;
		foreach($arr as $page){
			//*get page id
			$page_id=str_replace("cat_","",$page["attributes"]["id"]);

			$q=$this->shop->save("categoryID",$page_id,array(
				"parent"=>$parent_id,
				"sort_order"=>$i
				)
			);
			if(isset($page["children"]) && is_array($page["children"])){
				$this->_recursiveReOrder($page["children"],$page_id,$currPageId);
			}
			$i++;
			//*/
		}

	}

	private function _catTree($categories,$level=0,$parent=1){
  	$return = "<ul>";
  	if(is_array($categories)){
  		foreach($categories as $category){
  			if($category["level"]==$level && $category["parent"]==$parent){
  				$return.="<li id='cat_$category[categoryID]' rel='file'><a href='#'>$category[name]</a>";
  				if($category["ExistSubCategories"]){
  					$return.=$this->_catTree($categories,($level+1),$category["categoryID"]);
  				}
  				$return.="</li>";
  			}
  		}
  	}
  	$return.="</ul>";
  	return $return;
  }


}