$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // See More details on click
  $("#sidebar ul.list li").live("click",function(){
  	var _li=$(this);
  	_li.siblings().removeClass("active");
  	_li.addClass("active");
  	loadDetails(_li.attr("id"));
  	return false;
  });

  $("#filterBtn").live("click",function(){ 	
  	var date =$(".daterange").val();
  	var order_type=$("#order_type option:selected" ).val();
   
  	var date_range=date.split("-");

var start =date_range[0].replace(/\./g,"-");
var end=date_range[1].replace(/\./g,"-");


date=start+'to'+end;

var now = new Date();
var start=new Date(start);
var end =new Date(end);

var conditions=date+'type'+order_type;
conditions=conditions.replace(/\s/g, '');

loadFilteredOrderList(conditions);
});	
});
function loadFilteredOrderList(conditions){	
//alert(date+'inside loadFilteredOrderList');
	$(".sidebar-menu").load(base_url+"admin/order/filteredList/"+conditions);		
}
function loadDetails(id){
	
	showById("innerTabs1");
	$("#innerTabs").show();
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2]; 
	 $("#dContent").load(base_url+"admin/order/loadDetails/"+id);
	 $("#status").load(base_url+"admin/order/loadStatus/"+id);
	$("#thirdBar").show(0);
	
}

function showById(id){
	var total_tab=4;
	var i;
	for(i=1;i<=total_tab;i++)
	{
		var t="#"+"innerTabs"+i+"_content";
		$("#"+"innerTabs"+i+"_content").hide();
		$("#"+"innerTabs"+i).removeClass("active");
	}
	var id_content='#'+id+'_content';
	$(id_content).show();
	$("#"+id).addClass("active");

}

var body=$("body");

$(document).ready(function(){
	$("#innerTabs ul li").click(function(event){
		event.preventDefault();
		var id=$(this).attr('id');	 
		showById(id);    
	});


	$('#daterange').daterangepicker({
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			'Last 7 Days': [moment().subtract('days', 6), moment()],
			'Last 30 Days': [moment().subtract('days', 29), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		},
		startDate: moment().subtract('days', 29),
		endDate: moment()
	},
	function(start, end) {
		$('#daterange').val(start.format('YYYY.MM.DD') + ' - ' + end.format('YYYY.MM.DD'));
		//body.find("form").submit();
	});
});




