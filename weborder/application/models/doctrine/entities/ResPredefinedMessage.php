<?php



namespace entities;

/**
 * ResPredefinedMessage
 *
 * @Table(name="res_predefined_message")
 * @Entity
 */
class ResPredefinedMessage
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var smallint $weekDay
   *
   * @Column(name="week_day", type="smallint", nullable=false)
   */
  private $weekDay;
  
  /**
   * @var time $startTime
   *
   * @Column(name="start_time", type="time", nullable=false)
   */
  private $startTime;
  
  /**
   * @var time $endTime
   *
   * @Column(name="end_time", type="time", nullable=false)
   */
  private $endTime;
  
  /**
   * @var string $delayTime
   *
   * @Column(name="delay_time", type="string", length="5", nullable=false)
   */
  private $delayTime;
  
  /**
   * @var string $subject
   *
   * @Column(name="subject", type="string", length=150, nullable=false)
   */
  private $subject;
  
  /**
   * @var string $message
   *
   * @Column(name="message", type="string", length=1000, nullable=false)
   */
  private $message;
  
  /**
   * @var string $messageType
   *
   * @Column(name="message_type", type="string", nullable=true)
   */
  private $messageType;
  
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set weekDay
   *
   * @param smallint $weekDay
   */
  public function setWeekDay($weekDay)
  {
      $this->weekDay = $weekDay;
  }
  
  /**
   * Get weekDay
   *
   * @return smallint
   */
  public function getWeekDay()
  {
      return $this->weekDay;
  }
  
  /**
   * Set startTime
   *
   * @param time $startTime
   */
  public function setStartTime($startTime)
  {
      $this->startTime = $startTime;
  }
  
  /**
   * Get startTime
   *
   * @return time
   */
  public function getStartTime()
  {
      return $this->startTime;
  }
  
  /**
   * Set endTime
   *
   * @param time $endTime
   */
  public function setEndTime($endTime)
  {
      $this->endTime = $endTime;
  }
  
  /**
   * Get endTime
   *
   * @return time
   */
  public function getEndTime()
  {
      return $this->endTime;
  }
  
  /**
   * Set delayTime
   *
   * @param string $delayTime
   */
  public function setDelayTime($delayTime)
  {
      $this->delayTime = $delayTime;
  }
  
  /**
   * Get delayTime
   *
   * @return string
   */
  public function getDelayTime()
  {
      return $this->delayTime;
  }
  
  /**
   * Set message
   *
   * @param string $message
   */
  public function setMessage($message)
  {
  	$this->message = $message;
  }
  
  /**
   * Get message
   *
   * @return string
   */
  public function getMessage()
  {
  	return $this->message;
  }
  
  /**
   * Set subject
   *
   * @param string $subject
   */
  public function setSubject($subject)
  {
  	$this->subject = $subject;
  }
  
  /**
   * Get subject
   *
   * @return string
   */
  public function getSubject()
  {
  	return $this->subject;
  }
  
  /**
   * Set messageType
   *
   * @param string $messageType
   */
  public function setMessageType($messageType)
  {
  	$this->messageType = $messageType;
  }
  
  /**
   * Get messageType
   *
   * @return string
   */
  public function getMessageType()
  {
  	return $this->messageType;
  }
}