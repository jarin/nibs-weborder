<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="product";
		$this->load->model("activity_m","activity");
		$this->load->model("product_m","product");
		$this->load->model("Business_m","business");		
		
	}
	
	function index(){
		if(_checkAuthentication($this)){
			$Producthtml="";
			$business=$this->business->getRowBy('user_id',$this->_userid);
			$Product=$this->product->getProducts($business['id']);
			
			if(is_array($Product)){
				foreach($Product as $product){
					if($product["is_active"]==0){
						$class="off";
						$checked="";
					}else{
						$class="";
						$checked="checked";
					}
						
						
					$Producthtml.="<li id='product_$product[id]'>
					<a href='#'>
					<span class='name icon-box'><i class=''><img src='".base_url()."assets/images/admin/application_list.png'/></i></span>
					<span class='name nameBtn' title='$product[product_name]'>$product[product_name]</span>
					<span class='list_icon_btn'>
					
					<input class='main' id='checkboxmain_$product[id]' type='checkbox' $checked onclick='return false'/>
					</span>
					</a></li>";
				}
				}
				$temp="";
				$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
				$this->load->view("admin/product_template",array(
						"pagename"=>"Product Manager",
				"list1"=>$Producthtml
				));
				$this->load->view("admin/admin_foot");
		}
	}
	
	function loadDetails($id=""){
		if(_checkAuthentication($this)){
			$with =array();
			$without = array();
			$addon = array();
			$instructtion = false;
			if(trim($id)==""){
				echo '<span style="color:red">Error: </span>Product ID Not defined... Please reload page';
				exit;
			}
			
			$product=$this->product->getProduct($id);
			$addon=$this->product->getProductAddon($id);
			
			//echo '<pre>';print_r($product);echo '</pre>';
			
			if(count($product) == 0){
				echo '<span style="color:red">Error: Invalid Product</span>';
				exit;
			}
			foreach ($product as $k => $p){
					
				if($p['is_with'] == 1){
					
					if($p['ingredient_name'] == 'Special Instruction'){
						$instructtion = true;
					} else {
						$with[$k]['ingredient_name'] = $p['ingredient_name'];
						$with[$k]['price_with'] = $p['price_with'];
					}
				} else {
					
					if($p['ingredient_name'] == 'Special Instruction'){
						$instructtion = true;
					} else {
						$without[$k]['ingredient_name'] = $p['ingredient_name'];
						$without[$k]['price_without'] = $p['price_with'];
					}
				}
				
			}
			
			$html="
			<div class=''>
				<h3 class='page-title'>Product Details</h3><br />
				<div id='form_wizard_1' class='widget box'>
		            
	
		        <div class='widget-body form'>
				<div style='width:50%;text-align:left;'>
					<table cellpadding='4' cellspacing='0' align='center'>
					<tr>
					<th align='left' >Product Name</th>
					<td >".$product[0]['product_name']."</td>
					</tr>
							<tr>
					<th align='left' >Product Long Name</th>
					<td >".$product[0]['long_name']."</td>
					</tr>
					<tr>
					<th align='left' >Price</th>
					<td >&pound; ".$product[0]['web_order_price']."</td>
					</tr>
					<tr>
					<th align='left' >Description</th>
					<td >".$product[0]['description']."</td>
					</tr>
					<tr>
					<th align='left'>Category</th>
					<td >".$product[0]['category_name']."</td>
					</tr>
					
					<tr>
					<th align='left'>Favourite</th>
					<td >".(($product[0]['is_favourite'] == 1)? 'Yes':'No')."</td>
					</tr>
							
					<tr>
					<th align='left'>Active</th>
					<td >".(($product[0]['is_active'] == 0)? 'No':'Yes')."</td>
					</tr>
  				</table>
  			</div>";
			if(count($addon) > 0){
				$html .="<div style='width:50%;text-align:left;'>
						<h6 class='page-title' style='font-size: 20px;'>Add-On</h6><br />
					<table cellpadding='4' cellspacing='0' align='center'>
					";
				$html .="
						<tr><td>Name</td><td>Price</td></tr>";
				foreach ($addon as $ad){
					$html .="<tr><td >".$ad['addon_name']."</td><td> - &pound;" .$ad['web_order_price']. "</td></tr>";
				}
				$html .="</table></div>";
			
			}
			if(isset($product[0]['ingredient_name']) && $product[0]['ingredient_name'] !=''){
				$html .="<div style='width:50%;text-align:left;'>
						<h6 class='page-title' style='font-size: 20px;'>Ingredient</h6><br />
					<table cellpadding='4' cellspacing='0' align='center'>
						
						";
				if(count($with) > 0){
					$html .="<tr>
						<th align='left'>With:</th></tr>
						<tr><td>Name</td><td>Price</td></tr>";
					foreach ($with as $w){
						
						$html .="<tr><td >".$w['ingredient_name']."</td><td> + &pound;" .$w['price_with']. "</td></tr>";
						}
						
				}
				
				if(count($without) > 0){
					$html .="<tr>
						<th align='left'>Without:</th></tr>
						<tr><td>Name</td><td>Price</td></tr>";
					foreach ($without as $wo){
						$html .="<tr><td >".$wo['ingredient_name']."</td><td> - &pound;" .$wo['price_without']. "</td></tr>";
					}
					
				}
				if($instructtion){
					$html .="<tr><th align='left'>Additional Notes</th><td>Yes</td></tr>";
				}
					$html .="</table></div>";
				}
				
				
			$html.="
  		</div>
  	</div></div>
	  		
				";
	
				echo $html;
			}
		}
	

	
		
}