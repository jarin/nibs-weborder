<?php



namespace entities;

/**
 * ResConvertedToTakeAway
 *
 * @Table(name="res_converted_to_take_away")
 * @Entity
 */
class ResConvertedToTakeAway
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var smallint $quantity
   *
   * @Column(name="quantity", type="smallint", nullable=false)
   */
  private $quantity;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $customer;

  /**
   * @var ResOrder
   *
   * @ManyToOne(targetEntity="ResOrder")
   * @JoinColumns({
   *   @JoinColumn(name="order_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $order;

  /**
   * @var ResProduct
   *
   * @ManyToOne(targetEntity="ResProduct")
   * @JoinColumns({
   *   @JoinColumn(name="product_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $product;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;

  /**
   * @var ResQuantityType
   *
   * @ManyToOne(targetEntity="ResQuantityType")
   * @JoinColumns({
   *   @JoinColumn(name="quantity_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $quantityType;


  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set quantity
   *
   * @param smallint $quantity
   */
  public function setQuantity($quantity)
  {
    $this->quantity = $quantity;
  }

  /**
   * Get quantity
   *
   * @return smallint 
   */
  public function getQuantity()
  {
    return $this->quantity;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
    $this->customer = $customer;
  }

  /**
   * Get customer
   *
   * @return ResCustomer 
   */
  public function getCustomer()
  {
    return $this->customer;
  }

  /**
   * Set order
   *
   * @param ResOrder $order
   */
  public function setOrder(\ResOrder $order)
  {
    $this->order = $order;
  }

  /**
   * Get order
   *
   * @return ResOrder 
   */
  public function getOrder()
  {
    return $this->order;
  }

  /**
   * Set product
   *
   * @param ResProduct $product
   */
  public function setProduct(\ResProduct $product)
  {
    $this->product = $product;
  }

  /**
   * Get product
   *
   * @return ResProduct 
   */
  public function getProduct()
  {
    return $this->product;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }

  /**
   * Set quantityType
   *
   * @param ResQuantityType $quantityType
   */
  public function setQuantityType(\ResQuantityType $quantityType)
  {
    $this->quantityType = $quantityType;
  }

  /**
   * Get quantityType
   *
   * @return ResQuantityType 
   */
  public function getQuantityType()
  {
    return $this->quantityType;
  }
}