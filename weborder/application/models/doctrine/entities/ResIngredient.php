<?php

namespace entities;

/**
 * ResIngredient
 *
 * @Table(name="res_ingredient")
 * @Entity
 */
class ResIngredient
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $ingredientName
   *
   * @Column(name="ingredient_name", type="string", length=150, nullable=false)
   */
  private $ingredientName;

  /**
   * @var decimal $price
   *
   * @Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $price;
  
  /**
   * @var decimal $priceWithout
   *
   * @Column(name="price_without", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $priceWithout;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var bigint $restaurantId
   *
   * @Column(name="restaurant_id", type="bigint", nullable=true)
   */
  private $restaurantId;
  
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set ingredientName
   *
   * @param string $ingredientName
   */
  public function setIngredientName($ingredientName)
  {
    $this->ingredientName = $ingredientName;
  }

  /**
   * Get ingredientName
   *
   * @return string 
   */
  public function getIngredientName()
  {
    return $this->ingredientName;
  }

  /**
   * Set price
   *
   * @param decimal $price
   */
  public function setPrice($price)
  {
    $this->price = $price;
  }

  /**
   * Get price
   *
   * @return decimal 
   */
  public function getPrice()
  {
    return $this->price;
  }
  
  /**
   * Set priceWithout
   *
   * @param decimal $priceWithout
   */
  public function setPriceWithout($priceWithout)
  {
  	$this->priceWithout = $priceWithout;
  }
  
  /**
   * Get priceWithout
   *
   * @return decimal
   */
  public function getPriceWithout()
  {
  	return $this->priceWithout;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }
  
  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
  
  /**
   * Set restaurantId
   *
   * @param bigint $restaurantId
   */
  public function setRestaurantId($restaurantId)
  {
  	$this->restaurantId = $restaurantId;
  }
  
  /**
   * Get restaurantId
   *
   * @return bigint
   */
  public function getRestaurantId()
  {
  	return $this->restaurantId;
  }
}