$(function(){

  
});



function createTree(){
	//--------TREE
	$("#treeHolder").tree(
		{

		    selected    : false,        // FALSE or STRING or ARRAY
		    opened      : [],           // ARRAY OF INITIALLY OPENED NODES
		    languages   : [],           // ARRAY of string values (which will be used as CSS classes - so they must be valid)
		    path        : base_url+"assets/js/",        // FALSE or STRING (if false - will be autodetected)
		    cookies     : {prefix:"SEC"},        // FALSE or OBJECT (prefix, open, selected, opts - from jqCookie - expires, path, domain, secure)
		    ui      : {
		        dots        : true,     // BOOL - dots or no dots
		        rtl         : false,    // BOOL - is the tree right-to-left
		        animation   : 100,        // INT - duration of open/close animations in miliseconds
		        hover_mode  : true,     // SHOULD get_* functions chage focus or change hovered item
		        scroll_spd  : 4,
		        theme_path  : base_url+"assets/css/themes/",    // Path to themes
		        theme_name  : "default",// Name of theme
		        context     : [ 
		            {
		                id      : "create",
		                label   : "Create", 
		                icon    : "create.png",
		                visible : function (NODE, TREE_OBJ) { if(NODE.length != 1) return false; return TREE_OBJ.check("creatable", NODE); }, 
		                action  : function (NODE, TREE_OBJ) { TREE_OBJ.create(false, TREE_OBJ.get_node(NODE)); } 
		            },
		            "separator",

					{ 
		                id      : "edit",
		                label   : "Edit Content",
		                icon    : "edit.png",
		                visible : function (NODE, TREE_OBJ) { return NODE.attr("rel")!="folderparent";}, 
		                action  : function (NODE, TREE_OBJ) { 
			 				var $NODE=$(NODE);
							var id=$NODE.attr("id");
							var e=id.match(/(.+)[-=_](.+)/);
							var type=e[1];
							var id1=e[2];

							var url=base_url+"admin/content/viewPageContent/"+id1;

							$.fn.colorbox({width:"950px", height:"95%",iframe:true,title:"Edit Content",href:url,open:true,overlayClose:false});

						} 
		            },
		            { 
		                id      : "rename",
		                label   : "Rename", 
		                icon    : "rename.png",
		                visible : function (NODE, TREE_OBJ) { if(NODE.length != 1) return false; return TREE_OBJ.check("renameable", NODE); }, 
		                action  : function (NODE, TREE_OBJ) { TREE_OBJ.rename(); } 
		            },
		            { 
		                id      : "delete",
		                label   : "Delete",
		                icon    : "remove.png",
		                visible : function (NODE, TREE_OBJ) { var ok = true; $.each(NODE, function () { if(TREE_OBJ.check("deletable", this) == false) ok = false; return false; }); return ok; }, 
		                action  : function (NODE, TREE_OBJ) { $.each(NODE, function () { TREE_OBJ.remove(this); }); } 
		            }
		        ]
		    },
		    rules   : {
		        multiple    : false,
		        metadata    : false, 
		        type_attr   : "rel",
		        multitree   : false, 
		        createat    : "bottom",
		        use_inline  : false,
		        clickable   : "all",
		        renameable  : ["file"],
		        deletable   : ["file"],
		        creatable   : ["folderparent","file"],
		        draggable   : ["file"],
		        dragrules   : [
					"file inside file",
					"file before file",
					"file after file"

				], 
		        drag_copy   : false,
		        droppable   : [],
		        drag_button : "left"
		    },
		    lang : {
		        new_node    : "New Page",
		        loading     : "Loading ..."
		    },
		    callback    : { 
		        beforecreate: function(NODE,REF_NODE,TYPE,TREE_OBJ,PARENT) { 
					var loadingHTML="<img class='loader' border='0' src='"+base_url+"assets/images/admin/small_loader.gif' />";
					$("#treeLoader").html(loadingHTML);
					var $PARENT=$(PARENT);
					var $NODE=$(NODE).attr("rel","file");;
					if($PARENT.attr("id")=="treebase"){
						var type="treebase";
						var id="";
					}else{
						var e=$PARENT.attr("id").match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id=e[2];
					}

					var ret=false;

					var url=base_url+"admin/content/addPage";

					$.ajax({
						url:url,
						dataType:"json",
						type:"post",
						async:false,
						data:{type:type,id:id},
						success:function(data){
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();

							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
								$NODE.attr("id",data.newID).prepend("<input type='checkbox' id='"+data.newCheckBoxId+"' />");
								ret=true;
							}
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}
							$("#treeLoader").html("");

						}
					});

					return ret; 
				}, 
		        beforerename: function(NODE,LANG,TREE_OBJ) { 
					$NODE=$(NODE);
					$NODE.data("oldName",$NODE.children("a:eq(0)").text());
					return true; 
				}, 
		        beforedelete: function(NODE,TREE_OBJ,a,b,c) { 
					if(confirm("Are you sure you want to delete this page?\nSubpages, if any, will be orphaned !!")){
						var $NODE=$(NODE);
						var e=$NODE.attr("id").match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id=e[2];
						var loadingHTML="<img class='loader' border='0' src='"+base_url+"assets/images/admin/small_loader.gif' />";
						$("#treeLoader").html(loadingHTML);
						var ret=false;

						var url=base_url+"admin/content/deletePage/";

						$.ajax({
							url:url,
							dataType:"json",
							type:"post",
							async:false,
							data:{id:id},
							success:function(data){
								if($.isFunction(data.OVERRIDE)){
									data.OVERRIDE.apply();

								}
								if($.isFunction(data.successFunction)){
									data.successFunction.apply();
									ret=true;
								}
							},
							error:function(){
								if($.isFunction(ajaxFailure)){
									ajaxFailure.apply();
								}
								$("#treeLoader").html("");

							}
						});
					}

					return ret; 
				}, 

				onrename    : function(NODE,LANG,TREE_OBJ,RB) { 
					var $NODE=$(NODE);
					var oldName=$NODE.data("oldName");
					var newName=$NODE.children("a:eq(0)").text();
					var e=$NODE.attr("id").match(/(.+)[-=_](.+)/);
					var type=e[1];
					var id=e[2];
					var loadingHTML="<img class='loader' border='0' src='"+base_url+"assets/images/admin/small_loader.gif' />";
					$("#treeLoader").html(loadingHTML);

					var url=base_url+"admin/content/renamePage";

					$.ajax({
						url:url,
						dataType:"json",
						type:"post",
						data:{newName:newName,type:type,id:id},
						success:function(data){
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();
								$NODE.children("a:eq(0)").text(oldName);
							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
							}
							$("a",NODE).click();
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}
							$("#treeLoader").html("");
							$NODE.children("a:eq(0)").text(oldName);
						}
					});
				}, 
		        onmove      : function(NODE,REF_NODE,TYPE,TREE_OBJ,RB) { 
					var json=TREE_OBJ.getJSON();
					var loadingHTML="<img class='loader' border='0' src='"+base_url+"assets/images/admin/small_loader.gif' />";
					$("#treeLoader").html(loadingHTML);
					var url=base_url+"admin/content/reOrderPages";
					$.ajax({
						url:url,
						dataType:"json",
						type:"post",
						data:{data:JSON_stringify(json)},
						success:function(data){
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();
							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
							}
							$("a",NODE).click();
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}
							$("#treeLoader").html("");
						}
					});
				}, 


		        ondblclk    : function(NODE, TREE_OBJ) { TREE_OBJ.toggle_branch.call(TREE_OBJ, NODE); TREE_OBJ.select_branch.call(TREE_OBJ, NODE); },
				onchange : function (NODE, TREE_OBJ) { 
				  	var $NODE=$(NODE);
					var id=$NODE.attr("id");
					if(id!="treebase"){
						var e=id.match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id1=e[2];

						loadPageDetails(id);

					}else{
						$("#column2").empty();
					}
				}

		    }
		}
		);


	//--------END TREE
	
}
