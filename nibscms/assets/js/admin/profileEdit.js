	$(document).ready(function(){	

		$('#edit_opening_input').ptTimeSelect({
			containerClass: "timeCntr",
			containerWidth: "350px",
			setButtonLabel: "Select",
			minutesLabel: "min",
			hoursLabel: "Hrs"
		});
		$('#edit_closing_input').ptTimeSelect({
			containerClass: "timeCntr",
			containerWidth: "350px",
			setButtonLabel: "Select",
			minutesLabel: "min",
			hoursLabel: "Hrs"
		});

	});
	