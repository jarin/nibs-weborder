<div id="mainContent" class="onepane">
  <div id="mainContentInner" class="padded">
	<h1>Not Permitted</h1>
	<p>You do not have sufficient privileges to perform this action...<br />Please Contact you administrator to upgrade your clearance.</p>
</div>
</div>