#!/bin/bash

TARPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git stash
git pull origin master
git stash pop

#rm -rf ${TARPATH}/application/config/database.php
#cp /etc/nibsrms/database.php ${TARPATH}/application/config/database.php

curl http://nibsmail/orm/updateSchema
curl http://nibsmail/orm/loadFixture

chmod 0777 ${TARPATH}/gitpull.sh
