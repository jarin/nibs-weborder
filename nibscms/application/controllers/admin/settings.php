<?php

class Settings extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="settings";
		// $this->load->model("shophook_m","shop");
		$this->load->model("settings_m","settings");
        // $this->load->model("pages_gallery_m","gallery");
		//Settings
		$this->SETTINGS=array();
		_loadSettings($this);
	}

	function index(){
		if(_checkAuthentication($this)){
			$SETTINGS=$this->settings->getAll("group");

      $html="";$firstItem=true;
			$currentSet="";$list=array();

			if(is_array($SETTINGS)){
				foreach ($SETTINGS as $set) {
					$grp=$set["group"];
					$list[ucfirst(_uncamelize($grp," "))]=$grp;
				}
			}

			foreach($list as $k=>$v){
			  $html.="<li id='setting_$v'><a href='#' class=''><span class='icon-box'><i class='icon-cog'></i></span>$k</a></li>";
			}

			//Shop Settings
			/*
			$ShopSettings=$this->shop->getSettingGroups();

			foreach($ShopSettings as $sst){
			  if($sst["settings_groupID"]!=1){
			    $html.="<li id='setting_shop_$sst[settings_groupID]'><span>Shop &rarr; $sst[settings_group_name]</span></li>";
			  }
			}
			*/
			//End Shop Settings

			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
			$this->load->view("admin/settings_template",array(
				"pagename"=>"Site Settings",
				"list1"=>$html
			));
			$this->load->view("admin/admin_foot");
		}
	}

	function loadSettings($group){
	  if(_checkAuthentication($this)){

      $html="";

	    if(is_numeric($group)){
	      $settings=$this->shop->getSettings($group);
	      if(is_array($settings) && count($settings)>0){
	        $group=$this->shop->getSettingGroups($group);
	        $html.="<h1>Shop &rarr; ".$group["settings_group_name"]."</h1>";
	        $html.="<form method='post' action='".base_url()."admin/settings/saveSettings' rel='ajaxed' loadingDiv='settingsloader'>
    	    <div class='settingsTable'>
    	    <div class='fieldset'><table>";
    	    foreach ($settings as $set) {
    	      $html.="<tr><th>".$set["settings_title"]."</th><td>".$this->_generateShopField($set)."</td><td class='field_description'>$set[settings_description]</td></tr>";
    	    }
    	    $html.="</table></div>
    	    <hr />
    	    <div class='settingsTable'>
    	      <input type='hidden' name='isShopSetting' value='true' />
    	      <div style='float:right'>
    	        <div class='q_feedback' id='settingsloader' style='float:right'></div>
    	        <button class='button save' type='submit'>Save Settings</button>
    	      </div>
    	    </div>
    	    </form>";
	      }
	    }else{
	      $settings=$this->settings->getBy("group",$group);

  	    if(is_array($settings)){
  	      $html.="<h3 class='page-title'>".ucfirst(_uncamelize($group," "))."</h3>";
  	      $html.="

			<div id='form_wizard_1' class='widget box'>
    			<div class='widget-title'>
			        <h4>
			            <i class='icon-reorder'></i>
			            ".ucfirst(_uncamelize($group,''))." Form
			        </h4>
			    </div>
    
    			<div class='widget-body form'>
        			<form method='post' action='".base_url()."admin/settings/".(($settings[0]['type']=='file')?('saveSiteBanners'):('saveSettings'))."' rel='".(($settings[0]['type']=='file')?('ajaxedUpload'):('ajaxed'))."' loadingDiv='settingsloader' class='form-horizontal' ".(($settings[0]['type']=='file')?('enctype="multipart/form-data"'):('')).">
            		<div class='form-wizard'>             
		              <div class='tab-content'>
		              	<div class='settingsTable'>
		    	    		<div class='fieldset'>
		                		<table>";
				                  foreach ($settings as $set) {
				  	        $name=ucfirst(_uncamelize($set["name"]," "));
				            $html.="<tr><th>$name</th><td>".$this->_generateField($set)."</td><td class='field_description'>$set[description]</td></tr>";
				  	      }
			  	      $html.="</table>
			  	      	</div>
			  	      		<div class='form-actions clearfix settingsTable form_actions_button' >
				                <button class='button important resetButton reset' type='button' rel='$group'>Reset</button>
				            	<div style='float:right'>
						          	<div class='q_feedback' id='settingsloader' style='float:right'></div>
						          	<button class='button save' type='submit'>Save Settings</button>
				            	</div>
			              	</div>
			            </div>
			        </form>
			    </div>";

  	    	}
	   }
	    echo $html;
    }
}

	function saveSettings(){
		if(_checkAuthentication($this)){
		  $isShopSetting=$this->input->post("isShopSetting");
		  if($isShopSetting=="true"){
		    $this->_writeShopSettings($_POST);
  			echo '{success:"success",successFunction:function(){
  			  NIBS.notif.notify("success","Settings saved successfully.");
  			}}';
		  }else{
		    $this->_writeSettings($_POST);
  			echo '{success:"success",successFunction:function(){
  			  NIBS.notif.notify("success","Settings saved successfully.");
  			}}';
		  }
		}
	}

	function resetSettings(){
		if(_checkAuthentication($this)){
		  $group=$this->input->post("group");

			$post=array();
			if(trim($group)!=""){
			  $settings=$this->settings->getBy("group",$group);
			}else{
			  $settings=$this->settings->getAll();
			}

			if(is_array($settings)){
				foreach ($settings as $set) {
					$post[$set["name"]]="^";
				}
			}
			$this->_writeSettings($post);
			echo '{success:"success",successFunction:function(){
				NIBS.notif.notify("success","All settings have been reset to their default values.");
				loadSettings("setting_'.$group.'");
			}}';
		}
	}

	function _writeSettings($post=array()){
		$settings=$this->settings->getAll();
		if(is_array($settings)){
			foreach ($settings as $set) {
				if(isset($post[$set["name"]])){
					$this->settings->save($set["id"],array("value"=>$post[$set["name"]]));
				}
			}
		}
	}

	function _writeShopSettings($post=array()){
	  $this->shop->tablename="SS_settings";
		foreach ($post as $key => $value){
		  if(is_numeric($key)){
		    $this->shop->save("settingsID",$key,array("settings_value"=>$value));
		  }
		}
	}

	function _generateField($row,$attr="",$css=""){
		$type="text";
		$def=$row["default"];
		$enums="";
		if(trim($row["type"])!=""){
			$exp=explode(":",$row["type"]);
			$type=trim($exp[0]);
			if(isset($exp[1])){
				unset($exp[0]);
				$_exp=implode(":",$exp);
				$enums=$_exp;
			}
		}
		if(trim($row["value"])!="^"){
			$def=$row["value"];
		}
		switch($type){
			case "text":
				return "<input type='text' $attr style='$css' name='$row[name]' value='$def'  />";
			break;
			case "file":
				$path=$this->settings->_repo;
        		$path=str_replace("./assets",base_url()."assets",$path);
				return "<input type='file' $attr style='$css' name='$row[name]' value='$def'  />
				<span style='display:block;margin-top:12px'><img src='$path/$def' style='width:156px;height:80px'></span>";
			break;
			case "textarea":
				return "<textarea $attr style='$css' name='$row[name]' >$def</textarea>";
			break;
			case "select":
				$html="";
				$opts=explode(",",$enums);
				if(is_array($opts)){
					foreach ($opts as $opt) {
						$brk=explode("|",$opt);
						$nm=$vl=$brk[0];
						if(isset($brk[1])){
							unset($brk[0]);
							$_brk=implode("|",$brk);
							$nm=$_brk;
						}
						$html.="<option value='$vl'>$nm</option>";
					}
				}
				return "<select $attr style='$css' name='$row[name]'>
							"._sel_option($html,$def)."
						</select>";
			break;
            /*
			case "selectGallery":
				$html="<option value='0'>none</option>";
				$gallery=$this->gallery->getAll();
				if(is_array($gallery)){
					foreach($gallery as $gal){
						if($gal['active']!=0){
							$html.="<option value='$gal[id]'>$gal[name]</option>";
						}						
					}
				}
				return "<select $attr style='$css' name='$row[name]'>
							"._sel_option($html,$def)."
						</select>";
			break;
            */
			case "checkbox":
				return "<input type='checkbox' $attr style='$css' name='$row[name]' ".(($def==1)?("checked"):(""))."  />";
			break;
			case "radio":
				$html="";
				$opts=explode(",",$enums);
				if(is_array($opts)){
					foreach ($opts as $opt) {
						$brk=explode("|",$opt);
						$nm=$vl=$brk[0];
						if(isset($brk[1])){
							unset($brk[0]);
							$_brk=implode("|",$brk);
							$nm=$_brk;
						}
						$html.="<label><input type='radio' name='$row[name]' value='$vl' ".(($vl==$def)?("checked"):(""))."> $nm</label>";
					}
				}
				return $html;
			break;
		}
	}

	function _generateShopField($row,$attr="",$css=""){
    $fld=$row["settings_html_function"];
    $val=$row["settings_value"];
    $id=$row["settingsID"];
    $fldtp=str_replace("setting_","",array_shift(explode("(",$fld)));
    switch($fldtp){
      default:
        return $fldtp;
      break;
      case "TEXT_BOX":
        return "<input type='text' $attr style='$css' name='$id' value='$val' />";
      break;
      case "TEXT_AREA":
        return "<textarea $attr style='$css' name='$id'>$val</textarea>";
      break;
      case "CHECK_BOX":
        return "<label><input type='radio' name='$id' value='1' ".(($val==1)?("checked"):(""))."> Yes</label>".
        "<label><input type='radio' name='$id' value='0' ".(($val!=1)?("checked"):(""))."> No</label>";
      break;
      case "DATEFORMAT":
        return "<select name='$id'>"._sel_option("<option value='MM/DD/YYYY'>MM/DD/YYYY</option><option value='DD.MM.YYYY'>DD.MM.YYYY</option>",$val)."</select>";
      break;
      case "WEIGHT_UNIT":
        return "<select name='$id'>"._sel_option("<option value='lbs'>lbs - Pounds</option><option value='kg'>kg - Kilograms</option><option value='g'>g - Grams</option>",$val)."</select>";
      break;
      case "settingCONF_DEFAULT_COUNTRY":
      case "settingCONF_COUNTRY":
        $html="<select name='$id'>";
        $opt="<option value='0'>--Please Select--</option>";
        $ctry=$this->shop->getCountries();
        if(is_array($ctry)){
          foreach ($ctry as $ct) {
            $opt.="<option value='$ct[countryID]'>$ct[country_name]</option>";
          }
        }
        $html.=_sel_option($opt,$val);
        $html.="</select>";
        return $html;
      break;
      case "settingCONF_DEFAULT_CURRENCY":
        $html="<select name='$id'>";
        $opt="<option value='0'>--Please Select--</option>";
        $ctry=$this->shop->getCurrencies();
        if(is_array($ctry)){
          foreach ($ctry as $ct) {
            $opt.="<option value='$ct[CID]'>$ct[Name]</option>";
          }
        }
        $html.=_sel_option($opt,$val);
        $html.="</select>";
        return $html;
      break;
      case "settingCONF_DEFAULT_TAX_CLASS":
      case "settingCONF_CALCULATE_TAX_ON_SHIPPING":
        $html="<select name='$id'>";
        $opt="<option value='0'>None</option>";
        $ctry=$this->shop->getTaxClasses();
        if(is_array($ctry)){
          foreach ($ctry as $ct) {
            $opt.="<option value='$ct[classID]'>$ct[name]</option>";
          }
        }
        $html.=_sel_option($opt,$val);
        $html.="</select>";
        return $html;
      break;
      case "settingCONF_NEW_ORDER_STATUS":
      case "settingCONF_COMPLETED_ORDER_STATUS":
        $html="<select name='$id'>";
        $opt="<option value='0'>None</option>";
        $ctry=$this->shop->getOrderStatuses();
        if(is_array($ctry)){
          foreach ($ctry as $ct) {
            if($ct["statusID"]!=1){
              $opt.="<option value='$ct[statusID]'>$ct[status_name]</option>";
            }
          }
        }
        $html.=_sel_option($opt,$val);
        $html.="</select>";
        return $html;
      break;
    }
  }


  	function saveSiteBanners(){
        $showFoodRatingImage=$this->input->post("showFoodRatingImage");

		$path=$this->settings->_repo;
	    $this->load->library("upload",array(
	      "upload_path"=>$path,
	      "allowed_types"=>"gif|jpeg|jpg|png"
	    ));
        
		$files=array();
		foreach($_FILES as $key=>$value){
			if($value['size']!=0){
				$img_file=$value['name'];
			
				$this->upload->file_ext = $this->upload->get_extension($img_file);
	      $file=$this->upload->set_filename($path,$img_file);
						
				$files[$key]=$file;
                
                chmod($value['tmp_name'],0644);
			
				move_uploaded_file($value['tmp_name'], "$path/$file");
														
				$data[$key]=$file;
			}
		}
        
        $data['showFoodRatingImage']=$showFoodRatingImage;
		// print_r($data);
$n=0;
		$settings=$this->settings->getAll();
		// print_r($settings);
		if(is_array($settings)){
			foreach ($settings as $set) {
				if(is_array($data) && count($data)>0){
					foreach ($data as $d => $value) {
						if($set['name']==$d){
							$m=$this->settings->save($set["id"],array("value"=>$value));
							if($m){
                                // chmod($path.$value, 0644);    
								@$this->_resizePicture($value,156,80);
								$n=1;
							}
						}
					}
		
				}
				
			}
		}

		if($n==1){
			echo '{success:"success",successFunction:function(){
					$("#setting_foodRatingImage").click().find("li").html("");
					NIBS.notif.notify("success","Banner details saved");
				}}';

		}else{
			echo '{success:"error",msg:"Could not save to database..."}';
		}

    }

    function _resizePicture($filename="",$max_width=156,$max_height=80){
    $dir=$this->settings->_repo;
    if(file_exists($dir.$filename)){
      $ext=_file_extension($filename);
      list($width_orig, $height_orig)=getimagesize($dir.$filename);
      if(($width_orig==$max_width) &&  ($height_orig==$max_height)){
        return;
      }
      $sc=min(($max_width / $width_orig), ($max_height / $height_orig));
      if($sc < 1){
        $width=round($width_orig*$sc);
        $height=round($height_orig*$sc);
      }else{
        $width=$width_orig;
        $height=$height_orig;
      }


      $image_p = imagecreatetruecolor($width, $height);
      if($ext["extension"]==".gif"){
        $image = imagecreatefromgif($dir.$filename);
      }elseif($ext["extension"]==".png"){
        $image = imagecreatefrompng($dir.$filename);				
      }else{
        $image = imagecreatefromjpeg($dir.$filename);
      }
      imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

      $image_p2 = imagecreatetruecolor($max_width, $max_height);
      $base_color = imagecolorallocate($image_p2, 236, 236, 236);
      imagefill($image_p2, 0, 0, $base_color);
      imagecopy($image_p2, $image_p, (($max_width-$width)/2), (($max_height-$height)/2), 0, 0, $width, $height);

      $newFilename=$ext["filename"].$ext["extension"];
      if($ext["extension"]==".gif"){
        imagegif($image_p2, $dir.$newFilename, 100);
      }else{
        imagejpeg($image_p2, $dir.$newFilename);
      }

      //Free up resources
      ImageDestroy($image_p);
      ImageDestroy($image_p2);

      return;

    }
  }
	

}


?>
