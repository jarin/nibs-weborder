<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->tablename="app_dashboard";
		$this->user_tablename="app_users_dashboard";
	}

	function getUserWidgets($user_id){
	  return $this->db
	  ->where("user_id",$user_id)
	  ->order_by("sort_order","asc")->get($this->user_tablename)->result_array();
	}

	function getUserWidgetsJoined($user_id){
	  return $this->db
	  ->where("user_id",$user_id)
	  ->join($this->tablename, $this->tablename.".id = ".$this->user_tablename.".widget_id", 'left')
	  ->order_by($this->user_tablename.".sort_order","asc")->get($this->user_tablename)->result_array();
	}
	
	
}
?>