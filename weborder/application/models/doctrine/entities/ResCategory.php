<?php



namespace entities;

/**
 * ResCategory
 *
 * @Table(name="res_category")
 * @Entity
 */
class ResCategory
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=true)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var smallint $parendId
   *
   * @Column(name="parent_id", type="smallint", nullable=true)
   */
  private $parentId;
  
  /**
   * @var smallint $sequenceNumber
   *
   * @Column(name="sequence_number", type="smallint", nullable=false)
   */
  private $sequenceNumber;

  /**
   * @var string $categoryName
   *
   * @Column(name="category_name", type="string", length=100, nullable=false)
   */
  private $categoryName;

  /**
   * @var string $description
   *
   * @Column(name="description", type="string", length=150, nullable=true)
   */
  private $description;

  /**
   * @var string $topColor
   *
   * @Column(name="top_color", type="string", length=9, nullable=false)
   */
  private $topColor;
  
  /**
   * @var string $bottomColor
   *
   * @Column(name="bottom_color", type="string", length=9, nullable=false)
   */
  private $bottomColor;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;
  
  /**
   * @var boolean $isBar
   *
   * @Column(name="is_bar", type="boolean", nullable=false)
   */
  private $isBar=FALSE;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;

  /**
   * @var ResCategory
   *
   * @ManyToOne(targetEntity="ResCategory")
   * @JoinColumns({
   *   @JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $parent;

  /**
   * @var bigint $restaurantId
   *
   * @Column(name="restaurant_id", type="bigint", nullable=true)
   */
  private $restaurantId;

  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set categoryName
   *
   * @param string $categoryName
   */
  public function setCategoryName($categoryName)
  {
    $this->categoryName = $categoryName;
  }

  /**
   * Get categoryName
   *
   * @return string 
   */
  public function getCategoryName()
  {
    return $this->categoryName;
  }

  /**
   * Set description
   *
   * @param string $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * Get description
   *
   * @return string 
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Set topColor
   *
   * @param string $topColor
   */
  public function setTopColor($topColor)
  {
  	$this->topColor = $topColor;
  }
  
  /**
   * Get topColor
   *
   * @return string
   */
  public function getTopColor()
  {
  	return $this->topColor;
  }
  
  /**
   * Set bottomColor
   *
   * @param string $bottomColor
   */
  public function setBottomColor($bottomColor)
  {
  	$this->bottomColor = $bottomColor;
  }
  
  /**
   * Get bottomColor
   *
   * @return string
   */
  public function getBottomColor()
  {
  	return $this->bottomColor;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }
  
  /**
   * Set isBar
   *
   * @param boolean $isBar
   */
  public function setIsBar($isBar)
  {
  	$this->isBar = $isBar;
  }
  
  /**
   * Get isBar
   *
   * @return boolean
   */
  public function getIsBar()
  {
  	return $this->isBar;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }

  /**
   * Set parent
   *
   * @param ResCategory $parent
   */
  public function setParent(\ResCategory $parent)
  {
    $this->parent = $parent;
  }

  /**
   * Get parent
   *
   * @return ResCategory 
   */
  public function getParent()
  {
    return $this->parent;
  }
  
  /**
   * Set sequenceNumber
   *
   * @param smallint $sequenceNumber
   */
  public function setSequenceNumber($sequenceNumber)
  {
  	$this->sequenceNumber = $sequenceNumber;
  }
  
  /**
   * Get sequenceNumber
   *
   * @return smallint
   */
  public function getSequenceNumber()
  {
  	return $this->sequenceNumber;
  }
  
  /**
   * Set restaurantId
   *
   * @param bigint $restaurantId
   */
  public function setRestaurantId($restaurantId)
  {
  	$this->restaurantId = $restaurantId;
  }
  
  /**
   * Get restaurantId
   *
   * @return bigint
   */
  public function getRestaurantId()
  {
  	return $this->restaurantId;
  }
}