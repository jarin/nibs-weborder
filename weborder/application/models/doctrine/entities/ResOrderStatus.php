<?php



namespace entities;

/**
 * ResOrderStatus
 *
 * @Table(name="res_order_status")
 * @Entity
 */
class ResOrderStatus
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $status
   *
   * @Column(name="status", type="string", length=150, nullable=false)
   */
  private $status;
  
  /**
   * @var string $topColor
   *
   * @Column(name="top_color", type="string", length=9, nullable=false)
   */
  private $topColor;
  
  /**
   * @var string $bottomColor
   *
   * @Column(name="bottom_color", type="string", length=9, nullable=false)
   */
  private $bottomColor;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set status
   *
   * @param string $status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * Get status
   *
   * @return string 
   */
  public function getStatus()
  {
    return $this->status;
  }
  
  /**
   * Set topColor
   *
   * @param string $topColor
   */
  public function setTopColor($topColor)
  {
  	$this->topColor = $topColor;
  }
  
  /**
   * Get topColor
   *
   * @return string
   */
  public function getTopColor()
  {
  	return $this->topColor;
  }
  
  /**
   * Set bottomColor
   *
   * @param string $bottomColor
   */
  public function setBottomColor($bottomColor)
  {
  	$this->bottomColor = $bottomColor;
  }
  
  /**
   * Get bottomColor
   *
   * @return string
   */
  public function getBottomColor()
  {
  	return $this->bottomColor;
  }
}