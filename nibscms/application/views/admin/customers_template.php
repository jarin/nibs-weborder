<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/customers.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/jquery.livequery.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/calendar.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/customers.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
  <div class="padded"><h1><?=$pagename?></h1>
  <div class="initPage">
    <div>
      <p>Click on the list of Entries on the left to view the customers....</p>
    </div>
  </div></div>
</script>
<div id="thirdBar" class="secondpane">
  <div class='headertoolbar' style='padding-left:0.6em'>Entries</div>
  <div class="placeholder">No Customers</div>
  <ul class="comp_multilineListView nofooter">
    <?=$list1?>
  </ul>
</div>
<div id="mainContent" class="threepane lastpane">
  <div id="mainContentInner" class="">
    <div class="hidden">
      <div id="addCustomerForm">
       <form id='add_customer_form' method='post' rel='ajaxed' action='<?=$baseUrl?>admin/customers/addCustomer' loadingDiv='loader_add_customer' formType="adder">
			<table cellpadding='4' cellspacing='0' align='center'>
				<tr>
					<th style="width:122px">First Name</th>
					<td colspan="3"><input type='text' name='first_name'  /></td>
				</tr>
				<tr>
					<th>Last Name</th>
					<td colspan="3"><input type='text' name='last_name'  /></td>
				</tr>
				<tr><td colspan='4'><hr class='hr' /></td></tr>
				<tr>
					<th>Telephone</th>
					<td><input type='text' name='tel[]'  /></td>
					<td><select name="tel_type[]" style="min-width:80px">
					  <option value="home">Home</option>
					  <option value="mobile">Mobile</option>
					  <option value="office">Office</option>
					  <option value="other">Other</option>
					</select></td>
					<td style="text-align:left"><img class="adder" src="<?=$baseUrl?>assets/images/admin/icon_add.png" /></td>
				</tr>
				<tr><td colspan='4'><hr class='hr' /></td></tr>
				<tr>
					<th>Email</th>
					<td><input type='text' name='email[]'  /></td>
					<td><select name="email_type[]" style="min-width:80px">
					  <option value="home">Home</option>
					  <option value="office">Office</option>
					  <option value="other">Other</option>
					</select></td>
					<td style="text-align:left"><img class="adder" src="<?=$baseUrl?>assets/images/admin/icon_add.png" /></td>
				</tr>
				<tr><td colspan='4'><hr class='hr' /></td></tr>
				<tr>
				  <td colspan="2">
  				  <table cellpadding='4' cellspacing='0' width="100%">
  				    <tr>
      					<th style="width:122px">Address Line 1</th>
      					<td><input type='text' name='address_line1[]'  /></td>
      				</tr>
      				<tr>
      					<th>Address Line 2</th>
      					<td><input type='text' name='address_line2[]'  /></td>
      				</tr>
      				<tr>
      					<th>Town</th>
      					<td><input type='text' name='town[]'  /></td>
      				</tr>
      				<tr>
      					<th>County</th>
      					<td><input type='text' name='county[]'  /></td>
      				</tr>
      				<tr>
      					<th>Postcode</th>
      					<td><input type='text' name='postcode[]'  /></td>
      				</tr>
      				<tr>
      					<td><hr class='hr' /></td>
      				</tr>
  				  </table>
  				</td>
  				<td><select name="address_type[]" style="min-width:80px">
					  <option value="home">Home</option>
					  <option value="office">Office</option>
					  <option value="other">Other</option>
					</select></td>
			    <td style="text-align:left"><img class="adder" src="<?=$baseUrl?>assets/images/admin/icon_add.png" /></td>
			  </tr>
				<tr><td colspan='4'><hr class='hr' /></td></tr>
				<tr>
					<th>Memorable Date</th>
					<td><input type='text' class='date' name='date[]'  /></td>
					<td><select class="hasOther" name="date_type[]">
					  <option value="birthday">Birthday</option>
					  <option value="anniversary">Anniversary</option>
					  <option value="other">Other</option>
					</select><input type="text" class="isOther" name="date_othertype[]"></td>
					<td style="text-align:left"><img class="adder" src="<?=$baseUrl?>assets/images/admin/icon_add.png" /></td>
				</tr>
				<tr><td colspan='4'><hr class='hr' /></td></tr>
				<tr>
					<th>Notes</th>
					<td colspan="3"><textarea name="notes" style="height:100px"></textarea></td>
				</tr>
				<tr><td colspan='4'><hr class='hr' /></td></tr>
				<td align='right' colspan='4'><button class="button add" type='submit'>Add Customer</button><div id='loader_add_customer'  class='q_feedback'></div></td> </tr>
			</table>
		  </form>
      </div>
    </div>
    <div id="dContent">

    </div>
  </div>
</div>
<div id="sideBar" class="samelevel">
  <div class="headertoolbar" style="position:relative;">Filters <button class="addCustomerBtn" title="Add Customer"><img src="<?=$baseUrl?>assets/images/admin/icon_add.png" /></button></div>
    <ul class="shortlist nofooter">
      <li><span><b>Show Customers:</b></span>
        <ul>
          <li class='clickable active' rel='all'><span>All</span></li>
          <li class='clickable' rel='today'><span>Dates Today</span></li>
          <li class='clickable' rel='tomorrow'><span>Dates Tomorrow</span></li>
          <li class='clickable' rel='week'><span>Dates This Week</span></li>
          <li class='clickable' rel='nextweek'><span>Dates Next Week</span></li>
          <li class='clickable' rel='month'><span>Dates This Month</span></li>
          <li class='clickable' rel='nextmonth'><span>Dates Next Month</span></li>
        </ul>
      </li>
      <li><hr class="hr"></li>
      <li><span><input type='text' placeholder='Search by text' id='bQuery' /></span></li>
      <li><span style='text-align:right;'><button class='button' style='font-size:12px;line-height:20px' id='searchcustomers'>Go</button></span></li>
      <li><span><hr class="hr"></span></li>
      <li><span><input type='text' placeholder='Search by date' class='date' id='bDate' /></span></li>
      <li><span style='text-align:right;'><button class='button' style='font-size:12px;line-height:20px' id='searchdates'>Go</button></span></li>
    </ul>
</div>
<script type="text/javascript">
  var _KA=window.setInterval(function(){
    $.ajax({
      url:base_url+"admin/pages/XkeepAlive",
      dataType:"json",
      type:"post",
      data:{"keepAlive":"true"}
    })
  },180000);

</script>
