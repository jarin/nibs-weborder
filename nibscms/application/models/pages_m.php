<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->tablename="app_pages";
		$this->autosave_tablename="app_pages_autosave";
	}

	function getMainVisible(){
		return $this->db->where("active",1)
		->order_by("sort_order")->get($this->tablename)->result_array();
	}

	function getSecondaryVisible(){
		return $this->db->where("active_2",1)
		->order_by("sort_order")->get($this->tablename)->result_array();
	}

	function getTertiaryVisible(){
		return $this->db->where("active_3",1)
		->order_by("sort_order")->get($this->tablename)->result_array();
	}

	function autosave($page_id,$user_id,$content){
	  $res=$this->getAutoSaved($page_id,$user_id);
	  if(is_array($res) && isset($res["id"])){
	    $arr=array(
  			"modifieddate"=>time(),
  			"content"=>$content
  			);
  		return $this->db->update($this->autosave_tablename,$arr,array("user_id"=>$user_id,"page_id"=>$page_id));
	  }else{
	    $arr=array(
  			"modifieddate"=>time(),
  			"user_id"=>$user_id,
  			"content"=>$content,
  			"page_id"=>$page_id
  			);
  		return $this->db->insert($this->autosave_tablename,$arr);
	  }

	}

	function getAutoSaved($page_id,$user_id){
	  return $this->db->where("page_id",$page_id)
	  ->where("user_id",$user_id)
		->get($this->autosave_tablename)->row_array();
	}

	function deleteAutoSave($page_id,$user_id){
	  return $this->db->where("page_id",$page_id)
	  ->where("user_id",$user_id)
	  ->delete($this->autosave_tablename);
	}

	function deleteAutoSaves($page_id){
	  return $this->db->where("page_id",$page_id)->delete($this->autosave_tablename);
	}

	function getAllPages($user_id,$order_by){
		return $this->db->where("created_by",$user_id)
		->order_by($order_by,'asc')->get($this->tablename)->result_array();
	}
	
	function getstubAvailabe($user_id,$stub){
		return $this->db->where("created_by",$user_id)
		->where("stub",$stub)
		->get($this->tablename)->result_array();
	}

}
?>