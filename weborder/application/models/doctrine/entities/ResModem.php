<?php



namespace entities;

/**
 * ResModem
 *
 * @Table(name="res_modem")
 * @Entity
 */
class ResModem
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var string $modemName
   *
   * @Column(name="modem_name", type="string", length=45, nullable=true)
   */
  private $modemName;
  
  /**
   * @var string $modemDevice
   *
   * @Column(name="modem_device_id", type="string", length=200, nullable=true)
   */
  private $modemDevice;
  
  /**
   * @var string $ipAddress
   *
   * @Column(name="ip_address", type="string", length=45, nullable=true)
   */
  private $ipAddress;
  
  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;
  
  /**
   * @var datetime $queryTimer
   *
   * @Column(name="query_timer", type="time", nullable=true)
   */
  private $queryTimer;
  

  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set modemName
   *
   * @param string $modemName
   */
  public function setModemName($modemName)
  {
  	$this->modemName = $modemName;
  }
  
  /**
   * Get modemName
   *
   * @return string
   */
  public function getModemName()
  {
  	return $this->modemName;
  }
  
  /**
   * Set modemDevice
   *
   * @param string $modemDevice
   */
  public function setModemDevice($modemDevice)
  {
  	$this->modemDevice = $modemDevice;
  }
  
  /**
   * Get modemDevice
   *
   * @return string
   */
  public function getModemDevice()
  {
  	return $this->modemDevice;
  }
  
  /**
   * Set ipAddress
   *
   * @param string $ipAddress
   */
  public function setIpAddress($ipAddress)
  {
  	$this->ipAddress = $ipAddress;
  }
  
  /**
   * Get ipAddress
   *
   * @return string
   */
  public function getIpAddress()
  {
  	return $this->ipAddress;
  }
  
	/**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
  	$this->isActive = $isActive;
  }
  
  /**
   * Get isActive
   *
   * @return boolean
   */
  public function getIsActive()
  {
  	return $this->isActive;
  }
  
  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
  	$this->lastUpdatedBy = $lastUpdatedBy;
  }
  
  /**
   * Get lastUpdatedBy
   *
   * @return ResUser
   */
  public function getLastUpdatedBy()
  {
  	return $this->lastUpdatedBy;
  }
  
  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
  	$this->lastUpdate = $lastUpdate;
  }
  
  /**
   * Get lastUpdate
   *
   * @return datetime
   */
  public function getLastUpdate()
  {
  	return $this->lastUpdate;
  }
  
  /**
   * Set queryTimer
   *
   * @param time $queryTimer
   */
  public function setQueryTimer($queryTimer)
  {
  	$this->queryTimer = $queryTimer;
  }
  
  /**
   * Get queryTimer
   *
   * @return time
   */
  public function getQueryTimer()
  {
  	return $this->queryTimer;
  }
}