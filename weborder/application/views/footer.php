<!-- footer begin -->
			<footer id="footer">
				<div class="container">
					<div class="main-footer">
						<div class="row">
							<div class="col-sm-6 col-md-3">
								<img src="<?=str_replace('weborder/../','',base_url().$logo_path)?>" alt="">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed.
								</p>
							</div>

							<div class="col-sm-6 col-md-3">
								<h5>Quick Links</h5>
								<div class="row">
									<div class="col-md-6">
										<ul class="footer-links padd">
											<li><a href="#">Home</a>
											</li>
											<li><a href="#">Menu Card</a>
											</li>
											<li><a href="#">reservation</a>
											</li>
										</ul>
									</div>

									<div class="col-md-6">
										<ul class="footer-links">
											<li><a href="#">about us</a>
											</li>
											<li><a href="#">news & events</a>
											</li>
											<li><a href="#">contact us</a>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-md-3">
								<h5>Latest Tweets
									<span><i class="fa fa-chevron-left"></i>  <i class="fa fa-chevron-right"></i>
									</span>
								</h5>

								<p><a href="#">takeaway:</a> Take a (Photo) Tour of #Envato's #Melbourne Headquarters (...)
									<span>7 hours ago</span>
								</p>
							</div>

							<div class="col-sm-6 col-md-3">
								<h5>Newsletter</h5>
								<p>Sign up for our newsletter!</p>
								<div class="footer-subscribe">
									<form>
										<input type="Email" placeholder="Email address...">
										<button type="submit" value=""><i class="fa fa-plus-circle-o"></i></button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="footer-copyright">
					<div class="container">
						<p>Copyright 2014 &copy; TakeAway. All rights reserved. Powered by <a href="#">UOUapps</a>.</p>
						<ul class="footer-social">
							<li><a href="#"><i class="fa fa-facebook-square"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-twitter-square"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-google-plus-square"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-linkedin-square"></i></a>
							</li>
							<li><a href="#"><i class="fa fa-pinterest-square"></i></a>
							</li>
						</ul>
						<!-- end .footer-social -->
					</div>
				</div>
			</footer>
			<!-- end #footer -->
		</div>
	</div>
		<!-- end #main-wrapper -->


<script src="<?=base_url()?>assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?=base_url()?>assets/js/owl.carousel.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>assets/js/scripts.js"></script>
<script src="<?=base_url()?>assets/js/js.js"></script>

<?php  if($this->session->flashdata('restaurantError') !=''){ ?>
		<script type="text/javascript">
			 $('#cartsucces').html('We don\'t have this restaurant');
			 $('#cartsucces').show(0);
			 setTimeout(function(){ $('#cartsucces').hide(0); } , 2000);
		</script>';
<?php } ?>
<script type="text/javascript">
  (function() {
   var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
   po.src = 'https://apis.google.com/js/client:plusone.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
  $(document).ready(function(){
	  $('.fb_link').click(function(e){
		  var _this = $(this),
		  name = _this.attr('data-name'),
		  description = _this.attr('data-description'),
		  price = _this.attr('data-price');
	  e.preventDefault();
	  FB.ui(
	  {
	  method: 'feed',
	  name: '<?=$this->session->userdata('restaurantname')?>',
	  link: '<?=base_url()?>',
	  picture: '<?=str_replace('weborder/../','',base_url().$logo_path)?>',
	  caption: name+' price &pound;'+price,
	  description: description,
	  message: ''
	  });
	  });

	
	  });
 
</script>

</body>
</html>