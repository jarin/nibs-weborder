<?php


function uglify($path){
  $ch = curl_init();

  $data = '';
  $data .= 'js_code='.urlencode(file_get_contents($path));
  $data .= '&'.'download=_temp.js';


  curl_setopt($ch, CURLOPT_URL, "http://marijnhaverbeke.nl/uglifyjs");
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $js = curl_exec($ch);
  curl_close($ch);
  return $js;
}





?>