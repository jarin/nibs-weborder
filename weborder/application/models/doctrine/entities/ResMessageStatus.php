<?php



namespace entities;

/**
 * ResMessageStatus
 *
 * @Table(name="res_message_status")
 * @Entity
 */
class ResMessageStatus
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var ResMessage
   *
   * @ManyToOne(targetEntity="ResMessage")
   * @JoinColumns({
   *   @JoinColumn(name="message_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $messageId;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="reader", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $reader;
  
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set messageId
   *
   * @param ResMessage $messageId
   */
  public function setMessageId(\ResMessage $messageId)
  {
  	$this->messageId = $messageId;
  }
  
  /**
   * Get messageId
   *
   * @return ResMessage
   */
  public function getMessageId()
  {
  	return $this->messageId;
  }
  
  /**
   * Set reader
   *
   * @param ResUser $reader
   */
  public function setReader(\ResUser $reader)
  {
  	$this->reader = $reader;
  }
  
  /**
   * Get reader
   *
   * @return ResUser
   */
  public function getReader()
  {
  	return $this->reader;
  }
}