<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/generalsetting.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/generalsetting.js"></script>

<div id="main-content" class='onepane'>
  	<div class="container-fluid">
	    <div class='span12'>
	    	<h3 class='page-title'>General Settings</h3><br />
					<table cellpadding='4' cellspacing='0' align='center' class="table table-striped table-bordered table-hover">
						<?php if(count($settingsAr) > 0){
						foreach ($settingsAr as $st){ ?>
						<tr class="editlink" rel="<?=$st?>">
							<th align='left' ><?=_uncamelizeextended(camelize($st,'_'))?></th>
							<td >
							<?php if(count($settings) > 0){
								foreach ($settings as $key=>$setting){
									if($setting['settings_name'] == $st){
										echo $setting['value'];
									}
								}
							}else {
								echo '&nbsp;';
							}?>
							</td>
						</tr>
						<?php } } ?>
					</table>
				
	    	
	      								<!-- Modal -->
                                        <div id="editItemModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:600px;left:50%;top:20%;">
                                          <div style="background: #fff;">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                            <h3 id="myModalLabel" style="font-weight: bold;">Edit Settings</h3>
                                          </div>
                                          <form class="default-form" id='add_business_form' method='post' rel='ajaxed' action='<?=$baseUrl?>admin/generalsetting/saveSetting' loadingDiv='loader_add_user' formType="adder">
                                              <div class="modal-body modal_content">
                                                
                                              </div>
                                              <div class="modal-footer">
                                                <button class="btn btn-default-red" name="submit" type="submit" >Save</button>
                                                <button class="btn btn-default-black" data-dismiss="modal" aria-hidden="true">Close</button>
                                              </div>
                                          </form>
                                          </div>
                                        </div>
                                        <!-- END Modal -->
	  	</div>
	</div>
</div>

