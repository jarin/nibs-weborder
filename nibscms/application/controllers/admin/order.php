	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Order extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->_userid=$this->session->userdata("uid");
			$this->_username=$this->session->userdata("uname");
			$this->_utype=$this->session->userdata("utype");
			$this->_userImg=$this->session->userdata("uimg");
			$this->load->library("json");
			$this->load->helper("generic");

			//controller Specific
			$this->tab="order";
			$this->load->model("activity_m","activity");
			$this->load->model("order_m","order");
			
			
		}
		
		function index(){
 
			if(_checkAuthentication($this)){
				$orderhtml="";
				$today_date=date("Y.m.d");
			
				$dates = $today_date.'-'.$today_date;

				$uid=$this->session->CI->_userid;
				$web_bussiness=$this->order->getweb_business($uid);

				if(!empty($web_bussiness[0]['id'])){

					$orders=$this->order->getOrders($web_bussiness[0]['id']);
					
						
					if(empty($orders)){

							$orderhtml.="<li id='order_404'>
							<p> There is no order </p>							
						</li>";
						$temp="";
						$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
						$this->load->view("admin/order_template",array(
							"pagename"=>"Order Manager",
							"dates"=>$dates,
							"list1"=>$orderhtml
							));
						$this->load->view("admin/admin_foot");

					
				}
				else{
					if(is_array($orders)){
						foreach($orders as $order){	

							$orderhtml.="<li id='order_$order[id]'>
							<a href='#'>
								<span class='name icon-box'><i class=''><img src='".base_url()."assets/images/admin/application_list.png'/></i></span>
								<span class='name nameBtn' title='$order[id]'>Order: $order[id] $order[order_date]</span>					
							</a></li>";
						}
					}
					$temp="";
					$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
					$this->load->view("admin/order_template",array(
						"pagename"=>"Order Manager",
						"dates"=>$dates,
						"list1"=>$orderhtml
						));
					$this->load->view("admin/admin_foot");

				}	

			}

			else{
				$orderhtml.="<li id='order_404'>
				<p> There is no order. </p>							
			</li>";
			$temp="";
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
			$this->load->view("admin/order_template",array(
				"pagename"=>"Order Manager",
				"dates"=>$dates,
				"list1"=>$orderhtml
				));
			$this->load->view("admin/admin_foot");

		}
	}
								
}

function filteredList($condition)
{
	
	$cond=explode('type', $condition);
	  if(isset($cond[0]))
	  $date_range =explode('to',$cond[0]);
	 if(isset($cond[1]))
	  $order_type =$cond[1];

		if(_checkAuthentication($this)){
                 $orderhtml="";
				$uid=$this->session->CI->_userid;
				$web_bussiness=$this->order->getweb_business($uid);
			
				if(!empty($web_bussiness[0]['id'])){                     
                  
			 if(isset($date_range[1]))		
		    $end=date('Y-m-d', strtotime('+1 day', strtotime($date_range[1])));  
		    if(isset($date_range[0]))        
			$orders=$this->order->getOrdersByFilter($web_bussiness[0]['id'],$date_range[0], $end,$order_type);	
											
					if(empty($orders)){

							$orderhtml.="<li id='order_404'>
							<p> There is no order. </p>							
						</li>";
						
						$this->load->view("admin/filter_order_template",array(
							"pagename"=>"Order Manager",
							"list1"=>$orderhtml
							));
									
				}
				else{
					if(is_array($orders)){
						foreach($orders as $order){	

							$orderhtml.="<li id='order_$order[id]'>
							<a href='#'>
								<span class='name icon-box'><i class=''><img src='".base_url()."assets/images/admin/application_list.png'/></i></span>
								<span class='name nameBtn' title='$order[id]'>Order: $order[id] $order[order_date]</span>					
							</a></li>";
						}
					}
				
					$this->load->view("admin/filter_order_template",array(
						"pagename"=>"Order Manager",
						"list1"=>$orderhtml
						));
		

				}	
			}

			else{
				$orderhtml.="<li id='order_404'>
				<p> There is no order . </p>							
			</li>";
		
	
			$this->load->view("admin/filter_order_template",array(
				"pagename"=>"Order Manager",
				"list1"=>$orderhtml
				));
			
		}
	}
    
}

function find_array_by_key($arrays, $key, $keyValue)
{
	foreach ($arrays as $array) {
		if($array[$key]==$keyValue)

			return $array;           	
	}
}
function loadStatus($id=""){
	$all_info=$this->order->getOrder($id);
	echo $all_info['order'][0]['web_order_type'].' '.$all_info['order'][0]['order_date'];
}
function loadDetails($id=""){
	$all_info=$this->order->getOrder($id);
    $customer_id =$all_info['order'][0]['customer_id'];	
	$customer_info= $this->order->customer_infoById($customer_id);

	if(_checkAuthentication($this)){
		if(trim($id)==""){
			echo '<span style="color:red">Error: </span>Order ID Not defined... Please reload page';
			exit;
		}				
		$html="
		<div class=''>
		    <div id='innerTabs1_content'>
			<h3 class='page-title' >Order Details</h3><br />
			<div id='form_wizard_1' class='widget box'>		            
				<div class='widget-body form'>
					<div style='margin:0 auto;width:60%;text-align:center'>
						<table class='order_details' cellpadding='4' cellspacing='0' align='center'>
							<tr>
								<th align='left' >Discount</th>
								<td >&pound;". $all_info['order'][0]['discount']."</td>
							</tr>
							<tr>
								<th align='left' >Order date</th>
								<td >". $all_info['order'][0]['order_date']."</td>
							</tr>				
							<tr>
								<th align='left' >Sub total</th>
								<td >&pound;". $all_info['order'][0]['sub_total']."</td>
							</tr>	
							<tr>
								<th align='left' >Total</th>
								<td >&pound;". $all_info['order'][0]['total']."</td>
							</tr>
							<tr>
								<th align='left' >Tax</th>
								<td >&pound;". $all_info['order'][0]['tax']."</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			</div>";
			$html.=
			"<div id='innerTabs2_content'>
			<h3 class='page-title'>Customer Details</h3><br />
			<div id='form_wizard_1' class='widget box'>		            
				<div class='widget-body form'>
					<div style='margin:0 auto;width:60%;text-align:center'>
						<table class='customer_details' cellpadding='4' cellspacing='0' align='center'>
							<tr>
								<th align='left' >Name</th>
								<td >". $customer_info[0]['customer_name']."</td>
							</tr>
							<tr>
								<th align='left' >Address</th>
								<td >"."House : ".$customer_info[0]['house_name'].", House no : ".$customer_info[0]['house_no'].
								", Street: ".$customer_info[0]['street'].", postal code :".$customer_info[0]['post_code']."</td>
							</tr>				
							<tr>
								<th align='left' >Email</th>
								<td >". $customer_info[0]['email']."</td>
							</tr>	
							<tr>
								<th align='left' >Mobile</th>
								<td >". $customer_info[0]['mobile']."</td>
							</tr>						
						</table>
					</div>
				</div>
			</div>
			</div>";

			$html.="
            <div id='innerTabs3_content'>
			<h3 class='page-title'>Product Details</h3><br />
			<div id='form_wizard_1' class='widget box'>		            
				<div class='widget-body form'>";
					foreach ($all_info['product'] as  $product) {
						       // get child info related to product info 
						$child_info=$this->find_array_by_key($all_info['child'],'product_id', $product['id']);


						$html.="<div class='product-info'>
						<table class='fixed'>
							<col width='12%' />
							<col width='40%' />
							<col width='15%' />
							<col width='23%' />
							<col width='10%' />
							<tr>
								<th>Name</th>
								<th>Long Name</th>
								<th>Description</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Note</th>
							</tr>";

							$datas=$this->order->getaddonIdByChild_id($child_info['id']);	
							$addon_id = array();
							foreach ($datas as $data) {
								$addon_id[]=$data['addon_id'];
							}

							$note =$this->order->getNoteByChildid($child_info['id']); 
							$html.="                                        
							<col width='12%' />
							<col width='12%' />
							<col width='40%' />
							<col width='15%' />
							<col width='23%' />
							<col width='10%' />
							<tr>
								<td>".$product['product_name'] ."</td>
								<td>".$product['long_name'] ."</td>".
								"<td>".$product['description'] ."</td>".
								"<td> &pound;".$child_info['amount']/$child_info['quantity'] ."</td>".                   	       
								"<td>".$child_info['quantity'] ."</td>".
								"<td>".(empty($note[0]['instruction'])? 'X': $note[0]['instruction'])."</td>".
								"</tr>";
								$html.="</table>";
							$html.="</div>"; //end - product-info

							$html.='<div class="addon-info">
							<p class="sub-title">Addon Details</p>
							<table cellpadding="4" cellspacing="0" align="center">';
								if(!empty($addon_id)){
									$html.="
									<tr>
										<th>Name</th>										
										<th>Price</th>										
									</tr>";

									$addon_info =$this->order->getaddonBy_id($addon_id);  

									foreach ($addon_info as $item) {
										$html.="<tr>
										<td>".$item['addon_name']."</td>
										<td>&pound;".$item['web_order_price']."</td>
									</tr>";
								}                      																		

							}
							else{
								$html.="<tr> <th>No addon </th></tr>";
							}
						$html.='</table> </div>'; // end of addon-info						
						
						$datas=	$this->order->getingredientWithOutByChild_id($child_info['id']);

						$ingredient_ids= array();
						foreach($datas as $data){
							$ingredient_ids[]=$data['ingredient_id'];
						}

						$html.='<div class="ingredient-info">
						<p class="sub-title">Ingredient Details</p>
						<table cellpadding="4" cellspacing="0" align="center">';


							if(!empty($ingredient_ids)){
								$html.="
								<tr>
									<th>Description </th>										
									<th>Price</th>	

								</tr>";

								$i=0;
								foreach ($datas as $item) {
									$html.="<tr> 
									<td>".$item['description'] ."</td>
									<td>&pound;".$item['price'] ."</td>

								</tr>";
								$i++;
						  }                      																		

						}
						else{
							$html.="<tr> <th>No ingredient </th></tr>";
						}
						$html.='</table> </div>'; // end of ingredient-info			

						$html.='<div class="clearDiv"></div>';       

									//	echo '<pre>'; print_r($ingredient_details); echo '</pre>'; 
					}
					  $html.="</div>"; //end of widget-body
                        $html.="</div>"; //end of widget box
                        $html.="</div>"; //end of tab3
                    $html.="</div>"; // end of <div class=''>
                    echo $html; 
                }
            }

            
            function report($type){
            	if(_checkAuthentication($this)){
            		$typeAr = array('order','sales','delivery','collection','netsales');
            		$reporthtml = '';
            		$data = $this->input->post(NULL, TRUE);
            		if(isset($data['dates'])){
            			$d = explode("-", $data['dates']);
            			 
            			$from_date = trim($d[0]);
            			$to_date = trim($d[1]);
            		}else{
            			$from_date = date('Y.m.d',strtotime('first day of this month'));
            			$to_date = date('Y.m.d',strtotime('last day of this month'));
            		}
            		$dates = $from_date." - ".$to_date;
            		if(in_array($type, $typeAr)){
            			$user_id=$this->_userid;
            			$this->order->tablename=$this->order->restaurant;
            			$restaurant = $this->order->getRowBy('user_id',$user_id);
            			$this->order->tablename=$this->order->orderType;
            			$orderStatus = $this->order->getRowBy('order_type','Web Order');
            			
            			$this->order->tablename=$this->order->order;
            			if($type =='order'){
            				$total_order = $this->order->countBy(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id']));
            				$reporthtml .="<h3 class='page-title' style='margin: 4px;'>Total Order: $total_order</h3>";
            			} elseif($type =='sales') {
            				$total_sales = $this->order->totalSales(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id']));
            				$reporthtml .="<h3 class='page-title' style='margin: 4px;'>Total Sales: &pound;".(($total_sales['sub_total'] =='')?'0':$total_sales['sub_total'])."</h3>";
            			} elseif($type =='delivery') {
            				$total_delivery = $this->order->countBy(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id'],'web_order_type'=>'delivery'));
            				$reporthtml .="<h3 class='page-title' style='margin: 4px;'>Total Delivery: &pound;".(($total_delivery =='')?'0':$total_delivery)."</h3>";
            			} elseif($type =='collection') {
            				$total_collection = $this->order->countBy(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id'],'web_order_type'=>'collection'));
            				$reporthtml .="<h3 class='page-title' style='margin: 4px;'>Total Collection: &pound;".(($total_collection =='')?'0':$total_collection)."</h3>";
            			} elseif($type =='netsales') {
            				$total_sales_net = $this->order->totalNetSales(array('order_date >=' =>str_replace('.', '/', $from_date).' 00:00:00', 'order_date <=' =>str_replace('.', '/', $to_date).' 23:59:59', 'order_type_id' => $orderStatus['id'], 'restaurant_id'=>$restaurant['id']));
            				$reporthtml .="<h3 class='page-title' style='margin: 4px;'>Total Net Sales: &pound;".(($total_sales_net['total'] =='')?'0':$total_sales_net['total'])."</h3>";
            			}
            		} else {
            			$reporthtml = "<h3 class='page-title' style='margin: 4px;'> There is no order for $type. </h3>";
            		}
            		
            		$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
            		$this->load->view("admin/order_report",array(
            				"pagename"=>"Order Report",
            				"type"=>$type,
            				"dates"=>$dates,
            				"details" =>$reporthtml 
            		));
            		$this->load->view("admin/admin_foot");
            	}
            	
            }
}
