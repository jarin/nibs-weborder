<?php
$baseUrl=base_url();
?>

<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/common.css" type="text/css" media="screen"  />
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/order.css" type="text/css" media="screen"  />
<link href="<?=$baseUrl?>assets/css/admin/daterangepicker-bs2.css" rel="stylesheet" media="screen">
   <script src="<?=$baseUrl?>assets/js/moment.min.js"></script>
    <script src="<?=$baseUrl?>assets/js/daterangepicker.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/order.js"></script>


<script type="text/tmpl" id="defaultContent" charset="utf-8">
	<h3><?=$pagename?></h3>
	<div class="initPage">
		<div>

			<p>Click on the list of order on the left to view details....</p>
		</div>
	</div>
</script> 

<div id="innerTabs"> 
	<ul> 
		<li id="innerTabs1">Order details </li> 
		<li id="innerTabs2">Customer details </li> 
		<li id="innerTabs3">Product details </li>
		<li id="innerTabs4">Reservation </li>
        <li id="status" style="color: #ffffff;"> </li>
	</ul>

</div>
<div id="main-content" class='hastabs'>
	<div class="hidden"> </div> 
	<div class="container-fluid">
		<div id="page" class='site-settings'>						
			<div class="row-fluid" id="dContent">
			</div>
		</div>
	</div>
</div>

<div id="sidebar" class="nav-collapse collapse">
	<div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
	<div id="sidebarHeading">
		<div class="headertoolbar">
			<span>
				<em>Filter Criteria</em> <br/><br/>
					<div class="control-group">
							<label class="control-label" for="daterange" style="display: inline-block;width: 100px;line-height: 30px;">Date Range</label>
        			        <div class="controls" style="display: inline-block;height: 30px;">
								<div class="input-prepend">
									<span class="add-on"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
									<input type="text" style="width: 200px;" name="dates" id="daterange" class="daterange" value="<?php echo $dates; ?>" />
								</div>
							</div>
							<br>
							<label class="control-label" for="order_type" style="display: inline-block;width: 100px;line-height: 30px;">Order type</label>
        			        <div class="controls" style="display: inline-block;height: 30px;">
								<div class="input-prepend">
								<select id="order_type">
								    <option value="all">All</option>
									<option value="delivery">Delivery</option>
									<option value="collection">Collection</option>									
								</select>	
								</div>
							</div>
							<br/>
							<button type="button" id="filterBtn">Filter</button> 
						</div> 
				
			</span>

			<div id="searchCount">0 Filtered Results</div> 
			
		</div>
	</div> 
	<ul class="sidebar-menu list">
		<?=$list1?>
	</ul>
	<div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>

<script>
	var body=$("body");
	$(document).ready(function(){
		var url = '<?=$_SERVER['REQUEST_URI']?>';
		
		var str = "vieworder";
		if(url.indexOf(str) != -1){
			var id = url.match(/[\d]+$/);
			loadDetails('order_'+id);
		}
	});
		
</script>
