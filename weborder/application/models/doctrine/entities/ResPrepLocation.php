<?php



namespace entities;

/**
 * ResPrepLocation
 *
 * @Table(name="res_prep_location")
 * @Entity
 */
class ResPrepLocation
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $prepLocationName
   *
   * @Column(name="prep_location_name", type="string", length=150, nullable=false)
   */
  private $prepLocationName;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $user;

  /**
   * @var ResPrinter
   *
   * @ManyToOne(targetEntity="ResPrinter")
   * @JoinColumns({
   *   @JoinColumn(name="printer_id", referencedColumnName="id", onDelete="RESTRICT", onUpdate="CASCADE")
   * })
   */
  private $printer;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;


  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set prepLocationName
   *
   * @param string $prepLocationName
   */
  public function setPrepLocationName($prepLocationName)
  {
    $this->prepLocationName = $prepLocationName;
  }

  /**
   * Get prepLocationName
   *
   * @return string 
   */
  public function getPrepLocationName()
  {
    return $this->prepLocationName;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set user
   *
   * @param ResUser $user
   */
  public function setUser(\ResUser $user)
  {
    $this->user = $user;
  }

  /**
   * Get user
   *
   * @return ResUser 
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Set printer
   *
   * @param ResPrinter $printer
   */
  public function setPrinter(\ResPrinter $printer)
  {
    $this->printer = $printer;
  }

  /**
   * Get printer
   *
   * @return ResPrinter 
   */
  public function getPrinter()
  {
    return $this->printer;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
}