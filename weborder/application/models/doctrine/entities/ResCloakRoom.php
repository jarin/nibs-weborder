<?php



namespace entities;

/**
 * ResCloakRoom
 *
 * @Table(name="res_cloak_room")
 * @Entity
 */
class ResCloakRoom
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $customer;
  
  /**
   * @var ResTable
   *
   * @ManyToOne(targetEntity="ResTable")
   * @JoinColumns({
   *   @JoinColumn(name="table_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $table;
  
  /**
   * @var smallint $noOfItems
   *
   * @Column(name="no_of_items", type="smallint", nullable=false)
   */
  private $noOfItems;
  
  /**
   * @var boolean $isCollected
   *
   * @Column(name="is_collected", type="boolean", nullable=false)
   */
  private $isCollected=FALSE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var ResCloakRoomStatus
   *
   * @ManyToOne(targetEntity="ResCloakRoomStatus")
   * @JoinColumns({
   *   @JoinColumn(name="status_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $status;
  
  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;
  
  /**
   * @var boolean $hasItemsBroughtToCustomer
   *
   * @Column(name="has_items_brought_to_customer", type="boolean", nullable=false)
   */
  private $hasItemsBroughtToCustomer=0;
  
  /**
   * @var boolean $isCustomerItemsPrepared
   *
   * @Column(name="is_customer_items_prepared", type="boolean", nullable=false)
   */
  private $isCustomerItemsPrepared=0;
  
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
  	$this->customer = $customer;
  }
  
  /**
   * Get customer
   *
   * @return ResCustomer
   */
  public function getCustomer()
  {
  	return $this->customer;
  }
  
  /**
   * Set table
   *
   * @param ResTable $table
   */
  public function setTable(\ResTable $table)
  {
  	$this->table = $table;
  }
  
  /**
   * Get table
   *
   * @return ResTable
   */
  public function getTable()
  {
  	return $this->table;
  }
  
  /**
   * Set noOfItems
   *
   * @param smallint $noOfItems
   */
  public function setNoOfItems($noOfItems)
  {
  	$this->noOfItems = $noOfItems;
  }
  
  /**
   * Get noOfItems
   *
   * @return smallint
   */
  public function getNoOfItems()
  {
  	return $this->noOfItems;
  }
  
  /**
   * Set isCollected
   *
   * @param boolean $isCollected
   */
  public function setIsCollected($isCollected)
  {
    $this->isCollected = $isCollected;
  }

  /**
   * Get isCollected
   *
   * @return boolean 
   */
  public function getIsCollected()
  {
    return $this->isCollected;
  }
  
  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
  	$this->lastUpdate = $lastUpdate;
  }
  
  /**
   * Get lastUpdate
   *
   * @return datetime
   */
  public function getLastUpdate()
  {
  	return $this->lastUpdate;
  }
  
  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
  	$this->lastUpdatedBy = $lastUpdatedBy;
  }
  
  /**
   * Get lastUpdatedBy
   *
   * @return ResUser
   */
  public function getLastUpdatedBy()
  {
  	return $this->lastUpdatedBy;
  }
  
  /**
   * Set status
   *
   * @param ResCloakRoomStatus $status
   */
  public function setStatus(\ResClockRoomStatus $status)
  {
      $this->status = $status;
  }
  
  /**
   * Get status
   *
   * @return ResCloakRoomStatus
   */
  public function getStatus()
  {
      return $this->status;
  }
  
  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }
  
  /**
   * Set hasItemsBroughtToCustomer
   *
   * @param boolean $hasItemsBroughtToCustomer
   */
  public function setHasItemsBroughtToCustomer($hasItemsBroughtToCustomer)
  {
      $this->hasItemsBroughtToCustomer = $hasItemsBroughtToCustomer;
  }
  
  /**
   * Get hasItemsBroughtToCustomer
   *
   * @return boolean
   */
  public function getHasItemsBroughtToCustomer()
  {
      return $this->hasItemsBroughtToCustomer;
  }
  
  /**
   * Set isCustomerItemsPrepared
   *
   * @param boolean $isCustomerItemsPrepared
   */
  public function setIsCustomerItemsPrepared($isCustomerItemsPrepared)
  {
      $this->isCustomerItemsPrepared = $isCustomerItemsPrepared;
  }
  
  /**
   * Get isCustomerItemsPrepared
   *
   * @return boolean
   */
  public function getIsCustomerItemsPrepared()
  {
      return $this->isCustomerItemsPrepared;
  }
}