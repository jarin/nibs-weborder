<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/shop_orders.css" type="text/css" media="screen"  />
<script src="<?=$baseUrl?>assets/js/jquery.livequery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/shop/orders.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
  <div class="padded"><h1><?=$pagename?></h1>
  <div class="initPage">
    <div>
      <p>Click on a category in the tree on the left to view related products....</p>
      <p>Right-click on the categories in the tree to see more options</p>
    </div>
  </div></div>
</script>
<div id="innerTabs" style="display:none">
  <ul>
    <li class="active">Order Details</li>
    <li>Products Ordered</li>
    <li>Processing History</li>
  </ul>
</div>
<div id="thirdBar" class="secondpane">
  <div class='headertoolbar' style='padding-left:0.6em'>Orders <div id="searchCount" style="right:10px">0 Filtered Results</div></div>
  <div class="placeholder">No Orders</div>
  <ul class="comp_multilineListView">
  </ul>
  <div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>
<div id="mainContent" class="threepane lastpane">
  <div id="mainContentInner" class="">
    <div id="dContent">

    </div>
  </div>
</div>
<div id="sideBar" class="samelevel">
  <div class="headertoolbar" style="position:relative;">Filters </div>
    <ul class="shortlist nofooter">
      <?=$list1?>
    </ul>
</div>