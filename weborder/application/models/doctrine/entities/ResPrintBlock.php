<?php

namespace entities;

/**
 * ResPrintBlock
 *
 * @Table(name="res_print_block")
 * @Entity
 */
class ResPrintBlock
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $blockName
   *
   * @Column(name="block_name", type="string", length=150, nullable=false)
   */
  private $blockName;

  /**
   * @var smallint $sequenceNumber
   *
   * @Column(name="sequence_number", type="smallint", nullable=false)
   */
  private $sequenceNumber;
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set blockName
   *
   * @param string $blockName
   */
  public function setBlockName($blockName)
  {
    $this->blockName = $blockName;
  }

  /**
   * Get blockName
   *
   * @return string 
   */
  public function getBlockName()
  {
    return $this->blockName;
  }

  /**
   * Set sequenceNumber
   *
   * @param smallint $sequenceNumber
   */
  public function setSequenceNumber($sequenceNumber)
  {
  	$this->sequenceNumber = $sequenceNumber;
  }
  
  /**
   * Get sequenceNumber
   *
   * @return smallint
   */
  public function getSequenceNumber()
  {
  	return $this->sequenceNumber;
  }
}