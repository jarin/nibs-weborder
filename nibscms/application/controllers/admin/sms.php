<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends CI_Controller {
    
    private $sender = '';
    private $country;
    private $countryName;
    
    function __construct() {
			parent::__construct();
			$this->load->model("common_m");
			
			$restaurantId = $this->common_m->getAValue("restaurant", "id", array("user_id"=>$this->session->userdata("uid")));
			$config['apiKey'] = $this->common_m->getAValue("res_settings", "value", array("settings_name"=>"nibssms_api_key","restaurant_id"=>$restaurantId));
			$config['userName'] = $this->common_m->getAValue("res_settings", "value", array("settings_name"=>"nibssms_username","restaurant_id"=>$restaurantId));
			$this->sender = $this->common_m->getAValue("res_settings", "value", array("settings_name"=>"nibssms_sender_name","restaurant_id"=>$restaurantId));
			$this->countryName = $this->common_m->getAValue("res_settings", "value", array("settings_name"=>"RestaurantCountry","restaurant_id"=>$restaurantId));
			$this->country = $this->common_m->getAValue("res_country", "id", array("country_name"=>$this->countryName));
			
			$this->load->library('nibssms', $config);
    }
 
   /* public function process()
    {
        //echo $apiKey."--".$userName."--".$sender;
        
        $predefinedMessages = $this->common_m->runSQL("SELECT * FROM res_predefined_message WHERE message_type='SMS' AND week_day=".date("w")." ORDER BY start_time ASC"); //." AND start_time <= '".date('H:i:s')."' AND end_time >= '".date('H:i:s')."'");
        if(count($predefinedMessages)>0){
            //var_dump($predefinedMessages);
            foreach($predefinedMessages as $pm){
                $hour = explode(".", $pm['delay_time']);
                if(isset($hour[1])){
                    $interval = $hour[0].":30";
                }else{
                    $interval = $hour[0].":00";
                }
                $orders = $this->common_m->runSQL("SELECT o.id, c.mobile FROM res_order o LEFT JOIN res_customer c ON c.id=o.customer_id WHERE o.order_date between CONCAT(CURDATE(), ' ".$pm['start_time']."') AND CONCAT(CURDATE(), ' ".$pm['end_time']."') AND o.is_active=1 AND o.is_send_sms=0 AND DATE_ADD(o.order_date, INTERVAL '".$interval."' HOUR_MINUTE) < NOW()");
                if(count($orders)>0){
                    //var_dump($orders);
                    foreach($orders as $od){
                        if ($this->countryName=='United Kingdom') {
                            $od['mobile'] = substr($od['mobile'], 1);
                        }
                        $data = array(
                            'method' => 'sendOrScheduleSMS',
                            'params' => array(
                                'message'       => $pm['message'],
                                'country_id'    => $this->country, //253 for UK, 18 for BD
                                'sender_name'   => $this->sender,
                                'data_type'     => 'numbers',
                                'phone_numbers' => array(
                                    'number' => $od['mobile']
                                )
                            )
                        );
                        $this->nibssms->setMethod($data);
                        $result = $this->nibssms->call();
                        
                        if(isset($result['SUCCESS']) && $result['SUCCESS']==1){
                            $this->common_m->update("ResOrder", array("isSendSMS"=>1), array("id"=>$od['id']));
                            //echo $od['id'];
                        }
                    }
                }
            }
        }
        echo "OK";
    }*/
    
   /* public function setCountries()
    {
        $this->nibssms->setMethod(array('method'=>'getAllCountries'));
        $result = $this->nibssms->call();
        
        if (isset($result['SUCCESS']) && $result['SUCCESS']==1) {
            $countries = $result['result']['info'];
            $data = array();
            foreach ($countries as $c) {
                $data[] = array('id'=>$c['id'], 'country_name'=>$c['name']);
            }
            $this->load->database();
            $this->db->query('DELETE FROM res_country');
            
            $this->common_m->batchInsert("res_country", $data);
            echo json_encode(array('success'=>true));
        } else {
            echo json_encode(array('success'=>false, 'msg'=>$result['ERROR']['DESCRIPTION']));
        }
    }*/
    
    /**
     * Method to Send Bulk SMS to all/selective Customers
     * 
     * @return void
     */
   /* public function bulkSend()
    {
        $this->data['page_title'] = "Send SMS";
        $this->data['sub_title'] = '';
        $this->data['breadcrumb'] = array(
            "Send SMS"=>"sms/bulkSend"
        );
        
        $data = $this->input->post(null, true);
        if ($data!==false) {
            if ($data['mobile']!='') {
                $mobiles = explode(",", $data['mobile']);
            } else {
                $ary = json_decode($data['allCustomers'], true);
                foreach ($ary as $row) {
                    $mobiles[] = $row['mobile'];
                }
            }
            if ($this->countryName=='United Kingdom') {
                foreach ($mobiles as $k=>$v) {
                    $mobiles[$k] = substr($v, 1);
                }
            }

            $smsData = array(
                'method' => 'sendOrScheduleSMS',
                'params' => array(
                    'message'       => $data['message'],
                    'country_id'    => $this->country,
                    'sender_name'   => $this->sender,
                    'data_type'     => 'numbers',
                    'phone_numbers' => array(
                        'number' => $mobiles
                    )
                )
            );
            $this->nibssms->setMethod($smsData);
            $result = $this->nibssms->call();

            if (isset($result['SUCCESS']) && $result['SUCCESS']==1) {
                $this->viewPage(
                    'bulkSendError', 
                    array(
        	    	    "msg"=>'Your SMS processed successfuly.'
        			)
        		);
            } else {
                $this->viewPage(
                    'bulkSendError',
                    array(
                        "msg"=>'<b>SMS Portal Error:</b> '.$result['ERROR']['DESCRIPTION']
                    )
                );
            }
        } else {
            if(!empty($data['flash_type'])){
                $this->session->set_flashdata($data['flash_type'], $data['flash_msg']);
                $this->load->helper('url');
                redirect(current_url(), 'location', 301);
            }
            
            $customers = $this->common_m->getBy(
                "ResCustomer", 
                array("mobile IS NOT"=>"NULL", "mobile !="=>""), 
                "", 
                array("customerName"=>"asc")
            );
            $pm = $this->common_m->getBy(
                "ResPredefinedMessage", 
                array("messageType"=>"SMS"), 
                "", 
                array("subject"=>"asc")
            );
            
            $this->nibssms->setMethod(array('method'=>'getMaxCharactersForSMS'));
            $result = $this->nibssms->call();
            if (isset($result['SUCCESS']) && $result['SUCCESS']==1) {
                $maxChar = $result['result'];
                
                $this->viewPage(
                    'bulkSend',
                    array(
                        "customers" => $customers,
                        "maxChar"   => $maxChar,
                        "pm"        => $pm
                    )
                );
            } else {
                $this->viewPage(
                    'bulkSendError', 
                    array(
        	    	    "msg"=>'<b>SMS Portal Error:</b> '.$result['ERROR']['DESCRIPTION']
        			)
        		);
            }
        }
    }*/

    /**
     * Method to Send Reservation Confirmation
     * 
     * @param integer $id Reservation ID
     * 
     * @return void
     */
    public function sendReservationConfirmation()
    {
    	$reservation = array();
    	$id = '';
    	$data = $this->input->post(NULL, TRUE);
    	$type = $data['type'];
    	$id = $data['id'];
    	
        $this->load->model("reservation_m","reservation");
        $this->common_m->tablename ='res_reservation';
        $reservationAr = $this->common_m->getRows("*, res_reservation.id as id", array(
        		"res_customer" => "res_reservation.customer_id=res_customer.id",
        		"res_table" => "res_reservation.table_id=res_table.id",
        		"res_reservation_type" => "res_reservation_type.id=res_reservation.reservation_type_id",
        		"res_promo" => "res_promo.id=res_reservation.promo_id",
        		"res_reservation_status" => "res_reservation_status.id=res_reservation.reservation_status_id",
        		"res_deposit_type" => "res_deposit_type.id=res_reservation.deposit_type_id"), array('where'=>array("res_reservation.id"=>$id)));
        $reservation = $reservationAr[0];
    
      
       if($reservation['status'] != ucfirst($type)){
       
	        if (trim($reservation['mobile'])!='') {
	            if ($this->countryName=='United Kingdom') {
	                $reservation['mobile'] = substr(['mobile'], 1);
	            }
	            if($type == 'confirm'){
	            	$data = array(
	            			'method' => 'sendOrScheduleSMS',
	            			'params' => array(
	            					'message'       => "Reservation Confirmed",
	            					'country_id'    => $this->country, //253 for UK, 18 for BD
	            					'sender_name'   => $this->sender,
	            					'data_type'     => 'numbers',
	            					'phone_numbers' => array(
	            							'number' => $reservation['mobile']
	            					)
	            			)
	            	);
	            } elseif($type == 'cancel'){ 
	            	$data = array(
	            			'method' => 'sendOrScheduleSMS',
	            			'params' => array(
	            					'message'       => "Reservation cancelled",
	            					'country_id'    => $this->country, //253 for UK, 18 for BD
	            					'sender_name'   => $this->sender,
	            					'data_type'     => 'numbers',
	            					'phone_numbers' => array(
	            							'number' => $reservation['mobile']
	            					)
	            			)
	            	);
	            }
	           
	            $this->nibssms->setMethod($data);
	            $result = $this->nibssms->call();
	            
	            if(isset($result['SUCCESS']) && $result['SUCCESS']==1){
	            	if($type == 'confirm'){
	            		$reservationStatusId = $this->common_m->getAValue("res_reservation_status", "id", array("status"=>'Confirm'));
	            	} elseif($type == 'cancel'){
	            		$reservationStatusId = $this->common_m->getAValue("res_reservation_status", "id", array("status"=>'Cancel'));
	            	}
	            	
	            	$this->common_m->tablename = $this->reservation->tablename;
	            	$this->common_m->save($id, array("reservation_status_id"=>$reservationStatusId));
	            	
	            	if($type == 'confirm'){
	            		 echo '{"success":"success",successFunction:function(){
						alert("Confirmation SMS successfully send");
	                		
						}}';
	            	} elseif($type == 'cancel'){
	            		 echo '{"success":"success",successFunction:function(){
						alert("Cancellation SMS successfully send");
	                		
						}}';
	            	}
	               
	            } else {
	            	echo '{"success":"success",successFunction:function(){
						alert("Can not send SMS now. Please try later");
						}}';
	                 
	            }
	        } else {
	        	echo '{"success":"success",successFunction:function(){
						alert("No Mobile number found for this Customer");
						}}';
	            
	        }
	    } else {
	    	if($reservation['status'] == 'Confirm'){
	    		echo '{"success":"success",successFunction:function(){
						alert("This reservation is already Confirmed");
						}}';
	    	} elseif($reservation['status'] == 'Cancel'){
	    		echo '{"success":"success",successFunction:function(){
						alert("This reservation is already Cancelled");
						}}';
	    	}
	    	
	    }
    }
}