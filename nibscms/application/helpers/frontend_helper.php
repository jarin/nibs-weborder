<?php

function _buildbreadCrumb($array=array()){
	$baseUrl=base_url();
	$html=array();
	if(is_array($array)){
		foreach ($array as $key => $value) {
			$html[]="<a href='$value'>$key</a>";
		}
	}
	array_pop($html);
	return implode(" > ",$html)." > ";
}

function _buildMainMenu(&$CI,$page_id=0){
	$baseUrl=base_url();
	$html="<li class='first'><a href='".$baseUrl."'>Home</a></li>\n\t";
	$mainPages=$CI->pages->getMainVisible();
	if(is_array($mainPages)){
		foreach ($mainPages as $page) {
			$html.="<li><a href='".$baseUrl."pages/$page[stub]'>$page[name]</a></li>\n\t";
		}
	}
	$html.="<li><a href='".$baseUrl."news'>News</a></li>\n\t";
	$html.="<li><a href='".$baseUrl."downloads'>Downloads</a></li>\n\t";
	$html.="<li class='last'><a href='".$baseUrl."contact'>Contact Us</a></li>\n\t";
	return $html;
}

function _buildSidebarMenu(&$CI,$path=array(),$pageType=""){
	$baseUrl=base_url();
	$mainHtml=$html=array();
	$tmp="";$sectionName="";$mainSections="";
	
	$contentPages=$CI->db->query("select name,stub,id from ".$CI->content->tablename." where `parent_id`='0' and `active`='1' order by `sort_order`")->result_array();
	if(is_array($contentPages)){
		foreach($contentPages as $page){
			if(is_array($path) && isset($path[0]) && ($page["id"]==$path[0]["id"])){
				$mainSections.="<li class='active'><a href='".$baseUrl."content/$page[stub]'>$page[name]</a></li>";
				
				$sectionName="$page[name]";
				
				$children=$CI->db->query("select name,stub,id from ".$CI->content->tablename." where `parent_id`='$page[id]' and `active`='1' order by `sort_order`")->result_array();

				if(is_array($children) && count($children)>0){
					$level=1;
					foreach ($children as $item){
						if(isset($path[$level]) && $path[$level]["id"]==$item["id"]){
							$tmp.="<li class='active'><a href='".$baseUrl."content/$page[stub]/$item[stub]'>$item[name]</a>";
						}else{
							$tmp.="<li><a href='".$baseUrl."content/$page[stub]/$item[stub]'>$item[name]</a>";
						}

						$tmp.=__recursiveGetMenuChildren($CI,$path,
						$level+1,$item["id"],"$page[stub]/$item[stub]");

						$tmp.="</li>";

					}
				}
				$tmp.="</li>";
				$html[]=$tmp;
				unset($tmp);
			}else{
				$mainSections.="<li><a href='".$baseUrl."content/$page[stub]'>$page[name]</a></li>";
			}	
		}
		
	}
	return array(
		"mainSideMenu"=>$mainSections,
		"currentSectionName"=>$sectionName,
		"sideMenu"=>implode("",$html));
}

function _buildFooterMenu(&$CI){
	$html="";
	$html.="<li class='first'><a href='".base_url()."casestudies'>Case Study</a></li>\n\t";
	$mainPages=$CI->pages->getSecondaryVisible();
	if(is_array($mainPages)){
		foreach ($mainPages as $page) {
			$html.="<li><a href='".base_url()."pages/$page[stub]'>$page[name]</a></li>\n\t";
		}
	}
	$html.="<li><a href='".base_url()."sitemap'>Site Map</a></li>\n\t";
	// $html.="<li class='last'><a href='http://www.mechanet.co.uk/'>Log-in (FTP)</a></li>\n\t";	
	return $html;
}

function _buildBanners(&$CI){
	$banners="";$baseUrl=base_url();
	$bn=$CI->banners->getVisible("sort_order");
	foreach ($bn as $b) {
		if(file_exists($CI->banners->BannersDir."/".$b["filename"])){
			$banners.="<div class='slide'>
			<img src='".$baseUrl."".$CI->banners->BannersDir."/$b[filename]' />
			<div class='banner_tag'>$b[caption]</div>
			</div>";
		}
		
	}
	return $banners;
}

function _bannerSettings(&$CI){
	//Banner Settings
	$tType="fade";
	$tTime="4000";
	$tSpeed="400";
	$tEasing="swing";
	
	$transitionType=$CI->settings->getRowBy("name","transitionType");
	$transitionTimeOut=$CI->settings->getRowBy("name","transitionTimeOut");
	$transitionSpeed=$CI->settings->getRowBy("name","transitionSpeed");
	$transitionEasing=$CI->settings->getRowBy("name","transitionEasing");
	if(isset($transitionType["id"])){
		if(trim($transitionType["value"])!=""){
			$tType=$transitionType["value"];	
		}
	}
	if(isset($transitionTimeOut["id"])){
		if(trim($transitionTimeOut["value"])!=""){
			$tTime=$transitionTimeOut["value"]*1000;
		}
	}
	if(isset($transitionSpeed["id"])){
		if(trim($transitionSpeed["value"])!=""){
			$tSpeed=$transitionSpeed["value"]*1000;
		}
	}
	if(isset($transitionEasing["id"])){
		if(trim($transitionEasing["value"])!=""){
			$tEasing=$transitionEasing["value"];
		}
	}
	
	return array($tType,$tTime,$tSpeed,$tEasing);
}

function _newsMarquee(&$CI){
	$newses=$CI->news->getVisible("sort_order");
	$baseUrl=base_url();
	$html="";
	$i=0;
	if(is_array($newses)){
		foreach ($newses as $news) {
			if($i>5){
				break;
			}
			$html.="<span><a href='".$baseUrl."news/$news[id]'>$news[headline]</a></span>";
			$i++;
		}
	}
	return $html;
}

//-----------INNER FUNCTIONS----------------------
function __recursiveGetMenuChildren(&$CI,&$path,$level,$page_id,$stubAdd){
	$baseUrl=base_url();
	$html=array();

	$childrenOfPage=$CI->db->query("select name,stub,id from ".$CI->content->tablename." where `parent_id`='$page_id' and `active`='1' order by `sort_order`")->result_array();
	if(is_array($childrenOfPage) && count($childrenOfPage)>0){
		$html[]="<ul>";
		foreach($childrenOfPage as $item){
			$tmpHtml="";
			if(isset($path[$level]) && $path[$level]["id"]==$item["id"]){
				$tmpHtml.="<li class='active'><a href='".$baseUrl."content/$stubAdd/$item[stub]'>$item[name]</a>";
			}else{
				$tmpHtml.="<li><a href='".$baseUrl."content/$stubAdd/$item[stub]'>$item[name]</a>";
			}	
				$tmpHtml.=__recursiveGetMenuChildren($CI,$path,$level+1,$item["id"],"$stubAdd/$item[stub]");
			$tmpHtml.="</li>";
			$html[]=$tmpHtml;
			
		}
		$html[]="</ul>";
	}
	
	
	
	return implode("",$html);
}
//--*






//===========================COMMON========================



function _extractImgFromHtml($content,$attributesArray=array()){
	$html="";
	$content=str_replace(array("<br/>","<br />","<br>","<hr/>","<hr />","<hr>"),"",$content);
	preg_match_all('/\<img\s( [^\x00]*)?\>([^\x00]*)\<\/\\1\>|\<(.+)( [^\x00]*)?\/\>/U',$content,$matches);//get all img elements
	if(isset($matches[4]) && is_array($matches[4])){
		foreach($matches[4] as $attrs){
			preg_match_all('/ ([^ \r\n]+)=(["\'])(.*)\2/iUs', $attrs, $attributeMatches, PREG_SET_ORDER);//extract the attributes
			if(is_array($attributeMatches)){
				foreach($attributeMatches as $attribute){
					list($raw,$key,$delim,$val)=$attribute;
					if(strtolower($key)!="width"  &&  strtolower($key)!="height" &&  strtolower($key)!="style" &&  strtolower($key)!="border" &&  strtolower($key)!="class"){
						$attributesArray[]= "$key=".$delim.$val.$delim;
					}

				}
				$attributesArray[]="width='100%'";
				$html="<img ".implode(" ",$attributesArray)." />";
				break;
			}

		}
	}
		
	return $html;
}

function _extractExcerptFromHtml($content,$length=400){
	$temp=strip_tags(str_replace(array("\n","/>"),array(" ","/> "),$content));
	return ((strlen($temp)>$length)?(substr($temp, 0, strpos(wordwrap($temp, $length), "\n"))
	."..."):($temp));
}

/*function _sel_option($list,$val=""){
	$temp1=explode("value='",$list);
	for($i=0;$i<count($temp1);$i++){
		$temp2=explode(">",$temp1[$i]);
		if(($temp2[0]==($val."'")) || ($temp2[0]==($val."'")) ){
			$temp2[0]=$temp2[0]." Selected";
		}
		$temp3[$i]=implode(">",$temp2);
		unset($temp2);
	}
	$join1=implode("value='",$temp3);
	return $join1;
}*/

function _sel_option_by_val($list,$val=""){
	$temp1=explode("value='",$list);
	for($i=0;$i<count($temp1);$i++){
		$temp2=explode(">",$temp1[$i]);
		$temp2_a=explode("<",$temp2[1]);
		
			if ($temp2_a[0]==$val){
				
				$temp2[0]=$temp2[0]." Selected";
			}
		
		$temp3[$i]=implode(">",$temp2);
		unset($temp2);
	}
	$join1=implode("value='",$temp3);
	return $join1;
}

function _json_encode($array,$tabs=""){
	if(is_array($array) && (array_keys($array) !== range(0, sizeof($array) - 1))){
		foreach($array as $k=>$v){
			if(is_array($v)=="array"){
				$v1=_json_encode($v,$tabs);
			}elseif(gettype($v)=="boolean"){
				$v1= $v ? "true" : "false";
			}elseif(gettype($v)=="integer"){
				$v1= (int)$v;
			}elseif(gettype($v)=="double"||gettype($v)=="float"){
				$v1= (float)$v;
			}else{
				
				$v1=preg_replace("/(\r\n)/",'\n',addslashes(htmlspecialchars($v)));
				$v1="\"".preg_replace("/[\r\n]/",'\n',$v1)."\"";
			}
			$ret[]="$k : $v1";
		}
		if(isset($ret) && is_array($ret)){
			$return=implode(",",$ret);
			return "{".$return."}";
		}else{
			return '[]';
		}
	}elseif(is_array($array) && (array_keys($array) == range(0, sizeof($array) - 1))){
		foreach($array as $k=>$v){
			if(gettype($v)=="array"){
				$v1=_json_encode($v,$tabs);
			}elseif(gettype($v)=="boolean"){
				$v1= $v ? "true" : "false";
			}elseif(gettype($v)=="integer"){
				$v1= (int)$v;
			}elseif(gettype($v)=="double"||gettype($v)=="float"){
				$v1= (float)$v;
			}else{
				$v1=preg_replace("/(\r\n)/",'\n',addslashes(htmlspecialchars($v)));
				$v1="\"".preg_replace("/[\r\n]/",'\n',$v1)."\"";
			}
			$ret[]="$v1";
		}
		if(is_array($ret)){
			$return=implode(",",$ret);
			return "[$return]";
		}
	
	}else{
		return "";
	}
}


?>