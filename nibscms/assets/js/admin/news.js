$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addPageBtn").overlay({width:"30%",inline:true,title:"Add News Story",href:"#addPageForm"});

  //checkboxes
  $("#sideBar ul.list li input[type=checkbox]").live("click",function(){
		activate_news($(this));
		return false;
	});

  // See More details on click
  $("#sideBar ul.list li").live("click",function(){
		var _li=$(this);
		_li.siblings().removeClass("active");
		_li.addClass("active");
		loadNewsDetails(_li.attr("id"));
		return false;
	});

	//resize MCE editor as soon as it is shown
	$("#innerTabs li").live("click",function(){
	  var _this=$(this);
	  if (_this.index()==1){
	    resizeEditor();
	  }
	});

	//Autosaves
  $("#loadAutoSave").live("click",function(){
    var news_id=$("#EditableArea").parent().find("input[name=news_id]").val(),
    _this=$(this);
    _this.html("Loading... Please wait...").prop("disabled",true).next().prop("disabled",true);
    $.get(base_url+"admin/news/XgetAutoSave/"+news_id,function(text){
      var data = eval("(" + text + ")");
      if(data && data.data){
        tinyMCE.execInstanceCommand('EditableArea','mceSetContent',false,data.data);
      }
      var _par=_this.parent().parent()
      _par.addClass("hide");
      setTimeout(function(){
    	  _par.remove();
    	},300);
    })

  });
  $("#discardAutoSave").live("click",function(){
    var _par=$(this).parent().parent();
    _par.addClass("hide");
    setTimeout(function(){
  	  _par.remove();
  	},300);
  });
});

function addNews(name,id){
	NIBS.notif.notify("success","News story added");
	var html="<li id='news_"+id+"' class='deleted'>"+
	"<span class='name' title='"+name+"'>"+name+"</span>"+
	"<input title='Delete Page' type='image' class='action delete' rel='news/deleteNewsStory' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
	"<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"
	"</li>";

	$("#sideBar ul.list").prepend(html);
	$("#add_page_form").reset();
	setTimeout(function(){
	  $("#news_"+id).removeClass("deleted");
	},100);
}

function loadNewsDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	tinyMCE && tinyMCE.execCommand("mceRemoveControl",false,"EditableArea");
	$("#dContent").load(base_url+"admin/news/viewNewsStory/"+id,function(){
	  $(window).bind("resize",NIBS.util.debounce(resizeEditor));
	});
	loadPageInfo(id);
	$("#innerTabs,#thirdBar").show(0);
	$("#innerTabs li").removeClass("active").eq(0).addClass("active");
	$("#mainContent").addClass("hastabs threepane");
}

function loadPageInfo(id){
  $("#infoPanel").load(base_url+"admin/news/viewNewsStoryInfo/"+id);
}

function deleteNews(news_id){
  var el=$("#news_"+news_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
    $("#innerTabs,#thirdBar").hide(0);
    $("#mainContent").removeClass("threepane hastabs");
    $(window).unbind("resize");
  }
	el.addClass("deleted");
	setTimeout(function(){
	  el.remove()
	},500);
	NIBS.notif.notify("success","News story deleted successfully");
}

function activate_news(el){
  if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({id:id},"admin/news/activateDeactivateNewsStory");
}

function do_activate_news(id,val){
  $("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
  $("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

function resizeEditor(){
  var ifr=$("#EditableArea_ifr"),
  spn=$("#EditableArea_parent");
  ifr.css({height:spn.height()-62});
}
