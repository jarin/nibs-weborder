<?php



namespace entities;

/**
 * ResCustomerMessage
 *
 * @Table(name="res_customer_message")
 * @Entity
 */
class ResCustomerMessage
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="sender", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $sender;
  
  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="receiver", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $receiver;
  
  /**
   * @var datetime $sendDate
   *
   * @Column(name="send_date", type="datetime", nullable=false)
   */
  private $sendDate;
  
  /**
   * @var string $subject
   *
   * @Column(name="subject", type="string", length=150, nullable=false)
   */
  private $subject;
  
  /**
   * @var string $message
   *
   * @Column(name="message", type="string", length=1000, nullable=false)
   */
  private $message;
  
  /**
   * @var string $messageType
   *
   * @Column(name="message_type", type="string", nullable=true)
   */
  private $messageType;
  
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set sender
   *
   * @param ResUser $sender
   */
  public function setSender(\ResUser $sender)
  {
  	$this->sender = $sender;
  }
  
  /**
   * Get sender
   *
   * @return ResUser
   */
  public function getSender()
  {
  	return $this->sender;
  }
  
  /**
   * Set receiver
   *
   * @param ResCustomer $receiver
   */
  public function setReceiver(\ResCustomer $receiver)
  {
  	$this->receiver = $receiver;
  }
  
  /**
   * Get receiver
   *
   * @return ResCustomer
   */
  public function getReceiver()
  {
  	return $this->receiver;
  }
  
  /**
   * Set sendDate
   *
   * @param datetime $sendDate
   */
  public function setSendDate($sendDate)
  {
  	$this->sendDate = $sendDate;
  }
  
  /**
   * Get sendDate
   *
   * @return datetime
   */
  public function getSendDate()
  {
  	return $this->sendDate;
  }

  /**
   * Set message
   *
   * @param string $message
   */
  public function setMessage($message)
  {
  	$this->message = $message;
  }
    
  /**
   * Get message
   *
   * @return string
   */
  public function getMessage()
  {
  	return $this->message;
  }
  
  /**
   * Set subject
   *
   * @param string $subject
   */
  public function setSubject($subject)
  {
  	$this->subject = $subject;
  }
  
  /**
   * Get subject
   *
   * @return string
   */
  public function getSubject()
  {
  	return $this->subject;
  }
  
  /**
   * Set messageType
   *
   * @param string $messageType
   */
  public function setMessageType($messageType)
  {
  	$this->messageType = $messageType;
  }
  
  /**
   * Get messageType
   *
   * @return string
   */
  public function getMessageType()
  {
  	return $this->messageType;
  }
}