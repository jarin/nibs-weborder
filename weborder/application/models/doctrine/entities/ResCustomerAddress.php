<?php



namespace entities;

/**
 * ResCustomerAddress
 *
 * @Table(name="res_customer_address")
 * @Entity
 */
class ResCustomerAddress
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $houseNo
   *
   * @Column(name="house_no", type="string", length=150, nullable=true)
   */
  private $houseNo;
  
  /**
   * @var string $houseName
   *
   * @Column(name="house_name", type="string", length=150, nullable=true)
   */
  private $houseName;
  
  /**
   * @var string $street
   *
   * @Column(name="street", type="string", length=150, nullable=true)
   */
  private $street;
  
  /**
   * @var ResCity
   *
   * @ManyToOne(targetEntity="ResCity")
   * @JoinColumns({
   *   @JoinColumn(name="town_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $town;
  
  /**
   * @var ResState
   *
   * @ManyToOne(targetEntity="ResState")
   * @JoinColumns({
   *   @JoinColumn(name="state_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $state;
  
  /**
   * @var string $postCode
   *
   * @Column(name="post_code", type="string", length=150, nullable=true)
   */
  private $postCode;
  
  /**
   * @var ResCountry
   *
   * @ManyToOne(targetEntity="ResCountry")
   * @JoinColumns({
   *   @JoinColumn(name="country_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $country;
  
  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $customer;
  
  /**
   * @var string $email
   *
   * @Column(name="email", type="string", length=50, nullable=true)
   */
  private $email;
  
  /**
   * @var string $mobile
   *
   * @Column(name="mobile", type="string", length=50, nullable=true)
   */
  private $mobile;
  
  /**
   * @var string $landLine
   *
   * @Column(name="land_line", type="string", length=50, nullable=true)
   */
  private $landLine;
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set houseNo
   *
   * @param string $houseNo
   */
  public function setHouseNo($houseNo)
  {
    $this->houseNo = $houseNo;
  }

  /**
   * Get houseNo
   *
   * @return string 
   */
  public function getHouseNo()
  {
    return $this->houseNo;
  }
  
  /**
   * Set houseName
   *
   * @param string $houseName
   */
  public function setHouseName($houseName)
  {
  	$this->houseName = $houseName;
  }
  
  /**
   * Get houseName
   *
   * @return string
   */
  public function getHouseName()
  {
  	return $this->houseName;
  }
  
  /**
   * Set street
   *
   * @param string $street
   */
  public function setStreet($street)
  {
  	$this->street = $street;
  }
  
  /**
   * Get street
   *
   * @return string
   */
  public function getStreet()
  {
  	return $this->street;
  }
  
  /**
   * Set postCode
   *
   * @param string $postCode
   */
  public function setPostCode($postCode)
  {
  	$this->postCode = $postCode;
  }
  
  /**
   * Get postCode
   *
   * @return string
   */
  public function getPostCode()
  {
  	return $this->postCode;
  }
  
  /**
   * Set town
   *
   * @param ResCity $town
   */
  public function setTown(\ResCity $town)
  {
    $this->town = $town;
  }

  /**
   * Get town
   *
   * @return ResCity 
   */
  public function getTown()
  {
    return $this->town;
  }

  /**
   * Set country
   *
   * @param ResCountry $country
   */
  public function setCountry(\ResCountry $country)
  {
    $this->country = $country;
  }

  /**
   * Get country
   *
   * @return ResCountry 
   */
  public function getCountry()
  {
    return $this->country;
  }

  /**
   * Set state
   *
   * @param ResState $state
   */
  public function setState(\ResState $state)
  {
    $this->state = $state;
  }

  /**
   * Get state
   *
   * @return ResState 
   */
  public function getState()
  {
    return $this->state;
  }
  
  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
      $this->customer = $customer;
  }
  
  /**
   * Get customer
   *
   * @return ResCustomer
   */
  public function getCustomer()
  {
      return $this->customer;
  }
  
  /**
   * Set mobile
   *
   * @param string $mobile
   */
  public function setMobile($mobile)
  {
      $this->mobile = $mobile;
  }
  
  /**
   * Get mobile
   *
   * @return string
   */
  public function getMobile()
  {
      return $this->mobile;
  }
  
  /**
   * Set landLine
   *
   * @param string $landLine
   */
  public function setLandLine($landLine)
  {
      $this->landLine = $landLine;
  }
  
  /**
   * Get landLine
   *
   * @return string
   */
  public function getLandLine()
  {
      return $this->landLine;
  }
  
  /**
   * Set email
   *
   * @param string $email
   */
  public function setEmail($email)
  {
      $this->email = $email;
  }
  
  /**
   * Get email
   *
   * @return string
   */
  public function getEmail()
  {
      return $this->email;
  }
}