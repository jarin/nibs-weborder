<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProductImport extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->library("excel_lib");
		$this->load->helper("generic");
		$this->load->helper("excel");
		
		$this->tab="import";
		$this->load->model("activity_m","activity");
		$this->load->model("import_m","import");
		$this->load->model("Business_m","business");
		$this->load->model("epos_m","epos");
		
		
	}
	function index(){
		if(_checkAuthentication($this)){		
			$allbusiness = array(); 
			if($this->_utype =='admin'){
				$allbusiness=$this->business->getAllbusines();
			}
			$business_id = '';
			if($this->input->post('businessName') === false){
				$business=$this->business->getBusinesByUser($this->_userid);
				if(count($business) > 0){
					$business_id = $business["id"];
				}
				
			} else {
				$business_id = $this->input->post('businessName');
			}
			
			$epos_info = $this->epos->infobyBid($business_id);
			
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
			$this->load->view("admin/product_import",array( "pagename"=>"Product Import Manager","epos"=>$epos_info,"business_id"=>$business_id, 'allbusiness'=>$allbusiness));
			$this->load->view("admin/admin_foot");
		}
	}	
	function import($business_id=''){
		if(empty($business_id)){
			$this->session->set_flashdata('error', 'Please select a restaurant first.');
			redirect(base_url().'admin/productImport');
		}
		$product=$this->import->getProducts($business_id);
		if($product === false){
			$this->session->set_flashdata('error', 'Product can not import, please try later.');			
		} else {
			$this->session->set_flashdata('success', 'Product Imported successfully');			
		}
		redirect(base_url().'admin/productImport');
	}
	
	function uploadExcel(){
		$restaurant_id = $this->input->post('restaurant_id');
		$excelUpload = $this->input->post('excel');
		
		if($restaurant_id ==''){
			echo '{"success":"error","msg":"Please select a restaurant first"}';
			exit;
			
		}
		if($_FILES['excel']['name'] ==''){
			echo '{"success":"error","msg":"Please select a excel file first"}';
			exit;
				
		}
		$tempFilePath="temp";
		if(!file_exists($tempFilePath)){
			@mkdir($tempFilePath);
		}
		@chmod($tempFilePath,0777);
		$path=$tempFilePath."/";
		$this->load->library("upload",array(
				"allowed_types"=>"xls"
		));
			
		$files=array();
		foreach($_FILES as $key=>$value){
			if($value['size']!=0){
				$img_file=$value['name'];
				$this->upload->encrypt_name = true;
				$this->upload->file_ext = $this->upload->get_extension($img_file);
				$file=$this->upload->set_filename($path,$img_file);					
				$files[$key]=$file;					
				move_uploaded_file($value['tmp_name'], $path.$file);					
				//$data[$key]=$file;
				$filepath = $path.$file;					
			}
		}
		
		$res = bulkProducts($this,$filepath,$restaurant_id);
		echo '{"success":"success","msg":"'.$res.'"
				}';
		exit;		
	}
	
	function downloadSampleExcel(){
		downloadSample($this,'Products');
	}
	
	function saveinfo(){
		if(_checkAuthentication($this)){
			$id=$this->input->post("epos_id");
			$data=array(
					"db_name" => $this->input->post("db_name"),
					"db_host" => $this->input->post("db_host"),
					"db_user" => $this->input->post("db_user"),
					"db_pass" => $this->input->post("db_pass"),
					"restaurant_id" => $this->input->post("b_id")
			);
			
			if(!empty($id)){
				if($this->epos->save($id,$data)){
					$this->session->set_flashdata('success', 'Successfully Updated');
				}else{
					$this->session->set_flashdata('error', 'There is a problem in database');
				}
	
			}
			else{
				if($this->epos->create($data)){
					$this->session->set_flashdata('success', 'Successfully Saved');	
				}else{
					$this->session->set_flashdata('error', 'There is a problem in database');
					
				}
			}
			redirect(base_url().'admin/productImport');
				
		}
	}
	
}