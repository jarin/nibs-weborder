<?php



namespace entities;

/**
 * ResReservationType
 *
 * @Table(name="res_reservation_type")
 * @Entity
 */
class ResReservationType
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $reservationType
   *
   * @Column(name="reservation_type", type="string", length=150, nullable=false)
   */
  private $reservationType;
  
  /**
   * @var string $color
   *
   * @Column(name="color", type="string", length=9, nullable=false)
   */
  private $color;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set reservationType
   *
   * @param string $reservationType
   */
  public function setReservationType($reservationType)
  {
    $this->reservationType = $reservationType;
  }

  /**
   * Get depositType
   *
   * @return string 
   */
  public function getDepositType()
  {
    return $this->depositType;
  }
  
  /**
   * Set color
   *
   * @param string $color
   */
  public function setColor($color)
  {
  	$this->color = $color;
  }
  
  /**
   * Get color
   *
   * @return string
   */
  public function getColor()
  {
  	return $this->colorolor;
  }
}