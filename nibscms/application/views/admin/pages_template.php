<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/pages.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/pages.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
  <div class="padded"><h3><?=$pagename?></h3>
  <div class="initPage">
    <div>
      <p>Click on the list of pages on the left to edit....</p>
      <p>To delete a page use the <img src="<?=$baseUrl?>assets/images/admin/icon_trash.png" /> delete icon...</p>
      <p>To add a page, click on the <img src="<?=$baseUrl?>assets/images/admin/icon_add.png" /> Plus button...</p>
    </div>
  </div></div>
</script>





<div id="innerTabs" style="display:none">
  <ul>
    <li class="active">Page Details</li>
    <li>Page Content</li>
  </ul>
</div>




<div id="thirdBar" style="display:none">	
  <div class='headertoolbar'><span>Page Info</span></div>
  <div class='padded' id='infoPanel'></div>
</div>


<div id="main-content" class="">

  <div id="mainContentInner" class="">
    <div class="hidden">
      <div id="addPageForm">
       <form id='add_page_form' method='post' rel='ajaxed' action='<?=$baseUrl?>admin/pages/addPage' loadingDiv='loader_add_page' formType="adder">
			<table cellpadding='4' cellspacing='0' align='center'>
				<tr>
					<th>Name</th>
					<td colspan="2"><input type='text' name='name' id='add_pagename_input'
					  class='stubSenders'
						stubReceiver='add_pagestub_input'
						stubStatus='stubReturnValue'
						stubUrl='admin/pages/checkStubDuplication'
						stubPageTitle='add_pagetitle_input'
					   /></td>
				</tr>
				<tr>
					<th>Page Title</th>
					<td colspan="2"><input type='text' name='pagetitle' id='add_pagetitle_input'
					  class='stubReceivers'
						stubStatus='stubReturnValue'
						stubUrl='admin/pages/checkStubDuplication'
					   /></td>
				</tr>
				<tr>
					<th>Stub</th>
					<td ><input type='text' name='stub' id='add_pagestub_input' /></td>
					<td width="20"><span id="stubReturnValue" style="font-weight:bold;"></span></td>
				</tr>
				<tr>
					<th>Meta Keyword</th>
					<td ><input type='text' name='keyword' id='add_keyword_input' /></td>
					
				</tr>
				<tr>
					<th>Meta Description</th>
					<td ><textarea  name='description' id='add_description_input'></textarea></td>
					
				</tr>
				<tr><td colspan='3'><hr class='hr' /></td></tr>
				<td align='right' colspan='3'><button class="button add" type='submit'>Add Page</button><div id='loader_add_page'  class='q_feedback'></div></td> </tr>
			</table>
		</form>
      </div>
    </div>
    <div id="dContent">
    </div>
  </div>
</div>




<div id="sidebar" class="nav-collapse collapse">
	<div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
	<div id="sidebarHeading">
		<div class='headertoolbar'>
			<span><em>Pages</em></span>
	  		<div id="searchCount">0 Filtered Results</div> 
			<button class="addPageBtn" title="Add Page">
				<img src="<?=$baseUrl?>assets/images/admin/icon_add.png" style='margin-top:-6px'/>
			</button>
		</div>
  	</div> 
  	<ul class="sidebar-menu list reorderable" rel="pages/reOrderPages">
    	<?=$list1?>
  	</ul>
  	<div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>


<script type="text/javascript" src="<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce/tiny_mce_gzip.js"></script>
<script type="text/javascript">
tinyMCE_GZ.init({
	plugins : 'safari,style,table,save,advimage,advlink,contextmenu,paste,directionality,visualchars,nonbreaking,xhtmlxtras,inlinepopups,fullscreen,spellchecker',
	themes : 'advanced',
	languages : 'en',
	spellchecker_languages : "+English=en",
	disk_cache : true,
	debug : false,
	content_css : "assets/css/admin/pages.css" + new Date().getTime(),
	baseURL : "<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce"
});

</script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">
	var _KA=window.setInterval(function(){
		$.ajax({
			url:base_url+"admin/pages/XkeepAlive",
			dataType:"json",
			type:"post",
			data:{"keepAlive":"true"}
		})
	},180000);

	var changed_count=0;
  function MCE_Changed(){
    var autosaveUrl="<?=(isset($autosaveUrl) ? $autosaveUrl : "")?>"||"",threshold=3;
    changed_count++;
    if(changed_count>=threshold && $.trim(autosaveUrl)!=""){
      changed_count=0;
      var EE=tinyMCE.get("EditableArea"),
      content=EE.getContent(),
      page_id=$("#EditableArea").parent().find("input[name=page_id]").val();

      if(page_id > 0){
        NIBS.util.ajax({page_id:page_id,content:content},autosaveUrl,null,null);
      }
    }
  }
</script>
