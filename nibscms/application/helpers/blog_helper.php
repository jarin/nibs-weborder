<?php
function renderArticleList(&$CI,&$articles=array(),&$page=array(),$previous_page="",$next_page="",$pagination_url=""){
	$mainMenu=_buildFullMenu($CI);
	$sideMenu=_buildSideMenu($CI);
	
	$footerMenu=_buildFooterMenu($CI);
	
	$page["content"]="";
	foreach ($articles as $article) {
		$page["content"].="
		<div class='listitem'>
		<div class='listcrufts'>
			<div class='listcrufts_inner'>
				<h1 class='sifr'>$article[title]</h1>
				".date("l jS F Y",strtotime($article["created"]))." &nbsp;&nbsp;|&nbsp;&nbsp; posted by: $article[modifiedby] &nbsp;&nbsp;|&nbsp;&nbsp; comments: ".$CI->comments->countActiveByArticle($article["id"])."
			</div>
		</div>
		<p>";
		if(trim($article["excerpt"])==""){
			$page["content"].=_extractExcerptFromHtml($article["content"]);
		}else{
			$page["content"].=nl2br($article["excerpt"]);
		}
		$page["content"].="</p><p><a style='font-weight:bold;text-decoration:none' href='#'>Read the rest of this post...</a></p></div>";
	}
	
	if($previous_page!="" || $next_page!=""){
		$page["content"].="<div id='paginator'>";
		$temp=array();
		if($next_page!=""){
			$temp[]="<a href='$pagination_url/$next_page'>Older Posts</a>";
		}
		if($previous_page!=""){
			$temp[]="<a href='$pagination_url/$previous_page'>Newer Posts</a>";
		}
		
		$page["content"].=implode("&nbsp;&nbsp;|&nbsp;&nbsp;",$temp);
		$page["content"].="</div>";
	}
	
	$CI->load->view('site_header',array(
		"mainMenu"=>$mainMenu,
		"sideMenu"=>$sideMenu,
		//"banners"=>$banners,
		"footerMenu"=>$footerMenu,
		"page"=>$page
		));
	$CI->load->view('list');
	$CI->load->view('site_footer');
	
}

function renderArticle(&$CI,&$page){
	$mainMenu=_buildFullMenu($CI);
	$sideMenu=_buildSideMenu($CI);
	
	$page["pagetitle"]=$page["title"];
	$page["canonical"]=_canonical_url($page);
	$page["tags"]=$CI->tags->getByArticle($page["id"]);
	$comments=$CI->comments->getActiveByArticle($page["id"]);
	$page["commentCount"]=count($comments);
	$page["comments"]=_buildComments($comments);
	
	
	$footerMenu=_buildFooterMenu($CI);
	
	$CI->load->view('site_header',array(
		"mainMenu"=>$mainMenu,
		"sideMenu"=>$sideMenu,
		//"banners"=>$banners,
		"footerMenu"=>$footerMenu,
		"page"=>$page
		));
	$CI->load->view('content');
	$CI->load->view('site_footer');
}

function _canonical_url($page=array()){
	$url="";
	if(isset($page["id"])){
		$cr=$page["created"];
		$dt=date("d/m/Y",strtotime($cr));
		list($d,$m,$y)=explode("/",$dt);
		return base_url()."archives/$y/$m/$page[id]/".$page["stub"];
	}
	
	return $url;
}

function _buildComments($comments){
	$html="";
	if(count($comments)==0){
		return "";
	}else{
		$html="<h2>Comments</h2>";
		foreach ($comments as $comment) {
			$hasWebsite=(trim($comment["website"])!="");
			$html.="<div class='comment clearfix".(($comment["byOwner"]==1)?(" by_owner"):(""))."'>
				<div class='picture'>
				".(($comment["byOwner"]==1)?("<img src='".base_url()."assets/images/gravatar.jpg' alt='' />"):((($hasWebsite)?("<a href='$comment[website]'><img src='http://www.gravatar.com/avatar/".md5($comment["email"])."?s=64&amp;d=identicon' border='0' /></a>"):("<img src='http://www.gravatar.com/avatar/".md5($comment["email"])."?s=64&amp;d=identicon' />"))))."
				</div>
				<div class='comment_text'>
					".nl2br(strip_tags($comment["comment"]))."
				</div>
				<div class='username'>
					".(($hasWebsite)?("<a href='$comment[website]'>".$comment["name"]."</a>"):($comment["name"]))."
				</div>
				<div class='time'>
					posted on ".date("l jS F Y \a\\t H:i a",$comment["lefton"])."
				</div>
			</div>";
		}
	}

	return $html;
}

function _buildSideMenu(&$CI){
	$html="";
	//Recent posts
	$html.=__buildRecentPostsList($CI);
	//categories
	$html.=__buildTagsList($CI);
	//archive
	$html.=__buildArchivesList($CI);
	
	return $html;
}

function __buildRecentPostsList(&$CI){
	$baseUrl=base_url();
	$html="";
	$recent=$CI->articles->getRecent($CI->SETTINGS["recentArticlesOnSideMenu"]);
	if(is_array($recent) && count($recent)>0){
		$html.="<li><span>Recent Posts</span><ul>";
		foreach ($recent as $rec) {
			$html.="<li><a href='".$baseUrl."articles/$rec[stub]'>$rec[title]</a>";
		}
		$html.="</ul></li>";
	}
	return $html;
}

function __buildTagsList(&$CI){
	$baseUrl=base_url();
	$html="";
	$tags=$CI->tags->getAll();
	$tagsToUse=array();
	if(is_array($tags)){
		foreach ($tags as $tag) {
			$count=$CI->tags->countByArticle($tag["id"]);
			if($count > 0){
				$tagsToUse[]=array("id"=>$tag["id"],"tag"=>$tag["tag"],"count"=>$count);
			}
		}
		
		if(count($tagsToUse)>0){
			$html.="<li><span>Categories</span><ul>";
			foreach ($tagsToUse as $tag) {
				$html.="<li><a href='".$baseUrl."tags/".rawurlencode($tag["tag"])."'>$tag[tag] ($tag[count])</a>";
			}
			$html.="</ul></li>";
		}
	}
	return $html;
}

function __buildArchivesList(&$CI){
	$baseUrl=base_url();
	$html="";
	$Months=array(""/*To make the array 1-indexed*/,"January","February","March",
	"April","May","June","July","August","September","October","November","December");
	$archive=$CI->articles->getArchive();
	if(is_array($archive) && count($archive)>0){
		$html.="<li><span>Blog Archive</span><ul>";
		foreach ($archive as $m) {
			$html.="<li><a href='".$baseUrl."archives/$m[year]/$m[month]'>".$Months[$m["month"]]." $m[year] ($m[count])</a>";
		}
		$html.="</ul></li>";
	}
	return $html;
}


?>