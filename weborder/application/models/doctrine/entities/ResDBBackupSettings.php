<?php



namespace entities;

/**
 * ResDBBackupSettings
 *
 * @Table(name="res_dbbackup_settings")
 * @Entity
 */
class ResDBBackupSettings
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="integer", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $scheduleHour
   *
   * @Column(name="schedule_hour", type="string", length=2, nullable=false)
   */
  private $scheduleHour;
  
  /**
   * @var string $scheduleAP
   *
   * @Column(name="schedule_ap", type="string", length=2, nullable=false)
   */
  private $scheduleAP;
  
  /**
   * @var string $emailAddress
   *
   * @Column(name="email_address", type="string", length=255, nullable=false)
   */
  private $emailAddress;
  
  /**
   * @var string $host
   *
   * @Column(name="host", type="string", length=255, nullable=false)
   */
  private $host;
  
  /**
   * @var string $user
   *
   * @Column(name="user", type="string", length=255, nullable=false)
   */
  private $user;
  
  /**
   * @var string $password
   *
   * @Column(name="password", type="string", length=255, nullable=false)
   */
  private $password;
  
  /**
   * @var string $remoteDirectory
   *
   * @Column(name="remote_directory", type="string", length=255, nullable=false)
   */
  private $remoteDirectory;


  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set scheduleHour
   *
   * @param string $scheduleHour
   */
  public function setScheduleHour($scheduleHour)
  {
    $this->scheduleHour = $scheduleHour;
  }

  /**
   * Get scheduleHour
   *
   * @return string 
   */
  public function getScheduleHour()
  {
    return $this->scheduleHour;
  }
  
  /**
   * Set scheduleAP
   *
   * @param string $scheduleAP
   */
  public function setScheduleAP($scheduleAP)
  {
  	$this->scheduleAP = $scheduleAP;
  }
  
  /**
   * Get scheduleAP
   *
   * @return string
   */
  public function getScheduleAP()
  {
  	return $this->scheduleAP;
  }
  
  /**
   * Set emailAddress
   *
   * @param string $emailAddress
   */
  public function setEmailAddress($emailAddress)
  {
  	$this->emailAddress = $emailAddress;
  }
  
  /**
   * Get emailAddress
   *
   * @return string
   */
  public function getEmailAddress()
  {
  	return $this->emailAddress;
  }
  
  /**
   * Set host
   *
   * @param string $host
   */
  public function setHost($host)
  {
  	$this->host = $host;
  }
  
  /**
   * Get host
   *
   * @return string
   */
  public function getHost()
  {
  	return $this->host;
  }
  
  /**
   * Set user
   *
   * @param string $user
   */
  public function setUser($user)
  {
  	$this->user = $user;
  }
  
  /**
   * Get user
   *
   * @return string
   */
  public function getUser()
  {
  	return $this->user;
  }
  
  /**
   * Set password
   *
   * @param string $password
   */
  public function setPassword($password)
  {
  	$this->password = $password;
  }
  
  /**
   * Get password
   *
   * @return string
   */
  public function getPassword()
  {
  	return $this->password;
  }
  
  /**
   * Set remoteDirectory
   *
   * @param string $remoteDirectory
   */
  public function setRemoteDirectory($remoteDirectory)
  {
  	$this->remoteDirectory = $remoteDirectory;
  }
  
  /**
   * Get remoteDirectory
   *
   * @return string
   */
  public function getRemoteDirectory()
  {
  	return $this->remoteDirectory;
  }
  
  
}