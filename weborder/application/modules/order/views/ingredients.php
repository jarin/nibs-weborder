                                        <div class="dropdown-details">
                                            <?php if(count($ingredient)> 0){?>
											<form class="default-form" id="formIngred_<?php echo $productid;?>">
											    <input type="hidden" name="productid" value="<?php echo $productid;?>"/>
												<h5>Please Select Your Option</h5>
												<?php if(count($without)> 0){ ?>
												<div class="ingrd" style="width:50%; float: left;">
												<h6>Without</h6>
												<?php foreach ($without as $k=>$val) {
													
												    if($val['ingredientName'] !='Special Instruction'){ ?>
												    
												<span class="checkbox-input">
												
													<input type="checkbox" id="without_<?php echo $productid;?>_<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="withoutingredient[]">
													<label for="without_<?php echo $productid;?>_<?php echo $val['id']?>"><?php echo $val['ingredientName']?><i class="fa fa-minus price">&pound;<?php echo $val['priceWithout']?></i>
													</label>
												</span>
												
												<?php 
												        }
                                                     } ?>
                                                     </div>
                                                 <?php 
                                                  } 
                                                  ?>
												<?php if(count($with)> 0){ ?>
												<div class="ingrd" style="width:50%; float: left;">
												<h6>With</h6>
												<?php foreach ($with as $k=>$val) {
                                                    if($val['ingredientName'] !='Special Instruction'){
												    ?>
												   
												<span class="checkbox-input">
											
													<input type="checkbox" id="with_<?php echo $productid;?>_<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="withingredient[]">
													<label for="with_<?php echo $productid;?>_<?php echo $val['id']?>"><?php echo $val['ingredientName']?><i class="fa fa-plus price">&pound;<?php echo $val['price']?></i>
													</label>
												</span>
												
										
												<?php
                                                        }
												    } ?>
												 </div>
												 <?php 
												}
												?>
												<div style="clear:both;"></div>
												<?php if(count($addon)> 0){ ?>
												<div style="width:100%; clear:both;">
												<h6>Add-On</h6>
												<?php foreach ($addon as $k=>$val) {
                                                    
												    ?>
												   
												<span class="checkbox-input">
											
													<input type="checkbox" id="addon_<?php echo $productid;?>_<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="addon[]">
													<label for="addon_<?php echo $productid;?>_<?php echo $val['id']?>"><?php echo $val['addonName']?><i class="fa fa-plus price">&pound;<?php echo $val['webOrderPrice']?></i>
													</label>
												</span>
												
										
												<?php
                                                        
												    } ?>
												 </div>
												 <?php 
												}
												?>
												<?php 
												$additionalIns = false;
												    if(count($without)> count($with)){
                                                        $count= count($without);
                                                    } else if(count($with)>= count($without)){
                                                        $count= count($with);
                                                    }
												    for($i=0; $i<$count; $i++){
                                                    if(isset($without[$i]) || isset($with[$i]))
                                                    {
                                                        if((isset($without[$i]) && $without[$i]['ingredientName'] == 'Special Instruction') || (isset($with[$i]) && $with[$i]['ingredientName'] == 'Special Instruction'))
                                                        {
                                                           $additionalIns = true;         
                                                        }
                                                    }
                                                    }
												?>
												<?php if($additionalIns){?>
												<div style="width:100%;clear:both;">
												<h6>Additional Notes</h6>
												
												<textarea placeholder="Write here" name="additionalNote"></textarea>
												</div>
												<?php } ?>
												
												<!-- <a class="btn btn-default-red addIngred" rel="<?php echo $productid;?>" type="submit">Confirm</a>
												<a class="btn btn-default-black removeIngred" rel="<?php echo $productid;?>" name="cancel" type="submit">Cancle</a>-->
											</form>
											<?php } else { ?>
											    <h5>No Options Availabe for this item</h5>
											<?php } ?>
										</div>
										<!--end .dropdown-details-->
<script type="text/javascript">
$( '.checkbox-input' ).each(function(){
	$(this).uouCheckboxInput();
});
</script>

