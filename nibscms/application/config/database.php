<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'weborder';
$active_record = TRUE;

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = 'root';
$db['default']['database'] = 'cms4weborder';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$db['weborder']['hostname'] = 'localhost';
$db['weborder']['username'] = 'root';
$db['weborder']['password'] = 'root';
$db['weborder']['database'] = 'weborder';
$db['weborder']['dbdriver'] = 'mysql';
$db['weborder']['dbprefix'] = '';
$db['weborder']['pconnect'] = FALSE;
$db['weborder']['db_debug'] = TRUE;
$db['weborder']['cache_on'] = FALSE;
$db['weborder']['cachedir'] = '';
$db['weborder']['char_set'] = 'utf8';
$db['weborder']['dbcollat'] = 'utf8_general_ci';
$db['weborder']['swap_pre'] = '';
$db['weborder']['autoinit'] = TRUE;
$db['weborder']['stricton'] = FALSE;

$db['default_server']['hostname'] = 'localhost';
$db['default_server']['username'] = 'nibsadmin';
$db['default_server']['password'] = 'Password99';
$db['default_server']['database'] = 'weborder';
$db['default_server']['dbdriver'] = 'mysqli';
$db['default_server']['dbprefix'] = '';
$db['default_server']['pconnect'] = TRUE;
$db['default_server']['db_debug'] = TRUE;
$db['default_server']['cache_on'] = FALSE;
$db['default_server']['cachedir'] = '';
$db['default_server']['char_set'] = 'utf8';
$db['default_server']['dbcollat'] = 'utf8_general_ci';
$db['default_server']['swap_pre'] = '';
$db['default_server']['autoinit'] = TRUE;
$db['default_server']['stricton'] = FALSE;

$db['import_server']['hostname'] = 'localhost';
$db['import_server']['username'] = 'root';
$db['import_server']['password'] = 'root';
$db['import_server']['database'] = 'restpos';
$db['import_server']['dbdriver'] = 'mysqli';
$db['import_server']['dbprefix'] = '';
$db['import_server']['pconnect'] = TRUE;
$db['import_server']['db_debug'] = TRUE;
$db['import_server']['cache_on'] = FALSE;
$db['import_server']['cachedir'] = '';
$db['import_server']['char_set'] = 'utf8';
$db['import_server']['dbcollat'] = 'utf8_general_ci';
$db['import_server']['swap_pre'] = '';
$db['import_server']['autoinit'] = TRUE;
$db['import_server']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */