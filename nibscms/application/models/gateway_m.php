<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gateway_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->tablename="payment_gateway";
	}
	
	function saveData($data){
		foreach ($data as $key => $val){
			$rslt = $this->db->where('field_name', $key)
			->set('value',$val)
			->update($this->tablename);
		}
		
		return $rslt;
	}
}
?>