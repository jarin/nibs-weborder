<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Session class using native PHP session features and hardened against session fixation.
 * 
 * @category Sessions
 * @package  Libraries
 * @author   Tariqul Islam <tareq@webkutir.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.codeigniter.com/user_guide/libraries/sessions.html
 */
class CI_Session
{
    var $session_id_ttl; // session id time to live (TTL) in seconds
    var $flash_key = 'flash'; // prefix for "flash" variables (eg. flash:new:message)

    /**
     * Session
     * 
     * @return void
     */
    function CI_Session()
    {
    	$this->object =& get_instance();
        log_message('debug', "Native_session Class Initialized");
        $this->_sess_run();
    }

    /**
    * Regenerates session id
    * 
    * @return void
    */
    function regenerate_id()
    {
        // copy old session data, including its id
        $old_session_id = session_id();
        $old_session_data = $_SESSION;
        
        // regenerate session id and store it
        session_regenerate_id();
        $new_session_id = session_id();

        // switch to the old session and destroy its storage
        session_id($old_session_id);
        session_destroy();

        // switch back to the new session id and send the cookie
        session_id($new_session_id);
        session_start();

        // restore the old session data into the new session
        $_SESSION = $old_session_data;

        // update the session creation time
        $_SESSION['regenerated'] = time();

        // session_write_close() patch based on this thread
        // http://www.codeigniter.com/forums/viewthread/1624/
        // there is a question mark ?? as to side affects

        // end the current session and store session data.
        session_write_close();
    }

    /**
    * Destroys the session and erases session storage
    * 
    * @return void
    */
    function destroy()
    {
        //unset($_SESSION);
        session_unset();
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time()-42000, '/');
        }
        session_destroy();
    }

    /**
    * Reads given session attribute value
    * 
    * @param string $item item of userdata
    * 
    * @return mixed
    */    
    function userdata($item)
    {
        if ($item == 'session_id') { //added for backward-compatibility
            return session_id();
        } else {
            return ( ! isset($_SESSION[$item])) ? false : $_SESSION[$item];
        }
    }

    /**
     * Reads all session Data
     * 
     * @return array
     */
    function all_userdata()
    {
        return $_SESSION;
    }

    /**
    * Sets session attributes to the given values
    * 
    * @param array  $newdata New data array to be set
    * @param string $newval  New Value to be set
    * 
    * @return void
    */
    function set_userdata($newdata = array(), $newval = '')
    {
        if (is_string($newdata)) {
            $newdata = array($newdata => $newval);
        }
    
        if (count($newdata) > 0) {
            foreach ($newdata as $key => $val) {
                $_SESSION[$key] = $val;
            }
        }
    }

    /**
    * Erases given session attributes
    * 
    * @param array $newdata array of data
    * 
    * @return void
    */
    function unset_userdata($newdata = array())
    {
        if (is_string($newdata)) {
            $newdata = array($newdata => '');
        }

        if (count($newdata) > 0) {
            foreach ($newdata as $key => $val) {
                unset($_SESSION[$key]);
            }
        }
    }

    /**
    * Starts up the session system for current request
    * 
    * @return void
    */
    function _sess_run()
    {
    	$session_id_ttl = $this->object->config->item('sess_expiration');
    	
    	if (is_numeric($session_id_ttl))
    	{
    		if ($session_id_ttl > 0)
    		{
    			$this->session_id_ttl = $this->object->config->item('sess_expiration');
    		}
    		else
    		{
    			$this->session_id_ttl = (60*60*24*365*2);
    		}
    	}
        session_start();

        // check if session id needs regeneration
        if ($this->_session_id_expired()) {
            // regenerate session id (session data stays the
            // same, but old session storage is destroyed)
            $this->regenerate_id();
        }

        // delete old flashdata (from last request)
        $this->_flashdata_sweep();

        // mark all new flashdata as old (data will be deleted before next request)
        $this->_flashdata_mark();
    }

    /**
    * Checks if session has expired
    * 
    * @return void
    */
    function _session_id_expired()
    {
        if (!isset($_SESSION['regenerated'])) {
            $_SESSION['regenerated'] = time();
            return false;
        }

        $expiry_time = time() - $this->session_id_ttl;

        if ($_SESSION['regenerated'] <=  $expiry_time) {
            return true;
        }

        return false;
    }

    /**
    * Sets "flash" data which will be available only in next request (then it will
    * be deleted from session). You can use it to implement "Save succeeded" messages
    * after redirect.
    * 
    * @param string $key   key
    * @param string $value value
    * 
    * @return void
    */
    function set_flashdata($key, $value)
    {
        $flash_key = $this->flash_key.':new:'.$key;
        $this->set_userdata($flash_key, $value);
    }

    /**
    * Keeps existing "flash" data available to next request.
    * 
    * @param string $key key
    * 
    * @return void
    */
    function keep_flashdata($key)
    {
        $old_flash_key = $this->flash_key.':old:'.$key;
        $value = $this->userdata($old_flash_key);

        $new_flash_key = $this->flash_key.':new:'.$key;
        $this->set_userdata($new_flash_key, $value);
    }

    /**
    * Returns "flash" data for the given key.
    * 
    * @param string $key key
    * 
    * @return void
    */
    function flashdata($key)
    {
        $flash_key = $this->flash_key.':old:'.$key;
        return $this->userdata($flash_key);
    }

    /**
     * Gets all the flashdata stored in CI session
     *
     * @return void
     */
    function all_flashdata()
    {
        $all_flashdata=$res=array();
        foreach ($this->all_userdata() as $k=>$v) {
            if (preg_match('/^flash:old:(.+)/', $k, $res)) {
                $all_flashdata[$res[1]] = $this->userdata($this->flashdata_key.':old:'.$res[1]);
            }
        }
        return $all_flashdata;
    }

    /**
    * Internal method - marks "flash" session attributes as 'old'
    * 
    * @return void
    */
    private function _flashdata_mark()
    {
        foreach ($_SESSION as $name => $value) {
            $parts = explode(':new:', $name);
            if (is_array($parts) && count($parts) == 2) {
                $new_name = $this->flash_key.':old:'.$parts[1];
                $this->set_userdata($new_name, $value);
                $this->unset_userdata($name);
            }
        }
    }

    /**
    * Internal method - removes "flash" session marked as 'old'
    * 
    * @return void
    */
    private function _flashdata_sweep()
    {
        foreach ($_SESSION as $name => $value) {
            $parts = explode(':old:', $name);
            if (is_array($parts) && count($parts) == 2 && $parts[0] == $this->flash_key) {
                $this->unset_userdata($name);
            }
        }
    }
}
