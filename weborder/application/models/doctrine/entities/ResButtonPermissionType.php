<?php



namespace entities;

/**
 * ResButtonPermissionType
 *
 * @Table(name="res_button_permission_type")
 * @Entity
 */
class ResButtonPermissionType
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $permissionType
   *
   * @Column(name="permission_type", type="string", length=150, nullable=false)
   */
  private $permissionType;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set permissionType
   *
   * @param string $permissionType
   */
  public function setPermissionType($permissionType)
  {
    $this->permissionType = $permissionType;
  }

  /**
   * Get permissionType
   *
   * @return string 
   */
  public function getPermissionType()
  {
    return $this->permissionType;
  }
}