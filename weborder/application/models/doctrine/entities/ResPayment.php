<?php



namespace entities;

/**
 * ResPayment
 *
 * @Table(name="res_payment")
 * @Entity
 */
class ResPayment
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var ResInvoice
   *
   * @ManyToOne(targetEntity="ResInvoice")
   * @JoinColumns({
   *   @JoinColumn(name="invoice_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $invoice;
  
  /**
   * @var ResOrder
   *
   * @ManyToOne(targetEntity="ResOrder")
   * @JoinColumns({
   *   @JoinColumn(name="order_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $order;
  
  /**
   * @var ResPaymentMethod
   *
   * @ManyToOne(targetEntity="ResPaymentMethod")
   * @JoinColumns({
   *   @JoinColumn(name="payment_method_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $paymentMethod;
  
  /**
   * @var decimal $amount
   *
   * @Column(name="amount", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $amount=0;
  
  /**
   * @var ResVoucher
   *
   * @ManyToOne(targetEntity="ResVoucher")
   * @JoinColumns({
   *   @JoinColumn(name="voucher_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $voucher;
  
  /**
   * @var string $chequeNumber
   *
   * @Column(name="cheque_number", type="string", length=15, nullable=true)
   */
  private $chequeNumber;
  
  /**
   * @var string $ccNumber
   *
   * @Column(name="cc_number", type="string", length=16, nullable=true)
   */
  private $ccNumber;
  
  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set invoice
   *
   * @param ResInvoice $invoice
   */
  public function setInvoice(\ResInvoice $invoice)
  {
  	$this->invoice = $invoice;
  }
  
  /**
   * Get invoice
   *
   * @return ResInvoice
   */
  public function getInvoice()
  {
  	return $this->invoice;
  }
  
  /**
   * Set order
   *
   * @param ResOrder $order
   */
  public function setOrder(\ResOrder $order)
  {
  	$this->order = $order;
  }
  
  /**
   * Get order
   *
   * @return ResOrder
   */
  public function getOrder()
  {
  	return $this->order;
  }
  
  /**
   * Set paymentMethod
   *
   * @param ResPaymentMethod $paymentMethod
   */
  public function setPaymentMethod(\ResPaymentMethod $paymentMethod)
  {
  	$this->paymentMethod = $paymentMethod;
  }
  
  /**
   * Get paymentMethod
   *
   * @return ResPaymentMethod
   */
  public function getPaymentMethod()
  {
  	return $this->paymentMethod;
  } 
  
  /**
   * Set amount
   *
   * @param decimal $amount
   */
  public function setAmount($amount)
  {
  	$this->amount = $amount;
  }
  
  /**
   * Get amount
   *
   * @return decimal
   */
  public function getAmount()
  {
  	return $this->amount;
  }
  
  /**
   * Set voucher
   *
   * @param ResVoucher $voucher
   */
  public function setVoucher(\ResVoucher $voucher)
  {
  	$this->voucher = $voucher;
  }
  
  /**
   * Get voucher
   *
   * @return ResVoucher
   */
  public function getVoucher()
  {
  	return $this->voucher;
  }
  
  /**
   * Set chequeNumber
   *
   * @param string $chequeNumber
   */
  public function setChequeNumber($chequeNumber)
  {
  	$this->chequeNumber = $chequeNumber;
  }
  
  /**
   * Get chequeNumber
   *
   * @return string
   */
  public function getChequeNumber()
  {
  	return $this->chequeNumber;
  }
  
  /**
   * Set ccNumber
   *
   * @param string $ccNumber
   */
  public function setCcNumber($ccNumber)
  {
  	$this->ccNumber = $ccNumber;
  }
  
  /**
   * Get ccNumber
   *
   * @return string
   */
  public function getCcNumber()
  {
  	return $this->ccNumber;
  }
  
  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
}