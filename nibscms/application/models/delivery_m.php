<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->deliverCharge="web_delivery_charge";
		$this->tablename=$this->deliverCharge;
	}
	

	
	function getAllDeliverySettings($id,$order_by="id",$direction="asc"){
		return $this->db->select("*")
		->where("$this->tablename.restaurant_id",$id)
		->order_by($order_by,$direction)->get($this->tablename)->result_array();
	}
	
	function getdelivery($id){
		return $this->db->select("*")
		->where("$this->tablename.id",$id)		
		->get($this->tablename)->row_array();		 
	}
	
	function updateDeliveryNotTake($restaurant){
		return $this->db->where("$this->tablename.restaurant_id",$restaurant)
		->update($this->tablename,array('is_not_take_order'=>0));
	}

	
}
?>