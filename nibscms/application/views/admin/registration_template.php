<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/register.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/register.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
  <div class="padded"><h1><?=$pagename?></h1>
  <div class="initPage">
    <div>
      <p>Click on the list of Entries on the left to view....</p>
      <p>To delete a entry use the <img src="<?=$baseUrl?>assets/images/admin/icon_trash.png" /> delete icon...</p>
      <p>Or... you can de-activate an entry by un-ticking it...</p>
    </div>
  </div></div>
</script>

<div id="main-content" class="">
  <form method='post' action="<?=$baseUrl?>admin/registration/exportFile">
    <input type="submit" value="Export" style="float:right; margin:5px;"/>
  </form>
  <ul class="comp_multilineListView nofooter">
    <?=$list1?>
  </ul>
</div>

<div id="sidebar" class="nav-collapse collapse">
  <div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
  <div id="sidebarHeading">
    <div class="headertoolbar" style='padding-left:15px'>
      <span><i class='icon-cog'></i>
        <em>Registered Users</em>
      </span>
    </div>
  </div> 
</div>

  
 

<script type="text/javascript">
  var _KA=window.setInterval(function(){
    $.ajax({
      url:base_url+"admin/content/XkeepAlive",
      dataType:"json",
      type:"post",
      data:{"keepAlive":"true"}
    })
  },180000);

</script>
