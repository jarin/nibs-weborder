<?php



namespace entities;

/**
 * ResCity
 *
 * @Table(name="res_city")
 * @Entity
 */
class ResCity
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="integer", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $cityName
   *
   * @Column(name="city_name", type="string", length=100, nullable=false)
   */
  private $cityName;


  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set cityName
   *
   * @param string $cityName
   */
  public function setCityName($cityName)
  {
    $this->cityName = $cityName;
  }

  /**
   * Get cityName
   *
   * @return string 
   */
  public function getCityName()
  {
    return $this->cityName;
  }
}