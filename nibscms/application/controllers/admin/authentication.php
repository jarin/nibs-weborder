<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller {


	function __construct(){
		parent::__construct();
	}


	function index(){
		$this->logout();
	}

	function login(){
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$this->load->model("auth");
		$auth_ret=$this->auth->authorise(
			$username,
			$password
			);
		if($auth_ret && is_array($auth_ret)){
			foreach($auth_ret as $k=>$v){
				$this->session->set_userdata($k,$v);
			}
			header("Location: ".base_url()."admin/home");
		}else{
			header("Location: ".base_url()."admin/authentication/showlogin/Invalid username or password");
		}
	}

	function logout(){
		$this->session->unset_userdata("uid");
		$this->session->unset_userdata("uname");
		$this->session->unset_userdata("utype");
		$this->session->unset_userdata("uimg");
		$this->session->sess_destroy();
		header("Location: ".base_url()."admin/authentication/showlogin");
	}

	function showLogin($msg=""){
        // $this->load->view("admin/admin_head",array("section"=>""));
		$this->load->view("admin/login",array("msg"=>urldecode($msg)));
        // $this->load->view("admin/admin_foot");
	}

	function kickOut(){
		$this->tab="kickout";
		$this->load->view("admin/admin_head",array("section"=>"","tab"=>$this->tab));
		$this->load->view("admin/kickout");
		$this->load->view("admin/admin_foot");
	}

}
?>