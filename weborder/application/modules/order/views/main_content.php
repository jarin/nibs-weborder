

            <div id="loading" style="position:fixed; top:0px; padding-top: 10px; text-align: center;width: 100%;z-index: 5;"></div>
            <div class="col-md-9 col-sm-12 col-md-push-0">
				<ul class="nav nav-tabs" role="tablist">
					
					<?php if($categoryid !=''){
					    	if(count($categories) > 0){
					        foreach ($categories as $cat){
					            if($cat['id'] == $categoryid){
                                ?>
                                <li <?php if($cat['id'] == $categoryid && $subCategoryid =='') echo 'class="active"';?>><a href="order/index/<?php echo $categoryid;?>">All</a></li>
                                <?php 
					                if(count($cat['childCats']) >0){
					                    foreach ($cat['childCats'] as $subCat){
					                      ?>
					                      <li <?php if($subCat['id'] == $subCategoryid) echo 'class="active"';?>><a href="order/index/<?php echo $cat['id'].'/'.$subCat['id']?>"><?php echo $subCat['categoryName']?></a></li>
				
					                      <?php  
					                    }
					                }
					            }
					        }
					    }
					
					 } else {
                        ?>
                        <li class="active"><a data-toggle="tab" role="tab" href="#tab-1">All</a></li>
                        <?php 
                        }
					    ?>
				</ul>

				<div class="view-style">
					<!--  <div class="list-grid-view">
						<button class="thumb-view"><i class="fa fa-list"></i></button>
						<button class="without-thumb"><i class="fa fa-align-justify"></i></button>
						<button class="grid-view"><i class="fa fa-th-list"></i></button> 
					</div>-->
					<!-- end .list-grid-view -->
                    
					<div class="page-list text-right">
						<ul class="list-unstyled list-inline">
						    <?php if($count > $limit){
						       
						        for($i=1;$i<ceil($count/$limit)+1;$i++){
						     ?>
							<li><a class="next"  rel="<?=($i-1)?>" data="<?=($i*$limit)?>"><?=$i?></a>
							</li>
						
							<?php } } ?>
						</ul>
					</div>
					<!-- end .page-list -->
				</div>
				<!-- end view-style -->
				<div class="tab-content">
					<div class="tab-pane fade in active allitems" id="tab-1">
					    						
							 <?php
							 
						    if(count($items) > 0){ 
                                $catshow = array();
						        for($i=0;$i<$limit;$i++){ 
                            ?>
                            
                            
                            
                            <?php 
						            if(isset($items[$i])){
                             ?>
                             <?php       
                                     if(!in_array($items[$i]['category']['id'], $catshow)){                                
						        ?>
						    <div class="all-menu-details thumb">
						    <h5><?php echo $items[$i]['category']['categoryName'];?></h5>
						    <?php 
                                }
                                array_push($catshow,$items[$i]['category']['id']);
						    ?>
							<div class="item-list">
								<!--  <div class="list-image" >
									<img src="assets/img/content/menu-list-img.jpg" alt="">
								</div>-->
								<div class="all-details">
									<div class="visible-option">
										<div class="details">
											<h6><?=($i+1)?>. <?=$items[$i]['longName']?></h6>
											<input type="hidden" value="<?=$items[$i]['longName'];?>" id="name_<?=$items[$i]['id'];?>" name="name_<?=$items[$i]['id'];?>">
											
											<ul class="share-this list-inline text-right">
												<li><a href="#">Share</a>
													<ul class="list-inline">
														<li><a class="fb_link" href=".fb_link" data-name="<?=$items[$i]['productName'];?>" data-description="<?=$items[$i]['description']?>" data-price="<?=$items[$i]['webOrderPrice'];?>"><i class="fa fa-facebook-square"></i></a></li>
														<li>
															<a href="https://twitter.com/share?url=<?=urlencode(base_url())?>&text=<?=urlencode($items[$i]['productName']);?>">
															  <i class="fa fa-twitter-square"></i></a>
														</li>
														<li><a href="#" class="g-interactivepost"
																	  data-contenturl="<?=base_url()?>"
																	  data-contentdeeplinkid="/pages"
																	  data-clientid="<?=$google_plus_client_id?>"
																	  data-cookiepolicy="single_host_origin"
																	  data-prefilltext="<?=$items[$i]['productName'];?> - <?=$items[$i]['description']?>"
																	  data-calltoactionlabel="CREATE"
																	  data-calltoactionurl="<?=base_url()?>"
																	  data-calltoactiondeeplinkid="/pages/create"><i class="fa fa-google-plus-square"></i></a></li>
														<!--  <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>-->
													</ul>
												</li>
											</ul>
											<p class="m-with-details"><strong>Description:</strong><br><?=$items[$i]['description']?></p>

											<!--  <p class="m-with-details"><strong>Ingredients:</strong><br>5 tiger shrimps, garlic, butter, lemon, herbs, 5 tiger shrimps, garlic, butter, lemon, herbs</p>-->

											<p class="for-list"><?=$items[$i]['description']?></p>
										</div>

										<div class="price-option fl">
											<h4>&pound; <?=$items[$i]['webOrderPrice'];?></h4>
											<input type="hidden" value="<?=$items[$i]['webOrderPrice'];?>" id="price_<?=$items[$i]['id'];?>" name="price_<?=$items[$i]['id'];?>">
											<input type="hidden" value="<?=$items[$i]['webOrderPriceActual'];?>" id="actualprice_<?=$items[$i]['id'];?>" name="actualprice_<?=$items[$i]['id'];?>">
											<button class="toggle ingredient" rel="<?=$items[$i]['id'];?>">Option</button>
										</div>
										<!-- end .price-option-->
										<div class="qty-cart text-center">
											<h6>Qty</h6>
											<form class="default-form">
												<input type="text" placeholder="1" value="1" id="qty_<?=$items[$i]['id'];?>" name="qty_<?=$items[$i]['id'];?>">
												<br>
												<button class="addCart" rel="<?=$items[$i]['id'];?>"><i class="fa fa-shopping-cart"></i>
												</button>
											</form>
										</div> <!-- end .qty-cart -->
									</div> <!-- end .vsible-option -->
									

									<div class="dropdown-option clearfix ingredient_<?=$items[$i]['id'];?>">
										
									</div>
									<!--end .dropdown-option-->
								</div>
								<!-- end .all-details -->
							</div>
							<!-- end .item-list -->
							<?php
							if(isset($items[$i+1]['category']['id'])) {
							if(!in_array($items[$i+1]['category']['id'], $catshow)){
							?>
							</div>
						    <!--end all-menu-details-->
						    <?php } 
                                } else {
                             ?>
                             </div>
						    <!--end all-menu-details-->
                             <?php
                                }
						    ?>
                            <?php  
                                        
                                    }
                                  }
                                } else { 

                                     if(count($categories) > 0){
									$i = 0;
                                    foreach ($categories as $cat){
                                        if($categoryid == $cat['id']){
                                       if($subCategoryid !='') { 
                                            if(count($cat['childCats']) > 0){
                                                foreach ($cat['childCats'] as $subcat){
                                                   if($subCategoryid == $subcat['id']){ ?>
                                                   <div class="all-menu-details ">
                    						        <h5><?php echo $subcat['categoryName'];?></h5>
                    						        <p>No item found for this category</p>
                    						    </div>
                                              <?php      
                                               } 
                                             }
                                           }
                                        } else {
                                ?>
                                <div class="all-menu-details ">
    						        <h5><?php echo $cat['categoryName'];?></h5>
    						        <p>No item found for this category</p>
    						    </div>
                                <?php 
                                            }
                                        } else{ if($this->input->post('search') !='' && $i==0) { ?>
                                	<div class="all-menu-details ">
										
    						        	<p>No item found for <?php echo ($this->input->post('search'))?$this->input->post('search'):''?></p>
    						    	</div>
                                <?php }	
                                        }
                                        $i++;
                                    }
                                } 
                              }
                                ?>
						
						
						<!--  <div class="pagination">
							<ul class="list-inline  text-right">
								<li class="active"><a href="#">1</a>
								</li>
								<li><a href="#">2</a>
								</li>
								<li><a href="#">3</a>
								</li>
								<li><a href="#">4</a>
								</li>
								<li><a href="#">5</a>
								</li>
								<li><a href="#">6</a>
								</li>
							</ul>
						</div> -->
						<!-- end .pagination -->

					</div> <!-- end .tab-pane -->

					
				</div> <!-- end .tab-content -->
			</div>
			<!--end main-grid layout-->
<script type="text/javascript">	
var body=$("body");
$(document).ready(function(e){
	$(".page-list .list-inline li:first-child").addClass("active disabled");
	var maxshow = 5;
	var items = $(".page-list .list-inline li");
   
    items.slice(maxshow).hide();
    
});
body.find(".next").click(function(e){
	var _this=$(this),
	rel = parseInt(_this.attr("rel")),
	data = parseInt(_this.attr("data")),
	row = body.find(".allitems"),
	search = body.find('#search').val();
	
	
	 
	if(!_this.parent().hasClass('disabled'))
	{
		body.find('#loading').html('<img src="<?=base_url();?>assets/img/loading.gif"/>');
		$.ajax({
			
			async: false,
		  type: "POST",
		  url: "order/getNextItem",
		  data: "page="+rel+"&data="+data+"&categoryId="+<?php echo ($categoryid!='')?$categoryid:'0'?>+"&subCategoryId="+<?php echo ($subCategoryid!='')?$subCategoryid:'0'?>+"&search="+search,
		  success:function(data){
			  setTimeout(function(){  $('#loading').html(''); } , 1000);
			 
			  row.html(data);
			  var items = body.find('.page-list .list-inline li');
			    items.removeClass("active disabled");
				_this.parent().addClass("active disabled");
				var maxshow = 5;
				
			    var currentpage = _this.parent().prevAll().length;
			   
				var pages = items.length;
			   if(pages > maxshow)
			    {
				    if((pages - currentpage)  < 4){
					   var showFrom =  currentpage - (maxshow -(pages - currentpage));
				       var showTo = showFrom+maxshow;
				   } else if(currentpage > 3){
					   var showFrom = currentpage - 2;
				       var showTo = showFrom+maxshow;
				   }else {
					   var showFrom = 0;
				       var showTo = showFrom+maxshow;
				   }

			       
			       items.hide(0);
			       items.slice(showFrom, showTo).show(0);
			    	
			    }
		  }
		});
	}
});

$("body").on("click", ".ingredient", function(e){
	e.preventDefault();
	var _this=$(this),
	rel = parseInt(_this.attr("rel")),
	row = body.find(".ingredient_"+rel);
    	if(_this.hasClass('active')){
        	
    		$.ajax({
    			
    			async: false,
    		  type: "POST",
    		  url: "order/getIngredients",
    		  data: "pid="+rel,
    		  success:function(data){
    			  row.html(data);
    			  
    			
    		  }
    		});
    	}
	});

/*$("body").on("click", ".addIngred", function(e){
	e.preventDefault();
	
	var _this=$(this),
	rel = parseInt(_this.attr("rel")),
	row = body.find('#cartsucces'),
	div = body.find('.mycart');
	form = $('#formIngred_'+rel);
	
	$.ajax({
		async: false, 
		type: "POST",
		url: "order/addIngredient",
		data: form.serialize(),
		success:function(data){
		div.html(data);
		form.find('.removeIngred').addClass('disabled'); 
		 row.html('Ingredients is added to your cart');
		 row.show(0);
		 setTimeout(function(){ row.hide(0); } , 1000);
		
		},
        error: function(MLHttpRequest, textStatus, errorThrown){
            alert(errorThrown);
        }
		
	});
	
	});
	
$("body").on("click", ".removeIngred", function(e){
	e.preventDefault();
	var _this=$(this),
	rel = parseInt(_this.attr("rel")),
	form = $('#formIngred_'+rel);

	if(!_this.hasClass("disabled"))
	{
    	
		form.find('input[type=checkbox]:checked').removeAttr('checked');
		form.find('.checkbox-input').removeClass('active');
		form.find('textarea').val('');
		form.find('.removeIngred').removeClass('ajaxCall'); 
    		
	}


		
	});*/


</script>