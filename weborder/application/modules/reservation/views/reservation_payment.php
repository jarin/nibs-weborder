       <div id="page-content">

		<!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-12 col-sm-12 col-md-push-0">
    				       <?php echo validation_errors();?>
                                        
                            <?php if($this->session->flashdata("error") !='') echo '<p class="text-danger">'.$this->session->flashdata("error").'</p>';?>
                                      
                           <?php 
	                             if($gateway['mode'] == 'test'){
	                             	$paypalUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	                             } else {
	                             	$paypalUrl = 'https://www.paypal.com/cgi-bin/webscr';
	                             }
                             ?>
                            <form id="reservationPay" method="POST" action="<?=$paypalUrl?>">
                                <input type="hidden" name="custom" value="<?=$reservInfo['id']?>"/>
                                <input type="hidden" name="cmd" value="_xclick"/>
                                <input type="hidden" name="business" value="<?=$gateway['merchant_id']?>"/>
                               
                                <input type="hidden" name="currency_code" value="GBP"/>
                                <input type='hidden' name='item_name' value='Reservation depost' />
                               <input type='hidden' name='amount' value='<?=$restuarntInfo['deposit_amount']?>' />
                               
	            				 <input type="hidden" name="return" value="<?=base_url().'payment/paypal/reservationSuccess'?>"/>
                                 <input type="hidden" name="cancel_return" value="<?=base_url().'payment/paypal/reservationCancel/' .$reservInfo['id'] ?>"/>
                              
                               <input type="hidden" name="notify_url" value="<?=base_url().'payment/paypal/reservationNotify'?>"/>
                             
                     
                        
                              
                            <div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                          <h4>Reservation Information</h4>
                                          <p>You need to pay deposite amount for reservation</p>
                                          <div class="row">
                                          		<?php if(count($reservInfo) > 0){ ?>
                                          			<div class="col-md-12 col-sm-12">
		                                                <label>Reservation Time:</label>
		                                            
		                                             	<span><?=$reservInfo['timeFrom']->format('d-m-y h:m')?></span>
		                                            </div>
		                                            <div class="col-md-12 col-sm-12">
		                                                <label>No. of People:</label>
		                                            
		                                             	<span><?=$reservInfo['noOfGuests']?></span>
		                                            </div>
		                                            
		                                            <?php if($reservInfo['specialInstruction'] !=''){ ?>
		                                             <div class="col-md-12 col-sm-12">
		                                                <label>Special request:</label>
		                                            
		                                             	<span><?=$reservInfo['specialInstruction']?></span>
		                                            </div>
		                                            <?php } ?>
		                                            
		                                            <?php if(count($reservInfo['promo']) >0){ ?>
		                                             <div class="col-md-12 col-sm-12">
		                                                <label>Promotional Code:</label>
		                                            
		                                             	<span><?=$reservInfo['promo']['promoType']?></span>
		                                            </div>
		                                            <?php } ?>
                                          		<?php } ?>
	                                            <?php if(count($restuarntInfo)> 0){
	                                            	if($restuarntInfo['is_deposit'] ==1){
	                                            ?>
	                                            <div class="col-md-12 col-sm-12">
	                                                <label>Deposit Amount:</label>
	                                            
	                                             	<span>&pound; <?=$restuarntInfo['deposit_amount']?></span>
	                                            </div>
	                                            <?php } } ?>
	                                                                        
	                                     
                                        	
                                        		
                                            </div>
											
									    </div>
									</div>
							</div>
						
              
							
                    	    <button class="btn btn-default-red" type="submit"  style="float:right;margin-top:15px"><i class="fa fa-money"></i> Pay</button>
						</form>
										     
					</div>
			            <!-- end .col-md-9 -->

					
				</div>
				<!-- end .row -->
			</div>
			<!--end .container -->
			
		