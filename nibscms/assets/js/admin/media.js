$(document).ready(function() {

  //initial file view
  $("body").addClass(($.cookie("filesview")||"") + "View");

  // Add Button
  $("button.addFileBtn").overlay({width:"30%",inline:true,title:"Add New File",href:"#addDownloadForm"});

	//--------DRAGGABLES AND DROPPABLES
	(function($){
	  var t,BASEoffset,perfThreshold=50;
	  $("#downloadInner div.download:not(.noDrag)").livequery(function(){
  		$(this).draggable({
  			zIndex:"2700",
  			scroll:false,
  			appendTo:"body",
  			helper:"clone",
  			start:function(){
  			  var _this=$(this);
  			  if(_this.hasClass("ui-selected")){
  			    var siblings=_this.siblings().filter(".ui-selected");
  			    var count=siblings.add(_this).length;
  			    if(count>1){
  			      var clones=[];
  			      BASEoffset=_this.offset();
  			      if(count<perfThreshold){
  			        siblings.each(function(){
    			        var sibling=$(this),
    			        offset=sibling.offset(),
    			        clone=sibling.clone();
    			        clone.css({
    			          position:"absolute",
    			          top:offset.top,
    			          left:offset.left,
    			          zIndex:2699
    			        })
    			        .addClass("animate_transform")
    			        .data("x",offset.left)
    			        .data("y",offset.top)
    			        .appendTo("body");
    			        clones.push(clone);
    			      });
  			      }

  			      //counter
  			      var counter=$("<div class='drag_count'>"+count+"</div>")
  			      .addClass("no_transform")
  			      .css({
  			        position:"absolute",
			          top:BASEoffset.top,
			          left:BASEoffset.left,
			          zIndex:2701
  			      })
			        .data("x",BASEoffset.left)
			        .data("y",BASEoffset.top)
  			      .appendTo("body");

  			      clones.push(counter);

  			      _this.data("siblings",clones);
  			      t=setTimeout(function(){
  			        animate(clones);
  			      },500);
  			    }else{
  			      _this.data("siblings",null);
  			    }
  		    }else{
  		      _this.data("siblings",null);
  		    }
  			},
  			drag:function(e,ui){
  			  var _this=$(this),
  			  siblings=_this.data("siblings");
  			  if(siblings){
  			    var offset=ui.offset,
  			    op=ui.originalPosition
  			    $.each(siblings,function(){
  			      var sibling=$(this);
  			      sibling.css({
  			        left:parseInt(sibling.data("x"),10) + (offset.left-op.left),
  			        top:parseInt(sibling.data("y"),10) + (offset.top-op.top)
  			      });
  			    });
  			  }
  			},
  			revert:function(){
  			  var _this=$(this),
  			  siblings=_this.data("siblings");
  			  if(siblings){
  			    $.each(siblings,function(){
  			      var sibling=$(this);
  			      sibling.addClass("no_transform").animate({
  			        left:parseInt(sibling.data("x"),10),
  			        top:parseInt(sibling.data("y"),10)
  			      });
  			    });
  			  }
  			  return true;
  			},
  			stop:function(e,ui){
  			  clearTimeout(t);
  			  var _this=$(this),
  			  siblings=_this.data("siblings");
  			  if(siblings){
  			    $.each(siblings,function(){
  			      $(this).remove();
  			    });
  			  }
  			  _this.data("siblings",null);
  			}
  		});
  	}).live("click",function(e){
  		var _this=$(this);
  		if(e.metaKey){
  		  _this.toggleClass("ui-selected");
  		  MultipleFilesSelected();
  		}else{
  		  _this.addClass("ui-selected").siblings(".ui-selected")
    		.removeClass("ui-selected");
    		loadFileDetails(_this);
  		}

  	});

  	function animate(items){
  	  var left=BASEoffset.left,
  	  top=BASEoffset.top;
  	  $.each(items,function(){
  	    var deg=(Math.floor(Math.random()*10)) - 5,
  	    _this=$(this);
        var dx=left-parseInt(_this.data("x"),10),
        dy=top-parseInt(_this.data("y"),10);
        _this.css({
          transform: 'translate(' + dx + 'px,' + dy + 'px) rotate('+deg+'deg) ',
          '-moz-transform': 'translate(' + dx + 'px,' + dy + 'px) rotate('+deg+'deg) ',
          '-o-transform': 'translate(' + dx + 'px,' + dy + 'px) rotate('+deg+'deg) ',
          '-webkit-transform': 'translate(' + dx + 'px,' + dy + 'px) rotate('+deg+'deg) '
        });
  	  });
  	}

	})(jQuery)

	$("#downloads").selectable({
	  filter:"div.download:not(.noDrag)",
	  selected:function(){
	    MultipleFilesSelected();
	  }
	});

	$("#trashCan").droppable({
		accept:".download",
		hoverClass:"onDrop",
		tolerance:"pointer",
		drop:function(e,ui){
			var dropped=ui.draggable;
			if(dropped.hasClass("ui-selected")){
			  var selected=$("#downloadInner").find(".ui-selected:not(.drghelper)");
			  var msg=selected.length>1 ? "Delete "+selected.length+" files?": "Are you sure?";
			  if(confirm(msg)){
			    var ids=[];
			    selected.each(function(){
			      var e=$(this).attr("id").match(/(.+)[-=_](.+)/),
			      id=e[2];
			      ids.push(id);
			    });
			    if(ids.length){
			      NIBS.util.ajax({ids:ids.join(",")},"admin/media/deleteFile");
			    }
			  }
			}else{
			  if(confirm("Are you sure?")){
  				var e=dropped.attr("id").match(/(.+)[-=_](.+)/);
  				var id=e[2];
  				NIBS.util.ajax({id:id},"admin/media/deleteFile");
  			}
			}

		}
	});
	//--------END DRAGGABLES AND DROPPABLES
	//--------CHECK BOXES
	$("#downloads div.download input[type=checkbox]").livequery("click",function(){
		act_deactivateFile($(this));
		return false;
	});
	//--------END CHECK BOXES
	//--------TOOLBAR
		$("a.showLarge").click(function(){
			$("body").removeClass("smallView")
			.removeClass("listView");
			$(this).blur().addClass("selected")
			.siblings().removeClass("selected");
			$.cookie("filesview","large");
			return false;
		});
		$("a.showSmall").click(function(){
			$("body").removeClass("listView")
			.addClass("smallView");
			$(this).blur().addClass("selected")
			.siblings().removeClass("selected");
			$.cookie("filesview","small");
			return false;
		});
		$("a.showList").click(function(){
			$("body").removeClass("smallView")
			.addClass("listView");
			$(this).blur().addClass("selected")
			.siblings().removeClass("selected");
			$.cookie("filesview","list");
			return false;
		});
		$("a.sortName").click(function(){
			var mylist=$("#downloadInner");
			var listitems = mylist.children('div.download').get();
			listitems.sort(function(a, b) {
				var compA = $(a).find("span.name").text().toUpperCase();
				var compB = $(b).find("span.name").text().toUpperCase();
				return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
			});
			$.each(listitems, function(idx, itm) { mylist.append(itm); });
			$(this).blur();
			return false;
		});
		$("a.sortDate").click(function(){
			var mylist=$("#downloadInner");
			var listitems = mylist.children('div.download').get();
			listitems.sort(function(a, b) {
				var compA = $(a).find("span.uploadedOn").text();
				var compB = $(b).find("span.uploadedOn").text();
				var dateA = new Date(
					parseInt(compA.substring(6,10),10),
					parseInt(compA.substring(3,5),10)-1,
					parseInt(compA.substring(0,2),10),
					parseInt(compA.substring(11,13),10),
					parseInt(compA.substring(14,16),10),
					parseInt(compA.substring(17,19),10)
				);
				var dateB = new Date(
					parseInt(compB.substring(6,10),10),
					parseInt(compB.substring(3,5),10)-1,
					parseInt(compB.substring(0,2),10),
					parseInt(compB.substring(11,13),10),
					parseInt(compB.substring(14,16),10),
					parseInt(compB.substring(17,19),10)
				);
				return (dateA > dateB) ? -1 : (dateA < dateB) ? 1 : 0;
			});
			$.each(listitems, function(idx, itm) { mylist.append(itm); });
			$(this).blur();
			return false;
		});
		$("#searchInput").keyup(NIBS.util.debounce(function(){
			var _this=$(this),
			_val=_this.val(),
			reg=null,
			total=0;
			if(_val!=""){
				_this.next().show(0);
			}
			if ($.trim(_val)!=""){
				reg=new RegExp(_val,"i")
			}
			if(reg){
			  setTimeout(function(){
			    $("#downloadInner div.download").css({display:"none"}).each(function(i,el){
            var $el=$(el);
            var txt=$(el).text();
            if((reg.test(txt))){
              $el.css({display:"inline-block"});
              total++;
            }
    			});
    			$("#numItems").text(total);
			  },10);
			}else{
			  $("#downloadInner div.download").css({display:"inline-block"});
			  _this.next().hide(0);
        $("#numItems").text("0");
			}
		},250));
		$("img.show_er").click(function(){
			$(this).hide().prev().val("");
			$("#downloadInner div.download").show(0);
			$("#numItems").text(0);
		});
	//--------TOOLBAR
	//--------MISC
	$("#uploadField").change(function(){
		var _this=$(this);
		var str=_this.val();
		var str=str.substring(str.lastIndexOf("\\")+1);
		var filename=str.replace("."+str.split(".").pop(),"");
		var _val2=$("#uploadFilename").val();
		if($.trim(_val2)==""){
			$("#uploadFilename").val($.trim(filename.replace(/[_\-\|\"\']/g," ").replace(/\s+/," ")));
		}

	});
	//--------MISC

	//--------DND FILE UPLOAD
	var ddBox=$("#downloads"),ddBoxC=$("#downloadInner"),
	ddBoxP=$("#mainContentInner"),
	pbTemplate="<div class='download noDrag pn deleted'  style='z-index:2700;' id='pb_{guid}'><span>{file}</span><span class='bar_holder'><span class='bar'></span></span></div>";

	ddBox.DNDU({url:base_url+"admin/media/addFileViaDragAndDrop"})
	.bind("DNDU_drop",function(){
		ddBoxP.removeClass("onDrop");
	})
	.bind("DNDU_leave",function(){
		ddBoxP.removeClass("onDrop");
	})
	.bind("DNDU_over",function(){
		ddBoxP.addClass("onDrop");
	})
	.bind("DNDU_processStart",function(e,guid,filename){
	  ddBox[0].scrollTop=0;
		ddBoxC.prepend(pbTemplate.replace("{guid}",guid).replace("{file}",filename));
		_add($("#pb_"+guid+""));
	})
	.bind("DNDU_uploadProgress",function(e,guid,loaded,total){
		$("#pb_"+guid+" .bar").css(
			{width:((loaded/total)*100) + "%"}
			);
	})
	.bind("DNDU_uploadEnd",function(e,guid){
		$("#pb_"+guid+" .bar").css(
			{width:"100%"}
			);
	})
	.bind("DNDU_processEnd",function(e,guid,data){
		try{var objData= eval( "(" + data + ")" ) || null;}catch(e){};
		if(objData){
			if(objData.success=="error"){
				alert(objData.msg);
				_remove($("#pb_"+guid+""));
			}else if(objData.success=="success"){
				if($.isFunction(objData.successFunction)){
					addFileViaDnDHelper(guid,objData.successFunction);
				}
			}
		}else{
			alert("Error in response, File(s) may not have been saved");
			console.log(data);
			_remove($("#pb_"+guid+""));
		}
	});

	//--------END DND FILE UPLOAD

});





function addFile(id,name,path,icon,date){
	var html="<div class='download deleted' id='download_"+id+"' style='z-index:2700;'><input type='checkbox' id='downloadCheckbox_"+id+"' checked /><span class='icon'><img src='"+base_url+""+icon+"' border='0' /></span><span class='name' title='"+name+"'>"+name+"</span><span class='filename' title='"+path+"'>"+path+"</span><span class='uploadedOn'>"+date+"</span></div>";
	$("#downloads")[0].scrollTop=0
	$("#downloadInner").prepend(html);
	_add($("#download_"+id));
}

function addFileViaDnDHelper(guid,fn){
  setTimeout(function(){
    $("#pb_"+guid+"").replaceWith(fn.apply());
  },500);
}
function addFileViaDnD(id,name,path,icon,date){
  return $("<div class='download' id='download_"+id+"'><input type='checkbox' id='downloadCheckbox_"+id+"' checked /><span class='icon'><img src='"+base_url+""+icon+"' border='0' /></span><span class='name' title='"+name+"'>"+name+"</span><span class='filename' title='"+path+"'>"+path+"</span><span class='uploadedOn'>"+date+"</span></div>");
}

function act_deactivateFile(el){
	if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({"id":id},"admin/media/activateDeactivateFile");

}

function do_act_deactivateFile(id,val){
	if(val=="0"){
		$("#downloadCheckbox_"+id).prop("checked",false);
		$("#download_"+id).addClass("not_visible");
	}else{
		$("#downloadCheckbox_"+id).prop("checked",true);
		$("#download_"+id).removeClass("not_visible");
	}
}

function deleteMultiFileHelper(download_id,delay){
  setTimeout(function(){
    deleteFile(download_id)
  },delay);
}
function deleteFile(download_id){
  _remove($("#download_"+download_id));
}

function _remove(el){
  el.addClass("deleted");
	setTimeout(function(){
	  el.remove();
	},600);
}
function _add(el){
  setTimeout(function(){
	  el.removeClass("deleted");
	  setTimeout(function(){
	    el.css({zIndex:""});
	  },600);
	},100);
}

function loadFileDetails(id){
	var e=id.attr("id").match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#downloadEditor").load(base_url+"admin/media/viewFileDetails/"+id);
}
function MultipleFilesSelected(){
  $("#downloadEditor").html("<div class='headertoolbar'>File Details</div><div class='padded' style='text-align:center'><b>Multiple files selected...</b></div>")
}


jQuery.cookie=function(a,b,c){if(typeof b=="undefined"){var i=null;if(document.cookie&&document.cookie!=""){var j=document.cookie.split(";");for(var k=0;k<j.length;k++){var l=jQuery.trim(j[k]);if(l.substring(0,a.length+1)==a+"="){i=decodeURIComponent(l.substring(a.length+1));break}}}return i}c=c||{},b===null&&(b="",c.expires=-1);var d="";if(c.expires&&(typeof c.expires=="number"||c.expires.toUTCString)){var e;typeof c.expires=="number"?(e=new Date,e.setTime(e.getTime()+c.expires*24*60*60*1e3)):e=c.expires,d="; expires="+e.toUTCString()}var f=c.path?"; path="+c.path:"",g=c.domain?"; domain="+c.domain:"",h=c.secure?"; secure":"";document.cookie=[a,"=",encodeURIComponent(b),d,f,g,h].join("")};

(function($){
	var support={"XHRsendAsBinary":false,"XHRupload":false,"fileReader":false,"formData":false};
	var s_xhr=new XMLHttpRequest();
	support.XHRsendAsBinary=("sendAsBinary" in s_xhr);
	support.XHRupload=("upload" in s_xhr);
	try{ // firefox,chrome
		support.fileReader = !!new FileReader();
		support.formData = !!new FormData();
	}catch(e){};

	$.fn.DNDU=function(settings){

		//options
		var opts=$.extend({},{
			url:""
		},settings);

		return this.each(function(){
			var _this=this,$this=$(_this);
			_this.addEventListener("drop", function(e){
				e.preventDefault();
				var data=e.dataTransfer;
				var files=data.files;
				$this.trigger("DNDU_drop");
				new DNDU(_this,files,opts);
				return false;
			}, true);
			_this.addEventListener("dragenter", function(e){
				e.stopPropagation();
				e.preventDefault();
				$this.trigger("DNDU_enter");
				return false;
			}, true);
			_this.addEventListener("dragover", function(e){
				e.stopPropagation();
				e.preventDefault();
				$this.trigger("DNDU_over");
				return false;
			}, true);
			_this.addEventListener("dragleave", function(e){
				$this.trigger("DNDU_leave");
				return false;
			}, true);
		});
	};

	function DNDU(el,files,opts){ //constructor
		this.files=files;
		this.opts=opts;
		this.supports=support;
		this.el=el;
		this.url=this.opts.url+"/";
		var temp=[];
		for(var prop in this.supports){
			temp.push(prop+":"+this.supports[prop]);
		}
		this.url+=temp.join("_");

		this.process(this.files);

	};
	DNDU.prototype.process=function(){
		var _this=this,files=_this.files;
		for(var i=0;i<files.length;i++){
		  if(files[i].fileSize>0 || files[i].size>0){
		    var guid=(new Date()).getTime()+"_"+Math.floor(Math.random()*200);
  			files[i].guid=guid;
  			files[i].isResizable=false;
  			files[i].isResized=false;
  			files[i].fileName=files[i].fileName||files[i].name;
  			files[i].fileSize=files[i].fileSize||files[i].size;

  			$(_this.el).trigger("DNDU_processStart",[files[i].guid,files[i].fileName]);
  			_this.upload(files[i]);
		  }
		}
	};

	DNDU.prototype.upload=function(file){
		var _this=this;
		var headers={};
		if(_this.supports.XHRupload){
			headers={
			"Cache-Control":"no-cache",
			"X-Requested-With":"XMLHttpRequest",
			"X-File-Name":file.fileName,
			"X-File-Size":file.fileSize,
			"Content-Type":"multipart/form-data"};
			_this.send(file,headers);
		}
	};
	DNDU.prototype.send=function(file,headers){
		var _this=this;
		var xhr = new XMLHttpRequest();
		var upload = xhr.upload;
		upload.fileObj = file;
		upload.downloadStartTime = new Date().getTime();
		upload.currentStart = upload.downloadStartTime;
		upload.currentProgress = 0;
		upload.startData = 0;

		//trigger start upload
		$(_this.el).trigger("DNDU_uploadStart",[file.guid,file.fileName]);
		// add listeners
		upload.onprogress=function(e){
			//console.log(e);
			if (e.lengthComputable) {
				$(_this.el).trigger("DNDU_uploadProgress",[file.guid,e.loaded,e.total]);
			}
		};

		upload.onload=function(e){
			//console.log(e);
			$(_this.el).trigger("DNDU_uploadEnd",[file.guid]);
			//file=null;
			//delete(file);
		};

		xhr.onload=function(e){
			if ( e.target.readyState == 4 ) {
			  //console.log(this.responseText);
				$(_this.el).trigger("DNDU_processEnd",[file.guid,this.responseText]);
			}
		};

		if(file.isResized){
			xhr.open("POST", _this.url+"isResized=true",true);
		}else{
			xhr.open("POST", _this.url,true);
		}

		for(var prop in headers){
			xhr.setRequestHeader(prop, headers[prop]);
		}

		xhr.send(file);

	};
})(jQuery);
