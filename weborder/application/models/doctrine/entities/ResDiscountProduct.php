<?php



namespace entities;

/**
 * ResDiscountProduct
 *
 * @Table(name="res_discount_product")
 * @Entity
 */
class ResDiscountProduct
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResDiscount
   *
   * @ManyToOne(targetEntity="ResDiscount")
   * @JoinColumns({
   *   @JoinColumn(name="discount", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $discount;
  
  /**
   * @var ResProduct
   *
   * @ManyToOne(targetEntity="ResProduct")
   * @JoinColumns({
   *   @JoinColumn(name="product", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $product;
  
  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set discount
   *
   * @param ResDiscount $discount
   */
  public function setDiscount(\ResDiscount $discount)
  {
  	$this->discount = $discount;
  }
  
  /**
   * Get discount
   *
   * @return ResDiscount
   */
  public function getDiscount()
  {
  	return $this->discount;
  }
  
  /**
   * Set product
   *
   * @param ResProduct $product
   */
  public function setProduct(\ResProduct $product)
  {
  	$this->product = $product;
  }
  
  /**
   * Get product
   *
   * @return ResProduct
   */
  public function getProduct()
  {
  	return $this->product;
  }

  
}