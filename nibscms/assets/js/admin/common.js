(function($){

  $.fn.overlay=function(opts){
    var opts=$.extend({
      width:"50%",
      title:"",
      href:"",
      open:false
    },opts);

    var popupHolder=$("#popupHolder"),
    outer=$("#popupOuter",popupHolder),
    popup=$("#popup",popupHolder),
    title=$("#popupTitle span",popupHolder);

    function open(){
      var vW=window.innerWidth,vH=window.innerHeight;
      popup.empty().css({
        height:"auto",
        width:"auto"
      });
      outer.css({
        height:"auto",
        width:opts.width,
        maxHeight:"70%"
      });
      var linked_id=$(opts.href);
      if(linked_id.length){
        var plh=$("<div class='popup_placeholder'></div>");
        linked_id.after(plh);
        linked_id.data("popup_placeholder",plh);
        popup.append(linked_id);
      }
      popupHolder.show(0);

      title.html(opts.title)
      .next().one("click",close);

      var oH=outer.height(),
      oW=outer.width();

      outer.css({
        top:(vH-(oH+41))/2 + "px",
        left:(vW-(oW+12))/2 + "px"
      });

      popup.css({
        height:(oH+10) +"px",
        width:oW + "px"
      })

      outer.addClass("showing");
    }

    function close(){
      outer.removeClass("showing");
      setTimeout(function(){
        popupHolder.hide(0);
        var orig_el=popup.children().eq(0);
        (orig_el.data("popup_placeholder")).replaceWith(orig_el);
      },300);
      return false;
    }

    $.fn.overlay.close=close;

    if(opts.open){
      open();
    }

    return this.each(function(){
      $(this).bind("click.overlay",open);
    });
  };

})(jQuery);
//Form Reset plugin
$.fn.extend({reset:function(){
	return this.each(function(){
		$(this).is('form') && this.reset();
	});
}});




var tinyMCE={get:function(){}};
var NIBS={};
NIBS.util={
  debounce:function(func, threshold, execAsap) {
    var timeout;
    return function debounced () {
      var obj = this, args = arguments;
      function delayed () {
        if (!execAsap)
          func.apply(obj, args);
          timeout = null;
        };
        if (timeout)
          clearTimeout(timeout);
        else if (execAsap)
          func.apply(obj, args);
        timeout = setTimeout(delayed, threshold || 100);
    };
  },
  ajax:function(data,url,type,dataType,fn){
    type=type||"POST";
  	dataType=dataType||"text";

  	$.ajax({
  		url: base_url+url,
  		type: type,
  		dataType: dataType,
  		data: data,
  		success: function(text) {
  		  var data = eval("(" + text + ")");
  		  if(data.success=="success"){
  				if($.isFunction(data.successFunction)){
  					data.successFunction.apply();
  				}
  			}else{
  				NIBS.util.ajaxFailure();
  			}
  		},
  		error: function() {
  		   NIBS.util.ajaxFailure.apply(this,arguments);
  		},
  		complete:function(){
  			if($.isFunction(fn)){
  				fn.apply();
  			}
  		}
  	});
  },
  ajaxFailure:function(event, XMLHttpRequest, ajaxOptions, thrownError){
    if(event && (event.status=="412" || event.status=="403")){
  		return;
  	}else{
  		alert("Request could not be completed due to an internal error... ");
  	}
  },
  urlSanitize:function(str){
    return $.trim(str).replace(/[&| \/\\*%\)\(\]\[\}\{\"\'=+–><?.`,:;\#]/g, "-")
    .replace(/-$/,"").replace(/^-/,"")
  	.replace(/[-]{2,}/g,"-").toLowerCase();
  }
};

NIBS.notif={
  cache:[],
  timeout:null,
  setup:function(){
    //initial variables
    var obj=this;
    obj.dom=$("#notifier");
    //start the timer
    obj.interval=setInterval(function(){obj.run();},3000);
  },
  notify:function(type,msg){
    var obj=this, cache=obj.cache;
    //if cache is empty and notification is not showing
    //show it immediately
    if(cache.length > 0){
      obj.cache.push({type:type,msg:msg});
    }else{
      if(obj.dom.hasClass("showing") == false){
        clearInterval(obj.interval);
        obj.fire(type,msg);
        obj.interval=setInterval(function(){obj.run();},3000);
      }else{
        obj.cache.push({type:type,msg:msg});
      }
    }
  },
  run:function(){
    var obj=this,cache=obj.cache;
    if(cache.length){
      var current=cache.shift();
      obj.fire(current.type,current.msg);
    }else{
      obj.dom.removeClass("showing");
    }
  },
  fire:function(tp,msg){
    var obj=this, dom=obj.dom,
    classes="error success";
    dom.removeClass("showing");
    setTimeout(function(){
      dom.removeClass(classes);
      dom.addClass(tp).html(msg).addClass("showing");
    },300);
  }
};

NIBS.largeLoader="<div class='largeLoader'></div>";

NIBS.CSSanimate= function(el,cls,dur){
  var dur=dur || 2;
  el.removeClass(cls).addClass(cls);
  setTimeout(function(){
    el.removeClass(cls);
  },dur*1000);
};

//piggy back $.fn.load to show loading graphic
(function(){
  var _f=$.fn.load;
  $.fn.load=function(){
    this.html(NIBS.largeLoader);
    _f.apply(this,arguments);
  }
})(jQuery);

$(function(){
  //start the notification daemon
  NIBS.notif.setup();

  //Preloaders
  $("#preloader").bind("ajaxStart",function(){
		$(this).show(0);
	}).bind("ajaxStop",function(){
		$(this).hide(0);
	});
	//Ajax Errors
	$("body").ajaxError(function(event, XMLHttpRequest, ajaxOptions, thrownError){
	  	if(XMLHttpRequest && XMLHttpRequest.status=="403"){
			alert("You do not have sufficient privileges to perform this action...\nPlease Contact you administrator to upgrade your clearance.");
		}
		if(XMLHttpRequest && XMLHttpRequest.status=="412"){
			alert("Request not completed,\nYour session may have expired, Please reload the page...");
		}
	});
	//Ajaxed Forms
	$("form[rel=ajaxedUpload]").live("submit",function(){
		var _this=this;
		var strName = ("uploader" + (new Date()).getTime());
		$(_this).attr( "target", strName );
		var lD=$(_this).attr("loadingDiv");
		var frame=$( "<iframe style=\"display:none\" name=\"" + strName + "\" id=\"" + strName + "\"  />" );
		$( "body:first" ).append( frame );
		frame.load(function(){
			var __this=$(this);
			setTimeout(function(){
				var objUploadBody = __this[0].contentDocument.body;
				var jBody = $(objUploadBody);
     
     
				try{var objData= eval( "(" + jBody.html() + ")" ) || null;}catch(e){};
       // console.log(objData);
				if(objData){
					if(objData.success=="error"){
						alert(objData.msg);
					}else if(objData.success=="success"){
						$("#"+lD).html("<span style='color:#006b3e;font-weight:bold' class='loader'>Uploaded</span>");
						//reset form
						$(_this).reset();
						if($.isFunction(objData.successFunction)){
							objData.successFunction.apply();
						}
					}
				}else{
					alert("Error in response, File may not have been saved");
					
				}

				setTimeout(
					function(){
						frame.remove();
						$("#"+lD).html("");
					}, 200);
			},100);
		});
		$("#"+lD).html("<img class='loader' border='0' src='"+base_url+"assets/images/admin/small_loader.gif' />");

	});
	$("form[rel=ajaxed]").live("submit",function(){
	  var _this=$(this);
		var EE=tinyMCE.get("EditableArea");
		if(EE){
			EE.save();
			EE.setProgressState(1);
		}
		var sendData=_this.serializeArray();
		sendData.push({name:"ajaxed11185",value:"true"});
		var lD=_this.attr("loadingDiv");
		$("#"+lD).html("<img class='loader' border='0' src='"+base_url+"assets/images/admin/small_loader.gif' />");
    var formType=_this.attr("formType");
		$.ajax({
			url:_this.attr("action"),
			dataType:_this.attr("dataType")||"text",
			type:_this.attr("method"),
			data:sendData,
			success:function(text){
			  var data = eval("(" + text + ")");
				if($.isFunction(data.successFunction)){
					data.successFunction.apply();
				}
				if(data.success=="success"){
					$("#"+lD).html("<span style='color:#006b3e;font-weight:bold' class='loader'>"+(formType=="adder" ? "Added" : "Saved")+"</span>");
					setTimeout(function(){
						$("#"+lD).empty();
					},3000);
				}else{
					$("#"+lD).empty();
				}
				EE && EE.setProgressState(0);
			},
			error:function(){
				if($.isFunction(NIBS.util.ajaxFailure)){
					NIBS.util.ajaxFailure.apply(this,arguments);
				}
				$("#"+lD).html("<span style='color:red;font-weight:bold' class='loader'>Failed to "+(formType=="adder" ? "Add" : "Save")+"!!</span>");
				setTimeout(function(){
					$("#"+lD).empty();
				},3000);
				EE && EE.setProgressState(0);
			}
		});
		return false;
	});

	//Sortables
	$("ul.list.reorderable").sortable({
	  axis:"y",
	  update:function(){
	    NIBS.util.ajax($(this).sortable("serialize"),"admin/"+$(this).attr("rel"));
	  }
	});

	//Delete Buttons
	$("ul.list li .action.delete").live("click",function(){
		$(this).blur();
		if(confirm("Are you sure?")){
			NIBS.util.ajax({id:$(this).attr("elId")},"admin/"+$(this).attr("rel"));
		}
		return false;
	});

    //Delete img
  $(".img .action.delete").live("click",function(){
    $(this).blur();
    if(confirm("Are you sure?")){
      NIBS.util.ajax({id:$(this).attr("elId")},"admin/"+$(this).attr("rel"));
    }
    return false;
  });

	//filter items
  var _filterer=$("#filterer");
  if(_filterer.length){
    (function(f){
      var _inp=f.find("#theFilter"),
      cross=f.find("a"),list=f.parent().parent().find("ul").eq(0),count=$("#searchCount");

      _inp.keyup(NIBS.util.debounce(function(){
        var _this=$(this),
        _val=_this.val(),
        reg=null,total=0;
        if(_val!=""){
          _this.next().show(0);
        }
        if ($.trim(_val)!=""){
          reg=new RegExp(_val,"i");
        }
        if(reg){
          $("li",list).css({display:"none"}).each(function(i,el){
            var $el=$(el);
            var txt=$el.text();
            if((reg.test(txt))){
              $el.css({display:"block"});
              total++;
            }
          });
          count.text(total + " Filtered Result"+(total!=1?"s":"")).css({display:"inline"});
        }else{
          $("li",list).css({display:"block"});
          _this.next().hide(0);
          count.css({display:"none"});
        }
      },250));

      cross.click(function(){
        $(this).hide(0).prev().val("");
        $("li",list).css({display:"block"});
        count.css({display:"none"});
        return false;
      })
    })(_filterer);
  }

	//collapsibles
	$(".headertoolbar.collapsible").live("click",function(){
	  var _this=$(this),
	  h=_this.height(),
	  stored=_this.data("collapsible_parentHeight");

	  if(_this.is(".open")){
	    _this.removeClass("open");
	    _this.data("collapsible_parentHeight",_this.parent().height());
	    _this.parent().css({overflow:"hidden"}).animate({
	      height:h+12 + "px"
	    });
	  }else{
	    _this.addClass("open");
	    _this.parent().animate({
	      height:stored + "px"
	    },function(){
  	    _this.parent().css({height:"auto",overflow:""});
  	  });
	  }

	});

	//innertabs
	$("#innerTabs li:not(.disabled)").live("click",function(){
	  var _this=$(this),idx=_this.index();
	  _this.addClass("active").siblings().removeClass("active");
	  $("#main-content .tab_content").hide(0).eq(idx).show(0);
	})

	//--------STUB CHECKERS
	$("input.stubSenders").live("keyup",NIBS.util.debounce(function(){
			var _this=$(this);
			var stubReceiver=_this.attr("stubReceiver");
			var stubStatus=_this.attr("stubStatus");
			var pageTitle=_this.attr("stubPageTitle");
			var thisPage=_this.attr("pageID");
			if(pageTitle==""){pageTitle=null;}
			var stubUrl=_this.attr("stubUrl");
			populate_stubs(this,"#"+stubReceiver,
				"#"+pageTitle,"#"+stubStatus, stubUrl,thisPage);


	},200));


	//--------END STUB CHECKERS

	//--------SELF STUB CHECKERS
	$('input.stubReceivers').live("keyup",NIBS.util.debounce(function(){
		var _this=$(this);
		var rel=_this.attr("rel");
		var val=_this.val();
		var thisPage=_this.attr("pageID");
		var obj;
		if(typeof rel=="undefined"){
			obj={"stub":val,"pageId":thisPage};
		}else{
			obj={"stub":val,"rel":rel,"pageId":thisPage};
		}
		var stubStatus=_this.attr("stubStatus");
		var stubUrl=_this.attr("stubUrl");
		$.ajax({
			url:base_url+stubUrl,
			dataType:"text",
			type:"post",
			data:obj,
			success:function(text){
			  $('#'+stubStatus).html(text);
			}
		});
	},200));

});

function populate_stubs(from,el,pagetitle,stubchecker,url,pageId){
	/*The rel attribute on the stubReceiver is used where
	 a value exists in the database but, we don't want to flag it up as duplicate.
	because it's finding itself as a duplicate...*/

	var val=$(from).val();
	var badCharacters=["&","|","/","\\","*","%",")","(","]","[","}","{","\"","'","=","+",">","<","?",".","`",","];
	var new_val=$.trim(val).replace(/[&| \/\\*%\)\(\]\[\}\{\"\'=+–><?.`,:;\#]/g, "-").replace(/-$/,"").replace(/^-/,"")
	.replace(/[-]{2,}/g,"-").toLowerCase();
	$(el).val(new_val);

	if($(pagetitle).length){
		$(pagetitle).val(val);
	}
	var rel=$(el).attr("rel");
	if(typeof rel!="undefined"){
		if(url){
		  $.ajax({
    		url:base_url+url,
    		dataType:"text",
    		type:"post",
    		data:{"stub":new_val,"rel":rel,pageId:pageId},
    		success:function(text){
    		  $(stubchecker).html(text);
    		}
    	});
		}
	}else{
		if(url){
		  $.ajax({
    		url:base_url+url,
    		dataType:"text",
    		type:"post",
    		data:{"stub":new_val,pageId:pageId},
    		success:function(text){
    		  $(stubchecker).html(text);
    		}
    	});
		}
	}

}


// ++++++++++ JSON TO STRING FROM JSON 2 at JSON.ORG
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('h C(a,b,c){q i;3=\'\';j=\'\';5(6 c===\'B\'){s(i=0;i<c;i+=1){j+=\' \'}}D 5(6 c===\'w\'){j=c}e=b;5(b&&6 b!==\'h\'&&(6 b!==\'l\'||6 b.8!==\'B\')){10 11 12(\'C\');}9 m(\'\',{\'\':a})};h m(a,b){q i,k,v,8,o=3,7,2=b[a];5(2&&6 2===\'l\'&&6 2.E===\'h\'){2=2.E(a)}5(6 e===\'h\'){2=e.F(b,a,2)}13(6 2){p\'w\':9 x(2);p\'B\':9 14(2)?G(2):\'y\';p\'15\':p\'y\':9 G(2);p\'l\':5(!2){9\'y\'}3+=j;7=[];5(H.17.I.18(2)===\'[l 19]\'){8=2.8;s(i=0;i<8;i+=1){7[i]=m(i,2)||\'y\'}v=7.8===0?\'[]\':3?\'[\\n\'+3+7.z(\',\\n\'+3)+\'\\n\'+o+\']\':\'[\'+7.z(\',\')+\']\';3=o;9 v}5(e&&6 e===\'l\'){8=e.8;s(i=0;i<8;i+=1){k=e[i];5(6 k===\'w\'){v=m(k,2);5(v){7.J(x(k)+(3?\': \':\':\')+v)}}}}D{s(k 1a 2){5(H.1b.F(2,k)){v=m(k,2);5(v){7.J(x(k)+(3?\': \':\':\')+v)}}}}v=7.8===0?\'{}\':3?\'{\\n\'+3+7.z(\',\\n\'+3)+\'\\n\'+o+\'}\':\'{\'+7.z(\',\')+\'}\';3=o;9 v}};h x(b){q d=/[\\1c\\K\\L-\\M\\N\\O\\P\\Q-\\R\\S-\\T\\U-\\V\\W\\X-\\Y]/g,A=/[\\\\\\"\\1d-\\1e\\1f-\\1g\\K\\L-\\M\\N\\O\\P\\Q-\\R\\S-\\T\\U-\\V\\W\\X-\\Y]/g,3,j,Z={\'\\b\':\'\\\\b\',\'\\t\':\'\\\\t\',\'\\n\':\'\\\\n\',\'\\f\':\'\\\\f\',\'\\r\':\'\\\\r\',\'"\':\'\\\\"\',\'\\\\\':\'\\\\\\\\\'},e;A.1h=0;9 A.1i(b)?\'"\'+b.1j(A,h(a){q c=Z[a];9 6 c===\'w\'?c:\'\\\\u\'+(\'1k\'+a.1l(0).I(16)).1m(-4)})+\'"\':\'"\'+b+\'"\'};',62,85,'||value|gap||if|typeof|partial|length|return|||||rep|||function||indent||object|str||mind|case|var||for||||string|quote|null|join|escapable|number|JSON_stringify|else|toJSON|call|String|Object|toString|push|u00ad|u0600|u0604|u070f|u17b4|u17b5|u200c|u200f|u2028|u202f|u2060|u206f|ufeff|ufff0|uffff|meta|throw|new|Error|switch|isFinite|boolean||prototype|apply|Array|in|hasOwnProperty|u0000|x00|x1f|x7f|x9f|lastIndex|test|replace|0000|charCodeAt|slice'.split('|'),0,{}));
