<?php



namespace entities;

/**
 * ResAdditionalCompanyCategory
 *
 * @Table(name="res_additional_company_category")
 * @Entity
 */
class ResAdditionalCompanyCategory
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="integer", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResAdditionalCompany
   *
   * @ManyToOne(targetEntity="ResAdditionalCompany")
   * @JoinColumns({
   *   @JoinColumn(name="additional_company_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $additionalCompany;
  
  /**
   * @var ResCategory
   *
   * @ManyToOne(targetEntity="ResCategory")
   * @JoinColumns({
   *   @JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $category;
  
  /**
   * @var ResProduct
   *
   * @ManyToOne(targetEntity="ResProduct")
   * @JoinColumns({
   *   @JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $product;

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set additionalCompany
   *
   * @param ResAdditionalCompany $additionalCompany
   */
  public function setAdditionalCompany(\ResAdditionalCompany $additionalCompany)
  {
  	$this->additionalCompany = $additionalCompany;
  }
  
  /**
   * Get additionalCompany
   *
   * @return ResAdditionalCompany
   */
  public function getAdditionalCompany()
  {
  	return $this->additionalCompany;
  }
  
  /**
   * Set category
   *
   * @param ResCategory $category
   */
  public function setCategory(\ResCategory $category)
  {
      $this->category = $category;
  }
  
  /**
   * Get category
   *
   * @return ResCategory
   */
  public function getCategory()
  {
      return $this->category;
  }
  
  /**
   * Set product
   *
   * @param ResProduct $product
   */
  public function setProduct(\ResProduct $product)
  {
      $this->product = $product;
  }
  
  /**
   * Get product
   *
   * @return ResProduct
   */
  public function getProduct()
  {
      return $this->product;
  }
}