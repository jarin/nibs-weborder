<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/dashboard.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/jquery.livequery.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/dashboard.js"></script>

      <div id="main-content" class='onepane'>
        <div class="container-fluid" style=''>
          <div class="row-fluid">
            <div class="span11">
              <h3 class="page-title">
                Welcome <?=$username?>
              </h3>
        </div>
        <div class="span2" style="position:absolute;right:1em; margin-top:20px;">
          <button id="widgetAdderButton"  class="button secondary add">Add Widget</button>
          <div id="availableWidgets" class="comp_floatingList from_right" style="right:13.2em;height:auto;position:absolute;">
            <div class="chink rt"></div>
            <div class="headertoolbar">Available Widgets <span class="close" title="Close" onclick="$('#widgetAdderButton').click();return false;" style="right:auto;left:-9px"></span></div>
            <ul>
               <?php
                  if(is_array($widgets)){
                    foreach ($widgets as $widget){
                      if($widget["hidden"]==1){
                        echo '<li id="hwidget_'.$widget["id"].'" class="clearfix widget" rel="'.$widget["id"].'"><span class="name">'.$widget["title"].'</span></li>';
                      }
                    }
                  }
                ?>
            </ul>
          </div>
        </div>
        
        </div>
<hr/ style='margin-top:5px'>
        <!-- <div style="position:absolute;right:1em;"><button id="widgetAdderButton"  class="button secondary add">Add Widget</button></div> -->
                    <div id="widgetsHolder" class="clearfix">
                      <div id="widgetsMain" class="clearfix widgetpane" rel="home/reoderWidgets/left">
                        <?php
                          if(is_array($widgets)){
                            foreach ($widgets as $widget){
                              if($widget["hidden"]!=1 && $widget["column"]!="right"){
                                echo '<div id="widget_'.$widget["id"].'" class="dashbox '.$widget["size"].' '.(($widget["minimised"]==1)?"minimised":"").'" rel="'.$widget["dataurl"].'">';
                                echo '<div class="headertoolbar"><span>'.$widget["title"].' <a class="options" title="Options" href="#"></a><a class="minimise" title="Minimise" href="#"></a><a class="close" title="Close" href="#"></a></span><ul class="options"><li rel="">Full Width</li><li rel="half">Half Width</li><li rel="two_thirds">2/3<sup>rd</sup> Width</li><li rel="third">1/3<sup>rd</sup> Width</li></ul></div>
                                <div class="content">

                                </div>';
                                echo '</div>';
                              }

                            }
                          }
                        ?>
                      </div>
                      <div id="widgetsRight" class="clearfix widgetpane" rel="home/reoderWidgets/right">
                        <?php
                          if(is_array($widgets)){
                            foreach ($widgets as $widget){
                              if($widget["hidden"]!=1 && $widget["column"]=="right"){
                                echo '<div id="widget_'.$widget["id"].'" class="dashbox '.$widget["size"].' '.(($widget["minimised"]==1)?"minimised":"").'" rel="'.$widget["dataurl"].'">';
                                echo '<div class="headertoolbar"><span>'.$widget["title"].' <a class="options" title="Options" href="#"></a><a class="minimise" title="minimise" href="#"></a><a class="close" title="Close" href="#"></a></span><ul class="options"><li rel="">Full Width</li><li rel="half">Half Width</li><li rel="two_thirds">2/3<sup>rd</sup> Width</li><li rel="third">1/3<sup>rd</sup> Width</li></ul></div>
                                <div class="content">

                                </div>';
                                echo '</div>';
                              }

                            }
                          }
                        ?>
                      </div>
                    </div>
      
      </div>
      

</div>





