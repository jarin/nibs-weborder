$(function(){
  
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addPageBtn").overlay({width:"30%",inline:true,title:"Add Testimonial",href:"#addPageForm"});

  //checkboxes
  $("#sidebar ul.list li input[type=checkbox]").live("click",function(){
    activate_testimonial($(this));
    return false;
  });

  
  // See More details on click
  $("#sidebar ul.list li").live("click",function(){
    var _li=$(this);
    _li.siblings().removeClass("active");
    _li.addClass("active");
    loadTestimonialDetails(_li.attr("id"));
    return false;
  });


});



function addTestimonial(name,id){
  NIBS.notif.notify("success","testimonial added");
  var html="<li id='testimonial_"+id+"' class='deleted'>"+
  "<a href='#'>"+
  "<span class='name icon-box'><i class=''><img src='"+base_url+"assets/images/admin/drag_handle_active.png'/></i></span>"+
	"<span class='name nameBtn' title='"+name+"'>"+name+"</span>"+
  "<span class='list_icon_btn'>"+
    "<input title='Delete Item' type='image' class='action delete' rel='testimonials/deleteTestimonial' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
    "<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"+
    "</span>"+
    "</a>"+  
  "</li>";

  $("#sidebar ul.list").prepend(html);
  $("#add_page_form").reset();
  setTimeout(function(){
    $("#testimonial_"+id).removeClass("deleted");
  },100);
}

function loadTestimonialDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
  var id=e[2];
  $("#dContent").load(base_url+"admin/testimonials/viewTestimonial/"+id);
  
  loadPageInfo(id);
  
  $("#thirdBar").show(0);
  $("#main-content").addClass("threepane");
}

function loadPageInfo(id){
  $("#infoPanel").load(base_url+"admin/testimonials/viewTestimonialInfo/"+id);
}

function deleteTestimonial(testimonial_id){
  var el=$("#testimonial_"+testimonial_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
    $("#thirdBar").hide(0);
    $("#main-content").removeClass("threepane");
  }
  el.addClass("deleted");
  setTimeout(function(){
    el.remove()
  },500);
  NIBS.notif.notify("success","Testimonial deleted successfully");
}

function activate_testimonial(el){
  if(el){
    var e=el.attr("id").match(/(.+)[-=_](.+)/);
    var id=e[2];
  }
  NIBS.util.ajax({id:id},"admin/testimonials/activateDeactivateTestimonial");
}

function do_activate_testimonial(id,val){
  $("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
  $("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}