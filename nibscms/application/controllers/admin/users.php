<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="users";
		$this->load->model("activity_m","activity");
		$this->load->model("users_m","users");
		$this->load->model("settings_m","settings");
		//Settings
		$this->SETTINGS=array();
		_loadSettings($this);
	}

	function index(){
		if(_checkAuthentication($this)){
			$Usershtml="";
			$Users=$this->users->getAll("id");
			if(is_array($Users)){
				foreach($Users as $user){
				  if($user["userlevel"]=="admin"){
						$img="<img src='".base_url()."assets/images/admin/icon_admin.png' border='0' />";
					}else{
						$img="<img src='".base_url()."assets/images/admin/icon_user.png' border='0' />";
					}
					$Usershtml.="<li id='user_$user[id]'><a href='#'><span class='name icon-box'><i class=''>$img</i></span><span class='nameBtn'> $user[username]</span>
					<span class='list_icon_btn'><input title='Delete User' type='image' class='action delete' rel='users/deleteUser' elId='$user[id]' src='".base_url()."assets/images/admin/icon_trash.png' border='0' /></span>
					</a></li>";
				}
			}
			$temp="";
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
			$this->load->view("admin/users_template",array(
				"pagename"=>"Users Manager",
				"list1"=>$Usershtml
			));
			$this->load->view("admin/admin_foot");
		}
	}

	function loadUserDetails($user_id=""){
		if(_checkAuthentication($this)){
			if(trim($user_id)==""){
				echo '<span style="color:red">Error: </span>User ID Not defined... Please reload page';
					exit;
			}
			$User=$this->users->get($user_id);
			if(!isset($User["id"]) || !$User["id"]>0){
				echo '<span style="color:red">Error: Invalid user</span>';
					exit;
			}
			$html="
			<div class=''>
				<h3 class='page-title'>User Details</h3><br />
				<div id='form_wizard_1' class='widget box'>
		            <div class='widget-title'>
		                <h4><i class='icon-reorder'></i>Users Form</h4>
		            </div>
		    
		        <div class='widget-body form'>
				<div style='margin:0 auto;width:60%;text-align:center'><form method='post' rel='ajaxedUpload' action='".base_url()."admin/users/saveUser' loadingDiv='loader$User[id]' autocomplete='off' enctype='multipart/form-data'>
					<table cellpadding='4' cellspacing='0'>
						<tr>
							<th>Username</th>
							<td><input type='text' name='username' value=\"$User[username]\" /></td>
						</tr>
						<tr>
							<th>User Level</th>
							<td><select name='userlevel'>
							<option value='admin' ".(($User["userlevel"]=="admin")?("selected"):("")).">Administrator</option>
							<option value='managed' ".(($User["userlevel"]=="managed")?("selected"):("")).">Managed User</option>
							</select></td>
						</tr>
						<tr>
							<th>Password</th>
							<td><input type='password' name='password' value=\"\" /></td>
						</tr>
						<tr>
							<th>Retype Password</th>
							<td><input type='password' name='re_password' value=\"\" /></td>
						</tr>
						<tr><td></td></tr>
						<tr><td></td></tr>

						<tr>
							<th>Uploaded image</th>
							<td align='left' colspan='2' class='img' id='logoIcon_$User[id]'>
  							  	<input type='image' title='Delete Image' class='action delete delImg' rel='users/deleteImage'  elId='logoIcon_$User[id]' src='".base_url()."assets/images/admin/icon_trash.png' border='0' style='margin-left:25px' />
  							  	<img  src=\"".base_url().'assets/users_img/'.$User['user_img']."\" style='width:29px;height:29px'  />
  							 </td>
						</tr>
						<tr><td></td></tr>
							<tr>
  							  <th align='left' valign='top'><label>Replace Image</label></th>
  							  <td align='left' colspan='2'><input type='file' name='user_img' id='uploadField' style='width:100%' /></td>
  						  	</tr>
  						  	<tr><td></td></tr>
  						  	<tr><td></td></tr>
						<tr><td colspan='4'><hr></td></tr>
						<tr>
							<td align='right' colspan='4'>
							<button type='submit' class='button save'>Save Changes</button>
							<div id='loader$User[id]' class='q_feedback'></div>
							<input type='hidden' name='user_id' value='$User[id]' />
							</td>
						</tr>
					</table>
				</form></div>
			</div>
			</div></div>
			";

			echo $html;
		}
	}


	function deleteImage(){
    if(_checkAuthentication($this)){
      $img_id=$this->input->post('id');
			$id=explode("_", $img_id);
      if(trim($id[1])!=""){
				$img="";
				if($id[0]=='logoIcon'){
					$img.="user_img";
				}
        $c=$this->users->save($id[1],array(
					$img=>""
          ));
        if($c){
          activitylog($this,"Page details modified","m");

           echo '{success:"success",successFunction:function(){
             $("#pages_'.$id[1].'").click().find("span");
						 replaceImage("'.$img_id.'");
           }}';
         }else{
           echo '{success:"error",msg:"Could not save to database..."}';
         }
       }else{
         echo '{success:"error",msg:"Please reload page..."}';
       }
		}
	}



	function saveUser(){
		if(_checkAuthentication($this)){
			$username=$this->input->post("username");
			$password=$this->input->post("password");
			$re_password=$this->input->post("re_password");
			$userlevel=$this->input->post("userlevel");
			$user_id=$this->input->post("user_id");
			$admin_icon='<img src=\''.base_url()."assets/images/admin/icon_admin.png".'\'/>';
			$user_icon='<img src=\''.base_url()."assets/images/admin/icon_user.png".'\'/>';
			if(trim($password)!="" && $password!=$re_password){
				echo '{"success":"error","successFunction":function(){
					alert("Passwords do not match...");
				}}';
				exit;
			}
			$ex=$this->users->usernameExists($username);
			$continue=false;
			if($ex==$user_id || $ex===false){
				//same user
				$continue=true;
			}else{
				echo '{"success":"error","successFunction":function(){
					alert("This username already exists...\nPlease try another one.");
				}}';
				exit;
			}

			$data=array(
				"username"=>$username,
				"userlevel"=>$userlevel
			);

			$data1=array(
				"username"=>$username,
				"password"=>md5($password),
				"userlevel"=>$userlevel
			);


			$path=$this->users->_repo;
		    $this->load->library("upload",array(
		      "upload_path"=>$path,
		      "allowed_types"=>"gif|jpeg|jpg|png"
		    ));

			$files=array();
			foreach($_FILES as $key=>$value){
				if($value['size']!=0){
					$img_file=$value['name'];

					$this->upload->file_ext = $this->upload->get_extension($img_file);
		      		$file=$this->upload->set_filename($path,$img_file);

					$files[$key]=$file;

					move_uploaded_file($value['tmp_name'], "$path/$file");

					$data[$key]=$file;
					$data1[$key]=$file;
				}
			}

			if($continue && !empty($password)){
				if($this->users->save($user_id,$data1)){
					foreach($files as $key=>$value){
						if($key=='user_img'){
							chmod($path.$value, 0644);
							@$this->_resizePicture($value,29,29);
						}
					}

					  activitylog($this,"User <i>".$username."</i> details modified","m");

						echo '{"success":"success","successFunction":function(){
							$("#user_'.$user_id.'").click().find("span.name i").html(\''.(($userlevel=="admin")?($admin_icon):($user_icon)).'\');
							$("#user_'.$user_id.' span.nameBtn").html("'.$username.'");
							NIBS.notif.notify("success","User details saved");
						}}';
						exit;
				}else{
					echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update user information due to a database error...")
					}}';
					exit;
				}
			}elseif($continue && empty($password)){
				if($this->users->save($user_id, $data)){
					foreach($files as $key=>$value){
						if($key=='user_img'){
							chmod($path.$value, 0644);
							@$this->_resizePicture($value,29,29);
						}
					}

					  activitylog($this,"User <i>".$username."</i> details modified","m");

						echo '{"success":"success","successFunction":function(){
							$("#user_'.$user_id.'").click().find("span.name i").html(\''.(($userlevel=="admin")?($admin_icon):($user_icon)).'\');
							$("#user_'.$user_id.' span.nameBtn").html("'.$username.'");
							NIBS.notif.notify("success","User details saved");
						}}';
						exit;
				}else{
					echo '{"success":"error","successFunction":function(){
						alert("ERROR : Could not update user information due to a database error...")
					}}';
					exit;
				}
			}
		}
	}

	function addUser(){
		if(_checkAuthentication($this)){
			$username=$this->input->post("username");
			$userlevel=$this->input->post("userlevel");
			$password=$this->input->post("password");
			$re_password=$this->input->post("re_password");
			if(empty($password) || trim($username)==""){
				echo '{"success":"error","successFunction":function(){
					alert("Empty usernames/passwords are not allowed...");
				}}';
				exit;
			}
			if($password!=$re_password){
				echo '{"success":"error","successFunction":function(){
					alert("Passwords do not match...");
					$("#add_re_password_input").focus();
				}}';
				exit;
			}
			if($this->users->usernameExists($username)){
				echo '{"success":"error","successFunction":function(){
					alert("Username Exists,\nPlease try a different one...");
					$("#add_username_input").focus();
				}}';
				exit;
			}
			$ins=$this->users->create(array("username"=>$username,"userlevel"=>$userlevel,"password"=>md5($password)));
			if($ins){

			  activitylog($this,"User <i>".$username."</i> added","c");

				echo '{"success":"success",successFunction:function(){
					addUser("'.$username.'","'.$ins.'","'.$userlevel.'");
				}}';
				exit;

			}else{
				echo '{"success":"error",successFunction:function(){
					alert("Could not add User due to a database error");
					}}';
					exit;
			}
		}
	}

	function deleteUser(){
		if(_checkAuthentication($this)){
			$user_id=$this->input->post('id');
			if(trim($user_id)==""){
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove user due to a database error,\\nPlease reload page");
					}}';
					exit;
			}
			$U=$this->users->get($user_id);
			if($this->users->delete($user_id)){

			  activitylog($this,"User <i>".$U["username"]."</i> deleted","d");

				echo '{"success":"success",successFunction:function(){
					deleteUser("'.$user_id.'");
					}}';
					exit;
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove user due to a database error");
					}}';
					exit;
			}
		}
	}

	function loadUserPermissions($user_id=""){
		//By Default, users are allowed every where
		//have to manually uncheck items you would want them to be blocked on
		if(_checkAuthentication($this)){
			if(trim($user_id=="")){
				echo '<span style="color:red">Error: No user defined</span>';
					exit;
			}

			$User=$this->users->get($user_id);
			if(!isset($User["id"]) || !$User["id"]>0){
				echo '<span style="color:red">Error: Invalid user</span>';
					exit;
			}
			$html="<form action='".base_url()."admin/users/saveUserPermissions' method='post' rel='ajaxed' loadingDiv='permsLoader$User[id]'>";
			if($User["userlevel"]!="admin"){
					$allPerms=$this->_permittableActions();
					$userPerms=unserialize($User["pMatrix"]);
					if(is_array($allPerms)){
						ksort($allPerms);
						foreach($allPerms as $section=>$subsections){
							$html.="<div class='permissionSection clearfix'><span class='add_shadow section headertoolbar collapsible open'>$section <input class='main_cb' type='checkbox' onclick='checkUncheck_all_perms(event,this);' /></span>";
							if(is_array($subsections)){
								foreach($subsections as $subsection=>$name){
									if(isset($userPerms[$section][$subsection])){
										$html.="<label class='subsection'><input type='checkbox' name='".$section."_".$subsection."' /> $name</label>";
									}else{
										$html.="<label class='subsection'><input type='checkbox' name='".$section."_".$subsection."' checked /> $name</label>";
									}

								}
							}
							$html.="</div>";
						}
					}
					echo $html."<div class='clearit'></div><hr /><input type='hidden' name='user_id' value='".$User["id"]."' />

					<div style='text-align:center'><button class='button save' type='submit'>Save Permissions</button><div class='q_feedback' id='permsLoader$User[id]'></div></div>
          <hr />
					<div class='clearit'></div>
					</form>";
			}else{
				echo "<p class='padded' style='border:1px solid red;background:#ffdfdf'><span style='color:red'>Error:</span> This is an admin user. To configure permissions for this user, demote them to \"Managed\" level.</p>
				<div style='text-align:center;margin-top:10px'><button class='button important secondary' onclick='loadUserPermissions(\"user_".$User["id"]."\");return false;'><img src='".base_url()."assets/images/admin/icon_wrench.png' /> Manage Permissions</button></div>";
			}



		}
	}

	function saveUserPermissions(){
		if(_checkAuthentication($this)){
			$user_id=$this->input->post("user_id");
			$User=$this->users->get($user_id);
			if(!isset($User["id"]) || !$User["id"]>0){
				echo '{"success":"error",successFunction:function(){
					alert("Error: User not defined");
					}}';
					exit;
			}
			if($User["userlevel"]!="admin"){
				//get available permissions list
				$allPerms=$this->_permittableActions();
				//parse posted permissions
				$Posted=$_POST;
				$Posted_Arr=array();
				if(is_array($Posted)){
					foreach($Posted as $name=>$value){
						if($name!="user_id" && $name!="ajaxed11185"){
							list($class,$function)=explode("_",$name);
							$Posted_Arr[$class][$function]="";
						}

					}
				}

				//compare permissions and flag up that are not included
				$flagged_array=array();
				if(is_array($allPerms)){
					foreach($allPerms as $section=>$subsections){
						if(is_array($subsections)){
							foreach($subsections as $subsection=>$name){
								if(!isset($Posted_Arr[$section][$subsection])){
									//this permission is unchecked, ao add to flagged array
									$flagged_array[$section][$subsection]="";
								}
							}
						}
					}
				}

				$to_db=serialize($flagged_array);
				$success=$this->users->save($User["id"],array("pMatrix"=>$to_db));
				if($success){

				  activitylog($this,"User <i>".$User["username"]."</i> permissions modified","m");

					echo '{"success":"success",successFunction:function(){
						  NIBS.notif.notify("success","User permissions saved successfully");
						}}';
						exit;
				}else{
					echo '{"success":"error",successFunction:function(){
						alert("Could not save permissions due to a database error");
						}}';
						exit;
				}
			}else{
				echo '{"success":"error",successFunction:function(){
					alert("This is an admin user.\\nTo configure permissions for this user, demote them to \"Managed\" level.");
					}}';
					exit;
			}

		}
	}

	function _permittableActions(){
		//list application/controllers/admin
		$actionsArray=array();
		$path=APPPATH."controllers/admin/";
		$dh=opendir($path);
		while(false != ($file=readdir($dh))){
			//include all php files not beginning with a "."
			if(!is_dir($path.$file) && substr($file,0,1)!="."  && substr($file,-4,4)==".php"  && $file!="users.php" && $file!="authentication.php" && $file!="notifydaemon.php"){
				include($path.$file);
				//uppercase the filename to get class name
				$className=ucfirst(array_shift(explode(".",$file)));
				$readableClassName=ucwords(str_replace(array("_","-")," ",$className));
				$actionsArray[$readableClassName]=array();
				//foreach class name, find all methods
				$meths=get_class_methods($className);
				if(is_array($meths)){
					foreach ($meths as $method) {
						if($method!="Controller" && $method!="CI_Base" && $method!="get_instance" && $method!=$className && $method!="Controller" && substr($method,0,1)!="_" && substr($method,0,1)!="X"){
							if($method=="index"){
								$method2="showList";
							}else{
								$method2=$method;
							}
							$actionsArray[$readableClassName][$method]=ucfirst(_uncamelize($method2," "));
						}
					}
				}
			}
		}
		// now do this file

		$actionsArray["Users"]=array();
		//for this class name, find all methods
		$meths=get_class_methods($this);
		if(is_array($meths)){
			foreach ($meths as $method) {
				if($method!="Controller" && $method!="CI_Base" && $method!="get_instance" && $method!="Users" && $method!="Controller" && substr($method,0,1)!="_"){
					if($method=="index"){
						$method2="showList";
					}else{
						$method2=$method;
					}
					$actionsArray["Users"][$method]=ucfirst(_uncamelize($method2," "));
				}
			}
		}

		closedir($dh);
		return $actionsArray;

	}


	function _resizePicture($filename="",$max_width=513,$max_height=219){
    $dir=$this->users->_repo;
    if(file_exists($dir.$filename)){
      $ext=_file_extension($filename);
      list($width_orig, $height_orig)=getimagesize($dir.$filename);
      if(($width_orig==$max_width) &&  ($height_orig==$max_height)){
        return;
      }
      $sc=min(($max_width / $width_orig), ($max_height / $height_orig));
      if($sc < 1){
        $width=round($width_orig*$sc);
        $height=round($height_orig*$sc);
      }else{
        $width=$width_orig;
        $height=$height_orig;
      }


      $image_p = imagecreatetruecolor($width, $height);
      if($ext["extension"]==".gif"){
        $image = imagecreatefromgif($dir.$filename);
      }elseif($ext["extension"]==".png"){
        $image = imagecreatefrompng($dir.$filename);
      }else{
        $image = imagecreatefromjpeg($dir.$filename);
      }
      imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

      $image_p2 = imagecreatetruecolor($max_width, $max_height);
      $base_color = imagecolorallocate($image_p2, 236, 236, 236);
      imagefill($image_p2, 0, 0, $base_color);
      imagecopy($image_p2, $image_p, (($max_width-$width)/2), (($max_height-$height)/2), 0, 0, $width, $height);

      $newFilename=$ext["filename"].$ext["extension"];
      if($ext["extension"]==".gif"){
        imagegif($image_p2, $dir.$newFilename, 100);
      }else{
        imagejpeg($image_p2, $dir.$newFilename);
      }

      //Free up resources
      ImageDestroy($image_p);
      ImageDestroy($image_p2);

      return;

    }
  }


}


?>
