<?php



namespace entities;

/**
 * ResSettings
 *
 * @Table(name="res_settings")
 * @Entity
 */
class ResSettings
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=true)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var string $settingsName
   *
   * @Column(name="settings_name", type="string", length=150, nullable=false)
   */
  private $settingsName;
  
  /**
   * @var string $value
   *
   * @Column(name="value", type="string", length=150, nullable=false)
   */
  private $value;
  
  /**
   * @var boolean $isFlat
   *
   * @Column(name="is_flat", type="boolean", nullable=false)
   */
  private $isFlat=FALSE;

  /**
   * @var bigint $restaurantId
   *
   * @Column(name="restaurant_id", type="bigint", nullable=true)
   */
  private $restaurantId;

  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set settingsName
   *
   * @param string $settingsName
   */
  public function setSettingsName($settingsName)
  {
    $this->settingsName = $settingsName;
  }

  /**
   * Get settingsName
   *
   * @return string 
   */
  public function getSettingsName()
  {
    return $this->settingsName;
  }

  /**
   * Set value
   *
   * @param string $value
   */
  public function setValue($value)
  {
    $this->value = $value;
  }

  /**
   * Get value
   *
   * @return string 
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set isFlat
   *
   * @param boolean $isFlat
   */
  public function setIsFlat($isFlat)
  {
    $this->isFlat = $isFlat;
  }

  /**
   * Get isFlat
   *
   * @return boolean 
   */
  public function getIsFlat()
  {
    return $this->isFlat;
  }
  
  /**
   * Set restaurantId
   *
   * @param bigint $restaurantId
   */
  public function setRestaurantId($restaurantId)
  {
  	$this->restaurantId = $restaurantId;
  }
  
  /**
   * Get restaurantId
   *
   * @return bigint
   */
  public function getRestaurantId()
  {
  	return $this->restaurantId;
  }
}