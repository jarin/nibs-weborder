<?php



namespace entities;

/**
 * ResUserType
 *
 * @Table(name="res_user_type")
 * @Entity
 */
class ResUserType
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $userType
   *
   * @Column(name="user_type", type="string", length=150, nullable=false)
   */
  private $userType;
  
  /**
   * @var string $topColor
   *
   * @Column(name="top_color", type="string", length=9, nullable=false)
   */
  private $topColor;
  
  /**
   * @var string $bottomColor
   *
   * @Column(name="bottom_color", type="string", length=9, nullable=false)
   */
  private $bottomColor;


  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set userType
   *
   * @param string $userType
   */
  public function setUserType($userType)
  {
    $this->userType = $userType;
  }

  /**
   * Get userType
   *
   * @return string 
   */
  public function getUserType()
  {
    return $this->userType;
  }
  
  /**
   * Set topColor
   *
   * @param string $topColor
   */
  public function setTopColor($topColor)
  {
  	$this->topColor = $topColor;
  }
  
  /**
   * Get topColor
   *
   * @return string
   */
  public function getTopColor()
  {
  	return $this->topColor;
  }
  
  /**
   * Set bottomColor
   *
   * @param string $bottomColor
   */
  public function setBottomColor($bottomColor)
  {
  	$this->bottomColor = $bottomColor;
  }
  
  /**
   * Get bottomColor
   *
   * @return string
   */
  public function getBottomColor()
  {
  	return $this->bottomColor;
  }
}