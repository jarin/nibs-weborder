<?php
  @set_time_limit(0);
  ob_implicit_flush(true);
  error_reporting(E_ALL);
  session_start();
  ob_start();

  $jsPath="assets/js/admin/";

  require_once("vendor/uglifyphp/uglify.php");

  outPut("<pre>");

  js();

  outPut("</pre>");

  function outPut($msg=""){
    echo $msg."\n";
    ob_flush();
    flush();
  }

  function js(){
    global $jsPath;
    $path=$jsPath."source/";
    outPut("Processing Javascript files in $path");
    $dh=opendir($path);
    while(false !== ($file=readdir($dh))){
      if(!is_dir($path.$file) && substr($file,0,1)!="."){
        outPut("\tProcessing $file");
        try{
          file_put_contents($path.$file,uglify($path.$file));
          OutPut("\t\tSuccessfully processed $file");
        }catch(exception $ex){
          OutPut("\t\tError while processing $file");
          outPut("\t\t\tuglifier fatal error:<br />".$ex->getMessage());
        }
      }
    }
    closedir($dh);
  }


  ob_end_flush();
?>