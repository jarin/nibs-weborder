<?php
$baseUrl=base_url();
?>
<link href="<?=$baseUrl?>assets/css/admin/daterangepicker-bs2.css" rel="stylesheet" media="screen">
<script src="<?=$baseUrl?>assets/js/moment.min.js"></script>
<script src="<?=$baseUrl?>assets/js/daterangepicker.js"></script>
<div id='main-content' class='onepane'>
        <div class='container-fluid' style=''>
          <div class='row-fluid'>
          	<h3 class='page-title'><?=$pagename?></h3><br />
            <div class='span6' style="margin-left: 0px;">	
			<div class=''>
				
				<div id='form_wizard_1' class='widget box'>
		            
		        <div class='widget-body form'>
				<div style='margin:0 auto;width:60%;text-align:center'>
				<form method='post'  action='<?=base_url()?>admin/order/report/<?=$type?>' loadingDiv='loader' autocomplete='off' enctype='multipart/form-data'>
					<table cellpadding='4' cellspacing='0' align='center'>
						<tr>
							<div class="control-group">
								<label class="control-label" for="daterange" style="display: inline-block;width: 100px;line-height: 30px;">Date Range</label>
	        			        <div class="controls" style="display: inline-block;height: 30px;">
									<div class="input-prepend">
										<span class="add-on"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
										<input type="text" style="width: 200px;" name="dates" id="daterange" class="daterange" value="<?=$dates?>" />
									</div>
								</div>
							</div>
						</tr>
						
						
						
						</table>
				</form></div>
			</div>
			</div>
			
			</div>
		</div>
			 <div class='span6'>	
				<div class=''>
					<div id='form_wizard_1' class='widget box'>
			            
				        <div class='widget-body form'>
				        	<div style='margin:0 auto;width:60%;text-align:center'>
								<?=$details?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var body=$("body");

	$(document).ready(function(){

	body.find('#daterange').daterangepicker({
			ranges: {
		  	'Today': [moment(), moment()],
		    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
		    'Last 7 Days': [moment().subtract('days', 6), moment()],
		    'Last 30 Days': [moment().subtract('days', 29), moment()],
		    'This Month': [moment().startOf('month'), moment().endOf('month')],
		    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
			},
		  startDate: moment().subtract('days', 29),
		  endDate: moment()
		},
		function(start, end) {
			body.find('#daterange').val(start.format('YYYY.MM.DD') + ' - ' + end.format('YYYY.MM.DD'));
			body.find("form").submit();
	});
	});
</script>