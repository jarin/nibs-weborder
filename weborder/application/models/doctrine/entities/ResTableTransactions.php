<?php



namespace entities;

/**
 * ResTableTransactions
 *
 * @Table(name="res_table_transactions")
 * @Entity
 */
class ResTableTransactions
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResTable
   *
   * @ManyToOne(targetEntity="ResTable")
   * @JoinColumns({
   *   @JoinColumn(name="table_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $table;
  
  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $customer;

  /**
   * @var smallint $noOfGuests
   *
   * @Column(name="no_of_guests", type="smallint", nullable=false)
   */
  private $noOfGuests;

  /**
   * @var datetime $startTime
   *
   * @Column(name="start_time", type="datetime", nullable=false)
   */
  private $startTime;
  
  /**
   * @var datetime $endTime
   *
   * @Column(name="end_time", type="datetime", nullable=true)
   */
  private $endTime;
  
  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $user;


  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
    $this->customer = $customer;
  }

  /**
   * Get customer
   *
   * @return ResCustomer 
   */
  public function getCustomer()
  {
    return $this->customer;
  }

  /**
   * Set table
   *
   * @param ResTable $table
   */
  public function setTable(\ResTable $table)
  {
    $this->table = $table;
  }

  /**
   * Get table
   *
   * @return ResTable
   */
  public function getTable()
  {
    return $this->table;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
  
  /**
   * Set noOfGuests
   *
   * @param smallint $noOfGuests
   */
  public function setNoOfGuests($noOfGuests)
  {
  	$this->noOfGuests = $noOfGuests;
  }
  
  /**
   * Get noOfGuests
   *
   * @return smallint
   */
  public function getNoOfGuests()
  {
  	return $this->noOfGuests;
  }
  
  /**
   * Set startTime
   *
   * @param datetime $startTime
   */
  public function setStartTime($startTime)
  {
  	$this->startTime = $startTime;
  }
  
  /**
   * Get startTime
   *
   * @return datetime
   */
  public function getStartTime()
  {
  	return $this->startTime;
  }
  
  /**
   * Set endTime
   *
   * @param datetime $endTime
   */
  public function setEndTime($endTime)
  {
  	$this->endTime = $endTime;
  }
  
  /**
   * Get endTime
   *
   * @return datetime
   */
  public function getEndTime()
  {
  	return $this->endTime;
  }
  
  /**
   * Set user
   *
   * @param ResUser $user
   */
  public function setUser(\ResUser $user)
  {
  	$this->user = $user;
  }
  
  /**
   * Get user
   *
   * @return ResUser
   */
  public function getUser()
  {
  	return $this->user;
  }
}