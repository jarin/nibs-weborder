<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shop_orders extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->_userid=$this->session->userdata("uid");
    $this->_username=$this->session->userdata("uname");
    $this->_utype=$this->session->userdata("utype");
    $this->load->library("json");
    $this->load->helper("generic");

    //controller Specific
    $this->tab="shop_catalog";
    $this->load->model("settings_m","settings");
    $this->load->model("shophook_m","shop");
    //Settings
    $this->SETTINGS=array();
    _loadSettings($this);
  }

  function index($msg=""){
    if(_checkAuthentication($this)){
      $html="";
      $os=$this->shop->getOrderStatuses();
      if(is_array($os)){
        foreach ($os as $s) {
          if($s["status_name"]=="STRING_CANCELED_ORDER_STATUS"){
            $s["status_name"]="Cancelled";
          }
          $html.="<li><span><label><input type='checkbox' value='$s[statusID]' /> <b class='state state-$s[statusID]'></b> $s[status_name]</label></span></li>";
        }
      }

      $html="<li><span><b>Show orders of status:</b></span>
        <ul>$html</ul>
      </li>
      <li><span style='text-align:right;'><button class='button' style='font-size:12px;line-height:20px' id='findByStatus'>Go</button></span></li>
      <li><span><hr /></span></li>
      <li><span><b>Find order by order ID:</b></span></li>
      <li><span><input type='text' placeholder='Order ID' /></span></li>
      <li><span style='text-align:right;'><button class='button' style='font-size:12px;line-height:20px' id='findByID'>Go</button></span></li>";

      $this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
      $this->load->view("admin/shop/orders_template",array(
        "pagename"=>"Order Details",
        "list1"=>$html
      ));
      $this->load->view("admin/admin_foot");
    }
  }

  function filterOrders(){
    if(_checkAuthentication($this)){
      $ids=$this->input->post("ids");
      $ids=explode(",",$ids);
      $res=$this->shop->getOrders($ids);
      $this->_listOrders($res);
    }
  }

  function searchOrders(){
    if(_checkAuthentication($this)){
      $q=$this->input->post("q");
      if(trim($q)!=""){
        $res=$this->shop->searchOrders($q);
        $this->_listOrders($res);
      }
    }
  }

  function viewOrder($id=0){
    if(_checkAuthentication($this)){
      $order=$this->shop->getOrder($id);
      if(is_array($order) && isset($order["orderID"])){
        if($order["status_name"]=="STRING_CANCELED_ORDER_STATUS"){
          $order["status_name"]="Cancelled";
        }
        echo "
        <div class='tab_content padded' style='display:block'>
          <h1>Order Details</h1>
            <div class='settingsTable'>
            <div class='fieldset'>
              <table width='100%' cellpadding='4' cellspacing='0'>
                <tr><th align='left'>Order ID:</th><td>$order[orderID]</td></tr>
                <tr><th align='left'>Order time:</th><td>".date("d/m/Y H:i:s",$order["ordered_on"])."</td></tr>
                <tr><th align='left'>Customer:</th><td>$order[customer_firstname] $order[customer_lastname]</td></tr>
                <tr><th align='left'>Email:</th><td>$order[customer_email]</td></tr>
                <tr><th align='left'>Customer IP:</th><td>$order[customer_ip]</td></tr>
                <tr><th align='left'>Current Status:</th><td class='current_status'>$order[status_name]</td></tr>
                <tr><th align='left'>Customer Order Ref:</th><td>$order[internal_order_ref]</td></tr>
              </table>
            </div>
            <div class='clearfix'>
              <div style='width:49%;float:left'>
                <h2>Payment</h2>
                <div class='fieldset'>
                  <table width='100%' cellpadding='4' cellspacing='0'>
                    <tr><th align='left'>Payment:</th><td>$order[payment_type]</td></tr>
                    <tr><th align='left'>Payee:</th><td>$order[billing_firstname] $order[billing_lastname]</td></tr>
                    <tr><th align='left' valign='top'>Billing address:</th><td>
                      ".(($order["billing_address"]!="")?(nl2br($order["billing_address"])."<br />"):(""))."
                      ".(($order["billing_city"])?($order["billing_city"]."<br />"):(""))."
                      ".(($order["billing_state"])?($order["billing_state"]."<br />"):(""))."
                      ".(($order["billing_zip"])?($order["billing_zip"]."<br />"):(""))."
                      $order[billing_country]
                    </td></tr>
                    <tr><th align='left'>SagePay Order Ref:</th><td>$order[protx_order_ref]</td></tr>
                    <tr><th align='left'>SagePay Verification Id:</th><td>$order[protx_ver_id]</td></tr>
                  </table>
                </div>
              </div>
              <div style='width:49%;float:right'>
                <h2>Shipping</h2>
                <div class='fieldset'>
                  <table width='100%' cellpadding='4' cellspacing='0'>
                    <tr><th align='left'>Shipping:</th><td>$order[shipping_type]</td></tr>
                    <tr><th align='left'>Recipient:</th><td>$order[shipping_firstname] $order[shipping_lastname]</td></tr>
                    <tr><th align='left' valign='top'>Shipping address:</th><td>
                      ".(($order["shipping_address"]!="")?(nl2br($order["shipping_address"])."<br />"):(""))."
                      ".(($order["shipping_city"])?($order["shipping_city"]."<br />"):(""))."
                      ".(($order["shipping_state"])?($order["shipping_state"]."<br />"):(""))."
                      ".(($order["shipping_zip"])?($order["shipping_zip"]."<br />"):(""))."
                      $order[shipping_country]
                    </td></tr>
                  </table>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class='tab_content padded settingsTable' style=''>
            <h1>Products Ordered</h1>
            <div class='fieldset'>
            <table width='100%' cellpadding='4' cellspacing='0' class='ordered_items'>
              <tr><th style='text-align:center'>Name</th><th style='text-align:center'>Quantity</th><th style='text-align:center'>Tax</th><th style='text-align:right'>Price (ex)</th></tr>";

        if(is_array($order["orderContent"])){
          foreach ($order["orderContent"] as $item) {
            echo "<tr><td style='text-align:center'>$item[name]</td><td style='text-align:center'>$item[Quantity] @ $item[Price]</td><td style='text-align:center'>$item[tax]</td><td style='text-align:right'>$order[currency_code] ".($item["Quantity"] * $item["Price"])."</td></tr>";
          }
        }

        echo "
              <tr>
                <td colspan='2' class='totals' ></td>
                <td colspan='2' class='totals'><table cellpadding='2' cellspacing='0' style='width:auto;float:right'>
                  <tr><th align='left'>Subtotal:</th><td align='right'>$order[currency_code] $order[clear_total_price]</td></tr>
                  <tr><th align='left'>Discount @ $order[order_discount]%:</th><td align='right'> - $order[currency_code] $order[discount_amount]</td></tr>
                  <tr><th align='left'>VAT:</th><td align='right'> + $order[currency_code] $order[totalTax]</td></tr>
                  <tr><th align='left'>Total Shipping Cost:</th><td align='right'> + $order[currency_code] $order[shipping_cost]</td></tr>
                  <tr><th align='left'><b style='font-size:16px'>Total:</b></th><td align='right'><b style='font-size:16px'>$order[currency_code] $order[order_amount]</b></td></tr>
                </table></td>
              </tr>
            </table>
          </div>
        </div>
        ";
        echo "<div class='tab_content padded settingsTable' style=''>
        <h1>Processing History</h1>
        <div class='fieldset'>
        <table width='100%' cellpadding='4' cellspacing='0' class='ordered_items statusHistory'>
        <tr><th style='text-align:center'>Time</th><th style='text-align:center'>Comment</th><th style='text-align:center'>Status Name</th></tr>";
        if(is_array($order["orderHistory"])){
          foreach ($order["orderHistory"] as $change) {
            echo "<tr><td style='text-align:center'>".date("d/m/Y H:i:s",$change["status_change_time"])."</td>
            <td style='text-align:center'>$change[status_comment]</td>
            <td style='text-align:center'>".(($change["status_name"]=="STRING_CANCELED_ORDER_STATUS")?("Cancelled"):($change["status_name"]))."</td></tr>";
          }
        }
        echo "</table></div>
        <hr />
        <h2>Change Status</h2>";
        $os=$this->shop->getOrderStatuses();
        $opts="<option value='-1' >Please Select</option>";
        if(is_array($os)){
          foreach ($os as $s) {
            if($s["status_name"]=="STRING_CANCELED_ORDER_STATUS"){
              $s["status_name"]="Cancelled";
            }
            $opts.="<option value='$s[statusID]'>$s[status_name]</option>";
          }
        }

        echo "<div class='fieldset'>
        <form rel='ajaxed' method='post' action='".base_url()."admin/shop_orders/changeOrderStatus' loadingDiv='statusloader' >
            <table width='100%' cellpadding='4' cellspacing='0'>
            <tbody>
              <tr><th>Change order status to</th><td><select name='status'>".$opts."</select></td></tr>
              <tr><th>Notify customer by email about order's status change</th><td><input type='checkbox' name='notify_customer' value='1' /></td></tr>
              <tr><th valign='top'>Comments (if any)</th><td><textarea name='status_comment' rows='5'></textarea></td></tr>
            <tbody>
            </table>
            </div>
            <hr />
            <div>
              <div style='float:right'>
                <div class='q_feedback' id='statusloader' style='float:right'></div>
                <button class='button save' type='submit'>Change Order Status</button>
              </div>
            </div>
            <input type='hidden' name='orderID' value='".$order["orderID"]."' >
          </form>
        </div>";
      }else{
        echo "<div class='tab_content padded' style='display:block'><h1 style='color:red'>Error</h1><p>No such order...</p></div><div class='tab_content padded' style=''></div><div class='tab_content padded' style=''></div>";
      }
    }
  }

  function changeOrderStatus(){
    if(_checkAuthentication($this)){
      $status=$this->input->post("status");
      $status_comment=$this->input->post("status_comment");
      $orderID=$this->input->post("orderID");
      $notify_customer=$this->input->post("notify_customer");
      $status_name="";
      $_time=time();

      if($status<1){
        echo '{"success":"error",successFunction:function(){
          alert("Please select a status!");
          }}';
          exit;
      }

      $os=$this->shop->getOrderStatuses();
      if(is_array($os)){
        foreach ($os as $s) {
          if($status==$s["statusID"]){
            $status_name=$s["status_name"];
          }
        }
      }

      $res=$this->shop->updateOrderStatus($orderID,$status,$status_name,$status_comment,strftime("%Y-%m-%d %H:%M:%S", $_time));

      if($res){
        $text_to_show=(($status_name=="STRING_CANCELED_ORDER_STATUS")?("Cancelled"):($status_name));
        echo '{success:"success",successFunction:function(){
          $("#mainContentInner table.statusHistory tbody").append("<tr><td style=\"text-align:center\">'.date("d/m/Y H:i:s",$_time).'</td><td style=\"text-align:center\">'.$status_comment.'</td><td style=\"text-align:center\">'.$text_to_show.'</td></tr>");
          $("#mainContentInner td.current_status").text("'.$text_to_show.'");
          $("#thirdBar #order_'.$orderID.'").removeClass("state-1 state-2 state-3 state-4 state-5").addClass("state-'.$status.'")
          .find("span").eq(3).text("'.$text_to_show.'");
          NIBS.notif.notify("success","Order status changed successfully.");
        }}';
        if($notify_customer==1){
          //notify customer
        }
      }else{
        echo '{"success":"error",successFunction:function(){
          alert("Could not change Order Status, please try again, or refresh the page");
          }}';
      }
    }
  }

  function _listOrders($res){
    if(is_array($res)){
      foreach ($res as $order) {
        if($order["status_name"]=="STRING_CANCELED_ORDER_STATUS"){
          $order["status_name"]="Cancelled";
        }
        echo "<li id='order_$order[orderID]' class='state-$order[statusID]'>
          <span>$order[customer_firstname] $order[customer_lastname]</span>
          <span>$order[currency_code] ".sprintf("%01.2f", $order["order_amount"])."</span>
          <span>".date("d/m/Y H:i:s",$order["order_time"])."</span>
          <span>$order[status_name]</span>
        </li>";
      }
    }
  }

}