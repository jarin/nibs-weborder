<?php



namespace entities;

/**
 * ResDashboardSettings
 *
 * @Table(name="res_dashboard_settings")
 * @Entity
 */
class ResDashboardSettings
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $value
   *
   * @Column(name="value", type="array", nullable=false)
   */
  private $value;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $user;


  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set value
   *
   * @param string $value
   */
  public function setValue($value)
  {
    $this->value = $value;
  }

  /**
   * Get value
   *
   * @return string 
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set user
   *
   * @param ResUser $user
   */
  public function setUser(\ResUser $user)
  {
    $this->user = $user;
  }

  /**
   * Get user
   *
   * @return ResUser 
   */
  public function getUser()
  {
    return $this->user;
  }
}