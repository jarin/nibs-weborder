<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class <#=ucfirst(plural($base_name))#> extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="<#=$base_name#>";
		$this->load->model("activity_m","activity");
		$this->load->model("<#=plural($base_name)#>_m","<#=plural($base_name)#>");
		$this->load->model("settings_m","settings");
		//Settings
		$this->SETTINGS=array();
		_loadSettings($this);
	}

	function index(){
		if(_checkAuthentication($this)){
			$<#=plural($base_name)#>=$this-><#=plural($base_name)#>->getAll(<#if($reorderable){#>"sort_order"<#}else{#>"id"<#}#>,"desc");
			$html="";
			if(is_array($<#=plural($base_name)#>)){
				foreach($<#=plural($base_name)#> as $<#=$base_name#>){
					if($<#=$base_name#>["active"]==0){
						$class="off";
						$checkedmain="";
					}else{
						$class="";
						$checkedmain="checked";
					}
					$html.="<li id='<#=$base_name#>_$<#=$base_name#>[id]'>
					<span class='name' title='$<#=$base_name#>[headline]'>$<#=$base_name#>[headline]</span>
					<input title='Delete Item' type='image' class='action delete' rel='<#=plural($base_name)#>/delete<#=ucfirst($single_item_name)#>' elId='$<#=$base_name#>[id]' src='".base_url()."assets/images/admin/icon_trash.png' border='0' />
					<input class='main' id='checkboxmain_$<#=$base_name#>[id]' type='checkbox' $checkedmain />
					</li>";
				}
			}
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username));
			$this->load->view("admin/<#=plural($base_name)#>_template",array(
				"pagename"=>"<#=ucfirst(plural($base_name))#> Manager",
				"list1"=>$html,
				<#if($tinymce && $autosave){#>"autosaveUrl"=>"admin/<#=plural($base_name)#>/XautoSave/"<#}#>
			));
			$this->load->view("admin/admin_foot");
		}
	}

	function view<#=ucfirst($single_item_name)#>($id=""){
		if(_checkAuthentication($this)){
			if(trim($id=="")){
				header("location: ".base_url()."admin/<#=plural($base_name)#>/");
			}
			<#if($tinymce && $autosave){#>$has_autosave=false;<#}#>
			$<#=$base_name#>=$this-><#=plural($base_name)#>->get($id);
			if(is_array($<#=$base_name#>) && isset($<#=$base_name#>["id"])){
			  <#if($tinymce && $autosave){#>$autoSaved=$this-><#=plural($base_name)#>->getAutoSaved($<#=$base_name#>["id"],$this->_userid);
				if(is_array($autoSaved) && isset($autoSaved["id"])){
				  $has_autosave=true;
				}<#}#>
				$html="
				<#if($tinymce){#><div class='tab_content padded' style='display:block'><#}else{#>
				  <div class='padded'>
				  <h1><#=ucfirst(uncamelize($single_item_name))#> Details</h1><br /><#}#>
				  <div style='margin:0 auto;width:60%;text-align:center'>
				  <form method='post' rel='ajaxed' action='".base_url()."admin/<#=plural($base_name)#>/save<#=ucfirst($single_item_name)#>Details' loadingDiv='loader_save_page'>
					  <table cellpadding='4' cellspacing='0'>
						  <tr>
							  <th align='left'><label>Headline</label></th>
							  <td align='left' colspan='2'><input type='text' id='edit_pagename_input' name='headline' value=\"".str_replace('"',"&#34;",$<#=$base_name#>["headline"])."\" style='width:300px' /></td>
						  </tr>
						  <tr>
							  <th align='left' valign='top'><label>Excerpt</label></th>
							  <td align='left' colspan='2'><textarea name='excerpt' style='width:300px' rows='7'>".str_replace('"',"&#34;",$<#=$base_name#>["excerpt"])."</textarea></td>
						  </tr>
						  <tr><td></td><td align='right'>
						  <button type='submit' class='button save'>Save Changes</button>
						  <div id='loader_save_page' class='q_feedback'></div>
						  <input type='hidden' name='<#=$base_name#>_id' value='$<#=$base_name#>[id]' /></td></tr>
					  </table></form>
				  </div>
				</div><#if($tinymce){#>
				<div class='tab_content' style='position:absolute;left:0;top:0;width:100%;height:100%;overflow:hidden;'>
				  <div id='editArea'>
				    <form method='post' rel='ajaxed' action='".base_url()."admin/<#=plural($base_name)#>/save<#=ucfirst($single_item_name)#>Content' loadingDiv='loader_save_page_c'>
  							<textarea name='content' style='position:absolute;width:100%;height:100%' id='EditableArea'>".htmlspecialchars($<#=$base_name#>["<#=$content_column#>"])."</textarea>
  							<div class='footertoolbar' style='left:0;right:0;bottom:-31px;position:absolute;z-index:2'>
  							  <div style='float:right'>
    							  <div class='q_feedback' id='loader_save_page_c' style='float:right'></div>
    							  <button type='submit' class='button save'>Save Changes</button>
    							</div>
  							</div>
  							<input type='hidden' name='<#=$base_name#>_id' value='$<#=$base_name#>[id]' />
  				  </from>
  				</div>
				  <#if($tinymce && $autosave){#>".(($has_autosave==true) ? "<div id='autoSaveInfo'>
				    <p>There is an auto-saved version of this page available from your last session...<br />Would you like to load that instead?</p>
				    <p><button class='button' type='button' id='loadAutoSave'>Yes, load auto-saved version</button><button class='button' type='button' style='float:right' id='discardAutoSave'>No, discard it</button></p>
				  </div>" : "")."<#}#>
				</div>
<#}#>				";

				<#if($tinymce){#>$this->load->view("admin/edit_page",array(
					"html"=>$html
				))<#}else{#>echo $html<#}#>;

			}else{
				header("location: ".base_url()."admin/<#=plural($base_name)#>/");
			}
		}
	}

	function view<#=ucfirst($single_item_name)#>Info($id=""){
		if(_checkAuthentication($this)){
		  if(trim($id=="")){
				echo "";
			}
			$<#=$base_name#>=$this-><#=plural($base_name)#>->get($id);
			if(is_array($<#=$base_name#>) && isset($<#=$base_name#>["id"])){
			  echo "<p>
			  <b>Last Modified:</b> <br />
			  &nbsp;&nbsp;&nbsp;&nbsp;<b>By</b> <span class='i_by'>$<#=$base_name#>[modifiedby]</span> <br />
			  &nbsp;&nbsp;&nbsp;&nbsp;<b>On</b> <span class='i_on'>".date("d/m/y H:i",$<#=$base_name#>["modifieddate"])."</span>
			  </p>
			  <br />
			  <p><b><#=ucfirst(uncamelize($single_item_name))#> is currently:</b> <span class='i_state_$<#=$base_name#>[id]'>".(($<#=$base_name#>["active"]==1)?("Active"):("Inactive"))."</span></p>";
		  }
	  }
	}

	function save<#=ucfirst($single_item_name)#>Details(){
		if(_checkAuthentication($this)){
			$<#=$base_name#>_id=$this->input->post('<#=$base_name#>_id');
			$excerpt=$this->input->post('excerpt');
			$headline=$this->input->post("headline");
			$modified_date=time();
			if(trim($<#=$base_name#>_id)!=""){
				if($this-><#=plural($base_name)#>->save($<#=$base_name#>_id,array(
					"headline"=>$headline,
					"excerpt"=>$excerpt,
					"modifieddate"=>$modified_date,
					"modifiedby"=>$this->_username
					))){

					activitylog($this,"<#=ucfirst(uncamelize($single_item_name))#> <i>$headline</i> details modified","m");

					echo '{success:"success",successFunction:function(){
						$("#infoPanel").find(".i_by").html("'.addslashes($this->_username).'").end()
						.find(".i_on").html("'.date("d/m/y H:i",$modified_date).'");
						$("#<#=$base_name#>_'.$<#=$base_name#>_id.'").find("span").html("'.str_replace('"',"&#34;",$headline).'")
						.attr("title","'.str_replace('"',"&#34;",$headline).'");
						NIBS.notif.notify("success","<#=ucfirst(uncamelize($single_item_name))#> details saved");
					}}';
				}else{
					echo '{success:"error",msg:"Could not save to database..."}';
				}
			}else{
				echo '{success:"error",msg:"Please reload page..."}';
			}
		}
	}

	function save<#=ucfirst($single_item_name)#>Content(){
		if(_checkAuthentication($this)){
			$<#=$base_name#>_id=$this->input->post('<#=$base_name#>_id');
			$content=$this->input->post("content");
			$modified_date=time();
			if(trim($<#=$base_name#>_id)!=""){
			  $N=$this-><#=plural($base_name)#>->get($<#=$base_name#>_id);
				if($this-><#=plural($base_name)#>->save($<#=$base_name#>_id,array(
					"<#=$content_column#>"=>$content,
					"modifieddate"=>$modified_date,
					"modifiedby"=>$this->_username
					))){
<#if($tinymce && $autosave){#>
				  //delete redundant autosaves
					@$this-><#=plural($base_name)#>->deleteAutoSave($<#=$base_name#>_id,$this->_userid);
<#}#>
          activitylog($this,"<#=ucfirst(uncamelize($single_item_name))#> <i>".$N["headline"]."</i> content modified","m");

					echo '{success:"success",successFunction:function(){
						$("#infoPanel").find(".i_by").html("'.addslashes($this->_username).'").end()
						.find(".i_on").html("'.date("d/m/y H:i",$modified_date).'");
						NIBS.notif.notify("success","<#=ucfirst(uncamelize($single_item_name))#> content saved");
					}}';
				}else{
					echo '{success:"error",msg:"Could not save to database..."}';
				}
			}else{
				echo '{success:"error",msg:"Please reload page..."}';
			}
		}
	}

	function add<#=ucfirst($single_item_name)#>(){
		if(_checkAuthentication($this)){
			$headline=$this->input->post("headline");

			if(trim($headline)==""){
				echo '{"success":"error",successFunction:function(){
					alert("Please specify a headline for the <#=uncamelize($single_item_name)#>");
					}}';
					exit;
			}<#if($reorderable){#>
			$sort_order=$this-><#=plural($base_name)#>->nextSortOrderValue();<#}#>
			$modified_date=time();
			$ins=$this-><#=plural($base_name)#>->create(array(
				"headline"=>$headline,<#if($reorderable){#>
				"sort_order"=>$sort_order,<#}#>
				"modifieddate"=>$modified_date,
				"modifiedby"=>$this->_username,
				"created"=>$modified_date
				));
			if($ins){

			  activitylog($this,"<#=ucfirst(uncamelize($single_item_name))#> <i>".$headline."</i> added","c");

				echo '{"success":"success",successFunction:function(){
					add<#=ucfirst($single_item_name)#>("'.addslashes($headline).'","'.$ins.'","'.date("d/m/y H:i",$modified_date).'","'.$this->_username.'");
				}}';
				exit;

			}else{
				echo '{"success":"error",successFunction:function(){
					alert("Could not add <#=uncamelize($single_item_name)#> due to a database error");
					}}';
					exit;
			}
		}
	}

	function delete<#=ucfirst($single_item_name)#>(){
		if(_checkAuthentication($this)){
			$<#=$base_name#>_id=$this->input->post('id');
			if(trim($<#=$base_name#>_id)==""){
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove <#=uncamelize($single_item_name)#> due to a database error,\\nPlease reload page");
					}}';
					exit;
			}
			$N=$this-><#=plural($base_name)#>->get($<#=$base_name#>_id);
			if($this-><#=plural($base_name)#>->delete($<#=$base_name#>_id)){
			<#if($tinymce && $autosave){#>//delete all autosaves
				@$this-><#=plural($base_name)#>->deleteAutoSaves($page_id);<#}#>

				activitylog($this,"<#=ucfirst(uncamelize($single_item_name))#> <i>".$N["headline"]."</i> deleted","d");

				echo '{"success":"success",successFunction:function(){
					delete<#=ucfirst($single_item_name)#>("'.$<#=$base_name#>_id.'");
					}}';
					exit;
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove <#=uncamelize($single_item_name)#> due to a database error");
					}}';
					exit;
			}
		}
	}
<#if($reorderable){#>
	function reOrder<#=ucfirst($base_name)#>Items(){
		if(_checkAuthentication($this)){
			if($this-><#=plural($base_name)#>->reorder($this->input->post("<#=$base_name#>"),true)){
				echo '{success:"success",msg:"Saved"}';
			}else{
				echo '{success:"error",msg:"Could not save to database..."}';
			}
		}
	}<#}#>

	function activateDeactivate<#=ucfirst($single_item_name)#>(){
		if(_checkAuthentication($this)){
			$id=$this->input->post('id');
			$res=$this-><#=plural($base_name)#>->get($id);
			if($res["active"]=="0"){
				$new_value="1";
			}else{
				$new_value="0";
			}
			if($this-><#=plural($base_name)#>->save($id,array("active"=>$new_value))){
				echo '{"success":"success",successFunction:function(){
					do_activate_<#=ucfirst($single_item_name)#>("'.$id.'","'.$new_value.'");
					}}';
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not toggle visiblity due to server error...<br />Please try again");
					}}';
			}
		}
	}
<#if($tinymce && $autosave){#>
	function XautoSave(){
	  $user_id=$this->_userid;
	  $<#=$base_name#>_id=$this->input->post('<#=$base_name#>_id');
		$<#=$base_name#>=$this->input->post("content");
		$this-><#=plural($base_name)#>->autoSave($<#=$base_name#>_id,$user_id,$<#=$base_name#>);
		echo "{success:'success'}";
	}

	function XgetAutoSave($<#=$base_name#>_id){
    $user_id=$this->_userid;
    $res=$this-><#=plural($base_name)#>->getAutoSaved($<#=$base_name#>_id,$user_id);
    if(is_array($res) && isset($res["id"])){
      echo '{data:"'.str_replace(array("\r","\t","\n"),"",str_replace(array("'",'"'),"\'",$res["content"])).'"}';
    }else{
      echo '{data:false}';
    }
	}

<#}#>
}


?>
