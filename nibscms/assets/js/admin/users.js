$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addUserBtn").overlay({width:"30%",inline:true,title:"Add New User",href:"#addUserForm"});

  // See More details on click
  $("#sidebar ul.list li").live("click",function(){
		var _li=$(this);
		_li.siblings().removeClass("active");
		_li.addClass("active");
		loadUserDetails(_li.attr("id"));
		return false;
	});

	//checkbox states
	$("#thirdBar input[type=checkbox]:not(.main_cb)").live("click",function(){
	  determineMainCBState(this);
	});
});

function replaceImage(id){
  $("#"+id).find("img").attr("src","");
}

function addUser(name,id,level){
  NIBS.notif.notify("success",name+" has been added as a new user");
	if(level=="admin"){
		var img="<img src='"+base_url+"assets/images/admin/icon_user.png' border='0' />";
	}else{
		var img="<img src='"+base_url+"assets/images/admin/icon_user.png' border='0' />";
	}
	var html="<li id='user_"+id+"' class='deleted'><a href='#'><span class='name icon-box'><i class=''>"+img+"</i></span><span class='nameBtn'>"+name+"</span>"+
	"<span class='list_icon_btn'><input title='Delete User' type='image' class='action delete' rel='users/deleteUser' "+
	"elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' /></span></a>"+
	"</li>";

	$("#sidebar ul.list").append(html);
	$("#add_user_form").reset();
	setTimeout(function(){
	  $("#user_"+id).removeClass("deleted");
	},100);
}

function loadUserDetails(id){
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#dContent").load(base_url+"admin/users/loadUserDetails/"+id);
	$("#thirdBar").show(0);
	$("#main-content").addClass("threepane");
	$("#thirdBar").html("<div style='text-align:center;margin-top:40px'><button class='button edit secondary' onclick='loadUserPermissions(\"user_"+id+"\");return false;'>Manage Permissions</button></div>");
}

function deleteUser(user_id){
  var el=$("#user_"+user_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
    $("#thirdBar").hide(0);
    $("#main-content").removeClass("threepane");
  }
	el.addClass("deleted");
	setTimeout(function(){
	  el.remove()
	},500);
	NIBS.notif.notify("success",el.find("span").text()+" removed successfully");
}

function loadUserPermissions(id){
	var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#thirdBar").load(base_url+"admin/users/loadUserPermissions/"+id,function(){
	  $("#thirdBar input[type=checkbox]:not(.main_cb):first-child").each(function(){
	    determineMainCBState(this);
	  });
	});
}

function checkUncheck_all_perms(e,el){
  e.stopPropagation();
  var el=$(el),
  parent=el.parent().parent(),
  currentState=el.prop("checked");
  if(currentState==true || currentState==false){
    $("input[type=checkbox]",parent[0]).prop("checked",currentState);
  }
}

function determineMainCBState(el){
  var el=$(el),
  parent=el.parent().parent(),
  main_cb=parent.find("input.main_cb"),
  initial=false,truecount=falsecount=totalcount=0;
  parent.find("input[type=checkbox]:not(.main_cb)").each(function(){
    var _this=$(this);
    _this.prop("checked")==true ? truecount++ : falsecount++;
    totalcount++;
  });
  if(totalcount==truecount){
    main_cb.prop("indeterminate",false).prop("checked",true);

  }else if(totalcount==falsecount){
    main_cb.prop("indeterminate",false).prop("checked", false);
  }else{
    main_cb.prop("indeterminate",true);
  }
}
