       <div id="page-content">
       
		 <?php $this->load->view('top_content');?>

			<!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-9 col-sm-12 col-md-push-0">
				         <div class="page-content">
				           <div class="news-events-blog">
				               <?php 
				                   if(count($cartItems) > 0 ){
				                   $total = 0;
				                   foreach ($cartItems as $cartItem){
				                       $total = $total + $cartItem['total_price'];
				                   ?>
    							<div class="blog-list">
    								<div class="row">
    									<!-- <div class="col-md-4 col-sm-4">
    										<div class="blog-list-img">
    											<img class="" src="assets/img/content/blog-post.jpg" alt="">
    										</div>
    									</div> -->
    									<div class="col-md-12 col-sm-12">
    										<h5>
    										<?php echo $cartItem['product_name']?>
    										<span class="editLinkCart"><i class="fa fa-pencil editItem"  rel="<?php echo $cartItem['product_id']?>" data-item="<?php echo $cartItem['orderItemId']?>"></i><i class="fa fa-times delItems" rel="<?php echo $cartItem['product_id']?>" data-item="<?php echo $cartItem['orderItemId']?>"></i></span>
											</h5>
    										<p class="bl-sort"><?php echo $cartItem['description']?></p>
    										
    										<div class="tag-list">
    										    <?php if(count($cartItem['without']) > 0){
    										        $numItems = count($cartItem['without']);
    										        $i = 0;
    										        ?>
    											<p><i class="fa fa-minus-circle"></i>Without: 
    											<span class="bl-sort"><?php foreach ($cartItem['without'] as $key => $without){ echo $without['ingredientName']; if(++$i != $numItems) { echo ', ';} }?></span></p>
    											<?php } ?>
    											<?php if(count($cartItem['with']) > 0){
    											    $numItems = count($cartItem['with']);
    										        $i = 0;
    										        ?>
    											<p><i class="fa fa-plus-circle"></i>With: <span class="bl-sort"><?php foreach ($cartItem['with'] as $key => $with){ echo $with['ingredientName'];if(++$i != $numItems) { echo ', ';}}?></span></p>
    											<?php } ?>
    											<?php if(count($cartItem['addon']) > 0){
    											    $numItems = count($cartItem['addon']);
    										        $i = 0;
    										        ?>
    											<p><i class="fa fa-plus-square"></i>Add-On: <span class="bl-sort"><?php foreach ($cartItem['addon'] as $key => $addon){ echo $addon['addonName'];if(++$i != $numItems) { echo ', ';}}?></span></p>
    											<?php } ?>
    											<?php if($cartItem['instruction'] !='') {?>
    											<p><i class="fa fa-book"></i>Additional Notes: <span class="bl-sort"><?php echo $cartItem['instruction']?></span></p>
    											<?php } ?>
    											<p class="total">
    											    <span><i class="fa fa-shopping-cart"> </i>Quantity: <?php echo $cartItem['quantity']?></span>
    											    <span>Price: <b class="cartPrice">&pound;<?php echo $cartItem['price']?></b></span>
    											    <span>Total Price: <b class="cartPrice">&pound;<?php echo $cartItem['total_price']?></b></span>
    											</p>
    											
    										</div>
    									</div>
    									<!-- end .grid-layout -->
    								</div>
    								<!--end .row-->
    							</div>
    							<!--end .blog-list -->
    							
                                <?php } ?>
                              
				<hr class="content-divider color-transparent">
				<div class="call-to-action-section">
					<div class="css-table-cell">
						<div class="icon">
							<i class="fa fa-gbp" ></i>
						</div>
					</div>
					<div class="text css-table">
						<div class="css-table-cell">
							<h4>Total Price</h4>
							
						</div>


						<div class="css-table-cell" style="text-align:right">
							 <b class="cartPrice">&pound;<?php echo $total;?></b>
						</div>
					</div>
				</div>
				<!-- end .call-to-action-section -->
			
                                <?php } ?>
                                <!-- Modal -->
                                    <div id="editItemModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:600px;left:30%;top:10%;">
                                      <div style="background: #fff;">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                        <h3 id="myModalLabel" style="font-weight: bold;">Edit Cart Items</h3>
                                      </div>
                                      <form class="default-form form-inline" role="form" id="updateItem">
                                          <input type="hidden" name="confirm" id="confirm" value="1" />
                                          <div class="modal-body modal_content">
                                            
                                          </div>
                                          <div class="modal-footer">
                                            <button class="btn btn-default-red updateCart" name="submit" type="submit" style="padding:2px 10px">Confirm</button>
                                            <button class="btn btn-default-black" data-dismiss="modal" aria-hidden="true" style="margin-bottom:15px;">Close</button>
                                          </div>
                                      </form>
                                      </div>
                                    </div>
                                    <!-- END Modal -->
                                    
                                
                            	<a class="btn btn-default-black " href="order" style="padding: 7px 15px;">Add More Item</a>
                            	<a class="btn btn-default-red"  href="order/checkout" style="float:right;margin-top:15px"><i class="fa fa-shopping-cart"></i> Checkout</a>
    							
    							<!--  <div class="pagination">
    								<ul class="list-inline  text-right">
    									<li class="active"><a href="#">1</a>
    									</li>
    									<li><a href="#">2</a>
    									</li>
    									<li><a href="#">3</a>
    									</li>
    									<li><a href="#">4</a>
    									</li>
    									<li><a href="#">5</a>
    									</li>
    									<li><a href="#">6</a>
    									</li>
    								</ul>
    							</div> -->
						    </div>
						   <!-- end .news-events-blog -->
					    </div>
					    <!-- end .page-content -->
					</div>
			            <!-- end .col-md-9 -->

					<?php $this->load->view('left_sidebar')?>
				</div>
				<!-- end .row -->
			</div>
			<!--end .container -->