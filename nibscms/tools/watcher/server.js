var fs=require("fs"),
app=require('http').createServer(handler),
io = require('socket.io').listen(app),
config={
  basepath:"../../",
  host:"http://192.168.1.116/"
};

app.listen(5713);

function handler(req,res){
  res.writeHead(200);
  res.end("hello");
}
io.sockets.on('connection', function (socket) {
  var watching=[];
  socket.on('watch', function (data) {
    var relPath=data.path.replace(config.host,config.basepath);
    if(relPath!=data.path){
      var file=relPath + data.filename;
      watching.push(file);
      console.log("watching :"+file);
      fs.watchFile(file, function (curr, prev) {
        console.log("filechanged :"+data.filename);
        if(curr.mtime!==prev.mtime){
          socket.emit("filechanged",{filename:data.filename})
        }
      });
    }
  });
  socket.on('disconnect', function () {
    for(var i=0;i<watching.length;i++){
      console.log("unwatching :"+watching[i]);
      fs.unwatchFile(watching[i]);
    }
  });
});