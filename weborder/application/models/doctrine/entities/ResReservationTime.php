<?php



namespace entities;

/**
 * ResReservationTime
 *
 * @Table(name="res_reservation_time")
 * @Entity
 */
class ResReservationTime
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var datetime $timeFrom
   *
   * @Column(name="time_from", type="time", nullable=false)
   */
  private $timeFrom;
  
  /**
   * @var datetime $timeTo
   *
   * @Column(name="time_to", type="time", nullable=false)
   */
  private $timeTo;


  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set timeFrom
   *
   * @param time $timeFrom
   */
  public function setTimeFrom($timeFrom)
  {
    $this->timeFrom = $timeFrom;
  }

  /**
   * Get timeFrom
   *
   * @return time 
   */
  public function getTimeFrom()
  {
    return $this->timeFrom;
  }

  /**
   * Set timeTo
   *
   * @param time $timeTo
   */
  public function setTimeTo($timeTo)
  {
  	$this->timeTo = $timeTo;
  }
  
  /**
   * Get timeTo
   *
   * @return time
   */
  public function getTimeTo()
  {
  	return $this->timeTo;
  }
}