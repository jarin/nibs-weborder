 <?php
$baseUrl=base_url();

?>
<link href="<?=$baseUrl?>assets/css/admin/jquery-ui/jquery.ptTimeSelect.css" rel="stylesheet" media="screen">
    <script src="<?php echo $baseUrl; ?>assets/js/admin/jquery-ui/jquery.ptTimeSelect.js"></script>
    <script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/profileEdit.js"></script>
<div id='main-content' class='onepane'>
        <div class='container-fluid' style=''>
          <div class='row-fluid'>
            <div class='span12'>	
			<div class=''>
				<h3 class='page-title'>Business Profile</h3><br />
				<div id='form_wizard_1' class='widget box'>
		            
		        <div class='widget-body form'>
				<div style='margin:0 auto;width:60%;text-align:center'>
				<form method='post' rel='ajaxedUpload' action='<?=base_url()?>admin/business/saveBusinessProfile' loadingDiv='loader<?=$business['id']?>' autocomplete='off' enctype='multipart/form-data'>
					<table cellpadding='4' cellspacing='0' align='center'>
						<tr>
							<th align='left' >Business Name</th>
							<td ><input type='text' name='businessname' value="<?=$business['name']?>" id='edit_businessname_input' /></td>
						</tr>
						<tr>
							<th align='left' >Username</th>
							<td ><input type='text' name='username' value="<?=$business['username']?>" id='edit_username_input' /></td>
						</tr>
						<tr>
							<th align='left' >Password </th>
							<td ><input type='password' name='password' /> </td>
						</tr>
						<tr>
							<th align='left' >Retype Password</th>
							<td ><input type='password' name='re_password' id='edit_re_password_input' /></td>
						</tr>
						<tr>
							<th align='left' >Email</th>
							<td ><input type='text' name='email' id='edit_email_input' value="<?=$business['email']?>" /></td>
						</tr>
						<tr>
							<th align='left' >Phone</th>
							<td ><input type='text' name='phone' id='edit_phone_input' value="<?=$business['phone']?>" /></td>
						</tr>
						<tr>
							<th align='left' >Land Line Number</th>
							<td ><input type='text' name='landline' id='edit_landline_input' value="<?=$business['land_line']?>" /></td>
						</tr>
						<tr>
							<th align='left' >Address Line 1</th>
							<td ><textarea  name='address1' id='edit_address1_input' ><?=$business['address_line_1'] ?></textarea></td>
						</tr>
						<tr>
							<th align='left' >Address Line 2</th>
							<td ><textarea  name='address2' id='edit_address2_input' ><?=$business['address_line_2']?></textarea></td>
						</tr>
						<tr >
							<th align='left' >Opening Time </th>
							<td ><input type='text'  name='opening' id='edit_opening_input' value="<?php echo $business['opening']; ?>" /></td>
						</tr>
						<tr >
							<th align='left' >Closing Time </th>
							<td ><input type='text'  name='closing' id='edit_closing_input' value="<?php echo $business['closing']; ?>" /></td>
						</tr>
						<tr >
							<th align='left' >Reservation</th>
							<td ><input type='checkbox'  name='reservation' value="1" <?php echo (($business['is_reservation'] ==1)? 'checked':'')?>/></td>
						</tr>
						<tr >
							<th align='left'>Deposit</th>
							<td ><input type='checkbox'  name='deposit' value="1" <?=(($business['is_deposit'] ==1)? 'checked':'')?> /></td>
						</tr>
						<tr >
							<th align='left'>Deposit Week Day</th>
	                     	<td >
		                     	<select name="depositweekday">
		                     		<option value="">Select</option>
			                    	<option value="1" <?=(($business['deposit_week_day'] ==1)? 'selected':'')?>>Sunday</option>
			                    	<option value="2" <?=(($business['deposit_week_day'] ==2)? 'selected':'')?>>Monday</option>
			                    	<option value="3" <?=(($business['deposit_week_day'] ==3)? 'selected':'')?>>Tuesday</option>
			                    	<option value="4" <?=(($business['deposit_week_day'] ==4)? 'selected':'')?>>Wednesday</option>
			                    	<option value="5" <?=(($business['deposit_week_day'] ==5)? 'selected':'')?>>Thursday</option>
			                    	<option value="6" <?=(($business['deposit_week_day'] ==6)? 'selected':'')?>>Friday</option>
			                    	<option value="7" <?=(($business['deposit_week_day'] ==7)? 'selected':'')?>>Saturday</option>
			                    </select>
	                    	</td>
						</tr>
						<tr >
							<th align='left'>Deposit Amount</th>
	                     	<td ><input type='text'  name='deposit_amount' value="<?=$business['deposit_amount']?>" /></td>
						</tr>
						<tr >
							<th align='left'>Minimum Order Value for Collection</th>
	                     	<td ><input type='text'  name='order_value_collection' value="<?=$business['order_value_collection']?>" /></td>
						</tr>
						<tr >
							<th align='left'>Minimum Order Value for Delivery</th>
	                     	<td ><input type='text'  name='order_value_delivery' value="<?=$business['order_value_delivery']?>" /></td>
						</tr>
						<tr>
							<th align='left' >Url</th>
							<td ><?=str_replace($this->replace, $this->replace_word, base_url()).'restaurant/'.$business['unique_url']?></td>
						</tr>
						
						<tr>
							<td align='right' colspan='4'>
							<button type='submit' class='button save'>Save Changes</button>
							<div id='loader<?=$business['id']?>' class='q_feedback'></div>
							<input type='hidden' name='u_id' value='<?=$business['uid']?>' />
							<input type='hidden' name='business_id' value='<?=$business['id']?>' />
							</td>
						</tr>
					</table>
				</form></div>
			</div>
			</div></div>
			</div>
			</div>
			</div>
			</div>