<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->_userid=$this->session->userdata("uid");
		$this->_username=$this->session->userdata("uname");
		$this->_utype=$this->session->userdata("utype");
		$this->_userImg=$this->session->userdata("uimg");
		$this->load->library("json");
		$this->load->helper("generic");

		//controller Specific
		$this->tab="pages";
		$this->load->model("activity_m","activity");
		$this->load->model("pages_m","pages");
		$this->load->model("settings_m","settings");
		//Settings
		$this->SETTINGS=array();
		_loadSettings($this);
	}

	function index(){
		if(_checkAuthentication($this)){
			if($this->_utype == 'admin'){
				$contentPages=$this->pages->getAll("sort_order");
			} else {
				$contentPages=$this->pages->getAllPages($this->_userid, "sort_order");
			}			
			$html="";
			if(is_array($contentPages)){
				foreach($contentPages as $contentPage){
					if($contentPage["active"]==0){
						$class="off";
						$checkedmain="";
					}else{
						$class="";
						$checkedmain="checked";
					}
					$html.="<li id='page_$contentPage[id]'>
					<a href='#'>
						<span class='name icon-box'><i class=''><img src='".base_url()."assets/images/admin/drag_handle_active.png'/></i></span>
						<span class='name nameBtn' title='$contentPage[name]'>$contentPage[name]</span>
						<span class='list_icon_btn'>
							<input title='Delete Page' type='image' class='action delete' rel='pages/deletePage' elId='$contentPage[id]' src='".base_url()."assets/images/admin/icon_trash.png' border='0' />
							<input class='main' id='checkboxmain_$contentPage[id]' type='checkbox' $checkedmain />
						</span>
					</a></li>";
				}
			}
			$this->load->view("admin/admin_head",array("tab"=>$this->tab,"username"=>$this->_username, "userImg"=>$this->_userImg));
			$this->load->view("admin/pages_template",array(
				"pagename"=>"Standalone Pages Manager",
				"list1"=>$html,
				"autosaveUrl"=>"admin/pages/XautoSave/"
			));
			$this->load->view("admin/admin_foot");
		}
	}

	function viewPage($id=""){
		if(_checkAuthentication($this)){
			if(trim($id=="")){
				header("location: ".base_url()."admin/pages/");
			}
			$has_autosave=false;
			$contentPage=$this->pages->get($id);
			if(is_array($contentPage) && isset($contentPage["id"])){
			  $autoSaved=$this->pages->getAutoSaved($contentPage["id"],$this->_userid);
				if(is_array($autoSaved) && isset($autoSaved["id"])){
				  $has_autosave=true;
				}
				$html="
				<div class='tab_content padded' style='display:block'>
					<div id='form_wizard_1' class='widget box'>
		    			<div class='widget-title'>
					        <h4><i class='icon-reorder'></i>Content Manager</h4>
					    </div>
    
    					<div class='widget-body form'>
				  			<div style='margin:0 auto;width:60%;text-align:center'>
							  <form method='post' rel='ajaxedUpload' action='".base_url()."admin/pages/savePageDetails' loadingDiv='loader_save_page' enctype='multipart/form-data'>
								  <table cellpadding='4' cellspacing='0'>
									  <tr>
										  <th align='left'><label>Name</label></th>
										  <td align='left' colspan='2'><input type='text' id='edit_pagename_input' name='name' rel='$contentPage[id]' value=\"".str_replace('"',"&#34;",$contentPage["name"])."\"
										  style='width:300px'
										  class='stubSenders'
										  stubReceiver='edit_pagestub_input'
										  stubStatus='edit_stubReturnValue'
										  stubUrl='admin/pages/checkStubDuplication'
										  stubPageTitle='edit_pagetitle_input'
										   /></td>
									  </tr>
									  <tr>
										  <th align='left'><label>Page Title</label></th>
										  <td align='left' colspan='2'><input type='text' style='width:300px' id='edit_pagetitle_input' name='pagetitle' value=\"".str_replace('"',"&#34;",$contentPage["pagetitle"])."\"  /></td>
									  </tr>
										<tr>
											  <th align='left'><label>Stub</label></th>
											  <td align='left'><input type='text' name='stub' rel='$contentPage[id]'  id='edit_pagestub_input' value=\"".$contentPage["stub"]."\"
											  style='width:300px'
											  class='stubReceivers'
											  stubStatus='edit_stubReturnValue'
											  stubUrl='admin/pages/checkStubDuplication'
											   /></td>
											  <td width='20'><span id='edit_stubReturnValue' style='font-weight:bold;'></span></td>
										 </tr>

										<tr>
											<th>Meta Keyword</th>
											<td ><input type='text' name='keyword' id='edit_keyword_input'  style='width:300px' value=\"".$contentPage["meta_keyword"]."\"/></td>
										</tr>
										<tr>
											<th>Meta Description</th>
											<td ><textarea  name='description' id='edit_description_input'  style='width:300px'>".$contentPage["meta_description"]."</textarea></td>
										</tr>

										<tr><td></td></tr>
										<tr><td></td></tr>
										<tr><td></td></tr>
                                        
										<tr>
									
										<tr><td></td></tr>

										<tr><td colspan='4'><hr></td></tr>
										
									  <tr><td colspan='2' align='right'>
									  <button type='submit' class='button save'>Save Changes</button>
									  <div id='loader_save_page' class='q_feedback'></div>
									  <input type='hidden' name='page_id' value='$contentPage[id]' /></td></tr>
								  </table></form>
								  </div>
						  		</div>
						 	</div>
						</div>
				  	</div>
				</div>
				<div class='tab_content' style='position:absolute;left:0;top:0;width:100%;height:100%;overflow:hidden;'>
				  <div id='editArea'>
				    <form method='post' rel='ajaxed' action='".base_url()."admin/pages/savePageContent' loadingDiv='loader_save_page_c'>
  							<textarea name='content' style='position:absolute;width:100%;height:100%' id='EditableArea'>".htmlspecialchars($contentPage["content"])."</textarea>
  							<div class='footertoolbar' style='left:0;right:0;bottom:-24px;position:absolute;z-index:2'>
  							  <div style='float:right'>
    							  <div class='q_feedback' id='loader_save_page_c' style='float:right'></div>
    							  <button type='submit' class='button save'>Save Changes</button>
    							</div>
  							</div>
  							<input type='hidden' name='page_id' value='$contentPage[id]' />
  				  </form>
  				</div>
				  ".(($has_autosave==true) ? "<div id='autoSaveInfo'>
				    <p>There is an auto-saved version of this page available from your last session...<br />Would you like to load that instead?</p>
				    <p><button class='button' type='button' id='loadAutoSave'>Yes, load auto-saved version</button><button class='button' type='button' style='float:right' id='discardAutoSave'>No, discard it</button></p>
				  </div>" : "")."
				</div>
				";

				$this->load->view("admin/edit_page",array(
					"html"=>$html
				));

			}else{
				header("location: ".base_url()."admin/pages/");
			}
		}
	}

	function deleteImage(){
    if(_checkAuthentication($this)){
      $img_id=$this->input->post('id');
      $id=explode("_", $img_id);
      if(trim($id[1])!=""){
        $img="";
        if($id[0]=='bannerImg'){
          $img.="banner";
        }
        $c=$this->pages->save($id[1],array(
          $img=>""
          ));
        if($c){
          activitylog($this,"Pages details modified","m");

           echo '{success:"success",successFunction:function(){
             $("#page_'.$id[1].'").find("span.nameBtn");
             replaceImage("'.$img_id.'");
           }}';
         }else{
           echo '{success:"error",msg:"Could not save to database..."}';
         }
       }else{
         echo '{success:"error",msg:"Please reload page..."}';
       }
    }
  }

	function viewPageInfo($id=""){
		if(_checkAuthentication($this)){
		  if(trim($id=="")){
				echo "";
			}
			$contentPage=$this->pages->get($id);
			if(is_array($contentPage) && isset($contentPage["id"])){
			  echo "<p>
			  <b>Last Modified:</b> <br />
			  &nbsp;&nbsp;&nbsp;&nbsp;<b>By</b> <span class='i_by'>$contentPage[modifiedby]</span> <br />
			  &nbsp;&nbsp;&nbsp;&nbsp;<b>On</b> <span class='i_on'>".date("d/m/y H:i",$contentPage["modifieddate"])."</span>
			  </p>
			  <br />
			  
			  <br/>
			  <p><b>Page is currently:</b> <span class='i_state_$contentPage[id]'>".(($contentPage["active"]==1)?("Active"):("Inactive"))."</span></p>";
		  }
	  }
	}

	/**/
	function savePageDetails(){
	  if(_checkAuthentication($this)){
	    $page_id=$this->input->post('page_id');
			$pagetitle=$this->input->post('pagetitle');
			$stub=$this->input->post("stub");
			$name=$this->input->post("name");
			$keyword = strip_tags($this->input->post("keyword"));
			$description = strip_tags($this->input->post("description"));

			$modified_date=time();
			$data=array(					
				"modifieddate"=>$modified_date,
				"modifiedby"=>$this->_username
				);
			$data= array(
				"name"=>$name,
				"pagetitle"=>$pagetitle,
				"stub"=>$stub,
				"meta_keyword" => $keyword,
				"meta_description" => $description,
				);

			if(trim($page_id)!=""){
				$c=$this->pages->save($page_id,$data);
				if($c){
					activitylog($this,"Page <i>$name</i> details modified","m");

					echo '{success:"success",successFunction:function(){
						$("#infoPanel").find(".i_by").html("'.addslashes($this->_username).'").end()
						.find(".i_on").html("'.date("d/m/y H:i",$modified_date).'");
						$("#page_'.$page_id.'").click().find("span.nameBtn").html("'.addslashes($name).'")
						.attr("title","'.addslashes($name).'");
						NIBS.notif.notify("success","Page details saved");
					}}';
				}else{
					echo '{success:"error",msg:"Could not save to database..."}';
				}
			}else{
				echo '{success:"error",msg:"Please reload page..."}';
			}
    }
	}

	function savePageContent(){
		if(_checkAuthentication($this)){
			$page_id=$this->input->post('page_id');
			$content=$this->input->post("content");
			$modified_date=time();
			if(trim($page_id)!=""){
			  $P=$this->pages->get($page_id);
				if($this->pages->save($page_id,array(
					"content"=>$content,
					"modifieddate"=>$modified_date,
					"modifiedby"=>$this->_username
					))){

				  //delete redundant autosaves
					@$this->pages->deleteAutoSave($page_id,$this->_userid);

					activitylog($this,"Standalone page <i>".$P["name"]."</i> content modified","m");

					echo '{success:"success",successFunction:function(){
						$("#infoPanel").find(".i_by").html("'.addslashes($this->_username).'").end()
						.find(".i_on").html("'.date("d/m/y H:i",$modified_date).'");
						NIBS.notif.notify("success","Page content saved");
					}}';
				}else{
					echo '{success:"error",msg:"Could not save to database..."}';
				}
			}else{
				echo '{success:"error",msg:"Please reload page..."}';
			}
		}
	}


	function addPage(){
		if(_checkAuthentication($this)){
			$name=$this->input->post("name");
			$pagetitle=$this->input->post('pagetitle');
			$stub=$this->input->post("stub");
			$keyword = strip_tags($this->input->post("keyword"));
			$description = strip_tags($this->input->post("description"));
			$modified_date=time();

			if(trim($name)==""){
				echo '{"success":"error",successFunction:function(){
					alert("Please specify a name for the page");
					}}';
					exit;
			}

			$sort_order=$this->pages->nextSortOrderValue();

			if(trim($stub)==""){
				$stub=preg_replace('/[-]{2,}/','-',preg_replace('/-$/','',preg_replace('/^-/','',preg_replace('/[&| \/\\*%\)\(\]\[\}\{\"\'=+–><?.`,:;\#]/',"-",trim($name)))));
			}
			if(trim($pagetitle)==""){
				$pagetitle=$name;
			}

			$ins=$this->pages->create(array(
				"name"=>$name,
				"pagetitle"=>$pagetitle,
				"stub"=>$stub,
				"sort_order"=>$sort_order,
				"modifieddate"=>$modified_date,
				"modifiedby"=>$this->_username,
				"created"=>$modified_date,
				"created_by" => $this->_userid,
				"meta_keyword" => $keyword,
				"meta_description" => $description,
				));
			if($ins){

			  activitylog($this,"Standalone page <i>".$name."</i> added","c");

				echo '{"success":"success",successFunction:function(){
					addPage("'.addslashes($name).'","'.$ins.'","'.date("d/m/y H:i",$modified_date).'","'.$this->_username.'");
				}}';
				exit;

			}else{
				echo '{"success":"error",successFunction:function(){
					alert("Could not add Content Page due to a database error");
					}}';
					exit;
			}
		}
	}

	function deletePage(){
		if(_checkAuthentication($this)){
			$page_id=$this->input->post('id');
			if(trim($page_id)==""){
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove Content Page due to a database error,\\nPlease reload page");
					}}';
					exit;
			}
			$P=$this->pages->get($page_id);
			if($this->pages->delete($page_id)){
			  //delete all autosaves
				@$this->pages->deleteAutoSaves($page_id);

				activitylog($this,"Standalone page <i>".$P["name"]."</i> deleted","d");

				echo '{"success":"success",successFunction:function(){
					deletePage("'.$page_id.'");
					}}';
					exit;
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not remove Content Page due to a database error");
					}}';
					exit;
			}
		}
	}

	function reOrderPages(){
		if(_checkAuthentication($this)){
			if($this->pages->reorder($this->input->post("page"))){
				echo '{success:"success",msg:"Saved"}';
			}else{
				echo '{success:"error",msg:"Could not save to database..."}';
			}
		}
	}

	function activateDeactivatePage(){
		if(_checkAuthentication($this)){
			$id=$this->input->post('id');
			$res=$this->pages->get($id);
			if($res["active"]=="0"){
				$new_value="1";
			}else{
				$new_value="0";
			}
			if($this->pages->save($id,array("active"=>$new_value))){
				echo '{"success":"success",successFunction:function(){
					do_activate_page("'.$id.'","'.$new_value.'");
					}}';
			}else{
				echo '{"success":"success",successFunction:function(){
					alert("Could not toggle visiblity due to server error...<br />Please try again");
					}}';
			}
		}
	}


	function checkStubDuplication(){
		$stub=$this->input->post('stub');
		$rel=$this->input->post('rel');
		if(trim($stub!="")){
			if($this->_utype == 'admin'){
				$st=$this->pages->getBy("stub",$stub);
			} else {
				$st=$this->pages->getstubAvailabe($this->_userid,$stub);
			}
			
			if($rel!="undefined" && trim($rel)!=""){
				if(is_array($st)){
					foreach ($st as $v) {
						if($v["id"]!=$rel){
							echo "<span title='Stub Exists'><img src='".base_url()."assets/images/admin/icon_error.png'  border='0' /></span>";
							exit;
						}
					}
				}
				echo "<span title='Stub is Unique'><img src='".base_url()."assets/images/admin/icon_success.png'  border='0' /></span>";
			}else{
				if(isset($st[0]["id"])){
					echo "<span title='Stub Exists'><img src='".base_url()."assets/images/admin/icon_error.png'  border='0' /></span>";
				}else{
					echo "<span title='Stub is Unique'><img src='".base_url()."assets/images/admin/icon_success.png'  border='0' /></span>";
				}
			}

		}
		echo "";
		exit;
	}

	function XautoSave(){
	  $user_id=$this->_userid;
	  $page_id=$this->input->post('page_id');
		$content=$this->input->post("content");
		$this->pages->autoSave($page_id,$user_id,$content);
		echo "{success:'success'}";
	}

	function XgetAutoSave($page_id){
    $user_id=$this->_userid;
    $res=$this->pages->getAutoSaved($page_id,$user_id);
    if(is_array($res) && isset($res["id"])){
      echo '{data:"'.str_replace(array("\r","\t","\n"),"",str_replace(array("'",'"'),"\'",$res["content"])).'"}';
    }else{
      echo '{data:false}';
    }
	}

	function XkeepAlive(){
		echo '{alive:true}';
	}

	function _resizePicture($filename="",$max_width=513,$max_height=219){
    $dir=$this->pages->_repo;
    if(file_exists($dir.$filename)){
      $ext=_file_extension($filename);
      list($width_orig, $height_orig)=getimagesize($dir.$filename);
      if(($width_orig==$max_width) &&  ($height_orig==$max_height)){
        return;
      }
      $sc=min(($max_width / $width_orig), ($max_height / $height_orig));
      if($sc < 1){
        $width=round($width_orig*$sc);
        $height=round($height_orig*$sc);
      }else{
        $width=$width_orig;
        $height=$height_orig;
      }


      $image_p = imagecreatetruecolor($width, $height);
      if($ext["extension"]==".gif"){
        $image = imagecreatefromgif($dir.$filename);
      }elseif($ext["extension"]==".png"){
        $image = imagecreatefrompng($dir.$filename);				
      }else{
        $image = imagecreatefromjpeg($dir.$filename);
      }
      imagecopyresized($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

      $image_p2 = imagecreatetruecolor($max_width, $max_height);
      $base_color = imagecolorallocate($image_p2, 236, 236, 236);
      imagefill($image_p2, 0, 0, $base_color);
      imagecopy($image_p2, $image_p, (($max_width-$width)/2), (($max_height-$height)/2), 0, 0, $width, $height);

      $newFilename=$ext["filename"].$ext["extension"];
      if($ext["extension"]==".gif"){
        imagegif($image_p2, $dir.$newFilename, 100);
      }else{
        imagejpeg($image_p2, $dir.$newFilename);
      }

      //Free up resources
      ImageDestroy($image_p);
      ImageDestroy($image_p2);

      return;

    }
  }

}


?>
