<?php

namespace entities;

/**
 * ResProductAddon
 *
 * @Table(name="res_product_addon")
 * @Entity
 */
class ResProductAddon
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResProduct
   *
   * @ManyToOne(targetEntity="ResProduct")
   * @JoinColumns({
   *   @JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $product;
  
  /**
   * @var ResAddon
   *
   * @ManyToOne(targetEntity="ResAddon")
   * @JoinColumns({
   *   @JoinColumn(name="addon_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $addon;

  /**
   * @var bigint $restaurantId
   *
   * @Column(name="restaurant_id", type="bigint", nullable=true)
   */
  private $restaurantId;
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set product
   *
   * @param ResProduct $product
   */
  public function setProduct(\ResProduct $product)
  {
    $this->product = $product;
  }

  /**
   * Get product
   *
   * @return ResProduct 
   */
  public function getProduct()
  {
    return $this->product;
  }
  
  /**
   * Set addon
   *
   * @param ResAddon $addon
   */
  public function setAddon(\ResAddon $addon)
  {
  	$this->addon = $addon;
  }
  
  /**
   * Get addon
   *
   * @return ResAddon
   */
  public function getAddon()
  {
  	return $this->addon;
  }
  
  /**
   * Set restaurantId
   *
   * @param bigint $restaurantId
   */
  public function setRestaurantId($restaurantId)
  {
  	$this->restaurantId = $restaurantId;
  }
  
  /**
   * Get restaurantId
   *
   * @return bigint
   */
  public function getRestaurantId()
  {
  	return $this->restaurantId;
  }
}