<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articles_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->tablename="app_blog_articles";
		$this->versions_tablename="app_blog_article_versions";
		$this->autosave_tablename="app_blog_article_autosave";
	}

	function countActive(){
		return $this->db->where("active",1)->from($this->tablename)->count_all_results();
	}

	function getRecent($num=5,$offset=0){
		return $this->db->where("active",1)->order_by("created","desc")->limit($num,$offset)
			->get($this->tablename)->result_array();
	}

	function getArchive(){
		return $this->db->query("SELECT count(*) as count,MONTH(created) as month,YEAR(created) as year FROM `$this->tablename` where `active`=1 group by month(created) order by year desc,month desc")->result_array();
	}

	function getByArchiveDate($y,$m){
		return $this->db->query("SELECT * FROM `$this->tablename` where `active`=1 and month(created)='$m' and year(created)='$y'")->result_array();
	}

	function autosave($page_id,$user_id,$content){
	  $res=$this->getAutoSaved($page_id,$user_id);
	  if(is_array($res) && isset($res["id"])){
	    $arr=array(
  			"modifieddate"=>time(),
  			"content"=>$content
  			);
  		return $this->db->update($this->autosave_tablename,$arr,array("user_id"=>$user_id,"page_id"=>$page_id));
	  }else{
	    $arr=array(
  			"modifieddate"=>time(),
  			"user_id"=>$user_id,
  			"content"=>$content,
  			"page_id"=>$page_id
  			);
  		return $this->db->insert($this->autosave_tablename,$arr);
	  }

	}

	function getAutoSaved($page_id,$user_id){
	  return $this->db->where("page_id",$page_id)
	  ->where("user_id",$user_id)
		->get($this->autosave_tablename)->row_array();
	}

	function deleteAutoSave($page_id,$user_id){
	  return $this->db->where("page_id",$page_id)
	  ->where("user_id",$user_id)
	  ->delete($this->autosave_tablename);
	}

	function deleteAutoSaves($page_id){
	  return $this->db->where("page_id",$page_id)->delete($this->autosave_tablename);
	}

	function createSnapshot($page_id,$newContent){
		if(trim($newContent)!=""){
			$res=$this->get($page_id);
			if(is_array($res) && isset($res["id"])){
				if($res["content"]!=$newContent && trim($res["content"])!=""){
					$arr=array(
						"modifieddate"=>$res["modifieddate"],
						"modifiedby"=>$res["modifiedby"],
						"content"=>$res["content"],
						"page_id"=>$res["id"]
						);
					$res2=$this->db->insert($this->versions_tablename,$arr);
					return array(
						"version_id"=>$this->db->insert_id(),
						"modifieddate"=>$res["modifieddate"],
						"modifiedby"=>$res["modifiedby"]
						);
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	function getSnapshot($id){
		return $this->db->where("id",$id)
		->get($this->versions_tablename)->result_array();
	}

	function getSnapshots($page_id){
		return $this->db->where("page_id",$page_id)
		->order_by("modifieddate","asc")
		->get($this->versions_tablename)->result_array();
	}

	function deleteSnapshot($id){
		return $this->db->where("id",$id)->delete($this->versions_tablename);
	}

	function deleteSnapshots($page_id){
		return $this->db->where("page_id",$page_id)->delete($this->versions_tablename);
	}

}
?>