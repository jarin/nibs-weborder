

CREATE TABLE IF NOT EXISTS `SS_aff_commissions` (
  `cID` int(11) NOT NULL AUTO_INCREMENT,
  `Amount` float DEFAULT NULL,
  `CurrencyISO3` varchar(3) DEFAULT NULL,
  `xDateTime` datetime DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OrderID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`cID`),
  KEY `CUSTOMERID` (`CustomerID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_aff_payments` (
  `pID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `Amount` float DEFAULT NULL,
  `CurrencyISO3` varchar(3) DEFAULT NULL,
  `xDate` date DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pID`),
  KEY `CUSTOMERID` (`CustomerID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_aux_pages` (
  `aux_page_ID` int(11) NOT NULL AUTO_INCREMENT,
  `aux_page_name` varchar(64) DEFAULT NULL,
  `stub` varchar(255) NOT NULL,
  `aux_page_text` text,
  `aux_page_text_type` int(11) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `sort_order` varchar(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aux_page_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_categories` (
  `categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `stub` varchar(255) NOT NULL,
  `pagetitle` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `products_count` int(11) DEFAULT NULL,
  `description` text,
  `picture` varchar(30) DEFAULT NULL,
  `products_count_admin` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `viewed_times` int(11) DEFAULT '0',
  `allow_products_comparison` int(11) DEFAULT '0',
  `allow_products_search` int(11) DEFAULT '1',
  `show_subcategories_products` int(11) DEFAULT '1',
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`categoryID`),
  KEY `IDX_CATEGORIES1` (`parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_category_product` (
  `productID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  PRIMARY KEY (`productID`,`categoryID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_category_product_options__variants` (
  `optionID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `variantID` int(11) NOT NULL,
  PRIMARY KEY (`optionID`,`categoryID`,`variantID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_category__product_options` (
  `optionID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `set_arbitrarily` int(11) DEFAULT '1',
  PRIMARY KEY (`optionID`,`categoryID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_countries` (
  `countryID` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(64) DEFAULT NULL,
  `country_iso_2` char(2) DEFAULT NULL,
  `country_iso_3` char(3) DEFAULT NULL,
  PRIMARY KEY (`countryID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_countries_copy` (
  `countryID` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(64) DEFAULT NULL,
  `country_iso_2` char(2) DEFAULT NULL,
  `country_iso_3` char(3) DEFAULT NULL,
  PRIMARY KEY (`countryID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_currency_types` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) DEFAULT NULL,
  `code` varchar(7) DEFAULT NULL,
  `currency_value` float DEFAULT NULL,
  `where2show` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `currency_iso_3` char(3) DEFAULT NULL,
  PRIMARY KEY (`CID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_cusomer_log` (
  `customerID` int(11) NOT NULL,
  `customer_ip` varchar(15) DEFAULT NULL,
  `customer_logtime` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_custgroups` (
  `custgroupID` int(11) NOT NULL AUTO_INCREMENT,
  `custgroup_name` varchar(64) DEFAULT NULL,
  `custgroup_discount` float DEFAULT '0',
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`custgroupID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_customers` (
  `customerID` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(32) DEFAULT NULL,
  `cust_password` varchar(255) NOT NULL,
  `Email` varchar(64) DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `hear_about` varchar(255) NOT NULL,
  `subscribed4news` int(11) DEFAULT NULL,
  `custgroupID` int(11) DEFAULT NULL,
  `addressID` int(11) DEFAULT NULL,
  `billingAddressID` int(14) NOT NULL,
  `reg_datetime` datetime DEFAULT NULL,
  `ActivationCode` varchar(16) NOT NULL DEFAULT '',
  `CID` int(11) DEFAULT NULL,
  `affiliateID` int(11) NOT NULL DEFAULT '0',
  `affiliateEmailOrders` int(11) NOT NULL DEFAULT '1',
  `affiliateEmailPayments` int(11) NOT NULL DEFAULT '1',
  `discount_rate` float NOT NULL,
  `discount_type` int(1) NOT NULL DEFAULT '1',
  `delivery_exemption` varchar(255) NOT NULL,
  `min_ord_exemption` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerID`),
  KEY `AFFILIATEID` (`affiliateID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_customer_addresses` (
  `addressID` int(11) NOT NULL AUTO_INCREMENT,
  `customerID` int(11) NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `countryID` int(11) DEFAULT NULL,
  `zoneID` int(11) DEFAULT NULL,
  `zip` varchar(64) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `address` tinytext,
  PRIMARY KEY (`addressID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_customer_reg_fields` (
  `reg_field_ID` int(11) NOT NULL AUTO_INCREMENT,
  `reg_field_name` varchar(32) DEFAULT NULL,
  `reg_field_required` tinyint(1) DEFAULT NULL,
  `reg_field_number` tinyint(1) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`reg_field_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_customer_reg_fields_values` (
  `reg_field_ID` int(11) NOT NULL,
  `customerID` int(11) NOT NULL,
  `reg_field_value` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_customer_reg_fields_values_quickreg` (
  `reg_field_ID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `reg_field_value` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_discount_matrix` (
  `matrix_designation` varchar(255) NOT NULL,
  `value` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `SS_discussions` (
  `DID` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) DEFAULT NULL,
  `Author` varchar(40) DEFAULT NULL,
  `Body` text,
  `add_time` datetime DEFAULT NULL,
  `Topic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_homepage_categories` (
  `id` int(14) NOT NULL AUTO_INCREMENT,
  `categoryID` int(14) NOT NULL,
  `sort_order` int(14) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_linkexchange_categories` (
  `le_cID` int(11) NOT NULL AUTO_INCREMENT,
  `le_cName` varchar(100) DEFAULT NULL,
  `le_cSortOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`le_cID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_linkexchange_links` (
  `le_lID` int(11) NOT NULL AUTO_INCREMENT,
  `le_lText` varchar(255) DEFAULT NULL,
  `le_lURL` varchar(255) DEFAULT NULL,
  `le_lCategoryID` int(11) DEFAULT NULL,
  `le_lVerified` datetime DEFAULT NULL,
  PRIMARY KEY (`le_lID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manuf_name` varchar(255) NOT NULL,
  `manuf_logo` varchar(255) NOT NULL,
  `stub` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `pagetitle` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `sort_order` tinyint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_modules` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `ModuleClassName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_news_table` (
  `NID` int(11) NOT NULL AUTO_INCREMENT,
  `add_date` varchar(30) DEFAULT NULL,
  `title` text,
  `picture` varchar(30) DEFAULT NULL,
  `textToPublication` text,
  `textToMail` text,
  `add_stamp` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `emailed` int(11) DEFAULT NULL,
  PRIMARY KEY (`NID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_ordered_carts` (
  `itemID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `Price` float DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `tax` float DEFAULT NULL,
  `load_counter` int(11) DEFAULT '0',
  PRIMARY KEY (`itemID`,`orderID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_orders` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `customerID` int(11) DEFAULT NULL,
  `order_time` datetime DEFAULT NULL,
  `customer_ip` varchar(15) DEFAULT NULL,
  `shipping_type` varchar(30) DEFAULT NULL,
  `payment_type` varchar(30) DEFAULT NULL,
  `customers_comment` varchar(255) DEFAULT NULL,
  `statusID` int(11) DEFAULT NULL,
  `shipping_cost` float DEFAULT NULL,
  `order_discount` float DEFAULT NULL,
  `order_amount` float DEFAULT NULL,
  `currency_code` varchar(7) DEFAULT NULL,
  `currency_value` float DEFAULT NULL,
  `customer_firstname` varchar(64) DEFAULT NULL,
  `customer_lastname` varchar(64) DEFAULT NULL,
  `customer_email` varchar(50) DEFAULT NULL,
  `shipping_firstname` varchar(64) DEFAULT NULL,
  `shipping_lastname` varchar(64) DEFAULT NULL,
  `shipping_country` varchar(64) DEFAULT NULL,
  `shipping_state` varchar(64) DEFAULT NULL,
  `shipping_zip` varchar(64) DEFAULT NULL,
  `shipping_city` varchar(64) DEFAULT NULL,
  `shipping_address` varchar(64) DEFAULT NULL,
  `billing_firstname` varchar(64) DEFAULT NULL,
  `billing_lastname` varchar(64) DEFAULT NULL,
  `billing_country` varchar(64) DEFAULT NULL,
  `billing_state` varchar(64) DEFAULT NULL,
  `billing_zip` varchar(64) DEFAULT NULL,
  `billing_city` varchar(64) DEFAULT NULL,
  `billing_address` varchar(64) DEFAULT NULL,
  `cc_number` varchar(255) DEFAULT NULL,
  `cc_holdername` varchar(255) DEFAULT NULL,
  `cc_expires` char(255) DEFAULT NULL,
  `cc_cvv` varchar(255) DEFAULT NULL,
  `affiliateID` int(11) DEFAULT '0',
  `shippingServiceInfo` varchar(255) DEFAULT NULL,
  `google_order_number` varchar(50) NOT NULL,
  `internal_order_ref` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `protx_order_ref` varchar(255) NOT NULL,
  `protx_ver_id` varchar(255) NOT NULL,
  PRIMARY KEY (`orderID`),
  KEY `google_order_number` (`google_order_number`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_order_price_discount` (
  `discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `price_range` float DEFAULT NULL,
  `percent_discount` float DEFAULT NULL,
  PRIMARY KEY (`discount_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_order_status` (
  `statusID` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(30) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`statusID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_order_status_changelog` (
  `orderID` int(11) DEFAULT NULL,
  `status_name` varchar(255) DEFAULT NULL,
  `status_change_time` datetime DEFAULT NULL,
  `status_comment` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_payment_types` (
  `PID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `Enabled` int(11) DEFAULT NULL,
  `calculate_tax` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `email_comments_text` text,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`PID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_payment_types__customer_groups` (
  `PID` int(11) NOT NULL,
  `GID` int(11) NOT NULL,
  PRIMARY KEY (`PID`,`GID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_payment_types__shipping_methods` (
  `SID` int(11) NOT NULL,
  `PID` int(11) NOT NULL,
  PRIMARY KEY (`SID`,`PID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_products` (
  `productID` int(11) NOT NULL AUTO_INCREMENT,
  `categoryID` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `stub` varchar(255) NOT NULL,
  `pagetitle` varchar(255) NOT NULL,
  `description` text,
  `customers_rating` float DEFAULT '0',
  `Price` float DEFAULT NULL,
  `in_stock` int(11) DEFAULT NULL,
  `lead_time` varchar(255) NOT NULL,
  `customer_votes` int(11) DEFAULT '0',
  `items_sold` int(11) NOT NULL,
  `enabled` int(11) DEFAULT NULL,
  `brief_description` text,
  `list_price` float DEFAULT NULL,
  `product_code` varchar(25) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `default_picture` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `viewed_times` int(11) DEFAULT '0',
  `eproduct_filename` varchar(255) DEFAULT NULL,
  `eproduct_available_days` int(11) DEFAULT '5',
  `eproduct_download_times` int(11) DEFAULT '5',
  `weight` float DEFAULT '0',
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `free_shipping` int(11) DEFAULT '0',
  `min_order_amount` int(11) DEFAULT '1',
  `shipping_freight` float DEFAULT '0',
  `classID` int(11) DEFAULT NULL,
  `manufID` int(11) DEFAULT '0',
  `current_rating` varchar(255) NOT NULL,
  `enc_material` varchar(255) NOT NULL,
  `num_poles` varchar(255) NOT NULL,
  `fixing_hole` varchar(255) NOT NULL,
  PRIMARY KEY (`productID`),
  KEY `IDX_PRODUCTS1` (`categoryID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_products_opt_val_variants` (
  `variantID` int(11) NOT NULL AUTO_INCREMENT,
  `optionID` int(11) NOT NULL,
  `option_value` varchar(255) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`variantID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_product_attachments` (
  `id` int(14) NOT NULL AUTO_INCREMENT,
  `productID` int(14) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_product_options` (
  `optionID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`optionID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_product_options_set` (
  `productID` int(11) NOT NULL,
  `optionID` int(11) NOT NULL,
  `variantID` int(11) NOT NULL,
  `price_surplus` float DEFAULT '0',
  PRIMARY KEY (`productID`,`optionID`,`variantID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_product_options_values` (
  `optionID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `option_value` varchar(255) DEFAULT NULL,
  `option_type` tinyint(1) DEFAULT '0',
  `option_show_times` int(11) DEFAULT '1',
  `variantID` int(11) DEFAULT NULL,
  PRIMARY KEY (`optionID`,`productID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_product_pictures` (
  `photoID` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) NOT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `thumbnail` varchar(50) DEFAULT NULL,
  `enlarged` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`photoID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_related_items` (
  `productID` int(11) NOT NULL,
  `Owner` int(11) NOT NULL,
  PRIMARY KEY (`productID`,`Owner`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_settings` (
  `settingsID` int(11) NOT NULL AUTO_INCREMENT,
  `settings_groupID` int(11) DEFAULT NULL,
  `settings_constant_name` varchar(64) DEFAULT NULL,
  `settings_value` varchar(2000) DEFAULT NULL,
  `settings_title` varchar(128) DEFAULT NULL,
  `settings_description` varchar(255) DEFAULT NULL,
  `settings_html_function` varchar(255) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`settingsID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
INSERT INTO `SS_settings` (`settingsID`, `settings_groupID`, `settings_constant_name`, `settings_value`, `settings_title`, `settings_description`, `settings_html_function`, `sort_order`) VALUES
(1, 2, 'CONF_SHOP_NAME', 'Nibs Shop', 'Store name', 'The name you input here will be submitted to email notifications sent to customers', 'setting_TEXT_BOX(0,', 2),
(2, 2, 'CONF_DEFAULT_TITLE', 'The nibs Shop', 'Default pages'' title', 'Please input a default title of your store pages', 'setting_TEXT_BOX(0,', 0),
(3, 2, 'CONF_SHOP_URL', 'http://192.168.1.116/KHALED/cms/', 'Store URL', 'Website URL that will be submitted to email messages sent to customers (e.g. order notifications). You may specify any URL here, e.g. www.myshop.com', 'setting_TEXT_BOX(0,', 3),
(4, 6, 'CONF_CHECKSTOCK', '0', 'Stock level control', 'If disabled, product stock information will not be taken into account at all (e.g. will not be updated and verified when customer places an order)', 'setting_CHECK_BOX(', 4),
(5, 4, 'CONF_UPDATE_GCV', '1', 'Automatic update of products count value for categories', 'Recommended to be turned off if number of products in your store is greater than 10000', 'setting_CHECK_BOX(', 6),
(6, 2, 'CONF_DEFAULT_CURRENCY', '3', 'Default currency', '', 'settingCONF_DEFAULT_CURRENCY()', 7),
(7, 2, 'CONF_GENERAL_EMAIL', 'me.khaled@gmail.com', 'Store general email address', 'General store contact email address', 'setting_TEXT_BOX(0,', 8),
(8, 2, 'CONF_ORDERS_EMAIL', 'me.khaled@gmail.com', 'Orders notification email address', 'Order notifications will be sent to this email address', 'setting_TEXT_BOX(0,', 9),
(9, 6, 'CONF_NEW_ORDER_STATUS', '2', 'Status of the new order', 'All new orders will be automatically assigned this status', 'settingCONF_NEW_ORDER_STATUS()', 14),
(10, 6, 'CONF_COMPLETED_ORDER_STATUS', '5', 'Status of the completed order', 'All orders in this status will be assumed as completed', 'settingCONF_COMPLETED_ORDER_STATUS()', 15),
(11, 4, 'CONF_PRODUCTS_PER_PAGE', '20', 'Number of products per page (frontend)', 'number of products presented on the page at a time in the search results and product list', 'setting_TEXT_BOX(2,', 1),
(12, 0, 'CONF_COLUMNS_PER_PAGE', '1', 'Number of columns in the products presentation grid (frontend)', '', 'setting_TEXT_BOX(2,', 2),
(13, 6, 'CONF_SHOW_ADD2CART', '1', 'Enable shopping cart & ordering feature', 'Disable this option if you would like to completely disable checkout and shopping cart facilities', 'setting_CHECK_BOX(', 3),
(14, 3, 'CONF_DARK_COLOR', '82A1DD', 'Color 1', 'used for drawing tables in the store frontend (dark)', 'settingCONF_COLOR(', 5),
(15, 3, 'CONF_MIDDLE_COLOR', 'B2D1F5', 'Color 2', 'used for drawing tables in the store frontend (middle)', 'settingCONF_COLOR(', 6),
(16, 3, 'CONF_LIGHT_COLOR', 'D2E7FF', 'Color 3', 'used for drawing tables in the store frontend (light)', 'settingCONF_COLOR(', 7),
(17, 0, 'CONF_PRODUCT_SORT', '1', 'Allow customers to select products sorting criteria', 'If disabled, product list will be sorted according to "sort order" value; if enabled, customers will be able to select how to sort product list: by name, by price, by customer rating', 'setting_CHECK_BOX(', 8),
(18, 6, 'CONF_COUNTRY', '222', 'Country', 'Country where your store is located (required for shipping rate calculations)', 'settingCONF_COUNTRY()', 9),
(19, 6, 'CONF_ZONE', '', 'State', 'State where your store is located (required for shipping rate calculations)', 'settingCONF_ZONE()', 10),
(20, 0, 'CONF_QUICK_ORDER', '0', 'Allow Quick ordering', '', 'setting_CHECK_BOX(', 11),
(21, 1, 'CONF_DISCOUNT_TYPE', '7', 'Discount type', 'Discount type', 'settingCONF_DISCOUNT_TYPE()', 0),
(22, 6, 'CONF_ORDERING_REQUEST_BILLING_ADDRESS', '0', 'Request billing address', 'Shipping address is requested from customer by default. Enable this option if you would like to request billing address as well', 'setting_CHECK_BOX(', 0),
(23, 5, 'CONF_DEFAULT_COUNTRY', '222', 'Default country', 'default country in the customer registration form', 'settingCONF_DEFAULT_COUNTRY()', 0),
(24, 6, 'CONF_CALCULATE_TAX_ON_SHIPPING', '1', 'Calculate tax on shipping', 'Choose "no" if shipping is not taxable, otherwise select tax class for the shipping rate calculation', 'settingCONF_CALCULATE_TAX_ON_SHIPPING()', 0),
(25, 0, 'CONF_DEFAULT_CUSTOMER_GROUP', '1', 'Default customer group', 'customer group assigned to all new registered customers', 'settingCONF_DEFAULT_CUSTOMER_GROUP()', 0),
(26, 6, 'CONF_DEFAULT_TAX_CLASS', '1', 'Default tax class', 'tax class appended for newly added products by default', 'settingCONF_DEFAULT_TAX_CLASS()', 0),
(27, 0, 'CONF_NEWS_COUNT_IN_CUSTOMER_PART', '5', 'Maximum number of news posts represented on the shopping cart frontend', '', 'setting_TEXT_BOX(2,', 0),
(28, 2, 'CONF_FULL_SHOP_URL', 'http://192.168.1.116/KHALED/cms/shop/', 'Complete URL of your store', 'Specify a complete URL of this installation; including http://, excluding index.php and adding / in the end, e.g. http://www.myshop.com/shop/<br><b><font color=red>In case URL is specified incorrectly your store may work incorrectly</font></b>', 'setting_TEXT_BOX(0,', 0),
(29, 6, 'CONF_PROTECTED_CONNECTION', '0', 'Secure checkout', 'Please enable this checkbox if you would like checkout to be handled in secure SSL mode.<br><b><font color=red>NOTE:</font></b> This option will work ONLY if you have SSL certificate installed for your domain name', 'setting_CHECK_BOX(', 0),
(30, 1, 'CONF_ADDRESSFORM_STATE', '0', 'Address form - state/province field', '', '', 0),
(31, 1, 'CONF_ADDRESSFORM_ZIP', '0', 'Address form - zip', '', '', 0),
(32, 1, 'CONF_ADDRESSFORM_CITY', '0', 'Address form - city', '', '', 0),
(33, 1, 'CONF_ADDRESSFORM_ADDRESS', '0', 'Address form - street address', '', '', 0),
(34, 2, 'CONF_DATE_FORMAT', 'MM/DD/YYYY', 'Date format', 'Please select date format', 'setting_DATEFORMAT(', 0),
(35, 0, 'CONF_HOMEPAGE_META_KEYWORDS', 'Keywords', 'Homepage META Keywords', 'Please input META Keywords which will be published in your shopping cart frontend homepage', 'setting_TEXT_AREA(0,', 0),
(36, 0, 'CONF_HOMEPAGE_META_DESCRIPTION', 'Description', 'Homepage META Description', 'Please input META Description which will be published in your shopping cart frontend homepage', 'setting_TEXT_AREA(0,', 0),
(37, 1, 'CONF_BACKEND_SAFEMODE', '0', 'Safe mode', 'Enable this value (set to 1) to restrict major advantages of administrative mode.', '', 0),
(38, 0, 'CONF_OPEN_SHOPPING_CART_IN_NEW_WINDOW', '0', 'Open shopping cart in new window', '', 'setting_CHECK_BOX(', 0),
(39, 4, 'CONF_WEIGHT_UNIT', 'kg', 'Weight unit', 'Please choose weight unit in which you specify products weight values in your store', 'setting_WEIGHT_UNIT(', 0),
(40, 2, 'CONF_SMARTY_FORCE_COMPILE', '1', 'Force Smarty templates recompillation', 'Highly recommended to be enabled when you redesign your store (when you modify template files); and to be disabled when your store goes live (disabling this option will increase your store performance)', 'setting_CHECK_BOX(', 0),
(41, 1, 'CONF_AFFILIATE_EMAIL_NEW_PAYMENT', '1', 'Email customers when new payment is submitted', '', 'setting_CHECK_BOX(', 0),
(42, 1, 'CONF_AFFILIATE_PROGRAM_ENABLED', '0', 'Affiliate program', 'Is enabled or not', 'setting_CHECK_BOX(', 0),
(43, 1, 'CONF_AFFILIATE_AMOUNT_PERCENT', '10', 'Affiliate comission percent', '', 'setting_TEXT_BOX(0,', 0),
(44, 1, 'CONF_AFFILIATE_EMAIL_NEW_COMMISSION', '1', 'Email customers when new commission is submitted', '', 'setting_CHECK_BOX(', 0),
(45, 4, 'CONF_FULLY_EXPAND_CATEGORIES_IN_ADMIN_MODE', '1', 'Completely expand categories list in products and categories editing windows (in administrative mode)', 'Recommended to be turned off if number of categories in your store is greater than 100. This will increase performance when editing products and categories', 'setting_CHECK_BOX(', 6),
(46, 6, 'CONF_MINIMAL_ORDER_AMOUNT', '20', 'Minimal order amount (in conventional units)', 'Please input positive value or zero. If customer''s order amount will be less than specified value, customer will not be allowed to place this order.<br>Input 0 to disable minimal amount limit', 'setting_TEXT_BOX(2,', 9),
(47, 0, 'CONF_ALLOW_COMPARISON_FOR_SIMPLE_SEARCH', '0', 'Allow comparision for simple search', '', 'setting_CHECK_BOX(', 0),
(48, 5, 'CONF_ENABLE_CONFIRMATION_CODE', '0', 'Enable "Confirmation Code" verification', 'Prompt customer to input a Confirmation Code shown on an image. This facility helps to protect registration and express checkout forms from robots and spammers.', 'setting_CHECK_BOX(', 2),
(49, 5, 'CONF_ENABLE_REGCONFIRMATION', '0', 'Enable customer''s account activation using an Activation Key', 'If enabled, a randomly generated Activation Key is emailed to user after he fills the registration form. User''s account becomes active only after user inputs this key in your store.', 'setting_CHECK_BOX(', 2),
(50, 4, 'CONF_EXACT_PRODUCT_BALANCE', '0', 'Show customers exact products stock information', 'If enabled, customers will be shown exact stock information for all products, e.g. In stock: 34. If disabled, customers will only see whether product is in stock or not, e.g. In stock: yes', 'setting_CHECK_BOX(', 7),
(51, 7, 'GOOGLE_ANALYTICS_ENABLE', '1', 'Enable integration with Google Analytics', '', 'setting_CHECK_BOX(', 1),
(52, 7, 'GOOGLE_ANALYTICS_ACCOUNT', '5030356-5', 'Account number', 'Enter your Google Analytics account number', 'setting_TEXT_BOX(0,', 2),
(53, 7, 'GOOGLE_ANALYTICS_USD_CURRENCY', '3', 'USD currency type', 'Order amount transferred to Google is denominated in USD. Specify currency type in your shopping cart which is assumed as USD (oder amount will be calculated according to USD exchange rate; if not specified exchange rate will be assumed as 1)', 'setting_CURRENCY_SELECT(', 3),
(117, 1, 'CONF_SMSDRIVERCOM_LOGIN_16', '', 'Login', 'Your SMSDriver login', 'setting_TEXT_BOX(0,', 1),
(118, 1, 'CONF_SMSDRIVERCOM_PASSWORD_16', '', 'Password', 'Your SMSDriver password', 'setting_TEXT_BOX(0,', 1),
(119, 1, 'CONF_SMSDRIVERCOM_UNICODE_16', '0', 'Convert message to unicode', 'If enabled message length is limited to 70 characters', 'setting_CHECK_BOX(', 1),
(120, 1, 'CONF_SMSDRIVERCOM_ORIGINATOR_16', '', 'Originator', 'The name of the sender of SMS messages. Max 16 chars for the number of the sender in international format, or max 11 char for a text string (use English letters only)', 'setting_TEXT_BOX(0,', 1),
(77, 1, 'CONF_PAYMENTMODULE_PROTX_VENDORNAME_5', 'craigandderrico', 'Vendor name', 'Please input your Protx account ID (login)', 'setting_TEXT_BOX(0,', 1),
(78, 1, 'CONF_PAYMENTMODULE_PROTX_ENCPASSWORD_5', 'KT2db3uACPVvUdcJ', 'Encryption password', 'Please input your Protx encryption password', 'setting_TEXT_BOX(0,', 1),
(79, 1, 'CONF_PAYMENTMODULE_PROTX_MODE_5', '2', 'Transactions mode', 'Please select transaction mode', 'setting_SELECT_BOX(CProtx::getModeOptions(),', 1),
(122, 2, 'CONF_HOMEPAGE_TEXT', '', 'Introduction text for the shop landing page', 'Use this box to enter a brief introduction text to be displayed on the shop landing page', 'setting_TEXT_AREA(0,', 10),
(123, 8, 'CONF_MANUF_KEYWORDS', 'keywordssdfsd sdfsdf asdfsdf', 'Manufacturer Keywords', 'Keywords for the manufacturers main page', 'setting_TEXT_AREA(0,', 1),
(124, 8, 'CONF_MANUF_DESC', 'Descriptionsdfasdf sdf asd sd', 'Manufacturer Meta-Description', 'Meta-Description for the manufacturers main page', 'setting_TEXT_AREA(0,', 2),
(125, 8, 'CONF_MANUF_TITLE', 'pagetitle', 'Manufacturer Page Title', 'Page title for the manufacturers page', 'setting_TEXT_BOX(0,', 0),
(138, 1, 'CONF_PAYMENTMODULE_OnAccount_VENDORNAME_24', '', '', '', '', 0),
(168, 1, 'CONF_COURIER_ZONE_36', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_36,', 30),
(169, 1, 'CONF_COURIER_RATES_36', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierShippingModule::_settingRates(36,', 40),
(139, 1, 'CONF_COURIER_COUNTRY_25', '222', 'Country', 'Please select country from the list', 'CourierShippingModule::setting_COUNTRY_SELECT(true,', 20),
(140, 1, 'CONF_COURIER_ZONE_25', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_25,', 30),
(141, 1, 'CONF_COURIER_RATES_25', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierShippingModule::_settingRates(25,', 40),
(167, 1, 'CONF_COURIER_COUNTRY_36', '222', 'Country', 'Please select country from the list', 'CourierShippingModule::setting_COUNTRY_SELECT(true,', 20),
(166, 1, 'CONF_COURIER_RATES_35', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierShippingModule::_settingRates(35,', 40),
(164, 1, 'CONF_COURIER_COUNTRY_35', '222', 'Country', 'Please select country from the list', 'CourierShippingModule::setting_COUNTRY_SELECT(true,', 20),
(165, 1, 'CONF_COURIER_ZONE_35', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_35,', 30),
(163, 1, 'CONF_COURIER_RATES_34', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierShippingModule::_settingRates(34,', 40),
(162, 1, 'CONF_COURIER_ZONE_34', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_34,', 30),
(161, 1, 'CONF_COURIER_COUNTRY_34', '222', 'Country', 'Please select country from the list', 'CourierShippingModule::setting_COUNTRY_SELECT(true,', 20),
(170, 1, 'CONF_COURIER_COUNTRY_37', '222', 'Country', 'Please select country from the list', 'CourierShippingModule::setting_COUNTRY_SELECT(true,', 20),
(171, 1, 'CONF_COURIER_ZONE_37', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_37,', 30),
(172, 1, 'CONF_COURIER_RATES_37', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierShippingModule::_settingRates(37,', 40),
(173, 1, 'CONF_COURIER_COUNTRY_38', '222', 'Country', 'Please select country from the list', 'CourierCustgroupShippingModule::setting_COUNTRY_SELECT(true,', 20),
(174, 1, 'CONF_COURIER_ZONE_38', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierCustgroupShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_38,', 30),
(175, 1, 'CONF_COURIER_RATES_38', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierCustgroupShippingModule::_settingRates(38,', 40),
(176, 1, 'CONF_COURIER_COUNTRY_39', '222', 'Country', 'Please select country from the list', 'CourierCustgroupShippingModule::setting_COUNTRY_SELECT(true,', 20),
(177, 1, 'CONF_COURIER_ZONE_39', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierCustgroupShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_39,', 30),
(178, 1, 'CONF_COURIER_RATES_39', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierCustgroupShippingModule::_settingRates(39,', 40),
(184, 1, 'CONF_COURIER_RATES_41', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierCustgroupShippingModule::_settingRates(41,', 40),
(182, 1, 'CONF_COURIER_COUNTRY_41', '222', 'Country', 'Please select country from the list', 'CourierCustgroupShippingModule::setting_COUNTRY_SELECT(true,', 20),
(183, 1, 'CONF_COURIER_ZONE_41', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierCustgroupShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_41,', 30),
(185, 1, 'CONF_COURIER_COUNTRY_42', '222', 'Country', 'Please select country from the list', 'CourierCustgroupShippingModule::setting_COUNTRY_SELECT(true,', 20),
(186, 1, 'CONF_COURIER_ZONE_42', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierCustgroupShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_42,', 30),
(187, 1, 'CONF_COURIER_RATES_42', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierCustgroupShippingModule::_settingRates(42,', 40),
(188, 1, 'CONF_COURIER_COUNTRY_43', '222', 'Country', 'Please select country from the list', 'CourierCustgroupShippingModule::setting_COUNTRY_SELECT(true,', 20),
(189, 1, 'CONF_COURIER_ZONE_43', '0', 'State', 'Select state where the module will function. For any other state this module restricts shipping (simply makes shipping unavailable to any other state)', 'CourierCustgroupShippingModule::setting_ZONE_SELECT(CONF_COURIER_COUNTRY_43,', 30),
(190, 1, 'CONF_COURIER_RATES_43', '', 'Shipping charges', 'Please define "pairs" (order_amount, shipping_charge). Each pair indicates applicable shipping_charge if order amount is lower than specified order_amount. For all orders with amount greater than the maximum provided, shipping charge will be suppressed', 'CourierCustgroupShippingModule::_settingRates(43,', 40);


CREATE TABLE IF NOT EXISTS `SS_settings_groups` (
  `settings_groupID` int(11) NOT NULL AUTO_INCREMENT,
  `settings_group_name` varchar(64) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`settings_groupID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
INSERT INTO `SS_settings_groups` (`settings_groupID`, `settings_group_name`, `sort_order`) VALUES
(1, 'MODULES', 0),
(2, 'General configuration', 1),
(4, 'Catalog', 3),
(5, 'Customers', 4),
(6, 'Shopping cart & ordering', 5);

CREATE TABLE IF NOT EXISTS `SS_shipping_methods` (
  `SID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email_comments_text` text,
  `Enabled` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`SID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_shopping_carts` (
  `customerID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`customerID`,`itemID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_shopping_cart_items` (
  `itemID` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_shopping_cart_items_content` (
  `itemID` int(11) NOT NULL,
  `variantID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_special_offers` (
  `offerID` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`offerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_subscribers` (
  `MID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(50) DEFAULT NULL,
  `customerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`MID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_system` (
  `varName` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_tax_classes` (
  `classID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `address_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`classID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_tax_rates` (
  `classID` int(11) NOT NULL,
  `countryID` int(11) NOT NULL,
  `isGrouped` tinyint(1) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `isByZone` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`classID`,`countryID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_tax_rates__zones` (
  `classID` int(11) NOT NULL,
  `zoneID` int(11) NOT NULL,
  `value` float DEFAULT NULL,
  `isGrouped` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`classID`,`zoneID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_tax_zip` (
  `tax_zipID` int(11) NOT NULL AUTO_INCREMENT,
  `classID` int(11) DEFAULT NULL,
  `countryID` int(11) DEFAULT NULL,
  `zip_template` varchar(255) DEFAULT NULL,
  `value` float DEFAULT NULL,
  PRIMARY KEY (`tax_zipID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS_zones` (
  `zoneID` int(11) NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(64) DEFAULT NULL,
  `zone_code` char(64) DEFAULT NULL,
  `countryID` int(11) DEFAULT NULL,
  PRIMARY KEY (`zoneID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SS__courier_custgroup_rates` (
  `module_id` int(10) unsigned NOT NULL,
  `orderAmount` float DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `isPercent` tinyint(1) DEFAULT NULL,
  `custgroupID` int(11) DEFAULT NULL,
  KEY `module_id` (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `SS__courier_rates` (
  `module_id` int(10) unsigned NOT NULL,
  `orderAmount` float DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `isPercent` tinyint(1) DEFAULT NULL,
  KEY `module_id` (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;