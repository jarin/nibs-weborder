                    <!-- Side-panel begin -->
					<div class="col-md-3 col-sm-12 col-xs-12 col-md-pull-0">
					    <div class="left-side-panel">
						<div class="side-panel">
							
								<h6 class="toggle-main-title">Side Panel</h6>
								<?php if(!in_array($method,array('checkout','confirm'))) {?>
								<div class="my-check-right" style="border:0px;">
									<h5 class="">My Check</h5>
									<div class=" ">
										<div class="mycart">
										    <?php if($this->session->userdata('cart') !=''){
										            $this->load->view('cart');
										        ?>
										    <?php } else {?>
										        <p style="text-align: center;">You do not have any Items</p>
										    <?php } ?>
										</div>

										
									</div>
									<!--end .slide-toggle -->
								</div>
								<?php } ?>
								<!-- end .sd-side-panel class -->

								<div class="search-keyword">
								    <form class="default-form searchForm" action="order/index" method="POST" role="form">
    									<input type="text" placeholder="Search by keyword" id="search" name="search" value="<?php echo ($this->input->post('search'))?$this->input->post('search'):''?>">
    									<button type="submit" value="submit" class="search"><i class="fa fa-search"></i></button>
									</form>
									
							        <!-- end form -->
								</div>
								<!-- end .search-keyword -->
							
							
						</div>
						</div>
						<!-- end side-panel -->
					</div>
					<!--end .col-md-3 -->
					
