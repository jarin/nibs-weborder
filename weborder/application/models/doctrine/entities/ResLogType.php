<?php



namespace entities;

/**
 * ResLogType
 *
 * @Table(name="res_log_type")
 * @Entity
 */
class ResLogType
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $logType
   *
   * @Column(name="log_type", type="string", length=45, nullable=true)
   */
  private $logType;
  
  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set logType
   *
   * @param string $logType
   */
  public function setLogType($logType)
  {
    $this->logType = $logType;
  }

  /**
   * Get logType
   *
   * @return string 
   */
  public function getLogType()
  {
    return $this->logType;
  }
}