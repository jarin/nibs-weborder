<div id='main-content' class='onepane'>
        <div class='container-fluid' style=''>
          <div class='row-fluid'>
            <div class='span12'>	
			<div class=''>
				<h3 class='page-title'>Business Profile</h3><br />
				<div id='form_wizard_1' class='widget box'>
		         
		        <div class='widget-body form'>
				<div style='margin:0 auto;width:60%;text-align:center'>
				<form method='post' rel='ajaxedUpload' action='<?=base_url()?>admin/gateway/save' autocomplete='off'>
					<table cellpadding='4' cellspacing='0' align='center'>
					
						<?php if(count($gateway) > 0) {
							foreach ($gateway as $key=>$val){?>
							
							<tr>
							<?php
							 foreach ($config[$val['gateway']] as $j => $item){
								
								if($j == $val['field_name'] ){
									
								
								?>
									<th align='left' ><?=$item['FriendlyName']?></th>
									
									<td >
									<?php if($item['Type'] == 'text') { ?>
										<input type='text' name="gateway[<?=$j?>]" value="<?=$val['value']?>"/>
									<?php } ?>
									
									<?php if($item['Type'] == 'password') { ?>
										<input type='password' name="gateway[<?=$j?>]" value="<?=$val['value']?>"/>
									<?php } ?>
									
									<?php if($item['Type'] == 'dropdown') {  ?>
										<select name="gateway[<?=$j?>]" class="form-control">
                                       
                                        <?php
                                       
                                        $options = $config[$val['gateway']][$j]['Options'];
                                        foreach ($options as $opt){?>
                                            <option value="<?=$opt?>" <?php if ($val['value']==$opt) echo 'selected="selected"';  ?>><?=$opt?></option>
                                        <?php } ?>
                                    </select>
									<?php } ?>
									</td>
						
								<?php 
								}	
								
							}?>
						</tr>
						<?php } } ?>
						
						<tr>
							<td align='right' colspan='4'>
							<button type='submit' class='button save'>Save Changes</button>
							
							</td>
						</tr>
					</table>
				</form></div>
			</div>
			</div></div>
			</div>
			</div>
			</div>
			</div>