<?php $baseUrl=base_url() ?>

<?php if(!isset($noGuestbookContent) || $noGuestbookContent!=true) {?>	
    <div class="aboutGuestbook" style='margin-top:50px'>
    	<div class="row">
            <?=_get_gb_Entry($this)?>
        	
        </div>
    </div><!-- .aboutGuestbook -->
    <?php } ?>
    
    
    
<?php if(!isset($noHomeGallery) || $noHomeGallery!=true) {?>
	<div id="carouselArea"  style='width:100%'>
		<div data-ride="carousel" class="carousel slide" id="myCarousel" style='width:100%'>

			<div class="carousel-inner" style='width:100%'>
                <?=_buildBanners($this)?>
			</div>
		</div>
<?php } ?>  
        

    
    </div><!-- #pageContainer -->

    
    
    
    
    
    
    
    <div class="container" id="itemsArea">
    	<div class="row">
          <div class="col-xs-12 col-sm-3">
          	<div class="boxMenu">
            	<span class="boxBtn"><span>Menu</span></span>
                <div class="imgArea"><img src="<?=$baseUrl?>assets/images/common/box/img_1.jpg" alt="" class="img-responsive"></div>
                <div class="text">
                    <strong style='width:150px;display:inline-block'><?=_showSettingsVal($this,'menuContent')?></strong>
                </div>
                <a href='<?=$baseUrl?>menu' class="boxBtn">View Menu</a>
            </div><!-- .boxMenu -->
          </div>
          <div class="col-xs-12 col-sm-3">
          	<div class="boxHealth">
            	<span class="boxBtn"><span>Health Options</span></span>
                <div class="imgArea"><img src="<?=$baseUrl?>assets/images/common/box/img_2.jpg" alt="" class="img-responsive"></div>
                <?=_getRecentHealthyOptionData($this)?>
            </div><!-- .boxMenu -->
          </div>
        
          <div class="col-xs-12 col-sm-3">
          	<div class="boxOffers">
            	<span class="boxBtn"><span>Our Offers</span></span>
                <div class="imgArea"><img src="<?=$baseUrl?>assets/images/common/box/img_3.jpg" alt="" class="img-responsive"></div>
                
                    <?=_getRecentOffersData($this)?>
                	
            </div><!-- .boxMenu -->
          </div>
          <div class="col-xs-12 col-sm-3">
          	<div class="boxReservations">
            	<span class="boxBtn"><span>Contact Us</span></span>
                <div class="imgArea"><img src="<?=$baseUrl?>assets/images/common/box/img_4.jpg" alt="" class="img-responsive"></div>
                <div class="text">
                    <strong style="font-size:24px"><?=strtoupper(_showSettingsVal($this,'contactUsContent'));?></strong>
                    
                </div>
                <a href="<?=$baseUrl?>contact" class="boxBtn">VIEW MORE</a>
            </div><!-- .boxMenu -->
          </div>
        </div>
    </div>
    
    
    
    
    
<?php if(!isset($noHomeForm) || $noHomeForm!=true) {?>	    
    <div id="formContainer">
    	<div class="container">
        	<div class="pull-right formArea" id='signup_form'>
            	<div class="header">
                	<strong>SIGN UP</strong><br>FOR A CHANCE OF<br>
                    <span><strong>WINNING YOUR<br>NEXT MEAL FREE</strong></span>
                </div><!-- .header -->
                
                
                
                <form method='post' action='#' id='signingForm'>
                	<div>
                        <div class='confirmation_msg'>
                            <p id='confirmed' style='padding-left:3px;background:transparent'>
                              Thank you for signing up to Bombay Spice<br />We will contact to you as soon as possible
                            </p>
                            <p id='not_confirmed' style='padding-left:3px;background:#FFCC8F'>
                              Sorry, your enquiry could not be sent due to an internal error. 
                              <br />Please try again...
                            </p>
                      </div>
                      <div  class='error_container' style=''>
                            <p>
                              <em id='msg_error' class='error'>Please fill up the enquiry field</em>
                              <em id='name_error' class='error'>Name is Required</em>  
                              <em id='email_error' class='error'>Email is Required</em> 
                              <em id='email_validation_error' class='error'>Invalid Email Address</em> 
                            </p>
                      </div>
                      
                    	Name<br><input type="text" id='name' name="name"><br>
                        <input type="text" name="last_name" style="display:none">
                        Email<br><input type="text" id='email' name="email"><br>
                        Mobile<br><input type="text" value="" name="mobile"><br>
                    </div>
                    <input type="submit" value="CLICK TO SIGN UP" class="btn-block webformBtn">
                </form>
            </div><!-- .formArea -->
        </div>
    </div><!-- #formContainer -->
    
   <?php } ?> 
    
    
    
    
    
    
</div><!-- #mainContent -->
<div id="footerArea">
	<div class="container">
        
<?php if(!isset($noAbtContent) || $noAbtContent!=true) {?>	            
    	<div class="row">
        	<div class="col-xs-12 col-sm-3">
            	<h2>About Bombay Spice</h2>
                    <?=_getAboutUsContent($this);?>
                </p>
            </div>
        
<?php } ?>       
<?php if(!isset($noContentForm) || $noContentForm!=true) {?>	     
    	<div class="row">
        	<div class="col-xs-12 col-sm-3">
            	<div class="formBox" id="signup_form"> 
                	<span class="heading">
                    	<span style="color:#ffa545"><strong>sign up</strong> for a chance<br> of</span><strong> winning your<br> next meal free</strong>
                    </span>
                    
                    
                    <form method='post' action='#' id='signingForm'>
                	<div>
                        <div class='confirmation_msg'>
                            <p id='confirmed' style='padding-left:3px;background:transparent'>
                              Thank you for signing up to Bombay Spice<br />We will contact to you as soon as possible
                            </p>
                            <p id='not_confirmed' style='padding-left:3px'>
                              Sorry, your enquiry could not be sent due to an internal error. 
                              <br />Please try again...
                            </p>
                      </div>
                      <div>
                            <p>
                              <em id='msg_error' class='error'>Please fill up the enquiry field</em>
                              <em id='name_error' class='error'>Name is Required</em>  
                              <em id='email_error' class='error'>Email is Required</em> 
                              <em id='email_validation_error' class='error'>Invalid Email Address</em> 
                            </p>
                      </div>
                      
                    	Name<br><input type="text" id='name' name="name"><br>
                        <input type="text" name="last_name" style="display:none">
                        Email<br><input type="text" id='email' name="email"><br>
                        Mobile<br><input type="text" value="" name="mobile"><br>
                    </div>
                    <input type="submit" value="CLICK TO SIGN UP" class="btn-block webformBtn">
                </form>
                </div><!-- .formBox -->
            </div>
            <?php } ?>




        
        
    
                <div class="col-xs-12 col-sm-5">
                	<h2>Coming Events</h2>
                    <?=_getRecentEventsData($this)?>
                </div>
                <div class="col-xs-12 col-sm-4">
                	<h2>Opening Hours</h2>
                    <table class="opening">
                        <?=_getOpeninhHoursData($this)?>
                    </table>
                	<a href="<?=$baseUrl?>opening_hours" class="viewMore">View more...</a>
                </div>
            </div><!-- .row -->
        </div><!-- .container -->
        <div class="bottom">
        	<div class="container">
                <p>
                    <a href="#">Bombay Spice</a> 36 High Street, Woodville, Derby DE11 7EA<br>
                    Copyright Bombay Spice 2013
                </p>
                <a style="float:right; display:inline-block" target="_blank" href="https://www.facebook.com/BombaySpiceGrill"><img src="<?=$baseUrl?>assets/images/common/fb_find_footer.png" alt=""></a>
            </div>
            
        </div><!-- .bottom -->
    </div><!-- #footerArea -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=$baseUrl?>assets/js/bootstrap.min.js"></script>
    
    

    <div class='popup_window'>
    	<div class='popup_msg'></div>
    	<a href='#' class='close'>CLOSE</a>
    </div>
    <div id='mask'></div>
    
  </body>
</html>
