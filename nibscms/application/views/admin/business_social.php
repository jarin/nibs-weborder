

<div id='main-content' class='onepane'>
	<div class='container-fluid' style=''>
		<div class='row-fluid'>
			<div class='span12'>	
				<div class=''>
					<h3 class='page-title'>Social Information</h3><br />
					<div id='form_wizard_1' class='widget box'>

						<div class='widget-body form'>
							<div style='margin:0 auto;width:60%;text-align:center'>
								<form method='post'  action='<?=base_url()?>admin/social/saveinfo'>
									<table cellpadding='4' cellspacing='0' align='center'>
										<tr>
											<th align='left' >Facebook url</th>
											<td ><input type='text' name='fb_url' value="<?php if(!empty($social[0]['fb_url'])) echo $social[0]['fb_url']; ?>" id='edit_fb_url' /></td>
										</tr>
										<tr>
											<th align='left' >Twitter url</th>
											<td ><input type='text' name='twitter_url' value='<?php if(!empty($social[0]['twit_url'])) echo $social[0]["twit_url"]; ?>' id='edit_twit_url' /></td>
										</tr>
										<tr>
											<th align='left' >Google plus url</th>
											<td ><input type='text' name='google_plus' value='<?php if(!empty($social[0]['google_url'])) echo $social[0]["google_url"]; ?>' id='google-plus_url' /></td>
										</tr>
									
										<tr>
											<th align='left' >Linkedin url</th>
											<td ><input type='text' name='linkedin' value='<?php if(!empty($social[0]['linkedin_url'])) echo $social[0]["linkedin_url"]; ?>' id='linkedin_url' /></td>
										</tr>
										<tr>
											<th align='left' >Pinterest url</th>
											<td ><input type='text' name='pinterest' value='<?php if(!empty($social[0]['pinterest_url'])) echo $social[0]["pinterest_url"]; ?>' id='pinterest_url' /></td>
										</tr>
										<tr>										
											<td align='right' colspan='4'>
												<button type='submit' class='button save'>Save Changes</button>
												<div id='loader<?=$business['id']?>' class='q_feedback'></div>
												<input type='hidden' name='b_id' value='<? if(!empty($business_id)) echo $business_id; ?>' />
												<input type='hidden' name='social_id' value='<? if(!empty($social[0]["id"])) echo $social[0]["id"]; ?>' />
											</td>
										</tr>
									</table>
								</form></div>
							</div>
						</div></div>
					</div>
				</div>
			</div>
		</div>