<?php

function uncamelize($camel,$splitter=" ") {
	$camel=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter.'$0', $camel));
	//$uncamel = strtolower( join($splitter, $matches[0]) );
	return $camel;
}

function singular($str){
  $str = trim($str);
  $end = substr($str, -3);

      $str = preg_replace('/(.*)?([s|c]h)es/i','$1$2',$str);

  if (strtolower($end) == 'ies'){
    $str = substr($str, 0, strlen($str)-3).(preg_match('/[a-z]/',$end) ? 'y' : 'Y');
  }elseif (strtolower($end) == 'ses'){
    $str = substr($str, 0, strlen($str)-2);
  }else{
    $end = strtolower(substr($str, -1));
    if ($end == 's'){
      $str = substr($str, 0, strlen($str)-1);
    }
  }

  return $str;
}

function plural($str, $force = true){
      $str = trim($str);
  $end = substr($str, -1);

  if (preg_match('/y/i',$end)){
    // Y preceded by vowel => regular plural
    $vowels = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');
    $str = in_array(substr($str, -2, 1), $vowels) ? $str.'s' : substr($str, 0, -1).'ies';
  }elseif (preg_match('/h/i',$end)){
    if(preg_match('/^[c|s]h$/i',substr($str, -2))){
      $str .= 'es';
    }else{
      $str .= 's';
    }
  }elseif (preg_match('/s/i',$end)){
    if ($force == TRUE){
      $str .= 'es';
    }
  }else{
    $str .= 's';
  }

  return $str;
}



?>