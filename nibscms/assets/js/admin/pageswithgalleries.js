$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addPageBtn").overlay({width:"30%",inline:true,title:"Add Page",href:"#addPageForm"});

  //checkboxes
  $("#sideBar ul.list li input[type=checkbox]").live("click",function(){
    activate_Page($(this));
    return false;
  });

  // See More details on click
  $("#sideBar ul.list li").live("click",function(){
    var _li=$(this);
    _li.siblings().removeClass("active");
    _li.addClass("active");
    loadPageDetails(_li.attr("id"));
    return false;
  });

  //resize MCE editor as soon as it is shown
  $("#innerTabs li").live("click",function(){
    var _this=$(this);
    if (_this.index()==1){
      resizeEditor();
    }
  });

  //sliding panel
  $("#slidingPanel div.name").live("click",function(){
    $(this).parent().toggleClass("showing")
  });
  $("#slidingPanel ul.reorderable").sortable({
    items:"li:not(.adder)",
	  axis:"x",
	  update:function(){
	    NIBS.util.ajax($(this).sortable("serialize"),"admin/"+$(this).attr("rel"));
	  }
	});
	$("#slidingPanel li a.delete").live("click",function(){
		$(this).blur();
		if(confirm("Are you sure?")){
			NIBS.util.ajax({id:$(this).attr("elId")},"admin/"+$(this).attr("rel"));
		}
		return false;
	});
	$("#slidingPanel li input[type=checkbox]").live("click",function(){
		activate_Image($(this));
    return false;
	});
	$("#slidingPanel li input[type=text]").live("change",function(){
		var _this=$(this),caption=_this.val();
  	var e=_this.parent().parent().attr("id").match(/(.+)[-=_](.+)/);
  	var id=e[2];

		NIBS.util.ajax({id:id,caption:caption},"admin/pageswithgalleries/changeImageCaption");
	});

  //Autosaves
  $("#loadAutoSave").live("click",function(){
    var pagesWithGallery_id=$("#EditableArea").parent().find("input[name=pagesWithGallery_id]").val(),
    _this=$(this);
    _this.html("Loading... Please wait...").prop("disabled",true).next().prop("disabled",true);
    $.get(base_url+"admin/pageswithgalleries/XgetAutoSave/"+pagesWithGallery_id,function(text){
      var data = eval("(" + text + ")");
      if(data && data.data){
        tinyMCE.execInstanceCommand('EditableArea','mceSetContent',false,data.data);
      }
      var _par=_this.parent().parent()
      _par.addClass("hide");
      setTimeout(function(){
        _par.remove();
      },300);
    })

  });
  $("#discardAutoSave").live("click",function(){
    var _par=$(this).parent().parent();
    _par.addClass("hide");
    setTimeout(function(){
      _par.remove();
    },300);
  });

  //--------DND FILE UPLOAD
	var ddBox=$("#slidingPanel"),ddBoxC=ddBox.find("ul"),
	ddBoxP=$("#slidingPanel li.adder"),
	pbTemplate="<li id='image_{guid}' class='deleted'><div><span class='filename'>{file}</span><span class='bar_holder'><span class='bar'></span></span></div></li>";
  ddBoxP.livequery(function(){
    var _this=$(this);
    _this.overlay({width:"30%",inline:true,title:"Add New Image",href:"#addImageForm"});
    _this.DNDU({url:base_url+"admin/pageswithgalleries/addImageViaDragAndDrop/"+_this.attr("pageID")})
  	.bind("DNDU_drop",function(){
  		_this.removeClass("onDrop");
  	})
  	.bind("DNDU_leave",function(){
  		_this.removeClass("onDrop");
  	})
  	.bind("DNDU_over",function(){
  		_this.addClass("onDrop");
  	})
  	.bind("DNDU_processStart",function(e,guid,filename){
  		_this.after(pbTemplate.replace("{guid}",guid).replace("{file}",filename));
  		_add($("#image_"+guid+""));
  	})
  	.bind("DNDU_uploadProgress",function(e,guid,loaded,total){
  		$("#image_"+guid+" .bar").css(
  			{width:((loaded/total)*100) + "%"}
  			);
  	})
  	.bind("DNDU_uploadEnd",function(e,guid){
  		$("#image_"+guid+" .bar").css(
  			{width:"100%"}
  			);
  	})
  	.bind("DNDU_processEnd",function(e,guid,data){
  		try{var objData= eval( "(" + data + ")" ) || null;}catch(e){};
  		if(objData){
  			if(objData.success=="error"){
  				alert(objData.msg);
  				_remove($("#image_"+guid+""));
  			}else if(objData.success=="success"){
  				if($.isFunction(objData.successFunction)){
  					addFileViaDnDHelper(guid,objData.successFunction);
  				}
  			}
  		}else{
  			alert("Error in response, File(s) may not have been saved");
  			console.log(data);
  			_remove($("#image_"+guid+""));
  		}
  	});
  })

	//--------END DND FILE UPLOAD
});

function addPage(name,id){
  NIBS.notif.notify("success","page added");
  var html="<li id='pagesWithGallery_"+id+"' class='deleted'>"+
  "<span class='name' title='"+name+"'>"+name+"</span>"+
  "<input title='Delete Item' type='image' class='action delete' rel='pageswithgalleries/deletePage' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
  "<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"
  "</li>";

  $("#sideBar ul.list").prepend(html);
  $("#add_page_form").reset();
  setTimeout(function(){
    $("#pagesWithGallery_"+id).removeClass("deleted");
  },100);
}

function loadPageDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
  var id=e[2];
  tinyMCE && tinyMCE.execCommand("mceRemoveControl",false,"EditableArea");
  $("#dContent").load(base_url+"admin/pageswithgalleries/viewPage/"+id,function(){
    $(window).bind("resize",NIBS.util.debounce(resizeEditor));
  });
  loadPageInfo(id);
  $("#innerTabs,#thirdBar").show(0);
  $("#innerTabs li").removeClass("active").eq(0).addClass("active");
  $("#mainContent").addClass("hastabs threepane");
  $("#uploaderPageId").val(id);
  $("#slidingPanel").show(0);
}

function loadPageInfo(id){
  $("#infoPanel").load(base_url+"admin/pageswithgalleries/viewPageInfo/"+id);
  $("#slidingPanel ul").load(base_url+"admin/pageswithgalleries/viewPageImages/"+id);
}

function deletePage(pagesWithGallery_id){
  var el=$("#pagesWithGallery_"+pagesWithGallery_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
    $("#innerTabs,#thirdBar").hide(0);
    $("#mainContent").removeClass("threepane hastabs");
    $(window).unbind("resize");
    $("#slidingPanel").hide(0);
  }
  el.addClass("deleted");
  setTimeout(function(){
    el.remove()
  },500);
  NIBS.notif.notify("success","Page deleted successfully");
}

function activate_Page(el){
  if(el){
    var e=el.attr("id").match(/(.+)[-=_](.+)/);
    var id=e[2];
  }
  NIBS.util.ajax({id:id},"admin/pageswithgalleries/activateDeactivatePage");
}

function do_activate_Page(id,val){
  $("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
  $("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

function resizeEditor(){
  var ifr=$("#EditableArea_ifr"),
  spn=$("#EditableArea_parent");
  ifr.css({height:spn.height()-62});
}

//=============================================

function activate_Image(el){
	if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({"id":id},"admin/pageswithgalleries/activateDeactivateImage");

}

function do_act_deactivateImage(id,val){
	if(val=="0"){
		$("#checkboximage_"+id).prop("checked",false);
	}else{
		$("#checkboximage_"+id).prop("checked",true);
	}
}

function addFile(id,path,caption){
	var html="<li id='image_"+id+"' class='deleted'>\
    <div><img src='"+path+"'/></div>\
    <span class='caption'><input type='text' name='caption' placeholder='Image Caption' value='"+caption+"'/></span>\
    <span class='activator'><input type='checkbox' id='checkboximage_"+id+"' /></span>\
    <a class='delete' rel='pageswithgalleries/deleteImage' elID='"+id+"'></a>\
  </li>";
	$("#slidingPanel li.adder").after(html);
	var _counter=$("#imageCount");
	_counter.html(function(idx,h){
    return parseInt(h,10) + 1
  });
  NIBS.CSSanimate(_counter,"bounce");
	_add($("#image_"+id));
}

function deleteFile(download_id){
  _remove($("#image_"+download_id));
  var _counter=$("#imageCount");
	_counter.html(function(idx,h){
    return parseInt(h,10) - 1
  });
  NIBS.CSSanimate(_counter,"bounce");
}

function addFileViaDnDHelper(guid,fn){
  setTimeout(function(){
    $("#image_"+guid+"").replaceWith(fn.apply());
    var _counter=$("#imageCount");
  	_counter.html(function(idx,h){
      return parseInt(h,10) + 1
    });
    NIBS.CSSanimate(_counter,"bounce");
  },500);
}
function addFileViaDnD(id,path){
  return $("<li id='image_"+id+"'>\
    <div><img src='"+path+"'/></div>\
    <span class='caption'><input type='text' name='caption' placeholder='Image Caption' value=''/></span>\
    <span class='activator'><input type='checkbox' id='checkboximage_"+id+"' /></span>\
    <a class='delete' rel='pageswithgalleries/deleteImage' elID='"+id+"'></a>\
  </li>");
}

function _remove(el){
  el.addClass("deleted");
	setTimeout(function(){
	  el.remove();
	},600);
}
function _add(el){
  setTimeout(function(){
	  el.removeClass("deleted");
	  setTimeout(function(){
	    el.css({zIndex:""});
	  },600);
	},100);
}

(function($){
	var support={"XHRsendAsBinary":false,"XHRupload":false,"fileReader":false,"formData":false};
	var s_xhr=new XMLHttpRequest();
	support.XHRsendAsBinary=("sendAsBinary" in s_xhr);
	support.XHRupload=("upload" in s_xhr);
	try{ // firefox,chrome
		support.fileReader = !!new FileReader();
		support.formData = !!new FormData();
	}catch(e){};

	$.fn.DNDU=function(settings){

		//options
		var opts=$.extend({},{
			url:""
		},settings);

		return this.each(function(){
			var _this=this,$this=$(_this);
			_this.addEventListener("drop", function(e){
				e.preventDefault();
				var data=e.dataTransfer;
				var files=data.files;
				$this.trigger("DNDU_drop");
				new DNDU(_this,files,opts);
				return false;
			}, true);
			_this.addEventListener("dragenter", function(e){
				e.stopPropagation();
				e.preventDefault();
				$this.trigger("DNDU_enter");
				return false;
			}, true);
			_this.addEventListener("dragover", function(e){
				e.stopPropagation();
				e.preventDefault();
				$this.trigger("DNDU_over");
				return false;
			}, true);
			_this.addEventListener("dragleave", function(e){
				$this.trigger("DNDU_leave");
				return false;
			}, true);
		});
	};

	function DNDU(el,files,opts){ //constructor
		this.files=files;
		this.opts=opts;
		this.supports=support;
		this.el=el;
		this.url=this.opts.url+"/";
		var temp=[];
		for(var prop in this.supports){
			temp.push(prop+":"+this.supports[prop]);
		}
		this.url+=temp.join("_");

		this.process(this.files);

	};
	DNDU.prototype.process=function(){
		var _this=this,files=_this.files;
		for(var i=0;i<files.length;i++){
		  if(files[i].fileSize>0 || files[i].size>0){
		    var guid=(new Date()).getTime()+"_"+Math.floor(Math.random()*200);
  			files[i].guid=guid;
  			files[i].isResizable=false;
  			files[i].isResized=false;
  			files[i].fileName=files[i].fileName||files[i].name;
  			files[i].fileSize=files[i].fileSize||files[i].size;

  			$(_this.el).trigger("DNDU_processStart",[files[i].guid,files[i].fileName]);
  			_this.upload(files[i]);
		  }
		}
	};

	DNDU.prototype.upload=function(file){
		var _this=this;
		var headers={};
		if(_this.supports.XHRupload){
			headers={
			"Cache-Control":"no-cache",
			"X-Requested-With":"XMLHttpRequest",
			"X-File-Name":file.fileName,
			"X-File-Size":file.fileSize,
			"Content-Type":"multipart/form-data"};
			_this.send(file,headers);
		}
	};
	DNDU.prototype.send=function(file,headers){
		var _this=this;
		var xhr = new XMLHttpRequest();
		var upload = xhr.upload;
		upload.fileObj = file;
		upload.downloadStartTime = new Date().getTime();
		upload.currentStart = upload.downloadStartTime;
		upload.currentProgress = 0;
		upload.startData = 0;

		//trigger start upload
		$(_this.el).trigger("DNDU_uploadStart",[file.guid,file.fileName]);
		// add listeners
		upload.onprogress=function(e){
			//console.log(e);
			if (e.lengthComputable) {
				$(_this.el).trigger("DNDU_uploadProgress",[file.guid,e.loaded,e.total]);
			}
		};

		upload.onload=function(e){
			//console.log(e);
			$(_this.el).trigger("DNDU_uploadEnd",[file.guid]);
			//file=null;
			//delete(file);
		};

		xhr.onload=function(e){
			if ( e.target.readyState == 4 ) {
			  //console.log(this.responseText);
				$(_this.el).trigger("DNDU_processEnd",[file.guid,this.responseText]);
			}
		};

		if(file.isResized){
			xhr.open("POST", _this.url+"isResized=true",true);
		}else{
			xhr.open("POST", _this.url,true);
		}

		for(var prop in headers){
			xhr.setRequestHeader(prop, headers[prop]);
		}

		xhr.send(file);

	};
})(jQuery);
