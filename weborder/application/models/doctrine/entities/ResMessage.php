<?php



namespace entities;

/**
 * ResMessage
 *
 * @Table(name="res_message")
 * @Entity
 */
class ResMessage
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var ResMessage
   *
   * @ManyToOne(targetEntity="ResMessage")
   * @JoinColumns({
   *   @JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $parentId;

  /**
   * @var datetime $sendDate
   *
   * @Column(name="send_date", type="datetime", nullable=false)
   */
  private $sendDate;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="sender", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $sender;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="receiver", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $receiver;
  
  /**
   * @var string $message
   *
   * @Column(name="message", type="string", length=1000, nullable=false)
   */
  private $message;
  
  /**
   * @var string $subject
   *
   * @Column(name="subject", type="string", length=100, nullable=false)
   */
  private $subject;
  
  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;
  
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set sendDate
   *
   * @param datetime $sendDate
   */
  public function setSendDate($sendDate)
  {
  	$this->sendDate = $sendDate;
  }
  
  /**
   * Get sendDate
   *
   * @return datetime
   */
  public function getSendDate()
  {
  	return $this->sendDate;
  }
  
  /**
   * Set parentId
   *
   * @param ResMessage $parentId
   */
  public function setParentId(\ResMessage $parentId)
  {
  	$this->parentId = $parentId;
  }
  
  /**
   * Get parentId
   *
   * @return ResMessage
   */
  public function getParentId()
  {
  	return $this->parentId;
  }
  
  /**
   * Set sender
   *
   * @param ResUser $sender
   */
  public function setSender(\ResUser $sender)
  {
  	$this->sender = $sender;
  }
  
  /**
   * Get sender
   *
   * @return ResUser
   */
  public function getSender()
  {
  	return $this->sender;
  }
  
  /**
   * Set receiver
   *
   * @param ResUser $receiver
   */
  public function setReceiver(\ResUser $receiver)
  {
  	$this->receiver = $receiver;
  }
  
  /**
   * Get receiver
   *
   * @return ResUser
   */
  public function getReceiver()
  {
  	return $this->receiver;
  }
  
  /**
   * Set message
   *
   * @param string $message
   */
  public function setMessage($message)
  {
  	$this->message = $message;
  }
  
  /**
   * Get message
   *
   * @return string
   */
  public function getMessage()
  {
  	return $this->message;
  }
  
  /**
   * Set subject
   *
   * @param string $subject
   */
  public function setSubject($subject)
  {
  	$this->subject = $subject;
  }
  
  /**
   * Get subject
   *
   * @return string
   */
  public function getSubject()
  {
  	return $this->subject;
  }
  
  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
  	$this->isActive = $isActive;
  }
  
  /**
   * Get isActive
   *
   * @return boolean
   */
  public function getIsActive()
  {
  	return $this->isActive;
  }
}