       <div id="page-content">
       
		 <?php $this->load->view('top_content');?>

			<!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-9 col-sm-12 col-md-push-0">
    				     
				                    <?php if($this->session->userdata('cart') !=''){
                                        ?>
                                        <div class="my-check-right" style="border:0px;">
                                            <h5 class="">My Check</h5>
                                            <div class=" ">
                                        <ul class="list-unstyled">
                                            <li>
                                                <p>
                                                <span>Product Name</span>
                                                <span class="checkoutQty">Quantity</span>
                                                <span class="checkoutPrice">Price</span>
                                                <span class="checkoutTotal">Sub-Total</span>
                                                </p>
                                            </li>
                                            <?php 
                                            $total = 0;
                                            foreach ($this->session->userdata('cart') as $key => $cartItem){
                                            $total = $total + $cartItem['total_price'];
                                             ?>
											<li>
												<p>
												<span><?=($key+1).'. '?></span>
												<?php echo $cartItem['product_name']?>
												<span class="checkoutQty"><?php echo $cartItem['quantity']?>x</span><span class="price checkoutPrice">&pound; <?php echo $cartItem['price']?></span>
												
												<span class="price checkoutTotal">&pound; <?php echo $cartItem['total_price']?></span>
												</p>
											</li>
											<?php } ?>
											<li>
												<!-- list for total price-->
												<p>Sub Total</p>
												<p class="price-total">&pound; <?php echo $total?></p>
											</li>
											<?php if($discount > 0) {?>
											<li>
												<!-- list for total price-->
												<p>Discount</p>
												<p class="price-total">(-)&pound; <?php echo $discount?></p>
											</li>
											<?php }?>
											<li id="deliveryCharge" style="display:none">
											</li>
											<li>
												<!-- list for total price-->
												<p>Total</p>
												<p class="price-total">&pound; <?php echo $total - $discount?></p>
											</li>
										</ul>
								    </div>
                             </div>
                          
                            <form id="confirmorder" method="POST" action="order/confirmorder">
                           		<input type="hidden" name="customer_id" value="<?=$this->session->userdata("userid")?>"/>
                              <div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Order Type</h4>
											<span class="radio-input">
												<input type="radio" id="collection" value="collection" name="type" checked>
												<label for="collection">Collection</label>
											</span>
											<br/>
											<span class="radio-input">
												<input type="radio" id="delivery" value="delivery" name="type" >
												<label for="delivery">Delivery</label>
											</span>
											<p id="minimumvalue" style="color:#E00000"></p>
									    </div>
									</div>
							</div>
							
							<div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Payment Type</h4>
                                         	<span class="radio-input">
												<input type="radio" id="paypal" value="paypal" name="payment" checked>
												<label for="paypal"><i class="fa fa-cc-paypal"></i> Paypal</label>
											</span>
											<br/>
											<span class="radio-input">
												<input type="radio" id="paycollection" value="payonCollection" name="payment" >
												<label for="paycollection"> Pay On Collection</label>
											</span>
											
											<span class="radio-input">
												<input type="radio" id="paydelivery" value="payonDelivery" name="payment" >
												<label for="paydelivery"> Pay On Delivery</label>
											</span>
											
											
									    </div>
									</div>
							</div>
														
                             <div class="page-content">
                                 <div class="contact-us">
                                     <div class="send-message" style="border:0px;">
                                        <h4>Customer Info</h4>
                                       
                                          <div class="row">
                                            <?php if(count($user) > 0){
                                                
                                                ?>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Name*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Name*" name="name" value="<?=$user['customerName']?>" data-rule-required="true"  data-msg-required="Please enter your name">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>House No*:</label>
                                            </div> 
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House No*" name="house_no" value="<?=$user['houseNo']?>" data-rule-required="true"  data-msg-required="Please enter your house no">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>House Name:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House Name" name="house_name" value="<?=$user['houseName']?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Street*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Street*" name="street" value="<?=$user['street']?>" data-rule-required="true"  data-msg-required="Please enter your street">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Town:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Town" name="town" value="<?=$user['town']['cityName']?>" >
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Post Code*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Post Code*" name="post_code" value="<?=$user['postCode']?>" data-rule-required="true"  data-msg-required="Please enter your post code">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country / State*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                             <input type="text" placeholder="Country / State" name="state" value="<?=$user['state']['stateName']?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                             <input type="text" placeholder="Country*" name="country_default" value="<?=$user['country']['countryName']?>" data-rule-required="true"  data-msg-required="Please enter your country">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Phone*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Phone*" name="phone" value="<?=$user['mobile']?>" data-rule-required="true"  data-msg-required="Please enter your phone number">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Email*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6" style="padding-right:0px;">
                                              <input type="email" placeholder="Email*" name="email" value="<?=$user['email']?>" data-rule-required="true" data-rule-email="true" data-msg-required="Please enter your email address" data-msg-email="Please enter a valid email address">
                                            </div>
                                            <?php } ?>
                                          
                                          <!-- end nasted .row -->
                                          <!--  <div class="col-md-6 col-sm-6">
                                                <label>Your message:</label>
                                            </div>
                                        <textarea placeholder="Your message" name="message"></textarea>-->
                                        </div>
                                          
                                        
                                      </div>
                                       <!-- end .send-message -->
                                  </div>
                              </div>
                              
                             <div class="page-content" id="deliveryaddress">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Delivery address</h4>
                                         	<div class="row">
                                         		<div class="col-md-6 col-sm-6">
	                                                <label>Delivery Address:</label>
	                                            </div>
	                                            
	                                        		<textarea placeholder="Order Delivery Address" name="deliverAddress" data-rule-required="true" data-msg-required="Please enter your email address" data-msg-email="Please enter a valid email address"></textarea>
	                                        	
                                         	</div>
									  </div>
								</div>
							</div>
              
							<a class="btn btn-default-black " href="order/confirm" style="padding: 7px 15px;">Back</a>
							<?php
							
							 if(count($ordervalue)>0){
								echo '<input type="hidden" name="collectionminimum" value='.$ordervalue['order_value_collection'].' />
									 <input type="hidden" name="deliveryminimum" value='.$ordervalue['order_value_delivery'].' />';
								
				                }
				             ?>
				             
                    	    <button class="btn btn-default-red" type="submit"  style="float:right;margin-top:15px"><i class="fa fa-money"></i> Confirm</button>
							
						</form>
						
                        <script type="text/javascript">
                                             
                        	/* $("input[name='payment']").change(function(){
                        	var _this=$(this),
                        		payment = _this.val(),
                        		payString = 'payon';//alert(payment);
                        		if(payment.indexOf(payString) != -1){
                        			var ar = payment.split('payon');
                        			if(ar[1]){
                            			
                        				$("input[name='type']").parents().removeClass('active');
                        				
                        				$('#'+ar[1].toLowerCase()).parent().addClass('active');
                        				$('#'+ar[1].toLowerCase()).prop('checked', true);
                        				$("input[name='type']").not($('#'+ar[1].toLowerCase()))
                        	            .prop('checked', false);
                        				
                        			}
                        		}
                        	});*/

                        	$( '.checkbox-input' ).each(function(){
                        		$(this).uouCheckboxInput();
                        	});
                        	 $(document).ready(function() {
                        		 $("input[name='type']").change(function(){
                                  	var _this=$(this),
                                  		orderType = _this.val(),
                                  		mincollection = parseFloat($("input[name='collectionminimum']").val()),
                                  		mindelivery = parseFloat($("input[name='deliveryminimum']").val()),
                                  		total = parseFloat("<?php echo $total?>"),
                                  		row = $("#minimumvalue");
                                  		
                                  		$('#paypal').parent().addClass('active');
     	                             	if(orderType == 'collection'){
     	                             		$('#deliveryaddress').hide(0); 
     	                             		$("#paydelivery").parents().removeClass('active');
         	                             	$('#paycollection').parent().show(0);
         	                             	$('#paydelivery').parent().hide(0);
  	                             			if(mincollection != 'undefined'){
  	                             				if(mincollection > total){
  	     	                         				row.html("You need to spend &pound;"+mincollection+" or more to order for collection");
  	     	                         				$('.btn-default-red').addClass('disabled');
  	     	                                 	}else {
  	     	                                 		$('.btn-default-red').removeClass('disabled');
  	     	                                 		row.html('');
  	     	                                 	} 
  	  	                             		}
     	                         			
     	                         		} else if(orderType == 'delivery'){
         	                         		$('#deliveryaddress').show(0); 
     	                         			$("#paycollection").parents().removeClass('active');
     	                         			$('#paydelivery').parent().show(0);
     	                         			$('#paycollection').parent().hide(0);
         	                         		if(mindelivery != 'undefined'){
         	                         			if(mindelivery > total){ 
         	                         				$('.btn-default-red').addClass('disabled');
         	                         				row.html("You need to spend &pound;"+mindelivery+" or more to order for delivery");
         	                                 	} else {
         	                                 		$('.btn-default-red').removeClass('disabled');
         	                                 		row.html('');
         	                                 	}
             	                         	}
         	                         		$.ajax({
         	                           		async: false, 
         	                           		type: "POST",
         	                           		dataType: "json",
         	                           		url: "order/checkDistance",         	                           		
         	                           		success:function(data){
             	                           		if(data.success == true){
													alert(data.value);
                 	                           		}
             	                           		}
         	                         		});
     	                         		}
                                  	});
                        		  
                        		  $("input[name='type']:first").prop("checked", true).trigger("change");
                        		  
                        		});
                        </script>
							
							<?php } else { ?>
							<p style="text-align: center;">You do not have any Items</p>
							<?php } ?>
										

                           

				     
					</div>
			            <!-- end .col-md-9 -->

					<?php $this->load->view('left_sidebar')?>
				</div>
				<!-- end .row -->
			</div>
			<!--end .container -->