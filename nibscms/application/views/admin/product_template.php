<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/business.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/product.js"></script>


<script type="text/tmpl" id="defaultContent" charset="utf-8">
	<h3><?=$pagename?></h3>
	<div class="initPage">
		<div>
		  <p>Click on the list of product on the left to view details....</p>
		</div>
	</div>
</script> 




<div id="main-content" class=''>
  	<div class="container-fluid">
	    <div id="page" class='site-settings'>
	    	
	    	
	      	<div class="row-fluid" id="dContent">
		 	</div>
	  	</div>
	</div>
</div>


<div id="sidebar" class="nav-collapse collapse">
	<div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
	<div id="sidebarHeading">
		<div class="headertoolbar">
			<span>
				<em>Products</em>
			</span>

	  		<div id="searchCount">0 Filtered Results</div> 
			
		</div>
  	</div> 
  	<ul class="sidebar-menu list">
   	 <?=$list1?>
  	</ul>
  	<div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>
