<div>
<!-- login -->
    <!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-6 col-sm-12 col-md-push-3">
							<div class="page-content">
                                 <div class="contact-us">
                                     <div class="send-message" style="border:0px;">
                                        <h4 style="color:#e00000;">Login</h4>
                                        <?php if($this->session->flashdata("error") !='') echo '<p class="text-danger">'.$this->session->flashdata("error").'</p>';?>
                                       <form method="post" action="auth/doLogin">
                                          <div class="row">
                                            <div class="col-md-12">
                                              <input type="text" name="username" placeholder="Enter Username*">
                                            </div>
                                            <div class="col-md-12">
                                              <input type="password" name="password" placeholder="Enter Password*">
                                            </div>
                                           <input type="hidden" name="uri" value="<?=$uri?>"/>
                                            
                                          </div>
                                          <!-- end nasted .row -->
                                        
                                          <button type="submit"><i class="fa fa-key"></i> Login</button>
                                         
                                          
                                        </form>
                                        
                                        <div class="row">
                                          <div class="col-md-12" style="margin-top:30px;">
                                              <label>New User?</label>
                                              <button onClick="location.href='customer/addCustomer?uri=<?=$uri?>'" class="button"><i class="fa fa-lock"></i> Register</button>
                                           </div>
                                        </div>
                                           
                                      </div>
                                       <!-- end .send-message -->
                                  </div>
                              </div>
                                  <!-- end .page-content -->
                          </div>
                      </div>
                 </div>
                 <!-- end .container -->

<!-- END login -->


