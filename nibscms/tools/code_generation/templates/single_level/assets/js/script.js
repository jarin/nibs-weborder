$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addPageBtn").overlay({width:"30%",inline:true,title:"Add <#=ucfirst(uncamelize($single_item_name))#>",href:"#addPageForm"});

  //checkboxes
  $("#sideBar ul.list li input[type=checkbox]").live("click",function(){
		activate_<#=ucfirst($single_item_name)#>($(this));
		return false;
	});

  // See More details on click
  $("#sideBar ul.list li").live("click",function(){
		var _li=$(this);
		_li.siblings().removeClass("active");
		_li.addClass("active");
		load<#=ucfirst($single_item_name)#>Details(_li.attr("id"));
		return false;
	});

<#if($tinymce){#>	//resize MCE editor as soon as it is shown
	$("#innerTabs li").live("click",function(){
	  var _this=$(this);
	  if (_this.index()==1){
	    resizeEditor();
	  }
	});
<#}#>
<#if($tinymce && $autosave){#>  //Autosaves
  $("#loadAutoSave").live("click",function(){
    var <#=$base_name#>_id=$("#EditableArea").parent().find("input[name=<#=$base_name#>_id]").val(),
    _this=$(this);
    _this.html("Loading... Please wait...").prop("disabled",true).next().prop("disabled",true);
    $.get(base_url+"admin/<#=plural($base_name)#>/XgetAutoSave/"+<#=$base_name#>_id,function(text){
      var data = eval("(" + text + ")");
      if(data && data.data){
        tinyMCE.execInstanceCommand('EditableArea','mceSetContent',false,data.data);
      }
      var _par=_this.parent().parent()
      _par.addClass("hide");
      setTimeout(function(){
    	  _par.remove();
    	},300);
    })

  });
  $("#discardAutoSave").live("click",function(){
    var _par=$(this).parent().parent();
    _par.addClass("hide");
    setTimeout(function(){
  	  _par.remove();
  	},300);
  });<#}#>
});

function add<#=ucfirst($single_item_name)#>(name,id){
	NIBS.notif.notify("success","<#=uncamelize($single_item_name)#> added");
	var html="<li id='<#=$base_name#>_"+id+"' class='deleted'>"+
	"<span class='name' title='"+name+"'>"+name+"</span>"+
	"<input title='Delete Item' type='image' class='action delete' rel='<#=plural($base_name)#>/delete<#=ucfirst($single_item_name)#>' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
	"<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"
	"</li>";

	$("#sideBar ul.list").prepend(html);
	$("#add_page_form").reset();
	setTimeout(function(){
	  $("#<#=$base_name#>_"+id).removeClass("deleted");
	},100);
}

function load<#=ucfirst($single_item_name)#>Details(id){
  var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
<#if($tinymce){#>	tinyMCE && tinyMCE.execCommand("mceRemoveControl",false,"EditableArea");<#}#>
	$("#dContent").load(base_url+"admin/<#=plural($base_name)#>/view<#=ucfirst($single_item_name)#>/"+id<#if($tinymce){#>,function(){
	  $(window).bind("resize",NIBS.util.debounce(resizeEditor));
	}<#}#>);
	load<#=ucfirst($single_item_name)#>Info(id);
	$("#innerTabs,#thirdBar").show(0);
	$("#innerTabs li").removeClass("active").eq(0).addClass("active");
	$("#mainContent").addClass("hastabs threepane");
}

function load<#=ucfirst($single_item_name)#>Info(id){
  $("#infoPanel").load(base_url+"admin/<#=plural($base_name)#>/view<#=ucfirst($single_item_name)#>Info/"+id);
}

function delete<#=ucfirst($single_item_name)#>(<#=$base_name#>_id){
  var el=$("#<#=$base_name#>_"+<#=$base_name#>_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
    $("#innerTabs,#thirdBar").hide(0);
    $("#mainContent").removeClass("threepane hastabs");<#if($tinymce){#>
    $(window).unbind("resize");<#}#>
  }
	el.addClass("deleted");
	setTimeout(function(){
	  el.remove()
	},500);
	NIBS.notif.notify("success","<#=ucfirst(uncamelize($single_item_name))#> deleted successfully");
}

function activate_<#=ucfirst($single_item_name)#>(el){
  if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({id:id},"admin/<#=plural($base_name)#>/activateDeactivate<#=ucfirst($single_item_name)#>");
}

function do_activate_<#=ucfirst($single_item_name)#>(id,val){
  $("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
  $("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

<#if($tinymce){#>function resizeEditor(){
  var ifr=$("#EditableArea_ifr"),
  spn=$("#EditableArea_parent");
  ifr.css({height:spn.height()-62});
}<#}#>
