<?php



namespace entities;

/**
 * ResProductWithWithout
 *
 * @Table(name="res_product_with_without")
 * @Entity
 */
class ResProductWithWithout
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var boolean $isWith
   *
   * @Column(name="is_with", type="boolean", nullable=false)
   */
  private $isWith=TRUE;

  /**
   * @var ResOrderChild
   *
   * @ManyToOne(targetEntity="ResOrderChild")
   * @JoinColumns({
   *   @JoinColumn(name="order_child_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $orderChild;

  /**
   * @var ResProduct
   *
   * @ManyToOne(targetEntity="ResProduct")
   * @JoinColumns({
   *   @JoinColumn(name="product_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $product;

  /**
   * @var ResIngredient
   *
   * @ManyToOne(targetEntity="ResIngredient")
   * @JoinColumns({
   *   @JoinColumn(name="ingredient_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $ingredient;
  
  /**
   * @var string $description
   *
   * @Column(name="description", type="string", length=500, nullable=true)
   */
  private $description;
  
  /**
   * @var decimal $price
   *
   * @Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
   */
  private $price;
  
  /**
   * @var smallint $quantity
   *
   * @Column(name="quantity", type="smallint", nullable=false)
   */
  private $quantity;
  
  /**
   * @var ResQuantityType
   *
   * @ManyToOne(targetEntity="ResQuantityType")
   * @JoinColumns({
   *   @JoinColumn(name="quantity_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $quantityTypeId;


  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set isWith
   *
   * @param boolean $isWith
   */
  public function setIsWith($isWith)
  {
    $this->isWith = $isWith;
  }

  /**
   * Get isWith
   *
   * @return boolean 
   */
  public function getIsWith()
  {
    return $this->isWith;
  }

  /**
   * Set orderChild
   *
   * @param ResOrderChild $orderChild
   */
  public function setOrderChild(\ResOrderChild $orderChild)
  {
    $this->orderChild = $orderChild;
  }

  /**
   * Get orderChild
   *
   * @return ResOrderChild 
   */
  public function getOrderChild()
  {
    return $this->orderChild;
  }

  /**
   * Set product
   *
   * @param ResProduct $product
   */
  public function setProduct(\ResProduct $product)
  {
    $this->product = $product;
  }

  /**
   * Get product
   *
   * @return ResProduct 
   */
  public function getProduct()
  {
    return $this->product;
  }

  /**
   * Set ingredient
   *
   * @param ResIngredient $ingredient
   */
  public function setIngredient(\ResIngredient $ingredient)
  {
    $this->ingredient = $ingredient;
  }

  /**
   * Get ingredient
   *
   * @return ResIngredient 
   */
  public function getIngredient()
  {
    return $this->ingredient;
  }
  
  /**
   * Set description
   *
   * @param string $description
   */
  public function setDescription($description)
  {
  	$this->description = $description;
  }
  
  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
  	return $this->description;
  }
  
  /**
   * Set price
   *
   * @param decimal $price
   */
  public function setPrice($price)
  {
  	$this->price = $price;
  }
  
  /**
   * Get price
   *
   * @return decimal
   */
  public function getPrice()
  {
  	return $this->price;
  }
  
  /**
   * Set quantity
   *
   * @param smallint $quantity
   */
  public function setQuantity($quantity)
  {
  	$this->quantity = $quantity;
  }
  
  /**
   * Get quantity
   *
   * @return smallint
   */
  public function getQuantity()
  {
  	return $this->quantity;
  }
  
  /**
   * Set quantityTypeId
   *
   * @param boolean $quantityTypeId
   */
  public function setQuantityTypeId($quantityTypeId)
  {
  	$this->quantityTypeId = $quantityTypeId;
  }
  
  /**
   * Get quantityTypeId
   *
   * @return boolean
   */
  public function getQuantityTypeId()
  {
  	return $this->quantityTypeId;
  }
}