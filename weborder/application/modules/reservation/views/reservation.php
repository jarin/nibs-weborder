       <div id="page-content">

		<!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-12 col-sm-12 col-md-push-0">
    				       <?php echo validation_errors();?>
                                        
                            <?php if($this->session->flashdata("error") !='') echo '<p class="text-danger">'.$this->session->flashdata("error").'</p>';?>
                                      
                            <form id="confirmorder" method="POST" action="reservation">
                     
                           						
                             <div class="page-content">
                                 <div class="contact-us">
                                     <div class="send-message" style="border:0px;">
                                        <h4>Customer Info</h4>
                                       
                                          <div class="row">
                                            <?php if($this->session->userdata("userid") ==''){?>
                                            <div class="col-md-6 col-sm-6">
                                                <label>User Name*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="User Name*" name="userName" value="<?php echo set_value('userName'); ?>" data-rule-required="true"  data-msg-required="Please enter your user name">
                                            </div>
                                            
                                            <div class="col-md-6 col-sm-6">
                                                <label>Password*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="password" placeholder="Password*" name="password" value="" data-rule-required="true"  data-msg-required="Please enter your password">
                                            </div>
                                            <?php } ?>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Name*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Name*" name="name" value="<?=(!empty($user['customerName'])? $user['customerName']:'')?>" data-rule-required="true"  data-msg-required="Please enter your name">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>House No:</label>
                                            </div> 
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House No" name="house_no" value="<?=(!empty($user['houseNo'])?$user['houseNo']:'')?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>House Name:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="House Name" name="house_name" value="<?=(!empty($user['houseName'])?$user['houseName']:'')?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Street:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Street" name="street" value="<?=(!empty($user['street']) ?$user['street'] :'')?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Town*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Town" name="town" value="<?=(!empty($user['town']['cityName'])?$user['town']['cityName']:'')?>" data-rule-required="true"  data-msg-required="Please enter your town">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Post Code:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Post Code*" name="post_code" value="<?=(!empty($user['postCode'])?$user['postCode']:'')?>">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country / State*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                             <input type="text" placeholder="Country / State*" name="state" value="<?=(!empty($user['state']['stateName'])?$user['state']['stateName']:'')?>" data-rule-required="true"  data-msg-required="Please enter your state">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Country:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                             <input type="text" placeholder="Country" name="country" value="<?=(!empty($user['country']['countryName'])?$user['country']['countryName']:'')?>" data-rule-required="true"  data-msg-required="Please enter your country">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Phone*:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                              <input type="text" placeholder="Phone*" name="phone" value="<?=(!empty($user['mobile'])?$user['mobile']:'')?>" data-rule-required="true"  data-msg-required="Please enter your phone number">
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <label>Email:</label>
                                            </div>
                                            <div class="col-md-6 col-sm-6" style="padding-right:0px;">
                                              <input type="email" placeholder="Email" name="email" value="<?=(!empty($user['email'])?$user['email']:'')?>">
                                            </div>
                                            
                                          
                                          <!-- end nasted .row -->
                                          <!--  <div class="col-md-6 col-sm-6">
                                                <label>Your message:</label>
                                            </div>
                                        <textarea placeholder="Your message" name="message"></textarea>-->
                                        </div>
                                          
                                        
                                      </div>
                                       <!-- end .send-message -->
                                  </div>
                              </div>
                              
                            <div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                          <h4>Reservation</h4>
                                          
                                          <div class="row">
                                          		<div class="col-md-6 col-sm-6">
	                                                <label>Send Confirmation*:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                                <span class="radio-input" style="margin-left:7px;">
														<input type="radio" id="sms" value="sms" name="confirmation" checked>
														<label for="sms">SMS</label>
													</span>
													<div style="clear:both;"></div>
													<span class="radio-input">
														<input type="radio" id="email" value="email" name="confirmation">
														<label for="email">Email</label>
													</span>
													<!-- <div style="clear:both;"></div>
													<span class="radio-input">
														<input type="radio" id="voice_call" value="voice call" name="confirmation">
														<label for="voice_call">Voice Call</label>
													</span> -->
	                                            </div>
	                                            
                                          		<div class="col-md-6 col-sm-6">
	                                                <label>No. of People*:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                             	<input type="text" placeholder="No. of People*" name="people" value="1" data-rule-required="true"  data-msg-required="Please enter No. of People">
	                                            </div>
	                                            <?php if(count($restuarntInfo)> 0){
	                                            	
	                                            ?>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Deposit Amount*:</label>
	                                            </div>
	                                            
	                                            <div class="col-md-6 col-sm-6">
	                                             	<input type="text" placeholder="Deposit Amount*" name="deposit_amount" value="&pound;<?=$restuarntInfo['deposit_amount']?>" style="width:90%;" data-rule-required="true"  data-msg-required="Please enter Deposit Amount" readonly>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                            	<label>Payment Type</label>
	                                            </div>
	                                              <div class="col-md-6 col-sm-6">
		                                         	<span class="radio-input" style="margin-left:7px;">
														<input type="radio" id="paypal" value="paypal" name="payment" checked>
														<label for="paypal"><i class="fa fa-cc-paypal"></i> Paypal</label>
													</span>
													<div style="clear:both;"></div>
													<span class="radio-input">
														<input type="radio" id="payatrestaurant" value="payatrestaurant" name="payment" >
														<label for="payatrestaurant"> Visit Restaurant and Pay</label>
													</span>
													
	                                            </div>
	                                            <?php  } ?>
	                                           
                                        		
                                        		
                                        		<div class="col-md-6 col-sm-6">
	                                                <label>Reservation Date*:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                             	<!--  <div id="datetimepicker" class="input-append">
													    <input data-format="yyyy-MM-dd hh:mm:ss" type="text" style="width: 90%;" data-rule-required="true"  data-msg-required="Please select a reservation time" name="date_time"/>
													    <span class="add-on">
													      <i data-time-icon="fa fa-clock-o" data-date-icon="fa fa-calendar">
													      </i>
													    </span>
													  </div>-->
													   <input class="datepickerF" data-date-format="yyyy-mm-dd" type="text" data-rule-required="true"  data-msg-required="Please select a reservation date" name="resevation_date"/>
	                                            </div>                               
	                                            <?php if(count($reservationTime) > 0){?>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Reservation Time*:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                        		<span class="select-box" title="Reservation Time">
															<select name="reservation_time"  data-rule-required="true"  data-msg-required="Please select a reservation time">
																
																<?php foreach ($reservationTime as $key => $rt){ ?>
																	<option value="<?=date("H:i", $rt['timeFrom']->getTimestamp())?>"><?=date("H:i", $rt['timeFrom']->getTimestamp())?></option>
																	
																<?php } ?>
															</select>
														</span>
                                        		</div>
                                        		<?php } ?>
                                        		<?php if(count($promo) > 0){?>
	                                            <div class="col-md-6 col-sm-6">
	                                                <label>Promotional Code:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                        		<span class="select-box" title="Promotional Code">
															<select name="promotional_Code" data-placeholder="Promotional Code">
																<option>Promotional Code</option>
																<?php foreach ($promo as $key => $pm){ ?>
																	<option value="<?=$pm['promoType']?>"><?=$pm['promoType']?></option>
																<?php } ?>
															</select>
														</span>
                                        		</div>
                                        		<?php } ?>
                                        		 <div class="col-md-6 col-sm-6">
	                                                <label>Special request:</label>
	                                            </div>
	                                            <div class="col-md-6 col-sm-6">
	                                        		<textarea placeholder="Your message" name="request"></textarea>
                                        		</div>
                                            </div>
											
									    </div>
									</div>
							</div>
						
              
							
                    	    <button class="btn btn-default-red" type="submit"  style="float:right;margin-top:15px"><i class="fa fa-money"></i> Confirm</button>
						</form>
										     
					</div>
			            <!-- end .col-md-9 -->

					
				</div>
				<!-- end .row -->
			</div>
			<!--end .container -->
			
			 <script type="text/javascript">
                  $("input[name='confirmation']").change(function(){

                      var _this=$(this),
                      confirmation = _this.val(),
                      emailField = $('input[name=\'email\']'),
                      phoneField = $('input[name=\'phone\']');
                      if(confirmation =='email'){
                      		emailField.attr('data-rule-required',"true");
                      		emailField.attr('data-rule-email',"true");
                      		emailField.attr('data-msg-required',"Please enter your email address");
                      		emailField.attr('data-msg-email',"Please enter a valid email address");
                      	} else {
                      		emailField.attr('data-rule-required','');
                        }
                        });
                        </script>