<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_m extends My_Model {

	function __construct(){
		parent::__construct();
		
		$this->product="res_product";
		$this->order="res_order";
		$this->category="res_category";
		$this->restaurant="restaurant";
		$this->customer="res_customer";
		$this->orderType="res_order_type";
		$this->orderStatus="res_order_status";
		$this->orderChild="res_order_child";
		$this->city="res_city";
		$this->state="res_state";
		$this->country="res_country";
		$this->tablename=$this->order;
	}
	
	// 	function getProducts($order_by="sequence_number",$direction="asc"){
	// 	return $this->db->select("*")
	// 	->order_by($order_by,$direction)->get($this->tablename)->result_array();
	// }
	
	// function getOrders(){	
	//  return  $this->db->select("*,$this->tablename.id")
	// 	->where("$this->orderType.order_type",'Web Order')
	// 	->join($this->customer,"$this->order.customer_id = $this->customer.id","left")
	// 	->join($this->orderType,"$this->order.order_type_id = $this->orderType.id","left")
	// 	->join($this->orderStatus,"$this->order.status_id = $this->orderStatus.id","left")
	// 	->order_by('order_date','asc')->get($this->tablename)->result_array();	
	// }
	
	function getOrder($id){
//        SELECT *
// FROM (
// `res_order`
// )
// LEFT JOIN `res_order_child` AS child ON FIND_IN_SET( child.id, res_order.order_child_ids )
// WHERE `res_order`.`id` = '54'
// ORDER BY `order_date` ASC
// LIMIT 0 , 30
		// this works fine
	 $order_record= $this->db->select("*")
		->where("$this->order.id",$id)
		->get($this->tablename)->result_array();	

    $restaurant_info= $this->db->select("*")
		->where("$this->restaurant.id", $order_record[0]['restaurant_id'])
		->get($this->restaurant)->result_array();
        $child_ids=explode(",", $order_record[0]['order_child_ids']);  

    $orderChild_record= $this->db->select("*")
		->where_in("$this->orderChild.id", $child_ids)
		->get($this->orderChild)->result_array();
   
  //       	 $addon_id= $this->db->select("*")
		// ->where_in("res_order_product_addon.order_child_id", $child_ids)
		// ->get("res_order_product_addon")->result_array();
		// echo $this->db->last_query();

          $product_ids=array();
          foreach ($orderChild_record as  $item) {
          	 $product_ids[]=$item['product_id'];
          }

          $product_info =  $this->db->select("*")
		->where_in("$this->product.id", $product_ids)
		->get($this->product)->result_array();

       $all_info =array();
       $all_info['order'] = $order_record;
       $all_info['restaurant'] = $restaurant_info;
       $all_info['child']=$orderChild_record;
       $all_info['product']=    $product_info;
	
      return $all_info;

       
	}

	function getaddonIdByChild_id($child_id)
	{
		     return $addon_id= $this->db->select("addon_id")
	          ->where("res_order_product_addon.order_child_id", $child_id)
		     ->get("res_order_product_addon")->result_array();

		    
		     

	}

	function getaddonBy_id($addonid)
	{
		     return $addon_id= $this->db->select("*")
	          ->where_in("res_addon.id", $addonid)
		     ->get("res_addon")->result_array();
		  //echo $this->db->last_query();

	}

	function getingredientWithOutByChild_id($child_id)
	{
		     return $ingredient_id= $this->db->select("*")
	          ->where("res_product_with_without.order_child_id", $child_id)
		     ->get("res_product_with_without")->result_array();
	}

	

	function getingredientinfoByid($id)
	{
		    return  $ingredient_id= $this->db->select("*")
	          ->where_in("res_ingredient.id", $id)
		     ->get("res_ingredient")->result_array();
		   
	}

	function getNoteByChildid($id)
	{
		   return $ingredient_id= $this->db->select("*")
	          ->where("res_special_instructions.order_child_id", $id)
		     ->get("res_special_instructions")->result_array();

		  //   echo $this->db->last_query();
		   
	}

	function getweb_business($uid)
	{
		   return $ingredient_id= $this->db->select("*")
	          ->where("$this->restaurant.user_id", $uid)
		     ->get("$this->restaurant")->result_array();

	}
	
	function getOrders($web_id)
	{
		$today_date=date("Y-m-d");
		$datetime = new DateTime('tomorrow');
		$tomorrow_date=$datetime->format('Y-m-d');
		return  $ingredient_id= $this->db->select("*")
		->where("res_order.restaurant_id", $web_id)
		->where('order_date >=', $today_date)
		->where('order_date <', $tomorrow_date)
		->order_by("id", "desc")
		->get("res_order")->result_array();
		    // echo $this->db->last_query();
	}

	function getOrdersByFilter($web_id,$start,$end,$order_type)
	{
		if($order_type!='all'){
			return  $orders= $this->db->select("*")
			->where("res_order.restaurant_id", $web_id)
			->where('order_date >=', $start)
			->where('order_date <', $end)		
			->where('web_order_type', $order_type)
			->order_by("id", "desc")
			->get("res_order")->result_array();

		}
		else{
			return  $orders= $this->db->select("*")
			->where("res_order.restaurant_id", $web_id)
			->where('order_date >=', $start)
			->where('order_date <', $end)		
			->order_by("id", "desc")
			->get("res_order")->result_array();				
		}
		
		// //return $orders;

		// echo '<pre>';		print_r( $orders); echo '</pre>';
		// echo $this->db->last_query();
		// exit;		 
	}

	function customer_infoById($id)
	{
		return  $ingredient_id= $this->db->select("*")
		->where("res_customer.id", $id)
		->get("res_customer")->result_array();
	}
	
	function totalSales($where){
		if(count($where) > 0){
			foreach ($where as $f => $v){
				$this->db->where($f,$v);
			}
		}
		return $this->db->select_sum('sub_total')
		
		->get($this->order)->row_array();
		
	}
	
	function totalNetSales($where){
		if(count($where) > 0){
			foreach ($where as $f => $v){
				$this->db->where($f,$v);
			}
		}
		return $this->db->select_sum('total')
	
		->get($this->order)->row_array();
	
	}
	
	function getOrderByStatus($restaurantId, $statusId, $orderTypeId, $discount)
	{
		
		 $this->db->select("*,$this->order.id as id")
		->where("$this->order.restaurant_id", $restaurantId)
		
		->where('order_type_id', $orderTypeId)
		->join("$this->customer","$this->customer.id = $this->order.customer_id",'left')
		->join("$this->city","$this->customer.town_id = $this->city.id",'left')
		->join("$this->state","$this->customer.state_id = $this->state.id",'left')
		->join("$this->country","$this->customer.country_id = $this->country.id",'left')
		->join("$this->orderStatus","$this->order.status_id = $this->orderStatus.id",'left')
		->order_by("order_date", "desc")
		->limit(15);
		 
		 if($statusId !=0){
		 	$this->db->where('status_id', $statusId);
		 }
		if($discount){
			$this->db->where('discount >', 0);
		}
		 return $this->db->get("$this->order")->result_array();
		 echo $this->db->last_query();
	}
	
}
?>