<?php if(isset($productid)){
  
      $productname = '';
      $productQty = '';
      $productprice = '';
      if($this->session->userdata('cart') !=''){
          foreach ($this->session->userdata('cart') as $cartItem){
              if($cartItem['orderItemId'] == $itemId){
                  $productname = $cartItem['product_name'];
                  $productQty = $cartItem['quantity'];
                  $productprice = $cartItem['price'] * $productQty;
              }
          }
      }
      ?>
            <input type="hidden" name="productid" value="<?php echo $productid;?>"/>
            <input type="hidden" name="itemId" value="<?php echo $itemId;?>"/>
            <input type="hidden" name="productname" value="<?php echo $productname;?>"/>
            <input type="hidden" name="productprice" value="<?php echo $price;?>"/>
            <div class="form-group">
    		    <h3><label>Name:&nbsp;</label><?php echo $productname;?></h3>
    		</div>
    		<br/>
    		<div class="form-group">
    		    <label>Price:&nbsp; &pound;<?php echo $productprice;?></label>		
    		</div>
    		<br/>
    		<div class="form-group">						
		        <label>Quantity:&nbsp;</label><input type="text" name="productQty" class="form-control" value="<?php echo $productQty;?>"/>
		    </div>
			<?php if(count($ingredient)> 0){ ?>
			<div class="col-md-12 col-sm-12" style="padding:0px;">
			<label>Please Select Your Option</label>
			</div>
			<?php if(count($without)> 0){ ?>
			<div class="col-md-6 col-sm-6" style="padding:0px;">
			<label>Without</label>
			<?php foreach ($without as $k=>$val) {
			    if($val['ingredientName'] !='Special Instruction'){ ?>
			
			<?php 
			$cheched = '';
			$cancelIns = false;
			if($this->session->userdata('cart') !=''){
			    foreach ($this->session->userdata('cart') as $cartItem){
                    if($cartItem['orderItemId'] == $itemId){
                        $cancelIns = true;
                        if(isset($cartItem['ingredients']) && count($cartItem['ingredients'])> 0){
                            if(isset($cartItem['ingredients']['without']) && count($cartItem['ingredients']['without']) > 0){
                                foreach ($cartItem['ingredients']['without'] as $v){
                                    if($val['id'] == $v){
                                        $cheched = 'checked';
                                    }
                                }
                               
                            }
                        }
                    }
                }
			}
			?>
			<span class="checkbox-input">
				<input type="checkbox" id="cartwithout_<?php echo $productid;?>_<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="withoutingredient[]" <?php echo $cheched;?>>
				<label for="cartwithout_<?php echo $productid;?>_<?php echo $val['id']?>"><?php echo $val['ingredientName']?><i class="fa fa-plus price">&pound;<?php echo $val['priceWithout']?></i>
				</label>
			</span>
			<?php 
			        }
                 } ?>
         </div>
         <?php 
              } 
              ?>
			<?php if(count($with)> 0){ ?>
			<div class="col-md-6 col-sm-6" style="padding:0px;">
			<label>With</label>
			<?php foreach ($with as $k=>$val) {
                if($val['ingredientName'] !='Special Instruction'){
			    ?>
			   
			<span class="checkbox-input">
			 <?php 
			$cheched = '';
			if($this->session->userdata('cart') !=''){
			    foreach ($this->session->userdata('cart') as $cartItem){
                    if($cartItem['orderItemId'] == $itemId){
                        $cancelIns = true;
                        if(isset($cartItem['ingredients']) && count($cartItem['ingredients'])> 0){
                            if(isset($cartItem['ingredients']['with']) && count($cartItem['ingredients']['with']) > 0){
                                foreach ($cartItem['ingredients']['with'] as $v){
                                    if($val['id'] == $v){
                                        $cheched = 'checked';
                                    }
                                }
                               
                            }
                        }
                    }
                }
			}
			?>
				<input type="checkbox" id="cartwith_<?php echo $productid;?>_<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="withingredient[]" <?php echo $cheched;?>>
				<label for="cartwith_<?php echo $productid;?>_<?php echo $val['id']?>"><?php echo $val['ingredientName']?><i class="fa fa-plus price">&pound;<?php echo $val['price']?></i>
				</label>
			</span>
	
			<?php
                    }
			    } ?>
			</div>
			<?php 
			}
			?>
			<?php if(count($addon)> 0){ ?>
				<div class="col-md-12 col-sm-12" style="padding:0px;">
				<h6>Add-On</h6>
				<?php foreach ($addon as $k=>$val) {
                    
				    $cheched = '';
				    if($this->session->userdata('cart') !=''){
				        foreach ($this->session->userdata('cart') as $cartItem){
				            if($cartItem['orderItemId'] == $itemId){
				               
				                if(isset($cartItem['ingredients']) && count($cartItem['ingredients'])> 0){
				                    if(isset($cartItem['ingredients']['addon']) && count($cartItem['ingredients']['addon']) > 0){
				                        foreach ($cartItem['ingredients']['addon'] as $v){
				                            if($val['id'] == $v){
				                                $cheched = 'checked';
				                            }
				                        }
				                         
				                    }
				                }
				            }
				        }
				    }
				    ?>
				   
				<span class="checkbox-input">
			
					<input type="checkbox" id="addon_<?php echo $productid;?>_<?php echo $val['id']?>" value="<?php echo $val['id']?>" name="addon[]" <?php echo $cheched;?>>
					<label for="addon_<?php echo $productid;?>_<?php echo $val['id']?>"><?php echo $val['addonName']?><i class="fa fa-plus price">&pound;<?php echo $val['webOrderPrice']?></i>
					</label>
				</span>
				
		
				<?php
                        
				    } ?>
				 </div>
				 <?php 
				}
				?>
			<?php 
			$additionalIns = false;
			    if(count($without)> count($with)){
                    $count= count($without);
                } else if(count($with)>= count($without)){
                    $count= count($with);
                }
			    for($i=0; $i<$count; $i++){
                if(isset($without[$i]) || isset($with[$i]))
                {
                    if((isset($without[$i]) && $without[$i]['ingredientName'] == 'Special Instruction') || (isset($with[$i]) && $with[$i]['ingredientName'] == 'Special Instruction'))
                    {
                       $additionalIns = true;         
                    }
                }
                }
			?>
			<?php if($additionalIns){?>
			<div class="form-group">
			    <label>Additional Notes</label>
			 <?php 
			 $instruction ='';
			if($this->session->userdata('cart') !=''){
			    foreach ($this->session->userdata('cart') as $cartItem){
                    if($cartItem['orderItemId'] == $itemId){
                        if(isset($cartItem['ingredients']) && count($cartItem['ingredients'])> 0){
                            if(isset($cartItem['ingredients']['specialInstruction'])){
                                $instruction = $cartItem['ingredients']['specialInstruction'];
                            }
                        }
                    }
                }
			}
			?>
			    <textarea placeholder="Write here" name="additionalNote" class="form-control"><?php echo $instruction;?></textarea>
			</div>
			<?php } ?>
		
		<?php }
}
?>
<script type="text/javascript">
$( '.checkbox-input' ).each(function(){
	$(this).uouCheckboxInput();
});


</script>