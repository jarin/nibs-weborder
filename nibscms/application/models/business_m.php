<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Business_m extends My_Model {

	function __construct(){
		parent::__construct();
		$this->business="restaurant";
		$this->tablename=$this->business;
		$this->user="app_users";
		$this->_repo = "./assets/logo/";
	}
	
	function usernameExists($username){
		$res=$this->db->query("select * from $this->user where  `username`='$username'")->result_array();
		if(isset($res[0]["id"])){
			return $res[0]["id"];
		}else{
			return false;
		}
	}
	
	function getAllbusines($order_by="restaurant.id",$direction="asc"){
		return $this->db->select("*, $this->tablename.id as id, $this->user.id as uid")->group_by("$this->tablename.id")
		->join($this->user,"$this->tablename.user_id = $this->user.id","left")
		->order_by($order_by,$direction)->get($this->tablename)->result_array();
	}
	
	function getBusines($id){
		$business=$this->db->select("*, $this->tablename.id as id, $this->user.id as uid")
		->where("$this->tablename.id",$id)
		->join($this->user,"$this->tablename.user_id = $this->user.id","left")
		->get($this->tablename)->row_array();
	
		return $business;
	}
	
	function deleteBusiness($id){
		$sql = "DELETE b, u FROM $this->tablename b
		  JOIN $this->user u ON b.user_id = u.id
		  WHERE b.id = $id";
		return $this->db->query($sql);
		
	}
	
	function getBusinesByUser($id){
		$business=$this->db->select("*, $this->tablename.id as id, $this->user.id as uid")
		->where("$this->user.id",$id)
		->join($this->user,"$this->tablename.user_id = $this->user.id","left")
		->get($this->tablename)->row_array();
	
		return $business;
	}
}
?>