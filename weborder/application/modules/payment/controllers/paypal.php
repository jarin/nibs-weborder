<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Paypal Payment Gateway
 *
 * @category PaymentGateway
 * @package  CodeIgniter
 * @author   Jarin Anika <jarinanika@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://nibssolutions.com
 */
class Paypal extends MY_Controller
{
    /**
     * Class Constructor
     */
    function __construct()
    {
        parent::__construct();
    }
    

    function success(){
    	$paypal_response = $_REQUEST;
    	
    	if(isset($paypal_response['txn_id'])){
    		$this->session->unset_userdata('cart');
    		$message = '';
    		$tx = $paypal_response['txn_id'];
    		$st = strtolower($paypal_response['payment_status']);
    		$amount = $paypal_response['mc_gross'];
    		$cc = $paypal_response['mc_currency'];
    		$order_id = $paypal_response['custom'];
    		 
    		
    		if($st == 'completed' || $st == 'pending'){
    			 
    			$method_id = $this->common_m->getAValue("ResPaymentMethod", "id", array("paymentMethodName"=>"Paypal"));
    			//$totalAmt = $this->common_m->getAValue("ResOrder", "total", array("id"=>$order_id));
    		
    			$invoiceData = array(
    					'order_id' => $order_id,
    		
    					//'payment_method_id' => $method_id,
    					'bill_amount' => $amount,
    					'total_amount' =>$amount ,
    					'invoice_date' => mdate("%Y-%m-%d %H:%i:%s"),
    					'last_update' => mdate("%Y-%m-%d %H:%i:%s"),
    					'is_active' => 1
    			);
    		
    			$invoice_id = $this->common_m->insert("ResInvoice", $invoiceData);
    		
    			$insert_id = $this->common_m->insert("ResPayment", array("orderId"=>$order_id,"paymentMethodId"=>$method_id,"amount"=>$amount,"isActive"=>ANSWER_YES,"lastUpdate"=>mdate("%Y-%m-%d %H:%i:%s"),"invoice_id"=>$invoice_id));
    			$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Paid'));
    			$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));

    			$message = 'Your payment is completed and Order has been placed successfully';
    			
    		}elseif($st == 'refunded'){//Payment is refunded after approval
    			$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Cancelled'));
    			$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    			 
    		}elseif($st == 'reversed'){//Payment is refunded before approval or it has been cancelled
    			 
    			$reason = strtolower($paypal_response['reason_code']);//Reason for payment reversal
    			 
    			if($reason == 'refund'){//Payment is refunded before approval
    				$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Cancelled'));
    				$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    				 
    			}elseif($reason == 'other'){//Payment has been cancelled
    				$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Cancelled'));
    				$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    			}
    			 
    		}
    		$this->viewPage('notification', array('message'=>$message));
    	}
    	
    }
    
    function cancel($order_id){
    
    	if (isset($order_id) ) {
    		$this->session->unset_userdata('cart');    		
    		$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Cancelled'));
    		$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    		$message = 'Your payment is cancelled and Order has been cancelled';
    		$this->viewPage('notification', array('message'=>$message));
    	}
    }
    
    function notify(){
    	
    	$paypal_response = $_REQUEST;
    	if (isset( $paypal_response ) ) {
    	
    		$tx = $paypal_response['txn_id'];
    		$st = strtolower($paypal_response['payment_status']);
    		$amount = $paypal_response['mc_gross'];
    		$cc = $paypal_response['mc_currency'];
    		
    		
    		$order_id = $paypal_response['custom'];
    	
   	
    		if($st == 'completed' || $st == 'pending'){
    	
				$method_id = $this->common_m->getAValue("ResPaymentMethod", "id", array("paymentMethodName"=>"Paypal"));
				//$totalAmt = $this->common_m->getAValue("ResOrder", "total", array("id"=>$order_id));
				
				$invoiceData = array(
				    'order_id' => $order_id,
				    
				    //'payment_method_id' => $method_id,
				    'bill_amount' => $amount,
				    'total_amount' =>$amount ,
				    'invoice_date' => mdate("%Y-%m-%d %H:%i:%s"),
				    'last_update' => mdate("%Y-%m-%d %H:%i:%s"),
				    'is_active' => 1
				);
				
				$invoice_id = $this->common_m->insert("ResInvoice", $invoiceData);
				
				$insert_id = $this->common_m->insert("ResPayment", array("orderId"=>$order_id,"paymentMethodId"=>$method_id,"amount"=>$amount,"isActive"=>ANSWER_YES,"lastUpdate"=>mdate("%Y-%m-%d %H:%i:%s"),"invoice_id"=>$invoice_id));
				$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Paid'));
				$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    	
    		}elseif($st == 'refunded'){//Payment is refunded after approval
    			$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Cancelled'));
    			$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    		}elseif($st == 'reversed'){//Payment is refunded before approval or it has been cancelled
    	
    			$reason = strtolower($paypal_response['reason_code']);//Reason for payment reversal
    	
    			if($reason == 'refund'){//Payment is refunded before approval
    				$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Cancelled'));
    				$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    			}elseif($reason == 'other'){//Payment has been cancelled
    				$statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Cancelled'));
    				$this->common_m->update("ResOrder", array( "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    			}
    	
    		}
    	}
    }
    
    function reservationSuccess(){
    	$paypal_response = $_REQUEST;
    	 
    	if(isset($paypal_response['txn_id'])){
    		
    		$message = '';
    		$tx = $paypal_response['txn_id'];
    		$st = strtolower($paypal_response['payment_status']);
    		$amount = $paypal_response['mc_gross'];
    		$cc = $paypal_response['mc_currency'];
    		$reservation_id = $paypal_response['custom'];
    		 
    
    		if($st == 'completed' || $st == 'pending'){
    
    			$method_id = $this->common_m->getAValue("ResPaymentMethod", "id", array("paymentMethodName"=>"Paypal"));
    			//$totalAmt = $this->common_m->getAValue("ResOrder", "total", array("id"=>$order_id));
    
    			$invoiceData = array(
    					'reservationId' => $reservation_id,    
    					'paymentMethodId' => $method_id,
    					'billAmount' => $amount,
    					'totalAmount' =>$amount ,
    					'invoiceDate' => mdate("%Y-%m-%d %H:%i:%s"),
    					'lastUpdate' => mdate("%Y-%m-%d %H:%i:%s"),
    					'isActive' => 1
    			);
    
    			$invoice_id = $this->common_m->insert("ResReservationInvoice", $invoiceData);
    
    			$depositTypeId = $this->common_m->getAValue("ResDepositType", "id", array("depositType"=>'Web Paypal'));
    			$this->common_m->update("ResReservation", array("depositTypeId"=>$depositTypeId, "depositAmount"=>$amount), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    
    			$message = 'Your payment is completed and Reservation has been placed successfully. We will confirm you with in a sort time.';
    			 
    		}elseif($st == 'refunded'){//Payment is refunded after approval
    			$reservationStatusId = $this->common_m->getAValue("ResReservationStatus", "id", array("status"=>'Cancel'));
    			$this->common_m->update("ResReservation", array("reservationStatusId"=>$reservationStatusId), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    			$message = 'Your payment is cancelled and Reservatyion has been cancelled';
    
    		}elseif($st == 'reversed'){//Payment is refunded before approval or it has been cancelled
    
    			$reason = strtolower($paypal_response['reason_code']);//Reason for payment reversal
    
    			if($reason == 'refund'){//Payment is refunded before approval
    				$reservationStatusId = $this->common_m->getAValue("ResReservationStatus", "id", array("status"=>'Cancel'));
    				$this->common_m->update("ResReservation", array("reservationStatusId"=>$reservationStatusId), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    				$message = 'Your payment is cancelled and Reservatyion has been cancelled';
    					
    			}elseif($reason == 'other'){//Payment has been cancelled
    				$reservationStatusId = $this->common_m->getAValue("ResReservationStatus", "id", array("status"=>'Cancel'));
    				$this->common_m->update("ResReservation", array("reservationStatusId"=>$reservationStatusId), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    				$message = 'Your payment is cancelled and Reservatyion has been cancelled';
    			}
    
    		}
    		$this->viewPage('notification', array('message'=>$message));
    	}
    	 
    }
    
    function reservationCancel($reservation_id){
    	if (isset($reservation_id) ) {
    		$reservationStatusId = $this->common_m->getAValue("ResReservationStatus", "id", array("status"=>'Cancel'));
    		$this->common_m->update("ResReservation", array("reservationStatusId"=>$reservationStatusId), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    		$message = 'Your payment is cancelled and Reservatyion has been cancelled';
    		$this->viewPage('notification', array('message'=>$message));
    	}
    }
    function reservationNotify(){
    	$paypal_response = $_REQUEST;
    	
    	if(isset($paypal_response['txn_id'])){
    	
    		$tx = $paypal_response['txn_id'];
    		$st = strtolower($paypal_response['payment_status']);
    		$amount = $paypal_response['mc_gross'];
    		$cc = $paypal_response['mc_currency'];
    		$reservation_id = $paypal_response['custom'];
    		 
    	
    		if($st == 'completed' || $st == 'pending'){
    	
    			$method_id = $this->common_m->getAValue("ResPaymentMethod", "id", array("paymentMethodName"=>"Paypal"));
    			//$totalAmt = $this->common_m->getAValue("ResOrder", "total", array("id"=>$order_id));
    	
    			$invoiceData = array(
    					'reservationId' => $reservation_id,
    					'paymentMethodId' => $method_id,
    					'billAmount' => $amount,
    					'totalAmount' =>$amount ,
    					'invoiceDate' => mdate("%Y-%m-%d %H:%i:%s"),
    					'lastUpdate' => mdate("%Y-%m-%d %H:%i:%s"),
    					'isActive' => 1
    			);
    	
    			$invoice_id = $this->common_m->insert("ResReservationInvoice", $invoiceData);
    	
    			$depositTypeId = $this->common_m->getAValue("ResDepositType", "id", array("depositType"=>'Web Paypal'));
    			$this->common_m->update("ResReservation", array("depositTypeId"=>$depositTypeId, "depositAmount"=>$amount), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    	
    		} elseif($st == 'refunded') {//Payment is refunded after approval
    			$reservationStatusId = $this->common_m->getAValue("ResReservationStatus", "id", array("status"=>'Cancel'));
    			$this->common_m->update("ResReservation", array("reservationStatusId"=>$reservationStatusId), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    			
    		} elseif($st == 'reversed') {//Payment is refunded before approval or it has been cancelled
    	
    			$reason = strtolower($paypal_response['reason_code']);//Reason for payment reversal
    	
    			if($reason == 'refund') {//Payment is refunded before approval
    				$reservationStatusId = $this->common_m->getAValue("ResReservationStatus", "id", array("status"=>'Cancel'));
    				$this->common_m->update("ResReservation", array("reservationStatusId"=>$reservationStatusId), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    				
    			} elseif($reason == 'other') {//Payment has been cancelled
    				$reservationStatusId = $this->common_m->getAValue("ResReservationStatus", "id", array("status"=>'Cancel'));
    				$this->common_m->update("ResReservation", array("reservationStatusId"=>$reservationStatusId), array("id"=>$reservation_id, "isActive"=>ANSWER_YES));
    				
    			}
    	
    		}
    		
    	}
    }
}