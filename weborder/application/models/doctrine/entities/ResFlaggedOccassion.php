<?php



namespace entities;

/**
 * ResFlaggedOccassion
 *
 * @Table(name="res_flagged_occassion")
 * @Entity
 */
class ResFlaggedOccassion
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResFlaggedOccassionType
   *
   * @ManyToOne(targetEntity="ResFlaggedOccassionType")
   * @JoinColumns({
   *   @JoinColumn(name="flagged_occassion_type_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $flaggedOccassionType;

  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $customer;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;
  
  /**
   * @var datetime $occassionDate
   *
   * @Column(name="occassion_date", type="date", nullable=true)
   */
  private $occassionDate;
  
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set flaggedOccassionType
   *
   * @param ResCustomer $flaggedOccassionType
   */
  public function setFlaggedOccassionType(\ResFlaggedOccassionType $flaggedOccassionType)
  {
  	$this->flaggedOccassionType = $flaggedOccassionType;
  }
  
  /**
   * Get flaggedOccassionType
   *
   * @return ResFlaggedOccassionType
   */
  public function getFlaggedOccassionType()
  {
  	return $this->flaggedOccassionType;
  }
  
  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
      $this->customer = $customer;
  }
  
  /**
   * Get customer
   *
   * @return ResCustomer
   */
  public function getCustomer()
  {
      return $this->customer;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
      $this->lastUpdatedBy = $lastUpdatedBy;
  }
  
  /**
   * Get lastUpdatedBy
   *
   * @return ResUser
   */
  public function getLastUpdatedBy()
  {
      return $this->lastUpdatedBy;
  }
  
  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
      $this->lastUpdate = $lastUpdate;
  }
  
  /**
   * Get lastUpdate
   *
   * @return datetime
   */
  public function getLastUpdate()
  {
      return $this->lastUpdate;
  }
  
  /**
   * Set occassionDate
   *
   * @param date $occassionDate
   */
  public function setOccassionDate($occassionDate)
  {
      $this->occassionDate = $occassionDate;
  }
  
  /**
   * Get occassionDate
   *
   * @return date
   */
  public function getOccassionDate()
  {
      return $this->occassionDate;
  }

}