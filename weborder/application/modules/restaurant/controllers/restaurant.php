<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Restaurant Controller
 *
 * @category Controller
 * @package  CodeIgniter
 * @author   Jarin Anika <jarinanika@gmail.com>
 * @license  http://directory.fsf.org/wiki/License:ReciprocalPLv1.3 Reciprocal Public License v1.3
 * @link     http://nibssolutions.com
 */
class Restaurant extends MY_Controller {
	public function __construct(){
		parent::__construct();
		
			if($this->uri->segment(3) !=''){
				$module = $this->uri->segment(3);
			} else {
				$module = 'order';
			}
			
			if($this->uri->segment(4) !=''){
				$method = $this->uri->segment(4);
			} else {
				$method = 'index';
			}
			$param_offset = 4;
			$params = array_slice($this->uri->rsegment_array(), $param_offset);
			
			//$params = implode(",", $params);
			//print_r($params);
		
			//require_once(APPPATH.'modules/'.$module.'/controllers/'.$module.EXT);
				
			//$CI = new $module();
			
			$this->load->module($module);
			if (method_exists($this->$module, $method)) {
			
				if(count($params) == 1){
					echo modules::run( $module.'/'.$method, $params[0]);
					exit;
				} elseif(count($params) == 2){
					echo modules::run( $module.'/'.$method, $params[0], $params[1]);
					exit;
				} elseif(count($params) == 3){
					echo modules::run( $module.'/'.$method, $params[0], $params[1], $params[2]);
					exit;
				} elseif(count($params) == 4){
					echo modules::run( $module.'/'.$method, $params[0], $params[1], $params[2], $params[3]);
					exit;
				} elseif(count($params) == 5){
					echo modules::run( $module.'/'.$method, $params[0], $params[1], $params[2], $params[3], $params[4]);
					exit;
				}elseif(count($params) == 0){ 
					echo modules::run( $module.'/'.$method);
					exit;
				}
				
				//echo call_user_func_array(array (&$CI, $method ), $params);
				//var_dump( call_user_func_array($this->$module->index , $params));
			}
		
	}

}