<?php
$baseUrl=base_url();
?>

<!--[if lt IE 9]>
  <script src="assets/js/excanvas.js"></script><script src="assets/js/respond.js"></script><![endif]-->
<link href="<?=$baseUrl?>assets/css/admin/business_dashboard.css" rel="stylesheet" media="screen">
<script src="<?=$baseUrl?>assets/js/jquery-knob/js/jquery.knob.js"></script>
<link href="<?=$baseUrl?>assets/css/admin/daterangepicker-bs2.css" rel="stylesheet" media="screen">
<script src="<?=$baseUrl?>assets/js/moment.min.js"></script>
<script src="<?=$baseUrl?>assets/js/daterangepicker.js"></script>


	<div id="main-content" class="onepane">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12">
					<h3 class="page-title"> Dashboard <small> General Information </small></h3>
				</div>
			</div>
			<div style="height:30px;clear:both;"></div>
			<div id="page" class="dashboard" style="background: #fff;">
				<div class="span12">
					<form method='post'  action='<?=base_url()?>admin/home' loadingDiv='loader' autocomplete='off' enctype='multipart/form-data' style="margin:0px;padding:20px;">
						<table cellpadding='4' cellspacing='0' align='center'>
							<tr>
								<div class="control-group">
									<label class="control-label" for="daterange" style="display: inline-block;width: 100px;line-height: 30px;">Date Range</label>
		        			        <div class="controls" style="display: inline-block;height: 30px;">
										<div class="input-prepend">
											<span class="add-on"><i class="glyphicon glyphicon-calendar icon-calendar"></i></span>
											<input type="text" style="width: 200px;" name="dates" id="daterange" class="daterange" value="<?=$dates?>" />
										</div>
									</div>
								</div>
							</tr>
						</table>
					</form>
				</div>
				<div style="height:10px;clear:both;"></div>
				<div class="row-fluid circle-state-overview">
					<div class="span2 responsive" data-tablet="span3" data-desktop="span2">
						<div class="circle-stat block">
							<div class="visual">
								
								<input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" value="<?=((10/100)*$totalOrder)?>" data-fgcolor="#4CC5CD" data-bgcolor="#ddd" />
							</div>
							<div class="details">
								<div class="number">+<?=(($totalOrder =='')?'0':$totalOrder)?></div>
								<div class="title"><a href="<?=$baseUrl?>admin/order/report/order">Total Orders</a></div>
							</div>
						</div>
					</div>
					<div class="span2 responsive" data-tablet="span3" data-desktop="span2">
						<div class="circle-stat block">
							<div class="visual">
								
								<input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" value="<?=number_format(((10/100)*$totalSales),1)?>" data-fgcolor="#e17f90" data-bgcolor="#ddd" />
							</div>
							<div class="details">
								<div class="number">&pound;<?=(($totalSales =='')?'0':$totalSales)?></div>
								<div class="title"><a href="<?=$baseUrl?>admin/order/report/sales">Total Gross Sales</a></div>
							</div>
						</div>
					</div>
					<div class="span2 responsive" data-tablet="span3" data-desktop="span2">
						<div class="circle-stat block">
							<div class="visual">
								
								<input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" value="<?=number_format(((10/100)*$totalNetSate),1)?>" data-fgcolor="#4487AF" data-bgcolor="#ddd" />
							</div>
							<div class="details">
								<div class="number">&pound;<?=(($totalNetSate =='')?'0':$totalNetSate)?></div>
								<div class="title"><a href="<?=$baseUrl?>admin/order/report/netsales">Total Net Sales</a></div>
							</div>
						</div>
					</div>
					<div class="span2 responsive" data-tablet="span3" data-desktop="span2">
						<div class="circle-stat block">
							<div class="visual">
								
								<input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" value="<?=((10/100)*$totalDelivery)?>" data-fgcolor="#a8c77b" data-bgcolor="#ddd" />
							</div>
							<div class="details">
								<div class="number">+<?=(($totalDelivery =='')?'0':$totalDelivery)?></div>
								<div class="title"><a href="<?=$baseUrl?>admin/order/report/delivery">Total Delivery</a></div>
							</div>
						</div>
					</div>
					<div class="span2 responsive" data-tablet="span3" data-desktop="span2">
						<div class="circle-stat block">
							<div class="visual">
								
								<input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" value="<?=((10/100)*$totalCollection)?>" data-fgcolor="#b9baba" data-bgcolor="#ddd" />
							</div>
							<div class="details">
								<div class="number">+<?=(($totalCollection =='')?'0':$totalCollection)?></div>
								<div class="title"><a href="<?=$baseUrl?>admin/order/report/collection">Total Collection</a></div>
							</div>
						</div>
					</div>
					<div class="span2 responsive" data-tablet="span3" data-desktop="span2">
						<div class="circle-stat block">
							<div class="visual">
								
								<input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" value="<?=number_format(((10/100)*$total_reservation),1)?>" data-fgcolor="#c8abdb" data-bgcolor="#ddd" />
							</div><div class="details"><div class="number">+<?=(($total_reservation =='')?'0':$total_reservation)?></div><div class="title"><a href="<?=$baseUrl?>admin/reservation">Total Reservation</a></div>
							</div>
						</div>
					</div>
					<!--<div class="span2 responsive" data-tablet="span3" data-desktop="span2">
						<div class="circle-stat block">
							<div class="visual">
								<div class="circle-state-icon"><i class="icon-bar-chart blue-color"></i></div>
								<input class="knob" data-width="100" data-height="100" data-displayprevious="true" data-thickness=".2" value="25" data-fgcolor="#93c4e4" data-bgcolor="#ddd" />
							</div>
							<div class="details"><div class="number">478</div><div class="title">Updates</div></div>
						</div>
					</div>-->
				</div>
			</div>
			<div style="height:20px;clear:both;"></div>
			<div class="row-fluid">
				<div class="span12">
					<div class="widget business-dashboard">
						<div class="widget-title">
							<div class="span2" style="background-color: #4CC5CD;"><a href="#" class="order" rel="unpaid"><h4> Active Orders</h4></a></div>
							<div class="span2" style="background-color: #e17f90;"><a href="#" class="order" rel="paid"><h4> Paid Orders</h4></a></div>
							<div class="span2" style="background-color: #4487AF;"><a href="#" class="order" rel="cancel"><h4> Cancelled Orders</h4></a></div>
							<div class="span2" style="background-color: #a8c77b;"><a href="#" class="order" rel="discount"><h4> Discounted Orders </h4></a></div>
							<div class="span2" style="background-color: #b9baba;"><a href="#" class="order" rel="transit"><h4> In Transit Orders </h4></a></div>
							<div class="span2" style="background-color: #c8abdb;"><a href="#"><h4>Order Types </h4></a></div>
						</div>
						<div class="widget-body">
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
									<tr>
										<th>Order number</th>
										<th>Order Type</th>
										<th>Customer Name</th>
										<th>Customer Address</th>
										<th>Order status</th>
										<th>Date and time</th>
										<th>Value of Order</th>
										<th>Value of Discount</th>
										<th>View Order</th>
									</tr>
								</thead>
								<tbody id="orderdiv">
																		
								</tbody>
							</table>
							<div class="space7"></div>
							<div class="clearfix"><a href="<?php echo base_url();?>admin/order" class="btn btn-mini pull-right">All Orders</a></div>
					</div>
				</div>
			</div>
		</div>
			<!-- icon set for Block B -->
			<div class="row-fluid">
				<a class="icon-btn span2" href="#"><i class="icon-wrench"></i><div>System Settings</div></a>
				<a class="icon-btn span2" href="#"><i class="icon-cog"></i><div>Business Settings </div></a>
				<a class="icon-btn span2" href="#"><i class="icon-align-justify"></i><div>Menu</div></a>
				<a class="icon-btn span2" href="#"><i class="icon-list"></i><div>Report</div></a>
				<a class="icon-btn span2" href="#"><i class="icon-picture"></i><div>Advertising</div></a>
				<a class="icon-btn span2" href="<?php echo base_url();?>admin/order"><i class="icon-tasks"></i><div>Orders</div></a>
			</div>
			<div class="row-fluid">
				<a class="icon-btn span2" href="<?php echo base_url();?>admin/reservation"><i class="icon-book"></i><div>Reservations</div></a>
				<a class="icon-btn span2" href="#"><i class="icon-comment"></i><div>Reviews</div></a>
				<a class="icon-btn span2" href="#"><i class="icon-briefcase"></i><div>Financial</div></a>
			</div>
			<!-- END icon set for Block B -->
			<div style="height:20px;clear:both;"></div>
		</div>
	</div>
	<script>
	var body=$("body");
	$(document).ready(function(){
		$(".knob").knob({
		'min':0,
        'max':100,
        'step': 1,
        'displayPrevious': true,
        'readOnly': true,
        'draw' : function () { $(this.i).val(this.cv + '%'); }
		});
	

		body.find('#daterange').daterangepicker({
				ranges: {
			  	'Today': [moment(), moment()],
			    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			    'Last 7 Days': [moment().subtract('days', 6), moment()],
			    'Last 30 Days': [moment().subtract('days', 29), moment()],
			    'This Month': [moment().startOf('month'), moment().endOf('month')],
			    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
				},
			  startDate: moment().subtract('days', 29),
			  endDate: moment()
			},
			function(start, end) {
				body.find('#daterange').val(start.format('YYYY.MM.DD') + ' - ' + end.format('YYYY.MM.DD'));
				body.find("form").submit();
		});
		body.find('.order').on("click", function(e){
			e.preventDefault();
			var _this = $(this),
				sortby = _this.attr('rel'),
				row = body.find('#orderdiv');
			
				$.ajax({
		    		async: false, 
		    		type: "POST",
		    		url: "admin/home/orderlist",
		    		data: "sortby="+ sortby,
		    		success:function(data){
		    		  row.html(data);		    		
		    		}
		    	});
			});
		body.find(".order:first").prop("checked", true).trigger("click");

		});
</script>
