<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/shop_catalog.css" type="text/css" media="screen"  />
<script src="<?=$baseUrl?>assets/js/jquery.livequery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/shop/catalog.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
  <div class="padded"><h1><?=$pagename?></h1>
  <div class="initPage">
    <div>
      <p>Click on a category in the tree on the left to view related products....</p>
      <p>Right-click on the categories in the tree to see more options</p>
    </div>
  </div></div>
</script>
<div id="innerTabs" style="display:none">
  <ul>
    <li class="active">Basic Options</li>
    <li>Description</li>
    <li>Pictures</li>
    <li>Additional Categories</li>
    <li>Configuration</li>
  </ul>
</div>
<div id="thirdBar" class="secondpane">
  <div class='headertoolbar'>Products <div id="searchCount">0 Filtered Results</div> <button class="addArticleBtn" title="Add Product"><img src="<?=$baseUrl?>assets/images/admin/icon_add.png" /></button></div>
  <div class="placeholder">No Products</div>
  <ul class="list reorderable" rel="shop_catalog/reorderProducts">

  </ul>
  <div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>
<div id="mainContent" class="threepane lastpane">
  <div id="mainContentInner" class="">
    <div id="dContent">

    </div>
  </div>
</div>
<div id="sideBar" style="box-shadow:none;border-right:1px solid #999">
  <div class="headertoolbar" style="position:relative;box-shadow:none">Categories <span id="orphanage_link"><?=$orphanCount?> Orphan<?=(($orphanCount != 1)?("s"):(""))?></span></div>
  <div id="treeHolder">
    <ul>
      <?=$list1?>
    </ul>
  </div>
</div>
<div id="orphanage" class="comp_floatingList">
  <div class="chink lt"></div>
  <div class="headertoolbar">Orphaned Categories <span class="close" title="Close" onclick="$('#orphanage_link').click();return false;"></span></div>
  <ul>
    <?=$orphanList?>
  </ul>
</div>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce/tiny_mce_gzip.js"></script>
<script type="text/javascript">
tinyMCE_GZ.init({
	plugins : 'safari,style,table,save,advimage,advlink,contextmenu,paste,directionality,visualchars,nonbreaking,xhtmlxtras,inlinepopups,fullscreen,spellchecker,media',
	themes : 'advanced',
	languages : 'en',
	spellchecker_languages : "+English=en",
	disk_cache : true,
	debug : false,
	baseURL : "<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce"
});

</script>
<script type="text/javascript" src="<?=$baseUrl?>assets/js/tinymce/jscripts/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script type="text/javascript">
	var _KA=window.setInterval(function(){
		$.ajax({
			url:base_url+"admin/pages/XkeepAlive",
			dataType:"json",
			type:"post",
			data:{"keepAlive":"true"}
		})
	},180000);


	var changed_count=0;
  function MCE_Changed(){

  }
</script>
