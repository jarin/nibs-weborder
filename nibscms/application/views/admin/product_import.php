<div id='main-content' class='onepane'>
     <div class='container-fluid' style=''>
          <div class='row-fluid'>
            <div class='span12'>	
				<div class=''>
				        <?php 
				if($this->session->flashdata('success') !=''){
					echo  '<p style="color:green">'.$this->session->flashdata('success').'</p>';
				}
				if($this->session->flashdata('error') !=''){
					echo '<p style="color:red">'.$this->session->flashdata('error').'</p>';
					
				}
				?>
				
					<?php if($this->session->userdata("utype") == 'admin'){
						if(count($allbusiness) > 0){
						?>
						<h3 class='page-title'>Restaurants</h3><br />
							<div id='form_wizard_1' class='widget box'>		
								<div class='widget-body form'>
									<div style='margin:0 auto;width:60%;text-align:center'>
										<form method='post'  action='<?=base_url()?>admin/productImport' >
											<table cellpadding='4' cellspacing='0' align='center'>
												<tr>
													<th align='left' >Choose Restaurant</th>
													<td >
														<select name='businessName' onChange="this.form.submit();">
															<option value=''>Select</option>
															<?php foreach ($allbusiness as $business){?>
															<option <?php if($business_id == $business['id']) echo 'selected';?> value='<?php echo $business['id']?>'><?php echo $business['name'].'('. $business['unique_url'] .')'?></option>
															<?php }?>
														</select>
													</td>
												</tr>
											
											</table>
										</form></div>
								</div>
							</div>
				
				<?php } } ?>
				
				
					<h3 class='page-title'>Epos settings</h3><br />
					<div id='form_wizard_1' class='widget box'>		
						<div class='widget-body form'>
							<div style='margin:0 auto;width:60%;text-align:center'>
								<form method='post'  action='<?=base_url()?>admin/productImport/saveinfo'>
									<table cellpadding='4' cellspacing='0' align='center'>
										<tr>
											<th align='left' >Database Name</th>
											<td ><input type='text' name='db_name' value="<?php if(!empty($epos[0]['db_name'])) echo $epos[0]['db_name']; ?>" id='edit_db_name' /></td>
										</tr>
										<tr>
											<th align='left' >Host name</th>
											<td ><input type='text' name='db_host' value='<?php if(!empty($epos[0]['db_host'])) echo $epos[0]["db_host"]; ?>' id='edit_db_host' /></td>
										</tr>
										<tr>
											<th align='left' >User name</th>
											<td ><input type='text' name='db_user' value='<?php if(!empty($epos[0]['db_user'])) echo $epos[0]["db_user"]; ?>' id='google-plus_url' /></td>
										</tr>
									
										<tr>
											<th align='left' >password</th>
											<td ><input type='text' name='db_pass' value='<?php if(!empty($epos[0]['db_pass'])) echo $epos[0]["db_pass"]; ?>' id='db_pass' /></td>
										</tr>
										
										<tr>										
											<td align='right' colspan='4'>
												<button type='submit' class='button save'>Save Changes</button>
												<div id='loader<?=$business['id']?>' class='q_feedback'></div>
												<input type='hidden' name='b_id' value='<? if(!empty($business_id)) echo $business_id; ?>' />
												<input type='hidden' name='epos_id' value='<? if(!empty($epos[0]["id"])) echo $epos[0]["id"]; ?>' />
											</td>
										</tr>
									</table>
								</form></div>
						</div>
					</div>
					<h3 class='page-title'>Import Product</h3><br />
					<div id='form_wizard_1' class='widget box'>
				    
				        <div class='widget-body form'>
				        	<div style='margin:0 auto;width:60%;text-align:center'>
								
									<table cellpadding='4' cellspacing='0' align='center'>
										
										<tr>
											<td>To import from remote server click</td>
											<td align='right'>
												<button type='submit' class='button' onClick="location.href='<?=base_url()?>admin/productImport/import/<? if(!empty($business_id)) echo $business_id; ?>'">Import</button>									
											</td>
										</tr>
									</table>
								
							</div>
						</div>
					</div>
					<h4>Or</h4>
					<div id='form_wizard_1' class='widget box'>		
						<div class='widget-body form'>
							<div style='margin:0 auto;width:60%;text-align:center'>
								<form method='post' rel='excelUpload' action='<?=base_url()?>admin/productImport/uploadExcel' loadingDiv='loader' autocomplete='off' enctype='multipart/form-data'>
									<table cellpadding='4' cellspacing='0' align='center'>
										<tr>
											<td>Download Sample</td>
											<td>
												<a href="<?= base_url();?>admin/productImport/downloadSampleExcel" title="Download Template"><img src="assets/images/icon_excel.gif" border="0" /></a>
											</td>
										</tr>
										<tr>
											<th align='left' >Upload Excel</th>
											<td ><input type='file' name='excel' value="" id='edit_excel_input' /></td>
										
											<td align='right' >
											<input type='hidden' name='restaurant_id' value='<? if(!empty($business_id)) echo $business_id; ?>' />
											<button type='submit' class='button'>Upload</button>
											<div id='loader' class='q_feedback'></div>
											
											</td>
										</tr>
									</table>
								</form>
								<div id="download_path"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$("form[rel=excelUpload]").live("submit",function(){
	var _this=this;
	var strName = ("uploader" + (new Date()).getTime());
	$(_this).attr( "target", strName );
	var lD=$(_this).attr("loadingDiv");
	var frame=$( "<iframe style=\"display:none\" name=\"" + strName + "\" id=\"" + strName + "\"  />" );
	$( "body:first" ).append( frame );
	frame.load(function(){
		var __this=$(this);
		setTimeout(function(){
			var objUploadBody = __this[0].contentDocument.body;
			var jBody = $(objUploadBody);
			try{var objData= eval( "(" + jBody.html() + ")" ) || null;}catch(e){};
			
			if(objData){
				if(objData.success=="error"){
					alert(objData.msg);
				}else if(objData.success=="success"){
					$("#"+lD).html("<span style='color:#006b3e;font-weight:bold' class='loader'>Uploaded</span>");
					$("#download_path").html('');
					$("#download_path").html('<a href="'+objData.msg+'">Download Server Response</a>');
					//reset form
					$(_this).reset();
					if($.isFunction(objData.successFunction)){
						objData.successFunction.apply();
					}
				}
			}else{
				alert("Error in response, File may not have been saved");
				console.log(objData);
				console.log(jBody.html());
			}

			setTimeout(
				function(){
					frame.remove();
					$("#"+lD).html("");
				}, 200);
		},100);
	});
	$("#"+lD).html("<img class='loader' border='0' src='"+base_url+"assets/images/admin/small_loader.gif' />");

});
</script>
