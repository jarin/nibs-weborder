
$(function(){

  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  //--------CHECK BOXES
	$("#treebase input[type=checkbox]").live("click",function(){
		act_deactivatePage($(this));
		return false;
	});

  //resize MCE editor as soon as it is shown
	$("#innerTabs li").live("click",function(){
	  var _this=$(this);
	  if (_this.index()==1){
	    resizeEditor();
	  }
	});

	//--Page Types
	$("input.pagetype").live("click",function(){
	  switchPageTypes(this);
	})

	//--Page versions
	$("#pageVersionsList li").live("click",function(e){
		var _this=$(this);
		var target=e.target
		if(!$(target).is("input")){
			if(!_this.is(".active")){
				if(!_this.is("#version_latestversion")){
					requestVersion(_this);
				}else{
					switchAreas("edit");
					_this.addClass("active").siblings().removeClass("active");
				}
			}
		}else{
			if(!$(target)[0].checked){
				$('#versionCompareButton').prop("disabled",true);
			}
			if($("input:checked",$("#pageVersionsList")).length>2){
				$("input:checked",$("#pageVersionsList")).not(target).eq(1)[0].checked="";
				$('#versionCompareButton').prop("disabled",false);
			}else if($("input:checked",$("#pageVersionsList")).length==2){
				$('#versionCompareButton').prop("disabled",false);
			}
		}
	});
  
  $("input[name='sub_title']").removeAttr('disabled');
  
  $("input[name='has_banner']").live("click",function(){
    var _this=$(this),par=_this.parent().parent(),_next=par.next();
    _next.toggle();
    _next.find("select").removeAttr('disabled');
  });
  
  
	//--orphanage
	$("#orphanage_link").live("click",function(){
	  var _this=$(this);
		var state=_this.data("menuShowing") || false;
		if(state){
			hideOrphanage();
			_this.removeClass("active").data("menuShowing",false);
		}else{
			showOrphanage();
			_this.addClass("active").data("menuShowing",true);
		}
	});

	$("#orphanage li").livequery(function(){
		$(this).draggable({
			opacity: 0.9,
			revert:"invalid",
			zIndex:"2700",
			helper:"clone"
		});
	});

	$("#treeHolder li").livequery(function(){
		$(this).droppable({
			accept:".orphan",
			hoverClass:"onDrop",
			tolerance:"pointer",
			greedy:true,
			drop:function(e,ui){
			  var dropped=ui.draggable;
				var e=dropped.attr("id").match(/(.+)[-=_](.+)/);
				var dropped_id=e[2];
				var e2=$(this).attr("id").match(/(.+)[-=_](.+)/);
				var recv_id=(e2 ? e2[2] : 0);
				var recv_type="page";
				NIBS.util.ajax({type:recv_type,orphan_id:dropped_id,page_id:recv_id},"admin/content/attachOrphanToNewParent");
			}
		});
	});

  //the TREEEE
  createTree();

  //Autosaves
  $("#loadAutoSave").live("click",function(){
    var page_id=$("#EditableArea").parent().find("input[name=page_id]").val(),
    _this=$(this);
    _this.html("Loading... Please wait...").prop("disabled",true).next().prop("disabled",true);
    $.get(base_url+"admin/content/XgetAutoSave/"+page_id,function(text){
      var data = eval("(" + text + ")");
      if(data && data.data){
        tinyMCE.execInstanceCommand('EditableArea','mceSetContent',false,data.data);
      }
      var _par=_this.parent().parent()
      _par.addClass("hide");
      setTimeout(function(){
    	  _par.remove();
    	},300);
    })

  });
  $("#discardAutoSave").live("click",function(){
    var _par=$(this).parent().parent();
    _par.addClass("hide");
    setTimeout(function(){
  	  _par.remove();
  	},300);
  });
})

function switchPageTypes(el){
  var _this=$(el),form=_this.parents("form"),type=_this.attr("tp");
  $("input[type=text],select[name='master_page_id']",form).filter(":not(."+type+")").prop("disabled",true)
  .end().filter("."+type).prop("disabled",false);
  if(type!="normal"){
    $("#innerTabs li").eq(1).addClass("disabled");
    $("#pageVersionsList").hide(0);
  }else{
    $("#innerTabs li").eq(1).removeClass("disabled");
    $("#pageVersionsList").show(0);
    $("select[name='treatment_id']").removeAttr('disabled'); 
  }
}


function act_deactivatePage(el){
	if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var type=e[1];
		var id=e[2];
	}
	NIBS.util.ajax({"id":id},"admin/content/activateDeactivatePage");

}

function do_act_deactivatePage(id,val){
  $("#chPage_"+id).prop("checked",val=="1"?true:false);
  $("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

function loadPageDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	tinyMCE && tinyMCE.execCommand("mceRemoveControl",false,"EditableArea");
	$("#dContent").load(base_url+"admin/content/viewPageDetails/"+id,function(){
	  $(window).bind("resize",NIBS.util.debounce(resizeEditor));
	  switchPageTypes($("input.pagetype:checked")[0]);
    
    $("input[name='sub_title']").removeAttr('disabled');
	});
	loadPageInfo(id);
	$("#innerTabs,#thirdBar").show(0);
	$("#innerTabs li").removeClass("active").eq(0).addClass("active");
	$("#main-content").addClass("hastabs threepane");
	loadPageVersions(id);
}

function resizeEditor(){
  var ifr=$("#EditableArea_ifr"),
  spn=$("#EditableArea_parent"),
  ifr2=$("#previewIframe"),
  spn2=$("#previewArea");
  ifr.css({height:spn.height()-62});
  ifr2.css({height:spn2.height()-62});
}

function loadPageInfo(id){
  $("#infoPanel").load(base_url+"admin/content/viewPageInfo/"+id);
}

//--Versions
function loadPageVersions(id){
  $.ajax({
    url:base_url+"admin/content/viewVersionsList/"+id,
    dataType:"text",
    success:function(text){
      var data = eval("(" + text + ")");
      $("#versionPanel").html(data.list);
      $("#versionsCount").html(data.count);
    }
  });
  $('#versionCompareButton').prop("disabled",true);
}
function revertVersion(){
	if(confirm("Are you sure?\nAny unsaved changes will be overwritten!")){
		var contents=$("#previewIframe").contents().find("body").html();
		tinyMCE.activeEditor.setContent(contents);
		$("#version_latestversion").click()
	}
}
function compareVersions(){
	var checked=$("input:checked",$("#pageVersionsList"));
	var vNum1=checked.eq(0).parent().attr("rel");
	var vNum2=checked.eq(1).parent().attr("rel");
	var page_id=checked.eq(0).parent().attr("page_id");
	var url=checked.eq(0).parent().parent().attr("rel");
	url=url+"compareVersions/"+page_id+"/"+vNum1+"/"+vNum2;
	NIBS.util.ajax({},url,"GET");
}
function requestVersion(el) {
	var url=el.parent().attr("rel");
	var vNum=el.attr("rel");
	var page_id=el.attr("page_id")
	url=url+"viewPageVersion/"+page_id+"/"+vNum;
	NIBS.util.ajax({},url,"GET");
}

function switchAreas(type){
	var type=type || "edit";

	var tab=$("#innerTabs li").eq(1);
	if(tab.not(".active")){
	  tab.click();
	}

	if(type=="edit"){
		$("#previewArea").css({
		  "-webkit-transition-property":"-webkit-transform",
		  "-webkit-transition-duration":"0.5s",
		  "-webkit-transform":"translate(-100%,0)",
		  "-moz-transition-property":"-moz-transform",
		  "-moz-transition-duration":"0.5s",
		  "-moz-transform":"translate(-100%,0)",
		});
		$("#editArea").show(0).css({
		  "-webkit-transition-property":"-webkit-transform",
		  "-webkit-transition-duration":"0.5s",
		  "-webkit-transform":"translate(0,0)",
		  "-moz-transition-property":"-moz-transform",
		  "-moz-transition-duration":"0.5s",
		  "-moz-transform":"translate(0,0)",
		});
		setTimeout(function(){
		  $("#previewArea").hide(0);
		  resizeEditor();
		},500);
	}else if(type=="preview"){
		$("#editArea").css({
		  "-webkit-transition-property":"-webkit-transform",
		  "-webkit-transition-duration":"0.5s",
		  "-webkit-transform":"translate(100%,0)",
		  "-moz-transition-property":"-moz-transform",
		  "-moz-transition-duration":"0.5s",
		  "-moz-transform":"translate(100%,0)",
		});
		$("#previewArea").show(0).css({
		  "-webkit-transition-property":"-webkit-transform",
		  "-webkit-transition-duration":"0.5s",
		  "-webkit-transform":"translate(0,0)",
		  "-moz-transition-property":"-moz-transform",
		  "-moz-transition-duration":"0.5s",
		  "-moz-transform":"translate(0,0)",
		});
		setTimeout(function(){
		  $("#editArea").hide(0);
		  resizeEditor();
		},500);
	}
}

function injectContentIntoIframe(content){
	var cssPath=base_url+"assets/js/tinymce/jscripts/tiny_mce/themes/advanced/skins/default/content.css";
	var diffCssPath=base_url+"assets/css/admin/diff.css";
	content="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'\
		'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> \
	<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'><head><link rel='stylesheet' href='"+cssPath+"' type='text/css' /><link rel='stylesheet' href='"+diffCssPath+"' type='text/css' /></head><body>"+content+"</body></html>";
	var iframe=$("#previewIframe")[0];
	var doc = iframe.document;
	if(iframe.contentDocument)
	    doc = iframe.contentDocument; // For NS6
	else if(iframe.contentWindow)
	    doc = iframe.contentWindow.document; // For IE5.5 and IE6
	// Put the content in the iframe
	doc.open();
	doc.writeln(content);
	doc.close();
}
//--END Versions

//--orphanage
function showOrphanage(){
  var orphanage=$("#orphanage");
  orphanage.removeClass("showing").show(0);
  setTimeout(function(){
	  orphanage.addClass("showing");
	},50);
}

function hideOrphanage(){
  var orphanage=$("#orphanage");
  orphanage.removeClass("showing");
  setTimeout(function(){
	  orphanage.hide(0);
	},300);
}


/*--------------------*/
function createTree(){
	//--------TREE
	$("#treeHolder").tree(
		{

		    selected    : false,        // FALSE or STRING or ARRAY
		    opened      : [],           // ARRAY OF INITIALLY OPENED NODES
		    languages   : [],           // ARRAY of string values (which will be used as CSS classes - so they must be valid)
		    path        : base_url+"assets/js/",        // FALSE or STRING (if false - will be autodetected)
		    cookies     : {prefix:"SEC"},        // FALSE or OBJECT (prefix, open, selected, opts - from jqCookie - expires, path, domain, secure)
		    ui      : {
		        dots        : true,     // BOOL - dots or no dots
		        rtl         : false,    // BOOL - is the tree right-to-left
		        animation   : 100,        // INT - duration of open/close animations in miliseconds
		        hover_mode  : true,     // SHOULD get_* functions chage focus or change hovered item
		        scroll_spd  : 4,
		        theme_path  : base_url+"assets/images/admin/",    // Path to themes
		        theme_name  : "tree",// Name of theme
		        context     : [
		            {
		                id      : "create",
		                label   : "Create",
		                icon    : "create.png",
		                visible : function (NODE, TREE_OBJ) { if(NODE.length != 1) return false; return TREE_OBJ.check("creatable", NODE); },
		                action  : function (NODE, TREE_OBJ) { TREE_OBJ.create(false, TREE_OBJ.get_node(NODE)); }
		            },
		            "separator",

					      /*{
		                id      : "edit",
		                label   : "View Details",
		                icon    : "edit.png",
		                visible : function (NODE, TREE_OBJ) { return NODE.attr("rel")!="folderparent";},
		                action  : function (NODE, TREE_OBJ) {
			 				        $(NODE).find(" > a").click();
						        }
		            },*/
		            {
		                id      : "rename",
		                label   : "Rename",
		                icon    : "rename.png",
		                visible : function (NODE, TREE_OBJ) { if(NODE.length != 1) return false; return TREE_OBJ.check("renameable", NODE); },
		                action  : function (NODE, TREE_OBJ) { TREE_OBJ.rename(); }
		            },
		            {
		                id      : "delete",
		                label   : "Delete",
		                icon    : "remove.png",
		                visible : function (NODE, TREE_OBJ) { var ok = true; $.each(NODE, function () { if(TREE_OBJ.check("deletable", this) == false) ok = false; return false; }); return ok; },
		                action  : function (NODE, TREE_OBJ) { $.each(NODE, function () { TREE_OBJ.remove(this); }); }
		            }
		        ]
		    },
		    rules   : {
		        multiple    : false,
		        metadata    : false,
		        type_attr   : "rel",
		        multitree   : false,
		        createat    : "bottom",
		        use_inline  : false,
		        clickable   : "all",
		        renameable  : ["file"],
		        deletable   : ["file"],
		        creatable   : ["folderparent","file"],
		        draggable   : ["file"],
		        dragrules   : [
					"file inside file",
					"file before file",
					"file after file"

				],
		        drag_copy   : false,
		        droppable   : [],
		        drag_button : "left"
		    },
		    lang : {
		        new_node    : "New Page",
		        loading     : "Loading ..."
		    },
		    callback    : {
		        beforecreate: function(NODE,REF_NODE,TYPE,TREE_OBJ,PARENT) {
					var $PARENT=$(PARENT);
					var $NODE=$(NODE).attr("rel","file");;
					if($PARENT.attr("id")=="treebase"){
						var type="treebase";
						var id="";
					}else{
						var e=$PARENT.attr("id").match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id=e[2];
					}

					var ret=false;

					var url=base_url+"admin/content/addPage";

					$.ajax({
						url:url,
						dataType:"text",
						type:"post",
						async:false,
						data:{type:type,id:id},
						success:function(text){
						  var data = eval("(" + text + ")");
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();
							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
								$NODE.attr("id",data.newID).prepend("<input type='checkbox' id='"+data.newCheckBoxId+"' />");
								ret=true;
							}
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}

						}
					});

					return ret;
				},
		        beforerename: function(NODE,LANG,TREE_OBJ) {
					$NODE=$(NODE);
					$NODE.data("oldName",$NODE.children("a:eq(0)").text());
					return true;
				},
		        beforedelete: function(NODE,TREE_OBJ,a,b,c) {
					if(confirm("Are you sure you want to delete this page?\nSubpages, if any, will be orphaned !!")){
						var $NODE=$(NODE);
						var e=$NODE.attr("id").match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id=e[2];
						var ret=false;

						var url=base_url+"admin/content/deletePage/";

						$.ajax({
							url:url,
							dataType:"text",
							type:"post",
							async:false,
							data:{id:id},
							success:function(text){
							  var data = eval("(" + text + ")");
								if($.isFunction(data.OVERRIDE)){
									data.OVERRIDE.apply();

								}
								if($.isFunction(data.successFunction)){
									data.successFunction.apply();
									ret=true;
								}
							},
							error:function(){
								if($.isFunction(ajaxFailure)){
									ajaxFailure.apply();
								}

							}
						});
					}

					return ret;
				},

				onrename    : function(NODE,LANG,TREE_OBJ,RB) {
					var $NODE=$(NODE);
					var oldName=$NODE.data("oldName");
					var newName=$NODE.children("a:eq(0)").text();
					var e=$NODE.attr("id").match(/(.+)[-=_](.+)/);
					var type=e[1];
					var id=e[2];

					var url=base_url+"admin/content/renamePage";

					$.ajax({
						url:url,
						dataType:"text",
						type:"post",
						data:{newName:newName,type:type,id:id},
						success:function(text){
						  var data = eval("(" + text + ")");
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();
								$NODE.children("a:eq(0)").text(oldName);
							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
							}
							$("a",NODE).click();
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}
							$NODE.children("a:eq(0)").text(oldName);
						}
					});
				},
		        onmove      : function(NODE,REF_NODE,TYPE,TREE_OBJ,RB) {
					var json=TREE_OBJ.getJSON();
					var url=base_url+"admin/content/reOrderPages";
					$.ajax({
						url:url,
						dataType:"text",
						type:"post",
						data:{data:JSON_stringify(json)},
						success:function(text){
						  var data = eval("(" + text + ")");
							if($.isFunction(data.OVERRIDE)){
								data.OVERRIDE.apply();
							}
							if($.isFunction(data.successFunction)){
								data.successFunction.apply();
							}
							$("a",NODE).click();
						},
						error:function(){
							if($.isFunction(ajaxFailure)){
								ajaxFailure.apply();
							}
						}
					});
				},


		        ondblclk    : function(NODE, TREE_OBJ) { TREE_OBJ.toggle_branch.call(TREE_OBJ, NODE); TREE_OBJ.select_branch.call(TREE_OBJ, NODE); },
				onchange : function (NODE, TREE_OBJ) {
				  	var $NODE=$(NODE);
					var id=$NODE.attr("id");
					if(id!="treebase"){
						var e=id.match(/(.+)[-=_](.+)/);
						var type=e[1];
						var id1=e[2];

						loadPageDetails(id);

					}else{
					  $("#dContent").html($("#defaultContent").html());
					  $("#innerTabs,#thirdBar").hide(0);
					  $("#main-content").removeClass("threepane hastabs");
					  $(window).unbind("resize");
					}
				}

		    }
		}
		);


	//--------END TREE

}
