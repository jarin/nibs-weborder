<?php



namespace entities;

/**
 * ResUser
 *
 * @Table(name="res_user")
 * @Entity
 */
class ResUser
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $code
   *
   * @Column(name="code", type="string", length=50, nullable=true)
   */
  private $code;

  /**
   * @var string $userName
   *
   * @Column(name="user_name", type="string", length=150, nullable=true)
   */
  private $userName;

  /**
   * @var string $password
   *
   * @Column(name="password", type="string", length=40, nullable=true)
   */
  private $password;

  /**
   * @var string $mobile
   *
   * @Column(name="mobile", type="string", length=20, nullable=true)
   */
  private $mobile;

  /**
   * @var string $email
   *
   * @Column(name="email", type="string", length=50, nullable=true)
   */
  private $email;
  
  /**
   * @var string $ipAddress
   *
   * @Column(name="ip_address", type="string", length=15, nullable=true)
   */
  private $ipAddress;
  
  /**
   * @var datetime $lastActivity
   *
   * @Column(name="last_activity", type="datetime", nullable=true)
   */
  private $lastActivity;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=true)
   */
  private $lastUpdate;

  /**
   * @var ResUserType
   *
   * @ManyToOne(targetEntity="ResUserType")
   * @JoinColumns({
   *   @JoinColumn(name="user_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $userType;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;


  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set code
   *
   * @param string $code
   */
  public function setCode($code)
  {
    $this->code = $code;
  }

  /**
   * Get code
   *
   * @return string 
   */
  public function getCode()
  {
    return $this->code;
  }

  /**
   * Set userName
   *
   * @param string $userName
   */
  public function setUserName($userName)
  {
    $this->userName = $userName;
  }

  /**
   * Get userName
   *
   * @return string 
   */
  public function getUserName()
  {
    return $this->userName;
  }

  /**
   * Set password
   *
   * @param string $password
   */
  public function setPassword($password)
  {
    $this->password = $password;
  }

  /**
   * Get password
   *
   * @return string 
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * Set mobile
   *
   * @param string $mobile
   */
  public function setMobile($mobile)
  {
    $this->mobile = $mobile;
  }

  /**
   * Get mobile
   *
   * @return string 
   */
  public function getMobile()
  {
    return $this->mobile;
  }

  /**
   * Set email
   *
   * @param string $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

  /**
   * Get email
   *
   * @return string 
   */
  public function getEmail()
  {
    return $this->email;
  }
  
  /**
   * Set ipAddress
   *
   * @param string $ipAddress
   */
  public function setIpAddress($ipAddress)
  {
  	$this->ipAddress = $ipAddress;
  }
  
  /**
   * Get ipAddress
   *
   * @return string
   */
  public function getIpAddress()
  {
  	return $this->ipAddress;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastActivity
   *
   * @param datetime $lastActivity
   */
  public function setLastActivity($lastActivity)
  {
    $this->lastActivity = $lastActivity;
  }

  /**
   * Get lastActivity
   *
   * @return datetime 
   */
  public function getLastActivity()
  {
    return $this->lastActivity;
  }
  
  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
  	$this->lastUpdate = $lastUpdate;
  }
  
  /**
   * Get lastUpdate
   *
   * @return datetime
   */
  public function getLastUpdate()
  {
  	return $this->lastUpdate;
  }

  /**
   * Set userType
   *
   * @param ResUserType $userType
   */
  public function setUserType(\ResUserType $userType)
  {
    $this->userType = $userType;
  }

  /**
   * Get userType
   *
   * @return ResUserType 
   */
  public function getUserType()
  {
    return $this->userType;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
}