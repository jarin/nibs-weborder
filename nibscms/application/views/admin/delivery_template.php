<?php
$baseUrl=base_url();
?>
<link rel="stylesheet" href="<?=$baseUrl?>assets/css/admin/business.css" type="text/css" media="screen"  />
<script type="text/javascript" src="<?=$baseUrl?>assets/js/admin/delivery.js"></script>
<script type="text/tmpl" id="defaultContent" charset="utf-8">
	<h3><?=$pagename?></h3>
	<div class="initPage">
		<div>
		  <p>Click on the list of businesses on the left to edit....</p>
		  <p>To delete a business use the <img src="<?=$baseUrl?>assets/images/admin/icon_trash.png" /> delete icon...</p>
		  <p>To add a business, click on the <img src="<?=$baseUrl?>assets/images/admin/icon_add.png" /> Plus button...</p>
		</div>
	</div>
</script> 


<div id="main-content" class=''>
  	<div class="container-fluid">
	    <div id="page" class='site-settings'>
	    	<div class="hidden">
		      <div id="addForm">
		       <form id='add_form' method='post' rel='ajaxed' action='<?=$baseUrl?>admin/deliverycharge/add' loadingDiv='loader_add' formType="adder">
					<table cellpadding='4' cellspacing='0' align='center'>
					
						<tr>
							<th align='left' >Distance (Miles)</th>
							<td ><input type='text' name='distance' id='add_distance_input' /></td>
						</tr>
						<tr>
							<th align='left' >Delivery Charge</th>
							<td ><input type='text' name='amount' id='add_amount_input' /></td>
						</tr>
						
						<tr >
							<th align='left' >Is Active</th>
							<td ><input type='checkbox'  name='is_active' value="1" /></td>
						</tr>
						<tr >
							<th align='left'>Do not take Order greater then this distance</th>
							<td ><input type='checkbox'  name='take_order' value="1" /></td>
						</tr>
											
						<tr><td colspan='2'><hr class='hr' /></td></tr>
						<input type='hidden'  name='restaurant' value="<?php echo $restaurant;?>" />
						<td align='right' colspan='2'><button class="button add" type='submit'>Add </button><div id='loader_add'  class='q_feedback'></div></td> </tr>
					</table>
				</form>
		      </div>
	    	</div>
	    	
	      	<div class="row-fluid" id="dContent">
		 	</div>
	  	</div>
	</div>
</div>


<div id="sidebar" class="nav-collapse collapse">
	<div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
	<div id="sidebarHeading">
		<div class="headertoolbar">
			<span>
				<em>Delivery Charge</em>
			</span>

	  		<div id="searchCount">0 Filtered Results</div> 
			<button class="addBusinessBtn" title="Add Business" style=''>
				<img src="<?=$baseUrl?>assets/images/admin/icon_add.png" style='margin-top:-6px'/>
			</button>
		</div>
  	</div> 
  	<ul class="sidebar-menu list">
   	 <?=$list?>
  	</ul>
  	<div class="footertoolbar dark"><div id="filterer"><input type="text" id="theFilter" /><a href="#" title="Clear Filter"></a></div></div>
</div>
