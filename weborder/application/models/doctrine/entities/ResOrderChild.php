<?php



namespace entities;

/**
 * ResOrderChild
 *
 * @Table(name="res_order_child")
 * @Entity
 */
class ResOrderChild
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var smallint $quantity
   *
   * @Column(name="quantity", type="smallint", nullable=false)
   */
  private $quantity;

  /**
   * @var ResQuantityType
   *
   * @ManyToOne(targetEntity="ResQuantityType")
   * @JoinColumns({
   *   @JoinColumn(name="quantity_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $quantityTypeId;

  /**
   * @var decimal $amount
   *
   * @Column(name="amount", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $amount=0;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResOrder
   *
   * @ManyToOne(targetEntity="ResOrder")
   * @JoinColumns({
   *   @JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $order;

  /**
   * @var ResProduct
   *
   * @ManyToOne(targetEntity="ResProduct")
   * @JoinColumns({
   *   @JoinColumn(name="product_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $product;

  /**
   * @var ResOrderStatus
   *
   * @ManyToOne(targetEntity="ResOrderStatus")
   * @JoinColumns({
   *   @JoinColumn(name="status_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $status;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var boolean $isPrintQue
   *
   * @Column(name="is_print_que", type="boolean", nullable=false)
   */
  private $isPrintQue=FALSE;
  
  /**
   * @var boolean $isAddOn
   *
   * @Column(name="is_add_on", type="boolean", nullable=false)
   */
  private $isAddOn=FALSE;
  
  /**
   * @var boolean $hasSendToKitchen
   *
   * @Column(name="has_send_to_kitchen", type="boolean", nullable=false)
   */
  private $hasSendToKitchen=FALSE;
  
  
  /**
   * @var smallint $queSequenceNumber
   *
   * @Column(name="que_sequence_number", type="smallint", nullable=false)
   */
  private $queSequenceNumber;


  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set quantity
   *
   * @param smallint $quantity
   */
  public function setQuantity($quantity)
  {
    $this->quantity = $quantity;
  }

  /**
   * Get quantity
   *
   * @return smallint 
   */
  public function getQuantity()
  {
    return $this->quantity;
  }

  /**
   * Set quantityTypeId
   *
   * @param boolean $quantityTypeId
   */
  public function setQuantityTypeId($quantityTypeId)
  {
    $this->quantityTypeId = $quantityTypeId;
  }

  /**
   * Get quantityTypeId
   *
   * @return boolean 
   */
  public function getQuantityTypeId()
  {
    return $this->quantityTypeId;
  }

  /**
   * Set amount
   *
   * @param decimal $amount
   */
  public function setAmount($amount)
  {
    $this->amount = $amount;
  }

  /**
   * Get amount
   *
   * @return decimal 
   */
  public function getAmount()
  {
    return $this->amount;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set order
   *
   * @param ResOrder $order
   */
  public function setOrder(\ResOrder $order)
  {
    $this->order = $order;
  }

  /**
   * Get order
   *
   * @return ResOrder 
   */
  public function getOrder()
  {
    return $this->order;
  }

  /**
   * Set product
   *
   * @param ResProduct $product
   */
  public function setProduct(\ResProduct $product)
  {
    $this->product = $product;
  }

  /**
   * Get product
   *
   * @return ResProduct 
   */
  public function getProduct()
  {
    return $this->product;
  }

  /**
   * Set status
   *
   * @param ResStatus $status
   */
  public function setStatus(\ResStatus $status)
  {
    $this->status = $status;
  }

  /**
   * Get status
   *
   * @return ResStatus 
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
  
  /**
   * Set queSequenceNumber
   *
   * @param smallint $queSequenceNumber
   */
  public function setQueSequenceNumber($queSequenceNumber)
  {
  	$this->queSequenceNumber = $queSequenceNumber;
  }
  
  /**
   * Get queSequenceNumber
   *
   * @return smallint
   */
  public function getQueSequenceNumber()
  {
  	return $this->queSequenceNumber;
  }
  
  /**
   * Set isPrintQue
   *
   * @param boolean $isPrintQue
   */
  public function setIsPrintQue($isPrintQue)
  {
  	$this->isPrintQue = $isPrintQue;
  }
  
  /**
   * Get isPrintQue
   *
   * @return boolean
   */
  public function getIsPrintQue()
  {
  	return $this->isPrintQue;
  }
  
  /**
   * Set isAddOn
   *
   * @param boolean $isAddON
   */
  public function setIsAddOn($isAddOn)
  {
      $this->isAddOn = $isAddOn;
  }
  
  /**
   * Get isAddOn
   *
   * @return boolean
   */
  public function getIsAddOn()
  {
      return $this->isAddOn;
  }
  
  /**
   * Set hasSendToKitchen
   *
   * @param boolean $hasSendToKitchen
   */
  public function setHasSendToKitchen($hasSendToKitchen)
  {
  	$this->hasSendToKitchen = $hasSendToKitchen;
  }
  
  /**
   * Get hasSendToKitchen
   *
   * @return boolean
   */
  public function getHasSendToKitchen()
  {
  	return $this->hasSendToKitchen;
  }
}