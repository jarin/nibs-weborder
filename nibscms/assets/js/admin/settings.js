$(function(){

  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  $("button.resetButton").live("click",function(){
		if(confirm("Are you sure?\nAll settings will be reset to their default values")){
			NIBS.util.ajax({group:$(this).attr("rel")},"admin/settings/resetSettings");
		}
	});

	// See More details on click
  $("#sidebar ul.shortlist li").live("click",function(){
		var _li=$(this);
		_li.siblings().removeClass("active");
		_li.addClass("active");
		loadSettings(_li.attr("id"));
		return false;
	});
});

function loadSettings(id){
  var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	$("#dContent").load(base_url+"admin/settings/loadSettings/"+id);
}

  // File Upload Button
$("button.addFileBtn").overlay({width:"30%",inline:true,title:"Add Image",href:"#addDownloadForm"});

function addFile(id,path,caption){
  var html="<div class='download deleted' id='download_"+id+"'>\
  <div><img src='"+path+"'/></div>\
  <span class='caption'><input type='text' name='caption' placeholder='Image Caption' value='"+caption+"'/></span>\
  <span class='activator'><input type='checkbox' id='downloadCheckbox_"+id+"' checked /></span>\
  </div>"
	$("#downloads")[0].scrollTop=0
	$("#downloadInner").prepend(html);
	_add($("#download_"+id));
}

