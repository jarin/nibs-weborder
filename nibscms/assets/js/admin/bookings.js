$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addPageBtn").overlay({width:"30%",inline:true,title:"Add Entry",href:"#addPageForm"});

  //Delete Buttons
	$("#main-content button.delete,#cancelBtn").live("click",function(){
		if(confirm("Are you sure?")){
			NIBS.util.ajax({id:$(this).attr("elId")},"admin/"+$(this).attr("rel"));
		}
		return false;
	});
	
  //Booking Confirmation Buttons
	$("#confirmedByEmail, #confirmedByBoth, #confirmedBySms").live("click",function(){
			$.post(base_url+"admin/"+$(this).attr("rel"),{id:$(this).attr("elId")}, function(data){
				var dt=eval("("+data+")");
				console.log(dt);
				if(dt && dt.success && dt.success===true){
					alert(dt.result);
				}else{
					alert("could not send messsage due to an internal error");
				}
				
			});
		return false;
	});
	

  // See More details on click
  $("#thirdBar ul.comp_multilineListView li").live("click",function(){
    var _li=$(this);
    _li.siblings().removeClass("active");
    _li.addClass("active");
    loadEntryDetails(_li.attr("id"));
    return false;
  });

  //filters
  $("#sidebar li.clickable").live("click",function(){
    var _li=$(this);
    _li.parent().parent().parent().find("li").not(_li).removeClass("active");
    _li.addClass("active");
    loadFiltered(_li.attr("rel"));
    return false;
  });
  $("#searchbookings").click(function(){
    var q=$("#bQuery").val();
    if($.trim(q)!=""){
      $("#sidebar li.active").removeClass("active");
      $("#thirdBar ul.comp_multilineListView").load(base_url+"admin/bookings/filterBookings/",{query:q});
    }
  })


});

function loadFiltered(filter){
  $("#thirdBar ul.comp_multilineListView").load(base_url+"admin/bookings/filterBookings/"+filter);
}

function loadEntryDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
  var id=e[2];

  $("#dContent").load(base_url+"admin/bookings/viewEntry/"+id);

}

function deleteEntry(booking_id){
  var el=$("#booking_"+booking_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
  }
  el.addClass("deleted");
  setTimeout(function(){
    el.remove()
  },500);
  NIBS.notif.notify("success","Entry deleted successfully");
}

function updatecounts(all,a,c,t,tom){
  $("#sidebar li span.count.all").html(all);
  $("#sidebar li span.count.active").html(a);
  $("#sidebar li span.count.cancelled").html(c);
  $("#sidebar li span.count.today").html(t);
  $("#sidebar li span.count.tomorrow").html(tom);
}




