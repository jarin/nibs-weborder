<?php

namespace entities;

/**
 * ResDocContent
 *
 * @Table(name="res_doc_content")
 * @Entity
 */
 class ResDocContent
 {
	/**
   * @var integer $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
	private $id;
  
  
	/**
     * @var ResDocContent
     *
     * @ManyToOne(targetEntity="ResDocContent")
     * @JoinColumns({
     *   @JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $parent;
	/**
     * @var string
     *
     * @Column(name="doc_name", type="string", length=255, nullable=false)
     */
    private $docName;
	/**
     * @var string
     *
     * @Column(name="pagetitle", type="string", length=255, nullable=false)
     */
    private $pagetitle;
	/**
     * @var string
     *
     * @Column(name="stub", type="string", length=255, nullable=false)
     */
    private $stub;
	/**
     * @var string
     *
     * @Column(name="content", type="text", nullable=false)
     */
    private $content;
	/**
     * @var integer
     *
     * @Column(name="sort_order", type="bigint", nullable=false)
     */
    private $sortOrder;
	/**
     * @var string
     *
     * @Column(name="link_url", type="string", length=255, nullable=true)
     */
    private $linkUrl;
	/**
     * @var integer
     *
     * @Column(name="modified_date", type="datetime", nullable=true)
     */
    private $modifiedDate;
	/**
     * @var integer
     *
     * @Column(name="created_date", type="datetime", nullable=false)
     */
    private $createdDate;
	/**
     * @var ResUserType
     *
     * @ManyToOne(targetEntity="ResUserType")
     * @JoinColumns({
     *   @JoinColumn(name="user_type_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $userType;
	/**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
	private $isActive=TRUE;
	
	
	/**
	* Constructor
	*/
	public function __construct()
    {
    	$this->createdDate = new \DateTime("now");
		$this->isActive = 1;
    }
	
	
	
	
	
  
	/**
	* Get id
	*
	* @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}
	
	/**
     * Set parent
     *
     * @param ResDocContent $parent
     * 
     * @return ResDocContent
     */
    public function setParent(ResDocContent $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }
    
    /**
     * Get parent
     *
     * @return ResDocContent
     */
    public function getParent()
    {
        return $this->parent;
    }
	/**
     * Set docName
     *
     * @param string $docName
     * 
     * @return DocContent
     */
    public function setDocName($docName)
    {
        $this->docName = $docName;
    
        return $this;
    }
    
    /**
     * Get docName
     *
     * @return string
     */
    public function getDocName()
    {
        return $this->docName;
    }
	
    /**
     * Set pagetitle
     *
     * @param string $pagetitle
     * 
     * @return DocContent
     */
    public function setPageTitle($pagetitle)
    {
        $this->pagetitle = $pagetitle;
    
        return $this;
    }
    
    /**
     * Get pagetitle
     *
     * @return string
     */
	 public function getPageTitle()
     {
        return $this->pagetitle;
     }
	 /**
     * Set stub
     *
     * @param string $stub
     *
     * @return ResDocContent
     */
	 public function setStub($stub)
     {
        $this->stub = $stub;
    
        return $this;
     }
    
     /**
     * Get stub
     *
     * @return string
     */
     public function getStub()
     {
        return $this->stub;
     }
	 /**
     * Set content
     *
     * @param string $content
     *
     * @return ResDocContent
     */
     public function setContent($content)
     {
        $this->content = $content;
    
        return $this;
     }
    
     /**
      * Get content
      *
      * @return string
      */
     public function getContent()
     {
        return $this->content;
     }
	 
	 /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return ResDocContent
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }
    
    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
	/**
     * Set linkUrl
     *
     * @param string $linkUrl
     *
     * @return DocContent
     */
    public function setLinkUrl($linkUrl)
    {
        $this->linkUrl = $linkUrl;
    
        return $this;
    }
    
    /**
     * Get linkUrl
     *
     * @return string
     */
    public function getLinkUrl()
    {
        return $this->linkUrl;
    }
	 /**
     * Set modifiedDate
     *
     * @param DateTime $modifiedDate
     *
     * @return DocContent
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }
    
    /**
     * Get modifiedDate
     *
     * @return DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }
	/**
     * Set createdDate
     *
     * @param DateTime $createdDate
     *
     * @return DocContent
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    
        return $this;
    }
    
    /**
     * Get createdDate
     *
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }
	/**
     * Set userType
     *
     * @param UserType $userType
     * 
     * @return DocContent
     */
     public function setUserType(UserType $userType)
     {
        $this->userType = $userType;
    
        return $this;
     }
    
    /**
     * Get userType
     *
     * @return UserType
     */
     public function getUserType()
     {
        return $this->userType;
     }
	  /**
     * Set isActive
     *
     * @param integer $isActive
     *
     * @return ResDocContent
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }
    
    /**
     * Get isActive
     *
     * @return integer
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
	
	
	
	
	
	/**
   * Set isFlat
   *
   * @param boolean $isFlat
   */
  public function setIsFlat($isFlat)
  {
  	$this->isFlat = $isFlat;
  }
  
  /**
   * Get isFlat
   *
   * @return boolean
   */
  public function getIsFlat()
  {
  	return $this->isFlat;
  }
	
	
	
	
 }
 