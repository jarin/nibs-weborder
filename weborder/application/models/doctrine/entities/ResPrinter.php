<?php



namespace entities;

/**
 * ResPrinter
 *
 * @Table(name="res_printer")
 * @Entity
 */
class ResPrinter
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $printerName
   *
   * @Column(name="printer_name", type="string", length=100, nullable=false)
   */
  private $printerName;

  /**
   * @var string $ipAddress
   *
   * @Column(name="ip_address", type="string", length=15, nullable=false)
   */
  private $ipAddress;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var ResPrinterModel
   *
   * @ManyToOne(targetEntity="ResPrinterModel")
   * @JoinColumns({
   *   @JoinColumn(name="printer_model_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $printerModel;


  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set printerName
   *
   * @param string $printerName
   */
  public function setPrinterName($printerName)
  {
    $this->printerName = $printerName;
  }

  /**
   * Get printerName
   *
   * @return string 
   */
  public function getPrinterName()
  {
    return $this->printerName;
  }

  /**
   * Set ipAddress
   *
   * @param string $ipAddress
   */
  public function setIpAddress($ipAddress)
  {
    $this->ipAddress = $ipAddress;
  }

  /**
   * Get ipAddress
   *
   * @return string 
   */
  public function getIpAddress()
  {
    return $this->ipAddress;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
  
  /**
   * Set printerModel
   *
   * @param ResPrinterModel $printerModel
   */
  public function setPrinterModel(\ResPrinterModel $printerModel)
  {
      $this->printerModel = $printerModel;
  }
  
  /**
   * Get printerModel
   *
   * @return ResPrinterModel
   */
  public function getPrinterModel()
  {
      return $this->printerModel;
  }
}