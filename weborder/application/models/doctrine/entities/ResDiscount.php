<?php



namespace entities;

/**
 * ResDiscount
 *
 * @Table(name="res_discount")
 * @Entity
 */
class ResDiscount
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
   /**
    * @var ResOrderType
	*
	* @ManyToOne(targetEntity="ResOrderType")
	* @JoinColumns({
	*   @JoinColumn(name="order_type", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
	* })
	*/
  Private $orderType;
  
  /**
   * @var smallint $weekDay
   *
   * @Column(name="week_day", type="smallint", nullable=false)
   */
  private $weekDay;
  
  /**
   * @var decimal $discountRate
   *
   * @Column(name="discount_rate", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $discountRate=0;
  
  /**
   * @var boolean $isFlat
   *
   * @Column(name="is_flat", type="boolean", nullable=false)
   */
  private $isFlat=TRUE;
  
  /**
   * @var decimal $minimumValue
   *
   * @Column(name="minimum_value", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $minimumValue=0;
  
  
  

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }
  
  
  /**
   * Set orderType
   *
   * @param ResOrderType $orderType
   */
  public function setOrderType(\ResOrderType $orderType)
  {
  	$this->orderType = $orderType;
  }
  
  /**
   * Get orderType
   *
   * @return ResOrderType
   */
  public function getOrderTyper()
  {
  	return $this->orderType;
  }
  

  /**
   * Set weekDay
   *
   * @param smallint $weekDay
   */
  public function setWeekDay($weekDay)
  {
      $this->weekDay = $weekDay;
  }
  
  /**
   * Get weekDay
   *
   * @return smallint
   */
  public function getWeekDay()
  {
      return $this->weekDay;
  }
  
  /**
   * Set discountRate
   *
   * @param decimal $discountRate
   */
  public function setDiscountRate($discountRate)
  {
  	$this->discountRate = $discountRate;
  }
  
  /**
   * Get discountRate
   *
   * @return decimal
   */
  public function getDiscountRate()
  {
  	return $this->discountRate;
  }
  
  /**
   * Set isFlat
   *
   * @param boolean $isFlat
   */
  public function setIsFlat($isFlat)
  {
  	$this->isFlat = $isFlat;
  }
  
  /**
   * Get isFlat
   *
   * @return boolean
   */
  public function getIsFlat()
  {
  	return $this->isFlat;
  }
  
  /**
   * Set minimumValue
   *
   * @param decimal $minimumValue
   */
  public function setMinimumValue($minimumValue)
  {
  	$this->minimumValue = $minimumValue;
  }
  
  /**
   * Get minimumValue
   *
   * @return decimal
   */
  public function getMinimumValue()
  {
  	return $this->minimumValue;
  }
  }