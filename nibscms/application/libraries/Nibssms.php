<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * NibsSMS Class
 * 
 * This Class will interact with Nibs SMS using API
 * 
 * @category Library
 * @package  CodeIgniter
 * @author   Tariqul Islam <tareq@webkutir.net>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://nibssolutions.com
 */
class Nibssms
{
    private $xml;
    private $reqparam;

    /**
     * Class Constructor
     * 
     * @param array $config Configuration Array
     */ 
    function __construct($config=array())
    {
	    if (!isset($config['return'])) {
	        $config['return'] = 'json';
	    }
	    
	    if (!isset($config['url'])) {
	        $config['url'] = 'http://nibssms.com/portal/apiclient';
	    }
	    
		$this->xml = new MyXMLElement('<REQUEST/>');
		$this->xml->addChild('RETURNTYPE', $config['return']);
		$this->xml->addChild('USER');
		$this->xml->USER->addChild('LOGIN', $config['userName']);
		$this->xml->USER->addChild('APIKEY', $config['apiKey']);

		$curlfollow	= ((ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) ? true : false );
		$this->setopt = array(
		    CURLOPT_URL => $config['url'],
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => (
                isset( $_SERVER['HTTP_USER_AGENT'] ) ? 
                    $_SERVER['HTTP_USER_AGENT'] : 
                    "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)"
            ),
            CURLOPT_MAXREDIRS => 5,
            CURLOPT_FOLLOWLOCATION => $curlfollow,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false
        );
    }

	/**
	 * Constructs XML
	 * 
	 * @param array $data Array of Data
	 * 
	 * @return void
	 */
	public function setMethod($data)
    {
        $this->reqparam=$this->xml->addChild('REQUESTPARAM');
        $this->reqparam->addChild('METHOD', $data['method']);

        if (isset($data['params'])) {
            $this->reqparam->addChild('PARAMS');
            $this->_setRecurrsiveMethod($this->reqparam->PARAMS, $data['params']);
        }
    }

    /**
     * Sets XML Elements Resursively
     * 
     * @param node  $root Root Node of the XML
     * @param array $data Data Array
     * 
     * @return void
     */
    private function _setRecurrsiveMethod($root, $data)
    {
        foreach ($data as $k=>$v) {
            if (is_array($v)) {
                $newChild = strtoupper($k);
                $root->addChild($newChild);
                $this->_setRecurrsiveMethod($root->$newChild, $v);
            } else {
                if (!is_numeric($k)) {
                    $root->addChild(strtoupper($k), $v);
                } else {
                    $parent = $root->getParentNode();
                    $son = $root->getName();
                    if ($parent->$son=='') {
                        $parent->$son = $v;
                    } else {
                        $parent->addChild(strtoupper($son), $v);
                    }
                }
            }
        }
    }

    /**
     * Helper method to see the generated XML File
     * 
     * @return void
     */
    public function getXML()
    {
        $message=$this->xml->asXML();
        echo $message;
    }

    /**
     * Calls Nibs SMS API
     * 
     * @return array
     */
    public function call()
    {
        $message=$this->xml->asXML();

        $ch = curl_init();
        foreach ($this->setopt as $k=>$v) {
            curl_setopt($ch, $k, $v);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'Content-length: '.strlen($message)));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message);

        $return = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($return, true);

        if (count($data)>1) {
            return $data;
        } else {
            if (is_null($data)) {
                $data['SUCCESS'] = true;
                return $data;
            } else {
                if (array_shift(array_keys($data))!='ERROR') {
                    return $data[array_shift(array_keys($data))];
                } else {
                    return $data;
                }
            }
        }
    }
}

class MyXMLElement extends SimpleXMLElement
{
    /**
     * Function to Get Parent Node
     * 
     * @return node
     */
    public function getParentNode()
    {
        return current($this->xpath('parent::*'));
    }
}
