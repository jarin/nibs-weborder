<?php



namespace entities;

/**
 * ResVoucherExclusions
 *
 * @Table(name="res_voucher_exclusions")
 * @Entity
 */
class ResVoucherExclusions
{
  /**
   * @var integer $id
   *
   * @Column(name="id", type="integer", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $exclusiveDaysId
   *
   * @Column(name="exclusive_days_id", type="string", length=45, nullable=true)
   */
  private $exclusiveDaysId;
  
  /**
   * @var ResVoucher
   *
   * @ManyToOne(targetEntity="ResVoucher")
   * @JoinColumns({
   *   @JoinColumn(name="voucher_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $voucher;
  
  /**
   * @var ResOrderType
   *
   * @ManyToOne(targetEntity="ResOrderType")
   * @JoinColumns({
   *   @JoinColumn(name="exclusive_order_type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $exclusiveOrderType;


  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set exclusiveDaysId
   *
   * @param string $exclusiveDaysId
   */
  public function setExclusiveDaysId($exclusiveDaysId)
  {
    $this->exclusiveDaysId = $exclusiveDaysId;
  }

  /**
   * Get exclusiveDaysId
   *
   * @return string 
   */
  public function getExclusiveDaysId()
  {
    return $this->exclusiveDaysId;
  }
  
  /**
   * Set voucher
   *
   * @param ResVoucher $voucher
   */
  public function setVoucher(\ResVoucher $voucher)
  {
  	$this->voucher = $voucher;
  }
  
  /**
   * Get voucher
   *
   * @return ResVoucher
   */
  public function getVoucher()
  {
  	return $this->voucher;
  }
  
  /**
   * Set exclusiveOrderType
   *
   * @param ResOrderType $exclusiveOrderType
   */
  public function setExclusiveOrderType(\ResOrderType $exclusiveOrderType)
  {
  	$this->exclusiveOrderType = $exclusiveOrderType;
  }
  
  /**
   * Get exclusiveOrderType
   *
   * @return ResOrderType
   */
  public function getExclusiveOrderType()
  {
  	return $this->exclusiveOrderType;
  }
}