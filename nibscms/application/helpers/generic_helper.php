<?php

function _checkAuthentication(&$CI){
	if(!isset($CI->_userid) || $CI->_userid==""){
		if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
			header("HTTP/1.1 412 Precondition Failed");
			exit;
		}else{
			header("Location: ".base_url()."admin/authentication/showlogin");
			exit;
		}
	}else{
		//check user type
		if($CI->_utype!="admin"){
			//load permissions matrix
			$CI->load->model("users_m","users");
			$matrix=$CI->users->get($CI->_userid);
			$pMatrix=unserialize($matrix["pMatrix"]);
			if(is_array($pMatrix)){
				//find out who called this function
				$trace=debug_backtrace();
				$caller=$trace["1"];
				$C_funct=$caller['function'];
				if (isset($caller['class'])){
					$C_class=$caller['class'];
				}
				//By Default, users are allowed every where
				//have to manually uncheck items you would want them to be blocked on
				if(isset($pMatrix[$C_class])){
					if(isset($pMatrix[$C_class][$C_funct])){
						_kickOut();
					}else{
						return true;
					}
				}else{
					return true;
				}



			}else{
				return true;
			}
		}else{
			//administrator, so allowed to anywhere
			return true;
		}
	}
}

function _loadSettings(&$CI){
	$settings=$CI->settings->getAll();
	$CI->SETTINGS=array();
	if(is_array($settings)){
		foreach ($settings as $set) {
			$def=$set["default"];
			if(trim($set["value"])!="^"){
				$def=$set["value"];
			}
			$CI->SETTINGS[$set["name"]]=$def;
		}
	}
}

function activitylog(&$CI,$msg,$type="m"){
  $CI->activity->create(array("time"=>time(),"type"=>$type,
    "log"=>$msg." by ".$CI->_username));
}

function _kickOut(){
	if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
		header("HTTP/1.1 403 Forbidden");
		exit;
	}else{
		header("Location: ".base_url()."admin/authentication/kickout");
		exit;
	}
}

/**
 * Camelize
 *
 * Takes multiple words separated by spaces or underscores and camelizes them
 *
 * @access	public
 * @param	string
 * @return	str
 */

	function camelize($str)
	{
		$str = 'x'.strtolower(trim($str));
		$str = ucwords(preg_replace('/[\s_]+/', ' ', $str));
		return substr(str_replace(' ', '', $str), 1);
	}


/*
 * It will make myFieldName to my_field_name
*/
function _uncamelize($camel,$splitter="_") {
	$camel=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter.'$0', $camel));
	//$uncamel = strtolower( join($splitter, $matches[0]) );
	return $camel;
}

/**
 * Uncamelize
 *
 * returns uncamelized versions of camel case strings
 * example input: theQuickBrownFox
 * output: The Quick Brown Fox
 *
 * @access	public
 * @param string
 * @param string
 * @return	string
 */
function _uncamelizeextended($camel,$case="camel",$splitter=" ") {
	$camel=preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter.'$0', $camel));
	if($case=='camel'){
		return ucfirst($camel);
	}else{
		return strtolower($camel);
	}
}

function _notify($type="",$msg=""){
	echo '{success:"success",successFunction:function(){
		$("#notifyBar").html("<div class=\"inner '.$type.'\">'.$msg.'</div>");
	}}';
}

function _file_extension($filename){
	$x = explode('.', $filename);
	$ext=end($x);
	$filenameSansExt=str_replace('.'.$ext,"",$filename);
	return array(
		"filename"=>$filenameSansExt,
		"extension"=>'.'.$ext,
		"extension_undotted"=>$ext
		);
}

function _sel_option($list,$val=""){
	$temp1=explode("value='",$list);
	for($i=0;$i<count($temp1);$i++){
		$temp2=explode(">",$temp1[$i]);
		if(($temp2[0]==($val."'")) || ($temp2[0]==($val."'")) ){
			$temp2[0]=$temp2[0]." Selected";
		}
		$temp3[$i]=implode(">",$temp2);
		unset($temp2);
	}
	$join1=implode("value='",$temp3);
	return $join1;
}

function _mapFileToIcon($filename){
	$f=_file_extension($filename);
	$extension=strtolower($f["extension_undotted"]);
	$type="Uncategorized";
	switch($extension){
		case "jpg":
		case "png":
		case "gif":
		case "jpeg":
		case "bmp":
		case "psd":
		case "tif":
		case "odt":
			$type="Images";
		break;
		case "doc":
		case "docx":
		case "odf":
		case "wpd":
		case "wps":
		case "pages":
			$type="Word Documents";
		break;
		case "xls":
		case "xlsx":
		case "csv":
		case "ods":
			$type="Excel Documents";
		break;
		case "txt":
		case "rtf":
		case "msg":
		case "log":
			$type="Plain Text Files";
		break;
		case "pdf":
		case "indd":
		case "qxd":
		case "qxp":
			$type="PDF Documents";
		break;
		case "htm":
		case "html":
		case "php":
		case "phps":
		case "js":
		case "css":
		case "xml":
			$type="HTML Files";
		break;
		case "zip":
		case "tar":
		case "gz":
		case "rar":
		case "pkg":
		case "sit":
		case "sitx":
		case "sea":
			$type="Zip Archives";
		break;
		case "ppt":
		case "pptx":
		case "pps":
		case "key":
		case "odp":
			$type="Presentations";
		break;
		default:
			$type="Uncategorized";
		break;
	}
	$typeArray=array(
		"Zip Archives"=>"assets/images/admin/zip.png",
		"PDF Documents"=>"assets/images/admin/pdf.png",
		"Word Documents"=>"assets/images/admin/doc.png",
		"Excel Documents"=>"assets/images/admin/xls.png",
		"Plain Text Files"=>"assets/images/admin/txt.png",
		"Images"=>"assets/images/admin/image.png",
		"HTML Files"=>"assets/images/admin/html.png",
		"Presentations"=>"assets/images/admin/ppt.png",
		"Uncategorized"=>"assets/images/admin/file.png"
		);
	return $typeArray[$type];
}

function _prettyDate($time){
  $now=strtotime(date("m/d/Y",time()));
  $diff=$now-$time;
  if($diff<0){
    $negative=true;
    $diff=abs($diff);
  }else{
    $negative=false;
  }
  $day_diff=floor($diff/86400);
  if($day_diff!=(int)$day_diff || $day_diff<0){
    return "";
  }
  if($day_diff==0){
    if(date("d",$time)!==date("d",$now)){
      $str="Yesterday";
    }else{
      $str="Today";
    }
  }else{
    switch($day_diff){
      case $day_diff==1:
        $str="Yesterday";
      break;
      case $day_diff<14:
        $str=$day_diff." days";
      break;
      case $day_diff<62:
        $str=ceil($day_diff/7)." weeks";
      break;
      case $day_diff<731:
        $str=ceil($day_diff/30)." months";
      break;
      case $day_diff>731:
        $str=ceil($day_diff/365)." years";
      break;
    }
  }
  if($negative){
    if($str=="Yesterday"){
      $str="Tomorrow";
    }elseif($str=="Today"){
      $str=$str;
    }else{
      $str="in ".$str;
    }
  }else{
    if($str!="Yesterday" && $str!="Today"){
      $str=$str." ago";
    }
  }
  return $str;
}

function getSMSConfig(&$CI){
	$CI->load->model("common_m");
	$restaurantId = $CI->common_m->getAValue("restaurant", "id", array("user_id"=>$CI->session->userdata("uid")));
	$config['apiKey'] = $CI->common_m->getAValue("general_setting", "value", array("settings_name"=>"nibssms_api_key","restaurant_id"=>$restaurantId));
	$config['userName'] = $CI->common_m->getAValue("general_setting", "value", array("settings_name"=>"nibssms_username","restaurant_id"=>$restaurantId));
	$CI->sender = $CI->common_m->getAValue("general_setting", "value", array("settings_name"=>"nibssms_sender_name","restaurant_id"=>$restaurantId));
	$CI->countryName = $CI->common_m->getAValue("general_setting", "value", array("settings_name"=>"RestaurantCountry","restaurant_id"=>$restaurantId));
	$CI->country = $CI->common_m->getAValue("res_country", "id", array("country_name"=>$CI->countryName));
	$CI->load->library('nibssms', $config);

}

?>
