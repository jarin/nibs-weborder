<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Customer Controller
 *
 * @category Controller
 * @package  CodeIgniter
 * @author   Jarin Anika <jarinanika@gmail.com>
 * @license  http://directory.fsf.org/wiki/License:ReciprocalPLv1.3 Reciprocal Public License v1.3
 * @link     http://nibssolutions.com
 */

class Customer extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		
		
	}

	
	public function addCustomer()
	{
		
		$this->data['page_title'] = "Register";
		$this->load->library('form_validation');
		$uri = $this->input->get('uri');
		if($uri===FALSE){
			$uri = '';
		}
		
		$this->form_validation->set_rules('userName', 'User Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('customerName', 'Customer Name', 'required');
		$this->form_validation->set_rules('town', 'Town', 'required');
		$this->form_validation->set_rules('state', 'State', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->viewPage('addCustomer',array("uri"=>$uri));
		} else {
			$data = $this->input->post(NULL, TRUE);
			$ary = $data;
			$result = $this->common_m->isUnique("ResCustomer", array("userName" => $data['userName']));
			if ($result) {
				if($data['town']==''){
					$data['town']='';
				}else{
					$town = $this->common_m->getAValue("ResCity", "id", array("cityName"=>$data['town']));
					if($town===FALSE){
						$result = $this->common_m->insert("ResCity", array("cityName"=>$data['town']));
						$data['town'] = $result;
					}else{
						$data['town'] = $town;
					}
				}
			
				if($data['state']==''){
					$data['state']='';
				}else{
					$state = $this->common_m->getAValue("ResState", "id", array("stateName"=>$data['state']));
					if($state===FALSE){
						$result = $this->common_m->insert("ResState", array("stateName"=>$data['state']));
						$data['state'] = $result;
					}else{
						$data['state'] = $state;
					}
				}
			
				if($data['country']==''){
					$data['country']=1;
				}else{
					$country = $this->common_m->getAValue("ResCountry", "id", array("countryName"=>$data['country']));
					if($country===FALSE){
						$result = $this->common_m->insert("ResCountry", array("countryName"=>$data['country']));
						$data['country'] = $result;
					}else{
						$data['country'] = $country;
					}
				}
			
				$insertAry = array(
						"customerName"=>$data['customerName'],
						"houseNo"=>$data['houseNo'],
						"houseName"=>$data['houseName'],
						"street"=>$data['street'],
						"townId"=>$data['town'],
						"stateId"=>$data['state'],
						"postCode"=>$data['postCode'],
						"countryId"=>$data['country'],
						"email"=>$data['email'],
						"mobile"=>$data['mobile'],
						"landLine"=>$data['landLine'],
						"customerKey"=>generateCustomerCode(),
						"membershipNo"=>$data['membershipNo']
				);
				$insertAry['userName'] = $data['userName'];
				$insertAry['password'] = makeEncryptedPassword($data['password']);
				$insertAry['dateOfBirth'] = $data['dateOfBirth'];
				$insertAry['anniversaryDate'] = $data['anniversaryDate'];
			
				$address1 = $this->common_m->getAValue("ResSettings", "value", array("settingsName"=>"address"));
				$address2 = "";
				if(isset($ary['houseNo'])){
					$address2 .= $ary['houseNo'].',';
				}
				if(isset($ary['houseName'])){
					$address2 .= $ary['houseName'].',';
				}
				if(isset($ary['street'])){
					$address2 .= $ary['street'].',';
				}
				if($ary['town']!=''){
					$address2 .= $ary['town'].',';
				}
				if($ary['state']!=''){
					$address2 .= $ary['state'].',';
				}
				if(isset($ary['postCode'])){
					$address2 .= $ary['postCode'].',';
				}
				if(isset($ary['country'])){
					$address2 .= $ary['country'].',';
				}
				if($address2!=''){
					$address2 = substr($address2, 0, strlen($address2)-1);
					$insertAry['dist'] = number_format(_calculateDrivingDistanceFromAddress($address1, $address2),1).'miles';
				}
			
				$result = $this->common_m->insert("ResCustomer", $insertAry);
				if(is_numeric($result)){
					
					$this->session->set_userdata("userid", $result);
					 
					$this->session->set_userdata("customer_name", $data['userName']);
					if(isset($data['uri']) && $data['uri'] !=''){
    	                redirect($data['uri']);
    	            } else {
    	                redirect("/order");
    	            }
				}else{
					$this->session->set_flashdata("error", $result);
					redirect("customer/addCustomer");
				}
			} else {
				$this->session->set_flashdata("error", "This Customer Name is already Added.");
				redirect("customer/addCustomer");
			}
		}
		
	}
	
	
	
}