<div id="container" class="row-fluid">
  <div id="sidebar" class="nav-collapse collapse">
    <div class="sidebar-toggler hidden-phone" id="sidebarToggler"></div>
    <div class="navbar-inverse">
          <form class="navbar-search visible-phone" />
          <input type="text" class="search-query" placeholder="Search" />
        </form>
    </div>
  <ul class="sidebar-menu">
    <li class="active">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-dashboard"></i></span>
        Dashboard
      </a>
    </li>
    <li class="has-sub">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-cogs"></i></span>
        Services
        <span class="arrow"></span>
      </a>
      <ul class="sub">
        <li class="has-sub"><a class="" href="sms">SMS</a></li>
        <li><a class="" href="#">Email Marketing</a></li>
        <li><a class="" href="#">Server Monitoring</a></li>
        <li><a class="" href="#">Social Network Management</a></li>
        <li><a class="" href="#">Freelancer Management</a></li>
        <li><a class="" href="#">Project Management</a></li>
        <li><a class="" href="#"><i class="icon-plus"></i>Add a Service</a></li>
      </ul>
    </li>
    <li class="">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-user"></i></span>
        My Contacts
      </a>
    </li>
    <li class="">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-check"></i></span>
        My Tasks
      </a>
    </li>
    <li class="">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-calendar"></i></span>
        My Calendar
      </a>
    </li>
    <li class="">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-user"></i></span>
        Clients
      </a>
    </li>
    <li class="">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-money"></i></span>
        Billing
      </a>
    </li>
    <li class="">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-book"></i></span>
        Knowledge Base
      </a>
    </li>
    <li class="">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-download"></i></span>
        Downloads
      </a>
    </li>
    <li class="">
      <a href="javascript:;" class="">
        <span class="icon-box"><i class="icon-file-alt"></i></span>
        Forum
      </a>
    </li>
  </ul>
</div>

<div id="main-content">
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <!-- <div id="theme-change" class="hidden-phone">
          <i class="icon-cogs"></i>
          <span class="settings">
            <span class="text">Theme:</span>
            <span class="colors">
              <span class="color-default" data-style="default"></span>
              <span class="color-gray" data-style="gray"></span>
              <span class="color-purple" data-style="purple"></span>
              <span class="color-navy-blue" data-style="navy-blue"></span>
            </span>
          </span>
        </div> -->
        <!-- Page Alerts -->
       <!--  <?php
       /*
        $allFlashes = $this->session->all_flashdata();
        if(count($allFlashes) > 0){
            foreach($allFlashes as $k=>$v){
        ?>
        <div class="alert alert-<?=$k?>"><a class="close" data-dismiss="alert">&times;</a> <?=$v?></div>
        <?php }} */?> -->

          <!-- -->
        <h3 class="page-title">
          Dashboard
          <small>
            General Information
          </small>
        </h3>
        <ul class="breadcrumb">
          <li>
            <a href="#">
              <i class="icon-home">
              </i>
            </a>
            <span class="divider">
              &nbsp;
            </span>
          </li>
          <li>
            <a href="#">
              Brand Name
            </a>
            <span class="divider">
              &nbsp;
            </span>
          </li>
          <li>
            <a href="#">
              Dashboard
            </a>
            <span class="divider-last">
              &nbsp;
            </span>
          </li>
          <li class="pull-right search-wrap">
            <form class="hidden-phone" />
            <div class="search-input-area">
              <input id=" " class="search-query" type="text" placeholder="Search" />
              <i class="icon-search">
              </i>
            </div>
          </form>
      </li>
    </ul>
  </div>
</div>
    <div id="page" class="dashboard">
      <div class="row-fluid">