<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Git extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function pull(){
		$data = $this->input->post(NULL, TRUE);
		$data['payload'] = json_decode($data['payload'], TRUE);
		
		if($data['payload']['ref']=='refs/heads/master'){
			$root_path = __DIR__;
			$ary=explode("/",$root_path);
			$n=count($ary);
			unset($ary[$n-1]);
			unset($ary[$n-2]);
			unset($ary[$n-3]);
			unset($ary[$n-4]);
			$root_path=implode("/",$ary);
			
			/*
			$this->load->helper('file');
			write_file($root_path."/logs/git/data.txt", json_encode($data['payload']));
			*/
			
			exec("/bin/bash $root_path/../gitpull.sh > $root_path/../logs/git/data.txt");
		}
	}
}