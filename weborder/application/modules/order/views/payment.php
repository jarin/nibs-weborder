       <div id="page-content">
       
		 <?php $this->load->view('top_content');?>

			<!-- start #main-wrapper -->
			<div class="container">
				<div class="row mt30">
				     <div class="col-md-9 col-sm-12 col-md-push-0">
 						 <?php if($this->session->userdata('cart') !=''){ ?>
                                  
                             <?php 
                            
	                             if($gateway['mode'] == 'test'){
	                             	$paypalUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	                             } else {
	                             	$paypalUrl = 'https://www.paypal.com/cgi-bin/webscr';
	                             }
                             ?>
                            <form id="confirmorder" method="POST" action="<?=$paypalUrl?>">
                                <input type="hidden" name="custom" value="<?=$orderInfo['id']?>"/>
                                <input type="hidden" name="cmd" value="_cart"/>
                                <input type="hidden" name="business" value="<?=$gateway['merchant_id']?>"/>
                                <input type="hidden" name="upload" value="1"/>
                                <input type="hidden" name="currency_code" value="GBP"/>
                                <input type="hidden" name="rm" value="2"/>
                                <input type="hidden" name="discount_amount_cart" value="<?=$discount?>"/>
                               
	            				 <input type="hidden" name="return" value="<?=base_url().'payment/paypal/success'?>"/>
                                 <input type="hidden" name="cancel_return" value="<?=base_url().'payment/paypal/cancel/' .$orderInfo['id'] ?>"/>
                              
                               <input type="hidden" name="notify_url" value="<?=base_url().'payment/paypal/notify'?>"/>
                                <?php 	$i=1;
			    	           foreach ($this->session->userdata('cart') as $key => $cart){
			    	           
			    	           	echo ' <input type="hidden" name="item_name_'.$i.'" value='.$cart['product_name'].' />';
			    	           	echo ' <input type="hidden" name="amount_'.$i.'" value='.$cart['price'].' />';
			    	           	echo ' <input type="hidden" name="quantity_'.$i.'" value='.$cart['quantity'].' />';
							            $i++;
						        }
						        
				             ?>
                              <div class="page-content">
                                 <div class="contact-us" style="padding-bottom:0px;">
                                     <div class="send-message" style="border:0px;">
                                         <h4>Order Information </h4>
											<label >Order Type:</label>
											<span><?php echo $orderInfo['webOrderType']?></span>
											<br/>
											<label>Payment Type:</label>
											<span><?php echo $data['payment']?></span>
									    </div>
									</div>
							</div>
							
						              
							
                    	    <button class="btn btn-default-red" type="submit"  style="float:right;margin-top:15px"><i class="fa fa-money"></i> Payment</button>
						</form>

                     							
					<?php } else { ?>
					<p style="text-align: center;">You do not have any Items</p>
					<?php } ?>
										

                           

				     
					</div>
			            <!-- end .col-md-9 -->

					<?php $this->load->view('left_sidebar')?>
				</div>
				<!-- end .row -->
			</div>
			<!--end .container -->