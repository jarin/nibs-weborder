<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Order Controller
 *
 * @category Controller
 * @package  CodeIgniter
 * @author   Jarin Anika <jarinanika@gmail.com>
 * @license  http://directory.fsf.org/wiki/License:ReciprocalPLv1.3 Reciprocal Public License v1.3
 * @link     http://nibssolutions.com
 */
class Order extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->data['page_title'] = "Order";
        $this->data['limit'] = 20;
        
       
    }
    
	public function index($categoryId='', $subCategoryId='')
	{
		
	    //print_r($this->session->all_userdata());
	    //print_r($this->session->userdata('cart'));
	    $this->data['breadcrumb']['Order'] = 'order';
	    $data = $this->input->post(NULL, TRUE);
	   
	 if(isset($subCategoryId) && $subCategoryId !=''){
	     
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$subCategoryId." OR j.parent=".$subCategoryId.")  ORDER BY j.parentId, j.sequenceNumber, t.sequenceNumber" , "", array('offset'=>0,'limit'=>$this->data['limit']));
	      $count = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$subCategoryId." OR j.parent=".$subCategoryId.") ");
	      $count = count($count);
	 }elseif(isset($categoryId) && $categoryId !=''){
	     
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$categoryId." OR j.parent=".$categoryId.")  ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>0,'limit'=>$this->data['limit']));
	      $count = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$categoryId." OR j.parent=".$categoryId.") ");
	      $count = count($count);
	  }elseif($data){
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.categoryName LIKE '".$data['search']."' OR t.productName LIKE '%".$data['search']."%') ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>0,'limit'=>$this->data['limit']));
	      $count = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.categoryName LIKE '".$data['search']."' OR t.productName LIKE '%".$data['search']."%') ");
	      $count = count($count);
	  } else {
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES."  ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>0,'limit'=>$this->data['limit']));
	      $count = $this->common_m->countRows('ResProduct', array("isActive"=>ANSWER_YES));
	  }
	   $categories = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.parentId IS NULL AND t.isActive=".ANSWER_YES." ORDER BY t.parentId, t.sequenceNumber");
	    foreach ($categories as $k => $v) {
	        $categories[$k]['childCats'] = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.isActive=" . ACTIVE_TABLE . " AND t.parent=" . $categories[$k]['id'] . " ORDER BY t.parentId, t.sequenceNumber");
	    }
	       
	    $vat = '';
	    
	       if(count($items)){
	           $vat_number = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='VatNumber'");
	           if(count($vat_number) > 0 ){
	               $vat = $vat_number[0]['value'];
	               
	               if($vat !='' && $vat!=' '){
	               	
	                   $tax_ar = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='tax'");
	                   if(count($tax_ar) > 0){
	                       $tax = $tax_ar[0]['value'];
	                   }
	                   $tax_per_ar = $this->common_m->runSQL("SELECT is_flat FROM res_settings WHERE settings_name='tax'");
	                   
	                   if(count($tax_per_ar)){
	                       $tax_per = $tax_per_ar[0]['is_flat'];
	                   }
	                   
	                   // add only exclusive tax with price
	                   foreach ($items as $key=> $item){
	                   	$exclusive_tax = 0;
	                   	$exclusive_amount_total = 0;
	                  	if($items[$key]['isTaxInclusive'] == 0 && $items[$key]['isTaxable']==1){
	                   		$exclusive_amount_total =$items[$key]['webOrderPrice'];
	                   	}
	                   	
	                   	if ($tax_per==0){
	                   		$exclusive_tax = ($tax*$exclusive_amount_total)/100;
	                   	}
	                   	$items[$key]['webOrderPriceActual'] = $items[$key]['webOrderPrice'];
	                   	
	                   $items[$key]['webOrderPrice']=number_format($items[$key]['webOrderPrice']+$exclusive_tax,2);
	                   	 
	                   }
	                   
	               } 
	           }
	           
	       }
	       //echo "<pre>";print_r($items);echo '</pre>';exit;
	       //echo $count;
	        $this->viewPage('order',array("items"=>$items, 'count'=>$count,'categories'=>$categories));
	    
	    
	}
	
	public function getNextItem()
	{
	    $data = $this->input->post(NULL, TRUE);
	    
	    if($data['page'] > 0){
	        $offset = $data['data'] - $this->data['limit'];
	        
	    } else {
	         $offset = 0;
	    }
	    
	     $limit = $data['data'];
	   
	     if(isset($data['subCategoryId']) && $data['subCategoryId'] !=0){
	         $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$data['subCategoryId']." OR j.parent=".$data['subCategoryId'].")  ORDER BY j.parentId, j.sequenceNumber, t.sequenceNumber" , "", array('offset'=>$offset,'limit'=>$limit));
	     }elseif(isset($data['categoryId']) && $data['categoryId'] !=0){
	         $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.id=".$data['categoryId']." OR j.parent=".$data['categoryId'].")  ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>$offset,'limit'=>$limit));
	     }elseif($data['search'] && $data['search'] !=''){
	      $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES." AND (j.categoryName LIKE '%".$data['search']."%' OR t.productName LIKE '%".$data['search']."%') ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>$offset,'limit'=>$limit));
	      
	      } else {
	         $items = $this->common_m->getByDql("SELECT t,j FROM entities\\ResProduct t LEFT JOIN t.category j WHERE t.isActive=".ANSWER_YES." AND j.isActive=".ANSWER_YES."  ORDER BY j.parentId, j.sequenceNumber" , "", array('offset'=>$offset,'limit'=>$limit));
	     }
	     
	     $vat = '';
	      
	     if(count($items)){
	     	$vat_number = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='VatNumber'");
	     	if(count($vat_number) > 0 ){
	     		$vat = $vat_number[0]['value'];
	     
	     		if($vat !='' && $vat!=' '){
	     			 
	     			$tax_ar = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='tax'");
	     			if(count($tax_ar) > 0){
	     				$tax = $tax_ar[0]['value'];
	     			}
	     			$tax_per_ar = $this->common_m->runSQL("SELECT is_flat FROM res_settings WHERE settings_name='tax'");
	     
	     			if(count($tax_per_ar)){
	     				$tax_per = $tax_per_ar[0]['is_flat'];
	     			}
	     
	     			// add only exclusive tax with price
	     			foreach ($items as $key=> $item){
	     				$exclusive_tax = 0;
	     				$exclusive_amount_total = 0;
	     				if($items[$key]['isTaxInclusive'] == 0 && $items[$key]['isTaxable']==1){
	     					$exclusive_amount_total =$items[$key]['webOrderPrice'];
	     				}
	     				 
	     				if ($tax_per==0){
	     					$exclusive_tax = ($tax*$exclusive_amount_total)/100;
	     				}
	     				$items[$key]['webOrderPriceActual'] = $items[$key]['webOrderPrice'];
	     				 
	     				$items[$key]['webOrderPrice']=number_format($items[$key]['webOrderPrice']+$exclusive_tax,2);
	     
	     			}
	     
	     		}
	     	}
	     
	     }
	     
	    $this->load->view('nextItems',array("items"=>$items,"limit"=>$limit, "offset"=>$offset));
	}
	
	public function getIngredients(){
	    $data = $this->input->post(NULL, TRUE);
	    $with = array();
	    $without = array();
	    $addon = array();
	
	    $items = $this->common_m->getByDql("SELECT t, j FROM entities\\ResProductIngredient t LEFT JOIN t.ingredient j WHERE t.product=".$data['pid']." AND j.isActive=".ACTIVE_TABLE." ORDER BY t.isWith DESC, j.ingredientName");
	    if(count($items)>0){
	        foreach($items as $k=>$v){
	            if($v['isWith']  ){
	                $with[] = $v['ingredient'];
	            }else{
	                $without[] = $v['ingredient'];  
	            }
	        }
	    }
	    $addons = $this->common_m->getByDql("SELECT t, j FROM entities\\ResProductAddon t LEFT JOIN t.addon j WHERE t.product=".$data['pid']." AND j.isActive=".ACTIVE_TABLE." ORDER BY j.addonName");
	    if(count($addons)>0){
	        foreach($addons as $k=>$v){
	                $addon[] = $v['addon'];
	            
	        }
	    }
	    $vat = '';
	    
	    	$vat_number = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='VatNumber'");
	    	if(count($vat_number) > 0 ){
	    		$vat = $vat_number[0]['value'];
	    
	    		if($vat !='' && $vat!=' '){
	    
	    			$tax_ar = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='tax'");
	    			if(count($tax_ar) > 0){
	    				$tax = $tax_ar[0]['value'];
	    			}
	    			$tax_per_ar = $this->common_m->runSQL("SELECT is_flat FROM res_settings WHERE settings_name='tax'");
	    
	    			if(count($tax_per_ar)){
	    				$tax_per = $tax_per_ar[0]['is_flat'];
	    			}
	    
	    			// add only exclusive tax with price
	    			
	    				
	    				$exclusive_amount_total_ar=$this->common_m->getByDql("SELECT p.webOrderPrice FROM entities\\ResProduct p WHERE p.id=" .$data['pid']. " AND p.isTaxInclusive=0 AND p.isTaxable=1");
	    				if(count($exclusive_amount_total_ar) > 0){
	    					
	    					if(count($with) > 0){
	    					 
	    					foreach ($with as $k=>$v){
	    						$exclusive_tax = 0;
	    						$exclusive_amount_total = 0;
	    						$exclusive_amount_total =$with[$k]['price'];
	    						if ($tax_per==0){
	    							$exclusive_tax = ($tax*$exclusive_amount_total)/100;
	    						}
	    						$with[$k]['price_actual']=$with[$k]['price'];
	    						$with[$k]['exclusive_tax']=number_format($exclusive_tax,2);
	    						$with[$k]['price']=number_format($with[$k]['price'] + $exclusive_tax,2);
	    						
	    					}
	    				}
	    				
	    				if(count($addon) > 0){
	    						
	    					foreach ($addon as $k=>$v){
	    						$exclusive_tax = 0;
	    						$exclusive_amount_total = 0;
	    						$exclusive_amount_total =$addon[$k]['webOrderPrice'];
	    						if ($tax_per==0){
	    							$exclusive_tax = ($tax*$exclusive_amount_total)/100;
	    						}
	    						$addon[$k]['webOrderPriceactual']=$addon[$k]['webOrderPrice'];
	    						$addon[$k]['exclusive_tax']=number_format($exclusive_tax,2);
	    						$addon[$k]['webOrderPrice']=number_format($addon[$k]['webOrderPrice'] + $exclusive_tax,2);
	    						
	    					}
	    				}
	    			}
	    		}
	    	}
	    
	    
	   
	    $this->load->view('ingredients',array("ingredient"=>$items, "with"=>$with, "without"=>$without, "addon" => $addon, "productid"=>$data['pid']));
	}
	
	public function addCart()
	{
	    $data = $this->input->post(NULL, TRUE);
	   
	   print_r($data);exit;
	   if($data){
	        $total = $data['price'] * $data['qty'];
	        $time = __rand_string(6,false);
	        $orderItemId = $time;
	        $cartNew[] = array(
	            'orderItemId' =>$orderItemId,
	            'product_id'=>$data['pid'],
	            'product_name'=>$data['name'],
	            'quantity'=>$data['qty'],
	        	'actual_price'=>$data['actualprice'],
	            'price'=>$data['price'],
	            'total_price'=>$total,
	            'ingredients' =>array()
	        );
	        if($this->session->userdata('cart') !=''){
	        	$found = false;
	        	$total = $data['price'] * $data['qty'];
	            foreach ($this->session->userdata('cart') as $cartitem){
	                if($cartitem['ingredients']){
	                    $ingred = $cartitem['ingredients'];
	                } else {
	                    $ingred = array();
	                }
	                
	                if($cartitem['product_id'] == $data['pid'] && count($ingred) == 0){
	                	$cartAr[] = array(
	                			 'orderItemId' =>$cartitem['orderItemId'],
			                     'product_id'=>$cartitem['product_id'],
			                     'product_name'=>$cartitem['product_name'],
		        	             'quantity'=>$cartitem['quantity']+$data['qty'],
			                   	 'actual_price'=>$cartitem['actual_price'],
		        	             'price'=>$cartitem['price'],
		        	             'total_price'=>$total,
			                     'ingredients' =>$ingred
	                	);
	                	$found = true;
	                
	                } else {
	                   $cartAr[] = array(
	                     'orderItemId' =>$cartitem['orderItemId'],
	                     'product_id'=>$cartitem['product_id'],
	                     'product_name'=>$cartitem['product_name'],
        	             'quantity'=>$cartitem['quantity'],
	                   	 'actual_price'=>$cartitem['actual_price'],
        	             'price'=>$cartitem['price'],
        	             'total_price'=>$cartitem['total_price'],
	                     'ingredients' =>$ingred
                       ); 
	                }
	               
	            }
	            
	            if($found == false){
	            	$this->session->set_userdata('cart',array_merge($cartNew, $cartAr));
	            } else {
	            	$this->session->set_userdata('cart',$cartAr);
	            }
	            
	          
	           
	        } else {
	            $this->session->set_userdata('cart',$cartNew);
	        }
	       
	        $this->load->view('cart');
	    }
	    
	}
    
	public function removeCart()
	{
	    $data = $this->input->post(NULL, TRUE);
	    
	    if($data){
	        if($this->session->userdata('cart') !=''){
	            $cartAr = '';
	            foreach ($this->session->userdata('cart') as $cartitem){
	                if($cartitem['ingredients']){
	                    $ingred = $cartitem['ingredients'];
	                } else {
	                    $ingred = array();
	                }
	                if($cartitem['orderItemId'] != $data['itemId']){
	                    $cartAr[] = array(
	                        'orderItemId' =>$cartitem['orderItemId'],
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                    	'actual_price'=>$cartitem['actual_price'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>$ingred
	                    );
	                   
	                
	                }
	            }
	            $this->session->set_userdata('cart',$cartAr);
	            
	        }
	    }
	    $this->load->view('cart');
	}
	
	public function addIngredient()
	{
	    $data = $this->input->post(NULL, TRUE);
	    if($data){ 
	        $product = $this->common_m->getBy('ResProduct', array('id'=>$data['productid']));
	        $with = array();
	        $without = array();
	        $inst = '';
	        $addon = array();
	        $addon_price = 0;
	        $without_price = 0;
	        $with_price = 0;
	       if(isset($data['withoutingredient'])){
	           $without = $data['withoutingredient'];
	           if(count($without) > 0){
	               foreach ($without as $without_id){
	                   $without_price-= $this->common_m->getAValue('ResIngredient','priceWithout',array('id'=>$without_id));
	               }
	           }
	       }
	       if(isset($data['withingredient'])){
	           $with = $data['withingredient'];
	           if(count($with) > 0){
	               foreach ($with as $with_id){
	                   $with_price+= $this->common_m->getAValue('ResIngredient','price',array('id'=>$with_id));
	               }
	           }
	       }
	       if(isset($data['additionalNote'])){
	           $inst = $data['additionalNote'];
	       }
	       if(isset($data['addon'])){
	           $addon = $data['addon'];
	           if(count($addon) > 0){
	               foreach ($addon as $add_id){
	                  $addon_price+= $this->common_m->getAValue('ResAddon','webOrderPrice',array('id'=>$add_id));
	               }
	           }
	       }
	       
	       $vat = '';
	        
	      
	       	$vat_number = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='VatNumber'");
	       	if(count($vat_number) > 0 ){
	       		$vat = $vat_number[0]['value'];
	       
	       		if($vat !='' && $vat!=' '){
	       
	       			$tax_ar = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='tax'");
	       			if(count($tax_ar) > 0){
	       				$tax = $tax_ar[0]['value'];
	       			}
	       			$tax_per_ar = $this->common_m->runSQL("SELECT is_flat FROM res_settings WHERE settings_name='tax'");
	       
	       			if(count($tax_per_ar)){
	       				$tax_per = $tax_per_ar[0]['is_flat'];
	       			}
	       
	       			       			
	       				$exclusive_tax = 0;
	       				$exclusive_amount_total = 0;
	       				if($product[0]['isTaxInclusive'] == 0 && $product[0]['isTaxable']==1){
	       					$exclusive_amount_total =$product[0]['webOrderPrice'];
	       				}
	       				 
	       				if ($tax_per==0){
	       					$exclusive_tax = ($tax*$exclusive_amount_total)/100;
	       				}
	       				$actual_price = $product[0]['webOrderPrice'];
	       				 
	       				$product[0]['webOrderPrice']=number_format($product[0]['webOrderPrice']+$exclusive_tax,2);
	       				if($addon_price > 0){
	       					if ($tax_per==0){
	       						$addon_price += ($tax*$addon_price)/100;
	       					}
	       				}
	       				
	       				if($with_price > 0){
	       					if ($tax_per==0){
	       						$with_price += ($tax*$with_price)/100;
	       					}
	       				}
	       			
	       
	       		}
	       	}
	       
	       
	       
	       $time = __rand_string(6,false);
	       $orderItemId = $time;
	        $cartNew[] = array(
	            'orderItemId' =>$orderItemId,
	            'product_id'=>$data['productid'],
	            'product_name'=>$product[0]['productName'],
	            'quantity'=>"1",
	        	'actual_price'=>$actual_price,
	            'price'=>number_format($product[0]['webOrderPrice']+$addon_price+$without_price+$with_price, 2),
	            'total_price'=>number_format($product[0]['webOrderPrice']+$addon_price+$without_price+$with_price, 2),
	            'ingredients' =>array('without'=>$without,'without_price'=>$without_price, 'with'=>$with,'with_price'=>$with_price,'addon' => $addon, 'addon_price'=>$addon_price, 'specialInstruction'=>$inst)
	        );
	        
	        if($this->session->userdata('cart') !=''){
	           
	            foreach ($this->session->userdata('cart') as $cartitem){
	                 
	                    if($cartitem['ingredients']){
	                        $ingred = $cartitem['ingredients'];
	                    } else {
	                        $ingred = array();
	                    }
	                    $cartAr[] = array(
	                        'orderItemId' =>$cartitem['orderItemId'],
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                    	'actual_price'=>$cartitem['actual_price'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>$ingred
	                    );
	                
	            }
	             
	            
	            $this->session->set_userdata('cart',array_merge($cartNew, $cartAr));
	                
	            
	        } else {
	            $this->session->set_userdata('cart',$cartNew);
	        }
	        //print_r($this->session->userdata('cart'));
	        $this->load->view('cart');
	    }
	}
	
	
	
	public function editCartItem()
	{
	    $data = $this->input->post(NULL, TRUE);
	    if($data){
	        $with = array();
	        $without = array();
	        $addon = array();
	        
	        $items = $this->common_m->getByDql("SELECT t, j FROM entities\\ResProductIngredient t 
	            LEFT JOIN t.ingredient j WHERE t.product=".$data['pid']." AND j.isActive=".ACTIVE_TABLE." 
	            ORDER BY t.isWith DESC, j.ingredientName");
	        if(count($items)>0){
	            foreach($items as $k=>$v){
	                if($v['isWith']  ){
	                    $with[] = $v['ingredient'];
	                }else{
	                    $without[] = $v['ingredient'];
	                }
	            }
	        }
	        $addons = $this->common_m->getByDql("SELECT t, j FROM entities\\ResProductAddon t 
	            LEFT JOIN t.addon j WHERE t.product=".$data['pid']." AND j.isActive=".ACTIVE_TABLE." 
	            ORDER BY j.addonName");
	        if(count($addons)>0){
	            foreach($addons as $k=>$v){
	                $addon[] = $v['addon'];
	                 
	            }
	        }
	        $vat = '';
	         
	        $vat_number = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='VatNumber'");
	        if(count($vat_number) > 0 ){
	        	$vat = $vat_number[0]['value'];
	        	 
	        	if($vat !='' && $vat!=' '){
	        	  
	        		$tax_ar = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='tax'");
	        		if(count($tax_ar) > 0){
	        			$tax = $tax_ar[0]['value'];
	        		}
	        		$tax_per_ar = $this->common_m->runSQL("SELECT is_flat FROM res_settings WHERE settings_name='tax'");
	        	  
	        		if(count($tax_per_ar)){
	        			$tax_per = $tax_per_ar[0]['is_flat'];
	        		}
	        	  
	        		// add only exclusive tax with price
	        
	        		 
	        		$exclusive_amount_total_ar=$this->common_m->getByDql("SELECT p.webOrderPrice FROM entities\\ResProduct p WHERE p.id=" .$data['pid']. " AND p.isTaxInclusive=0 AND p.isTaxable=1");
	        		if(count($exclusive_amount_total_ar) > 0){
	        
	        			if(count($with) > 0){
	        					
	        				foreach ($with as $k=>$v){
	        					$exclusive_tax = 0;
	        					$exclusive_amount_total = 0;
	        					$exclusive_amount_total =$with[$k]['price'];
	        					if ($tax_per==0){
	        						$exclusive_tax = ($tax*$exclusive_amount_total)/100;
	        					}
	        					$with[$k]['price_actual']=$with[$k]['price'];
	        					$with[$k]['exclusive_tax']=number_format($exclusive_tax,2);
	        					$with[$k]['price']=number_format($with[$k]['price'] + $exclusive_tax,2);
	        	    	
	        				}
	        			}
	        			 
	        			if(count($addon) > 0){
	        					
	        				foreach ($addon as $k=>$v){
	        					$exclusive_tax = 0;
	        					$exclusive_amount_total = 0;
	        					$exclusive_amount_total =$addon[$k]['webOrderPrice'];
	        					if ($tax_per==0){
	        						$exclusive_tax = ($tax*$exclusive_amount_total)/100;
	        					}
	        					$addon[$k]['webOrderPriceactual']=$addon[$k]['webOrderPrice'];
	        					$addon[$k]['exclusive_tax']=number_format($exclusive_tax,2);
	        					$addon[$k]['webOrderPrice']=number_format($addon[$k]['webOrderPrice'] + $exclusive_tax,2);
	        	    	
	        				}
	        			}
	        		}
	        	}
	        }
	        $price = $this->common_m->getAValue('ResProduct','webOrderPrice',array('id'=>$data['pid']));
	        $this->load->view('editItems',array(
	            "ingredient"=>$items, 
	            "with"=>$with, 
	            "without"=>$without,
	            "addon"=>$addon, 
	            "productid"=>$data['pid'],
	            "itemId"=>$data['itemId'],
	            "price"=>$price
	            )
	            );
	    }
	}
	
	public function updateCartItem()
	{
	    $data = $this->input->post(NULL, TRUE);
	    if($data){
	        
	        $with = array();
	        $without = array();
	        $inst = '';
	        $addon = array();
	        $addon_price = 0;
	        $without_price = 0;
	        $with_price = 0;
	       if(isset($data['withoutingredient'])){
	           $without = $data['withoutingredient'];
	           if(count($without) > 0){
	               foreach ($without as $without_id){
	                   $without_price-= $this->common_m->getAValue('ResIngredient','priceWithout',
	                       array('id'=>$without_id));
	               }
	           }
	       }
	       if(isset($data['withingredient'])){
	           $with = $data['withingredient'];
	           if(count($with) > 0){
	               foreach ($with as $with_id){
	                   $with_price+= $this->common_m->getAValue('ResIngredient','price',array('id'=>$with_id));
	               }
	           }
	       }
	       if(isset($data['additionalNote'])){
	           $inst = $data['additionalNote'];
	       }
	       if(isset($data['addon'])){
	           $addon = $data['addon'];
	           if(count($addon) > 0){
	               foreach ($addon as $add_id){
	                  $addon_price+= $this->common_m->getAValue('ResAddon','webOrderPrice',array('id'=>$add_id));
	               }
	           }
	       }
	       $vat = '';
	        
	        
	       $vat_number = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='VatNumber'");
	       if(count($vat_number) > 0 ){
	       	$vat = $vat_number[0]['value'];
	       
	       	if($vat !='' && $vat!=' '){
	       
	       		$tax_ar = $this->common_m->runSQL("SELECT value FROM res_settings WHERE settings_name='tax'");
	       		if(count($tax_ar) > 0){
	       			$tax = $tax_ar[0]['value'];
	       		}
	       		$tax_per_ar = $this->common_m->runSQL("SELECT is_flat FROM res_settings WHERE settings_name='tax'");
	       
	       		if(count($tax_per_ar)){
	       			$tax_per = $tax_per_ar[0]['is_flat'];
	       		}
	       		$exclusive_tax = 0;
	       		$exclusive_amount_total = 0;
	       		$product = $this->common_m->getBy('ResProduct', array('id'=>$data['productid']));
	       		if($product[0]['isTaxInclusive'] == 0 && $product[0]['isTaxable']==1){
	       			$exclusive_amount_total =$product[0]['webOrderPrice'];
	       		}
	       		 
	       		if ($tax_per==0){
	       			$exclusive_tax = ($tax*$exclusive_amount_total)/100;
	       		}
	       		
	       		 
	       		$data['productprice']=number_format($product[0]['webOrderPrice']+$exclusive_tax,2);
	       		if($addon_price > 0){
	       			if ($tax_per==0){
	       				$addon_price += ($tax*$addon_price)/100;
	       			}
	       		}
	       
	       		if($with_price > 0){
	       			if ($tax_per==0){
	       				$with_price += ($tax*$with_price)/100;
	       			}
	       		}
	       		 
	       
	       	}
	       }
	       
	        $cartNew[] = array(
	            'orderItemId' =>$data['itemId'],
	            'product_id'=>$data['productid'],
	            'product_name'=>$data['productname'],
	            'quantity'=>$data['productQty'],
	        	'actual_price'=>$data['productprice'],
	            'price'=>$data['productprice']+$without_price+$with_price+$addon_price,
	            'total_price'=>(($data['productprice']+$without_price+$with_price+$addon_price)*$data['productQty']),
	            'ingredients' =>array(
	                'without'=>$without,
	                'without_price'=>$without_price, 
	                'with'=>$with,'with_price'=>$with_price,
	                'addon' => $addon, 
	                'addon_price'=>$addon_price, 
	                'specialInstruction'=>$inst)
	        );
	        
	        if($this->session->userdata('cart') !=''){
	            $found = false;
	            foreach ($this->session->userdata('cart') as $cartitem){
	                 
	                if($cartitem['orderItemId'] == $data['itemId']){
	                    $found = true;
	                    $cartAr[] = array(
	                        'orderItemId' =>$cartitem['orderItemId'],
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$data['productQty'],
	                    	'actual_price'=>$cartitem['actual_price'],
            	            'price'=>$data['productprice']+$without_price+$with_price+$addon_price,
            	            'total_price'=>(($data['productprice']+$without_price+$with_price+$addon_price)*$data['productQty']),
            	            'ingredients' =>array(
            	                'without'=>$without,
            	                'without_price'=>$without_price, 
            	                'with'=>$with,'with_price'=>$with_price,
            	                'addon' => $addon, 
            	                'addon_price'=>$addon_price, 
            	                'specialInstruction'=>$inst)
	                    );
	                } else {
	                    if($cartitem['ingredients']){
	                        $ingred = $cartitem['ingredients'];
	                    } else {
	                        $ingred = array();
	                    }
	                    $cartAr[] = array(
	                        'orderItemId' =>$cartitem['orderItemId'],
	                        'product_id'=>$cartitem['product_id'],
	                        'product_name'=>$cartitem['product_name'],
	                        'quantity'=>$cartitem['quantity'],
	                    	'actual_price'=>$cartitem['actual_price'],
	                        'price'=>$cartitem['price'],
	                        'total_price'=>$cartitem['total_price'],
	                        'ingredients' =>$ingred
	                    );
	                }
	                
	                
	            }
	             
	            if($found == false){
	                $this->session->set_userdata('cart',array_merge($cartNew, $cartAr));
	                
	            } else {
	                $this->session->set_userdata('cart',$cartAr);
	            }
	        } else {
	            $this->session->set_userdata('cart',$cartNew);
	        }
	        
	    }
	    
	    $this->load->view('cart');
	}
	
	public function confirm()
	{
	    $this->data['breadcrumb']['Confirm'] = 'order/confirm';
	    $categories = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.parentId IS NULL AND t.isActive=".ANSWER_YES." ORDER BY t.parentId, t.sequenceNumber");
	    foreach ($categories as $k => $v) {
	        $categories[$k]['childCats'] = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.isActive=" . ACTIVE_TABLE . " AND t.parent=" . $categories[$k]['id'] . " ORDER BY t.parentId, t.sequenceNumber");
	    }
	    $cart = array();
	   
	    if($this->session->userdata('cart') !=''){
	        
	        foreach ($this->session->userdata('cart') as $cartItem){
	            $without= array();
	            $with =array();
	            $instruction = '';
	            $addon = array();
	            $des = $this->common_m->getAValue('ResProduct','description',array('id'=>$cartItem['product_id']));
	            if(count($cartItem['ingredients']) > 0){
	                if(isset($cartItem['ingredients']['without']) && count($cartItem['ingredients']['without']) > 0){
	                    $without = $this->common_m->getByDql("SELECT t.ingredientName FROM entities\\ResIngredient t WHERE t.id IN(".implode(',',$cartItem['ingredients']['without']).") ");
	                     
	                }
	                if(isset($cartItem['ingredients']['with']) && count($cartItem['ingredients']['with']) >0){
	                    $with = $this->common_m->getByDql("SELECT t.ingredientName FROM entities\\ResIngredient t WHERE t.id IN(".implode(',',$cartItem['ingredients']['with']).") ");
	                
	                }
	                if(isset($cartItem['ingredients']['specialInstruction'])){
	                    $instruction = $cartItem['ingredients']['specialInstruction'];
	                }
	                if(isset($cartItem['ingredients']['addon']) && count($cartItem['ingredients']['addon']) >0){
	                    $addon = $this->common_m->getByDql("SELECT t.addonName FROM entities\\ResAddon t WHERE t.id IN(".implode(',',$cartItem['ingredients']['addon']).") ");
	                     
	                }
	            }
	            $cart[] = array(
	                'orderItemId' =>$cartItem['orderItemId'],
	                'product_id'=>$cartItem['product_id'],
	                'product_name'=>$cartItem['product_name'],
	                'quantity'=>$cartItem['quantity'],
	                'price'=>$cartItem['price'],
	                'total_price'=>$cartItem['total_price'],
	                'description' =>$des,
	                'without'=>$without,
	                'with'=>$with,
	                'instruction' =>$instruction,
	                'addon' => $addon,
	            );
	        }
	    }
	   
	    
	    $this->viewPage('confirmorder',array('categories'=>$categories,'cartItems'=>$cart));
	}
	
	public function checkout()
	{
	    $this->data['breadcrumb']['Checkout'] ='order/checkout';
	   
	    $uid = $this->session->userdata("userid");
	    if(!isset($uid) || !is_numeric($uid)){
	        
	        redirect("/auth/index?uri=order/checkout");
	    } else {
	    	$discount = 0;
	        $categories = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.parentId IS NULL AND t.isActive=".ANSWER_YES." ORDER BY t.parentId, t.sequenceNumber");
	        foreach ($categories as $k => $v) {
	            $categories[$k]['childCats'] = $this->common_m->getByDql("SELECT t FROM entities\\ResCategory t WHERE t.isActive=" . ACTIVE_TABLE . " AND t.parent=" . $categories[$k]['id'] . " ORDER BY t.parentId, t.sequenceNumber");
	        }
	        $user = $this->common_m->getByDql("SELECT t, to, s, c FROM entities\\ResCustomer t LEFT JOIN t.town to LEFT JOIN t.state s LEFT JOIN t.country c WHERE t.id = '".$uid."' ORDER BY t.customerKey");
	        $orderTypeId = $this->common_m->getAValue("ResOrderType", "id", array("orderType"=>'Web Order'));
	       
	        $discountIdAr = $this->common_m->runSQL("SELECT product FROM res_discount_product WHERE discount IN(SELECT id FROM res_discount WHERE week_day = (Select IF(WEEKDAY(CURDATE())=6,  0, WEEKDAY(CURDATE())+1)) AND order_type = " .$orderTypeId. ")");
			if(count($discountIdAr) > 0){
				$discountIds = array();
				foreach ($discountIdAr as $k=>$v){
					
					$discountIds[]= $v['product'];
				}
				
				$discount_total = 0;
				$discountAr = $this->common_m->runSQL("SELECT minimum_value, is_flat, discount_rate FROM res_discount WHERE week_day = (Select IF(WEEKDAY(CURDATE())=6,  0, WEEKDAY(CURDATE())+1)) AND order_type = " .$orderTypeId. "");
				
				foreach ($this->session->userdata('cart') as $k=> $cart){
					if(!in_array($cart['product_id'], $discountIds)){
						$discount_total +=($cart['actual_price']*$cart['quantity']); 
					} 
				}
				if($discount_total > $discountAr[0]['minimum_value']){
					if( $discountAr[0]['is_flat'] == 1){
						$discount = $discountAr[0]['discount_rate'];
					} else {
						$discount = number_format(($discountAr[0]['discount_rate'] * $discount_total)/100,2);
					}
					
				}
			}
			
	       
	        $this->viewPage('checkout',array('categories'=>$categories,'user'=>$user[0],'discount'=>$discount));
	    }
	    
	}
	
	public function confirmorder()
	{
	    
	    $data = $this->input->post(NULL, TRUE);
	   
	    if($data){
	        $order_id = $this->_makeOrder($data);
	        $this->editCustomer($data);
	        if($order_id){
    	        $statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Completed'));
    	        if($this->session->userdata('cart') !=''){
    	           foreach ($this->session->userdata('cart') as $cart){
    	              
    	                   $child_order_id = $this->common_m->insert("ResOrderChild", array("orderId"=>$order_id, "lastUpdate" => mdate("%Y-%m-%d %H:%i:%s"), "isActive"=>ANSWER_YES, "productId"=>$cart['product_id'], "quantity"=>$cart['quantity'], "amount"=>($cart['actual_price']*$cart['quantity']), "statusId"=>$statusId));
    	               
    	                   //Update Product Stock
    	                   /*
    	                    $pro = $this->common_m->getBy("ResProduct", array("id"=>$cart['product_id']), "", "", "", array("stockQuantityType"));
    	                   if (!is_null($pro[0]['stockQuantityType'])) {
    	                   $dql = "UPDATE entities\\ResProduct t SET t.stock = (t.stock - t.quantity) WHERE t.id=".$cart['product_id'];
    	                   $sto = $this->common_m->getByDql($dql);
    	                   }
    	                   */
    	               if($child_order_id){
    	                   if(count($cart['ingredients']) > 0){
    	                       if(isset($cart['ingredients']['without']) && count($cart['ingredients']['without']) > 0){
    	                           foreach ($cart['ingredients']['without'] as $without){
    	                               $withoutinged = $this->common_m->getBy("ResIngredient", array("id"=>$without));
    	                               $description = 'Without '.$withoutinged[0]['ingredientName'];
    	                               $result = $this->common_m->insert("ResProductWithWithout", array("productId"=>$cart['product_id'], "quantity"=>'1', "price"=>$withoutinged[0]['priceWithout'], "ingredientId"=>$without, "orderChildId"=>$child_order_id, "isWith"=>'0', "description"=>$description));
    	                           }
    	                   
    	                       }
    	                   
    	                   
    	                       if(isset($cart['ingredients']['with']) && count($cart['ingredients']['with']) > 0){
    	                           foreach ($cart['ingredients']['with'] as $with){
    	                               $withingred = $this->common_m->getBy("ResIngredient", array("id"=>$with));
    	                               $description = 'With '.$withingred[0]['ingredientName'];
    	                               $result = $this->common_m->insert("ResProductWithWithout", array("productId"=>$cart['product_id'], "quantity"=>'1', "price"=>$withingred[0]['price'], "ingredientId"=>$with, "orderChildId"=>$child_order_id, "isWith"=>'1', "description"=>$description));
    	                           }
    	                   
    	                       }
    	                       if(isset($cart['ingredients']['addon']) && count($cart['ingredients']['addon']) > 0){
    	                           foreach ($cart['ingredients']['addon'] as $addon){
    	                               $result = $this->common_m->insert("ResOrderProductAddon", array("productId"=>$cart['product_id'], "quantity"=>'1', "addonId"=>$addon, "orderChildId"=>$child_order_id));
    	                           }
    	                   
    	                       }
    	                   
    	                       if(isset($cart['ingredients']['specialInstruction']) && $cart['ingredients']['specialInstruction'] !=''){
    	                           $result = $this->common_m->insert("ResSpecialInstructions", array("orderChildId"=>$child_order_id, "instruction"=>$cart['ingredients']['specialInstruction'], "price"=>'0'));
    	                       }
    	                   }
    	               }
    	               
    	               $orderChildIds = $this->common_m->getAValue("ResOrder", "orderChildIds", array("id"=>$order_id, "isActive"=>ANSWER_YES));
    	               if(is_null($orderChildIds)){
    	                   $ary = array();
    	               }else{
    	                   $ary = explode(",",$orderChildIds);
    	               }
    	               
    	               $ary[] = $child_order_id;
    	               $ary = array_unique($ary);
    	               sort($ary);
    	               $orderChildIds = implode(",",$ary);
    	               $result = $this->common_m->update("ResOrder", array("orderChildIds"=>$orderChildIds, "statusId"=>$statusId), array("id"=>$order_id, "isActive"=>ANSWER_YES));
    	               if($result != false){
    	                   $this->session->unset_userdata('cart');
	                  
	                   }
	               }
	            }
	            redirect('order');
	        }
	    }
	}
	
	private function _makeOrder($data)
	{
	    if(!isset($data['date'])){
	        $time = time();
	        $data['date'] = mdate("%Y-%m-%d", $time);
	        $data['time'] = mdate("%H:%i:%s", $time);
	    }
	    $orderTypeId = $this->common_m->getAValue("ResOrderType", "id", array("orderType"=>'Web Order'));
	    $statusId = $this->common_m->getAValue("ResOrderStatus", "id", array("status"=>'Web Order Completed'));
	    if (isset($data['customer_id'])) {
	        $result = $this->common_m->insert("ResOrder", array("lastUpdate" => mdate("%Y-%m-%d %H:%i:%s"), "orderDate" => $data['date'].' '.$data['time'], "isConvertedToTakeAway"=>($data['type']!='Dine In' ? ANSWER_YES : ANSWER_NO), "isActive"=>ANSWER_YES, "tax"=>0, "serviceCharge"=>0, "gratuity"=>0, "subTotal"=>0, "total"=>0, "isPartPayment"=>ANSWER_NO, "isWriteOff"=>ANSWER_NO, "customerId"=>$data['customer_id'], "orderTypeId"=>$orderTypeId, "isPreOrder"=>ANSWER_YES, "statusId"=>$statusId, "isComingFromLocalWeb"=>ANSWER_NO,"isComingFromWeb"=>ANSWER_YES));
	    } else {
	        $result = $this->common_m->insert("ResOrder", array("lastUpdate" => mdate("%Y-%m-%d %H:%i:%s"), "orderDate" => $data['date'].' '.$data['time'], "isConvertedToTakeAway"=>($data['type']!='Dine In' ? ANSWER_YES : ANSWER_NO), "isActive"=>ANSWER_YES, "tax"=>0, "serviceCharge"=>0, "gratuity"=>0, "subTotal"=>0, "total"=>0, "isPartPayment"=>ANSWER_NO, "isWriteOff"=>ANSWER_NO, "orderTypeId"=>$orderTypeId, "isPreOrder"=>ANSWER_YES, "statusId"=>$statusId, "isComingFromLocalWeb"=>ANSWER_NO,"isComingFromWeb"=>ANSWER_YES));
	    }
	    $order_id = $result;
	    return $order_id;
	}
	
	public function editCustomer($data){
	    
	    $ary = $data;
	
	    if($data['town']==''){
	        $data['town']='NULL';
	    }else{
	        $town = $this->common_m->getAValue("ResCity", "id", array("cityName"=>$data['town']));
	        if($town===FALSE){
	            $result = $this->common_m->insert("ResCity", array("cityName"=>$data['town']));
	            $data['town'] = $result;
	        }else{
	            $data['town'] = $town;
	        }
	    }
	
	    if($data['state']==''){
	        $data['country']='NULL';
	    }else{
	        $state = $this->common_m->getAValue("ResState", "id", array("stateName"=>$data['state']));
	        if($state===FALSE){
	            $result = $this->common_m->insert("ResState", array("stateName"=>$data['state']));
	            $data['country'] = $result;
	        }else{
	            $data['country'] = $state;
	        }
	    }
	
	    if($data['country_default']==''){
	        $data['country_default']=1;
	    }else{
	        $country = $this->common_m->getAValue("ResCountry", "id", array("countryName"=>$data['country_default']));
	        if($country===FALSE){
	            $result = $this->common_m->insert("ResCountry", array("countryName"=>$data['country_default']));
	            $data['country_default'] = $result;
	        }else{
	            $data['country_default'] = $country;
	        }
	    }
	
	    foreach($data as $k=>$v){
	        if(!in_array($k, array("town", "country", "country_default"))){
	            if($v==''){
	                $data[$k] = 'NULL';
	            }else{
	                $data[$k]="'".$v."'";
	            }
	        }
	    }
	    if(isset($data['name'])){
	        $data['name']=$data['name'];
	    }
	    if($data['name']!='NULL'){
	        $address1 = $this->common_m->getAValue("ResSettings", "value", array("settingsName"=>"address"));
	        $address2 = "";
	        if($data['house_no']!='NULL'){
	            $address2 .= $ary['house_no'].',';
	        }
	        if($data['house_name']!='NULL'){
	            $address2 .= $ary['house_name'].',';
	        }
	        if($data['street']!='NULL'){
	            $address2 .= $ary['street'].',';
	        }
	        if($ary['town']!=''){
	            $address2 .= $ary['town'].',';
	        }
	        if($ary['state']!=''){
	            $address2 .= $ary['state'].',';
	        }
	        if($data['post_code']!='NULL'){
	            $address2 .= $ary['post_code'].',';
	        }
	        if($ary['country_default']!=''){
	            $address2 .= $ary['country_default'].',';
	        }
	
	        if($address2!=''){
	            $address2 = substr($address2, 0, strlen($address2)-1);
	            $data['dist'] = number_format(_calculateDrivingDistanceFromAddress($address1, $address2),1).'miles';
	
	            $dql = "UPDATE entities\\ResCustomer t SET t.town=".$data['town'].", t.country=".$data['country_default'].", t.state=".$data['country'].", t.postCode=".$data['post_code'].", t.email=".$data['email'].", t.customerName=".$data['name'].", t.houseNo=".$data['house_no'].", t.houseName=".$data['house_name'].", t.street=".$data['street'].", t.mobile=".$data['phone'].", t.dist='".$data['dist']."' WHERE t.id=".$data['customer_id'];
	            $result = $this->common_m->getByDql($dql);
	            return true;
	        }else{
	            return false;
	        }
	    }else{
	        return false;
	    }
	}
	
}
