<?php



namespace entities;

/**
 * ResLoyaltyPoints
 *
 * @Table(name="res_loyalty_points")
 * @Entity
 */
class ResLoyaltyPoints
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var smallint $weekDay
   *
   * @Column(name="week_day", type="smallint", nullable=false)
   */
  private $weekDay;
  
  /**
   * @var time $startTime
   *
   * @Column(name="start_time", type="time", nullable=false)
   */
  private $startTime;
  
  /**
   * @var time $endTime
   *
   * @Column(name="end_time", type="time", nullable=false)
   */
  private $endTime;
  
  /**
   * @var decimal $multiplier
   *
   * @Column(name="multiplier", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $multiplier;
  
  /**
   * @var ResOrderType
   *
   * @ManyToOne(targetEntity="ResOrderType")
   * @JoinColumns({
   *   @JoinColumn(name="order_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $orderType;
  
  

  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set weekDay
   *
   * @param smallint $weekDay
   */
  public function setWeekDay($weekDay)
  {
      $this->weekDay = $weekDay;
  }
  
  /**
   * Get weekDay
   *
   * @return smallint
   */
  public function getWeekDay()
  {
      return $this->weekDay;
  }
  
  /**
   * Set startTime
   *
   * @param time $startTime
   */
  public function setStartTime($startTime)
  {
      $this->startTime = $startTime;
  }
  
  /**
   * Get startTime
   *
   * @return time
   */
  public function getStartTime()
  {
      return $this->startTime;
  }
  
  /**
   * Set endTime
   *
   * @param time $endTime
   */
  public function setEndTime($endTime)
  {
      $this->endTime = $endTime;
  }
  
  /**
   * Get endTime
   *
   * @return time
   */
  public function getEndTime()
  {
      return $this->endTime;
  }
  
  /**
   * Set multiplier
   *
   * @param decimal $multiplier
   */
  public function setMultiplier($multiplier)
  {
      $this->multiplier = $multiplier;
  }
  
  /**
   * Get multiplier
   *
   * @return decimal
   */
  public function getMultiplier()
  {
      return $this->multiplier;
  }
  
  /**
   * Set orderType
   *
   * @param ResStatus $orderType
   */
  public function setOrderType(\ResOrderType $orderType)
  {
      $this->orderType = $orderType;
  }
  
  /**
   * Get orderType
   *
   * @return ResOrderType
   */
  public function getOrderType()
  {
      return $this->orderType;
  }
}