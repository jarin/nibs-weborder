<?php



namespace entities;

/**
 * ResPaymentMethod
 *
 * @Table(name="res_payment_method")
 * @Entity
 */
class ResPaymentMethod
{
  /**
   * @var smallint $id
   *
   * @Column(name="id", type="smallint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $paymentMethodName
   *
   * @Column(name="payment_method_name", type="string", length=150, nullable=false)
   */
  private $paymentMethodName;


  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set paymentMethodName
   *
   * @param string $paymentMethodName
   */
  public function setPaymentMethodName($paymentMethodName)
  {
    $this->paymentMethodName = $paymentMethodName;
  }

  /**
   * Get paymentMethodName
   *
   * @return string 
   */
  public function getPaymentMethodName()
  {
    return $this->paymentMethodName;
  }
}