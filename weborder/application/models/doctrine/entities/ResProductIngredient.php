<?php

namespace entities;

/**
 * ResProductIngredient
 *
 * @Table(name="res_product_ingredient")
 * @Entity
 */
class ResProductIngredient
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var ResProduct
   *
   * @ManyToOne(targetEntity="ResProduct")
   * @JoinColumns({
   *   @JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $product;
  
  /**
   * @var ResIngredient
   *
   * @ManyToOne(targetEntity="ResIngredient")
   * @JoinColumns({
   *   @JoinColumn(name="ingredient_id", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $ingredient;

  /**
   * @var boolean $isWith
   *
   * @Column(name="is_with", type="boolean", nullable=false)
   */
  private $isWith=TRUE;
  
  /**
   * @var bigint $restaurantId
   *
   * @Column(name="restaurant_id", type="bigint", nullable=true)
   */
  private $restaurantId;
  
  
  /**
   * Get id
   *
   * @return smallint 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set product
   *
   * @param ResProduct $product
   */
  public function setProduct(\ResProduct $product)
  {
    $this->product = $product;
  }

  /**
   * Get product
   *
   * @return ResProduct 
   */
  public function getProduct()
  {
    return $this->product;
  }
  
  /**
   * Set ingredient
   *
   * @param ResIngredient $ingredient
   */
  public function setIngredient(\ResIngredient $ingredient)
  {
  	$this->ingredient = $ingredient;
  }
  
  /**
   * Get ingredient
   *
   * @return ResIngredient
   */
  public function getIngredient()
  {
  	return $this->ingredient;
  }

  /**
   * Set isWith
   *
   * @param boolean $isWith
   */
  public function setIsWith($isWith)
  {
  	$this->isWith = $isWith;
  }
  
  /**
   * Get isWith
   *
   * @return boolean
   */
  public function getIsWith()
  {
  	return $this->isWith;
  }
  /**
   * Set restaurantId
   *
   * @param bigint $restaurantId
   */
  public function setRestaurantId($restaurantId)
  {
  	$this->restaurantId = $restaurantId;
  }
  
  /**
   * Get restaurantId
   *
   * @return bigint
   */
  public function getRestaurantId()
  {
  	return $this->restaurantId;
  }
}