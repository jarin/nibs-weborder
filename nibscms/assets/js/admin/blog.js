$(function(){
  //default Content
  var def=$("#defaultContent").html();
  $("#dContent").html(def);

  // Add Button
  $("button.addArticleBtn").overlay({width:"30%",inline:true,title:"Add New Article",href:"#addArticleForm"});
  $("button.addCommentBtn").livequery(function(){
    $(this).overlay({width:"30%",inline:true,title:"Add New Comment",href:"#addCommentForm"});
  });
  $("button.addTagBtn").livequery(function(){
    $(this).overlay({width:"30%",inline:true,title:"Add New tag",href:"#addTagForm"});
  });

  //checkboxes
  $("#sideBar ul.list li input[type=checkbox]").live("click",function(){
		activate_page($(this));
		return false;
	});

	// See More details on click
  $("#sideBar ul.list li").live("click",function(){
		var _li=$(this);
		_li.siblings().removeClass("active");
		_li.addClass("active");
		loadArticleDetails(_li.attr("id"));
		return false;
	});

	//resize MCE editor as soon as it is shown
	$("#innerTabs li").live("click",function(){
	  var _this=$(this);
	  if (_this.index()==1){
	    resizeEditor();
	  }
	});

	//Autosaves
  $("#loadAutoSave").live("click",function(){
    var page_id=$("#EditableArea").parent().find("input[name=page_id]").val(),
    _this=$(this);
    _this.html("Loading... Please wait...").prop("disabled",true).next().prop("disabled",true);
    $.get(base_url+"admin/blog/XgetAutoSave/"+page_id,function(text){
      var data = eval("(" + text + ")");
      if(data && data.data){
        tinyMCE.execInstanceCommand('EditableArea','mceSetContent',false,data.data);
      }
      var _par=_this.parent().parent()
      _par.addClass("hide");
      setTimeout(function(){
    	  _par.remove();
    	},300);
    })

  });
  $("#discardAutoSave").live("click",function(){
    var _par=$(this).parent().parent();
    _par.addClass("hide");
    setTimeout(function(){
  	  _par.remove();
  	},300);
  });

  //--Page versions
	$("#pageVersionsList li").live("click",function(e){
		var _this=$(this);
		var target=e.target
		if(!$(target).is("input")){
			if(!_this.is(".active")){
				if(!_this.is("#version_latestversion")){
					requestVersion(_this);
				}else{
					switchAreas("edit");
					_this.addClass("active").siblings().removeClass("active");
				}
			}
		}else{
			if(!$(target)[0].checked){
				$('#versionCompareButton').prop("disabled",true);
			}
			if($("input:checked",$("#pageVersionsList")).length>2){
				$("input:checked",$("#pageVersionsList")).not(target).eq(1)[0].checked="";
				$('#versionCompareButton').prop("disabled",false);
			}else if($("input:checked",$("#pageVersionsList")).length==2){
				$('#versionCompareButton').prop("disabled",false);
			}
		}
	});

  //--------Comments
  	$("#commentsList li span.delete input").livequery("click",function(){
  		$(this).blur();
  		if(confirm("Are you sure?")){
  			NIBS.util.ajax({id:$(this).attr("elId")},"admin/"+$(this).attr("rel"),"POST","text",function(){
  				updateCommentsCount($("#articleID").val());
  			});
  		}
  		return false;
  	});
  	$("#commentsList input[type=checkbox]").livequery("click",function(){
  		act_deactivateComment($(this));
  		return false;
  	});
  //--------END comments

  //--------Tags
  $("#tagListLink").live("click",function(){
	  var _this=$(this);
		var state=_this.data("menuShowing") || false;
		if(state){
			hidetagHolder();
			_this.removeClass("important").data("menuShowing",false);
		}else{
			showtagHolder();
			_this.addClass("important").data("menuShowing",true);
		}
	});

	//Delete Buttons
	$("#tagHolder li span.delete input").live("click",function(){
		$(this).blur();
		if(confirm("Are you sure?")){
			NIBS.util.ajax({id:$(this).attr("elId")},"admin/"+$(this).attr("rel"));
		}
		return false;
	});

	//Edit Buttons
	$("#tagHolder li span.view a").live("click",function(){
	  var _this=$(this);
	  _this.blur();
	  $("#tagEditor_name").val(_this.parent().next().text());
	  $("#tagEditor_id").val(_this.attr("rel"))
    $.fn.overlay({width:"30%",inline:true,title:"Edit Tag",href:"#editTagForm",open:true})
		return false;
	});

	$("#tagHolder li").livequery(function(){
		$(this).draggable({
			opacity: 0.9,
			revert:"invalid",
			zIndex:"2700",
			helper:"clone"
		});
	});

	$("#tagSoup").livequery(function(){
		$(this).droppable({
			accept:".tag",
			hoverClass:"onDrop",
			drop:function(e,ui){
				var dropped=ui.draggable;
				var e=dropped.attr("id").match(/(.+)[-=_](.+)/);
				var id=e[2];
				var tid=$(this).attr("rel");
				NIBS.util.ajax({article_id:tid,tag_id:id},"admin/blog/addTagToArticle");
			}
		});
	});

	$("#tagSoup span.tag a").livequery("click",function(){
		if(confirm("Are you sure?")){
			var article_id=0;
			var _this=$(this);
			var tag_id=_this.parent().attr("rel");
			var article_id=_this.parent().parent().attr("rel");
			if(article_id > 0 && tag_id > 0){
				NIBS.util.ajax({tag_id:tag_id,article_id:article_id},"admin/blog/removeTagFromArticle");
			}else{
				alert("Error in removing tag...\nPlease try again...\nIf that does not help, reload the page");
			}

		}
	});
  //--------End Tags

});


function addArticle(name,id){
	NIBS.notif.notify("success","Article added");
	var html="<li id='article_"+id+"' class='deleted'>"+
	"<span class='name' title='"+name+"'>"+name+"</span>"+
	"<input title='Delete Article' type='image' class='action delete' rel='blog/deleteArticle' elId='"+id+"' src='"+base_url+"assets/images/admin/icon_trash.png' border='0' />"+
	"<input class='main' id='checkboxmain_"+id+"' type='checkbox' />"
	"</li>";

	$("#sideBar ul.list").prepend(html);
	$("#add_article_form").reset();
	setTimeout(function(){
	  $("#article_"+id).removeClass("deleted");
	},100);
}

function loadArticleDetails(id){
  var e=id.match(/(.+)[-=_](.+)/);
	var id=e[2];
	tinyMCE && tinyMCE.execCommand("mceRemoveControl",false,"EditableArea");
	$("#dContent").load(base_url+"admin/blog/viewArticleDetails/"+id,function(){
	  $(window).bind("resize",NIBS.util.debounce(resizeEditor));
	});
	loadArticleInfo(id);
	$("#articleID").val(id);
	$("#innerTabs,#thirdBar").show(0);
	$("#innerTabs li").removeClass("active").eq(0).addClass("active");
	$("#mainContent").addClass("hastabs threepane");
	loadPageVersions(id);
	updateCommentsCount(id);
}

function loadArticleInfo(id){
  $("#infoPanel").load(base_url+"admin/blog/viewArticleInfo/"+id);
}

function deleteArticle(page_id){
  var el=$("#article_"+page_id);
  if(el.is(".active")){
    $("#dContent").html($("#defaultContent").html());
    $("#innerTabs,#thirdBar").hide(0);
    $("#mainContent").removeClass("threepane hastabs");
    $(window).unbind("resize");
  }
	el.addClass("deleted");
	setTimeout(function(){
	  el.remove()
	},500);
	NIBS.notif.notify("success","Article deleted successfully");
}

function activate_page(el){
  if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var id=e[2];
	}
	NIBS.util.ajax({id:id},"admin/blog/activateDeactivateArticle");
}

function do_activate_article(id,val){
  $("#checkboxmain_"+id).prop("checked",val=="1"?true:false);
  $("#infoPanel").find(".i_state_"+id).html(val=="1"?"Active":"Inactive");
}

function resizeEditor(){
  var ifr=$("#EditableArea_ifr"),
  spn=$("#EditableArea_parent"),
  ifr2=$("#previewIframe"),
  spn2=$("#previewArea");
  ifr.css({height:spn.height()-62});
  ifr2.css({height:spn2.height()-62});
}

//--Comments
function addComment(comment,id,dt,name,email,website,article_id,byOwner){
	var html="<li id='comment_"+id+"' class='clearfix byOwner' style='display:none'>\
		<span class='delete' style='float:right;margin-right:10px'><input type='image' rel='blog/deleteComment' elId='"+id+"' value='Delete Comment' src='"+base_url+"assets/images/admin/icon_trash.png' title='Delete' alt='Delete' /></span>\
		<input id='checkbox_"+id+"' type='checkbox' checked style='float:left;margin:2px 10px 10px 0' />\
		<p>"+comment+"</p>\
		<p class='meta'>"+name+", "+email+", "+website+"</p>\
		<p class='meta'><b>Left on:</b> "+dt+"</p>\
	</li>";
	$("#commentsList").prepend(html);
	$("#add_comment_form").reset();
	$("#comment_"+id).slideDown();
	updateCommentsCount($("#articleID").val());
}

function act_deactivateComment(el){
	if(el){
		var e=el.attr("id").match(/(.+)[-=_](.+)/);
		var type=e[1];
		var id=e[2];
	}
	NIBS.util.ajax({"id":id},"admin/blog/activateDeactivateComment");

}

function do_act_deactivateComment(id,val,article_id){
	if(val=="0"){
		$("#checkbox_"+id).prop("checked",false).parent().addClass("not_visible");
	}else{
		$("#checkbox_"+id).prop("checked",true).parent().removeClass("not_visible");
	}
	updateCommentsCount($("#articleID").val());
}

function deleteComment(id){
	$("#comment_"+id).slideUp(function(){
		$(this).remove();
	});
	updateCommentsCount($("#articleID").val());
}

function updateCommentsCount(article_id){
	$.get(base_url+"admin/blog/XupdateCommentsCount/"+article_id, function(text){
	  var data = eval("(" + text + ")");
	  var cnt=$("#innerTabs li span.count");
		$("span:eq(1)",cnt).html(data.inactive);
		$("span:eq(0)",cnt).html(data.active);
		NIBS.CSSanimate(cnt,"bulge",1.5);
	});
}

//-End Comments

//--tags
function showtagHolder(){
  var tagHolder=$("#tagHolder");
  tagHolder.removeClass("showing").show(0);
  setTimeout(function(){
	  tagHolder.addClass("showing");
	},50);
}

function hidetagHolder(){
  var tagHolder=$("#tagHolder");
  tagHolder.removeClass("showing");
  setTimeout(function(){
	  tagHolder.hide(0);
	},300);
}

function addTag(tag,id){
	var html="<li class='clearfix tag' id='tag_"+id+"' rel='"+id+"'> \
	<span class='delete' style='float:right;margin-right:8px'><input type='image' rel='blog/deleteTag' elId='"+id+"' value='Delete Tag' src='"+base_url+"assets/images/admin/icon_trash.png' title='Delete' alt='Delete' /></span> \
	<span class='view' style='float:right;margin-right:5px'><a href='#' rel='"+id+"' title='Edit' alt='Edit'><img src='"+base_url+"assets/images/admin/icon_edit.png'  border='0' /></a></span> \
	<span class='name'>"+tag+"</span></li>";
	$("#tagHolder ul").append(html);
	$("#add_tag_form").reset();
	$("#tag_"+id).slideDown();

}

function deleteTag(id){
	$("#tag_"+id).fadeOut(function(){
		$(this).remove();
	});
	$("#tagSoup span.tag[rel="+id+"]").fadeOut(function(){
		$(this).remove();
	});
	NIBS.notif.notify("success","Tag Removed");
}

function amendTag(tag,id){
  $("#tag_"+id).find("span.name").html(tag);
  $("#tagSoup span.tag[rel="+id+"]").html(tag+"<a href='#' title='Remove from Article'></a>");
  NIBS.notif.notify("success","Tag Amended");
}

function attachTagToArticle(article_id,tag_id){
	var exists=$("#tagSoup span.tag[rel="+tag_id+"]");
	if(exists.length){
		exists.css({borderColor:"red",color:"red"});
		setTimeout(function(){
			exists.css({borderColor:"",color:""});
		},750);
	}else{
		var html="<span class='tag' rel='"+tag_id+"'>"+$("#tag_"+tag_id+" span.name").html()+"<a title='Remove from Article' href='#'></a></div>";
		$("#tagSoup").append(html);
	}
}

//--Versions
function loadPageVersions(id){
  $.ajax({
    url:base_url+"admin/blog/viewVersionsList/"+id,
    dataType:"text",
    success:function(text){
      var data = eval("(" + text + ")");
      $("#versionPanel").html(data.list);
      $("#versionsCount").html(data.count);
    }
  });
  $('#versionCompareButton').prop("disabled",true);
}
function revertVersion(){
	if(confirm("Are you sure?\nAny unsaved changes will be overwritten!")){
		var contents=$("#previewIframe").contents().find("body").html();
		tinyMCE.activeEditor.setContent(contents);
		$("#version_latestversion").click()
	}
}
function compareVersions(){
	var checked=$("input:checked",$("#pageVersionsList"));
	var vNum1=checked.eq(0).parent().attr("rel");
	var vNum2=checked.eq(1).parent().attr("rel");
	var page_id=checked.eq(0).parent().attr("page_id");
	var url=checked.eq(0).parent().parent().attr("rel");
	url=url+"compareVersions/"+page_id+"/"+vNum1+"/"+vNum2;
	NIBS.util.ajax({},url,"GET");
}
function requestVersion(el) {
	var url=el.parent().attr("rel");
	var vNum=el.attr("rel");
	var page_id=el.attr("page_id")
	url=url+"viewPageVersion/"+page_id+"/"+vNum;
	NIBS.util.ajax({},url,"GET");
}
function switchAreas(type){
	var type=type || "edit";

	var tab=$("#innerTabs li").eq(1);
	if(tab.not(".active")){
	  tab.click();
	}

	if(type=="edit"){
		$("#previewArea").css({
		  "-webkit-transition-property":"-webkit-transform",
		  "-webkit-transition-duration":"0.5s",
		  "-webkit-transform":"translate(-100%,0)",
		  "-moz-transition-property":"-moz-transform",
		  "-moz-transition-duration":"0.5s",
		  "-moz-transform":"translate(-100%,0)",
		});
		$("#editArea").show(0).css({
		  "-webkit-transition-property":"-webkit-transform",
		  "-webkit-transition-duration":"0.5s",
		  "-webkit-transform":"translate(0,0)",
		  "-moz-transition-property":"-moz-transform",
		  "-moz-transition-duration":"0.5s",
		  "-moz-transform":"translate(0,0)",
		});
		setTimeout(function(){
		  $("#previewArea").hide(0);
		  resizeEditor();
		},500);
	}else if(type=="preview"){
		$("#editArea").css({
		  "-webkit-transition-property":"-webkit-transform",
		  "-webkit-transition-duration":"0.5s",
		  "-webkit-transform":"translate(100%,0)",
		  "-moz-transition-property":"-moz-transform",
		  "-moz-transition-duration":"0.5s",
		  "-moz-transform":"translate(100%,0)",
		});
		$("#previewArea").show(0).css({
		  "-webkit-transition-property":"-webkit-transform",
		  "-webkit-transition-duration":"0.5s",
		  "-webkit-transform":"translate(0,0)",
		  "-moz-transition-property":"-moz-transform",
		  "-moz-transition-duration":"0.5s",
		  "-moz-transform":"translate(0,0)",
		});
		setTimeout(function(){
		  $("#editArea").hide(0);
		  resizeEditor();
		},500);
	}
}

function injectContentIntoIframe(content){
	var cssPath=base_url+"assets/js/tinymce/jscripts/tiny_mce/themes/advanced/skins/default/content.css";
	var diffCssPath=base_url+"assets/css/admin/diff.css";
	content="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'\
		'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> \
	<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'><head><link rel='stylesheet' href='"+cssPath+"' type='text/css' /><link rel='stylesheet' href='"+diffCssPath+"' type='text/css' /></head><body>"+content+"</body></html>";
	var iframe=$("#previewIframe")[0];
	var doc = iframe.document;
	if(iframe.contentDocument)
	    doc = iframe.contentDocument; // For NS6
	else if(iframe.contentWindow)
	    doc = iframe.contentWindow.document; // For IE5.5 and IE6
	// Put the content in the iframe
	doc.open();
	doc.writeln(content);
	doc.close();
}
//--END Versions
