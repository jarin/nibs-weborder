<?php



namespace entities;

/**
 * ResOrderReplica
 *
 * @Table(name="res_order_replica")
 * @Entity
 */
class ResOrderReplica
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var ResTable
   *
   * @ManyToOne(targetEntity="ResTable")
   * @JoinColumns({
   *   @JoinColumn(name="table_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $table;

  /**
   * @var datetime $orderDate
   *
   * @Column(name="order_date", type="datetime", nullable=false)
   */
  private $orderDate;

  /**
   * @var boolean $isConvertedToTakeAway
   *
   * @Column(name="is_converted_to_take_away", type="boolean", nullable=false)
   */
  private $isConvertedToTakeAway=FALSE;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;
  
  /**
   * @var boolean $isComingFromWeb
   *
   * @Column(name="is_coming_from_web", type="boolean", nullable=false)
   */
  private $isComingFromWeb=TRUE;
  
  /**
   * @var boolean $isComingFromLocalWeb
   *
   * @Column(name="is_coming_from_local_web", type="boolean", nullable=false)
   */
  private $isComingFromLocalWeb=TRUE;
  
  /**
   * @var ResOrderActionType
   *
   * @ManyToOne(targetEntity="ResOrderActionType")
   * @JoinColumns({
   *   @JoinColumn(name="local_web_action_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $localWebAction;
  
  /**
   * @var boolean $isPartPayment
   *
   * @Column(name="is_part_payment", type="boolean", nullable=false)
   */
  private $isPartPayment=FALSE;
  
  /**
   * @var boolean $isWriteOff
   *
   * @Column(name="is_write_off", type="boolean", nullable=false)
   */
  private $isWriteOff=FALSE;
  
  /**
   * @var boolean $isPreOrder
   *
   * @Column(name="is_pre_order", type="boolean", nullable=false)
   */
  private $isPreOrder=FALSE;

  /**
   * @var datetime $lastUpdate
   *
   * @Column(name="last_update", type="datetime", nullable=false)
   */
  private $lastUpdate;

  /**
   * @var ResCustomer
   *
   * @ManyToOne(targetEntity="ResCustomer")
   * @JoinColumns({
   *   @JoinColumn(name="customer_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $customer;

  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="last_updated_by", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $lastUpdatedBy;
  
  /**
   * @var decimal $tax
   *
   * @Column(name="tax", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $tax=0;
  
  /**
   * @var decimal $serviceCharge
   *
   * @Column(name="service_charge", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $serviceCharge=0;
  
  /**
   * @var decimal $gratuity
   *
   * @Column(name="gratuity", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $gratuity=0;
  
  /**
   * @var decimal $gratuitySettings
   *
   * @Column(name="gratuity_settings", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $gratuitySettings=0;
  
  /**
   * @var boolean $gratuityIsFlat
   *
   * @Column(name="gratuity_is_flat", type="boolean", nullable=false)
   */
  private $gratuityIsFlat=FALSE;
  
  /**
   * @var decimal $subTotal
   *
   * @Column(name="sub_total", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $subTotal=0;
  
  /**
   * @var decimal $total
   *
   * @Column(name="total", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $total=0;
  
  /**
   * @var ResOrderType
   *
   * @ManyToOne(targetEntity="ResOrderType")
   * @JoinColumns({
   *   @JoinColumn(name="order_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL", onUpdate="CASCADE")
   * })
   */
  private $orderType;
  
  /**
   * @var boolean $isPrintQue
   *
   * @Column(name="is_print_que", type="boolean", nullable=false)
   */
  private $isPrintQue=FALSE;
  
  /**
   * @var smallint $queSequenceNumber
   *
   * @Column(name="que_sequence_number", type="smallint", nullable=false)
   */
  private $queSequenceNumber;
  
  /**
   * @var string $orderChildIds
   *
   * @Column(name="order_child_ids", type="string", length=5000, nullable=true)
   */
  private $orderChildIds;
  
  /**
   * @var string $duplicateOrderChildIds
   *
   * @Column(name="duplicate_order_child_ids", type="string", length=5000, nullable=true)
   */
  private $duplicateOrderChildIds;
  
  /**
   * @var ResStatus
   *
   * @ManyToOne(targetEntity="ResOrderStatus")
   * @JoinColumns({
   *   @JoinColumn(name="status_id", referencedColumnName="id", onDelete="RESTRICT", onUpdate="CASCADE")
   * })
   */
  private $status;
  
  /**
   * @var decimal $discount
   *
   * @Column(name="discount", type="decimal", precision=10, scale=2, nullable=false)
   */
  private $discount=0;


  /**
   * Get id
   *
   * @return bigint 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set table
   *
   * @param ResTable $table
   */
  public function setTable(\ResTable $table)
  {
  	$this->table = $table;
  }
  
  /**
   * Get table
   *
   * @return ResTable
   */
  public function getTable()
  {
  	return $this->table;
  }

  /**
   * Set orderDate
   *
   * @param datetime $orderDate
   */
  public function setOrderDate($orderDate)
  {
    $this->orderDate = $orderDate;
  }

  /**
   * Get orderDate
   *
   * @return datetime 
   */
  public function getOrderDate()
  {
    return $this->orderDate;
  }

  /**
   * Set isConvertedToTakeAway
   *
   * @param boolean $isConvertedToTakeAway
   */
  public function setIsConvertedToTakeAway($isConvertedToTakeAway)
  {
    $this->isConvertedToTakeAway = $isConvertedToTakeAway;
  }

  /**
   * Get isConvertedToTakeAway
   *
   * @return boolean 
   */
  public function getIsConvertedToTakeAway()
  {
    return $this->isConvertedToTakeAway;
  }

  /**
   * Set isPartPayment
   *
   * @param boolean $isPartPayment
   */
  public function setIsPartPayment($isPartPayment)
  {
    $this->isPartPayment = $isPartPayment;
  }

  /**
   * Get isPartPayment
   *
   * @return boolean 
   */
  public function getIsPartPayment()
  {
    return $this->isPartPayment;
  }
  
  /**
   * Set isWriteOff
   *
   * @param boolean $isWriteOff
   */
  public function setIsWriteOff($isWriteOff)
  {
  	$this->isWriteOff = $isWriteOff;
  }
  
  /**
   * Get isWriteOff
   *
   * @return boolean
   */
  public function getIsWriteOff()
  {
  	return $this->isWriteOff;
  }
  
  /**
   * Set isPreOrder
   *
   * @param boolean $isPreOrder
   */
  public function setIsPreOrder($isPreOrder)
  {
  	$this->isPreOrder = $isPreOrder;
  }
  
  /**
   * Get isPreOrder
   *
   * @return boolean
   */
  public function getIsPreOrder()
  {
  	return $this->isPreOrder;
  }
  
  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
    $this->isActive = $isActive;
  }

  /**
   * Get isActive
   *
   * @return boolean 
   */
  public function getIsActive()
  {
    return $this->isActive;
  }

  /**
   * Set lastUpdate
   *
   * @param datetime $lastUpdate
   */
  public function setLastUpdate($lastUpdate)
  {
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get lastUpdate
   *
   * @return datetime 
   */
  public function getLastUpdate()
  {
    return $this->lastUpdate;
  }

  /**
   * Set customer
   *
   * @param ResCustomer $customer
   */
  public function setCustomer(\ResCustomer $customer)
  {
    $this->customer = $customer;
  }

  /**
   * Get customer
   *
   * @return ResCustomer 
   */
  public function getCustomer()
  {
    return $this->customer;
  }

  /**
   * Set lastUpdatedBy
   *
   * @param ResUser $lastUpdatedBy
   */
  public function setLastUpdatedBy(\ResUser $lastUpdatedBy)
  {
    $this->lastUpdatedBy = $lastUpdatedBy;
  }

  /**
   * Get lastUpdatedBy
   *
   * @return ResUser 
   */
  public function getLastUpdatedBy()
  {
    return $this->lastUpdatedBy;
  }
  
  /**
   * Set tax
   *
   * @param decimal $tax
   */
  public function setTax($tax)
  {
  	$this->tax = $tax;
  }
  
  /**
   * Get tax
   *
   * @return decimal
   */
  public function getTax()
  {
  	return $this->tax;
  }
  
  /**
   * Set serviceCharge
   *
   * @param decimal $serviceCharge
   */
  public function setServiceCharge($serviceCharge)
  {
  	$this->serviceCharge = $serviceCharge;
  }
  
  /**
   * Get serviceCharge
   *
   * @return decimal
   */
  public function getServiceCharge()
  {
  	return $this->serviceCharge;
  }
  
  /**
   * Set gratuity
   *
   * @param decimal $gratuity
   */
  public function setGratuity($gratuity)
  {
  	$this->gratuity = $gratuity;
  }
  
  /**
   * Get gratuity
   *
   * @return decimal
   */
  public function getGratuity()
  {
  	return $this->gratuity;
  }
  
  /**
   * Set gratuitySettings
   *
   * @param decimal $gratuitySettings
   */
  public function setGratuitySettings($gratuitySettings)
  {
  	$this->gratuitySettings = $gratuitySettings;
  }
  
  /**
   * Get gratuitySettings
   *
   * @return decimal
   */
  public function getGratuitySettings()
  {
  	return $this->gratuitySettings;
  }
  
  /**
   * Set gratuityIsFlat
   *
   * @param boolean $gratuityIsFlat
   */
  public function setGratuityIsFlat($gratuityIsFlat)
  {
  	$this->gratuityIsFlat = $gratuityIsFlat;
  }
  
  /**
   * Get gratuityIsFlat
   *
   * @return boolean
   */
  public function getGratuityIsFlat()
  {
  	return $this->gratuityIsFlat;
  }
  
  /**
   * Set subTotal
   *
   * @param decimal $subTotal
   */
  public function setSubTotal($subTotal)
  {
  	$this->subTotal = $subTotal;
  }
  
  /**
   * Get subTotal
   *
   * @return decimal
   */
  public function getSubTotal()
  {
  	return $this->subTotal;
  }
  
  /**
   * Set total
   *
   * @param decimal $total
   */
  public function setTotal($total)
  {
  	$this->total = $total;
  }
  
  /**
   * Get total
   *
   * @return decimal
   */
  public function getTotal()
  {
  	return $this->total;
  }
  
  /**
   * Set orderType
   *
   * @param ResStatus $orderType
   */
  public function setOrderType(\ResOrderType $orderType)
  {
  	$this->orderType = $orderType;
  }
  
  /**
   * Get orderType
   *
   * @return ResOrderType
   */
  public function getOrderType()
  {
  	return $this->orderType;
  }
  
  /**
   * Set queSequenceNumber
   *
   * @param smallint $queSequenceNumber
   */
  public function setQueSequenceNumber($queSequenceNumber)
  {
  	$this->queSequenceNumber = $queSequenceNumber;
  }
  
  /**
   * Get queSequenceNumber
   *
   * @return smallint
   */
  public function getQueSequenceNumber()
  {
  	return $this->queSequenceNumber;
  }
  
  /**
   * Set isPrintQue
   *
   * @param boolean $isPrintQue
   */
  public function setIsPrintQue($isPrintQue)
  {
  	$this->isPrintQue = $isPrintQue;
  }
  
  /**
   * Get isPrintQue
   *
   * @return boolean
   */
  public function getIsPrintQue()
  {
  	return $this->isPrintQue;
  }
  
  /**
   * Set orderChildIds
   *
   * @param string $orderChildIds
   */
  public function setOrderChildIds($orderChildIds)
  {
  	$this->orderChildIds = $orderChildIds;
  }
  
  /**
   * Get orderChildIds
   *
   * @return string
   */
  public function getOrderChildIds()
  {
  	return $this->orderChildIds;
  }
  
  /**
   * Set duplicateOrderChildIds
   *
   * @param string $duplicateOrderChildIds
   */
  public function setDuplicateOrderChildIds($duplicateOrderChildIds)
  {
  	$this->duplicateOrderChildIds = $duplicateOrderChildIds;
  }
  
  /**
   * Get duplicateOrderChildIds
   *
   * @return string
   */
  public function getDuplicateOrderChildIds()
  {
  	return $this->duplicateOrderChildIds;
  }
  
  /**
   * Set status
   *
   * @param ResOrderStatus $status
   */
  public function setStatus(\ResOrderStatus $status)
  {
  	$this->status = $status;
  }
  
  /**
   * Get status
   *
   * @return ResStatus
   */
  public function getStatus()
  {
  	return $this->status;
  }
  
  /**
   * Set isComingFromWeb
   *
   * @param boolean $isComingFromWeb
   */
  public function setIsComingFromWeb($isComingFromWeb)
  {
  	$this->isComingFromWeb = $isComingFromWeb;
  }
  
  /**
   * Get isComingFromWeb
   *
   * @return boolean
   */
  public function getIsComingFromWeb()
  {
  	return $this->isComingFromWeb;
  }
  
  /**
   * Set isComingFromLocalWeb
   *
   * @param boolean $isComingFromLocalWeb
   */
  public function setIsComingFromLocalWeb($isComingFromLocalWeb)
  {
  	$this->isComingFromLocalWeb = $isComingFromLocalWeb;
  }
  
  /**
   * Get isComingFromLocalWeb
   *
   * @return boolean
   */
  public function getIsComingFromLocalWeb()
  {
  	return $this->isComingFromLocalWeb;
  }

  /**
   * Set localWebAction
   *
   * @param ResOrderActionType $localWebAction
   */
  public function setLocalWebAction(\ResOrderActionType $localWebAction)
  {
  	$this->localWebAction = $localWebAction;
  }
  
  /**
   * Get localWebAction
   *
   * @return ResOrderActionType
   */
  public function getLocalWebAction()
  {
  	return $this->localWebAction;
  }
  
  /**
   * Set discount
   *
   * @param decimal $discount
   */
  public function setDiscount($discount)
  {
  	$this->discount = $discount;
  }
  
  /**
   * Get discount
   *
   * @return decimal
   */
  public function getDiscount()
  {
  	return $this->discount;
  }
}