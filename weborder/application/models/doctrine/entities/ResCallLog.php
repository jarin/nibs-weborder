<?php



namespace entities;

/**
 * ResCallLog
 *
 * @Table(name="res_call_log")
 * @Entity
 */
class ResCallLog
{
  /**
   * @var bigint $id
   *
   * @Column(name="id", type="bigint", nullable=false)
   * @Id
   * @GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var ResUser
   *
   * @ManyToOne(targetEntity="ResUser")
   * @JoinColumns({
   *   @JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", onUpdate="CASCADE")
   * })
   */
  private $user;
  
  /**
   * @var datetime $callDate
   *
   * @Column(name="call_date", type="datetime", nullable=true)
   */
  private $callDate;

  /**
   * @var string $number
   *
   * @Column(name="number", type="string", length=45, nullable=true)
   */
  private $number;

  /**
   * @var boolean $isActive
   *
   * @Column(name="is_active", type="boolean", nullable=false)
   */
  private $isActive=TRUE;

  

  /**
   * Get id
   *
   * @return boolean 
   */
  public function getId()
  {
    return $this->id;
  }
  
  /**
   * Set user
   *
   * @param ResUser $user
   */
  public function setUser(\ResUser $user)
  {
  	$this->user = $user;
  }
  
  /**
   * Get user
   *
   * @return ResUser
   */
  public function getUser()
  {
  	return $this->user;
  }
  
  /**
   * Set callDate
   *
   * @param datetime $callDate
   */
  public function setCallDate($callDate)
  {
  	$this->callDate = $callDate;
  }
  
  /**
   * Get callDate
   *
   * @return datetime
   */
  public function getCallDate()
  {
  	return $this->callDate;
  }

  /**
   * Set number
   *
   * @param string $number
   */
  public function setNumber($number)
  {
    $this->number = $number;
  }

  /**
   * Get number
   *
   * @return string 
   */
  public function getNumber()
  {
    return $this->number;
  }

  /**
   * Set isActive
   *
   * @param boolean $isActive
   */
  public function setIsActive($isActive)
  {
  	$this->isActive = $isActive;
  }
  
  /**
   * Get isActive
   *
   * @return boolean
   */
  public function getIsActive()
  {
  	return $this->isActive;
  }
}